<?php

$etat = [
    [
        'id'        => 1,
        'nom'       => "Alcoolisé",
        'icon'      => 'status_drunk',
        'modSurvie' => 4,
        'modDef'    => 20,
    ],
    [
        'id'        => 2,
        'nom'       => "Drogué",
        'icon'      => 'status_drugged',
        'modSurvie' => 0,
        'modDef'    => 10,
    ],
    [
        'id'        => 3,
        'nom'       => "Terrorisé",
        'icon'      => 'status_terror',
        'modSurvie' => -45,
        'modDef'    => -30,
    ],
    [
        'id'        => 4,
        'nom'       => "Blessé",
        'icon'      => 'status_wound',
        'modSurvie' => -20,
        'modDef'    => -20,
    ],
    [
        'id'        => 5,
        'nom'       => "Infecté",
        'icon'      => 'status_infect',
        'modSurvie' => -20,
        'modDef'    => -15,
    ],
    [
        'id'        => 6,
        'nom'       => "Convalescent",
        'icon'      => 'status_heal',
        'modSurvie' => -10,
        'modDef'    => -10,
    ],
    [
        'id'        => 7,
        'nom'       => "Gueule de bois",
        'icon'      => 'status_hungover',
        'modSurvie' => -5,
        'modDef'    => -10,
    ],
    [
        'id'        => 8,
        'nom'       => "Dépendant",
        'icon'      => 'status_addict',
        'modSurvie' => -10,
        'modDef'    => 15,
    ],
    [
        'id'        => 9,
        'nom'       => "Goule",
        'icon'      => 'status_ghoul',
        'modSurvie' => 5,
        'modDef'    => 0,
    ],
];
// Convertir le tableau en JSON
$json = json_encode($etat, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

// Spécifier le chemin du fichier où vous souhaitez enregistrer le JSON
$cheminFichier = 'src/DataFixtures/data/etat.json';

// Écrire le JSON dans le fichier
file_put_contents($cheminFichier, $json);

echo "Le fichier JSON a été créé avec succès.";