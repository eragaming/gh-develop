Gest'Hordes is a website designed by Eragony and people from the online browser game community "MyHordes" made by the community
from "Hordes" and other versions made by [Motion-Twin](https://motion-twin.com/fr/).

🇫🇷 Licence | 🇬🇧 License
🇫🇷 Ce projet est sous double licence :

Le code source est sous licence GNU General Public License v3.0
Les éléments graphiques dans les dossiers /assets/img et /assets/images sont sous licence Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

🇬🇧 This project is dual-licensed:

The source code is licensed under the GNU General Public License v3.0
The graphical elements in the /assets/img and /assets/images folders are licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

Voir | See LICENSE.md

# Prerequisites :

PHP 8.1, NodeJS  
You can install Laragon on Windows and download php 8.1  
https://forum.laragon.org/topic/166/tutorial-how-to-add-another-php-version-php-7-4-updated

# Using Laragon

Right Click => Quick App => blank (naming your project **sitegesthordes** will help for the setup :P)
Clone this git directory in your newly sitegesthordes created folder

# Install Composer

composer install

# Install Gulp

npm install --global gulp-cli

# Install modules :

npm i gulp-clean-css
npm i gulp-concat-css
npm i gulp-rename

# Sur PhpMyAdmin

Aller sur localhost/phpMyAdmin
Si besoin, ajoutez-le à Laragon : [https://forum.laragon.org/topic/98/tutorial-how-to-add-phpmyadmin](https://forum.laragon.org/topic/98/tutorial-how-to-add-phpmyadmin)
Créer une base de données sitegestroh avec encodage UTF8 (une autre base sitegesthordes doit s'être créée en même temps que le projet)

# Fill your databases

php bin/console app:migrate -u

Note : if your root password or db name are different, you have to change them in env.dev/local

=> il faut la clef app de MH (il faut se créer un serveur local ou avoir une application sur MH)
