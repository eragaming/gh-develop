declare module "*.jpg" {
    const path: any;
    export default path;
}

declare module "*.png" {
    const path: any;
    export default path;
}

declare module "*.svg" {
    const content: any;
    export default content;
}