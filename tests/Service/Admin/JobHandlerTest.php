<?php

namespace App\Tests\Service\Admin;

use App\Entity\JobPrototype;
use App\Repository\JobPrototypeRepository;
use App\Service\Admin\JobHandler;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class JobHandlerTest extends TestCase
{
    private EntityManagerInterface $entityManagerMock;
    private LoggerInterface        $loggerMocker;
    private JobHandler             $jobHandler;
    
    public function setUp(): void
    {
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        
        $this->loggerMocker = $this->createMock(LoggerInterface::class);
        
        $this->jobHandler = new JobHandler($this->entityManagerMock, $this->loggerMocker);
    }
    
    public function testControleExistanceJob()
    {
        
        $jobRepositoryMock = $this->getMockBuilder(JobPrototypeRepository::class)->disableOriginalConstructor()->getMock();
        
        $jobRepositoryMock->expects($this->any())->method('findOneBy')->willReturnOnConsecutiveCalls(new JobPrototype(), null);
        
        $this->entityManagerMock->expects($this->any())->method('getRepository')->with(JobPrototype::class)->willReturn($jobRepositoryMock);
        
        $this->assertEquals(true, $this->jobHandler->controleExistanceJob('1'));
        $this->assertEquals(false, $this->jobHandler->controleExistanceJob('2'));
        
    }
}
