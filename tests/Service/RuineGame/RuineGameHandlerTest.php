<?php

namespace App\Tests\Service\RuineGame;

use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\RuineGame\RuineGameHandler;
use App\Service\RuineGame\RuineMazeHandler;
use App\Service\UserHandler;
use App\Service\UserService;
use App\Service\Utils\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RuineGameHandlerTest extends TestCase
{
    private EntityManagerInterface $em;
    private TranslatorInterface    $translator;
    private GestHordesHandler      $gh;
    private UserHandler            $userHandler;
    private TranslateHandler       $translateHandler;
    private UserService            $userService;
    private LoggerInterface        $logger;
    private RuineMazeHandler       $ruineMazeHandler;
    private RuineGameHandler       $ruineGameHandler;
    private SerializerService      $serializerService;
    
    /**
     * @throws Exception
     */
    public final function setUp(): void
    {
        $this->em                = $this->createMock(EntityManagerInterface::class);
        $this->translator        = $this->createMock(TranslatorInterface::class);
        $this->gh                = $this->createMock(GestHordesHandler::class);
        $this->userHandler       = $this->createMock(UserHandler::class);
        $this->translateHandler  = $this->createMock(TranslateHandler::class);
        $this->logger            = $this->createMock(LoggerInterface::class);
        $this->serializerService = $this->createMock(SerializerService::class);
        $this->ruineMazeHandler  = $this->createMock(RuineMazeHandler::class);
        
        $this->ruineGameHandler = new RuineGameHandler($this->em, $this->logger, $this->translator, $this->translateHandler, $this->userHandler, $this->gh, $this->serializerService, $this->ruineMazeHandler);
    }
    
    public final function testGetNiveauDifficulteRuine(): void
    {
        $lvlDifficulte = $this->ruineGameHandler->getNiveauDifficulteRuine();
        $this->assertEquals(4, count($lvlDifficulte));
    }
}
