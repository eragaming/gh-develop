<?php

namespace App\Tests\Service\Ame;

use App\Entity\HistoriquePictos;
use App\Entity\PictoPrototype;
use App\Entity\Pictos;
use App\Entity\User;
use App\Repository\HistoriquePictosRepository;
use App\Repository\PictosRepository;
use App\Service\Ame\PictoHandler;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class PictoHandlerTest extends TestCase
{
    private EntityManagerInterface $em;
    private TranslatorInterface    $translator;
    private PictoHandler           $pictoHandler;
    
    public function setUp(): void
    {
        $this->em           = $this->createMock(EntityManagerInterface::class);
        $this->translator   = $this->createMock(TranslatorInterface::class);
        $this->pictoHandler = new PictoHandler($this->em, $this->translator);
    }
    
    public function testHistoPictoUserReturnsCorrectlyGroupedArray()
    {
        $user = new User();
        
        $pictoHistoRepositoryMock = $this->createMock(HistoriquePictosRepository::class);
        $pictoHistoRepositoryMock->expects($this->once())
                                 ->method('recupListHistoriqueVille')
                                 ->willReturn([
                                                  (new HistoriquePictos())->setMapId(1),
                                                  (new HistoriquePictos())->setMapId(1),
                                                  (new HistoriquePictos())->setMapId(2),
                                              ]);
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($pictoHistoRepositoryMock);
        
        $result = $this->pictoHandler->getHistoPictoUser($user);
        
        $this->assertCount(2, $result);
        $this->assertEquals(2, count($result[1]));
        $this->assertEquals(1, count($result[2]));
        
    }
    
    public function testListPictoGeneralReturnsCorrectlyTranslatedArray()
    {
        $pictoRepositoryMock = $this->createMock(PictosRepository::class);
        $pictoRepositoryMock->expects($this->once())
                            ->method('findAll')
                            ->willReturn([
                                             (new PictoPrototype())->setName('test1')->setDescription('desc-test1'),
                                             (new PictoPrototype())->setName('test2')->setDescription('desc-test2'),
                                         ]);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($pictoRepositoryMock);
        
        $this->translator->method('trans')->willReturnArgument(0);
        
        $result = $this->pictoHandler->getListPicto();
        $this->assertCount(2, $result);
        $this->assertEquals('test1', $result[0]->getName());
        $this->assertEquals('test2', $result[1]->getName());
        $this->assertEquals('desc-test1', $result[0]->getDescription());
        $this->assertEquals('desc-test2', $result[1]->getDescription());
        
    }
    
    public function testListPictoUserReturnsCorrectlyTranslatedArray()
    {
        
        $user = new User();
        
        $pictoRepositoryMock = $this->createMock(PictosRepository::class);
        $pictoRepositoryMock->expects($this->once())
                            ->method('recupPictosUser')
                            ->willReturn([
                                             (new Pictos())->setPicto((new PictoPrototype())->setName('test1')),
                                             (new Pictos())->setPicto((new PictoPrototype())->setName('test2')),
                                         ]);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($pictoRepositoryMock);
        
        $this->translator->method('trans')->willReturnArgument(0);
        
        $result = $this->pictoHandler->getListPictoUser($user);
        
        $this->assertCount(2, $result);
        $this->assertEquals('test1', $result[0]->getPicto()->getName());
        $this->assertEquals('test2', $result[1]->getPicto()->getName());
        
    }
    
    
}
