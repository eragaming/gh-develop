<?php

namespace App\Tests\Service\Villes;

use App\Entity\HistoriqueVille;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\VilleHistorique;
use App\Repository\HistoriqueVilleRepository;
use App\Repository\VilleRepository;
use App\Service\Villes\VilleHandler;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class VilleServiceTest extends TestCase
{
    
    private $em;
    private $translator;
    private $villeService;
    
    protected function setUp(): void
    {
        $this->em         = $this->createMock(EntityManagerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        
        $this->villeService = new VilleHandler($this->em, $this->translator);
    }
    
    public function testOldVillesHistosReturnsCorrectlySortedArray()
    {
        $user = new User();
        
        $villeHistoRepositoryMock = $this->createMock(HistoriqueVilleRepository::class);
        $villeHistoRepositoryMock->expects($this->once())
                                 ->method('findBy')
                                 ->willReturn([
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('native')->setMapId(5)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(1)->setPhase('alpha')->setMapId(1)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(1)->setPhase('beta')->setMapId(2)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(2)->setPhase('beta')->setMapId(2)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(3)->setPhase('import')->setMapId(3)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('beta')->setMapId(4)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('native')->setMapId(7)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('native')->setMapId(6)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('native')->setMapId(6)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('alpha')->setMapId(6)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('import')->setMapId(6)),
                                                  (new HistoriqueVille())->setVilleHisto((new VilleHistorique())->setSaison(4)->setPhase('beta')->setMapId(6)),
                                              ]);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($villeHistoRepositoryMock);
        
        
        $result = $this->villeService->getOldVillesHistos($user);
        
        $this->assertCount(12, $result);
        $this->assertEquals(7, $result[0]->getVilleHisto()->getMapid());
        $this->assertEquals(6, $result[1]->getVilleHisto()->getMapid());
        $this->assertEquals(6, $result[2]->getVilleHisto()->getMapid());
        $this->assertEquals(5, $result[3]->getVilleHisto()->getMapid());
        $this->assertEquals(4, $result[4]->getVilleHisto()->getSaison());
        $this->assertEquals('import', $result[4]->getVilleHisto()->getPhase());
        $this->assertEquals(4, $result[5]->getVilleHisto()->getSaison());
        $this->assertEquals('beta', $result[5]->getVilleHisto()->getPhase());
        $this->assertEquals(6, $result[5]->getVilleHisto()->getMapid());
        $this->assertEquals(4, $result[6]->getVilleHisto()->getSaison());
        $this->assertEquals(4, $result[6]->getVilleHisto()->getMapid());
        $this->assertEquals('beta', $result[6]->getVilleHisto()->getPhase());
        $this->assertEquals(4, $result[7]->getVilleHisto()->getSaison());
        $this->assertEquals('alpha', $result[7]->getVilleHisto()->getPhase());
        $this->assertEquals(6, $result[7]->getVilleHisto()->getMapid());
        $this->assertEquals(3, $result[8]->getVilleHisto()->getSaison());
        $this->assertEquals(2, $result[9]->getVilleHisto()->getSaison());
        $this->assertEquals(1, $result[10]->getVilleHisto()->getSaison());
        $this->assertEquals(1, $result[11]->getVilleHisto()->getSaison());
        $this->assertEquals('beta', $result[10]->getVilleHisto()->getPhase());
        $this->assertEquals('alpha', $result[11]->getVilleHisto()->getPhase());
    }
    
    public function testVilleStringReturnsCorrectArrayWhenUserHasMapId()
    {
        $user = (new User())->setMapId(1);
        
        $villeRepositoryMock = $this->createMock(VilleRepository::class);
        $villeRepositoryMock->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn((new Ville(1, 2))->setNom('Test Ville'));
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($villeRepositoryMock);
        
        $result = $this->villeService->getVilleString($user);
        
        $this->assertEquals(['nom' => 'Test Ville', 'mapId' => 1], $result);
    }
    
    public function testVilleStringReturnsCorrectArrayWhenUserHasMapIdButNoVilleFound()
    {
        $user = (new User())->setMapId(1);
        
        $villeRepositoryMock = $this->createMock(VilleRepository::class);
        $villeRepositoryMock->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn(null);
        
        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn($villeRepositoryMock);
        
        $result = $this->villeService->getVilleString($user);
        
        $this->assertEquals(['nom' => 'Inconnu', 'mapId' => 1], $result);
    }
    
    public function testVilleStringReturnsCorrectArrayWhenUserHasNoMapId()
    {
        $user = new User();
        
        $result = $this->villeService->getVilleString($user);
        
        $this->assertEquals(['nom' => null, 'mapId' => null], $result);
    }
}
