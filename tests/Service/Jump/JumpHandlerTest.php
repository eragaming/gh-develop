<?php

namespace App\Tests\Service\Jump;

use App\Entity\JobPrototype;
use App\Entity\StatutInscription;
use App\Repository\JobPrototypeRepository;
use App\Repository\StatutInscriptionRepository;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Jump\JumpHandler;
use App\Service\UserHandler;
use App\Service\Utils\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class JumpHandlerTest extends TestCase
{
    private EntityManagerInterface $em;
    private TranslatorInterface    $translator;
    private GestHordesHandler      $gh;
    private UserHandler            $userHandler;
    private TranslateHandler       $translateHandler;
    private LoggerInterface        $logger;
    private JumpHandler            $jumpHandler;
    private SerializerService      $serializerService;
    
    /**
     * @throws Exception
     */
    public final function setUp(): void
    {
        $this->em                = $this->createMock(EntityManagerInterface::class);
        $this->translator        = $this->createMock(TranslatorInterface::class);
        $this->gh                = $this->createMock(GestHordesHandler::class);
        $this->userHandler       = $this->createMock(UserHandler::class);
        $this->translateHandler  = $this->createMock(TranslateHandler::class);
        $this->logger            = $this->createMock(LoggerInterface::class);
        $this->serializerService = $this->createMock(SerializerService::class);
        
        $this->jumpHandler = new JumpHandler($this->em, $this->logger, $this->translator, $this->translateHandler, $this->userHandler, $this->gh, $this->serializerService);
    }
    
    /**
     * @throws Exception
     */
    public final function testRecuperationJobVille(): void
    {
        $job1 = new JobPrototype();
        $job1->setNom('job1')->setAlternatif('job1');
        
        $job2 = new JobPrototype();
        $job2->setNom('job2')->setAlternatif('job2');
        
        $job3 = new JobPrototype();
        $job3->setNom('job3')->setAlternatif('job3');
        
        // Create a mock for the JobPrototype repository
        $jobPrototypeRepository = $this->createMock(JobPrototypeRepository::class);
        $jobPrototypeRepository->method('findAllOrdo')->willReturn([$job1, $job2, $job3]);
        $jobPrototypeRepository->method('findAllExceptChaman')->willReturn([$job1, $job2]);
        
        $this->em->method('getRepository')->willReturn($jobPrototypeRepository);
        $this->translator->method('trans')->willReturnArgument(0);
        
        // Call the method and assert the expected results
        $this->assertEquals(count([$job1, $job2, $job3]), count($this->jumpHandler->recuperationJobVille(true)));
        $this->assertEquals([$job1, $job2, $job3], $this->jumpHandler->recuperationJobVille(true));
        $this->assertEquals(count([$job1, $job2]), count($this->jumpHandler->recuperationJobVille(false)));
        $this->assertEquals([$job1, $job2], $this->jumpHandler->recuperationJobVille(false));
    }
    
    public final function testRecuperationListStatut(): void
    {
        $status1 = new StatutInscription();
        $status1->setNom('status1');
        $status2 = new StatutInscription();
        $status2->setNom('status2');
        $listStatut = [$status1, $status2];
        // Create a mock for the JobPrototype repository
        $statutInscriptionRepository = $this->createMock(StatutInscriptionRepository::class);
        $statutInscriptionRepository->method('findAll')->willReturn($listStatut);
        
        $this->em->method('getRepository')->willReturn($statutInscriptionRepository);
        $this->translator->method('trans')->willReturnArgument(0);
        
        // Call the method and assert the expected results
        $this->assertEquals(count($listStatut), count($this->jumpHandler->recuperationListStatut()));
        $this->assertEquals($listStatut, $this->jumpHandler->recuperationListStatut());
    }
}
