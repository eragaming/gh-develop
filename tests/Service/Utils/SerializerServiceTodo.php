<?php

namespace App\Tests\Service\Utils;

use App\Factory\SerializerFactory;
use App\Service\Utils\SerializerService;

class SerializerServiceTodo
{
    private SerializerFactory $serializerFactory;
    private SerializerService $serializerService;
    
    public function testBidon()
    {
        $this->assertEquals(1, 1);
    }
    
    protected function setUp(): void
    {
        $this->serializerFactory = $this->createMock(SerializerFactory::class);
        $this->serializerService = new SerializerService($this->serializerFactory);
    }
    
    /*public function testCircularReferenceHandler()
    {
        $chantierPrototype = new ChantierPrototype();
        $itemPrototype     = new ItemPrototype();
        $batPrototype      = new BatPrototype();
        
        // Créer des références circulaires
        $chantierPrototype->setCatChantier($chantierPrototype);
        $itemPrototype->setListAssemblage((new ListAssemblage())->setItemPrincipal($itemPrototype));
        $batPrototype->addItem((new ItemBatiment())->setItem($itemPrototype));
        
        // Définir les IDs (ou les valeurs appropriées)
        $chantierPrototype->setIdChantier(1);
        $itemPrototype->setIdObjet(2);
        $batPrototype->setIdBat(3);
        
        // Sérialiser l'objet
        $serializedChantierData = $this->serializerService->serialize($chantierPrototype);
        $serializedItemData     = $this->serializerService->serialize($itemPrototype);
        $serializedBatimentData = $this->serializerService->serialize($batPrototype);
        
        // Vérifier le résultat
        $this->assertStringContainsString('"id_chantier":1', $serializedChantierData);
        $this->assertStringContainsString('"id_objet":2', $serializedItemData);
        $this->assertStringContainsString('"id_bat":3', $serializedBatimentData);
    }*/
    
    /*public function testDeserializationGroupsAreMergedCorrectly()
    {
        $objet = new ItemPrototype();
        $data  = '{}';
        
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock->method('deserialize')->willReturnCallback(function ($data, $type, $format, $context) use ($objet) {
            $this->assertContains('group1', $context['groups']);
            $this->assertContains('group2', $context['groups']);
            $this->assertEquals($objet, $context['object_to_populate']);
            return new ChantierPrototype();
        });
        
        $this->serializerFactory->expects($this->once())->method('create')->willReturn($serializerMock);
        
        $this->serializerService->deserialize($data, ChantierPrototype::class, 'json', $objet, ['group1'], ['groups' => ['group2']]);
    }
    
    public function testDeserializationReturnsCorrectlyDeserializedData()
    {
        $data = '{"id_chantier":1}';
        $this->serializer->method('deserialize')->willReturn(new ChantierPrototype());
        
        $result = $this->serializerService->deserialize($data, ChantierPrototype::class);
        
        $this->assertInstanceOf(ChantierPrototype::class, $result);
    }
    
    public function testSerializationGroupsAreMergedCorrectly()
    {
        $data = new ChantierPrototype();
        $this->serializer->method('serialize')->willReturnCallback(function ($data, $format, $context) {
            $this->assertContains('group1', $context['groups']);
            $this->assertContains('group2', $context['groups']);
            return '{}';
        });
        
        $this->serializerService->serialize($data, 'json', ['group1'], ['groups' => ['group2']]);
    }
    
    public function testSerializationReturnsCorrectlySerializedData()
    {
        $data = new ChantierPrototype();
        $data->setIdChantier(1);
        $this->serializer->method('serialize')->willReturn('{"id_chantier":1}');
        
        $result = $this->serializerService->serialize($data);
        
        $this->assertEquals('{"id_chantier":1}', $result);
    }
    
    public function testSerializeArrayReturnsCorrectlySerializedArray()
    {
        $data = [new ChantierPrototype(), new ItemPrototype(), new BatPrototype()];
        $this->serializer->method('serialize')->willReturn('[[],[],[]]');
        
        $result = $this->serializerService->serializeArray($data);
        
        $this->assertEquals([[], [], []], $result);
    }
    
    /*public function testSerializerServiceDeserializesDataCorrectly()
    {
        $data       = '{"id":1}';
        $type       = 'json';
        $object     = new stdClass();
        $object->id = 1;
        $groups     = ['group1', 'group2'];
        $context    = ['context1', 'context2'];
        
        $serializerMock = $this->createMock(SerializerInterface::class);
        $serializerMock->method('deserialize')->willReturn($object);
        
        $this->serializerFactory->expects($this->once())->method('create')->willReturn($serializerMock);
        
        $result = $this->serializerService->deserialize($data, $type, 'json', $object, $groups, $context);
        
        $this->assertEquals($object, $result);
    }*/
}
