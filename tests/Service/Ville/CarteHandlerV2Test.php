<?php

namespace App\Tests\Service\Ville;

use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\UserService;
use App\Service\Utils\SerializerService;
use App\Service\Ville\CarteHandlerV2;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CarteHandlerV2Test extends TestCase
{
    private EntityManagerInterface $em;
    private TranslatorInterface    $translator;
    private GestHordesHandler      $gh;
    private UserHandler            $userHandler;
    private TranslateHandler       $translateHandler;
    private LoggerInterface        $logger;
    private CarteHandlerV2         $carteHandlerV2;
    private SerializerService      $serializerService;
    
    public final function testCalculBordureKmCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(1);
        $zoneMap1->setYRel(2);
        
        $zoneMap2 = new ZoneMap(0, 0, 1);
        $zoneMap2->setXRel(0);
        $zoneMap2->setYRel(2);
        
        $zoneMap3 = new ZoneMap(0, 0, 1);
        $zoneMap3->setXRel(-11);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(0);
        $zoneMap4->setYRel(1);
        
        $this->carteHandlerV2->calculKmCase($zoneMap1);
        $this->carteHandlerV2->calculKmCase($zoneMap2);
        $this->carteHandlerV2->calculKmCase($zoneMap3);
        $this->carteHandlerV2->calculKmCase($zoneMap4);
        
        $this->carteHandlerV2->calculBordureCaseKm($zoneMap1, 3, 1, 3, 2);
        $this->carteHandlerV2->calculBordureCaseKm($zoneMap2, 3, 1, 2, 2);
        $this->carteHandlerV2->calculBordureCaseKm($zoneMap3, null, 16, 16, null);
        $this->carteHandlerV2->calculBordureCaseKm($zoneMap4, 2, 0, 1, 1);
        
        
        $this->assertEquals(ZoneMap::Bordure_ne, $zoneMap1->getBordKm());
        $this->assertEquals(ZoneMap::Bordure_n, $zoneMap2->getBordKm());
        $this->assertEquals(ZoneMap::Bordure_no, $zoneMap3->getBordKm());
        $this->assertEquals(ZoneMap::Bordure_n, $zoneMap4->getBordKm());
    }
    
    public final function testCalculBordurePaCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(1);
        $zoneMap1->setYRel(2);
        
        $zoneMap2 = new ZoneMap(0, 0, 1);
        $zoneMap2->setXRel(0);
        $zoneMap2->setYRel(2);
        
        $zoneMap3 = new ZoneMap(0, 0, 1);
        $zoneMap3->setXRel(-11);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(0);
        $zoneMap4->setYRel(1);
        
        $this->carteHandlerV2->calculPaCase($zoneMap1);
        $this->carteHandlerV2->calculPaCase($zoneMap2);
        $this->carteHandlerV2->calculPaCase($zoneMap3);
        $this->carteHandlerV2->calculPaCase($zoneMap4);
        
        $this->carteHandlerV2->calculBordureCasePa($zoneMap1, 4, 2, 4, 2);
        $this->carteHandlerV2->calculBordureCasePa($zoneMap2, 3, 1, 3, 3);
        $this->carteHandlerV2->calculBordureCasePa($zoneMap3, null, 22, 22, null);
        $this->carteHandlerV2->calculBordureCasePa($zoneMap4, 2, 0, 2, 2);
        
        
        $this->assertEquals(ZoneMap::Bordure_ne, $zoneMap1->getBordPa());
        $this->assertEquals(ZoneMap::Bordure_neo, $zoneMap2->getBordPa());
        $this->assertEquals(ZoneMap::Bordure_no, $zoneMap3->getBordPa());
        $this->assertEquals(ZoneMap::Bordure_neo, $zoneMap4->getBordPa());
    }
    
    public final function testCalculBordureScrutateurCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(3);
        $zoneMap1->setYRel(6);
        
        $zoneMapN1 = new ZoneMap(0, 0, 1);
        $zoneMapN1->setXRel(3);
        $zoneMapN1->setYRel(7);
        
        $zoneMapE1 = new ZoneMap(0, 0, 1);
        $zoneMapE1->setXRel(4);
        $zoneMapE1->setYRel(6);
        
        $zoneMapS1 = new ZoneMap(0, 0, 1);
        $zoneMapS1->setXRel(3);
        $zoneMapS1->setYRel(5);
        
        $zoneMapO1 = new ZoneMap(0, 0, 1);
        $zoneMapO1->setXRel(2);
        $zoneMapO1->setYRel(6);
        
        $zoneMapNE1 = new ZoneMap(0, 0, 1);
        $zoneMapNE1->setXRel(4);
        $zoneMapNE1->setYRel(7);
        
        $zoneMapSE1 = new ZoneMap(0, 0, 1);
        $zoneMapSE1->setXRel(4);
        $zoneMapSE1->setYRel(5);
        
        $zoneMapSO1 = new ZoneMap(0, 0, 1);
        $zoneMapSO1->setXRel(2);
        $zoneMapSO1->setYRel(5);
        
        $zoneMapNO1 = new ZoneMap(0, 0, 1);
        $zoneMapNO1->setXRel(4);
        $zoneMapNO1->setYRel(5);
        
        
        $zoneMap2 = new ZoneMap(0, 0, 2);
        $zoneMap2->setXRel(2);
        $zoneMap2->setYRel(6);
        
        $zoneMapN2 = new ZoneMap(0, 0, 2);
        $zoneMapN2->setXRel(2);
        $zoneMapN2->setYRel(7);
        
        $zoneMapE2 = new ZoneMap(0, 0, 2);
        $zoneMapE2->setXRel(3);
        $zoneMapE2->setYRel(6);
        
        $zoneMapS2 = new ZoneMap(0, 0, 2);
        $zoneMapS2->setXRel(2);
        $zoneMapS2->setYRel(5);
        
        $zoneMapO2 = new ZoneMap(0, 0, 2);
        $zoneMapO2->setXRel(1);
        $zoneMapO2->setYRel(6);
        
        $zoneMapNE2 = new ZoneMap(0, 0, 2);
        $zoneMapNE2->setXRel(3);
        $zoneMapNE2->setYRel(7);
        
        $zoneMapSE2 = new ZoneMap(0, 0, 2);
        $zoneMapSE2->setXRel(3);
        $zoneMapSE2->setYRel(5);
        
        $zoneMapSO2 = new ZoneMap(0, 0, 2);
        $zoneMapSO2->setXRel(1);
        $zoneMapSO2->setYRel(5);
        
        $zoneMapNO2 = new ZoneMap(0, 0, 2);
        $zoneMapNO2->setXRel(1);
        $zoneMapNO2->setYRel(5);
        
        
        $zoneMap3 = new ZoneMap(0, 0, 3);
        $zoneMap3->setXRel(0);
        $zoneMap3->setYRel(1);
        
        $zoneMapN3 = new ZoneMap(0, 0, 3);
        $zoneMapN3->setXRel(0);
        $zoneMapN3->setYRel(2);
        
        $zoneMapE3 = new ZoneMap(0, 0, 2);
        $zoneMapE3->setXRel(1);
        $zoneMapE3->setYRel(1);
        
        $zoneMapS3 = new ZoneMap(0, 0, 2);
        $zoneMapS3->setXRel(0);
        $zoneMapS3->setYRel(0);
        
        $zoneMapO3 = new ZoneMap(0, 0, 2);
        $zoneMapO3->setXRel(-1);
        $zoneMapO3->setYRel(1);
        
        $zoneMapNE3 = new ZoneMap(0, 0, 2);
        $zoneMapNE3->setXRel(1);
        $zoneMapNE3->setYRel(2);
        
        $zoneMapSE3 = new ZoneMap(0, 0, 2);
        $zoneMapSE3->setXRel(1);
        $zoneMapSE3->setYRel(0);
        
        $zoneMapSO3 = new ZoneMap(0, 0, 2);
        $zoneMapSO3->setXRel(-1);
        $zoneMapSO3->setYRel(0);
        
        $zoneMapNO3 = new ZoneMap(0, 0, 2);
        $zoneMapNO3->setXRel(-1);
        $zoneMapNO3->setYRel(2);
        
        $zoneMap4 = new ZoneMap(0, 0, 4);
        $zoneMap4->setXRel(-5);
        $zoneMap4->setYRel(12);
        
        $zoneMapE4 = new ZoneMap(0, 0, 2);
        $zoneMapE4->setXRel(-4);
        $zoneMapE4->setYRel(12);
        
        $zoneMapS4 = new ZoneMap(0, 0, 2);
        $zoneMapS4->setXRel(-5);
        $zoneMapS4->setYRel(11);
        
        $zoneMapO4 = new ZoneMap(0, 0, 2);
        $zoneMapO4->setXRel(-6);
        $zoneMapO4->setYRel(12);
        
        $zoneMapSE4 = new ZoneMap(0, 0, 2);
        $zoneMapSE4->setXRel(-4);
        $zoneMapSE4->setYRel(11);
        
        $zoneMapSO4 = new ZoneMap(0, 0, 2);
        $zoneMapSO4->setXRel(-6);
        $zoneMapSO4->setYRel(11);
        
        
        $this->carteHandlerV2->calculDirectionCase($zoneMap1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapN1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapE1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapS1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapO1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNE1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSE1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSO1);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNO1);
        
        $this->carteHandlerV2->calculDirectionCase($zoneMap2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapN2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapE2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapS2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapO2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNE2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSE2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSO2);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNO2);
        
        $this->carteHandlerV2->calculDirectionCase($zoneMap3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapN3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapE3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapS3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapO3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNE3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSE3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSO3);
        $this->carteHandlerV2->calculDirectionCase($zoneMapNO3);
        
        $this->carteHandlerV2->calculDirectionCase($zoneMap4);
        $this->carteHandlerV2->calculDirectionCase($zoneMapE4);
        $this->carteHandlerV2->calculDirectionCase($zoneMapS4);
        $this->carteHandlerV2->calculDirectionCase($zoneMapO4);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSE4);
        $this->carteHandlerV2->calculDirectionCase($zoneMapSO4);
        
        $this->carteHandlerV2->calculBordureCaseScrutateur($zoneMap1, $zoneMapN1, $zoneMapE1, $zoneMapS1, $zoneMapO1, $zoneMapNE1, $zoneMapNO1, $zoneMapSE1, $zoneMapSO1);
        $this->carteHandlerV2->calculBordureCaseScrutateur($zoneMap2, $zoneMapN2, $zoneMapE2, $zoneMapS2, $zoneMapO2, $zoneMapNE2, $zoneMapNO2, $zoneMapSE2, $zoneMapSO2);
        $this->carteHandlerV2->calculBordureCaseScrutateur($zoneMap3, $zoneMapN3, $zoneMapE3, $zoneMapS3, $zoneMapO3, $zoneMapNE3, $zoneMapNO3, $zoneMapSE3, $zoneMapSO3);
        $this->carteHandlerV2->calculBordureCaseScrutateur($zoneMap4, null, $zoneMapE4, $zoneMapS4, $zoneMapO4, null, null, $zoneMapSE4, $zoneMapSO4);
        
        
        $this->assertEquals(ZoneMap::Bordure_se, $zoneMap1->getBordScrut());
        $this->assertEquals(ZoneMap::Bordure_cse, $zoneMap2->getBordScrut());
        $this->assertEquals(ZoneMap::Bordure_eo, $zoneMap3->getBordScrut());
        $this->assertEquals(ZoneMap::Bordure_cso, $zoneMap4->getBordScrut());
    }
    
    public final function testCalculBordureZoneCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(1);
        $zoneMap1->setYRel(2);
        
        $zoneMap2 = new ZoneMap(0, 0, 1);
        $zoneMap2->setXRel(0);
        $zoneMap2->setYRel(2);
        
        $zoneMap3 = new ZoneMap(0, 0, 1);
        $zoneMap3->setXRel(-2);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(0);
        $zoneMap4->setYRel(1);
        
        $this->carteHandlerV2->calculPaCase($zoneMap1);
        $this->carteHandlerV2->calculPaCase($zoneMap2);
        $this->carteHandlerV2->calculPaCase($zoneMap3);
        $this->carteHandlerV2->calculPaCase($zoneMap4);
        
        $this->carteHandlerV2->calculZonageCase($zoneMap1);
        $this->carteHandlerV2->calculZonageCase($zoneMap2);
        $this->carteHandlerV2->calculZonageCase($zoneMap3);
        $this->carteHandlerV2->calculZonageCase($zoneMap4);
        
        $this->carteHandlerV2->calculBordureCaseZone($zoneMap1, 5, 3, 5, 3);
        $this->carteHandlerV2->calculBordureCaseZone($zoneMap2, 5, 3, 3, 3);
        $this->carteHandlerV2->calculBordureCaseZone($zoneMap3, null, 23, 23, 27);
        $this->carteHandlerV2->calculBordureCaseZone($zoneMap4, 3, 0, 3, 3);
        
        
        $this->assertEquals(ZoneMap::Bordure_ne, $zoneMap1->getBordZonage());
        $this->assertEquals(ZoneMap::Bordure_n, $zoneMap2->getBordZonage());
        $this->assertEquals(ZoneMap::Bordure_no, $zoneMap3->getBordZonage());
        $this->assertEquals(null, $zoneMap4->getBordZonage());
    }
    
    public final function testCalculCoorRelatifNegativeCoordinates(): void
    {
        $zoneMap = new ZoneMap(0, 5, 1);
        
        $zoneMap->createId(0, 5, 1)
                ->setX(0)
                ->setY(5);
        
        $ville = new Ville(1, 2);
        $ville->setPosX(5);
        $ville->setPosY(3);
        
        $this->carteHandlerV2->calculCoorRelatif($zoneMap, $ville);
        
        $this->carteHandlerV2->calculCoorRelatif($zoneMap, $ville);
        
        $this->assertEquals(-5, $zoneMap->getXRel());
        $this->assertEquals(-2, $zoneMap->getYRel());
    }
    
    public final function testCalculCoorRelatifPositiveCoordinates(): void
    {
        $zoneMap = new ZoneMap(10, 0, 1);
        
        $zoneMap->createId(10, 0, 1)
                ->setX(10)
                ->setY(0);
        
        $ville = new Ville(1, 2);
        $ville->setPosX(5);
        $ville->setPosY(3);
        
        $this->carteHandlerV2->calculCoorRelatif($zoneMap, $ville);
        
        $this->assertEquals(5, $zoneMap->getXRel());
        $this->assertEquals(3, $zoneMap->getYRel());
    }
    
    public final function testCalculDirectionCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(-7);
        $zoneMap1->setYRel(1);
        
        $zoneMap2 = new ZoneMap(1, 0, 1);
        $zoneMap2->setXRel(0);
        $zoneMap2->setYRel(5);
        
        $zoneMap3 = new ZoneMap(1, 0, 1);
        $zoneMap3->setXRel(5);
        $zoneMap3->setYRel(4);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(7);
        $zoneMap4->setYRel(-3);
        
        $zoneMap5 = new ZoneMap(0, 0, 1);
        $zoneMap5->setXRel(-8);
        $zoneMap5->setYRel(6);
        
        $zoneMap6 = new ZoneMap(0, 0, 1);
        $zoneMap6->setXRel(0);
        $zoneMap6->setYRel(-1);
        
        $zoneMap7 = new ZoneMap(0, 0, 1);
        $zoneMap7->setXRel(1);
        $zoneMap7->setYRel(-1);
        
        $zoneMap8 = new ZoneMap(0, 0, 1);
        $zoneMap8->setXRel(-5);
        $zoneMap8->setYRel(-4);
        
        $zoneMap9 = new ZoneMap(0, 0, 1);
        $zoneMap9->setXRel(0);
        $zoneMap9->setYRel(0);
        
        
        $this->carteHandlerV2->calculDirectionCase($zoneMap1);
        $this->carteHandlerV2->calculDirectionCase($zoneMap2);
        $this->carteHandlerV2->calculDirectionCase($zoneMap3);
        $this->carteHandlerV2->calculDirectionCase($zoneMap4);
        $this->carteHandlerV2->calculDirectionCase($zoneMap5);
        $this->carteHandlerV2->calculDirectionCase($zoneMap6);
        $this->carteHandlerV2->calculDirectionCase($zoneMap7);
        $this->carteHandlerV2->calculDirectionCase($zoneMap8);
        $this->carteHandlerV2->calculDirectionCase($zoneMap9);
        
        $this->assertEquals(ZoneMap::DIRECTION_OUEST, $zoneMap1->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_NORD, $zoneMap2->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_NORD_EST, $zoneMap3->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_EST, $zoneMap4->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_NORD_OUEST, $zoneMap5->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_SUD, $zoneMap6->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_SUD_EST, $zoneMap7->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_SUD_OUEST, $zoneMap8->getDirection());
        $this->assertEquals(ZoneMap::DIRECTION_VILLE, $zoneMap9->getDirection());
    }
    
    public final function testCalculKmCaseMaxKm(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(3);
        $zoneMap1->setYRel(4);
        
        $zoneMap2 = new ZoneMap(1, 0, 1);
        $zoneMap2->setXRel(5);
        $zoneMap2->setYRel(12);
        
        $kmMax = 0;
        
        $this->carteHandlerV2->calculKmCase($zoneMap1, $kmMax);
        $this->carteHandlerV2->calculKmCase($zoneMap2, $kmMax);
        
        $this->assertEquals(5, $zoneMap1->getKm());
        $this->assertEquals(13, $zoneMap2->getKm());
        $this->assertEquals(13, $kmMax);
    }
    
    public final function testCalculKmCaseWithNegativeCoordinates(): void
    {
        $zoneMap = new ZoneMap(0, 0, 1);
        $zoneMap->setXRel(-3);
        $zoneMap->setYRel(-4);
        
        $kmMax = 0;
        
        $this->carteHandlerV2->calculKmCase($zoneMap, $kmMax);
        
        $this->assertEquals(5, $zoneMap->getKm());
        $this->assertEquals(5, $kmMax);
    }
    
    public final function testCalculKmCaseWithPositiveCoordinates(): void
    {
        $zoneMap = new ZoneMap(0, 0, 1);
        $zoneMap->setXRel(3);
        $zoneMap->setYRel(4);
        
        $kmMax = 0;
        
        $this->carteHandlerV2->calculKmCase($zoneMap, $kmMax);
        
        $this->assertEquals(5, $zoneMap->getKm());
        $this->assertEquals(5, $kmMax);
    }
    
    public final function testCalculPaARCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(3);
        $zoneMap1->setYRel(4);
        
        $zoneMap2 = new ZoneMap(1, 0, 1);
        $zoneMap2->setXRel(5);
        $zoneMap2->setYRel(12);
        
        $zoneMap3 = new ZoneMap(1, 0, 1);
        $zoneMap3->setXRel(-5);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(-3);
        $zoneMap4->setYRel(-4);
        
        $zoneMap5 = new ZoneMap(0, 0, 1);
        $zoneMap5->setXRel(0);
        $zoneMap5->setYRel(-4);
        
        $zoneMap6 = new ZoneMap(0, 0, 1);
        $zoneMap6->setXRel(0);
        $zoneMap6->setYRel(-1);
        
        $zoneMap7 = new ZoneMap(0, 0, 1);
        $zoneMap7->setXRel(2);
        $zoneMap7->setYRel(0);
        
        $paMax = 0;
        
        $this->assertEquals(11, $this->carteHandlerV2->calculPaARCase($zoneMap1->getXRel(), $zoneMap1->getYRel()));
        $this->assertEquals(31, $this->carteHandlerV2->calculPaARCase($zoneMap2->getXRel(), $zoneMap2->getYRel()));
        $this->assertEquals(31, $this->carteHandlerV2->calculPaARCase($zoneMap3->getXRel(), $zoneMap3->getYRel()));
        $this->assertEquals(11, $this->carteHandlerV2->calculPaARCase($zoneMap4->getXRel(), $zoneMap4->getYRel()));
        $this->assertEquals(7, $this->carteHandlerV2->calculPaARCase($zoneMap5->getXRel(), $zoneMap5->getYRel()));
        $this->assertEquals(1, $this->carteHandlerV2->calculPaARCase($zoneMap6->getXRel(), $zoneMap6->getYRel()));
        $this->assertEquals(3, $this->carteHandlerV2->calculPaARCase($zoneMap7->getXRel(), $zoneMap7->getYRel()));
    }
    
    public final function testCalculPaCaseMaxPa(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(3);
        $zoneMap1->setYRel(4);
        
        $zoneMap2 = new ZoneMap(1, 0, 1);
        $zoneMap2->setXRel(5);
        $zoneMap2->setYRel(12);
        
        $zoneMap3 = new ZoneMap(1, 0, 1);
        $zoneMap3->setXRel(-5);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(-3);
        $zoneMap4->setYRel(-4);
        
        $paMax = 0;
        
        $this->carteHandlerV2->calculPaCase($zoneMap1, $paMax);
        $this->carteHandlerV2->calculPaCase($zoneMap2, $paMax);
        $this->carteHandlerV2->calculPaCase($zoneMap3, $paMax);
        $this->carteHandlerV2->calculPaCase($zoneMap4, $paMax);
        
        $this->assertEquals(7, $zoneMap1->getPa());
        $this->assertEquals(17, $zoneMap2->getPa());
        $this->assertEquals(17, $zoneMap3->getPa());
        $this->assertEquals(7, $zoneMap4->getPa());
        $this->assertEquals(17, $paMax);
    }
    
    public final function testCalculZonageCase(): void
    {
        $zoneMap1 = new ZoneMap(0, 0, 1);
        $zoneMap1->setXRel(3);
        $zoneMap1->setYRel(4);
        
        $zoneMap2 = new ZoneMap(1, 0, 1);
        $zoneMap2->setXRel(5);
        $zoneMap2->setYRel(12);
        
        $zoneMap3 = new ZoneMap(1, 0, 1);
        $zoneMap3->setXRel(-5);
        $zoneMap3->setYRel(12);
        
        $zoneMap4 = new ZoneMap(0, 0, 1);
        $zoneMap4->setXRel(-3);
        $zoneMap4->setYRel(-4);
        
        $zoneMap5 = new ZoneMap(0, 0, 1);
        $zoneMap5->setXRel(0);
        $zoneMap5->setYRel(-4);
        
        $zoneMap6 = new ZoneMap(0, 0, 1);
        $zoneMap6->setXRel(0);
        $zoneMap6->setYRel(-1);
        
        $zoneMap7 = new ZoneMap(0, 0, 1);
        $zoneMap7->setXRel(2);
        $zoneMap7->setYRel(0);
        
        $this->carteHandlerV2->calculPaCase($zoneMap1);
        $this->carteHandlerV2->calculPaCase($zoneMap2);
        $this->carteHandlerV2->calculPaCase($zoneMap3);
        $this->carteHandlerV2->calculPaCase($zoneMap4);
        $this->carteHandlerV2->calculPaCase($zoneMap5);
        $this->carteHandlerV2->calculPaCase($zoneMap6);
        $this->carteHandlerV2->calculPaCase($zoneMap7);
        $this->carteHandlerV2->calculZonageCase($zoneMap1);
        $this->carteHandlerV2->calculZonageCase($zoneMap2);
        $this->carteHandlerV2->calculZonageCase($zoneMap3);
        $this->carteHandlerV2->calculZonageCase($zoneMap4);
        $this->carteHandlerV2->calculZonageCase($zoneMap5);
        $this->carteHandlerV2->calculZonageCase($zoneMap6);
        $this->carteHandlerV2->calculZonageCase($zoneMap7);
        
        $this->assertEquals(11, $zoneMap1->getZone());
        $this->assertEquals(99, $zoneMap2->getZone());
        $this->assertEquals(99, $zoneMap3->getZone());
        $this->assertEquals(11, $zoneMap4->getZone());
        $this->assertEquals(7, $zoneMap5->getZone());
        $this->assertEquals(3, $zoneMap6->getZone());
        $this->assertEquals(3, $zoneMap7->getZone());
    }
    
    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->em                = $this->createMock(EntityManagerInterface::class);
        $this->translator        = $this->createMock(TranslatorInterface::class);
        $this->gh                = $this->createMock(GestHordesHandler::class);
        $this->userHandler       = $this->createMock(UserHandler::class);
        $this->translateHandler  = $this->createMock(TranslateHandler::class);
        $this->serializerService = $this->createMock(SerializerService::class);
        $this->logger            = $this->createMock(LoggerInterface::class);
        $this->carteHandlerV2    = new CarteHandlerV2($this->em, $this->logger, $this->translator, $this->translateHandler, $this->userHandler, $this->gh, $this->serializerService);
    }
    
    
}