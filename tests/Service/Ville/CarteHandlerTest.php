<?php

namespace App\Tests\Service\Ville;

use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Service\Ville\CarteHandler;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class CarteHandlerTest extends TestCase
{
    private EntityManagerInterface $em;
    private TranslatorInterface    $translator;
    private TranslateHandler       $translateHandler;
    private GestHordesHandler      $gh;
    private UpGradeHandler         $upGrade;
    private ExpeditionHandler      $expedition;
    private UserHandler            $userHandler;
    private ItemsHandler           $itemsHandler;
    private LocalFormatterUtils    $lfu;
    private CarteHandler           $carteHandler;
    private SerializerService      $serializerService;
    
    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->em                = $this->createMock(EntityManagerInterface::class);
        $this->translator        = $this->createMock(TranslatorInterface::class);
        $this->translateHandler  = $this->createMock(TranslateHandler::class);
        $this->gh                = $this->createMock(GestHordesHandler::class);
        $this->upGrade           = $this->createMock(UpGradeHandler::class);
        $this->expedition        = $this->createMock(ExpeditionHandler::class);
        $this->userHandler       = $this->createMock(UserHandler::class);
        $this->itemsHandler      = $this->createMock(ItemsHandler::class);
        $this->lfu               = $this->createMock(LocalFormatterUtils::class);
        $this->serializerService = $this->createMock(SerializerService::class);
        $this->carteHandler      = new CarteHandler($this->em, $this->translator, $this->translateHandler, $this->gh, $this->upGrade, $this->expedition, $this->userHandler, $this->lfu, $this->serializerService);
    }
    
    public function testGetZoneMaxCaseOK()
    {
        $listZonage = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27];
        
        $this->assertEquals(3, $this->carteHandler->getZoneMaxCase(1, $listZonage, 99));
        $this->assertEquals(99, $this->carteHandler->getZoneMaxCase(30, $listZonage, 99));
        $this->assertEquals(9, $this->carteHandler->getZoneMaxCase(8, $listZonage, 99));
        $this->assertEquals(9, $this->carteHandler->getZoneMaxCase(9, $listZonage, 99));
    }
    
}
