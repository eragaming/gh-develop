<?php

namespace App\Enum;

enum LevelSkill: int
{
    case  HABITANT = 0;
    case  DEBUTANT = 1;
    case  APPRENTI = 2;
    case  EXPERT   = 3;
    case  ELITE    = 4;
}
