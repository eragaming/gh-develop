<?php

namespace App\Enum;


enum MenuType: string
{
    case TYPE_G = 'group';
    case TYPE_M = 'menu';
}