<?php

namespace App\Enum;


enum MotifEpuisement: string
{
    case Hachure_gauche  = 'hac_dg_1';
    case Hachure_gauche3 = 'hac_dg_3';
    case Hachure_gauche2 = 'hac_dg_2';
    case Hachure_droite  = 'hac_gd_1';
    case Hachure_droite3 = 'hac_gd_3';
    case Hachure_droite2 = 'hac_gd_2';
}