<?php

namespace App\Enum;

use App\Entity\Ville;

enum EtatVille: string
{
    case  NORMAL = "Normal";
    case  CHAOS  = "Chaos";
    case  DEVAST = "Dévastée";
    
    public static function getEtatFromVille(Ville $ville): self
    {
        return match (true) {
            $ville->isChaos() && $ville->isDevast()  => self::DEVAST,
            $ville->isChaos() && !$ville->isDevast() => self::CHAOS,
            default                                  => self::NORMAL
        };
    }
    
}
