<?php

namespace App\Entity;

use App\Repository\HistoriqueVilleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;

#[ORM\Entity(repositoryClass: HistoriqueVilleRepository::class)]
class HistoriqueVille
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['picto'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private ?User $user = null;
    
    #[ORM\ManyToOne]
    #[Groups(['picto'])]
    private ?Ville $ville = null;
    
    #[ORM\ManyToOne(targetEntity: VilleHistorique::class, inversedBy: 'historiqueByCitizens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['picto'])]
    private ?VilleHistorique $villeHisto = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $score = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'id_mort', referencedColumnName: 'id_mort', nullable: false)]
    #[Groups(['picto'])]
    private ?TypeDeath $typeMort = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['picto'])]
    private ?string $msg = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['picto'])]
    private ?string $clean_name = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[Groups(['picto'])]
    private ?CleanUpCadaver $clean_type_histo = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['picto'])]
    private ?int $dayOfDeath = null;
    
    public function getCleanName(): ?string
    {
        return $this->clean_name;
    }
    
    public function setCleanName(?string $clean_name): self
    {
        $this->clean_name = $clean_name;
        
        return $this;
    }
    
    public function getCleanTypeHisto(): ?CleanUpCadaver
    {
        return $this->clean_type_histo;
    }
    
    public function setCleanTypeHisto(?CleanUpCadaver $clean_type): self
    {
        $this->clean_type_histo = $clean_type;
        
        return $this;
    }
    
    public function getDayOfDeath(): ?int
    {
        return $this->dayOfDeath;
    }
    
    public function setDayOfDeath(?int $dayOfDeath): self
    {
        $this->dayOfDeath = $dayOfDeath;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getMsg(): ?string
    {
        return $this->msg;
    }
    
    public function setMsg(?string $msg): self
    {
        $this->msg = $msg;
        
        return $this;
    }
    
    public function getScore(): ?int
    {
        return $this->score;
    }
    
    public function setScore(int $score): self
    {
        $this->score = $score;
        
        return $this;
    }
    
    public function getTypeMort(): ?TypeDeath
    {
        return $this->typeMort;
    }
    
    public function setTypeMort(?TypeDeath $typeMort): self
    {
        $this->typeMort = $typeMort;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getVilleHisto(): ?VilleHistorique
    {
        return $this->villeHisto;
    }
    
    public function setVilleHisto(?VilleHistorique $villeHisto): self
    {
        $this->villeHisto = $villeHisto;
        
        return $this;
    }
}
