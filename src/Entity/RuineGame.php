<?php

namespace App\Entity;

use App\Doctrine\IdAlphaRuineGameGenerator;
use App\Repository\RuineGameRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RuineGameRepository::class)]
class RuineGame
{
    #[ORM\Id, ORM\GeneratedValue(strategy: 'CUSTOM'), ORM\CustomIdGenerator(class: IdAlphaRuineGameGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?string $id = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $nbrEtage = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $qteOxygene = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $typePlan = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $nbrZombie = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $saison = null;
    
    /** @var Collection<RuineGameZone> */
    #[ORM\OneToMany(mappedBy: 'ruineGame', targetEntity: RuineGameZone::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private Collection $ruineGameZones;
    
    #[ORM\ManyToOne(inversedBy: 'ruineGames')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?User $user = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $manaKill = null;
    
    #[ORM\Column(length: 7)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?string $typeRuine = 'bunker';
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $x = 7;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $y = 0;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $z = 0;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?bool $inPiece = false;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $manaInitial = null;
    
    #[ORM\Column]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?DateTimeImmutable $beginAt = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?DateTimeImmutable $endedAt = null;
    
    #[ORM\Column]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?bool $ejected = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $pctZombieKill = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $pctExploration = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $pctFouille = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $oxygeneRestant = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $manaRestant = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $oxygenLost = 0;
    
    public function __construct()
    {
        $this->ruineGameZones = new ArrayCollection();
    }
    
    public function addRuineGameZone(RuineGameZone $ruineGameZone): static
    {
        if (!$this->ruineGameZones->contains($ruineGameZone)) {
            $this->ruineGameZones->add($ruineGameZone);
            $ruineGameZone->setRuineGame($this);
        }
        
        return $this;
    }
    
    public function getBeginAt(): ?DateTimeImmutable
    {
        return $this->beginAt;
    }
    
    public function setBeginAt(?DateTimeImmutable $beginAt): RuineGame
    {
        $this->beginAt = $beginAt;
        return $this;
    }
    
    public function getEjected(): ?bool
    {
        return $this->ejected;
    }
    
    public function setEjected(?bool $ejected): RuineGame
    {
        $this->ejected = $ejected;
        return $this;
    }
    
    public function getEndedAt(): ?DateTimeImmutable
    {
        return $this->endedAt;
    }
    
    public function setEndedAt(?DateTimeImmutable $endedAt): RuineGame
    {
        $this->endedAt = $endedAt;
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): RuineGame
    {
        $this->id = $id;
        return $this;
    }
    
    public function getManaInitial(): ?int
    {
        return $this->manaInitial;
    }
    
    public function setManaInitial(int $manaIntial): static
    {
        $this->manaInitial = $manaIntial;
        
        return $this;
    }
    
    public function getManaKill(): ?int
    {
        return $this->manaKill;
    }
    
    public function setManaKill(?int $manaKill): static
    {
        $this->manaKill = $manaKill;
        
        return $this;
    }
    
    public function getManaRestant(): ?int
    {
        return $this->manaRestant;
    }
    
    public function setManaRestant(?int $manaRestant): RuineGame
    {
        $this->manaRestant = $manaRestant;
        return $this;
    }
    
    public function getNbrEtage(): ?int
    {
        return $this->nbrEtage;
    }
    
    public function setNbrEtage(int $nbrEtage): static
    {
        $this->nbrEtage = $nbrEtage;
        
        return $this;
    }
    
    public function getNbrZombie(): ?int
    {
        return $this->nbrZombie;
    }
    
    public function setNbrZombie(int $nbrZombie): static
    {
        $this->nbrZombie = $nbrZombie;
        
        return $this;
    }
    
    public function getOxygenLost(): ?int
    {
        return $this->oxygenLost;
    }
    
    public function setOxygenLost(int $oxygenLost): static
    {
        $this->oxygenLost = $oxygenLost;
        
        return $this;
    }
    
    public function getOxygeneRestant(): ?int
    {
        return $this->oxygeneRestant;
    }
    
    public function setOxygeneRestant(?int $oxygeneRestant): RuineGame
    {
        $this->oxygeneRestant = $oxygeneRestant;
        return $this;
    }
    
    public function getPctExploration(): ?int
    {
        return $this->pctExploration;
    }
    
    public function setPctExploration(?int $pctExploration): RuineGame
    {
        $this->pctExploration = $pctExploration;
        return $this;
    }
    
    public function getPctFouille(): ?int
    {
        return $this->pctFouille;
    }
    
    public function setPctFouille(?int $pctFouille): RuineGame
    {
        $this->pctFouille = $pctFouille;
        return $this;
    }
    
    public function getPctZombieKill(): ?int
    {
        return $this->pctZombieKill;
    }
    
    public function setPctZombieKill(?int $pctZombieKill): RuineGame
    {
        $this->pctZombieKill = $pctZombieKill;
        return $this;
    }
    
    public function getQteOxygene(): ?int
    {
        return $this->qteOxygene;
    }
    
    public function setQteOxygene(int $qteOxygene): static
    {
        $this->qteOxygene = $qteOxygene;
        
        return $this;
    }
    
    public function getRuineGameZones(): Collection
    {
        return $this->ruineGameZones;
    }
    
    public function setRuineGameZones(Collection $ruineGameZones): RuineGame
    {
        $this->ruineGameZones = $ruineGameZones;
        return $this;
    }
    
    public function getRuineZonesEtage(int $etage): ArrayCollection|PersistentCollection
    {
        return $this->getRuineGameZones()->matching((new Criteria())->andWhere(new Comparison('z', Comparison::EQ, $etage)));
    }
    
    public function getSaison(): ?int
    {
        return $this->saison;
    }
    
    public function setSaison(int $saison): static
    {
        $this->saison = $saison;
        
        return $this;
    }
    
    public function getTypePlan(): ?int
    {
        return $this->typePlan;
    }
    
    public function setTypePlan(int $typePlan): static
    {
        $this->typePlan = $typePlan;
        
        return $this;
    }
    
    public function getTypeRuine(): ?string
    {
        return $this->typeRuine;
    }
    
    public function setTypeRuine(string $typeRuine): static
    {
        $this->typeRuine = $typeRuine;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(?int $x): static
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(?int $y): static
    {
        $this->y = $y;
        
        return $this;
    }
    
    public function getZ(): ?int
    {
        return $this->z;
    }
    
    public function setZ(?int $z): static
    {
        $this->z = $z;
        
        return $this;
    }
    
    public function isInPiece(): ?bool
    {
        return $this->inPiece;
    }
    
    public function removeRuineGameZone(RuineGameZone $ruineGameZone): static
    {
        if ($this->ruineGameZones->removeElement($ruineGameZone)) {
            // set the owning side to null (unless already changed)
            if ($ruineGameZone->getRuineGame() === $this) {
                $ruineGameZone->setRuineGame(null);
            }
        }
        
        return $this;
    }
    
    public function setInPiece(?bool $inPiece): static
    {
        $this->inPiece = $inPiece;
        
        return $this;
    }
    
    
}
