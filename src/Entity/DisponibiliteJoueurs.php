<?php

namespace App\Entity;

use App\Repository\DisponibiliteJoueursRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DisponibiliteJoueursRepository::class)]
class DisponibiliteJoueurs
{
    public const  TYPE_SEMAINE  = 1;
    public const  TYPE_WEEK_END = 2;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?int $typeCreneau = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: CreneauHorraire::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?CreneauHorraire $creneau = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeDispo::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['inscription_jump', 'creation_ins', 'candidature_jump'])]
    private ?TypeDispo $dispo = null;
    
    #[ORM\ManyToOne(targetEntity: InscriptionJump::class, fetch: 'EXTRA_LAZY', inversedBy: 'dispo')]
    #[ORM\JoinColumn(nullable: false)]
    private ?InscriptionJump $inscriptionJump = null;
    
    public function getCreneau(): ?CreneauHorraire
    {
        return $this->creneau;
    }
    
    public function setCreneau(?CreneauHorraire $creneau): self
    {
        $this->creneau = $creneau;
        
        return $this;
    }
    
    public function getDispo(): ?TypeDispo
    {
        return $this->dispo;
    }
    
    public function setDispo(?TypeDispo $dispo): self
    {
        $this->dispo = $dispo;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): DisponibiliteJoueurs
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getInscriptionJump(): ?InscriptionJump
    {
        return $this->inscriptionJump;
    }
    
    public function setInscriptionJump(?InscriptionJump $inscriptionJump): self
    {
        $this->inscriptionJump = $inscriptionJump;
        
        return $this;
    }
    
    public function getTypeCreneau(): ?int
    {
        return $this->typeCreneau;
    }
    
    public function setTypeCreneau(int $typeCreneau): self
    {
        $this->typeCreneau = $typeCreneau;
        
        return $this;
    }
}
