<?php

namespace App\Entity;

use App\Repository\HerosPrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'heros_prototype')]
#[ORM\Entity(repositoryClass: HerosPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class HerosPrototype
{
    public const ID_DON_JH         = 9;
    public const ID_CORPS_SAIN     = 12;
    public const ID_TROUVAILLE_AME = 16;
    public const ID_SS             = 18;
    public const ID_APAG           = 20;
    public const ID_VLM            = 23;
    public const ID_ARCHI          = 28;
    public const ID_VP             = 29;
    #[ORM\Id, ORM\Column(name: 'id', type: 'smallint', nullable: false),
        Groups(['admin', 'citoyens', 'ency', 'option', 'carte_gen', 'coalition', 'gestion', 'gestion_jump', 'jump', 'outils_expe', 'inscription_exp_visu', 'admin_heros_detail'])]
    private int $id;
    
    #[ORM\Column(name: 'nom', type: 'string', length: 24, nullable: false),
        Groups(['admin', 'general', 'citoyens', 'ency', 'option', 'carte_gen', 'coalition', 'gestion', 'gestion_jump',
                'jump', 'outils_expe', 'inscription_exp_visu', 'admin_heros_detail'])]
    private string $nom;
    
    #[ORM\Column(name: 'icon', type: 'string', length: 24, nullable: false),
        Groups(['admin', 'general', 'citoyens', 'ency', 'option', 'carte_gen', 'coalition', 'gestion', 'gestion_jump',
                'jump', 'outils_expe', 'inscription_exp_visu', 'admin_heros_detail'])]
    private string $icon = '';
    
    #[ORM\Column(name: 'description', type: 'text', nullable: false), Groups(['admin', 'ency', 'jump'])]
    private string $description;
    
    #[ORM\Column(name: 'jour', type: 'smallint', nullable: false, options: ['unsigned' => true]),
        Groups(['admin', 'ency', 'option', 'jump'])]
    private int $jour;
    
    #[ORM\Column(name: 'jour_cumul', type: 'smallint', nullable: false, options: ['unsigned' => true]),
        Groups(['admin', 'ency', 'option', 'jump'])]
    private int $jourCumul;
    
    #[ORM\Column(name: 'pouv_base', type: 'boolean', nullable: false)]
    private bool $pouvBase;
    
    #[ORM\Column(name: 'pouv_actif', type: 'boolean', nullable: false), Groups(['admin', 'citoyens'])]
    private bool $pouvActif;
    
    #[ORM\Column(name: 'method', type: 'string', length: 24, nullable: true)]
    private ?string $method = null;
    
    #[ORM\Column(type: Types::SMALLINT),
        Groups(['admin', 'carte', 'general', 'citoyens', 'option', 'carte_gen', 'coalition', 'jump', 'outils_expe', 'inscription_exp_visu'])]
    private ?int $ordre_recup = null;
    
    /**
     * @var Collection<int, HerosSkillPrototype>
     */
    #[ORM\OneToMany(mappedBy: 'heros', targetEntity: HerosSkillPrototype::class)]
    private Collection $herosSkillPrototypes;
    
    public function __construct()
    {
        $this->herosSkillPrototypes = new ArrayCollection();
    }
    
    public function addHerosSkillPrototype(HerosSkillPrototype $herosSkillPrototype): static
    {
        if (!$this->herosSkillPrototypes->contains($herosSkillPrototype)) {
            $this->herosSkillPrototypes->add($herosSkillPrototype);
            $herosSkillPrototype->setHeros($this);
        }
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return Collection<int, HerosSkillPrototype>
     */
    public function getHerosSkillPrototypes(): Collection
    {
        return $this->herosSkillPrototypes;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): HerosPrototype
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    public function setJour(int $jour): self
    {
        $this->jour = $jour;
        
        return $this;
    }
    
    public function getJourCumul(): ?int
    {
        return $this->jourCumul;
    }
    
    public function setJourCumul(int $jourCumul): self
    {
        $this->jourCumul = $jourCumul;
        
        return $this;
    }
    
    public function getMethod(): ?string
    {
        return $this->method;
    }
    
    public function setMethod(?string $method): HerosPrototype
    {
        $this->method = $method;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getOrdreRecup(): ?int
    {
        return $this->ordre_recup;
    }
    
    public function setOrdreRecup(int $ordre_recup): self
    {
        $this->ordre_recup = $ordre_recup;
        
        return $this;
    }
    
    public function isPouvActif(): bool
    {
        return $this->pouvActif;
    }
    
    public function setPouvActif(bool $pouvActif): HerosPrototype
    {
        $this->pouvActif = $pouvActif;
        
        return $this;
    }
    
    public function isPouvBase(): bool
    {
        return $this->pouvBase;
    }
    
    public function setPouvBase(bool $pouvBase): HerosPrototype
    {
        $this->pouvBase = $pouvBase;
        
        return $this;
    }
    
    public function removeHerosSkillPrototype(HerosSkillPrototype $herosSkillPrototype): static
    {
        if ($this->herosSkillPrototypes->removeElement($herosSkillPrototype)) {
            // set the owning side to null (unless already changed)
            if ($herosSkillPrototype->getHeros() === $this) {
                $herosSkillPrototype->setHeros(null);
            }
        }
        
        return $this;
    }
    
}
