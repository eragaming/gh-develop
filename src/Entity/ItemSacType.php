<?php

namespace App\Entity;

use App\Repository\ItemSacTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ItemSacTypeRepository::class)]
class ItemSacType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column]
    private ?bool $broken = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $nbr = null;
    
    /** @var Collection<SacType> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'items', targetEntity: SacType::class)]
    private Collection $sacTypes;
    
    public function __construct()
    {
        $this->sacTypes = new ArrayCollection();
    }
    
    public function addSacType(SacType $sacType): static
    {
        if (!$this->sacTypes->contains($sacType)) {
            $this->sacTypes->add($sacType);
            $sacType->setItems($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): static
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNbr(): ?int
    {
        return $this->nbr;
    }
    
    public function setNbr(int $nbr): static
    {
        $this->nbr = $nbr;
        
        return $this;
    }
    
    public function getSacTypes(): Collection
    {
        return $this->sacTypes;
    }
    
    public function isBroken(): ?bool
    {
        return $this->broken;
    }
    
    public function removeSacType(SacType $sacType): static
    {
        if ($this->sacTypes->removeElement($sacType)) {
            // set the owning side to null (unless already changed)
            if ($sacType->getItems() === $this) {
                $sacType->setItems(null);
            }
        }
        
        return $this;
    }
    
    public function setBroken(bool $broken): static
    {
        $this->broken = $broken;
        
        return $this;
    }
}
