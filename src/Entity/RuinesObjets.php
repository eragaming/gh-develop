<?php

namespace App\Entity;

use App\Repository\RuinesObjetsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RuinesObjetsRepository::class)]
class RuinesObjets
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['ruine', 'admin_ruine'])]
    private ?int $id = null;
    
    #[ORM\Column, Groups(['ruine', 'admin_ruine'])]
    private ?bool $broken = null;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['ruine', 'admin_ruine'])]
    private ?int $nombre = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false),
        Groups(['ruine', 'admin_ruine'])]
    private ?ItemPrototype $item = null;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): static
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): static
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): static
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function isBroken(): ?bool
    {
        return $this->broken;
    }
    
    public function setBroken(bool $broken): static
    {
        $this->broken = $broken;
        
        return $this;
    }
}
