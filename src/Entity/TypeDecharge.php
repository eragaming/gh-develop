<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

/**
 * TypeDecharge
 */
#[ORM\Table(name: 'type_decharge')]
#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeDecharge
{
    #[ORM\ManyToOne(targetEntity: 'ChantierPrototype', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id')]
    #[Groups(['ency_objet', 'admin_ch', 'admin_obj', 'carte', 'outils_expe', 'ency_decharge'])]
    private ChantierPrototype $chantier;
    
    /**
     * @var Collection<int, ItemPrototype>
     */
    #[ORM\OneToMany(mappedBy: 'decharge', targetEntity: ItemPrototype::class)]
    private Collection $itemPrototypes;
    
    public function __construct(#[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
                                #[ORM\Id, Groups(['admin_ch', 'admin_obj', 'carte', 'outils_expe', 'ency_decharge'])]
                                private int $id)
    {
        $this->itemPrototypes = new ArrayCollection();
    }
    
    public function addItemPrototype(ItemPrototype $itemPrototype): static
    {
        if (!$this->itemPrototypes->contains($itemPrototype)) {
            $this->itemPrototypes->add($itemPrototype);
            $itemPrototype->setDecharge($this);
        }
        
        return $this;
    }
    
    public function getChantier(): ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function setChantier(ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @return Collection<int, ItemPrototype>
     */
    public function getItemPrototypes(): Collection
    {
        return $this->itemPrototypes;
    }
    
    public function removeItemPrototype(ItemPrototype $itemPrototype): static
    {
        if ($this->itemPrototypes->removeElement($itemPrototype)) {
            // set the owning side to null (unless already changed)
            if ($itemPrototype->getDecharge() === $this) {
                $itemPrototype->setDecharge(null);
            }
        }
        
        return $this;
    }
    
    public function setId(int $id): TypeDecharge
    {
        $this->id = $id;
        
        return $this;
    }
    
}
