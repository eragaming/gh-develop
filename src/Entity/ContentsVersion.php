<?php

namespace App\Entity;

use App\Repository\ContentsVersionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ContentsVersionRepository::class)]
class ContentsVersion
{
    public const FEATURE = 1;
    public const FIX     = 2;
    public const OTHER   = 3;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['changelog', 'news'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 1000), Groups(['changelog', 'news'])]
    private ?string $contents = null;
    
    #[ORM\ManyToOne(targetEntity: VersionsSite::class, fetch: 'EXTRA_LAZY', inversedBy: 'contents')]
    #[ORM\JoinColumn(nullable: false)]
    private ?VersionsSite $versionsSite = null;
    
    #[ORM\Column(type: 'smallint', nullable: true, options: ['default' => 1]), Groups(['changelog', 'news'])]
    private ?int $typeContent = 1;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['changelog', 'news'])]
    private ?int $idRelatif = null;
    
    public function getContents(): ?string
    {
        return $this->contents;
    }
    
    public function setContents(string $contents): self
    {
        $this->contents = $contents;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ContentsVersion
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdRelatif(): ?int
    {
        return $this->idRelatif;
    }
    
    public function setIdRelatif(?int $idRelatif): self
    {
        $this->idRelatif = $idRelatif;
        
        return $this;
    }
    
    public function getTypeContent(): ?int
    {
        return $this->typeContent;
    }
    
    public function setTypeContent(?int $typeContent): self
    {
        $this->typeContent = $typeContent ?? 1;
        
        return $this;
    }
    
    public function getVersionsSite(): ?VersionsSite
    {
        return $this->versionsSite;
    }
    
    public function setVersionsSite(?VersionsSite $versionsSite): self
    {
        $this->versionsSite = $versionsSite;
        
        return $this;
    }
}
