<?php

namespace App\Entity;

use App\Repository\HistoriqueVeilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HistoriqueVeilleRepository::class)]
class HistoriqueVeille
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $citoyen = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $jour = null;
    
    #[ORM\Column(type: 'boolean')]
    private ?bool $veille = null;
    
    /** @var Collection<EtatPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: EtatPrototype::class, fetch: 'EXTRA_LAZY', indexBy: 'etat_prototype_id')]
    private Collection $listeEtats;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'historiqueVeilles')]
    private ?Ville $ville = null;
    
    public function __construct()
    {
        $this->listeEtats = new ArrayCollection();
    }
    
    public function addListeEtat(EtatPrototype $listeEtat): self
    {
        if (!$this->listeEtats->contains($listeEtat)) {
            $this->listeEtats[$listeEtat->getId()] = $listeEtat;
        }
        
        return $this;
    }
    
    public function getCitoyen(): ?User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): HistoriqueVeille
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    public function setJour(int $jour): self
    {
        $this->jour = $jour;
        
        return $this;
    }
    
    public function getListeEtat(int $idEtat): ?EtatPrototype
    {
        return $this->listeEtats->toArray()[$idEtat] ?? null;
    }
    
    public function getListeEtats(): Collection
    {
        return $this->listeEtats;
    }
    
    public function getVeille(): ?bool
    {
        return $this->veille;
    }
    
    public function setVeille(bool $veille): self
    {
        $this->veille = $veille;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function removeListeEtat(EtatPrototype $listeEtat): self
    {
        $this->listeEtats->removeElement($listeEtat);
        
        return $this;
    }
}
