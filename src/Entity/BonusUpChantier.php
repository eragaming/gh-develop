<?php

namespace App\Entity;

use App\Repository\BonusUpChantierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: BonusUpChantierRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class BonusUpChantier
{
    
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'smallint')]
    #[Groups(['evo_chantier', 'admin'])]
    private ?int $id = null;
    
    #[ORM\Column(name: 'level', type: 'smallint')]
    #[Groups(['evo_chantier', 'admin'])]
    private ?int $level = null;
    
    /** @var Collection<UpChantierPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: UpChantierPrototype::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\JoinTable(name: 'bonus_upChantiers')]
    #[ORM\JoinColumn(name: 'bonus_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'upChantier_id', referencedColumnName: 'id', unique: true)]
    #[Groups(['evo_chantier', 'admin'])]
    private Collection $bonusUps;
    
    public function __construct()
    {
        $this->bonusUps = new ArrayCollection();
    }
    
    public function addBonusUp(UpChantierPrototype $valeurUp): self
    {
        if (!$this->bonusUps->contains($valeurUp)) {
            $this->bonusUps[] = $valeurUp;
        }
        
        return $this;
    }
    
    public function getBonusUp(int $typeBonus): ?UpChantierPrototype
    {
        return $this->bonusUps->filter(fn(UpChantierPrototype $up) => $up->getTypeBonus() === $typeBonus)->first() ?: null;
    }
    
    /**
     * @return Collection<UpChantierPrototype>
     */
    public function getBonusUps(): Collection
    {
        return $this->bonusUps;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): BonusUpChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLevel(): ?int
    {
        return $this->level;
    }
    
    public function setLevel(int $level): self
    {
        $this->level = $level;
        
        return $this;
    }
    
    public function removeBonusUp(UpChantierPrototype $valeurUp): self
    {
        $this->bonusUps->removeElement($valeurUp);
        
        return $this;
    }
    
}
