<?php

namespace App\Entity;

use App\Repository\RuineGameZonePrototypeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RuineGameZonePrototypeRepository::class)]
class RuineGameZonePrototype
{
    public final const piece_ouverte       = "p";
    public final const piece_fermee_decap  = "pD";
    public final const piece_fermee_percu  = "pP";
    public final const piece_fermee_magn   = 'pM';
    public final const piece_escalier_up   = "up";
    public final const piece_escalier_down = "down";
    
    #[ORM\Id]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(length: 255)]
    private ?string $label = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'empreinte_id', referencedColumnName: 'id', nullable: true)]
    private ?ItemPrototype $empreinteItem = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'clef_id', referencedColumnName: 'id', nullable: true)]
    private ?ItemPrototype $clefItem = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $levelZone = null;
    
    #[ORM\Column(length: 3, nullable: true)]
    private ?string $typeCase = null;
    
    public function getClefItem(): ?ItemPrototype
    {
        return $this->clefItem;
    }
    
    public function setClefItem(?ItemPrototype $clefItem): static
    {
        $this->clefItem = $clefItem;
        
        return $this;
    }
    
    public function getEmpreinteItem(): ?ItemPrototype
    {
        return $this->empreinteItem;
    }
    
    public function setEmpreinteItem(?ItemPrototype $empreinteItem): static
    {
        $this->empreinteItem = $empreinteItem;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): RuineGameZonePrototype
    {
        $this->id = $id;
        return $this;
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }
    
    public function setLabel(string $label): static
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getLevelZone(): ?int
    {
        return $this->levelZone;
    }
    
    public function setLevelZone(?int $level): static
    {
        $this->levelZone = $level;
        
        return $this;
    }
    
    public function getTypeCase(): ?string
    {
        return $this->typeCase;
    }
    
    public function setTypeCase(?string $type): static
    {
        $this->typeCase = $type;
        
        return $this;
    }
}
