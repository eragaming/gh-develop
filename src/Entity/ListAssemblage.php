<?php

namespace App\Entity;

use App\Repository\ListAssemblageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ListAssemblageRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ListAssemblage
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ency', 'ency_objet', 'admin_ass', 'admin_ass_list'])]
    private ?int $id = null;
    
    /** @var Collection<ItemProbability> */
    #[ORM\ManyToMany(targetEntity: ItemProbability::class, inversedBy: 'listAssemblage', cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true), Groups(['admin_ass'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $itemObtain;
    
    /** @var Collection<ItemNeed> */
    #[ORM\ManyToMany(targetEntity: ItemNeed::class, inversedBy: 'listingAssemblages', cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY'), Groups(['admin_ass'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $itemNeed;
    
    
    #[ORM\ManyToOne(targetEntity: TypeActionAssemblage::class, fetch: 'EXTRA_LAZY', inversedBy: 'listAssemblages')]
    #[Groups(['ency', 'ency_objet', 'admin_ass', 'admin_ass_list'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private TypeActionAssemblage $typeAction;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['ency', 'ency_objet', 'admin_ass', 'admin_ass_list'])]
    private ?ItemPrototype $itemPrincipal = null;
    
    public function __construct()
    {
        $this->itemObtain = new ArrayCollection();
        $this->itemNeed   = new ArrayCollection();
    }
    
    public function addItemNeed(ItemNeed $itemNeed): self
    {
        if (!$this->itemNeed->contains($itemNeed)) {
            $this->itemNeed[] = $itemNeed;
        }
        
        return $this;
    }
    
    public function addItemObtain(ItemProbability $itemObtain): self
    {
        if (!$this->itemObtain->contains($itemObtain)) {
            $this->itemObtain[] = $itemObtain;
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ListAssemblage
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return Collection<ItemNeed>
     */
    public function getItemNeed(): Collection
    {
        return $this->itemNeed;
    }
    
    /**
     * @return Collection<ItemNeed>|ItemNeed[]
     */
    #[Groups(['ency'])]
    public function getItemNeeds(): Collection|array
    {
        return $this->itemNeed;
    }
    
    /**
     * @return Collection<ItemProbability>
     */
    public function getItemObtain(): Collection
    {
        return $this->itemObtain;
    }
    
    /**
     * @return Collection<ItemProbability>|ItemProbability[]
     */
    #[Groups(['ency'])]
    public function getItemObtains(): Collection|array
    {
        return $this->itemObtain;
    }
    
    public function getItemPrincipal(): ?ItemPrototype
    {
        return $this->itemPrincipal;
    }
    
    public function setItemPrincipal(?ItemPrototype $itemPrincipal): ListAssemblage
    {
        $this->itemPrincipal = $itemPrincipal;
        
        return $this;
    }
    
    public function getTypeAction(): TypeActionAssemblage
    {
        return $this->typeAction;
    }
    
    public function setTypeAction(TypeActionAssemblage $typeAction): ListAssemblage
    {
        $this->typeAction = $typeAction;
        
        return $this;
    }
    
    public function removeItemNeed(ItemNeed $itemNeed): self
    {
        $this->itemNeed->removeElement($itemNeed);
        
        return $this;
    }
    
    public function removeItemNeedAll(): self
    {
        foreach ($this->itemNeed as $item) {
            $this->itemNeed->removeElement($item);
        }
        
        return $this;
    }
    
    public function removeItemObtain(ItemProbability $itemObtain): self
    {
        $this->itemNeed->removeElement($itemObtain);
        
        return $this;
    }
    
    public function removeItemObtainAll(): self
    {
        foreach ($this->itemObtain as $item) {
            $this->itemObtain->removeElement($item);
        }
        
        return $this;
    }
    
    
}
