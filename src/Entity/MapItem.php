<?php

namespace App\Entity;

use App\Repository\MapItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: MapItemRepository::class)]
class MapItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'bigint')]
    private string $id;
    
    #[ORM\Column(type: 'smallint'), Groups(['carte'])]
    private ?int $nombre = null;
    
    #[ORM\JoinColumn(name: 'ville_id', referencedColumnName: 'id', unique: false)]
    #[ORM\ManyToOne(targetEntity: ZoneMap::class, fetch: 'EXTRA_LAZY', inversedBy: 'items')]
    private ?ZoneMap $zone = null;
    
    public function __construct(
        #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY'), ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false), Groups(['carte'])]
        #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
        private ItemPrototype                                          $item,
        #[ORM\Column(type: 'boolean'), Groups(['carte'])] private bool $broked,
    )
    {
    }
    
    public function getBroked(): bool
    {
        return $this->broked;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(string $id): MapItem
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ItemPrototype
    {
        return $this->item;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getZone(): ?ZoneMap
    {
        return $this->zone;
    }
    
    public function setZone(?ZoneMap $zone): self
    {
        $this->zone = $zone;
        
        return $this;
    }
    
    public function setBroked(bool $broked): self
    {
        $this->broked = $broked;
        
        return $this;
    }
    
    public function setItem(ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
}
