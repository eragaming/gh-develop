<?php

namespace App\Entity;

use App\Repository\StatutItemExpeditionnaireRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StatutItemExpeditionnaireRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class StatutItemExpeditionnaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(length: 30)]
    private ?string $nom = null;
    
    #[ORM\Column(length: 9)]
    private ?string $couleur = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;
    
    public function getCouleur(): ?string
    {
        return $this->couleur;
    }
    
    public function setCouleur(string $couleur): static
    {
        $this->couleur = $couleur;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): static
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
}
