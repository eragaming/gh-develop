<?php

namespace App\Entity;

use App\Repository\ExpeHordesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ExpeHordesRepository::class)]
class ExpeHordes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['expe'])]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'expeHordes'), Groups(['expe'])]
    private ?Ville $ville = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['expe'])]
    #[ORM\JoinColumn(name: 'citoyen_id', referencedColumnName: 'id', nullable: false)]
    private User $citoyen;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['expe'])]
    private ?string $nom = null;
    
    /**
     * @var int[][] $trace
     */
    #[ORM\Column(type: 'json'), Groups(['expe'])]
    private array $trace = [];
    
    #[ORM\Column(type: 'smallint'), Groups(['expe'])]
    private ?int $length = null;
    
    public function __construct(#[ORM\Column(type: 'smallint')]
                                private int $day)
    {
    }
    
    public function getCitoyen(): User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getDay(): int
    {
        return $this->day;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): ExpeHordes
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLength(): ?int
    {
        return $this->length;
    }
    
    public function setLength(int $length): self
    {
        $this->length = $length;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getTrace(): ?array
    {
        return $this->trace;
    }
    
    public function setTrace(array $trace): self
    {
        $this->trace = $trace;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
}
