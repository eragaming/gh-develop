<?php

namespace App\Entity;

use App\Repository\LeadJumpRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LeadJumpRepository::class)]
class LeadJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['gestion_jump'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY', inversedBy: 'leadJumps'), Groups(['gestion_jump'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeLead::class, fetch: 'EXTRA_LAZY'), Groups(['gestion_jump'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeLead $typeLead = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['gestion_jump'])]
    private ?bool $apprenti = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY', inversedBy: 'lead')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jump $jump = null;
    
    public function getApprenti(): ?bool
    {
        return $this->apprenti;
    }
    
    public function setApprenti(bool $apprenti): self
    {
        $this->apprenti = $apprenti;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): LeadJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(?Jump $jump): self
    {
        $this->jump = $jump;
        
        return $this;
    }
    
    public function getTypeLead(): ?TypeLead
    {
        return $this->typeLead;
    }
    
    public function setTypeLead(?TypeLead $typeLead): self
    {
        $this->typeLead = $typeLead;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
}
