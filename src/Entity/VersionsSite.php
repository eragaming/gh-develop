<?php

namespace App\Entity;

use App\Repository\VersionsSiteRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: VersionsSiteRepository::class)]
class VersionsSite
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: 'integer'), Groups(['changelog', 'news'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['changelog', 'news'])]
    private ?int $versionMajeur = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['changelog', 'news'])]
    private ?int $versionMineur = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['changelog', 'news'])]
    private ?int $versionCorrective = null;
    
    #[ORM\Column(type: 'string', length: 6, nullable: true), Groups(['changelog', 'news'])]
    private ?string $tagName = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['changelog', 'news'])]
    private ?int $numTag = null;
    
    #[ORM\Column(type: 'string', length: 255, nullable: true), Groups(['changelog', 'news'])]
    private ?string $titre = null;
    
    /** @var Collection<ContentsVersion> */
    #[ORM\OneToMany(mappedBy: 'versionsSite', targetEntity: ContentsVersion::class, cascade: ['persist', 'remove'],
        fetch: 'EAGER', orphanRemoval: true),
        Groups(['changelog', 'news'])]
    private Collection $contents;
    
    #[ORM\Column(type: 'datetime'), Groups(['changelog', 'news'])]
    private ?DateTimeInterface $dateVersion = null;
    
    
    /**
     *
     */
    public function __construct()
    {
        $this->contents = new ArrayCollection();
    }
    
    public function addContent(ContentsVersion $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setVersionsSite($this);
        }
        
        return $this;
    }
    
    public function getContent(int $id_relatif): ?ContentsVersion
    {
        return $this->contents->filter(
            static function (ContentsVersion $content) use ($id_relatif) {
                return $content->getIdRelatif() === $id_relatif;
            },
        )->first() ?: null;
    }
    
    /**
     * @return Collection<ContentsVersion>
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }
    
    public function setContents(ArrayCollection $contents): VersionsSite
    {
        $this->contents = $contents;
        
        return $this;
    }
    
    public function getDateVersion(): ?DateTimeInterface
    {
        return $this->dateVersion;
    }
    
    public function setDateVersion(?DateTimeInterface $dateVersion): self
    {
        $this->dateVersion = $dateVersion;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): VersionsSite
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNumTag(): ?int
    {
        return $this->numTag;
    }
    
    public function setNumTag(?int $numTag): self
    {
        $this->numTag = $numTag;
        
        return $this;
    }
    
    public function getTagName(): ?string
    {
        return $this->tagName;
    }
    
    public function setTagName(?string $tagName): self
    {
        $this->tagName = $tagName;
        
        return $this;
    }
    
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    
    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;
        
        return $this;
    }
    
    public function getVersionCorrective(): ?int
    {
        return $this->versionCorrective;
    }
    
    public function setVersionCorrective(?int $versionCorrective): self
    {
        $this->versionCorrective = $versionCorrective;
        
        return $this;
    }
    
    public function getVersionMajeur(): ?int
    {
        return $this->versionMajeur;
    }
    
    public function setVersionMajeur(?int $versionMajeur): self
    {
        $this->versionMajeur = $versionMajeur;
        
        return $this;
    }
    
    public function getVersionMineur(): ?int
    {
        return $this->versionMineur;
    }
    
    public function setVersionMineur(?int $versionMineur): self
    {
        $this->versionMineur = $versionMineur;
        
        return $this;
    }
    
    public function removeContent(ContentsVersion $content): self
    {
        if ($this->contents->removeElement($content)) {
            // set the owning side to null (unless already changed)
            if ($content->getVersionsSite() === $this) {
                $content->setVersionsSite(null);
            }
        }
        
        return $this;
    }
}
