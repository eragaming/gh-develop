<?php

namespace App\Entity;

use App\Doctrine\IdAlphaExpeditionGenerator;
use App\Repository\ExpeditionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ExpeditionRepository::class)]
class Expedition
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaExpeditionGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $id = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_expe'])]
    private ?User $createdBy = null;
    
    #[ORM\ManyToOne]
    #[Groups(['outils_expe'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column]
    #[Groups(['outils_expe'])]
    private ?DateTimeImmutable $createdAt = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?DateTimeImmutable $modifyAt = null;
    
    #[ORM\Column(length: 50)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $minPdc = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $nbrPartie = null;
    
    /** @var Collection<ExpeditionPart> */
    #[ORM\OneToMany(mappedBy: 'expedition', targetEntity: ExpeditionPart::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe'])]
    private Collection $expeditionParts;
    
    #[ORM\ManyToOne(inversedBy: 'expeditions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OutilsExpedition $outilsExpedition = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?ExpeditionType $typeExpe = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $priorite = 1;
    
    #[ORM\Column(name: 'verrou', nullable: true)]
    #[Groups(['outils_expe'])]
    private ?bool $verrou = null;
    
    #[ORM\Column(name: 'verrou_at', type: Types::DATETIME_IMMUTABLE, nullable: true)]
    #[Groups(['outils_expe'])]
    private ?DateTimeImmutable $verrouAt = null;
    
    #[ORM\ManyToOne]
    #[Groups(['outils_expe'])]
    private ?User $verrouBy = null;
    
    public function __construct()
    {
        $this->expeditionParts = new ArrayCollection();
    }
    
    public function addExpeditionPart(ExpeditionPart $expeditionPart): static
    {
        if (!$this->expeditionParts->contains($expeditionPart)) {
            $this->expeditionParts->add($expeditionPart);
            $expeditionPart->setExpedition($this);
        }
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): static
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    /** @return Collection<int, ExpeditionPart> */
    public function getExpeditionParts(): Collection
    {
        return $this->expeditionParts;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): Expedition
    {
        $this->id = $id;
        return $this;
    }
    
    public function getMinPdc(): ?int
    {
        return $this->minPdc;
    }
    
    public function setMinPdc(?int $minPdc): static
    {
        $this->minPdc = $minPdc;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeImmutable
    {
        return $this->modifyAt;
    }
    
    public function setModifyAt(?DateTimeImmutable $modifyAt): static
    {
        $this->modifyAt = $modifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): static
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getNbrPartie(): ?int
    {
        return $this->nbrPartie;
    }
    
    public function setNbrPartie(int $nbrPartie): static
    {
        $this->nbrPartie = $nbrPartie;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getOutilsExpedition(): ?OutilsExpedition
    {
        return $this->outilsExpedition;
    }
    
    public function setOutilsExpedition(?OutilsExpedition $outilsExpedition): static
    {
        $this->outilsExpedition = $outilsExpedition;
        
        return $this;
    }
    
    public function getPriorite(): ?int
    {
        return $this->priorite;
    }
    
    public function setPriorite(int $priorite): static
    {
        $this->priorite = $priorite;
        
        return $this;
    }
    
    public function getTypeExpe(): ?ExpeditionType
    {
        return $this->typeExpe;
    }
    
    public function setTypeExpe(?ExpeditionType $typeExpe): static
    {
        $this->typeExpe = $typeExpe;
        
        return $this;
    }
    
    public function getVerrouAt(): ?DateTimeImmutable
    {
        return $this->verrouAt;
    }
    
    public function setVerrouAt(?DateTimeImmutable $verouAt): static
    {
        $this->verrouAt = $verouAt;
        
        return $this;
    }
    
    public function getVerrouBy(): ?User
    {
        return $this->verrouBy;
    }
    
    public function setVerrouBy(?User $verrouBy): static
    {
        $this->verrouBy = $verrouBy;
        
        return $this;
    }
    
    public function isVerrou(): ?bool
    {
        return $this->verrou;
    }
    
    public function removeExpeditionPart(ExpeditionPart $expeditionPart): static
    {
        if ($this->expeditionParts->removeElement($expeditionPart)) {
            // set the owning side to null (unless already changed)
            if ($expeditionPart->getExpedition() === $this) {
                $expeditionPart->setExpedition(null);
            }
        }
        
        return $this;
    }
    
    public function setVerrou(?bool $verou): static
    {
        $this->verrou = $verou;
        
        return $this;
    }
}
