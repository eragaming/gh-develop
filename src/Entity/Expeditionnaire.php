<?php

namespace App\Entity;

use App\Doctrine\IdAlphaExpeditionnaireGenerator;
use App\Repository\ExpeditionnaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ExpeditionnaireRepository::class)]
class Expeditionnaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaExpeditionnaireGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $id = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $paBase = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?JobPrototype $job = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?bool $soif = null;
    
    #[ORM\Column(options: ['default' => false])]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?bool $preinscrit = false;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?HerosPrototype $actionHeroic = null;
    
    /** @var Collection<SacExpeditionnaire> */
    #[ORM\OneToMany(mappedBy: 'expeditionnaire', targetEntity: SacExpeditionnaire::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private Collection $sac;
    
    /** @var Collection<DispoExpedition> */
    #[ORM\OneToMany(mappedBy: 'expeditionnaire', targetEntity: DispoExpedition::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private Collection $dispo;
    
    #[ORM\ManyToOne(inversedBy: 'expeditionnaires')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ExpeditionPart $expeditionPart = null;
    
    #[ORM\ManyToOne(inversedBy: 'expeditionnaires')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?Citoyens $citoyen = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $position = null;
    
    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $jobFige = false;
    
    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $soifFige = false;
    
    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?bool $forBanni = false;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?int $dispoRapide = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?string $commentaire = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $peBase = null;
    
    public function __construct()
    {
        $this->sac   = new ArrayCollection();
        $this->dispo = new ArrayCollection();
    }
    
    public function addDispo(DispoExpedition $dispo): static
    {
        if (!$this->dispo->contains($dispo)) {
            $this->dispo->add($dispo);
            $dispo->setExpeditionnaire($this);
        }
        
        return $this;
    }
    
    public function addSac(SacExpeditionnaire $sac): static
    {
        if (!$this->sac->contains($sac)) {
            $this->sac->add($sac);
            $sac->setExpeditionnaire($this);
        }
        
        return $this;
    }
    
    public function getActionHeroic(): ?HerosPrototype
    {
        return $this->actionHeroic;
    }
    
    public function setActionHeroic(?HerosPrototype $actionHeroic): static
    {
        $this->actionHeroic = $actionHeroic;
        
        return $this;
    }
    
    public function getCitoyen(): ?Citoyens
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?Citoyens $citoyen): static
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }
    
    public function setCommentaire(?string $commentaire): static
    {
        $this->commentaire = $commentaire;
        
        return $this;
    }
    
    public function getDispo(): Collection
    {
        return $this->dispo;
    }
    
    public function setDispo(Collection $dispo): Expeditionnaire
    {
        $this->dispo = $dispo;
        return $this;
    }
    
    public function getDispoRapide(): ?int
    {
        return $this->dispoRapide;
    }
    
    public function setDispoRapide(?int $dispoRapide): static
    {
        $this->dispoRapide = $dispoRapide;
        
        return $this;
    }
    
    public function getExpeditionPart(): ?ExpeditionPart
    {
        return $this->expeditionPart;
    }
    
    public function setExpeditionPart(?ExpeditionPart $expeditionPart): static
    {
        $this->expeditionPart = $expeditionPart;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): Expeditionnaire
    {
        $this->id = $id;
        return $this;
    }
    
    public function getJob(): ?JobPrototype
    {
        return $this->job;
    }
    
    public function setJob(?JobPrototype $job): static
    {
        $this->job = $job;
        
        return $this;
    }
    
    public function getPaBase(): ?int
    {
        return $this->paBase;
    }
    
    public function setPaBase(?int $paBase): static
    {
        $this->paBase = $paBase;
        
        return $this;
    }
    
    public function getPeBase(): ?int
    {
        return $this->peBase;
    }
    
    public function setPeBase(?int $peBase): static
    {
        $this->peBase = $peBase;
        
        return $this;
    }
    
    public function getPosition(): ?int
    {
        return $this->position;
    }
    
    public function setPosition(int $position): static
    {
        $this->position = $position;
        
        return $this;
    }
    
    /** @return Collection<int, SacExpeditionnaire> */
    public function getSac(): Collection
    {
        return $this->sac;
    }
    
    public function setSac(Collection $sac): Expeditionnaire
    {
        $this->sac = $sac;
        return $this;
    }
    
    public function isForBanni(): ?bool
    {
        return $this->forBanni;
    }
    
    public function isJobFige(): ?bool
    {
        return $this->jobFige;
    }
    
    public function isPreinscrit(): ?bool
    {
        return $this->preinscrit;
    }
    
    public function isSoif(): ?bool
    {
        return $this->soif;
    }
    
    public function isSoifFige(): ?bool
    {
        return $this->soifFige;
    }
    
    public function removeDispo(DispoExpedition $dispo): static
    {
        if ($this->dispo->removeElement($dispo)) {
            // set the owning side to null (unless already changed)
            if ($dispo->getExpeditionnaire() === $this) {
                $dispo->setExpeditionnaire(null);
            }
        }
        
        return $this;
    }
    
    public function removeSac(SacExpeditionnaire $sac): static
    {
        if ($this->sac->removeElement($sac)) {
            // set the owning side to null (unless already changed)
            if ($sac->getExpeditionnaire() === $this) {
                $sac->setExpeditionnaire(null);
            }
        }
        
        return $this;
    }
    
    public function setForBanni(?bool $forBanni): static
    {
        $this->forBanni = $forBanni;
        
        return $this;
    }
    
    public function setJobFige(bool $jobFige): static
    {
        $this->jobFige = $jobFige;
        
        return $this;
    }
    
    public function setPreinscrit(bool $preinscrit): static
    {
        $this->preinscrit = $preinscrit;
        
        return $this;
    }
    
    public function setSoif(?bool $soif): static
    {
        $this->soif = $soif;
        
        return $this;
    }
    
    public function setSoifFige(bool $soifFige): static
    {
        $this->soifFige = $soifFige;
        
        return $this;
    }
}
