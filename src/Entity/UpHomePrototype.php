<?php

namespace App\Entity;

use App\Repository\UpHomePrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UpHomePrototypeRepository::class)]
class UpHomePrototype
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['outils_chantier', 'ency_hab'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 15)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?string $label = null;
    
    #[ORM\Column(type: 'string', length: 255, nullable: true), Groups(['ency_hab'])]
    private ?string $description = null;
    
    /** @var Collection<LvlUpHome> */
    #[ORM\JoinTable(name: 'up_lvl')]
    #[ORM\JoinColumn(name: 'up_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'lvl_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: LvlUpHome::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private Collection $levels;
    
    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?string $icon = null;
    
    public function __construct()
    {
        $this->levels = new ArrayCollection();
    }
    
    public function addLevel(LvlUpHome $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
        }
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): UpHomePrototype
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }
    
    public function setLabel(string $label): self
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getLevels(): Collection
    {
        return $this->levels;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function removeLevel(LvlUpHome $level): self
    {
        $this->levels->removeElement($level);
        
        return $this;
    }
}
