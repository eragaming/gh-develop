<?php

namespace App\Entity;

use App\Repository\MasquageJumpRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MasquageJumpRepository::class)]
class MasquageJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?User $user = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'jump_id', referencedColumnName: 'id', nullable: false)]
    private ?Jump $jump = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): MasquageJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(Jump $jump): self
    {
        $this->jump = $jump;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
}
