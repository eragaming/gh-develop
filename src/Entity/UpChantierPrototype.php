<?php

namespace App\Entity;

use App\Repository\UpChantierPrototypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UpChantierPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class UpChantierPrototype
{
    public const TYPE_ADD_EAU       = 1;
    public const TYPE_ADD_DEF       = 2;
    public const TYPE_ADD_DEF_PCT   = 3;
    public const TYPE_ADD_PCT_REGEN = 4;
    public const TYPE_RED_PA        = 5;
    public const TYPE_MOD_COUT_PA   = 6;
    public const TYPE_BONUS_OD      = 7;
    public const TYPE_SPE           = 8;
    public const TYPE_SUP_EAU       = 9;
    public const TYPE_ADD_AME       = 10;
    public const TYPE_SUP_PAMP      = 11;
    public const TYPE_SUP_PILE      = 12;
    public const TAB_TYPE_EVO       = [
        UpChantierPrototype::TYPE_ADD_EAU       => "Ajoute de l'eau",
        UpChantierPrototype::TYPE_ADD_DEF       => "Ajout de la def",
        UpChantierPrototype::TYPE_ADD_DEF_PCT   => "Ajout % def",
        UpChantierPrototype::TYPE_ADD_PCT_REGEN => "Ajout % régen",
        UpChantierPrototype::TYPE_RED_PA        => "Reduction cout en PA",
        UpChantierPrototype::TYPE_MOD_COUT_PA   => "gain PV par PA",
        UpChantierPrototype::TYPE_BONUS_OD      => "Valeur objet def",
        UpChantierPrototype::TYPE_SPE           => "Valeur textuel",
        UpChantierPrototype::TYPE_SUP_EAU       => "Consomme de l'eau",
        UpChantierPrototype::TYPE_ADD_AME       => "Ajout de la def/âme",
        UpChantierPrototype::TYPE_SUP_PAMP      => "Consomme des pamplemousses explosifs",
        UpChantierPrototype::TYPE_SUP_PILE      => "Consomme des piles",
    ];
    /*public const ARCHI_LVL0         = "1 Plans de chantier (communs) après chaque attaque" ;
    public const ARCHI_LVL1         = "4 Plans de chantier (communs) Plan de chantier (commun) une fois" ;
    public const ARCHI_LVL2         = "2 Plans de chantier (communs) Plan de chantier (commun), 2 Plans de chantier (inhabituels) Plan de chantier (inhabituel) et une surprise (rien ou 1 plan supérieur)" ;
    public const ARCHI_LVL3         = "2 Plans de chantier (inhabituels) Plan de chantier (inhabituel) et 2 Plans de chantier (rares) Plan de chantier (rare) et une surprise (rien ou 1 plan supérieur)" ;*/
    public const TDG_LVL0     = "La plateforme d'observation vous permet d'élire le citoyen le plus à même de vous guider lors des expéditions.";
    public const TDG_LVL1     = "Détecte chaque matin tous les zombies à 3 km de la ville.";
    public const TDG_LVL2     = "Détecte chaque matin tous les zombies à 6 km de la ville.";
    public const TDG_LVL3     = "Détecte chaque matin tous les zombies à 10 km de la ville.";
    public const TDG_LVL4     = "Détecte chaque matin tous les zombies à 10 km de la ville et permet aux citoyens à 1 km de la ville de rentrer gratuitement.";
    public const TDG_LVL5     = "Détecte chaque matin tous les zombies à 10 km de la ville et permet aux citoyens à 2 km de la ville de rentrer gratuitement";
    public const ECL_LVL0     = "La réduction des chances de fouilles la nuit est annulée dans un rayon de 6 km autour de la ville.";
    public const ECL_LVL1     = "La réduction des chances de fouilles la nuit est annulée sur toute la carte, 2 piles sont consommées par jour.";
    public const ECL_LVL2     = "Fournit un léger bonus de recherche uniquement la nuit, 3 piles sont consommées par jour.";
    public const POT_LVL0     = "Produit des légumes suspects et des melons d'intestin.";
    public const POT_LVL1     = "Produit quotidiennement du claviceps purpurea et des herbes en plus de sa production du niveau précédent.";
    public const POT_LVL2     = "Produit quotidiennement des pamplemousse explosifs en plus de sa production du niveau précédent.";
    public const POT_LVL3     = "Produit quotidiennement des pommes en plus de sa production du niveau précédent.";
    public const POT_LVL4     = "Produit quotidiennement de la citrouille en plus de sa production du niveau précédent.";
    public const RONDE_LVL0   = "Ce chemin de Ronde autour de la ville permettra à quelques veilleurs de protéger la ville depuis les remparts.";
    public const RONDE_LVL1   = "En élargissant quelquee peu le Chemin de Ronde, plus de veilleurs pourront lutter lors de l'attaque.";
    public const RONDE_LVL2   = "Un chemin de Ronde si large que toute la ville pourrait désormais y passer la nuit.";
    public const RONDE_LVL3   = "En aménageant le Chemin de Ronde, les veilleurs s'y sentiront légèrement plus en sécurité.";
    public const SOURCE_LVL2  = "Chaque âme torturée impacte moins la ville.";
    public const SOURCE_LVL3  = "Chaque âme purifiée apporte 2 points de classement et chaque âme torturée impacte moins la ville.";
    public const STRA_CIT_LV0 = "La ville obtiendra 1 plan inhabituel après l'attaque.";
    public const STRA_CIT_LV1 = "La ville obtiendra également 1 plan commun et 1 plan inhabituel après chaque attaque.";
    public const STRA_CIT_LV2 = "La ville reçoit 1 plan inhabituel et 1 plan rare après chaque attaque.";
    public const STRA_CIT_LV3 = "La ville reçoit 2 plans rares après chaque attaque.";
    public const STRA_CIT_LV4 = "Après chaque attaque, la ville reçoit 1 plan rare et -éventuellement- une très belle surprise.";
    public const PORTAIL_LV3  = "L'ouverture de la porte est gratuite avant midi.";
    public const DECHARGE_LV0 = "Permet de jeter des objets pour 1 point de défense.";
    public const DECHARGE_LV1 = "Augmente la défense fournie de tous les armes et nourritures jetés dans la décharge.";
    public const DECHARGE_LV2 = "Augmente la défense fournie pour tous les objets défensifs et animaux jetés dans la décharge.";
    
    #[ORM\Id]
    #[ORM\Column(type: 'smallint')]
    #[Groups(['evo_chantier', 'admin'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['evo_chantier', 'admin', 'comparatif'])]
    private ?int $typeBonus = null;
    
    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['evo_chantier', 'admin', 'comparatif'])]
    private ?string $valeurUp = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): UpChantierPrototype
    {
        $this->id = $id;
        return $this;
    }
    
    public function getTypeBonus(): ?int
    {
        return $this->typeBonus;
    }
    
    public function setTypeBonus(int $typeBonus): self
    {
        $this->typeBonus = $typeBonus;
        
        return $this;
    }
    
    public function getValeurUp(): string
    {
        return $this->valeurUp;
    }
    
    public function setValeurUp(string $valeurUp): UpChantierPrototype
    {
        $this->valeurUp = $valeurUp;
        
        return $this;
    }
    
}
