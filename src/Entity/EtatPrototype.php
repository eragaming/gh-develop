<?php

namespace App\Entity;

use App\Repository\EtatPrototypeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EtatPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class EtatPrototype
{
    public const ID_ALCOOL = 1;
    public const ID_DRUG   = 2;
    public const ID_TERRO  = 3;
    public const ID_BLESSE = 4;
    public const ID_INFECT = 5;
    public const ID_CONVA  = 6;
    public const ID_GDB    = 7;
    public const ID_DEPEND = 8;
    public const ID_GOULE  = 9;
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 20)]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 20)]
    private ?string $icon = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $modifSurvie = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $modifDef = null;
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getModifDef(): ?int
    {
        return $this->modifDef;
    }
    
    public function setModifDef(int $modifDef): self
    {
        $this->modifDef = $modifDef;
        
        return $this;
    }
    
    public function getModifSurvie(): ?int
    {
        return $this->modifSurvie;
    }
    
    public function setModifSurvie(int $modifSurvie): self
    {
        $this->modifSurvie = $modifSurvie;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
