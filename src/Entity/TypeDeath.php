<?php

namespace App\Entity;

use App\Repository\TypeDeathRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeDeathRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeDeath
{
    
    /**
     * Envoyé par l'API Hordes
     * 1 Déshydratation terminale
     * 2 Strangulation
     * 3 Suicide par ingestion de Cyanure
     * 4 Justice populaire (mort par Pendaison) !
     * 5 Disparu(e) dans l'Outre-Monde pendant la nuit !
     * 6 Lacéré(e)... Dévoré(e)... pendant l'attaque de la nuit !
     * 7 Pénurie de drogue
     * 8 Infection généralisée
     * 9 Balle dans la tête
     * 10 Raison inconnue
     * 11 Meurtre par empoisonnement !
     * 12 Dévoré(e) par une Goule !
     * 13 Goule abattue au cours d'une agression !
     * 14 Goule affamée
     * 15 Cage à viande
     * 16 Crucifixion
     * 17 Pulvérisé un peu partout
     * 18 Possédé par une âme torturée
     **/
    public const Dehydration   = 1;
    public const Strangulation = 2;
    public const Cyanure       = 3;
    public const Pendu         = 4;
    public const Disparu       = 5;
    public const MortAttaque   = 6;
    public const Penurie       = 7;
    public const Infection     = 8;
    public const Balle         = 9;
    public const Inconnue      = 10;
    public const Poison        = 11;
    public const DevoreGoule   = 12;
    public const GouleAbbatu   = 13;
    public const GouleAffame   = 14;
    public const CageAViande   = 15;
    public const Cruxifiction  = 16;
    public const Explosion     = 17;
    public const Possede       = 18;
    public const Radiations    = 19;
    public const Apocalypse    = 20;
    public const LiverEaten    = 21;
    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?string $nom   = null;
    #[ORM\Column(type: 'string', length: 15)]
    #[Groups(['citoyens', 'comparatif'])]
    private ?string $icon  = null;
    #[ORM\Column(type: 'string', length: 64)]
    #[Groups(['picto', 'citoyens', 'comparatif'])]
    private ?string $label = null;
    
    public function __construct(#[ORM\Id]
                                #[ORM\Column(type: 'smallint', unique: true, nullable: false)]
                                #[Groups(['citoyens', 'comparatif'])]
                                private ?int $idMort)
    {
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(?string $icon): TypeDeath
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getIdMort(): ?int
    {
        return $this->idMort;
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }
    
    public function setLabel(?string $label): TypeDeath
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function setIdMort(?int $idMort): TypeDeath
    {
        $this->idMort = $idMort;
        
        return $this;
    }
}
