<?php

namespace App\Entity;

use App\Enum\ListColor;
use App\Enum\MenuType;
use App\Repository\UserPersoCouleurRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UserPersoCouleurRepository::class)]
class UserPersoCouleur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\OneToOne(inversedBy: 'userPersoCouleur', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?ThemeUser $theme = null;
    
    #[ORM\Column(name: 'couleur_scrut', type: 'string', length: 9, nullable: false,
        options: ['default' => '#FFFFFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurScrut = '#FFFFFFFF';
    
    #[ORM\Column(name: 'couleur_km', type: 'string', length: 9, nullable: false, options: ['default' => '#0000FFFF']),
        Groups(['admin', 'carte_user', 'option'])]
    private string $couleurKm = '#0000FFFF';
    
    #[ORM\Column(name: 'couleur_pos', type: 'string', length: 9, nullable: false,
        options: ['default' => '#00FF04FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurPos = '#00FF04FF';
    
    #[ORM\Column(name: 'couleur_bat', type: 'string', length: 9, nullable: false,
        options: ['default' => '#919191FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurBat = '#919191FF';
    
    #[ORM\Column(name: 'couleur_select_obj', type: 'string', length: 9, nullable: false,
        options: ['default' => '#FF0000FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurSelectObj = '#FF0000FF';
    
    #[ORM\Column(name: 'couleur_select_bat', type: 'string', length: 9, nullable: false,
        options: ['default' => '#8100FFFF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurSelectBat = '#8100FFFF';
    
    #[ORM\Column(name: 'couleur_select_cit', type: 'string', length: 9, nullable: false,
        options: ['default' => '#F3FF00FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurSelectCit = '#F3FF00FF';
    
    #[ORM\Column(name: 'couleur_vue_auj', type: 'string', length: 9, nullable: false,
        options: ['default' => '#FFFF00FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurVueAuj = '#FFFF00FF';
    
    #[ORM\Column(name: 'couleur_vue24', type: 'string', length: 9, nullable: false,
        options: ['default' => '#FFA600FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurVue24 = '#FFA600FF';
    
    #[ORM\Column(name: 'couleur_vue48', type: 'string', length: 9, nullable: false,
        options: ['default' => '#FF0000FF']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurVue48 = '#FF0000FF';
    
    #[ORM\Column(name: 'couleur_epuise', type: 'string', length: 9, nullable: false,
        options: ['default' => '#00000040']), Groups(['admin', 'carte_user', 'option'])]
    private string $couleurEpuise = '#00000040';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#FF00FFFF']),
        Groups(['admin', 'carte_user', 'option'])]
    private string $colorSelExp = '#FF00FFFF';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#00D5FFFF']),
        Groups(['admin', 'carte_user', 'option'])]
    private string $couleurSelCaseMaj = '#00D5FFFF';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#00ff004d']),
        Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurDanger0 = '#00ff004d';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#0077004d']),
        Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurDanger1 = '#0077004d';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#ff9c004d']),
        Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurDanger2 = '#ff9c004d';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#ff00004d']),
        Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurDanger3 = '#ff00004d';
    
    #[ORM\Column(type: 'string', length: 9, nullable: false, options: ['default' => '#000000FF']),
        Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurDanger4 = '#000000FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#000000FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurPA = '#000000FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#00FBFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurZone = '#00FBFFFF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#000000FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorZombie = '#000000FF';
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'carte_user', 'option'])]
    private ?bool $carteTextured = false;
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#00000000']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurCarte = '#00000000';
    
    #[ORM\Column(type: 'string', length: 20, nullable: false, enumType: ListColor::class, options: ['default' => ListColor::JET]), Groups(['admin', 'carte_user', 'option'])]
    private ?ListColor $colormap = ListColor::JET;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 50]), Groups(['admin', 'carte_user', 'option'])]
    private ?int $alphaColormap = 50;
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#00FF00FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorEstimationZombie = '#00FF00FF';
    
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#0075FFFF']), Groups(['admin', 'ruine', 'option'])]
    private ?string $colorStairUp = '#0075FFFF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#F9FF00FF']), Groups(['admin', 'ruine', 'option'])]
    private ?string $colorStairDown = '#F9FF00FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#0000FFFF']), Groups(['admin', 'ruine', 'option'])]
    private ?string $colorSelectObjetRuine = '#0000FFFF';
    
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#A95400FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorMyExp = '#A95400FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#00EFFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorFlag = '#00EFFFFF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FF0000FF']), Groups(['admin', 'expe', 'option'])]
    private ?string $colorDispo1 = '#FF0000FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#1E7A00FF']), Groups(['admin', 'expe', 'option'])]
    private ?string $colorDispo2 = '#1E7A00FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FF9100FF']), Groups(['admin', 'expe', 'option'])]
    private ?string $colorDispo3 = '#FF9100FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FF00F1FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorFlagFinish = '#FF00F1FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#CD1274BA']), Groups(['admin', 'ame', 'option'])]
    private ?string $colorVilleCommune = '#CD1274BA';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#ff00fa70']), Groups(['admin', 'expe', 'option'])]
    private ?string $myColorExpe = '#ff00fa70';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#000000FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorTown = '#000000FF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFFFFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorCity = '#FFFFFFFF';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#00000066']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorNonVu = "#00000066";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#3cff3caf']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurControleOk = '#3cff3caf';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#ff3c3caf']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurContoleNok = '#ff3c3caf';
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#080094FF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $colorBatEpuise = "#080094FF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFFFFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurNbrCitoyen = "#FFFFFFFF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFFFFFFF']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurNbrItemsSol = "#FFFFFFFF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFEB3B7F']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurChantierConstruit = "#FFEB3B7F";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FF57227F']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurChantierARepa = "#FF57227F";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#2196F37F']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurChantierEnConstruction = "#2196F37F";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#9C27B07F']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurChantierDispo = "#9C27B07F";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#9E9E9E7F']), Groups(['admin', 'carte_user', 'option'])]
    private ?string $couleurPlanManquant = "#9E9E9E7F";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#C0C0C0FF']), Groups(['admin', 'carte_user', 'option','citoyen'])]
    private ?string $couleurNonHeros = "#C0C0C0FF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#A3C4F3FF']), Groups(['admin', 'carte_user', 'option','citoyen'])]
    private ?string $couleurDebutant = "#A3C4F3FF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#90EE90FF']), Groups(['admin', 'carte_user', 'option','citoyen'])]
    private ?string $couleurApprenti = "#90EE90FF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFA07AFF']), Groups(['admin', 'carte_user', 'option','citoyen'])]
    private ?string $couleurExpert = "#FFA07AFF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => '#FFD700FF']), Groups(['admin', 'carte_user', 'option','citoyen'])]
    private ?string $couleurElite = "#FFD700FF";
    
    public function getAlphaColormap(): ?int
    {
        return $this->alphaColormap;
    }
    
    public function setAlphaColormap(?int $alphaColormap): UserPersoCouleur
    {
        $this->alphaColormap = $alphaColormap;
        return $this;
    }
    
    public function getColorBatEpuise(): ?string
    {
        return $this->colorBatEpuise;
    }
    
    public function setColorBatEpuise(string $colorBatEpuise): static
    {
        $this->colorBatEpuise = $colorBatEpuise;
        
        return $this;
    }
    
    public function getColorCity(): ?string
    {
        return $this->colorCity;
    }
    
    public function setColorCity(?string $colorCity): UserPersoCouleur
    {
        $this->colorCity = $colorCity;
        return $this;
    }
    
    public function getColorDispo1(): ?string
    {
        return $this->colorDispo1;
    }
    
    public function setColorDispo1(?string $colorDispo1): UserPersoCouleur
    {
        $this->colorDispo1 = $colorDispo1;
        return $this;
    }
    
    public function getColorDispo2(): ?string
    {
        return $this->colorDispo2;
    }
    
    public function setColorDispo2(?string $colorDispo2): UserPersoCouleur
    {
        $this->colorDispo2 = $colorDispo2;
        return $this;
    }
    
    public function getColorDispo3(): ?string
    {
        return $this->colorDispo3;
    }
    
    public function setColorDispo3(?string $colorDispo3): UserPersoCouleur
    {
        $this->colorDispo3 = $colorDispo3;
        return $this;
    }
    
    public function getColorEstimationZombie(): ?string
    {
        return $this->colorEstimationZombie;
    }
    
    public function setColorEstimationZombie(?string $colorEstimationZombie): UserPersoCouleur
    {
        $this->colorEstimationZombie = $colorEstimationZombie;
        return $this;
    }
    
    public function getColorFlag(): ?string
    {
        return $this->colorFlag;
    }
    
    public function setColorFlag(?string $colorFlag): UserPersoCouleur
    {
        $this->colorFlag = $colorFlag;
        return $this;
    }
    
    public function getColorFlagFinish(): ?string
    {
        return $this->colorFlagFinish;
    }
    
    public function setColorFlagFinish(?string $colorFlagFinish): UserPersoCouleur
    {
        $this->colorFlagFinish = $colorFlagFinish;
        return $this;
    }
    
    public function getColorMyExp(): ?string
    {
        return $this->colorMyExp;
    }
    
    public function setColorMyExp(?string $colorMyExp): UserPersoCouleur
    {
        $this->colorMyExp = $colorMyExp;
        return $this;
    }
    
    public function getColorNonVu(): ?string
    {
        return $this->colorNonVu;
    }
    
    public function setColorNonVu(string $colorNonVu): static
    {
        $this->colorNonVu = $colorNonVu;
        
        return $this;
    }
    
    public function getColorSelExp(): string
    {
        return $this->colorSelExp;
    }
    
    public function setColorSelExp(string $colorSelExp): UserPersoCouleur
    {
        $this->colorSelExp = $colorSelExp;
        return $this;
    }
    
    public function getColorSelectObjetRuine(): ?string
    {
        return $this->colorSelectObjetRuine;
    }
    
    public function setColorSelectObjetRuine(?string $colorSelectObjetRuine): UserPersoCouleur
    {
        $this->colorSelectObjetRuine = $colorSelectObjetRuine;
        return $this;
    }
    
    public function getColorStairDown(): ?string
    {
        return $this->colorStairDown;
    }
    
    public function setColorStairDown(?string $colorStairDown): UserPersoCouleur
    {
        $this->colorStairDown = $colorStairDown;
        return $this;
    }
    
    public function getColorStairUp(): ?string
    {
        return $this->colorStairUp;
    }
    
    public function setColorStairUp(?string $colorStairUp): UserPersoCouleur
    {
        $this->colorStairUp = $colorStairUp;
        return $this;
    }
    
    public function getColorTown(): ?string
    {
        return $this->colorTown;
    }
    
    public function setColorTown(?string $colorTown): UserPersoCouleur
    {
        $this->colorTown = $colorTown;
        return $this;
    }
    
    public function getColorVilleCommune(): ?string
    {
        return $this->colorVilleCommune;
    }
    
    public function setColorVilleCommune(?string $colorVilleCommune): UserPersoCouleur
    {
        $this->colorVilleCommune = $colorVilleCommune;
        return $this;
    }
    
    public function getColorZombie(): ?string
    {
        return $this->colorZombie;
    }
    
    public function setColorZombie(?string $colorZombie): UserPersoCouleur
    {
        $this->colorZombie = $colorZombie;
        return $this;
    }
    
    public function getColormap(): ?ListColor
    {
        return $this->colormap;
    }
    
    public function setColormap(?ListColor $colormap): UserPersoCouleur
    {
        $this->colormap = $colormap;
        return $this;
    }
    
    public function getCouleurBat(): string
    {
        return $this->couleurBat;
    }
    
    public function setCouleurBat(string $couleurBat): UserPersoCouleur
    {
        $this->couleurBat = $couleurBat;
        return $this;
    }
    
    public function getCouleurCarte(): ?string
    {
        return $this->couleurCarte;
    }
    
    public function setCouleurCarte(?string $couleurCarte): UserPersoCouleur
    {
        $this->couleurCarte = $couleurCarte;
        return $this;
    }
    
    public function getCouleurChantierARepa(): ?string
    {
        return $this->couleurChantierARepa;
    }
    
    public function setCouleurChantierARepa(string $couleurChantierARepa): static
    {
        $this->couleurChantierARepa = $couleurChantierARepa;
        
        return $this;
    }
    
    public function getCouleurChantierConstruit(): ?string
    {
        return $this->couleurChantierConstruit;
    }
    
    public function setCouleurChantierConstruit(string $chantierConstruit): static
    {
        $this->couleurChantierConstruit = $chantierConstruit;
        
        return $this;
    }
    
    public function getCouleurChantierDispo(): ?string
    {
        return $this->couleurChantierDispo;
    }
    
    public function setCouleurChantierDispo(string $couleurChantierDispo): static
    {
        $this->couleurChantierDispo = $couleurChantierDispo;
        
        return $this;
    }
    
    public function getCouleurChantierEnConstruction(): ?string
    {
        return $this->couleurChantierEnConstruction;
    }
    
    public function setCouleurChantierEnConstruction(string $couleurChantierEnConstruction): static
    {
        $this->couleurChantierEnConstruction = $couleurChantierEnConstruction;
        
        return $this;
    }
    
    public function getCouleurContoleNok(): ?string
    {
        return $this->couleurContoleNok;
    }
    
    public function setCouleurContoleNok(string $couleurContoleNok): static
    {
        $this->couleurContoleNok = $couleurContoleNok;
        
        return $this;
    }
    
    public function getCouleurControleOk(): ?string
    {
        return $this->couleurControleOk;
    }
    
    public function setCouleurControleOk(string $couleurControleOk): static
    {
        $this->couleurControleOk = $couleurControleOk;
        
        return $this;
    }
    
    public function getCouleurDanger0(): ?string
    {
        return $this->couleurDanger0;
    }
    
    public function setCouleurDanger0(?string $couleurDanger0): UserPersoCouleur
    {
        $this->couleurDanger0 = $couleurDanger0;
        return $this;
    }
    
    public function getCouleurDanger1(): ?string
    {
        return $this->couleurDanger1;
    }
    
    public function setCouleurDanger1(?string $couleurDanger1): UserPersoCouleur
    {
        $this->couleurDanger1 = $couleurDanger1;
        return $this;
    }
    
    public function getCouleurDanger2(): ?string
    {
        return $this->couleurDanger2;
    }
    
    public function setCouleurDanger2(?string $couleurDanger2): UserPersoCouleur
    {
        $this->couleurDanger2 = $couleurDanger2;
        return $this;
    }
    
    public function getCouleurDanger3(): ?string
    {
        return $this->couleurDanger3;
    }
    
    public function setCouleurDanger3(?string $couleurDanger3): UserPersoCouleur
    {
        $this->couleurDanger3 = $couleurDanger3;
        return $this;
    }
    
    public function getCouleurDanger4(): ?string
    {
        return $this->couleurDanger4;
    }
    
    public function setCouleurDanger4(?string $couleurDanger4): UserPersoCouleur
    {
        $this->couleurDanger4 = $couleurDanger4;
        return $this;
    }
    
    public function getCouleurEpuise(): string
    {
        return $this->couleurEpuise;
    }
    
    public function setCouleurEpuise(string $couleurEpuise): UserPersoCouleur
    {
        $this->couleurEpuise = $couleurEpuise;
        return $this;
    }
    
    public function getCouleurKm(): string
    {
        return $this->couleurKm;
    }
    
    public function setCouleurKm(string $couleurKm): UserPersoCouleur
    {
        $this->couleurKm = $couleurKm;
        return $this;
    }
    
    public function getCouleurNbrCitoyen(): ?string
    {
        return $this->couleurNbrCitoyen;
    }
    
    public function setCouleurNbrCitoyen(string $couleurNbrCitoyen): static
    {
        $this->couleurNbrCitoyen = $couleurNbrCitoyen;
        
        return $this;
    }
    
    public function getCouleurNbrItemsSol(): ?string
    {
        return $this->couleurNbrItemsSol;
    }
    
    public function setCouleurNbrItemsSol(string $couleurNbrItemsSol): static
    {
        $this->couleurNbrItemsSol = $couleurNbrItemsSol;
        
        return $this;
    }
    
    public function getCouleurPA(): ?string
    {
        return $this->couleurPA;
    }
    
    public function setCouleurPA(?string $couleurPA): UserPersoCouleur
    {
        $this->couleurPA = $couleurPA;
        return $this;
    }
    
    public function getCouleurPlanManquant(): ?string
    {
        return $this->couleurPlanManquant;
    }
    
    public function setCouleurPlanManquant(string $couleurPlanManquant): static
    {
        $this->couleurPlanManquant = $couleurPlanManquant;
        
        return $this;
    }
    
    public function getCouleurPos(): string
    {
        return $this->couleurPos;
    }
    
    public function setCouleurPos(string $couleurPos): UserPersoCouleur
    {
        $this->couleurPos = $couleurPos;
        return $this;
    }
    
    public function getCouleurScrut(): string
    {
        return $this->couleurScrut;
    }
    
    public function setCouleurScrut(string $couleurScrut): UserPersoCouleur
    {
        $this->couleurScrut = $couleurScrut;
        
        return $this;
    }
    
    public function getCouleurSelCaseMaj(): string
    {
        return $this->couleurSelCaseMaj;
    }
    
    public function setCouleurSelCaseMaj(string $couleurSelCaseMaj): UserPersoCouleur
    {
        $this->couleurSelCaseMaj = $couleurSelCaseMaj;
        return $this;
    }
    
    public function getCouleurSelectBat(): string
    {
        return $this->couleurSelectBat;
    }
    
    public function setCouleurSelectBat(string $couleurSelectBat): UserPersoCouleur
    {
        $this->couleurSelectBat = $couleurSelectBat;
        return $this;
    }
    
    public function getCouleurSelectCit(): string
    {
        return $this->couleurSelectCit;
    }
    
    public function setCouleurSelectCit(string $couleurSelectCit): UserPersoCouleur
    {
        $this->couleurSelectCit = $couleurSelectCit;
        return $this;
    }
    
    public function getCouleurSelectObj(): string
    {
        return $this->couleurSelectObj;
    }
    
    public function setCouleurSelectObj(string $couleurSelectObj): UserPersoCouleur
    {
        $this->couleurSelectObj = $couleurSelectObj;
        return $this;
    }
    
    public function getCouleurVue24(): string
    {
        return $this->couleurVue24;
    }
    
    public function setCouleurVue24(string $couleurVue24): UserPersoCouleur
    {
        $this->couleurVue24 = $couleurVue24;
        return $this;
    }
    
    public function getCouleurVue48(): string
    {
        return $this->couleurVue48;
    }
    
    public function setCouleurVue48(string $couleurVue48): UserPersoCouleur
    {
        $this->couleurVue48 = $couleurVue48;
        return $this;
    }
    
    public function getCouleurVueAuj(): string
    {
        return $this->couleurVueAuj;
    }
    
    public function setCouleurVueAuj(string $couleurVueAuj): UserPersoCouleur
    {
        $this->couleurVueAuj = $couleurVueAuj;
        return $this;
    }
    
    public function getCouleurZone(): ?string
    {
        return $this->couleurZone;
    }
    
    public function setCouleurZone(?string $couleurZone): UserPersoCouleur
    {
        $this->couleurZone = $couleurZone;
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMyColorExpe(): ?string
    {
        return $this->myColorExpe;
    }
    
    public function setMyColorExpe(?string $myColorExpe): UserPersoCouleur
    {
        $this->myColorExpe = $myColorExpe;
        return $this;
    }
    
    public function getTheme(): ?ThemeUser
    {
        return $this->theme;
    }
    
    public function setTheme(ThemeUser $theme): static
    {
        $this->theme = $theme;
        
        return $this;
    }
    
    public function isCarteTextured(): ?bool
    {
        return $this->carteTextured;
    }
    
    public function setCarteTextured(?bool $carteTextured): UserPersoCouleur
    {
        $this->carteTextured = $carteTextured;
        return $this;
    }

    public function getCouleurNonHeros(): ?string
    {
        return $this->couleurNonHeros;
    }

    public function setCouleurNonHeros(string $couleurNonHeros): static
    {
        $this->couleurNonHeros = $couleurNonHeros;

        return $this;
    }

    public function getCouleurDebutant(): ?string
    {
        return $this->couleurDebutant;
    }

    public function setCouleurDebutant(string $couleurDebutant): static
    {
        $this->couleurDebutant = $couleurDebutant;

        return $this;
    }

    public function getCouleurApprenti(): ?string
    {
        return $this->couleurApprenti;
    }

    public function setCouleurApprenti(string $couleurApprenti): static
    {
        $this->couleurApprenti = $couleurApprenti;

        return $this;
    }

    public function getCouleurExpert(): ?string
    {
        return $this->couleurExpert;
    }

    public function setCouleurExpert(string $couleurExpert): static
    {
        $this->couleurExpert = $couleurExpert;

        return $this;
    }

    public function getCouleurElite(): ?string
    {
        return $this->couleurElite;
    }

    public function setCouleurElite(string $couleurElite): static
    {
        $this->couleurElite = $couleurElite;

        return $this;
    }
}
