<?php

namespace App\Entity;

use App\Doctrine\IdAlphaOutilsChantierGenerator;
use App\Repository\OutilsChantierRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OutilsChantierRepository::class)]
class OutilsChantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaOutilsChantierGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    private ?string $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['outils_chantier'])]
    private ?User $createdBy = null;
    
    #[ORM\Column(type: 'datetime')]
    #[Groups(['outils_chantier'])]
    private ?DateTimeInterface $createdAt = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[Groups(['outils_chantier'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['outils_chantier'])]
    private ?DateTimeInterface $modifyAt = null;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $ecoRessource = false;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $utilBDE = false;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $presScie = false;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrOuvrier = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrCampeur = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrMort = 0;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $rationEau = false;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $rationNourriture = false;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $typeNourriture = 6;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrMaxEau = 0;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['outils_chantier'])]
    private ?int $nbrMaxNourriture = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrAlcoolExpe = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrAlcoolVeille = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrDrogueExpe = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrDrogueVeille = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrDrogueAlcool = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrPALegendaire = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $nbrPAFao = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paChantier = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $defChantier = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paHabitation = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paReparationArme = 0;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $bricotVert = false;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['outils_chantier'])]
    private ?int $nbrKitVert = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paTransfo = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paTotal = 0;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paTdga = 0;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['outils_chantier'])]
    private ?int $lvlUp = null;
    
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantierup_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['outils_chantier'])]
    private ?ChantierPrototype $chantierUp = null;
    
    /** @var Collection<ProgrammeChantier> */
    #[ORM\OneToMany(mappedBy: 'outilsChantier', targetEntity: ProgrammeChantier::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'chantier_id')]
    #[Groups(['outils_chantier'])]
    private Collection $chantiersProgrammes;
    
    /** @var Collection<UpHabitation> */
    #[ORM\OneToMany(mappedBy: 'outilsChantier', targetEntity: UpHabitation::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['outils_chantier'])]
    private Collection $upHabitations;
    
    /** @var Collection<UpAmelio> */
    #[ORM\OneToMany(mappedBy: 'outilsChantier', targetEntity: UpAmelio::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['outils_chantier'])]
    private Collection $UpAmelios;
    
    /** @var Collection<EstimRessource> */
    #[ORM\OneToMany(mappedBy: 'outilsChantier', targetEntity: EstimRessource::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['outils_chantier'])]
    private Collection $estimRessources;
    
    /** @var Collection<RepaArme> */
    #[ORM\OneToMany(mappedBy: 'outilsChantier', targetEntity: RepaArme::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['outils_chantier'])]
    private Collection $repaArmes;
    
    #[ORM\OneToOne(mappedBy: 'outilsChantier', targetEntity: Outils::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private ?Outils $outils = null;
    
    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups(['outils_chantier'])]
    private ?int $defMaison = 0;
    
    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups(['outils_chantier'])]
    private ?int $defUpgrade = null;
    
    public function __construct()
    {
        $this->chantiersProgrammes = new ArrayCollection();
        $this->upHabitations       = new ArrayCollection();
        $this->UpAmelios           = new ArrayCollection();
        $this->estimRessources     = new ArrayCollection();
        $this->repaArmes           = new ArrayCollection();
    }
    
    public function addChantiersProgramme(ProgrammeChantier $chantiersProgramme): self
    {
        if (!$this->chantiersProgrammes->contains($chantiersProgramme)) {
            $this->chantiersProgrammes[$chantiersProgramme->getChantier()->getId()] = $chantiersProgramme;
            $chantiersProgramme->setOutilsChantier($this);
        }
        
        return $this;
    }
    
    public function addEstimRessource(EstimRessource $estimRessource): self
    {
        if (!$this->estimRessources->contains($estimRessource)) {
            $this->estimRessources[] = $estimRessource;
            $estimRessource->setOutilsChantier($this);
        }
        
        return $this;
    }
    
    public function addRepaArme(RepaArme $repaArme): self
    {
        if (!$this->repaArmes->contains($repaArme)) {
            $this->repaArmes[] = $repaArme;
            $repaArme->setOutilsChantier($this);
        }
        
        return $this;
    }
    
    public function addUpAmelio(UpAmelio $upAmelio): self
    {
        if (!$this->UpAmelios->contains($upAmelio)) {
            $this->UpAmelios[] = $upAmelio;
            $upAmelio->setOutilsChantier($this);
        }
        
        return $this;
    }
    
    public function addUpHabitation(UpHabitation $upHabitation): self
    {
        if (!$this->upHabitations->contains($upHabitation)) {
            $this->upHabitations[] = $upHabitation;
            $upHabitation->setOutilsChantier($this);
        }
        
        return $this;
    }
    
    public function getBricotVert(): ?bool
    {
        return $this->bricotVert;
    }
    
    public function setBricotVert(bool $bricotVert): self
    {
        $this->bricotVert = $bricotVert;
        
        return $this;
    }
    
    public function getChantierUp(): ?ChantierPrototype
    {
        return $this->chantierUp;
    }
    
    public function setChantierUp(?ChantierPrototype $chantierUp): self
    {
        $this->chantierUp = $chantierUp;
        
        return $this;
    }
    
    public function getChantiersProgrammes(): Collection
    {
        return $this->chantiersProgrammes;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getDefChantier(): ?int
    {
        return $this->defChantier;
    }
    
    public function setDefChantier(int $defChantier): self
    {
        $this->defChantier = $defChantier;
        
        return $this;
    }
    
    public function getDefMaison(): ?int
    {
        return $this->defMaison;
    }
    
    public function setDefMaison(int $defMaison): static
    {
        $this->defMaison = $defMaison;
        
        return $this;
    }
    
    public function getDefUpgrade(): ?int
    {
        return $this->defUpgrade;
    }
    
    public function setDefUpgrade(int $defUpgrade): static
    {
        $this->defUpgrade = $defUpgrade;
        
        return $this;
    }
    
    public function getEcoRessource(): ?bool
    {
        return $this->ecoRessource;
    }
    
    public function setEcoRessource(bool $ecoRessource): self
    {
        $this->ecoRessource = $ecoRessource;
        
        return $this;
    }
    
    public function getEstimRessources(): Collection
    {
        return $this->estimRessources;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?string $id): OutilsChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getLvlUp(): ?int
    {
        return $this->lvlUp;
    }
    
    public function setLvlUp(?int $lvlUp): self
    {
        $this->lvlUp = $lvlUp;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->modifyAt;
    }
    
    public function setModifyAt(?DateTimeInterface $modifyAt): self
    {
        $this->modifyAt = $modifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): self
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getNbrAlcoolExpe(): ?int
    {
        return $this->nbrAlcoolExpe;
    }
    
    public function setNbrAlcoolExpe(int $nbrAlcoolExpe): self
    {
        $this->nbrAlcoolExpe = $nbrAlcoolExpe;
        
        return $this;
    }
    
    public function getNbrAlcoolVeille(): ?int
    {
        return $this->nbrAlcoolVeille;
    }
    
    public function setNbrAlcoolVeille(int $nbrAlcoolVeille): self
    {
        $this->nbrAlcoolVeille = $nbrAlcoolVeille;
        
        return $this;
    }
    
    public function getNbrCampeur(): ?int
    {
        return $this->nbrCampeur;
    }
    
    public function setNbrCampeur(int $nbrCampeur): self
    {
        $this->nbrCampeur = $nbrCampeur;
        
        return $this;
    }
    
    public function getNbrDrogueAlcool(): ?int
    {
        return $this->nbrDrogueAlcool;
    }
    
    public function setNbrDrogueAlcool(int $nbrDrogueAlcool): self
    {
        $this->nbrDrogueAlcool = $nbrDrogueAlcool;
        
        return $this;
    }
    
    public function getNbrDrogueExpe(): ?int
    {
        return $this->nbrDrogueExpe;
    }
    
    public function setNbrDrogueExpe(int $nbrDrogueExpe): self
    {
        $this->nbrDrogueExpe = $nbrDrogueExpe;
        
        return $this;
    }
    
    public function getNbrDrogueVeille(): ?int
    {
        return $this->nbrDrogueVeille;
    }
    
    public function setNbrDrogueVeille(int $nbrDrogueVeille): self
    {
        $this->nbrDrogueVeille = $nbrDrogueVeille;
        
        return $this;
    }
    
    public function getNbrKitVert(): ?int
    {
        return $this->nbrKitVert;
    }
    
    public function setNbrKitVert(?int $nbrKitVert): self
    {
        $this->nbrKitVert = $nbrKitVert;
        
        return $this;
    }
    
    public function getNbrMaxEau(): ?int
    {
        return $this->nbrMaxEau;
    }
    
    public function setNbrMaxEau(int $nbrMaxEau): self
    {
        $this->nbrMaxEau = $nbrMaxEau;
        
        return $this;
    }
    
    public function getNbrMaxNourriture(): ?int
    {
        return $this->nbrMaxNourriture;
    }
    
    public function setNbrMaxNourriture(?int $nbrMaxNourriture): self
    {
        $this->nbrMaxNourriture = $nbrMaxNourriture;
        
        return $this;
    }
    
    public function getNbrMort(): ?int
    {
        return $this->nbrMort;
    }
    
    public function setNbrMort(int $nbrMort): self
    {
        $this->nbrMort = $nbrMort;
        
        return $this;
    }
    
    public function getNbrOuvrier(): ?int
    {
        return $this->nbrOuvrier;
    }
    
    public function setNbrOuvrier(int $nbrOuvrier): self
    {
        $this->nbrOuvrier = $nbrOuvrier;
        
        return $this;
    }
    
    public function getNbrPAFao(): ?int
    {
        return $this->nbrPAFao;
    }
    
    public function setNbrPAFao(int $nbrPAFao): self
    {
        $this->nbrPAFao = $nbrPAFao;
        
        return $this;
    }
    
    public function getNbrPALegendaire(): ?int
    {
        return $this->nbrPALegendaire;
    }
    
    public function setNbrPALegendaire(int $nbrPALegendaire): self
    {
        $this->nbrPALegendaire = $nbrPALegendaire;
        
        return $this;
    }
    
    public function getOutils(): ?Outils
    {
        return $this->outils;
    }
    
    public function setOutils(?Outils $outils): self
    {
        // unset the owning side of the relation if necessary
        if ($outils === null && $this->outils !== null) {
            $this->outils->setOutilsChantier(null);
        }
        
        // set the owning side of the relation if necessary
        if ($outils !== null && $outils->getOutilsChantier() !== $this) {
            $outils->setOutilsChantier($this);
        }
        
        $this->outils = $outils;
        
        return $this;
    }
    
    public function getPaChantier(): ?int
    {
        return $this->paChantier;
    }
    
    public function setPaChantier(int $paChantier): self
    {
        $this->paChantier = $paChantier;
        
        return $this;
    }
    
    public function getPaHabitation(): ?int
    {
        return $this->paHabitation;
    }
    
    public function setPaHabitation(int $paHabitation): self
    {
        $this->paHabitation = $paHabitation;
        
        return $this;
    }
    
    public function getPaReparationArme(): ?int
    {
        return $this->paReparationArme;
    }
    
    public function setPaReparationArme(int $paReparationArme): self
    {
        $this->paReparationArme = $paReparationArme;
        
        return $this;
    }
    
    public function getPaTdga(): ?int
    {
        return $this->paTdga;
    }
    
    public function setPaTdga(int $paTdga): self
    {
        $this->paTdga = $paTdga;
        
        return $this;
    }
    
    public function getPaTotal(): ?int
    {
        return $this->paTotal;
    }
    
    public function setPaTotal(int $paTotal): self
    {
        $this->paTotal = $paTotal;
        
        return $this;
    }
    
    public function getPaTransfo(): ?int
    {
        return $this->paTransfo;
    }
    
    public function setPaTransfo(int $paTransfo): self
    {
        $this->paTransfo = $paTransfo;
        
        return $this;
    }
    
    public function getPresScie(): ?bool
    {
        return $this->presScie;
    }
    
    public function setPresScie(bool $presScie): self
    {
        $this->presScie = $presScie;
        
        return $this;
    }
    
    public function getRationEau(): ?bool
    {
        return $this->rationEau;
    }
    
    public function setRationEau(bool $rationEau): self
    {
        $this->rationEau = $rationEau;
        
        return $this;
    }
    
    public function getRationNourriture(): ?bool
    {
        return $this->rationNourriture;
    }
    
    public function setRationNourriture(bool $rationNourriture): self
    {
        $this->rationNourriture = $rationNourriture;
        
        return $this;
    }
    
    public function getRepaArmes(): Collection
    {
        return $this->repaArmes;
    }
    
    public function getTypeNourriture(): ?int
    {
        return $this->typeNourriture;
    }
    
    public function setTypeNourriture(int $typeNourriture): self
    {
        $this->typeNourriture = $typeNourriture;
        
        return $this;
    }
    
    public function getUpAmelios(): Collection
    {
        return $this->UpAmelios;
    }
    
    public function getUpHabitations(): Collection
    {
        return $this->upHabitations;
    }
    
    public function getUtilBDE(): ?bool
    {
        return $this->utilBDE;
    }
    
    public function setUtilBDE(bool $utilBDE): self
    {
        $this->utilBDE = $utilBDE;
        
        return $this;
    }
    
    public function removeChantiersProgramme(ProgrammeChantier $chantiersProgramme): self
    {
        if ($this->chantiersProgrammes->removeElement($chantiersProgramme)) {
            // set the owning side to null (unless already changed)
            if ($chantiersProgramme->getOutilsChantier() === $this) {
                $chantiersProgramme->setOutilsChantier(null);
            }
        }
        
        return $this;
    }
    
    public function removeEstimRessource(EstimRessource $estimRessource): self
    {
        if ($this->estimRessources->removeElement($estimRessource)) {
            // set the owning side to null (unless already changed)
            if ($estimRessource->getOutilsChantier() === $this) {
                $estimRessource->setOutilsChantier(null);
            }
        }
        
        return $this;
    }
    
    public function removeRepaArme(RepaArme $repaArme): self
    {
        if ($this->repaArmes->removeElement($repaArme)) {
            // set the owning side to null (unless already changed)
            if ($repaArme->getOutilsChantier() === $this) {
                $repaArme->setOutilsChantier(null);
            }
        }
        
        return $this;
    }
    
    public function removeUpAmelio(UpAmelio $upAmelio): self
    {
        if ($this->UpAmelios->removeElement($upAmelio)) {
            // set the owning side to null (unless already changed)
            if ($upAmelio->getOutilsChantier() === $this) {
                $upAmelio->setOutilsChantier(null);
            }
        }
        
        return $this;
    }
    
    public function removeUpHabitation(UpHabitation $upHabitation): self
    {
        if ($this->upHabitations->removeElement($upHabitation)) {
            // set the owning side to null (unless already changed)
            if ($upHabitation->getOutilsChantier() === $this) {
                $upHabitation->setOutilsChantier(null);
            }
        }
        
        return $this;
    }
}