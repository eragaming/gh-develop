<?php

namespace App\Entity;

use App\Repository\PictosRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\MaxDepth;

#[ORM\Entity(repositoryClass: PictosRepository::class)]
class Pictos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['picto', 'classement'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER', inversedBy: 'pictos')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['classement']), MaxDepth(1)]
    private ?User $user = null;
    
    #[ORM\Column(type: 'integer')]
    #[Groups(['picto', 'classement'])]
    private ?int $nombre = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: PictoPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'pictos')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['picto'])]
    private ?PictoPrototype $picto = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getPicto(): ?PictoPrototype
    {
        return $this->picto;
    }
    
    public function setPicto(?PictoPrototype $picto): self
    {
        $this->picto = $picto;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
    
}
