<?php

namespace App\Entity;

use App\Repository\DispoJumpRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: DispoJumpRepository::class)]
class DispoJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['coalition'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeDispoJump::class, fetch: 'EXTRA_LAZY'), Groups(['coalition'])]
    private ?TypeDispoJump $choixDispo = null;
    
    #[ORM\ManyToOne(targetEntity: Coalition::class, fetch: 'EXTRA_LAZY', inversedBy: 'dispos')]
    #[ORM\JoinColumn(name: 'coas_id', referencedColumnName: 'id', nullable: false)]
    private ?Coalition $coalition = null;
    
    #[ORM\ManyToOne(targetEntity: CreneauJump::class), Groups(['coalition'])]
    private ?CreneauJump $creneauJump = null;
    
    public function __construct()
    {
    }
    
    public function getChoixDispo(): ?TypeDispoJump
    {
        return $this->choixDispo;
    }
    
    public function setChoixDispo(?TypeDispoJump $choixDispo): self
    {
        $this->choixDispo = $choixDispo;
        
        return $this;
    }
    
    public function getCoalition(): ?Coalition
    {
        return $this->coalition;
    }
    
    public function setCoalition(?Coalition $coalition): self
    {
        $this->coalition = $coalition;
        
        return $this;
    }
    
    public function getCreneauJump(): ?CreneauJump
    {
        return $this->creneauJump;
    }
    
    public function setCreneauJump(?CreneauJump $creneauJump): DispoJump
    {
        $this->creneauJump = $creneauJump;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): DispoJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    
}
