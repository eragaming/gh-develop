<?php

namespace App\Entity;

use App\Repository\HerosSkillPrototypeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: HerosSkillPrototypeRepository::class)]
class HerosSkillPrototype
{
    #[ORM\Id]
    #[ORM\Column]
    #[Groups(['admin_heros_detail', 'ency','option','citoyens','candidature_jump','jump', 'coalition'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(inversedBy: 'herosSkillPrototypes')]
    #[Groups(['admin_heros_detail', 'ency','option','citoyens','candidature_jump','jump', 'coalition'])]
    private ?HerosPrototype $heros = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['admin_heros_detail', 'ency','option','citoyens','candidature_jump','jump', 'coalition'])]
    private ?int $value = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['admin_heros_detail', 'ency','option','citoyens','candidature_jump','jump', 'coalition'])]
    private ?int $value2 = null;
    
    #[ORM\Column(length: 255)]
    #[Groups(['admin_heros_detail', 'ency','option','citoyens','candidature_jump','jump', 'coalition'])]
    private ?string $text = null;
    
    #[ORM\ManyToOne(inversedBy: 'pouvoir')]
    #[ORM\JoinColumn(nullable: false)]
    private ?HerosSkillLevel $herosSkillLevel = null;
    
    public function getHeros(): ?HerosPrototype
    {
        return $this->heros;
    }
    
    public function setHeros(?HerosPrototype $heros): static
    {
        $this->heros = $heros;
        
        return $this;
    }
    
    public function getHerosSkillLevel(): ?HerosSkillLevel
    {
        return $this->herosSkillLevel;
    }
    
    public function setHerosSkillLevel(?HerosSkillLevel $herosSkillLevel): static
    {
        $this->herosSkillLevel = $herosSkillLevel;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): HerosSkillPrototype
    {
        $this->id = $id;
        return $this;
    }
    
    public function getText(): ?string
    {
        return $this->text;
    }
    
    public function setText(string $text): static
    {
        $this->text = $text;
        
        return $this;
    }
    
    public function getValue(): ?int
    {
        return $this->value;
    }
    
    public function setValue(?int $value): static
    {
        $this->value = $value;
        
        return $this;
    }
    
    public function getValue2(): ?int
    {
        return $this->value2;
    }
    
    public function setValue2(?int $value2): static
    {
        $this->value2 = $value2;
        
        return $this;
    }
}
