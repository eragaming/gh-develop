<?php

namespace App\Entity;

use App\Repository\InscriptionJumpRepository;
use App\Service\GestHordesHandler;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InscriptionJumpRepository::class)]
#[Assert\GroupSequence(['InscriptionJump', 'register'])]
#[ORM\Index(columns: ['user_id'])]
class InscriptionJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'),
        Groups(['coalition', 'gestion_jump', 'gestion_event', 'inscription_jump', 'creation_ins', 'candidature_jump', 'gestion_candidature', 'ins_event'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY', inversedBy: 'inscriptionJumps'),
        Groups(['event_inscription', 'coalition', 'gestion_jump', 'gestion_event', 'inscription_jump', 'creation_ins',
                'candidature_jump', 'ins_event'])]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?User $user = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY', inversedBy: 'inscriptionJumps')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jump $Jump = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: HerosPrototype::class, fetch: 'EXTRA_LAZY'),
        Groups(['coalition', 'gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?HerosPrototype $pouvoirFutur = null;
    
    #[ORM\Column(type: 'boolean'),
        Groups(['coalition', 'gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?bool $banCommu = false;
    
    #[ORM\Column(type: 'boolean'),
        Groups(['coalition', 'gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?bool $gouleCommu = false;
    
    #[ORM\Column(type: 'text', nullable: true),
        Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?string $motivations = null;
    
    #[ORM\Column(type: 'text', nullable: true),
        Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?string $commentaires = null;
    
    #[ORM\Column(type: 'text', nullable: true), Groups(['gestion_jump', 'candidature_jump', 'gestion_candidature'])]
    private ?string $blocNotes = null;
    
    #[ORM\Column(type: 'text', nullable: true),
        Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'gestion_candidature', 'ins_event'])]
    private ?string $reponse = null;
    
    /** @var Collection<RoleJump> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'inscription_role')]
    #[ORM\JoinColumn(name: 'inscription_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'role_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: RoleJump::class, fetch: 'EXTRA_LAZY'),
        Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private Collection $roleJoueurJumps;
    
    /** @var Collection<LeadInscription> */
    #[ORM\OneToMany(mappedBy: 'inscriptionJump', targetEntity: LeadInscription::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true),
        Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private Collection $leadInscriptions;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: StatutInscription::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false),
        Groups(['list_jump', 'event_inscription', 'coalition', 'gestion_jump', 'gestion_event', 'inscription_jump',
                'creation_ins', 'candidature_jump', 'gestion_candidature', 'ins_event'])]
    private ?StatutInscription $statut = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: LevelRuinePrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['gestion_jump', 'inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    #[Assert\NotNull(message: 'Il faut obligatoirement choisir un niveau de ruine', groups: ['register'])]
    private ?LevelRuinePrototype $lvlRuine = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['gestion_jump', 'inscription_jump', 'candidature_jump'])]
    private ?DateTimeInterface $dateInscription = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['gestion_jump', 'inscription_jump', 'candidature_jump'])]
    private ?DateTimeInterface $dateModification = null;
    
    #[ORM\Column(type: 'text', nullable: true), Groups(['inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?string $moyenContact = null;
    
    /**
     * 1 → uniquement lead/orga
     * 0 → tout le monde
     */
    #[ORM\Column(type: 'smallint'), Groups(['inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?int $typeDiffusion = 1;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: JobPrototype::class, fetch: 'EXTRA_LAZY'),
        Groups(['event_inscription', 'coalition', 'gestion_jump', 'inscription_jump', 'creation_ins',
                'candidature_jump', 'ins_event'])]
    #[Assert\NotNull(message: 'Il faut obligatoirement choisir un métier', groups: ['register'])]
    private ?JobPrototype $voeuxMetier1 = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: JobPrototype::class, fetch: 'EXTRA_LAZY'),
        Groups(['event_inscription', 'coalition', 'gestion_jump', 'inscription_jump', 'creation_ins',
                'candidature_jump', 'ins_event'])]
    private ?JobPrototype $voeuxMetier2 = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: JobPrototype::class, fetch: 'EXTRA_LAZY'),
        Groups(['event_inscription', 'coalition', 'gestion_jump', 'inscription_jump', 'creation_ins',
                'candidature_jump', 'ins_event'])]
    private ?JobPrototype $voeuxMetier3 = null;
    
    /** @var Collection<DisponibiliteJoueurs> */
    #[ORM\OneToMany(mappedBy: 'inscriptionJump', targetEntity: DisponibiliteJoueurs::class,
        cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true),
        Groups(['inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private Collection $dispo;
    
    /**
     * @var null|DisponibiliteJoueurs[]
     */
    private ?array $dispoArray = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: JobPrototype::class), Groups(['coalition'])]
    private ?JobPrototype $metierDef = null;
    
    /** @var Collection<LogEventInscription> */
    #[ORM\OneToMany(mappedBy: 'inscription', targetEntity: LogEventInscription::class, cascade: ['persist', 'remove'],
        orphanRemoval: true), Groups(['inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private Collection $logEventInscriptions;
    
    #[ORM\Column(length: 255, nullable: false, options: ['default' => 'Europe/Paris']),
        Groups(['inscription_jump', 'creation_ins', 'candidature_jump', 'ins_event'])]
    private ?string $fuseau = 'Europe/Paris';

    /**
     * @var Collection<int, HerosSkillLevel>
     */
    #[ORM\ManyToMany(targetEntity: HerosSkillLevel::class),
        Groups(['inscription_jump', 'coalition'])]
    private Collection $skill;
    
    public function __construct()
    {
        $this->roleJoueurJumps      = new ArrayCollection();
        $this->leadInscriptions     = new ArrayCollection();
        $this->dispo                = new ArrayCollection();
        $this->logEventInscriptions = new ArrayCollection();
        $this->skill = new ArrayCollection();
    }
    
    public function addDispo(DisponibiliteJoueurs $dispo): self
    {
        if (!$this->dispo->contains($dispo)) {
            $this->dispo[] = $dispo;
            $dispo->setInscriptionJump($this);
        }
        
        return $this;
    }
    
    public function addLeadInscription(LeadInscription $leadInscription): self
    {
        if (!$this->leadInscriptions->contains($leadInscription)) {
            $this->leadInscriptions[] = $leadInscription;
            $leadInscription->setInscriptionJump($this);
        }
        
        return $this;
    }
    
    public function addLogEventInscription(LogEventInscription $logEventInscription): static
    {
        if (!$this->logEventInscriptions->contains($logEventInscription)) {
            $this->logEventInscriptions->add($logEventInscription);
            $logEventInscription->setInscription($this);
        }
        
        return $this;
    }
    
    public function addRoleJoueurJump(RoleJump $roleJoueurJump): self
    {
        if (!$this->roleJoueurJumps->contains($roleJoueurJump)) {
            $this->roleJoueurJumps[] = $roleJoueurJump;
        }
        
        return $this;
    }
    
    public function dechiffreMoyenContact(GestHordesHandler $gh): string
    {
        
        if ($this->getMoyenContact() === null || $this->getMoyenContact() === "") {
            return "";
        }
        
        openssl_private_decrypt(base64_decode($this->getMoyenContact()), $moyenContact, $gh->getPrivateKey());
        
        return $moyenContact;
    }
    
    public function getBanCommu(): ?bool
    {
        return $this->banCommu;
    }
    
    public function setBanCommu(bool $banCommu): self
    {
        $this->banCommu = $banCommu;
        
        return $this;
    }
    
    public function getBlocNotes(): ?string
    {
        return $this->blocNotes;
    }
    
    public function setBlocNotes(?string $blocNotes): self
    {
        $this->blocNotes = $blocNotes;
        
        return $this;
    }
    
    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }
    
    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;
        
        return $this;
    }
    
    public function getDateInscription(): ?DateTimeInterface
    {
        return $this->dateInscription;
    }
    
    public function setDateInscription(DateTimeInterface $dateInscription): self
    {
        $this->dateInscription = $dateInscription;
        
        return $this;
    }
    
    public function getDateModification(): ?DateTimeInterface
    {
        return $this->dateModification;
    }
    
    public function setDateModification(?DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;
        
        return $this;
    }
    
    /**
     * @return Collection<int, DisponibiliteJoueurs>
     */
    public function getDispo(): Collection
    {
        return $this->dispo;
    }
    
    public function getDispoId(CreneauHorraire $creneauHorraire, int $typeHorraire): DisponibiliteJoueurs
    {
        if ($this->dispoArray === null) {
            $this->tranfoCollecToArray();
        }
        
        return $this->dispoArray[$creneauHorraire->getId() . '-' . $typeHorraire];
    }
    
    public function getFuseau(): ?string
    {
        return $this->fuseau;
    }
    
    public function setFuseau(string $fuseau): static
    {
        $this->fuseau = $fuseau;
        
        return $this;
    }
    
    public function getGouleCommu(): ?bool
    {
        return $this->gouleCommu;
    }
    
    public function setGouleCommu(bool $gouleCommu): self
    {
        $this->gouleCommu = $gouleCommu;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): InscriptionJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->Jump;
    }
    
    public function setJump(?Jump $Jump): self
    {
        $this->Jump = $Jump;
        
        return $this;
    }
    
    /**
     * @return Collection<int, LeadInscription>
     */
    public function getLeadInscriptions(): Collection
    {
        return $this->leadInscriptions;
    }
    
    /**
     * @return Collection<int, LogEventInscription>
     */
    public function getLogEventInscriptions(): Collection
    {
        return $this->logEventInscriptions;
    }
    
    public function getLvlRuine(): ?LevelRuinePrototype
    {
        return $this->lvlRuine;
    }
    
    public function setLvlRuine(?LevelRuinePrototype $lvlRuine): self
    {
        $this->lvlRuine = $lvlRuine;
        
        return $this;
    }
    
    public function getMetierDef(): ?JobPrototype
    {
        return $this->metierDef;
    }
    
    public function setMetierDef(?JobPrototype $metierDef): self
    {
        $this->metierDef = $metierDef;
        
        return $this;
    }
    
    public function getMotivations(): ?string
    {
        return $this->motivations;
    }
    
    public function setMotivations(?string $motivations): self
    {
        $this->motivations = $motivations;
        
        return $this;
    }
    
    public function getMoyenContact(): ?string
    {
        return $this->moyenContact;
    }
    
    public function setMoyenContact(?string $moyenContact): self
    {
        $this->moyenContact = $moyenContact;
        
        return $this;
    }
    
    public function getPouvoirFutur(): ?HerosPrototype
    {
        return $this->pouvoirFutur;
    }
    
    public function setPouvoirFutur(?HerosPrototype $pouvoirFutur): self
    {
        $this->pouvoirFutur = $pouvoirFutur;
        
        return $this;
    }
    
    public function getReponse(): ?string
    {
        return $this->reponse;
    }
    
    public function setReponse(?string $reponse): self
    {
        $this->reponse = $reponse;
        
        return $this;
    }
    
    /**
     * @return Collection<int, RoleJump>
     */
    public function getRoleJoueurJumps(): Collection
    {
        return $this->roleJoueurJumps;
    }
    
    public function getStatut(): ?StatutInscription
    {
        return $this->statut;
    }
    
    public function setStatut(?StatutInscription $statut): self
    {
        $this->statut = $statut;
        
        return $this;
    }
    
    public function getTypeDiffusion(): ?int
    {
        return $this->typeDiffusion;
    }
    
    public function setTypeDiffusion(int $typeDiffusion): self
    {
        $this->typeDiffusion = $typeDiffusion;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): self
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function getVoeuxMetier1(): ?JobPrototype
    {
        return $this->voeuxMetier1;
    }
    
    public function setVoeuxMetier1(?JobPrototype $voeuxMetier1): self
    {
        $this->voeuxMetier1 = $voeuxMetier1;
        
        return $this;
    }
    
    public function getVoeuxMetier2(): ?JobPrototype
    {
        return $this->voeuxMetier2;
    }
    
    public function setVoeuxMetier2(?JobPrototype $voeuxMetier2): self
    {
        $this->voeuxMetier2 = $voeuxMetier2;
        
        return $this;
    }
    
    public function getVoeuxMetier3(): ?JobPrototype
    {
        return $this->voeuxMetier3;
    }
    
    public function setVoeuxMetier3(?JobPrototype $voeuxMetier3): self
    {
        $this->voeuxMetier3 = $voeuxMetier3;
        
        return $this;
    }
    
    public function removeDispo(DisponibiliteJoueurs $dispo): self
    {
        if ($this->dispo->removeElement($dispo)) {
            // set the owning side to null (unless already changed)
            if ($dispo->getInscriptionJump() === $this) {
                $dispo->setInscriptionJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeLeadInscription(LeadInscription $leadInscription): self
    {
        if ($this->leadInscriptions->removeElement($leadInscription)) {
            // set the owning side to null (unless already changed)
            if ($leadInscription->getInscriptionJump() === $this) {
                $leadInscription->setInscriptionJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeLogEventInscription(LogEventInscription $logEventInscription): static
    {
        if ($this->logEventInscriptions->removeElement($logEventInscription)) {
            // set the owning side to null (unless already changed)
            if ($logEventInscription->getInscription() === $this) {
                $logEventInscription->setInscription(null);
            }
        }
        
        return $this;
    }
    
    public function removeRoleJoueurJump(RoleJump $roleJoueurJump): self
    {
        $this->roleJoueurJumps->removeElement($roleJoueurJump);
        
        return $this;
    }
    
    private function tranfoCollecToArray(): void
    {
        
        $dispo = $this->dispo->toArray();
        
        $this->dispoArray = array_combine(
            array_map(fn(DisponibiliteJoueurs $dispo) => $dispo->getCreneau()->getId() . '-' . $dispo->getTypeCreneau(),
                $dispo),
            $dispo,
        );
    }

    /**
     * @return Collection<int, HerosSkillLevel>
     */
    public function getSkill(): Collection
    {
        return $this->skill;
    }

    public function addSkill(HerosSkillLevel $skill): static
    {
        if (!$this->skill->contains($skill)) {
            $this->skill->add($skill);
        }

        return $this;
    }

    public function removeSkill(HerosSkillLevel $skill): static
    {
        $this->skill->removeElement($skill);

        return $this;
    }
}
