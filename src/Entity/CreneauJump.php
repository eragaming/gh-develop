<?php

namespace App\Entity;

use App\Repository\CreneauJumpRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CreneauJumpRepository::class)]
class CreneauJump
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['coalition'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['coalition'])]
    private ?DateTime $dateCreneau = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: CreneauHorraire::class, fetch: 'EXTRA_LAZY'), Groups(['coalition'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?CreneauHorraire $creneauHorraire = null;
    
    #[ORM\ManyToOne(targetEntity: Jump::class, fetch: 'EXTRA_LAZY', inversedBy: 'creneau')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jump $jump = null;
    
    public function getCreneauHorraire(): ?CreneauHorraire
    {
        return $this->creneauHorraire;
    }
    
    public function setCreneauHorraire(?CreneauHorraire $creneauHorraire): self
    {
        $this->creneauHorraire = $creneauHorraire;
        
        return $this;
    }
    
    public function getDateCreneau(): ?DateTime
    {
        return $this->dateCreneau;
    }
    
    public function setDateCreneau(DateTime $dateCreneau): self
    {
        $this->dateCreneau = $dateCreneau;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): CreneauJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJump(): ?Jump
    {
        return $this->jump;
    }
    
    public function setJump(?Jump $jump): self
    {
        $this->jump = $jump;
        
        return $this;
    }
}
