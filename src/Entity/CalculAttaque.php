<?php

namespace App\Entity;

use App\Repository\CalculAttaqueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CalculAttaqueRepository::class)]
class CalculAttaque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'calculAttaques')]
    private ?Ville $ville = null;
    
    #[ORM\Column(name: 'day', type: 'smallint')]
    private ?int $day = null;
    
    #[ORM\Column(type: 'float')]
    private ?float $margeEstim = null;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $atkEstim = null;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $atkEstimMarge = null;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $atkVisee = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $ajustFeux = null;
    
    
    public function getAjustFeux(): ?bool
    {
        return $this->ajustFeux;
    }
    
    public function setAjustFeux(?bool $ajustFeux): self
    {
        $this->ajustFeux = $ajustFeux;
        
        return $this;
    }
    
    public function getAtkEstim(): ?int
    {
        return $this->atkEstim;
    }
    
    public function setAtkEstim(?int $atkEstim): self
    {
        $this->atkEstim = $atkEstim;
        
        return $this;
    }
    
    public function getAtkEstimMarge(): ?int
    {
        return $this->atkEstimMarge;
    }
    
    public function setAtkEstimMarge(int $atkEstimMarge): self
    {
        $this->atkEstimMarge = $atkEstimMarge;
        
        return $this;
    }
    
    public function getAtkVisee(): ?int
    {
        return $this->atkVisee;
    }
    
    public function setAtkVisee(int $atkVisee): self
    {
        $this->atkVisee = $atkVisee;
        
        return $this;
    }
    
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): CalculAttaque
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMargeEstim(): ?float
    {
        return $this->margeEstim;
    }
    
    public function setMargeEstim(float $margeEstim): self
    {
        $this->margeEstim = $margeEstim;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
}
