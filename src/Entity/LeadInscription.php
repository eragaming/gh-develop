<?php

namespace App\Entity;

use App\Repository\LeadInscriptionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LeadInscriptionRepository::class)]
class LeadInscription
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['gestion_jump', 'creation_ins', 'inscription_jump', 'candidature_jump'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeLead::class, fetch: 'EXTRA_LAZY'), Groups(['gestion_jump', 'creation_ins', 'inscription_jump', 'candidature_jump'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeLead $lead = null;
    
    #[ORM\ManyToOne(targetEntity: InscriptionJump::class, fetch: 'EXTRA_LAZY', inversedBy: 'leadInscriptions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?InscriptionJump $inscriptionJump = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['gestion_jump', 'creation_ins', 'inscription_jump', 'candidature_jump'])]
    private ?bool $apprenti = null;
    
    public function getApprenti(): ?bool
    {
        return $this->apprenti;
    }
    
    public function setApprenti(bool $apprenti): self
    {
        $this->apprenti = $apprenti;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): LeadInscription
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getInscriptionJump(): ?InscriptionJump
    {
        return $this->inscriptionJump;
    }
    
    public function setInscriptionJump(?InscriptionJump $inscriptionJump): self
    {
        $this->inscriptionJump = $inscriptionJump;
        
        return $this;
    }
    
    public function getLead(): ?TypeLead
    {
        return $this->lead;
    }
    
    public function setLead(?TypeLead $lead): self
    {
        $this->lead = $lead;
        
        return $this;
    }
    
}
