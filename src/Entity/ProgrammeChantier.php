<?php

namespace App\Entity;

use App\Repository\ProgrammeChantierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ProgrammeChantierRepository::class)]
class ProgrammeChantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['outils_chantier'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['outils_chantier'])]
    private ?ChantierPrototype $chantier = null;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $bde = null;
    
    #[ORM\Column(type: 'boolean')]
    #[Groups(['outils_chantier'])]
    private ?bool $finir;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $paALaisser = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['outils_chantier'])]
    private ?int $priority = null;
    
    #[ORM\ManyToOne(targetEntity: OutilsChantier::class, fetch: 'EXTRA_LAZY', inversedBy: 'chantiersProgrammes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OutilsChantier $outilsChantier = null;
    
    #[ORM\Column(length: 50), Groups(['outils_chantier'])]
    private ?string $uuid = null;
    
    public function getBde(): ?bool
    {
        return $this->bde;
    }
    
    public function setBde(bool $bde): self
    {
        $this->bde = $bde;
        
        return $this;
    }
    
    public function getChantier(): ?ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function setChantier(?ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ProgrammeChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getOutilsChantier(): ?OutilsChantier
    {
        return $this->outilsChantier;
    }
    
    public function setOutilsChantier(?OutilsChantier $outilsChantier): self
    {
        $this->outilsChantier = $outilsChantier;
        
        return $this;
    }
    
    public function getPaALaisser(): ?int
    {
        return $this->paALaisser;
    }
    
    public function setPaALaisser(int $paALaisser): self
    {
        $this->paALaisser = $paALaisser;
        
        return $this;
    }
    
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;
        
        return $this;
    }
    
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
    
    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;
        
        return $this;
    }
    
    public function isFinir(): bool
    {
        return $this->finir;
    }
    
    public function setFinir(bool $finir): self
    {
        $this->finir = $finir;
        
        return $this;
    }
}
