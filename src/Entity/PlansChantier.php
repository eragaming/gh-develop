<?php

namespace App\Entity;

use App\Repository\PlansChantierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: PlansChantierRepository::class)]
class PlansChantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'plansChantiers')]
    private ?Ville $ville = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['plan', 'comparatif'])]
    private ?ChantierPrototype $chantier = null;
    
    public function getChantier(): ?ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function setChantier(?ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): PlansChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
}
