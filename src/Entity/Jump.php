<?php

namespace App\Entity;

use App\Doctrine\IdAlphaJumpGenerator;
use App\Repository\JumpRepository;
use App\Structures\Collection\GestionJumps;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: JumpRepository::class)]
class Jump
{
    public const ORGA      = "ORGA";
    public const GEST      = "GEST";
    public const ORGA_GEST = "ORGAGEST";
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaJumpGenerator::class)]
    #[ORM\Column(type: 'string', length: 24),
        Groups(['list_jump', 'creation_event', 'event_inscription', 'list_coa', 'coalition', 'gestion_event',
                'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?string $id = null;
    
    #[ORM\Column(type: 'string', length: 255),
        Groups(['list_jump', 'creation_jump', 'creation_event', 'event_inscription', 'list_coa', 'coalition',
                'gestion_event', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?string $nom = '';
    
    #[ORM\Column(type: 'datetime'), Groups(['list_jump', 'creation_jump', 'gestion_event', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateDebInscription = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['list_jump', 'creation_jump', 'gestion_event', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateFinInscription = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['list_jump', 'creation_jump', 'list_coa', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateApproxJump = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['creation_jump', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?bool $villePrive = false;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['list_jump', 'creation_jump', 'gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateJumpEffectif = null;
    
    #[ORM\OneToOne(inversedBy: 'jump', targetEntity: Ville::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'ville_id', referencedColumnName: 'id', nullable: true), Groups(['gestion_jump_spe','event_inscription'])]
    private ?Ville $ville = null;
    
    /** @var Collection<ObjectifVillePrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'jump_objectif')]
    #[ORM\JoinColumn(name: 'jump_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'objectif_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: ObjectifVillePrototype::class, cascade: ['persist'], fetch: 'EXTRA_LAZY'),
        Groups(['list_jump', 'creation_jump', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private Collection $objectif;
    
    /** @var Collection<LeadJump> */
    #[ORM\OneToMany(mappedBy: 'jump', targetEntity: LeadJump::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true), Groups(['gestion_jump', 'admin_jump'])]
    private Collection $lead;
    
    /** @var Collection<InscriptionJump> */
    #[ORM\OneToMany(mappedBy: 'Jump', targetEntity: InscriptionJump::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'user_id'),
        Groups(['event_inscription', 'coalition', 'gestion_jump', 'gestion_event'])]
    private Collection $inscriptionJumps;
    
    #[ORM\Column(type: 'datetime'), Groups(['gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateCreation = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?DateTimeInterface $dateModif = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY', inversedBy: 'jumps')]
    #[ORM\JoinColumn(nullable: false), Groups(['gestion_jump', 'admin_jump'])]
    private ?User $createdBy = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: TypeVille::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['list_jump', 'creation_jump', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?TypeVille $typeVille = null;
    
    /** @var Collection<CreneauHorraire> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'jump_creneau_semaine')]
    #[ORM\JoinColumn(name: 'jump_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'creneau_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: CreneauHorraire::class, cascade: ['persist'],
        fetch: 'EXTRA_LAZY'), Groups(['gestion_jump', 'inscription_jump'])]
    private Collection $creneauSemaine;
    
    /** @var Collection<CreneauHorraire> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'jump_creneau_weekend')]
    #[ORM\JoinColumn(name: 'jump_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'creneau_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: CreneauHorraire::class, cascade: ['persist'],
        fetch: 'EXTRA_LAZY'), Groups(['gestion_jump', 'inscription_jump'])]
    private Collection $CreneauWeekend;
    
    /** @var Collection<User> */
    #[ORM\ManyToMany(targetEntity: User::class,
        inversedBy: 'gestionJumps',
        fetch: 'EXTRA_LAZY', indexBy: 'id'),
        Groups(['list_jump', 'list_coa', 'coalition', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private Collection $gestionnaires;
    
    #[ORM\Column(type: 'boolean', options: ['default' => false]), Groups(['coalition', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private bool $jobFige = false;
    
    /** @var Collection<CreneauJump> */
    #[ORM\OneToMany(mappedBy: 'jump', targetEntity: CreneauJump::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true), Groups(['coalition'])]
    private Collection $creneau;
    
    /** @var Collection<Coalition> */
    #[ORM\OneToMany(mappedBy: 'jump', targetEntity: Coalition::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'user_id'), Groups(['coalition'])]
    private Collection $coalitions;
    
    #[ORM\ManyToOne(targetEntity: Event::class, fetch: 'EXTRA_LAZY', inversedBy: 'listJump'), Groups(['gestion_jump', 'admin', 'admin_jump', 'inscription_jump'])]
    private ?Event $event = null;
    
    #[ORM\Column(type: 'text', nullable: true), Groups(['list_jump', 'creation_jump', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?string $description = '';
    
    #[ORM\Column(type: 'string', length: 255, nullable: true), Groups(['creation_jump', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?string $banniere = null;
    
    /** @var Collection<LogEventJump> */
    #[ORM\OneToMany(mappedBy: 'jump', targetEntity: LogEventJump::class, cascade: ['persist', 'remove'],
        orphanRemoval: true), Groups(['gestion_jump'])]
    private Collection $logEvent;
    
    #[ORM\Column(length: 3, nullable: true), Groups(['list_jump', 'creation_jump', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?string $community = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false]), Groups(['creation_jump', 'gestion_jump', 'gestion_event', 'event_inscription', 'admin_jump', 'inscription_jump'])]
    private ?bool $jobSpecific = null;
    
    /**
     * @var Collection<int, JobPrototype>
     */
    #[ORM\JoinTable(name: 'jump_job')]
    #[ORM\JoinColumn(name: 'jump_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'job_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: JobPrototype::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY'), Groups(['creation_jump', 'gestion_jump', 'gestion_event', 'event_inscription', 'admin_jump', 'inscription_jump'])]
    private Collection $job;
    
    #[ORM\Column(type: 'boolean', options: ['default' => false]), Groups(['coalition', 'gestion_jump', 'admin_jump', 'inscription_jump'])]
    private ?bool $figeCompetence = false;
    
    
    public function __construct()
    {
        $this->objectif         = new ArrayCollection();
        $this->lead             = new ArrayCollection();
        $this->inscriptionJumps = new ArrayCollection();
        $this->creneauSemaine   = new ArrayCollection();
        $this->CreneauWeekend   = new ArrayCollection();
        $this->gestionnaires    = new ArrayCollection();
        $this->creneau          = new ArrayCollection();
        $this->coalitions       = new ArrayCollection();
        $this->logEvent         = new ArrayCollection();
        $this->job              = new ArrayCollection();
    }
    
    public function addCoalition(coalition $coalition): self
    {
        if (!$this->coalitions->contains($coalition)) {
            $this->coalitions[$coalition->getUser()->getId()] = $coalition;
            $coalition->setJump($this);
        }
        
        return $this;
    }
    
    public function addCreneau(CreneauJump $creneau): self
    {
        if (!$this->creneau->contains($creneau)) {
            $this->creneau[] = $creneau;
            $creneau->setJump($this);
        }
        
        return $this;
    }
    
    public function addCreneauSemaine(CreneauHorraire $creneauSemaine): self
    {
        if (!$this->creneauSemaine->contains($creneauSemaine)) {
            $this->creneauSemaine[] = $creneauSemaine;
        }
        
        return $this;
    }
    
    public function addCreneauWeekend(CreneauHorraire $creneauWeekend): self
    {
        if (!$this->CreneauWeekend->contains($creneauWeekend)) {
            $this->CreneauWeekend[] = $creneauWeekend;
        }
        
        return $this;
    }
    
    public function addGestionnaire(User $gestionnaire): self
    {
        if (!$this->gestionnaires->contains($gestionnaire)) {
            $this->gestionnaires[$gestionnaire->getId()] = $gestionnaire;
        }
        
        return $this;
    }
    
    public function addInscriptionJump(InscriptionJump $inscriptionJump): self
    {
        if (!$this->inscriptionJumps->contains($inscriptionJump)) {
            $this->inscriptionJumps[$inscriptionJump->getUser()->getId()] = $inscriptionJump;
            $inscriptionJump->setJump($this);
        }
        
        return $this;
    }
    
    public function addJob(JobPrototype $job): static
    {
        if (!$this->job->contains($job)) {
            $this->job->add($job);
        }
        
        return $this;
    }
    
    public function addLead(LeadJump $lead): self
    {
        if (!$this->lead->contains($lead)) {
            $this->lead[] = $lead;
            $lead->setJump($this);
        }
        
        return $this;
    }
    
    public function addLogEvent(LogEventJump $logEvent): static
    {
        if (!$this->logEvent->contains($logEvent)) {
            $this->logEvent->add($logEvent);
            $logEvent->setJump($this);
        }
        
        return $this;
    }
    
    public function addObjectif(ObjectifVillePrototype $objectif): self
    {
        if (!$this->objectif->contains($objectif)) {
            $this->objectif[] = $objectif;
        }
        
        return $this;
    }
    
    public function getBanniere(): ?string
    {
        return $this->banniere;
    }
    
    public function setBanniere(?string $banniere): self
    {
        $this->banniere = $banniere;
        
        return $this;
    }
    
    public function getCoalition(int $userId): coalition
    {
        return $this->coalitions->toArray()[$userId];
    }
    
    public function getCoalitions(): Collection
    {
        return $this->coalitions;
    }
    
    public function getCommunity(): ?string
    {
        return $this->community;
    }
    
    public function setCommunity(?string $community): static
    {
        $this->community = $community;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getCreneau(): Collection
    {
        return $this->creneau;
    }
    
    public function getCreneauSemaine(): Collection
    {
        return $this->creneauSemaine;
    }
    
    public function getCreneauWeekend(): Collection
    {
        return $this->CreneauWeekend;
    }
    
    public function getDateApproxJump(): ?DateTimeInterface
    {
        return $this->dateApproxJump;
    }
    
    public function setDateApproxJump(DateTimeInterface $dateApproxJump): self
    {
        $this->dateApproxJump = $dateApproxJump;
        
        return $this;
    }
    
    public function getDateCreation(): ?DateTimeInterface
    {
        return $this->dateCreation;
    }
    
    public function setDateCreation(DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;
        
        return $this;
    }
    
    public function getDateDebInscription(): ?DateTimeInterface
    {
        return $this->dateDebInscription;
    }
    
    public function setDateDebInscription(DateTimeInterface $dateDebInscription): self
    {
        $this->dateDebInscription = $dateDebInscription;
        
        return $this;
    }
    
    public function getDateFinInscription(): ?DateTimeInterface
    {
        return $this->dateFinInscription;
    }
    
    public function setDateFinInscription(DateTimeInterface $dateFinInscription): self
    {
        $this->dateFinInscription = $dateFinInscription;
        
        return $this;
    }
    
    public function getDateJumpEffectif(): ?DateTimeInterface
    {
        return $this->dateJumpEffectif;
    }
    
    public function setDateJumpEffectif(?DateTimeInterface $dateJumpEffectif): self
    {
        $this->dateJumpEffectif = $dateJumpEffectif;
        
        return $this;
    }
    
    public function getDateModif(): ?DateTimeInterface
    {
        return $this->dateModif;
    }
    
    public function setDateModif(?DateTimeInterface $dateModif): self
    {
        $this->dateModif = $dateModif;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getEvent(): ?Event
    {
        return $this->event;
    }
    
    public function setEvent(?Event $event): self
    {
        $this->event = $event;
        
        return $this;
    }
    
    public function getGestionnaire(int $userId): User|null
    {
        return $this->gestionnaires->toArray()[$userId] ?? null;
    }
    
    public function getGestionnaires(): Collection
    {
        return $this->gestionnaires;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): Jump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getInscriptionJump(int $userId): ?InscriptionJump
    {
        return $this->inscriptionJumps->toArray()[$userId] ?? null;
    }
    
    /**
     * @return Collection<int, InscriptionJump>
     */
    public function getInscriptionJumps(): Collection
    {
        return $this->inscriptionJumps;
    }
    
    /**
     * @return Collection<int, JobPrototype>
     */
    public function getJob(): Collection
    {
        return $this->job;
    }
    
    public function getJobFige(): ?bool
    {
        return $this->jobFige;
    }
    
    public function setJobFige(bool $jobFige): self
    {
        $this->jobFige = $jobFige;
        
        return $this;
    }
    
    public function getLead(): Collection|array
    {
        return $this->lead;
    }
    
    public function setLead(Collection $lead): Jump
    {
        $this->lead = $lead;
        
        return $this;
    }
    
    public function getLogEvent(): Collection
    {
        return $this->logEvent;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getObjectif(): Collection|array
    {
        return $this->objectif;
    }
    
    public function setObjectif(Collection $objectif): Jump
    {
        $this->objectif = $objectif;
        
        return $this;
    }
    
    public function getTypeVille(): ?TypeVille
    {
        return $this->typeVille;
    }
    
    public function setTypeVille(?TypeVille $typeVille): self
    {
        $this->typeVille = $typeVille;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function getVillePrive(): ?bool
    {
        return $this->villePrive;
    }
    
    public function setVillePrive(bool $villePrive): self
    {
        $this->villePrive = $villePrive;
        
        return $this;
    }
    
    public function isFigeCompetence(): ?bool
    {
        return $this->figeCompetence;
    }
    
    public function isJobSpecific(): ?bool
    {
        return $this->jobSpecific;
    }
    
    /**
     * @throws Exception
     */
    public function listOrga(): array
    {
        $gestionJumps = new GestionJumps();
        
        $gestionJumps->setGestionJumps($this->getGestionnaires());
        
        try {
            $gestionJumps = $gestionJumps->triByPseudo();
            
            return array_map(fn(User $gest) => $gest->getPseudo(), $gestionJumps->toArray());
        } catch (Exception) {
            throw new Exception("Erreur lors du tri");
        }
        
        
    }
    
    public function removeCoalition(coalition $coalition): self
    {
        if ($this->coalitions->removeElement($coalition)) {
            // set the owning side to null (unless already changed)
            if ($coalition->getJump() === $this) {
                $coalition->setJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeCreneau(CreneauJump $creneau): self
    {
        if ($this->creneau->removeElement($creneau)) {
            // set the owning side to null (unless already changed)
            if ($creneau->getJump() === $this) {
                $creneau->setJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeCreneauSemaine(CreneauHorraire $creneauSemaine): self
    {
        $this->creneauSemaine->removeElement($creneauSemaine);
        
        return $this;
    }
    
    public function removeCreneauWeekend(CreneauHorraire $creneauWeekend): self
    {
        $this->CreneauWeekend->removeElement($creneauWeekend);
        
        return $this;
    }
    
    public function removeGestionnaire(User $gestionnaire): self
    {
        $this->gestionnaires->removeElement($gestionnaire);
        
        return $this;
    }
    
    public function removeInscriptionJump(InscriptionJump $inscriptionJump): self
    {
        if ($this->inscriptionJumps->removeElement($inscriptionJump)) {
            // set the owning side to null (unless already changed)
            if ($inscriptionJump->getJump() === $this) {
                $inscriptionJump->setJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeJob(JobPrototype $job): static
    {
        $this->job->removeElement($job);
        
        return $this;
    }
    
    public function removeLead(LeadJump $lead): self
    {
        $this->lead->removeElement($lead);
        
        return $this;
    }
    
    public function removeLogEvent(LogEventJump $logEvent): static
    {
        if ($this->logEvent->removeElement($logEvent)) {
            // set the owning side to null (unless already changed)
            if ($logEvent->getJump() === $this) {
                $logEvent->setJump(null);
            }
        }
        
        return $this;
    }
    
    public function removeObjectif(ObjectifVillePrototype $objectif): self
    {
        $this->objectif->removeElement($objectif);
        
        return $this;
    }
    
    public function setFigeCompetence(bool $figeCompetence): static
    {
        $this->figeCompetence = $figeCompetence;
        
        return $this;
    }
    
    public function setGestionnaire(Collection $gestionnaires): Jump
    {
        $this->gestionnaires = $gestionnaires;
        
        return $this;
    }
    
    public function setJobSpecific(?bool $jobSpecific): static
    {
        $this->jobSpecific = $jobSpecific;
        
        return $this;
    }
}
