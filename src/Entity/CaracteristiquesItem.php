<?php

namespace App\Entity;

use App\Repository\CaracteristiquesItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: CaracteristiquesItemRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class CaracteristiquesItem
{
    #[ORM\Id]
    #[ORM\Column]
    #[Groups(['admin_obj', 'outils_expe', 'ency_objet'])]
    private ?int $id = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['admin_obj', 'outils_expe', 'ency_objet'])]
    private ?int $value = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['admin_obj', 'outils_expe', 'ency_objet'])]
    private ?int $probabilite = null;
    
    /** @var Collection<ItemPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: ItemPrototype::class, mappedBy: 'caracteristiques', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'carac_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'item_id', referencedColumnName: 'id')]
    private Collection $itemPrototypes;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(inversedBy: 'caracteristiquesItems')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['admin_obj', 'outils_expe', 'ency_objet'])]
    private ?TypeCaracteristique $typeCarac = null;
    
    public function __construct()
    {
        $this->itemPrototypes = new ArrayCollection();
    }
    
    public function addItemPrototype(ItemPrototype $itemPrototype): static
    {
        if (!$this->itemPrototypes->contains($itemPrototype)) {
            $this->itemPrototypes->add($itemPrototype);
            $itemPrototype->addCaracteristique($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }
    
    public function getItemPrototypes(): Collection
    {
        return $this->itemPrototypes;
    }
    
    public function getProbabilite(): ?int
    {
        return $this->probabilite;
    }
    
    public function setProbabilite(?int $probabilite): static
    {
        $this->probabilite = $probabilite;
        
        return $this;
    }
    
    public function getTypeCarac(): ?TypeCaracteristique
    {
        return $this->typeCarac;
    }
    
    public function setTypeCarac(?TypeCaracteristique $typeCarac): static
    {
        $this->typeCarac = $typeCarac;
        
        return $this;
    }
    
    public function getValue(): ?int
    {
        return $this->value;
    }
    
    public function setValue(?int $value): static
    {
        $this->value = $value;
        
        return $this;
    }
    
    public function removeItemPrototype(ItemPrototype $itemPrototype): static
    {
        if ($this->itemPrototypes->removeElement($itemPrototype)) {
            $itemPrototype->removeCaracteristique($this);
        }
        
        return $this;
    }
}
