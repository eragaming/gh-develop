<?php

namespace App\Entity;

use App\Repository\SacTypeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SacTypeRepository::class)]
class SacType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(length: 255)]
    private ?string $nom = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(inversedBy: 'sacTypes')]
    private ?ItemSacType $items = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getItems(): ?ItemSacType
    {
        return $this->items;
    }
    
    public function setItems(?ItemSacType $items): static
    {
        $this->items = $items;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
}
