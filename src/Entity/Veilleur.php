<?php

namespace App\Entity;

use App\Repository\VeilleurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VeilleurRepository::class)]
class Veilleur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $citoyen = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $nbreObjet = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $survieJour = null;
    
    /** @var Collection<EtatPrototype> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToMany(targetEntity: EtatPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'etat_id', referencedColumnName: 'id', nullable: true)]
    private Collection $listeEtats;
    
    #[ORM\Column(type: 'boolean')]
    private ?bool $drogueExpe = null;
    
    /** @var Collection<SacVeilleur> */
    #[ORM\OneToMany(mappedBy: 'veilleur', targetEntity: SacVeilleur::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\JoinColumn(name: 'sac_id', referencedColumnName: 'id', nullable: true)]
    private Collection $sacVeilleurs;
    
    #[ORM\ManyToOne(targetEntity: OutilsVeille::class, fetch: 'EXTRA_LAZY', inversedBy: 'veilleurs')]
    #[ORM\JoinColumn(name: 'outils_veille_id', referencedColumnName: 'id', nullable: false)]
    private ?OutilsVeille $outilsVeille = null;
    
    #[ORM\Column(type: 'boolean')]
    private ?bool $veilleValide = false;
    
    #[ORM\Column(type: 'boolean')]
    private ?bool $besoinVeilleur = false;
    
    public function __construct()
    {
        $this->listeEtats   = new ArrayCollection();
        $this->sacVeilleurs = new ArrayCollection();
    }
    
    public function addListeEtat(EtatPrototype $listeEtat): self
    {
        if (!$this->listeEtats->contains($listeEtat)) {
            $this->listeEtats[] = $listeEtat;
        }
        
        return $this;
    }
    
    public function addSacVeilleur(SacVeilleur $sacVeilleur): self
    {
        if (!$this->sacVeilleurs->contains($sacVeilleur)) {
            $this->sacVeilleurs[] = $sacVeilleur;
            $sacVeilleur->setVeilleur($this);
        }
        
        return $this;
    }
    
    public function getBesoinVeilleur(): ?bool
    {
        return $this->besoinVeilleur;
    }
    
    public function setBesoinVeilleur(bool $besoinVeilleur): self
    {
        $this->besoinVeilleur = $besoinVeilleur;
        
        return $this;
    }
    
    public function getCitoyen(): ?User
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?User $citoyen): self
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getDrogueExpe(): ?bool
    {
        return $this->drogueExpe;
    }
    
    public function setDrogueExpe(bool $drogueExpe): self
    {
        $this->drogueExpe = $drogueExpe;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): Veilleur
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getListeEtats(): Collection
    {
        return $this->listeEtats;
    }
    
    public function getNbreObjet(): ?int
    {
        return $this->nbreObjet;
    }
    
    public function setNbreObjet(int $nbreObjet): self
    {
        $this->nbreObjet = $nbreObjet;
        
        return $this;
    }
    
    public function getOutilsVeille(): ?OutilsVeille
    {
        return $this->outilsVeille;
    }
    
    public function setOutilsVeille(?OutilsVeille $outilsVeille): self
    {
        $this->outilsVeille = $outilsVeille;
        
        return $this;
    }
    
    public function getSacVeilleurs(): Collection
    {
        return $this->sacVeilleurs;
    }
    
    public function getSurvieJour(): ?int
    {
        return $this->survieJour;
    }
    
    public function setSurvieJour(int $survieJour): self
    {
        $this->survieJour = $survieJour;
        
        return $this;
    }
    
    public function getVeilleValide(): ?bool
    {
        return $this->veilleValide;
    }
    
    public function setVeilleValide(bool $veilleValide): self
    {
        $this->veilleValide = $veilleValide;
        
        return $this;
    }
    
    public function removeListeEtat(EtatPrototype $listeEtat): self
    {
        $this->listeEtats->removeElement($listeEtat);
        
        return $this;
    }
    
    public function removeSacVeilleur(SacVeilleur $sacVeilleur): self
    {
        if ($this->sacVeilleurs->removeElement($sacVeilleur)) {
            // set the owning side to null (unless already changed)
            if ($sacVeilleur->getVeilleur() === $this) {
                $sacVeilleur->setVeilleur(null);
            }
        }
        
        return $this;
    }
}
