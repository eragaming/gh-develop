<?php

namespace App\Entity;

use App\Repository\TypeCritereExpeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypeCritereExpeRepository::class)]
class TypeCritereExpe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 30)]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 30)]
    private ?string $method = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $valeur = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeCritereExpe
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMethod(): ?string
    {
        return $this->method;
    }
    
    public function setMethod(string $method): self
    {
        $this->method = $method;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getValeur(): ?int
    {
        return $this->valeur;
    }
    
    public function setValeur(int $valeur): self
    {
        $this->valeur = $valeur;
        
        return $this;
    }
}
