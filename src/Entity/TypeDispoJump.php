<?php

namespace App\Entity;

use App\Repository\TypeDispoJumpRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeDispoJumpRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeDispoJump
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer'), Groups(['coalition', 'coalition_gen'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 20), Groups(['coalition_gen'])]
    private ?string $nom = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeDispoJump
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
}
