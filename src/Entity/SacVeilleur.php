<?php

namespace App\Entity;

use App\Repository\SacVeilleurRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SacVeilleurRepository::class)]
class SacVeilleur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $nombre = null;
    
    #[ORM\ManyToOne(targetEntity: Veilleur::class, fetch: 'EXTRA_LAZY', inversedBy: 'sacVeilleurs')]
    #[ORM\JoinColumn(name: 'veilleur_id', referencedColumnName: 'id', nullable: false)]
    private ?Veilleur $veilleur = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): SacVeilleur
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;
        
        return $this;
    }
    
    public function getVeilleur(): ?Veilleur
    {
        return $this->veilleur;
    }
    
    public function setVeilleur(?Veilleur $veilleur): self
    {
        $this->veilleur = $veilleur;
        
        return $this;
    }
}
