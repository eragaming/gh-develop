<?php

namespace App\Entity;

use App\Repository\ThemeUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ThemeUserRepository::class)]
class ThemeUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'option'])]
    private ?int $id = null;
    
    #[ORM\Column(length: 60), Groups(['admin', 'option'])]
    private ?string $nom = "Hordes";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $statsBgColor = "#252525FF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $primaryBorderColor = "#DDDDDDFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $backgroundColor = "#00000000";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $myLineColor = "#2300B44C";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $primaryColor = "#5C2B20FF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $secondaryColor = "#7E4D2AFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $tertiaryColor = "#47271CFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $hoverFontColor = "#FFFFFFFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $primaryFontColor = "#FFFFFFFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $tertiaryFontColor = "#eba475FF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $primaryRowColor = "#653A26FF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $secondaryRowColor = "#8A5432FF";
    
    #[ORM\Column(length: 15), Groups(['admin', 'option'])]
    private ?string $baseTheme = "hordes";
    
    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'themesUser')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Column(nullable: false, options: ['default' => '0']), Groups(['admin', 'option'])]
    private ?bool $selected = false;
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $statsFontColor = "#ffffffFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $statsBorderColor = "#FFFFFF26";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $bgHoverColor = "#47271CFF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $succesColor = "#00f300FF";
    
    #[ORM\Column(length: 9, nullable: true), Groups(['admin', 'option'])]
    private ?string $errorColor = "#ff0000FF";
    
    #[ORM\Column(length: 9, nullable: false, options: ['default' => "#ff0000FF"]), Groups(['admin', 'option'])]
    private ?string $specifiqueColor = "#ff0000FF";
    
    #[ORM\OneToOne(mappedBy: 'theme', cascade: ['persist', 'remove']), Groups(['admin', 'option'])]
    private ?UserPersoCouleur $userPersoCouleur = null;
    
    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }
    
    public function setBackgroundColor(string $backgroundColor): static
    {
        $this->backgroundColor = $backgroundColor;
        
        return $this;
    }
    
    public function getBaseTheme(): ?string
    {
        return $this->baseTheme;
    }
    
    public function setBaseTheme(string $baseTheme): static
    {
        $this->baseTheme = $baseTheme;
        
        return $this;
    }
    
    public function getBgHoverColor(): ?string
    {
        return $this->bgHoverColor;
    }
    
    public function setBgHoverColor(?string $bgHoverColor): ThemeUser
    {
        $this->bgHoverColor = $bgHoverColor;
        return $this;
    }
    
    public function getErrorColor(): ?string
    {
        return $this->errorColor;
    }
    
    public function setErrorColor(string $errorColor): static
    {
        $this->errorColor = $errorColor;
        
        return $this;
    }
    
    public function getHoverFontColor(): ?string
    {
        return $this->hoverFontColor;
    }
    
    public function setHoverFontColor(string $hoverFontColor): static
    {
        $this->hoverFontColor = $hoverFontColor;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): ThemeUser
    {
        $this->id = $id;
        return $this;
    }
    
    public function getMyLineColor(): ?string
    {
        return $this->myLineColor;
    }
    
    public function setMyLineColor(string $myLineColor): static
    {
        $this->myLineColor = $myLineColor;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getPrimaryBorderColor(): ?string
    {
        return $this->primaryBorderColor;
    }
    
    public function setPrimaryBorderColor(string $primaryBorderColor): static
    {
        $this->primaryBorderColor = $primaryBorderColor;
        
        return $this;
    }
    
    public function getPrimaryColor(): ?string
    {
        return $this->primaryColor;
    }
    
    public function setPrimaryColor(string $primaryColor): static
    {
        $this->primaryColor = $primaryColor;
        
        return $this;
    }
    
    public function getPrimaryFontColor(): ?string
    {
        return $this->primaryFontColor;
    }
    
    public function setPrimaryFontColor(string $primaryFontColor): static
    {
        $this->primaryFontColor = $primaryFontColor;
        
        return $this;
    }
    
    public function getPrimaryRowColor(): ?string
    {
        return $this->primaryRowColor;
    }
    
    public function setPrimaryRowColor(string $primaryRowColor): static
    {
        $this->primaryRowColor = $primaryRowColor;
        
        return $this;
    }
    
    public function getSecondaryColor(): ?string
    {
        return $this->secondaryColor;
    }
    
    public function setSecondaryColor(string $secondaryColor): static
    {
        $this->secondaryColor = $secondaryColor;
        
        return $this;
    }
    
    public function getSecondaryRowColor(): ?string
    {
        return $this->secondaryRowColor;
    }
    
    public function setSecondaryRowColor(string $secondaryRowColor): static
    {
        $this->secondaryRowColor = $secondaryRowColor;
        
        return $this;
    }
    
    public function getSpecifiqueColor(): ?string
    {
        return $this->specifiqueColor;
    }
    
    public function setSpecifiqueColor(string $specifiqueColor): static
    {
        $this->specifiqueColor = $specifiqueColor;
        
        return $this;
    }
    
    public function getStatsBgColor(): ?string
    {
        return $this->statsBgColor;
    }
    
    public function setStatsBgColor(string $statsBgColor): static
    {
        $this->statsBgColor = $statsBgColor;
        
        return $this;
    }
    
    public function getStatsBorderColor(): ?string
    {
        return $this->statsBorderColor;
    }
    
    public function setStatsBorderColor(?string $statsBorderColor): ThemeUser
    {
        $this->statsBorderColor = $statsBorderColor;
        return $this;
    }
    
    public function getStatsFontColor(): ?string
    {
        return $this->statsFontColor;
    }
    
    public function setStatsFontColor(?string $statsFontColor): ThemeUser
    {
        $this->statsFontColor = $statsFontColor;
        return $this;
    }
    
    public function getSuccesColor(): ?string
    {
        return $this->succesColor;
    }
    
    public function setSuccesColor(string $succesColor): static
    {
        $this->succesColor = $succesColor;
        
        return $this;
    }
    
    public function getTertiaryColor(): ?string
    {
        return $this->tertiaryColor;
    }
    
    public function setTertiaryColor(string $tertiaryColor): static
    {
        $this->tertiaryColor = $tertiaryColor;
        
        return $this;
    }
    
    public function getTertiaryFontColor(): ?string
    {
        return $this->tertiaryFontColor;
    }
    
    public function setTertiaryFontColor(string $tertiaryFontColor): static
    {
        $this->tertiaryFontColor = $tertiaryFontColor;
        
        return $this;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function getUserPersoCouleur(): ?UserPersoCouleur
    {
        return $this->userPersoCouleur;
    }
    
    public function setUserPersoCouleur(UserPersoCouleur $userPersoCouleur): static
    {
        // set the owning side of the relation if necessary
        if ($userPersoCouleur->getTheme() !== $this) {
            $userPersoCouleur->setTheme($this);
        }
        
        $this->userPersoCouleur = $userPersoCouleur;
        
        return $this;
    }
    
    public function isSelected(): ?bool
    {
        return $this->selected;
    }
    
    public function setSelected(bool $selected): static
    {
        $this->selected = $selected;
        
        return $this;
    }
    
    
}
