<?php

namespace App\Entity;

use App\Repository\VilleHistoriqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\Ignore;

#[ORM\Entity(repositoryClass: VilleHistoriqueRepository::class)]
class VilleHistorique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['picto'])]
    private ?int $id = null;
    
    #[ORM\Column]
    #[Groups(['picto'])]
    private ?int $mapid = null;
    
    #[ORM\Column]
    #[Groups(['picto'])]
    private ?int $origin = null;
    
    #[ORM\Column(length: 255)]
    #[Groups(['picto'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['picto'])]
    private ?int $saison = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['picto'])]
    private ?int $derJour = null;
    
    #[ORM\Column(length: 20, nullable: true)]
    #[Groups(['picto'])]
    private ?string $phase = null;
    
    /** @var Collection<HistoriqueVille> */
    #[ORM\OneToMany(mappedBy: 'villeHisto', targetEntity: HistoriqueVille::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true, indexBy: 'user_id')]
    #[Ignore]
    private Collection $historiqueByCitizens;
    
    public function __construct()
    {
        $this->historiqueByCitizens = new ArrayCollection();
        
    }
    
    public function addHistoriqueByCitizen(HistoriqueVille $historiqueVille): self
    {
        if (!$this->historiqueByCitizens->contains($historiqueVille)) {
            $this->historiqueByCitizens[$historiqueVille->getUser()->getId()] = $historiqueVille;
            $historiqueVille->setVilleHisto($this);
        }
        
        return $this;
    }
    
    public function getDerJour(): ?int
    {
        return $this->derJour;
    }
    
    public function setDerJour(int $derJour): self
    {
        $this->derJour = $derJour;
        
        return $this;
    }
    
    public function getHistoriqueByCitizen(int $userId): ?HistoriqueVille
    {
        return $this->historiqueByCitizens->toArray()[$userId] ?? null;
    }
    
    public function getHistoriqueByCitizens(): Collection
    {
        return $this->historiqueByCitizens;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getMapid(): ?int
    {
        return $this->mapid;
    }
    
    public function setMapid(int $mapid): self
    {
        $this->mapid = $mapid;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getOrigin(): ?int
    {
        return $this->origin;
    }
    
    public function setOrigin(int $origin): self
    {
        $this->origin = $origin;
        
        return $this;
    }
    
    public function getPhase(): ?string
    {
        return $this->phase;
    }
    
    public function setPhase(?string $phase): self
    {
        $this->phase = $phase;
        
        return $this;
    }
    
    public function getSaison(): ?int
    {
        return $this->saison;
    }
    
    public function setSaison(int $saison): self
    {
        $this->saison = $saison;
        
        return $this;
    }
    
    public function removeHistoriqueByCitizen(HistoriqueVille $historiqueVille): self
    {
        if ($this->historiqueByCitizens->removeElement($historiqueVille)) {
            // set the owning side to null (unless already changed)
            if ($historiqueVille->getVilleHisto() === $this) {
                $historiqueVille->setVilleHisto(null);
            }
        }
        
        return $this;
    }
}
