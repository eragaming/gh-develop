<?php

namespace App\Entity;

use App\Repository\SacExpeditionnaireRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: SacExpeditionnaireRepository::class)]
class SacExpeditionnaire
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?bool $broken = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?int $nbr = null;
    
    #[ORM\ManyToOne]
    #[Groups(['outils_expe', 'inscription_exp_visu'])]
    private ?StatutItemExpeditionnaire $statut = null;
    
    #[ORM\ManyToOne(inversedBy: 'sac')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?Expeditionnaire $expeditionnaire = null;
    
    #[ORM\ManyToOne(inversedBy: 'sac')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?Ouvriers $ouvriers = null;
    
    public function getExpeditionnaire(): ?Expeditionnaire
    {
        return $this->expeditionnaire;
    }
    
    public function setExpeditionnaire(?Expeditionnaire $expeditionnaire): static
    {
        $this->expeditionnaire = $expeditionnaire;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): SacExpeditionnaire
    {
        $this->id = $id;
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): static
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNbr(): ?int
    {
        return $this->nbr;
    }
    
    public function setNbr(int $nbr): static
    {
        $this->nbr = $nbr;
        
        return $this;
    }
    
    public function getOuvriers(): ?Ouvriers
    {
        return $this->ouvriers;
    }
    
    public function setOuvriers(?Ouvriers $ouvriers): static
    {
        $this->ouvriers = $ouvriers;
        
        return $this;
    }
    
    public function getStatut(): ?StatutItemExpeditionnaire
    {
        return $this->statut;
    }
    
    public function setStatut(?StatutItemExpeditionnaire $statut): static
    {
        $this->statut = $statut;
        
        return $this;
    }
    
    public function isBroken(): ?bool
    {
        return $this->broken;
    }
    
    public function setBroken(bool $broken): static
    {
        $this->broken = $broken;
        
        return $this;
    }
    
}
