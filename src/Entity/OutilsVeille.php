<?php

namespace App\Entity;

use App\Doctrine\IdAlphaOutilsVeilleGenerator;
use App\Repository\OutilsVeilleRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OutilsVeilleRepository::class)]
class OutilsVeille
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaOutilsVeilleGenerator::class)]
    #[ORM\Column(type: 'string', length: 24)]
    private ?string $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $createdBy = null;
    
    #[ORM\Column(type: 'datetime')]
    private ?DateTimeInterface $createdAt = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $ModifyAt = null;
    
    #[ORM\Column(type: 'smallint')]
    private ?int $defVeilleur = null;
    
    /** @var Collection<Veilleur> */
    #[ORM\OneToMany(mappedBy: 'outilsVeille', targetEntity: Veilleur::class, cascade: ['persist', 'remove'],
        fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $veilleurs;
    
    #[ORM\OneToOne(mappedBy: 'outilsVeille', targetEntity: Outils::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private ?Outils $outils = null;
    
    public function __construct()
    {
        $this->veilleurs = new ArrayCollection();
    }
    
    public function addVeilleur(Veilleur $veilleur): self
    {
        if (!$this->veilleurs->contains($veilleur)) {
            $this->veilleurs[] = $veilleur;
            $veilleur->setOutilsVeille($this);
        }
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getDefVeilleur(): ?int
    {
        return $this->defVeilleur;
    }
    
    public function setDefVeilleur(int $defVeilleur): self
    {
        $this->defVeilleur = $defVeilleur;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): OutilsVeille
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->ModifyAt;
    }
    
    public function setModifyAt(?DateTimeInterface $ModifyAt): self
    {
        $this->ModifyAt = $ModifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): self
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getOutils(): ?Outils
    {
        return $this->outils;
    }
    
    public function setOutils(?Outils $outils): self
    {
        // unset the owning side of the relation if necessary
        if ($outils === null && $this->outils !== null) {
            $this->outils->setOutilsVeille(null);
        }
        
        // set the owning side of the relation if necessary
        if ($outils !== null && $outils->getOutilsVeille() !== $this) {
            $outils->setOutilsVeille($this);
        }
        
        $this->outils = $outils;
        
        return $this;
    }
    
    public function getVeilleurs(): Collection
    {
        return $this->veilleurs;
    }
    
    public function removeVeilleur(Veilleur $veilleur): self
    {
        if ($this->veilleurs->removeElement($veilleur)) {
            // set the owning side to null (unless already changed)
            if ($veilleur->getOutilsVeille() === $this) {
                $veilleur->setOutilsVeille(null);
            }
        }
        
        return $this;
    }
}
