<?php

namespace App\Entity;

use App\Repository\EstimationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: EstimationRepository::class)]
class Estimation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['carte_gen'])]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'estimation')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'integer'), Groups(['carte_gen', 'comparatif'])]
    private ?int $minJour = 0;
    
    #[ORM\Column(type: 'integer'), Groups(['carte_gen', 'comparatif'])]
    private ?int $maxJour = 0;
    
    #[ORM\Column(type: 'boolean'), Groups(['carte_gen', 'comparatif'])]
    private ?bool $maxedJour = false;
    
    #[ORM\Column(type: 'integer', nullable: true), Groups(['carte_gen', 'comparatif'])]
    private ?int $minPlanif = 0;
    
    #[ORM\Column(type: 'integer', nullable: true), Groups(['carte_gen', 'comparatif'])]
    private ?int $maxPlanif = 0;
    
    #[ORM\Column(type: 'boolean', nullable: true), Groups(['carte_gen', 'comparatif'])]
    private ?bool $maxedPlanif = false;
    
    public function __construct(#[ORM\Column(type: 'smallint'), Groups(['carte_gen', 'comparatif'])]
                                private int $day)
    {
    }
    
    public function getDay(): int
    {
        return $this->day;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Estimation
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getMaxJour(): ?int
    {
        return $this->maxJour;
    }
    
    public function setMaxJour(int $maxJour): self
    {
        $this->maxJour = $maxJour;
        
        return $this;
    }
    
    public function getMaxPlanif(): ?int
    {
        return $this->maxPlanif;
    }
    
    public function setMaxPlanif(?int $maxPlanif): self
    {
        $this->maxPlanif = $maxPlanif;
        
        return $this;
    }
    
    public function getMaxedJour(): ?bool
    {
        return $this->maxedJour;
    }
    
    public function setMaxedJour(bool $maxedJour): self
    {
        $this->maxedJour = $maxedJour;
        
        return $this;
    }
    
    public function getMaxedPlanif(): ?bool
    {
        return $this->maxedPlanif;
    }
    
    public function setMaxedPlanif(?bool $maxedPlanif): self
    {
        $this->maxedPlanif = $maxedPlanif;
        
        return $this;
    }
    
    public function getMinJour(): ?int
    {
        return $this->minJour;
    }
    
    public function setMinJour(int $minJour): self
    {
        $this->minJour = $minJour;
        
        return $this;
    }
    
    public function getMinPlanif(): ?int
    {
        return $this->minPlanif;
    }
    
    public function setMinPlanif(?int $minPlanif): self
    {
        $this->minPlanif = $minPlanif;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function setDay(int $day): self
    {
        $this->day = $day;
        
        return $this;
    }
}
