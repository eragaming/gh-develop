<?php

namespace App\Entity;

use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: MenuRepository::class)]
class Menu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'general', 'option'])]
    private ?int $id = null;
    
    #[ORM\OneToOne(inversedBy: 'menu')]
    private ?User $user = null;
    
    /**
     * @var Collection<int, MenuElement>
     */
    #[ORM\ManyToMany(targetEntity: MenuElement::class, cascade: ['persist', 'remove']), Groups(['admin', 'general', 'option'])]
    private Collection $items;
    
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    public function addItem(MenuElement $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return Collection<int, MenuElement>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(?User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function removeItem(MenuElement $item): static
    {
        $this->items->removeElement($item);
        
        return $this;
    }
}
