<?php

namespace App\Entity;

use App\Repository\AvancementChantierRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'avancement_chantier')]
#[ORM\Entity(repositoryClass: AvancementChantierRepository::class)]
class AvancementChantier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'avancementChantiers')]
    private ?Ville $ville = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['comparatif'])]
    private ChantierPrototype $chantier;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['chantier', 'comparatif'])]
    private ?int $pa = null;
    
    public function getChantier(): ?ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function setChantier(?ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): AvancementChantier
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): self
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
}
