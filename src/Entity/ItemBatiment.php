<?php

namespace App\Entity;

use App\Repository\ItemBatimentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ItemBatimentRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ItemBatiment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: BatPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'items')]
    #[ORM\JoinColumn(name: 'bat_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['banque', 'admin_bat'])]
    private ?BatPrototype $batPrototype = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'itemBatiments')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['ency', 'admin_bat'])]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['banque', 'ency', 'admin_bat'])]
    private ?int $probabily = null;
    
    public function getBatPrototype(): ?BatPrototype
    {
        return $this->batPrototype;
    }
    
    public function setBatPrototype(?BatPrototype $batPrototype): self
    {
        $this->batPrototype = $batPrototype;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): ItemBatiment
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getProbabily(): ?int
    {
        return $this->probabily;
    }
    
    public function setProbabily(int $probabily): self
    {
        $this->probabily = $probabily;
        
        return $this;
    }
    
}
