<?php

namespace App\Entity;

use App\Doctrine\IdAlphaOutilsReparationGenerator;
use App\Repository\OutilsReparationRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OutilsReparationRepository::class)]
class OutilsReparation
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdAlphaOutilsReparationGenerator::class)]
    #[ORM\Column(type: 'string', length: 24), Groups(['outils_repa'])]
    private ?string $id = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['outils_repa'])]
    private ?User $createdBy = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY'), Groups(['outils_repa'])]
    private ?User $modifyBy = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['outils_repa'])]
    private ?DateTimeInterface $createdAt = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['outils_repa'])]
    private ?DateTimeInterface $modifyAt = null;
    
    #[ORM\Column(type: 'boolean'), Groups(['outils_repa'])]
    private ?bool $twoStep = false;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $gainDef = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $paTot = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['outils_repa'])]
    private ?int $defBase = null;
    
    /** @var Collection<ReparationChantier> */
    #[ORM\OneToMany(mappedBy: 'outilsReparation', targetEntity: ReparationChantier::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', indexBy: 'chantier_id'), Groups(['outils_repa'])]
    private Collection $reparationChantiers;
    
    #[ORM\OneToOne(mappedBy: 'outilsReparation', targetEntity: Outils::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY')]
    private ?Outils $outils = null;
    
    public function __construct()
    {
        $this->reparationChantiers = new ArrayCollection();
    }
    
    public function addReparationChantier(ReparationChantier $reparationChantier): self
    {
        if (!$this->reparationChantiers->contains($reparationChantier)) {
            $this->reparationChantiers[$reparationChantier->getChantier()->getId()] = $reparationChantier;
            $reparationChantier->setOutilsReparation($this);
        }
        
        return $this;
    }
    
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function setCreatedAt(?DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }
    
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;
        
        return $this;
    }
    
    public function getDefBase(): ?int
    {
        return $this->defBase;
    }
    
    public function setDefBase(int $defBase): self
    {
        $this->defBase = $defBase;
        
        return $this;
    }
    
    public function getGainDef(): ?int
    {
        return $this->gainDef;
    }
    
    public function setGainDef(int $gainDef): self
    {
        $this->gainDef = $gainDef;
        
        return $this;
    }
    
    public function getId(): ?string
    {
        return $this->id;
    }
    
    public function setId(?string $id): OutilsReparation
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getModifyAt(): ?DateTimeInterface
    {
        return $this->modifyAt;
    }
    
    public function setModifyAt(?DateTimeInterface $modifyAt): self
    {
        $this->modifyAt = $modifyAt;
        
        return $this;
    }
    
    public function getModifyBy(): ?User
    {
        return $this->modifyBy;
    }
    
    public function setModifyBy(?User $modifyBy): self
    {
        $this->modifyBy = $modifyBy;
        
        return $this;
    }
    
    public function getOutils(): ?Outils
    {
        return $this->outils;
    }
    
    public function setOutils(?Outils $outils): self
    {
        // unset the owning side of the relation if necessary
        if ($outils === null && $this->outils !== null) {
            $this->outils->setOutilsReparation(null);
        }
        
        // set the owning side of the relation if necessary
        if ($outils !== null && $outils->getOutilsReparation() !== $this) {
            $outils->setOutilsReparation($this);
        }
        
        $this->outils = $outils;
        
        return $this;
    }
    
    public function getPaTot(): ?int
    {
        return $this->paTot;
    }
    
    public function setPaTot(int $paTot): self
    {
        $this->paTot = $paTot;
        
        return $this;
    }
    
    public function getReparationChantier(ChantierPrototype $chantier): ReparationChantier|null
    {
        return $this->reparationChantiers->toArray()[$chantier->getId()] ?? null;
    }
    
    public function getReparationChantiers(): Collection
    {
        return $this->reparationChantiers;
    }
    
    public function getTwoStep(): ?bool
    {
        return $this->twoStep;
    }
    
    public function setTwoStep(bool $twoStep): self
    {
        $this->twoStep = $twoStep;
        
        return $this;
    }
    
    public function removeReparationChantier(ReparationChantier $reparationChantier): self
    {
        if ($this->reparationChantiers->removeElement($reparationChantier)) {
            // set the owning side to null (unless already changed)
            if ($reparationChantier->getOutilsReparation() === $this) {
                $reparationChantier->setOutilsReparation(null);
            }
        }
        
        return $this;
    }
}
