<?php

namespace App\Entity;

use App\Repository\OuvriersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: OuvriersRepository::class)]
class Ouvriers
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['outils_expe'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?JobPrototype $job = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?bool $soif = null;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?bool $preinscrit = null;
    
    /** @var Collection<SacExpeditionnaire> */
    #[ORM\OneToMany(mappedBy: 'ouvriers', targetEntity: SacExpeditionnaire::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe'])]
    private Collection $sac;
    
    /** @var Collection<DispoExpedition> */
    #[ORM\OneToMany(mappedBy: 'ouvriers', targetEntity: DispoExpedition::class,
        cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(['outils_expe'])]
    private Collection $dispo;
    
    #[ORM\ManyToOne(inversedBy: 'ouvriers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OutilsExpedition $outilsExpedition = null;
    
    #[ORM\ManyToOne(inversedBy: 'ouvriers')]
    #[Groups(['outils_expe'])]
    private ?Citoyens $citoyen = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['outils_expe'])]
    private ?string $consigne = null;
    
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['outils_expe'])]
    private ?string $commentaire = null;
    
    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups(['outils_expe'])]
    private ?int $position = null;
    
    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $jobFige = false;
    
    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $soifFige = false;
    
    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    #[Groups(['outils_expe'])]
    private ?bool $forBanni = false;
    
    #[ORM\Column(nullable: true)]
    #[Groups(['outils_expe'])]
    private ?int $dispoRapide = null;
    
    public function __construct()
    {
        $this->sac   = new ArrayCollection();
        $this->dispo = new ArrayCollection();
    }
    
    public function addDispo(DispoExpedition $dispo): static
    {
        if (!$this->dispo->contains($dispo)) {
            $this->dispo->add($dispo);
            $dispo->setOuvriers($this);
        }
        
        return $this;
    }
    
    public function addSac(SacExpeditionnaire $sac): static
    {
        if (!$this->sac->contains($sac)) {
            $this->sac->add($sac);
            $sac->setOuvriers($this);
        }
        
        return $this;
    }
    
    public function getCitoyen(): ?Citoyens
    {
        return $this->citoyen;
    }
    
    public function setCitoyen(?Citoyens $citoyen): static
    {
        $this->citoyen = $citoyen;
        
        return $this;
    }
    
    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }
    
    public function setCommentaire(?string $commentaire): static
    {
        $this->commentaire = $commentaire;
        
        return $this;
    }
    
    public function getConsigne(): ?string
    {
        return $this->consigne;
    }
    
    public function setConsigne(?string $consigne): static
    {
        $this->consigne = $consigne;
        
        return $this;
    }
    
    public function getDispo(): Collection
    {
        return $this->dispo;
    }
    
    public function setDispo(Collection $dispo): Ouvriers
    {
        $this->dispo = $dispo;
        return $this;
    }
    
    public function getDispoRapide(): ?int
    {
        return $this->dispoRapide;
    }
    
    public function setDispoRapide(?int $dispoRapide): static
    {
        $this->dispoRapide = $dispoRapide;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): Ouvriers
    {
        $this->id = $id;
        return $this;
    }
    
    public function getJob(): ?JobPrototype
    {
        return $this->job;
    }
    
    public function setJob(?JobPrototype $job): static
    {
        $this->job = $job;
        
        return $this;
    }
    
    public function getOutilsExpedition(): ?OutilsExpedition
    {
        return $this->outilsExpedition;
    }
    
    public function setOutilsExpedition(?OutilsExpedition $outilsExpedition): static
    {
        $this->outilsExpedition = $outilsExpedition;
        
        return $this;
    }
    
    public function getPosition(): ?int
    {
        return $this->position;
    }
    
    public function setPosition(int $position): static
    {
        $this->position = $position;
        
        return $this;
    }
    
    /** @return Collection<int, SacExpeditionnaire> */
    public function getSac(): Collection
    {
        return $this->sac;
    }
    
    public function setSac(Collection $sac): Ouvriers
    {
        $this->sac = $sac;
        return $this;
    }
    
    public function isForBanni(): ?bool
    {
        return $this->forBanni;
    }
    
    public function isJobFige(): ?bool
    {
        return $this->jobFige;
    }
    
    public function isPreinscrit(): ?bool
    {
        return $this->preinscrit;
    }
    
    public function isSoif(): ?bool
    {
        return $this->soif;
    }
    
    public function isSoifFige(): ?bool
    {
        return $this->soifFige;
    }
    
    public function removeDispo(DispoExpedition $dispo): static
    {
        if ($this->dispo->removeElement($dispo)) {
            // set the owning side to null (unless already changed)
            if ($dispo->getOuvriers() === $this) {
                $dispo->setOuvriers(null);
            }
        }
        
        return $this;
    }
    
    public function removeSac(SacExpeditionnaire $sac): static
    {
        if ($this->sac->removeElement($sac)) {
            // set the owning side to null (unless already changed)
            if ($sac->getOuvriers() === $this) {
                $sac->setOuvriers(null);
            }
        }
        
        return $this;
    }
    
    public function setForBanni(?bool $forBanni): static
    {
        $this->forBanni = $forBanni;
        
        return $this;
    }
    
    public function setJobFige(bool $jobFige): static
    {
        $this->jobFige = $jobFige;
        
        return $this;
    }
    
    public function setPreinscrit(?bool $preinscrit): static
    {
        $this->preinscrit = $preinscrit;
        
        return $this;
    }
    
    public function setSoif(?bool $soif): static
    {
        $this->soif = $soif;
        
        return $this;
    }
    
    public function setSoifFige(bool $soifFige): static
    {
        $this->soifFige = $soifFige;
        
        return $this;
    }
}
