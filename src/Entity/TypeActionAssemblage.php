<?php

namespace App\Entity;

use App\Repository\TypeActionAssemblageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: TypeActionAssemblageRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class TypeActionAssemblage
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ency', 'ency_objet', 'admin_ass', 'admin_ass_type'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['ency', 'ency_objet', 'admin_ass_list', 'admin_ass', 'admin_ass_type'])]
    private ?string $nom = null;
    
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['ency', 'admin_ass_type'])]
    private ?string $description = null;
    
    /** @var Collection<ListAssemblage> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'typeAction', targetEntity: ListAssemblage::class, fetch: 'EXTRA_LAZY')]
    private Collection $listAssemblages;
    
    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['ency', 'ency_objet', 'admin_ass_type'])]
    private ?string $nom_item_need = null;
    
    #[ORM\Column(type: 'string', length: 50)]
    #[Groups(['ency', 'ency_objet', 'admin_ass_type'])]
    private ?string $nom_item_obtain = null;
    
    public function __construct()
    {
        $this->listAssemblages = new ArrayCollection();
    }
    
    public function addListAssemblage(ListAssemblage $listAssemblage): self
    {
        if (!$this->listAssemblages->contains($listAssemblage)) {
            $this->listAssemblages[] = $listAssemblage;
            $listAssemblage->setTypeAction($this);
        }
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): TypeActionAssemblage
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getListAssemblages(): Collection
    {
        return $this->listAssemblages;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getNomItemNeed(): ?string
    {
        return $this->nom_item_need;
    }
    
    public function setNomItemNeed(string $nom_item_need): self
    {
        $this->nom_item_need = $nom_item_need;
        
        return $this;
    }
    
    public function getNomItemObtain(): ?string
    {
        return $this->nom_item_obtain;
    }
    
    public function setNomItemObtain(string $nom_item_obtain): self
    {
        $this->nom_item_obtain = $nom_item_obtain;
        
        return $this;
    }
    
    public function removeListAssemblage(ListAssemblage $listAssemblage): self
    {
        if ($this->listAssemblages->removeElement($listAssemblage)) {
            $listAssemblage->setTypeAction($this);
        }
        
        return $this;
    }
}
