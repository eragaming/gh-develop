<?php

namespace App\Entity;

use App\Repository\LvlUpHomeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: LvlUpHomeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class LvlUpHome
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?int $level = null;
    
    /** @var Collection<RessourceUpHome> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\JoinTable(name: 'lvl_up_ressources')]
    #[ORM\JoinColumn(name: 'lvl_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'ressource_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: RessourceUpHome::class, cascade: ['persist', 'remove'], fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private Collection $ressources;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['citoyens', 'outils_chantier', 'ency_hab'])]
    private ?int $pa = null;
    
    public function __construct()
    {
        $this->ressources = new ArrayCollection();
    }
    
    public function addRessource(RessourceUpHome $ressource): self
    {
        if (!$this->ressources->contains($ressource)) {
            $this->ressources[] = $ressource;
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getLevel(): ?int
    {
        return $this->level;
    }
    
    public function setLevel(int $level): self
    {
        $this->level = $level;
        
        return $this;
    }
    
    public function getPa(): ?int
    {
        return $this->pa;
    }
    
    public function setPa(int $pa): self
    {
        $this->pa = $pa;
        
        return $this;
    }
    
    public function getRessources(): Collection
    {
        return $this->ressources;
    }
    
    public function removeRessource(RessourceUpHome $ressource): self
    {
        $this->ressources->removeElement($ressource);
        
        return $this;
    }
}
