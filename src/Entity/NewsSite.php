<?php

namespace App\Entity;

use App\Repository\NewsSiteRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: NewsSiteRepository::class)]
class NewsSite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['news'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', length: 255), Groups(['news'])]
    private ?string $titre = null;
    
    #[ORM\Column(type: 'string', length: 2500, nullable: true), Groups(['news'])]
    private ?string $content = null;
    
    #[ORM\Column(type: 'datetime'), Groups(['news'])]
    private ?DateTimeInterface $dateAjout = null;
    
    #[ORM\Column(type: 'datetime', nullable: true), Groups(['news'])]
    private ?DateTimeInterface $dateModif = null;
    
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: false), Groups(['news'])]
    private ?User $auteur = null;
    
    #[ORM\OneToOne(targetEntity: VersionsSite::class, cascade: ['persist',
                                                                'remove'], fetch: 'EXTRA_LAZY'), Groups(['news'])]
    private ?VersionsSite $version = null;
    
    public function getAuteur(): ?User
    {
        return $this->auteur;
    }
    
    public function setAuteur(?User $auteur): self
    {
        $this->auteur = $auteur;
        
        return $this;
    }
    
    public function getContent(): ?string
    {
        return $this->content;
    }
    
    public function setContent(string $content): self
    {
        $this->content = $content;
        
        return $this;
    }
    
    public function getDateAjout(): ?DateTimeInterface
    {
        return $this->dateAjout;
    }
    
    public function setDateAjout(DateTimeInterface $dateAjout): self
    {
        $this->dateAjout = $dateAjout;
        
        return $this;
    }
    
    public function getDateModif(): ?DateTimeInterface
    {
        return $this->dateModif;
    }
    
    public function setDateModif(?DateTimeInterface $dateModif): self
    {
        $this->dateModif = $dateModif;
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): NewsSite
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;
        
        return $this;
    }
    
    public function getVersion(): ?VersionsSite
    {
        return $this->version;
    }
    
    public function setVersion(?VersionsSite $version): self
    {
        $this->version = $version;
        
        return $this;
    }
}
