<?php

namespace App\Entity;

use App\Repository\RuinesCasesRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RuinesCasesRepository::class)]
class RuinesCases
{
    public const CASE_VIDE         = 0;
    public const DIRECTION_O       = 1;
    public const DIRECTION_N       = 2;
    public const DIRECTION_E       = 4;
    public const DIRECTION_S       = 8;
    public const DIRECTION_ON      = 3;
    public const DIRECTION_OE      = 5;
    public const DIRECTION_OS      = 9;
    public const DIRECTION_NE      = 6;
    public const DIRECTION_NS      = 10;
    public const DIRECTION_SE      = 12;
    public const DIRECTION_ONE     = 7;
    public const DIRECTION_ONS     = 11;
    public const DIRECTION_OSE     = 13;
    public const DIRECTION_NSE     = 14;
    public const DIRECTION_ONSE    = 15;
    public const CASE_ENTRE        = 16;
    public const CASE_COULOIR_VIDE = 17;
    
    public const TYPE_PORTE_OUV   = 'p';
    public const TYPE_PORTE_FERM  = 'pC';
    public const TYPE_PORTE_PERCU = 'pP';
    public const TYPE_PORTE_DECAP = 'pD';
    public const TYPE_PORTE_MAGN  = 'pM';
    public const TYPE_ESCALIER    = 'esc';
    public const NB_0Z            = 0;
    public const NB_1Z            = 1;
    public const NB_2Z            = 2;
    public const NB_3Z            = 3;
    public const NB_4Z            = 4;
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer'), Groups(['ruine', 'admin_ruine'])]
    private ?int $id = null;
    
    #[ORM\ManyToOne(targetEntity: RuinesPlans::class, fetch: 'EXTRA_LAZY', inversedBy: 'cases')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RuinesPlans $ruinesPlans = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['ruine', 'admin_ruine'])]
    private ?int $x = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['ruine', 'admin_ruine'])]
    private ?int $y = null;
    
    #[ORM\Column(type: 'smallint'), Groups(['ruine', 'admin_ruine'])]
    private ?int $typeCase = null;
    
    #[ORM\Column(type: 'string', length: 2, nullable: true), Groups(['ruine', 'admin_ruine'])]
    private ?string $typePorte = null;
    
    #[ORM\Column(type: 'smallint', nullable: true), Groups(['ruine', 'admin_ruine'])]
    private ?int $nbrZombie = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: false, options: ['default' => 0]), Groups(['ruine', 'admin_ruine'])]
    private ?int $z = 0;
    
    #[ORM\Column(name: 'type_escalier', length: 5, nullable: true), Groups(['ruine', 'admin_ruine'])]
    private ?string $typeEscalier = null;
    
    /** @var Collection<RuinesObjets> */
    #[ORM\ManyToMany(targetEntity: RuinesObjets::class,
        cascade: ['persist', 'remove']), Groups(['ruine', 'admin_ruine'])]
    private Collection $items;
    
    #[ORM\ManyToOne, Groups(['ruine', 'admin_ruine'])]
    private ?User $updateBy = null;
    
    #[ORM\Column(type: 'datetime_immutable', nullable: true), Groups(['ruine', 'admin_ruine'])]
    private ?DateTimeImmutable $updateAt = null;
    
    #[Groups(['ruine'])]
    private ?bool $correction = false;
    
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    public function addItem(RuinesObjets $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
        
        return $this;
    }
    
    public function getCorrection(): ?bool
    {
        return $this->correction;
    }
    
    public function setCorrection(?bool $correction): RuinesCases
    {
        $this->correction = $correction;
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return Collection<RuinesObjets>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function setItems(Collection $items): void
    {
        $this->items = $items;
    }
    
    public function getNbrZombie(): ?int
    {
        return $this->nbrZombie;
    }
    
    public function setNbrZombie(?int $nbrZombie): self
    {
        $this->nbrZombie = $nbrZombie;
        
        return $this;
    }
    
    public function getRuinesPlans(): ?RuinesPlans
    {
        return $this->ruinesPlans;
    }
    
    public function setRuinesPlans(?RuinesPlans $ruinesPlans): self
    {
        $this->ruinesPlans = $ruinesPlans;
        
        return $this;
    }
    
    public function getTypeCase(): ?int
    {
        return $this->typeCase;
    }
    
    public function setTypeCase(int $typeCase): self
    {
        $this->typeCase = $typeCase;
        
        return $this;
    }
    
    public function getTypeEscalier(): ?string
    {
        return $this->typeEscalier;
    }
    
    public function setTypeEscalier(?string $type_escalier): static
    {
        $this->typeEscalier = $type_escalier;
        
        return $this;
    }
    
    public function getTypePorte(): ?string
    {
        return $this->typePorte;
    }
    
    public function setTypePorte(?string $typePorte): self
    {
        $this->typePorte = $typePorte;
        
        return $this;
    }
    
    public function getUpdateAt(): ?DateTimeImmutable
    {
        return $this->updateAt;
    }
    
    public function setUpdateAt(?DateTimeImmutable $updateAt): static
    {
        $this->updateAt = $updateAt;
        
        return $this;
    }
    
    public function getUpdateBy(): ?User
    {
        return $this->updateBy;
    }
    
    public function setUpdateBy(?User $updateBy): static
    {
        $this->updateBy = $updateBy;
        
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(?int $x): self
    {
        $this->x = $x;
        
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(int $y): self
    {
        $this->y = $y;
        
        return $this;
    }
    
    public function getZ(): ?int
    {
        return $this->z;
    }
    
    public function setZ(int $z): static
    {
        $this->z = $z;
        
        return $this;
    }
    
    public function removeItem(RuinesObjets $item): static
    {
        $this->items->removeElement($item);
        
        return $this;
    }
}
