<?php

namespace App\Entity;

use App\Enum\MotifEpuisement;
use App\Repository\UserPersonnalisationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: UserPersonnalisationRepository::class)]
class UserPersonnalisation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'option', 'carte_user'])]
    private ?int $id = null;
    
    #[ORM\OneToOne(inversedBy: 'userPersonnalisation', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;
    
    #[ORM\Column(name: 'fige_menu', type: 'boolean', nullable: false, options: ['default' => false]),
        Groups(['admin', 'option', 'carte_user'])]
    private bool $figeMenu = false;
    
    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false]), Groups(['admin', 'carte_user', 'option'])]
    private ?bool $popUpClick = false;
    
    #[ORM\Column(nullable: false, options: ['default' => true]), Groups(['admin', 'citoyens', 'option'])]
    private ?bool $citoyensModeCompact = true;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['admin', 'citoyens', 'option'])]
    private ?bool $blocMajCitoyens = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'carte_user', 'option'])]
    private ?bool $affichageNbCitoyen = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'carte_user', 'option'])]
    private ?bool $zombieDiscret = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'general', 'carte_user', 'option'])]
    private ?bool $keepMenuOpen = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]),
        Groups(['admin', 'general', 'carte_user', 'option'])]
    private ?bool $menuBandeau = false;
    
    #[ORM\Column(nullable: false, options: ['default' => '0']), Groups(['admin', 'option'])]
    private ?bool $majCo = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'option', 'citoyens'])]
    private ?bool $blocMajCitoyen = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'option', 'expe'])]
    private ?bool $expeOnTheTop = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'option', 'citoyens'])]
    private ?bool $onTopCitoyen = false;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'carte_user', 'option'])]
    private ?bool $resizabled = false;
    
    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 42]), Groups(['admin', 'carte_user', 'option'])]
    private ?int $widthCase = 42;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0]), Groups(['admin', 'expe', 'option'])]
    private ?bool $fixInscriptionExpe = false;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['admin', 'carte', 'carte_user', 'option'])]
    private ?bool $affichageBat = false;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['admin', 'carte', 'carte_user', 'option'])]
    private ?bool $actionDiffCarteAlter = false;
    
    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['admin', 'carte', 'carte_user', 'option'])]
    private ?bool $afficherNbrItemsSol = false;
    
    #[ORM\Column(type: 'string', length: 10, enumType: MotifEpuisement::class, options: ['default' => MotifEpuisement::Hachure_gauche]), Groups(['admin', 'carte', 'carte_user', 'option'])]
    private ?MotifEpuisement $motifEpuisement = MotifEpuisement::Hachure_gauche;

    #[ORM\Column(type: 'boolean', options: ['default' => 0]), Groups(['admin', 'carte', 'carte_user', 'option'])]
    private ?bool $itemSeparedCate = false;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
    
    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function setUser(User $user): static
    {
        $this->user = $user;
        
        return $this;
    }
    
    public function getWidthCase(): ?int
    {
        return $this->widthCase;
    }
    
    public function setWidthCase(int $widthCase): static
    {
        $this->widthCase = $widthCase;
        
        return $this;
    }
    
    public function isActionDiffCarteAlter(): ?bool
    {
        return $this->actionDiffCarteAlter;
    }
    
    public function isAffichageBat(): ?bool
    {
        return $this->affichageBat;
    }
    
    public function isAffichageNbCitoyen(): ?bool
    {
        return $this->affichageNbCitoyen;
    }
    
    public function isAfficherNbrItemsSol(): ?bool
    {
        return $this->afficherNbrItemsSol;
    }
    
    public function isBlocMajCitoyen(): ?bool
    {
        return $this->blocMajCitoyen;
    }
    
    public function isBlocMajCitoyens(): ?bool
    {
        return $this->blocMajCitoyens;
    }
    
    public function isCitoyensModeCompact(): ?bool
    {
        return $this->citoyensModeCompact;
    }
    
    public function isExpeOnTheTop(): ?bool
    {
        return $this->expeOnTheTop;
    }
    
    public function isFigeMenu(): bool
    {
        return $this->figeMenu;
    }
    
    public function setFigeMenu(bool $figeMenu): UserPersonnalisation
    {
        $this->figeMenu = $figeMenu;
        return $this;
    }
    
    public function isFixInscriptionExpe(): ?bool
    {
        return $this->fixInscriptionExpe;
    }
    
    public function isKeepMenuOpen(): ?bool
    {
        return $this->keepMenuOpen;
    }
    
    public function isMajCo(): ?bool
    {
        return $this->majCo;
    }
    
    public function isMenuBandeau(): ?bool
    {
        return $this->menuBandeau;
    }
    
    public function isOnTopCitoyen(): ?bool
    {
        return $this->onTopCitoyen;
    }
    
    public function isPopUpClick(): ?bool
    {
        return $this->popUpClick;
    }
    
    public function isResizabled(): ?bool
    {
        return $this->resizabled;
    }
    
    public function isZombieDiscret(): ?bool
    {
        return $this->zombieDiscret;
    }
    
    public function setActionDiffCarteAlter(bool $actionDiffCarteAlter): static
    {
        $this->actionDiffCarteAlter = $actionDiffCarteAlter;
        
        return $this;
    }
    
    public function setAffichageBat(bool $affichageBat): static
    {
        $this->affichageBat = $affichageBat;
        
        return $this;
    }
    
    public function setAffichageNbCitoyen(?bool $affichageNbCitoyen): UserPersonnalisation
    {
        $this->affichageNbCitoyen = $affichageNbCitoyen;
        return $this;
    }
    
    public function setAfficherNbrItemsSol(bool $afficherNbrItemsSol): static
    {
        $this->afficherNbrItemsSol = $afficherNbrItemsSol;
        
        return $this;
    }
    
    public function setBlocMajCitoyen(?bool $blocMajCitoyen): UserPersonnalisation
    {
        $this->blocMajCitoyen = $blocMajCitoyen;
        return $this;
    }
    
    public function setBlocMajCitoyens(?bool $blocMajCitoyens): UserPersonnalisation
    {
        $this->blocMajCitoyens = $blocMajCitoyens;
        return $this;
    }
    
    public function setCarteTextured(?bool $carteTextured): UserPersonnalisation
    {
        $this->carteTextured = $carteTextured;
        return $this;
    }
    
    public function setCitoyensModeCompact(?bool $citoyensModeCompact): UserPersonnalisation
    {
        $this->citoyensModeCompact = $citoyensModeCompact;
        return $this;
    }
    
    public function setExpeOnTheTop(?bool $expeOnTheTop): UserPersonnalisation
    {
        $this->expeOnTheTop = $expeOnTheTop;
        return $this;
    }
    
    public function setFixInscriptionExpe(bool $fixInscriptionExpe): static
    {
        $this->fixInscriptionExpe = $fixInscriptionExpe;
        
        return $this;
    }
    
    public function setKeepMenuOpen(?bool $keepMenuOpen): UserPersonnalisation
    {
        $this->keepMenuOpen = $keepMenuOpen;
        return $this;
    }
    
    public function setMajCo(?bool $majCo): UserPersonnalisation
    {
        $this->majCo = $majCo;
        return $this;
    }
    
    public function setMenuBandeau(?bool $menuBandeau): UserPersonnalisation
    {
        $this->menuBandeau = $menuBandeau;
        return $this;
    }
    
    public function setOnTopCitoyen(?bool $onTopCitoyen): UserPersonnalisation
    {
        $this->onTopCitoyen = $onTopCitoyen;
        return $this;
    }
    
    public function setPopUpClick(?bool $popUpClick): UserPersonnalisation
    {
        $this->popUpClick = $popUpClick;
        return $this;
    }
    
    public function setResizabled(bool $resizabled): static
    {
        $this->resizabled = $resizabled;
        
        return $this;
    }
    
    public function setZombieDiscret(?bool $zombieDiscret): UserPersonnalisation
    {
        $this->zombieDiscret = $zombieDiscret;
        return $this;
    }
    
    
    public function getMotifEpuisement(): ?MotifEpuisement
    {
        return $this->motifEpuisement;
    }
    
    public function setMotifEpuisement(MotifEpuisement $motifEpuisement): static
    {
        $this->motifEpuisement = $motifEpuisement;
        
        return $this;
    }

    public function isItemSeparedCate(): ?bool
    {
        return $this->itemSeparedCate;
    }

    public function setItemSeparedCate(bool $itemSeparedCate): static
    {
        $this->itemSeparedCate = $itemSeparedCate;

        return $this;
    }
    
}
