<?php

namespace App\Entity;

use App\Repository\ChantiersRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ChantiersRepository::class)]
class Chantiers
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;
    
    #[ORM\ManyToOne(targetEntity: Ville::class, fetch: 'EXTRA_LAZY', inversedBy: 'chantiers')]
    private ?Ville $ville = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['plan_construit', 'comparatif'])]
    private ?int $pv = null;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['plan_construit', 'comparatif'])]
    private ?int $def = null;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $jour = null;
    
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['plan_construit', 'journal', 'comparatif'])]
    private ?bool $detruit = false;
    
    #[ORM\Column(type: Types::SMALLINT, nullable: false, options: ['default' => 1])]
    #[Groups(['comparatif'])]
    private ?int $nbrConstruit = null;
    
    /**
     * @var int[]|null $jourConstruction
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(['comparatif'])]
    private ?array $jourConstruction = [];
    
    /**
     * @var int[]|null $jourDestruction
     */
    #[ORM\Column(type: 'json', nullable: true, options: ['default' => '[]'])]
    #[Groups(['comparatif'])]
    private ?array $jourDestruction = [];
    
    public function __construct(
        #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
        #[ORM\ManyToOne(targetEntity: ChantierPrototype::class, fetch: 'EXTRA_LAZY')]
        #[ORM\JoinColumn(name: 'chantier_id', referencedColumnName: 'id', nullable: false), Groups(['journal', 'plan_construit', 'comparatif'])]
        private ChantierPrototype $chantier)
    {
    }
    
    public function getChantier(): ChantierPrototype
    {
        return $this->chantier;
    }
    
    public function getDef(): ?int
    {
        return $this->def;
    }
    
    public function setDef(int $def): self
    {
        $this->def = $def;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Chantiers
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    public function setJour(?int $jour): static
    {
        $this->jour = $jour;
        
        return $this;
    }
    
    public function getJourConstruction(): ?array
    {
        return $this->jourConstruction;
    }
    
    public function setJourConstruction(?array $jourConstruction): static
    {
        $this->jourConstruction = $jourConstruction;
        
        return $this;
    }
    
    public function getJourDestruction(): ?array
    {
        return $this->jourDestruction;
    }
    
    public function setJourDestruction(?array $jourDestruction): static
    {
        $this->jourDestruction = $jourDestruction;
        
        return $this;
    }
    
    public function getNbrConstruit(): ?int
    {
        return $this->nbrConstruit;
    }
    
    public function setNbrConstruit(int $nbrConstruit): static
    {
        $this->nbrConstruit = $nbrConstruit;
        
        return $this;
    }
    
    public function getPv(): ?int
    {
        return $this->pv;
    }
    
    public function setPv(int $pv): self
    {
        $this->pv = $pv;
        
        return $this;
    }
    
    public function getVille(): ?Ville
    {
        return $this->ville;
    }
    
    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    public function isDetruit(): ?bool
    {
        return $this->detruit;
    }
    
    public function setChantier(ChantierPrototype $chantier): self
    {
        $this->chantier = $chantier;
        
        return $this;
    }
    
    public function setDetruit(bool $detruit): static
    {
        $this->detruit = $detruit;
        
        return $this;
    }
}
