<?php

namespace App\Entity;

use App\Enum\MenuType;
use App\Repository\MenuElementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: MenuElementRepository::class)]
class MenuElement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column, Groups(['admin', 'general','option'])]
    private ?int $id = null;
    
    #[ORM\Column(type: 'string', enumType: MenuType::class), Groups(['admin', 'general','option'])]
    private ?MenuType $typeMenu = null;
    
    #[ORM\ManyToOne, Groups(['admin', 'general','option']), ORM\JoinColumn(nullable: true)]
    private ?MenuPrototype $menu = null;
    
    #[ORM\ManyToOne(targetEntity: MenuElement::class, inversedBy: 'items')]
    #[ORM\JoinColumn(nullable: true)]
    private ?MenuElement $parent = null;
    
    /**
     * @var Collection<int, MenuElement>
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: MenuElement::class, cascade: ['persist', 'remove'], orphanRemoval: true), Groups(['admin', 'general','option'])]
    private Collection $items;
    
    #[ORM\Column(type: Types::SMALLINT), Groups(['admin', 'general','option'])]
    private ?int $ordre = null;
    
    #[ORM\Column(length: 40, nullable: true), Groups(['admin', 'general','option'])]
    private ?string $name = null;
    
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    public function addItem(self $item): static
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setParent($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(?int $id): static
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return Collection<int, MenuElement>
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
    
    public function getMenu(): ?MenuPrototype
    {
        return $this->menu;
    }
    
    public function setMenu(?MenuPrototype $menu): static
    {
        $this->menu = $menu;
        
        return $this;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }
    
    public function setName(?string $name): static
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function getOrdre(): ?int
    {
        return $this->ordre;
    }
    
    public function setOrdre(int $ordre): static
    {
        $this->ordre = $ordre;
        
        return $this;
    }
    
    public function getParent(): ?MenuElement
    {
        return $this->parent;
    }
    
    public function setParent(?MenuElement $parent): static
    {
        $this->parent = $parent;
        
        return $this;
    }
    
    public function getTypeMenu(): ?MenuType
    {
        return $this->typeMenu;
    }
    
    public function setTypeMenu(MenuType $typeMenu): static
    {
        $this->typeMenu = $typeMenu;
        
        return $this;
    }
    
    public function removeItem(MenuElement $item): static
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getParent() === $this) {
                $item->setParent(null);
            }
        }
        
        return $this;
    }
    
    public function removeItems(): static
    {
        foreach ($this->items as $item) {
            $this->removeItem($item);
        }
        
        return $this;
    }
    
}
