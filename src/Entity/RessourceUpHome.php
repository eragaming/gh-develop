<?php

namespace App\Entity;

use App\Repository\RessourceUpHomeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: RessourceUpHomeRepository::class)]
class RessourceUpHome
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'ressourceUpHomes')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false), Groups(['outils_chantier', 'ency_hab'])]
    private ?ItemPrototype $item = null;
    
    #[ORM\Column, Groups(['outils_chantier', 'ency_hab'])]
    private ?int $nombre = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): static
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getNombre(): ?int
    {
        return $this->nombre;
    }
    
    public function setNombre(int $nombre): static
    {
        $this->nombre = $nombre;
        
        return $this;
    }
}
