<?php

namespace App\Entity;

use App\Repository\ItemPrototypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

/**
 * ItemPrototype
 */
#[ORM\Table(name: 'item_prototype')]
#[ORM\Entity(repositoryClass: ItemPrototypeRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ItemPrototype
{
    
    public const ITEM_MARQUEURS_SCRUT = 5007;
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
    #[Groups(['banque', 'chantier', 'citoyens', 'general', 'ency', 'ency_objet', 'ency_weapons', 'ency_bat',
              'ency_veille', 'ency_poubelle', 'outils_decharge', 'admin_gen', 'carte', 'ruine', 'ency_hab',
              'outils_expe', 'inscription_exp_visu', 'ency_decharge', 'admin_ass', 'admin_ass_list','comparatif'])]
    private int $id;
    
    #[ORM\Column(name: 'nom', type: 'string', length: 64, nullable: false)]
    #[Groups(['banque', 'chantier', 'citoyens', 'general', 'ency_bat', 'outils_decharge', 'admin_gen', 'carte', 'ruine',
              'ency_hab', 'outils_expe', 'inscription_exp_visu', 'ency_decharge', 'admin_ass_list', 'admin_ass','comparatif'])]
    private ?string $nom;
    
    #[ORM\Column(name: 'icon', type: 'string', length: 40, nullable: false)]
    #[Groups(['banque', 'chantier', 'citoyens', 'general', 'ency_bat', 'outils_decharge', 'admin_gen', 'carte', 'ruine',
              'ency_hab', 'outils_expe', 'inscription_exp_visu', 'ency_decharge', 'admin_ass','comparatif'])]
    private ?string $icon;
    
    #[ORM\Column(name: 'description', type: 'string', length: 2550, nullable: false)]
    #[Groups(['banque', 'general', 'admin_obj', 'carte'])]
    private ?string $description;
    
    #[ORM\Column(name: 'objet_veille', type: 'boolean', nullable: false), Groups(['admin_obj'])]
    private bool $objetVeille;
    
    #[ORM\ManyToOne(targetEntity: 'TypeObjet', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'type_objet_id', referencedColumnName: 'id', nullable: true), Groups(['admin_obj', 'carte', 'outils_expe', 'inscription_exp_visu'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?TypeObjet $typeObjet = null;
    
    #[ORM\ManyToOne(targetEntity: 'CategoryObjet', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'category_objet_id', referencedColumnName: 'id')]
    #[Groups(['banque', 'general', 'admin_obj', 'carte', 'outils_expe', 'inscription_exp_visu','comparatif'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private CategoryObjet $categoryObjet;
    
    #[ORM\ManyToOne(targetEntity: 'TypeDecharge', fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(name: 'type_decharge_id', referencedColumnName: 'id', nullable: true), Groups(['admin_obj', 'carte'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?TypeDecharge $typeDecharge = null;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?int $defBase = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $armurerie = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $magasin = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $tourelle = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $lanceBete = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj', 'outils_expe'])]
    private ?bool $encombrant = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $usageUnique = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj'])]
    private ?bool $reparable = null;
    
    #[ORM\Column(type: 'string', length: 64, unique: true, nullable: true), Groups(['admin_obj'])]
    private ?string $uid = null;
    
    #[ORM\Column(type: 'smallint', unique: true, nullable: true)]
    private ?int $idHordes = null;
    
    /** @var Collection<ItemBatiment> */
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemBatiment::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[Groups(['banque', 'admin_obj'])]
    private Collection $itemBatiments;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj', 'outils_expe'])]
    private ?bool $conteneur = null;
    
    #[ORM\OneToOne(targetEntity: ListAssemblage::class, fetch: 'EXTRA_LAZY')]
    #[Groups(['ency_objet', 'admin_obj'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?ListAssemblage $listAssemblage = null;
    
    /** @var Collection<ItemNeed> */
    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemNeed::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $itemNeeds;
    
    /** @var Collection<ItemProbability> */
    #[ORM\OneToMany(mappedBy: 'item', targetEntity: ItemProbability::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $itemObtains;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_weapons', 'admin_obj'])]
    private ?int $killMin = null;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_weapons', 'admin_obj'])]
    private ?int $killMax = null;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_weapons', 'admin_obj'])]
    private ?int $chanceKill = null;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_weapons', 'admin_obj'])]
    private ?int $chance = null;
    
    #[ORM\Column(type: 'smallint', nullable: true)]
    #[Groups(['ency_weapons', 'admin_obj'])]
    private ?int $type = null;
    
    /** @var Collection<ItemPrototype> */
    #[ORM\JoinTable(name: 'drop_arme')]
    #[ORM\JoinColumn(name: 'item_ini_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'item_drop_id', referencedColumnName: 'id')]
    #[ORM\ManyToMany(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY')]
    #[Groups(['ency_weapons', 'admin_obj'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $itemResult;
    
    #[ORM\Column(name: 'id_mh', nullable: true), Groups(['banque', 'chantier', 'citoyens', 'general', 'ency', 'ency_objet', 'ency_weapons', 'ency_bat',
                                                         'ency_veille', 'ency_poubelle', 'outils_decharge', 'admin_gen', 'carte', 'ruine', 'ency_hab',
                                                         'outils_expe', 'admin_obj'])]
    private ?int $idMh = null;
    
    #[ORM\Column(nullable: true, options: ['default' => false])]
    #[Groups(['ency_objet', 'admin_obj', 'admin_gen_obj'])]
    private ?bool $actif = null;
    
    #[ORM\Column, Groups(['admin_gen_obj'])]
    private ?bool $updateByAdmin = null;
    
    #[ORM\Column(type: Types::INTEGER, nullable: true), Groups(['ency_poubelle', 'admin_obj'])]
    private ?int $probaPoubelle = null;
    
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    #[ORM\OneToMany(mappedBy: 'item', targetEntity: RessourceUpHome::class)]
    private Collection $ressourceUpHomes;
    
    #[ORM\Column(type: Types::SMALLINT, options: ['default' => 0]), Groups(['admin_obj', 'admin_gen_obj', 'admin_gen', 'ency_objet'])]
    private ?int $deco = 0;
    
    #[ORM\ManyToMany(targetEntity: CaracteristiquesItem::class, inversedBy: 'itemPrototypes', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id')]
    #[ORM\InverseJoinColumn(name: 'carac_id', referencedColumnName: 'id')]
    #[Groups(['ency_objet', 'admin_obj', 'admin_gen_obj', 'outils_expe', 'banque'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private Collection $caracteristiques;
    
    #[ORM\ManyToOne(targetEntity: 'TypeDecharge', fetch: 'EAGER', inversedBy: 'itemPrototypes')]
    #[ORM\JoinColumn(name: 'decharge_id', referencedColumnName: 'id', nullable: true), Groups(['admin_obj', 'carte', 'ency_objet', 'ency_decharge'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?TypeDecharge $decharge = null;
    
    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(['ency_veille', 'admin_obj', 'outils_expe', 'carte'])]
    private ?bool $expedition = false;
    
    public function __construct()
    {
        $this->itemBatiments    = new ArrayCollection();
        $this->itemNeeds        = new ArrayCollection();
        $this->itemObtains      = new ArrayCollection();
        $this->itemResult       = new ArrayCollection();
        $this->ressourceUpHomes = new ArrayCollection();
        $this->caracteristiques = new ArrayCollection();
    }
    
    public function addCaracteristique(CaracteristiquesItem $caracteristique): static
    {
        if (!$this->caracteristiques->contains($caracteristique)) {
            $this->caracteristiques->add($caracteristique);
        }
        
        return $this;
    }
    
    public function addItemBatiment(ItemBatiment $itemBatiment): self
    {
        if (!$this->itemBatiments->contains($itemBatiment)) {
            $this->itemBatiments[] = $itemBatiment;
            $itemBatiment->setItem($this);
        }
        
        return $this;
    }
    
    public function addItemNeed(ItemNeed $itemNeed): self
    {
        if (!$this->itemNeeds->contains($itemNeed)) {
            $this->itemNeeds[$itemNeed->getId()] = $itemNeed;
            $itemNeed->setItem($this);
        }
        
        return $this;
    }
    
    public function addItemObtain(ItemProbability $itemObtain): self
    {
        if (!$this->itemObtains->contains($itemObtain)) {
            $this->itemObtains[$itemObtain->getId()] = $itemObtain;
            $itemObtain->setItem($this);
        }
        
        return $this;
    }
    
    public function addItemResult(self $itemResult): self
    {
        if (!$this->itemResult->contains($itemResult)) {
            $this->itemResult[] = $itemResult;
        }
        
        return $this;
    }
    
    public function addRessourceUpHome(RessourceUpHome $ressourceUpHome): static
    {
        if (!$this->ressourceUpHomes->contains($ressourceUpHome)) {
            $this->ressourceUpHomes->add($ressourceUpHome);
            $ressourceUpHome->setItem($this);
        }
        
        return $this;
    }
    
    public function getArmurerie(): ?bool
    {
        return $this->armurerie;
    }
    
    public function setArmurerie(?bool $armurerie): self
    {
        $this->armurerie = $armurerie;
        
        return $this;
    }
    
    #[Groups(['ency_weapons'])]
    public function getAvgKill(): float
    {
        
        if ($this->killMin === null || $this->chanceKill === null || $this->chance === null) {
            return 0;
        }
        
        // Convert percentages to fractions
        $successRate = $this->chanceKill / 100;
        $failureRate = $this->chance / 100;
        
        // Calculate the average number of kills per successful attempt
        $averageKillsPerAttempt = ($this->killMin + ($this->killMax ?? $this->killMin)) / 2;
        
        // Calculate the number of attempts before failure
        $averageAttemptsBeforeFailure = 1 / $failureRate;
        
        // Calculate the number of successful attempts before failure
        $averageSuccessfulAttempts = $averageAttemptsBeforeFailure * $successRate;
        
        // Calculate the average number of kills before failure
        return $averageSuccessfulAttempts * $averageKillsPerAttempt;
    }
    
    /**
     * @return Collection<int,CaracteristiquesItem>
     */
    public function getCaracteristiques(): Collection
    {
        return $this->caracteristiques;
    }
    
    public function getCategoryObjet(): ?CategoryObjet
    {
        return $this->categoryObjet;
    }
    
    public function setCategoryObjet(CategoryObjet $categoryObjet): self
    {
        $this->categoryObjet = $categoryObjet;
        
        return $this;
    }
    
    public function getChance(): ?int
    {
        return $this->chance;
    }
    
    public function setChance(?int $chance): self
    {
        $this->chance = $chance;
        
        return $this;
    }
    
    public function getChanceKill(): ?int
    {
        return $this->chanceKill;
    }
    
    public function setChanceKill(?int $chanceKill): self
    {
        $this->chanceKill = $chanceKill;
        
        return $this;
    }
    
    public function getConteneur(): ?bool
    {
        return $this->conteneur;
    }
    
    public function setConteneur(?bool $conteneur): self
    {
        $this->conteneur = $conteneur;
        
        return $this;
    }
    
    public function getDecharge(): ?TypeDecharge
    {
        return $this->decharge;
    }
    
    public function setDecharge(?TypeDecharge $decharge): static
    {
        $this->decharge = $decharge;
        
        return $this;
    }
    
    public function getDeco(): ?int
    {
        return $this->deco;
    }
    
    public function setDeco(int $deco): static
    {
        $this->deco = $deco;
        
        return $this;
    }
    
    public function getDefBase(): ?int
    {
        return $this->defBase;
    }
    
    public function setDefBase(?int $defBase): self
    {
        $this->defBase = $defBase;
        
        return $this;
    }
    
    public function getDescription(): ?string
    {
        return $this->description;
    }
    
    public function setDescription(string $description): self
    {
        $this->description = $description;
        
        return $this;
    }
    
    public function getEncombrant(): ?bool
    {
        return $this->encombrant;
    }
    
    public function setEncombrant(?bool $encombrant): self
    {
        $this->encombrant = $encombrant;
        
        return $this;
    }
    
    public function getIcon(): ?string
    {
        return $this->icon;
    }
    
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        
        return $this;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getIdHordes(): ?int
    {
        return $this->idHordes;
    }
    
    public function setIdHordes(int $idHordes): self
    {
        $this->idHordes = $idHordes;
        
        return $this;
    }
    
    public function getIdMh(): ?int
    {
        return $this->idMh;
    }
    
    public function setIdMh(?int $idMh): self
    {
        $this->idMh = $idMh;
        
        return $this;
    }
    
    public function getItemBatiments(): Collection
    {
        return $this->itemBatiments;
    }
    
    public function getItemNeed(int $id): ItemNeed
    {
        return $this->itemNeeds[$id];
    }
    
    public function getItemNeeds(): Collection
    {
        return $this->itemNeeds;
    }
    
    public function getItemObtain(int $id): ItemProbability
    {
        return $this->itemObtains[$id];
    }
    
    public function getItemObtains(): Collection
    {
        return $this->itemObtains;
        
    }
    
    public function getItemResult(): Collection
    {
        return $this->itemResult;
    }
    
    public function getKillMax(): ?int
    {
        return $this->killMax;
    }
    
    public function setKillMax(?int $killMax): self
    {
        $this->killMax = $killMax;
        
        return $this;
    }
    
    public function getKillMin(): ?int
    {
        return $this->killMin;
    }
    
    public function setKillMin(?int $killMin): self
    {
        $this->killMin = $killMin;
        
        return $this;
    }
    
    public function getLanceBete(): ?bool
    {
        return $this->lanceBete;
    }
    
    public function setLanceBete(?bool $lanceBete): self
    {
        $this->lanceBete = $lanceBete;
        
        return $this;
    }
    
    public function getListAssemblage(): ?ListAssemblage
    {
        return $this->listAssemblage;
    }
    
    public function setListAssemblage(?ListAssemblage $listAssemblage): self
    {
        $this->listAssemblage = $listAssemblage;
        
        return $this;
    }
    
    public function getMagasin(): ?bool
    {
        return $this->magasin;
    }
    
    public function setMagasin(?bool $magasin): self
    {
        $this->magasin = $magasin;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
    public function getObjetVeille(): ?bool
    {
        return $this->objetVeille;
    }
    
    public function setObjetVeille(bool $objetVeille): self
    {
        $this->objetVeille = $objetVeille;
        
        return $this;
    }
    
    public function getProbaPoubelle(): ?int
    {
        return $this->probaPoubelle;
    }
    
    public function setProbaPoubelle(?int $probaPoubelle): static
    {
        $this->probaPoubelle = $probaPoubelle;
        
        return $this;
    }
    
    public function getReparable(): ?bool
    {
        return $this->reparable;
    }
    
    public function setReparable(?bool $reparable): self
    {
        $this->reparable = $reparable;
        
        return $this;
    }
    
    public function getRessourceUpHomes(): Collection
    {
        return $this->ressourceUpHomes;
    }
    
    public function getTourelle(): ?bool
    {
        return $this->tourelle;
    }
    
    public function setTourelle(?bool $tourelle): self
    {
        $this->tourelle = $tourelle;
        
        return $this;
    }
    
    public function getType(): ?int
    {
        return $this->type;
    }
    
    public function setType(?int $type): self
    {
        $this->type = $type;
        
        return $this;
    }
    
    public function getTypeDecharge(): ?TypeDecharge
    {
        return $this->typeDecharge;
    }
    
    public function setTypeDecharge(?TypeDecharge $typeDecharge): self
    {
        $this->typeDecharge = $typeDecharge;
        
        return $this;
    }
    
    public function getTypeObjet(): ?TypeObjet
    {
        return $this->typeObjet;
    }
    
    public function setTypeObjet(?TypeObjet $typeObjet): self
    {
        $this->typeObjet = $typeObjet;
        
        return $this;
    }
    
    public function getUid(): ?string
    {
        return $this->uid;
    }
    
    public function setUid(string $uid): self
    {
        $this->uid = $uid;
        
        return $this;
    }
    
    public function getUsageUnique(): ?bool
    {
        return $this->usageUnique;
    }
    
    public function setUsageUnique(?bool $usageUnique): self
    {
        $this->usageUnique = $usageUnique;
        
        return $this;
    }
    
    public function isActif(): ?bool
    {
        return $this->actif;
    }
    
    public function isExpedition(): ?bool
    {
        return $this->expedition;
    }
    
    public function isUpdateByAdmin(): ?bool
    {
        return $this->updateByAdmin;
    }
    
    public function removeAllItemResult(): self
    {
        foreach ($this->getItemResult() as $item_result) {
            $this->itemResult->removeElement($item_result);
        }
        
        return $this;
    }
    
    public function removeCaracteristique(CaracteristiquesItem $caracteristique): static
    {
        $this->caracteristiques->removeElement($caracteristique);
        
        return $this;
    }
    
    public function removeItemBatiment(ItemBatiment $itemBatiment): self
    {
        if ($this->itemBatiments->removeElement($itemBatiment)) {
            // set the owning side to null (unless already changed)
            if ($itemBatiment->getItem() === $this) {
                $itemBatiment->setItem(null);
            }
        }
        
        return $this;
    }
    
    public function removeItemNeed(ItemNeed $itemNeed): self
    {
        if ($this->itemNeeds->removeElement($itemNeed)) {
            // set the owning side to null (unless already changed)
            if ($itemNeed->getItem() === $this) {
                $itemNeed->setItem(null);
            }
        }
        
        return $this;
    }
    
    public function removeItemObtain(ItemProbability $itemObtain): self
    {
        if ($this->itemObtains->removeElement($itemObtain)) {
            // set the owning side to null (unless already changed)
            if ($itemObtain->getItem() === $this) {
                $itemObtain->setItem(null);
            }
        }
        
        return $this;
    }
    
    public function removeItemResult(self $itemResult): self
    {
        $this->itemResult->removeElement($itemResult);
        
        return $this;
    }
    
    public function removeRessourceUpHome(RessourceUpHome $ressourceUpHome): static
    {
        if ($this->ressourceUpHomes->removeElement($ressourceUpHome)) {
            // set the owning side to null (unless already changed)
            if ($ressourceUpHome->getItem() === $this) {
                $ressourceUpHome->setItem(null);
            }
        }
        
        return $this;
    }
    
    public function setActif(?bool $actif): self
    {
        $this->actif = $actif;
        
        return $this;
    }
    
    public function setExpedition(bool $expedition): static
    {
        $this->expedition = $expedition;
        
        return $this;
    }
    
    public function setUpdateByAdmin(bool $updateByAdmin): self
    {
        $this->updateByAdmin = $updateByAdmin;
        
        return $this;
    }
    
}
