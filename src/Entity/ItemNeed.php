<?php

namespace App\Entity;

use App\Repository\ItemNeedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ItemNeedRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class ItemNeed
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ency', 'admin_ass'])]
    private ?int $id;
    
    #[ORM\ManyToOne(targetEntity: ItemPrototype::class, fetch: 'EXTRA_LAZY', inversedBy: 'itemNeeds')]
    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['ency', 'admin_ass'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
    private ?ItemPrototype $item;
    
    #[ORM\Column(type: 'smallint')]
    #[Groups(['ency', 'admin_ass'])]
    private ?int $number;
    
    /** @var Collection<ListAssemblage> */
    #[ORM\ManyToMany(targetEntity: ListAssemblage::class, mappedBy: 'itemNeed', fetch: 'EXTRA_LAZY')]
    private Collection $listingAssemblages;
    
    public function __construct(ItemPrototype $item, int $number)
    {
        $this->id                 = $number * 10000 + $item->getId();
        $this->item               = $item;
        $this->number             = $number;
        $this->listingAssemblages = new ArrayCollection();
    }
    
    public function addListingAssemblage(ListAssemblage $listingAssemblage): self
    {
        if (!$this->listingAssemblages->contains($listingAssemblage)) {
            $this->listingAssemblages[] = $listingAssemblage;
            $listingAssemblage->addItemNeed($this);
        }
        
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getItem(): ?ItemPrototype
    {
        return $this->item;
    }
    
    public function setItem(?ItemPrototype $item): self
    {
        $this->item = $item;
        
        return $this;
    }
    
    public function getListingAssemblages(): Collection
    {
        return $this->listingAssemblages;
    }
    
    public function getNumber(): ?int
    {
        return $this->number;
    }
    
    public function setNumber(int $number): self
    {
        $this->number = $number;
        
        return $this;
    }
    
    public function removeListingAssemblage(ListAssemblage $listingAssemblage): self
    {
        if ($this->listingAssemblages->removeElement($listingAssemblage)) {
            $listingAssemblage->removeItemNeed($this);
        }
        
        return $this;
    }
    
}
