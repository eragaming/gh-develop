<?php

namespace App\Entity;

use App\Repository\CategoryObjetRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Table(name: 'category_objet')]
#[ORM\Entity(repositoryClass: CategoryObjetRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE', region: 'prototype_region')]
class CategoryObjet
{
    
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'smallint', nullable: false)]
    #[Groups(['banque', 'general', 'admin_obj', 'carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private int $id;
    
    #[ORM\Column(name: 'nom', type: 'string', length: 24, nullable: false)]
    #[Groups(['banque', 'general', 'admin_obj', 'carte', 'outils_expe', 'inscription_exp_visu', 'comparatif'])]
    private string $nom;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getNom(): ?string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        
        return $this;
    }
    
}
