<?php

namespace App\Utils;

class StringManipulation
{
    public function camelCaseToUnderscore(string $input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
    
    public function underscoreToCamelCase(string $input)
    {
        return lcfirst(str_replace("_", "", ucwords($input, " /_")));
    }
}