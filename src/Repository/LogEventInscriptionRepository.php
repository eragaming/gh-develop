<?php

namespace App\Repository;

use App\Entity\LogEventInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LogEventInscription>
 *
 * @method LogEventInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogEventInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogEventInscription[]    findAll()
 * @method LogEventInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogEventInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogEventInscription::class);
    }

//    /**
//     * @return LogEventInscription[] Returns an array of LogEventInscription objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?LogEventInscription
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
