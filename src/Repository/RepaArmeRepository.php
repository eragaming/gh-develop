<?php

namespace App\Repository;

use App\Entity\RepaArme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RepaArme|null find($id, $lockMode = null, $lockVersion = null)
 * @method RepaArme|null findOneBy(array $criteria, array $orderBy = null)
 * @method RepaArme[]    findAll()
 * @method RepaArme[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepaArmeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RepaArme::class);
    }
    
    // /**
    //  * @return RepaArme[] Returns an array of RepaArme objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RepaArme
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
