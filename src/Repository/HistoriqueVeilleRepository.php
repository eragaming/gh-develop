<?php

namespace App\Repository;

use App\Entity\HistoriqueVeille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HistoriqueVeille|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoriqueVeille|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoriqueVeille[]    findAll()
 * @method HistoriqueVeille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoriqueVeilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoriqueVeille::class);
    }
    
    // /**
    //  * @return HistoriqueVeille[] Returns an array of HistoriqueVeille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?HistoriqueVeille
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
