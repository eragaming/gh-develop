<?php

namespace App\Repository;

use App\Entity\CalculAttaque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CalculAttaque|null find($id, $lockMode = null, $lockVersion = null)
 * @method CalculAttaque|null findOneBy(array $criteria, array $orderBy = null)
 * @method CalculAttaque[]    findAll()
 * @method CalculAttaque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalculAttaqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CalculAttaque::class);
    }
    
    // /**
    //  * @return CalculAttaque[] Returns an array of CalculAttaque objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?CalculAttaque
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
