<?php

namespace App\Repository;

use App\Entity\HerosSkillPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HerosSkillPrototype>
 */
class HerosSkillPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HerosSkillPrototype::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $qb = $this->createQueryBuilder('hsp')
                   ->select('MAX(hsp.id)')
                   ->getQuery();
        
        return ($qb->getSingleScalarResult() ?? 0);
    }

//    /**
//     * @return HerosSkillPrototype[] Returns an array of HerosSkillPrototype objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HerosSkillPrototype
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
