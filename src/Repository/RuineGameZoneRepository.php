<?php

namespace App\Repository;

use App\Entity\RuineGameZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuineGameZone>
 *
 * @method RuineGameZone|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuineGameZone|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuineGameZone[]    findAll()
 * @method RuineGameZone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuineGameZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuineGameZone::class);
    }
    
    //    /**
    //     * @return RuineGameZone[] Returns an array of RuineGameZone objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?RuineGameZone
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
