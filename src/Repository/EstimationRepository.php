<?php

namespace App\Repository;

use App\Entity\Estimation;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Estimation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Estimation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Estimation[]    findAll()
 * @method Estimation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstimationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Estimation::class);
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function derniereEstimation(Ville $ville): ?Estimation
    {
        
        return $this->createQueryBuilder('e')
                    ->where('e.ville = :ville')
                    ->setParameter('ville', $ville)
                    ->orderBy('e.day', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
        
    }
    
    public function minMaxEstimation(int $saison): array
    {
        
        $q = $this->createQueryBuilder('e');
        
        return $q->select('e.day, MIN(e.minJour) as min, MAX(e.maxJour) as max')
                 ->from('App:Ville', 'v')
                 ->where('e.minJour <> 0')
            //->andWhere('e.maxedJour = 1')
                 ->andWhere('e.ville = v.id')
                 ->andWhere('v.saison = :saison')
                 ->setParameter('saison', $saison)
                 ->groupBy('e.day')
                 ->getQuery()
                 ->getResult();
        
    }
    
    // /**
    //  * @return Estimation[] Returns an array of Estimation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Estimation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
