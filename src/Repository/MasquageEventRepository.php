<?php

namespace App\Repository;

use App\Entity\MasquageEvent;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MasquageEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method MasquageEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method MasquageEvent[]    findAll()
 * @method MasquageEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasquageEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MasquageEvent::class);
    }
    
    /**
     * @return array|MasquageEvent[]
     */
    public function eventMasqueUser(User $user): array
    {
        
        return $this->createQueryBuilder('me', 'me.event')
                    ->where('me.user = :user')
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->getResult();
        
    }
    
    // /**
    //  * @return MasquageEvent[] Returns an array of MasquageEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?MasquageEvent
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
