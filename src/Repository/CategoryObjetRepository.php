<?php

namespace App\Repository;

use App\Entity\CategoryObjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoryObjet|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryObjet|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryObjet[]    findAll()
 * @method CategoryObjet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryObjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryObjet::class);
    }
    
    /**
     * @return CategoryObjet[]
     */
    public function findAllExceptInactif(): array
    {
        
        return $this->createQueryBuilder('c')
                    ->andWhere('c.id < 10')
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return CategoryObjet[]
     */
    public function findAllExceptMarqueurs(): array
    {
        
        return $this->createQueryBuilder('c')
                    ->andWhere('c.id < 9')
                    ->getQuery()
                    ->getResult();
        
    }
    
    // /**
    //  * @return CategoryObjet[] Returns an array of CategoryObjet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?CategoryObjet
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
