<?php

namespace App\Repository;

use App\Entity\RuineGameExplorationStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuineGameExplorationStats>
 *
 * @method RuineGameExplorationStats|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuineGameExplorationStats|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuineGameExplorationStats[]    findAll()
 * @method RuineGameExplorationStats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuineGameExplorationStatsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuineGameExplorationStats::class);
    }
    
    //    /**
    //     * @return RuineGameExplorationStats[] Returns an array of RuineGameExplorationStats objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?RuineGameExplorationStats
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
