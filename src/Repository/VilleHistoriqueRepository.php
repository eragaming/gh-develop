<?php

namespace App\Repository;

use App\Entity\HistoriqueVille;
use App\Entity\User;
use App\Entity\VilleHistorique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VilleHistorique>
 *
 * @method VilleHistorique|null find($id, $lockMode = null, $lockVersion = null)
 * @method VilleHistorique|null findOneBy(array $criteria, array $orderBy = null)
 * @method VilleHistorique[]    findAll()
 * @method VilleHistorique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VilleHistoriqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VilleHistorique::class);
    }
    
    /*
     * @return int[]
     */
    
    public function commonTown(User $myUser, User $user): array
    {
        
        $listMyVilleId = $this->createQueryBuilder('vh')
                              ->select('vh.id')
                              ->join(HistoriqueVille::class, 'hv', 'WITH', 'hv.villeHisto = vh.id')
                              ->where('hv.user = :me')
                              ->setParameter('me', $myUser)
                              ->getQuery()
                              ->getScalarResult();
        
        $listMyVilleId = array_column($listMyVilleId, 'id');
        
        $listUserVilleId = $this->createQueryBuilder('vh')
                                ->select('vh.id')
                                ->join(HistoriqueVille::class, 'hv', 'WITH', 'hv.villeHisto = vh.id')
                                ->where('hv.user = :userOther')
                                ->andWhere('vh.id IN (:listVilleId)')
                                ->setParameter('listVilleId', $listMyVilleId)
                                ->setParameter('userOther', $user->getId())
                                ->getQuery()
                                ->getScalarResult();
        
        return array_column($listUserVilleId, 'id');
        
        
    }
    
    /*
     * @return array[]
     */
    
    public function neighborTown(User $user, ?int $saison = null, ?string $phase = null): array
    {
        
        $ville = $this->createQueryBuilder('vh')
                      ->select('vh.id')
                      ->join(HistoriqueVille::class, 'hv', 'WITH', 'hv.villeHisto = vh.id')
                      ->where('hv.user = :user')
                      ->setParameter('user', $user);
        
        if ($saison !== null && $phase !== null) {
            $ville->andWhere('vh.saison = :saison')
                  ->andWhere('vh.phase = :phase')
                  ->setParameter('saison', $saison)
                  ->setParameter('phase', $phase);
        }
        
        return $ville->getQuery()
                     ->getResult();
        
    }
    
    /*
     * @return array[]
     */
    
    public function neighborUser(User $user, ?int $saison = null, ?string $phase = null): array
    {
        $listVilleId = $this->neighborTown($user, $saison, $phase);
        
        
        return $this->createQueryBuilder('vh')
                    ->select('u.pseudo, u.idMyHordes as id, COUNT(hv.id) AS nbVille')
                    ->join(HistoriqueVille::class, 'hv', 'WITH', 'hv.villeHisto = vh.id')
                    ->join(User::class, 'u', 'WITH', 'hv.user = u.id')
                    ->where('vh.id IN (:listVilleId)')
                    ->setParameter('listVilleId', $listVilleId)
                    ->andWhere('u.id <> :user')
                    ->setParameter('user', $user->getId())
                    ->groupBy('hv.user')
                    ->orderBy('nbVille', 'DESC')
                    ->getQuery()
                    ->getResult();
        
        
    }
    
    public function remove(VilleHistorique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);
        
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
    public function save(VilleHistorique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);
        
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return VilleHistorique[] Returns an array of VilleHistorique objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VilleHistorique
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
