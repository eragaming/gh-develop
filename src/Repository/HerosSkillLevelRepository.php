<?php

namespace App\Repository;

use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HerosSkillLevel>
 */
class HerosSkillLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HerosSkillLevel::class);
    }
    
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $qb = $this->createQueryBuilder('hsl')
                   ->select('MAX(hsl.id)')
                   ->getQuery();
        
        return ($qb->getSingleScalarResult() ?? 0);
    }
    
    /**
     * @param HerosSkillType[] $herosSkillType
     * @return HerosSkillLevel[]
     */
    public function getHerosSkillLevel(array $herosSkillType): array
    {
        $qb = $this->createQueryBuilder('hsl')
                   ->select('hsl')
                   ->where('hsl.herosSkillType IN (:herosSkillType)')
                   ->setParameter('herosSkillType', $herosSkillType)
                   ->getQuery();
        
        return $qb->getResult();
    }
    
    
    //    /**
    //     * @return HerosSkillLevel[] Returns an array of HerosSkillLevel objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('h')
    //            ->andWhere('h.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('h.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?HerosSkillLevel
    //    {
    //        return $this->createQueryBuilder('h')
    //            ->andWhere('h.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
