<?php

namespace App\Repository;

use App\Entity\LevelRuinePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LevelRuinePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method LevelRuinePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method LevelRuinePrototype[]    findAll()
 * @method LevelRuinePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LevelRuinePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LevelRuinePrototype::class);
    }
    
    /**
     * @return LevelRuinePrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('lrp', 'lrp.id')->getQuery()->getResult();
    }
    
    // /**
    //  * @return LevelRuinePrototype[] Returns an array of LevelRuinePrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?LevelRuinePrototype
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
