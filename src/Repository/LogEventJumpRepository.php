<?php

namespace App\Repository;

use App\Entity\LogEventJump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LogEventJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogEventJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogEventJump[]    findAll()
 * @method LogEventJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogEventJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogEventJump::class);
    }
    
    // /**
    //  * @return LogEventJump[] Returns an array of LogEventJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?LogEventJump
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
