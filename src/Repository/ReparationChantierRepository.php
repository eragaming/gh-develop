<?php

namespace App\Repository;

use App\Entity\ReparationChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReparationChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReparationChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReparationChantier[]    findAll()
 * @method ReparationChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReparationChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReparationChantier::class);
    }
    
    // /**
    //  * @return ReparationChantier[] Returns an array of ReparationChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ReparationChantier
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
