<?php

namespace App\Repository;

use App\Entity\ItemNeed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemNeed|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemNeed|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemNeed[]    findAll()
 * @method ItemNeed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemNeedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemNeed::class);
    }
    
    // /**
    //  * @return ItemNeed[] Returns an array of ItemNeed objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ItemNeed
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
