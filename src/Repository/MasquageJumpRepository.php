<?php

namespace App\Repository;

use App\Entity\MasquageJump;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MasquageJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method MasquageJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method MasquageJump[]    findAll()
 * @method MasquageJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasquageJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MasquageJump::class);
    }
    
    /**
     * @return array|MasquageJump[]
     */
    public function eventMasqueUser(User $user): array
    {
        
        return $this->createQueryBuilder('mj', 'mj.jump')
                    ->where('mj.user = :user')
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->getResult();
        
    }
    
    // /**
    //  * @return MasquageJump[] Returns an array of MasquageJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?MasquageJump
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
