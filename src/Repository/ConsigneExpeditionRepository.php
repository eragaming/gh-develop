<?php

namespace App\Repository;

use App\Entity\ConsigneExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConsigneExpedition>
 *
 * @method ConsigneExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConsigneExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConsigneExpedition[]    findAll()
 * @method ConsigneExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsigneExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsigneExpedition::class);
    }
    
    //    /**
    //     * @return ConsigneExpedition[] Returns an array of ConsigneExpedition objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?ConsigneExpedition
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
