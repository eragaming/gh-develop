<?php

namespace App\Repository;

use App\Entity\JobPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobPrototype[]    findAll()
 * @method JobPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobPrototype::class);
    }
    
    /**
     * @return JobPrototype[]
     */
    public function findAllExceptAutre(): array
    {
        return $this->createQueryBuilder('j')->where('j.id <> 1')->orderBy('j.isHeros', 'ASC')
                    ->addOrderBy('j.nom', 'ASC')->getQuery()
                    ->getResult();
    }
    
    /**
     * @return JobPrototype[]
     */
    public function findAllExceptChaman(): array
    {
        return $this->createQueryBuilder('j')->where('j.nom <> \'Chaman\'')->orderBy('j.isHeros', 'ASC')
                    ->addOrderBy('j.nom', 'ASC')->getQuery()
                    ->getResult();
    }
    
    /**
     * @Return JobPrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('j', 'j.ref')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return JobPrototype[]
     */
    public function findAllOrdo(): array
    {
        return $this->createQueryBuilder('j')->orderBy('j.isHeros', 'ASC')->addOrderBy('j.nom', 'ASC')->getQuery()
                    ->getResult();
    }
    // /**
    //  * @return JobPrototype[] Returns an array of JobPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?JobPrototype
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
