<?php

namespace App\Repository;

use App\Entity\EtatPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EtatPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method EtatPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method EtatPrototype[]    findAll()
 * @method EtatPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtatPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EtatPrototype::class);
    }
    
    /**
     * @return EtatPrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('e', 'e.id')
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return EtatPrototype[] Returns an array of EtatPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?EtatPrototype
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
