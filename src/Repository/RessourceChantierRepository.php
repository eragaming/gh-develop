<?php

namespace App\Repository;

use App\Entity\RessourceChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RessourceChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method RessourceChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method RessourceChantier[]    findAll()
 * @method RessourceChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessourceChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RessourceChantier::class);
    }
    
    // /**
    //  * @return RessourceChantier[] Returns an array of RessourceChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?RessourceChantier
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
