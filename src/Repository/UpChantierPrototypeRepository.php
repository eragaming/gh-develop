<?php

namespace App\Repository;

use App\Entity\UpChantierPrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpChantierPrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpChantierPrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpChantierPrototype[]    findAll()
 * @method UpChantierPrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpChantierPrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpChantierPrototype::class);
    }
    
    public function getLastId()
    {
        $query = $this->createQueryBuilder('ucp')
                      ->select('ucp.id')
                      ->orderBy('ucp.id', 'DESC')
                      ->setMaxResults(1)
                      ->getQuery();
        return $query->getSingleScalarResult() ?? 0;
    }
    
    // /**
    //  * @return UpChantierPrototype[] Returns an array of UpChantierPrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?UpChantierPrototype
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
