<?php

namespace App\Repository;

use App\Entity\Event;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }
    
    /**
     * @return Event[]
     */
    public function eventActif(): array
    {
        $date = new DateTime('now');
        
        return $this->createQueryBuilder('e')
                    ->where('e.debInscriptionDateAt <= :date')
                    ->andWhere('(e.eventEndAt is null or (e.eventEndAt is not null and e.eventEndAt >= :date))')
                    ->setParameter('date', $date)
                    ->orderBy('e.eventBeginAt', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return Event[]
     */
    public function eventInscriptionOuvert(): array
    {
        $date = new DateTime('now');
        
        return $this->createQueryBuilder('e')
                    ->where('e.debInscriptionDateAt <= :date')
                    ->andWhere('(e.finInscriptionDateAt is not null and e.finInscriptionDateAt >= :date)')
                    ->setParameter('date', $date)
                    ->orderBy('e.eventBeginAt', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return Event[]
     */
    public function eventUserArchve(): array
    {
        $date = new DateTime('now');
        
        return $this->createQueryBuilder('e')
                    ->join('e.listJump', 'j')
                    ->where('e.debInscriptionDateAt <= :date')
                    ->andWhere('(e.finInscriptionDateAt is not null and e.finInscriptionDateAt < :date)')
                    ->setParameter('date', $date)
                    ->orderBy('e.eventBeginAt', 'ASC')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function habiliterGestion(string $idEvent, int $idUser): bool
    {
        $retour = $this->createQueryBuilder('e')
                       ->innerJoin('e.organisateurs', 'listOrga')
                       ->where('e.id = :idEvent')
                       ->andWhere('listOrga.id = :idUser')
                       ->setParameter('idEvent', $idEvent)
                       ->setParameter('idUser', $idUser)
                       ->getQuery()
                       ->getOneOrNullResult();
        
        if ($retour === null) {
            return false;
        } else {
            return true;
        }
    }
    
    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
