<?php

namespace App\Repository;

use App\Entity\RuineGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuineGame>
 *
 * @method RuineGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuineGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuineGame[]    findAll()
 * @method RuineGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuineGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuineGame::class);
    }
    
    //    /**
    //     * @return RuineGame[] Returns an array of RuineGame objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?RuineGame
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
