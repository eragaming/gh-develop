<?php

namespace App\Repository;

use App\Entity\PictoTitrePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PictoTitrePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method PictoTitrePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method PictoTitrePrototype[]    findAll()
 * @method PictoTitrePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PictoTitrePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PictoTitrePrototype::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $qb = $this->createQueryBuilder('p')
                   ->select('MAX(p.id)')
                   ->getQuery();
        
        return ($qb->getSingleScalarResult() ?? 0);
    }
    
    // /**
    //  * @return PictoTitrePrototype[] Returns an array of PictoTitrePrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?PictoTitrePrototype
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
