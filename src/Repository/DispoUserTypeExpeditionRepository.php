<?php

namespace App\Repository;

use App\Entity\DispoUserTypeExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DispoUserTypeExpedition>
 *
 * @method DispoUserTypeExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method DispoUserTypeExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method DispoUserTypeExpedition[]    findAll()
 * @method DispoUserTypeExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispoUserTypeExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DispoUserTypeExpedition::class);
    }
    
    //    /**
    //     * @return DispoUserTypeExpedition[] Returns an array of DispoUserTypeExpedition objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?DispoUserTypeExpedition
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
