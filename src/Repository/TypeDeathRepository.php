<?php

namespace App\Repository;

use App\Entity\TypeDeath;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeDeath|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeDeath|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeDeath[]    findAll()
 * @method TypeDeath[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeDeathRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeDeath::class);
    }
    
    // /**
    //  * @return TypeDeath[] Returns an array of TypeDeath objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeDeath
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
