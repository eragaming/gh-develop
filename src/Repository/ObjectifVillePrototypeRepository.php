<?php

namespace App\Repository;

use App\Entity\ObjectifVillePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ObjectifVillePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectifVillePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectifVillePrototype[]    findAll()
 * @method ObjectifVillePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectifVillePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectifVillePrototype::class);
    }
    
    // /**
    //  * @return ObjectifVillePrototype[] Returns an array of ObjectifVillePrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ObjectifVillePrototype
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
