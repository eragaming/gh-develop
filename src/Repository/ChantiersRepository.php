<?php

namespace App\Repository;

use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Chantiers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chantiers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chantiers[]    findAll()
 * @method Chantiers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChantiersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chantiers::class);
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function chantierConstruit(Ville $ville, int $chantierPrototype): ?Chantiers
    {
        return $this->createQueryBuilder('c')
                    ->where('c.ville = :ville')
                    ->andWhere('c.chantier = :chantier')
                    ->setParameter('ville', $ville)
                    ->setParameter('chantier', $chantierPrototype)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function chantierConstruitJour(Ville $ville, int $chantierPrototype, int $jour): ?Chantiers
    {
        return $this->createQueryBuilder('c')
                    ->where('c.ville = :ville')
                    ->andWhere('c.chantier = :chantier')
                    ->andWhere('c.jour is null or c.jour <= :jour')
                    ->setParameter('ville', $ville)
                    ->setParameter('chantier', $chantierPrototype)
                    ->setParameter('jour', $jour)
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /**
     * @return Chantiers[]
     */
    public function recupChantierAReparer(Ville $ville): array
    {
        return $this->createQueryBuilder('c', 'c.chantier')
                    ->innerJoin(ChantierPrototype::class, 'cp', Join::WITH, 'c.chantier = cp')
                    ->where('c.ville = :ville')
                    ->andWhere('c.pv < cp.pv')
                    ->andWhere('c.detruit <> 1')
                    ->setParameter('ville', $ville)
                    ->getQuery()
                    ->getResult();
    }
    
    public function statsChantierByTypeVille(?int $saison, ?string $phase): array
    {
        // On récupère la somme des chantiers par type de ville
        // (on ne prend pas en compte les chantiers en cours de construction)
        $chantier = $this->createQueryBuilder('c')
                         ->select('cp.id as id, count(c.id) as nbr, cp.nom')
                         ->addSelect('CASE
						   WHEN v.weight <= 15 THEN \'RNE\'
						   WHEN v.weight > 15 AND v.hard =0 THEN \'RE\'
						   WHEN v.hard = 1 THEN \'Pande\'
						   ELSE \'Autre\'
						   END as typeVille')
                         ->innerJoin(ChantierPrototype::class, 'cp', Join::WITH, 'c.chantier = cp')
                         ->join(Ville::class, 'v', Join::WITH, 'c.ville = v');
        
        if ($saison !== null && $phase !== null) {
            $chantier->where('v.saison = :saison')
                     ->andWhere('v.phase = :phase')
                     ->setParameter('saison', $saison)
                     ->setParameter('phase', $phase);
        }
        return $chantier->groupBy('typeVille')
                        ->addGroupBy('cp.nom')
                        ->getQuery()
                        ->getResult();
    }
    
    // /**
    //  * @return Chantiers[] Returns an array of Chantiers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Chantiers
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
