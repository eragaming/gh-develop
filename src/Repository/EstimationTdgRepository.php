<?php

namespace App\Repository;

use App\Entity\EstimationTdg;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EstimationTdg|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstimationTdg|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstimationTdg[]    findAll()
 * @method EstimationTdg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstimationTdgRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstimationTdg::class);
    }
    
    public function getEstimationsJour(Ville $ville, int $day): array
    {
        
        return $this->createQueryBuilder('e')->where('e.day = :day')
                    ->andWhere('e.ville = :ville')
                    ->setParameter(':day', $day)
                    ->setParameter(':ville', $ville)
                    ->getQuery()
                    ->getResult();
        
    }
    
    /**
     * @return EstimationTdg[]|ArrayCollection
     */
    public function getEstimationsJourType(Ville $ville, int $day, int $type): ArrayCollection|array
    {
        
        $array = $this->createQueryBuilder('e')->where('e.day = :day')
                      ->andWhere('e.ville = :ville')
                      ->andWhere('e.typeEstim = :type')
                      ->setParameter(':day', $day)
                      ->setParameter(':ville', $ville)
                      ->setParameter(':type', $type)
                      ->getQuery()
                      ->getResult();
        
        return new ArrayCollection($array);
        
    }
    
    // /**
    //  * @return EstimationTdg[] Returns an array of EstimationTdg objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?EstimationTdg
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
