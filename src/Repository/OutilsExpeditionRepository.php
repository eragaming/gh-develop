<?php

namespace App\Repository;

use App\Entity\OutilsExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OutilsExpedition>
 *
 * @method OutilsExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutilsExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutilsExpedition[]    findAll()
 * @method OutilsExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutilsExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutilsExpedition::class);
    }
    
    //    /**
    //     * @return OutilsExpedition[] Returns an array of OutilsExpedition objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('o.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?OutilsExpedition
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
