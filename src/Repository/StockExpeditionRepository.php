<?php

namespace App\Repository;

use App\Entity\StockExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StockExpedition>
 *
 * @method StockExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockExpedition[]    findAll()
 * @method StockExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockExpedition::class);
    }
    
    //    /**
    //     * @return StockExpedition[] Returns an array of StockExpedition objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?StockExpedition
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
