<?php

namespace App\Repository;

use App\Entity\UpHomePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpHomePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpHomePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpHomePrototype[]    findAll()
 * @method UpHomePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpHomePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpHomePrototype::class);
    }
    
    /**
     * @return UpHomePrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('u', 'u.nom')
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return UpHomePrototype[] Returns an array of UpHomePrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?UpHomePrototype
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
