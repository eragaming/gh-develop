<?php

namespace App\Repository;

use App\Entity\SacType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SacType>
 *
 * @method SacType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SacType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SacType[]    findAll()
 * @method SacType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SacTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SacType::class);
    }
    
    //    /**
    //     * @return SacType[] Returns an array of SacType objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?SacType
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
