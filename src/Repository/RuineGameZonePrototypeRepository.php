<?php

namespace App\Repository;

use App\Entity\RuineGameZonePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RuineGameZonePrototype>
 *
 * @method RuineGameZonePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method RuineGameZonePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method RuineGameZonePrototype[]    findAll()
 * @method RuineGameZonePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuineGameZonePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RuineGameZonePrototype::class);
    }
    
    /**
     * @return RuineGameZonePrototype[]
     */
    public function findClosed(): array
    {
        return $this->createQueryBuilder('z')
                    ->andWhere('z.empreinteItem IS NOT NULL')
                    ->andWhere('z.levelZone = 0')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return RuineGameZonePrototype[]
     */
    public function findDown(): array
    {
        return $this->createQueryBuilder('z')
                    ->andWhere('z.levelZone = -1')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return RuineGameZonePrototype[]
     */
    public function findOuverte(): array
    {
        return $this->createQueryBuilder('z')
                    ->andWhere('z.empreinteItem IS NULL')
                    ->andWhere('z.levelZone = 0')
                    ->getQuery()
                    ->getResult();
    }
    
    /**
     * @return RuineGameZonePrototype[]
     */
    public function findUp(): array
    {
        return $this->createQueryBuilder('z')
                    ->andWhere('z.levelZone = 1')
                    ->getQuery()
                    ->getResult();
    }
    
}
