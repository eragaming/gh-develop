<?php

namespace App\Repository;

use App\Entity\Defense;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Defense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Defense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Defense[]    findAll()
 * @method Defense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Defense::class);
    }
    
    /**
     * @throws NonUniqueResultException
     */
    public function derniereDefense(Ville $ville): ?Defense
    {
        
        return $this->createQueryBuilder('d')
                    ->where('d.ville = :ville')
                    ->setParameter('ville', $ville)
                    ->orderBy('d.day', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
        
    }
    
    // /**
    //  * @return Defense[] Returns an array of Defense objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?Defense
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
