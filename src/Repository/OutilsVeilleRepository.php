<?php

namespace App\Repository;

use App\Entity\OutilsVeille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OutilsVeille|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutilsVeille|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutilsVeille[]    findAll()
 * @method OutilsVeille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutilsVeilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutilsVeille::class);
    }
    
    // /**
    //  * @return OutilsVeille[] Returns an array of OutilsVeille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?OutilsVeille
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
