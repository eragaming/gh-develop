<?php

namespace App\Repository;

use App\Entity\StockVeille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockVeille|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockVeille|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockVeille[]    findAll()
 * @method StockVeille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockVeilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockVeille::class);
    }
    
    // /**
    //  * @return StockVeille[] Returns an array of StockVeille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?StockVeille
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
