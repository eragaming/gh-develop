<?php

namespace App\Repository;

use App\Entity\ContentsVersion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContentsVersion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentsVersion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentsVersion[]    findAll()
 * @method ContentsVersion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentsVersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentsVersion::class);
    }
    
    public function getLastIdRelatif(): int
    {
        $qb = $this->createQueryBuilder('c')->select('MAX(c.idRelatif)');
        return $qb->getQuery()->getSingleScalarResult();
    }
    
    // /**
    //  * @return ContentsVersion[] Returns an array of ContentsVersion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?ContentsVersion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
