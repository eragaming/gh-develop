<?php

namespace App\Repository;

use App\Entity\OutilsChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OutilsChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutilsChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutilsChantier[]    findAll()
 * @method OutilsChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutilsChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutilsChantier::class);
    }
    
    // /**
    //  * @return OutilsChantier[] Returns an array of OutilsChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?OutilsChantier
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
