<?php

namespace App\Repository;

use App\Entity\UpHabitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpHabitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpHabitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpHabitation[]    findAll()
 * @method UpHabitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpHabitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpHabitation::class);
    }
    
    // /**
    //  * @return UpHabitation[] Returns an array of UpHabitation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?UpHabitation
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
