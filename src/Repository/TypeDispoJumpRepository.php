<?php

namespace App\Repository;

use App\Entity\TypeDispoJump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeDispoJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeDispoJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeDispoJump[]    findAll()
 * @method TypeDispoJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeDispoJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeDispoJump::class);
    }
    
    // /**
    //  * @return TypeDispoJump[] Returns an array of TypeDispoJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeDispoJump
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
