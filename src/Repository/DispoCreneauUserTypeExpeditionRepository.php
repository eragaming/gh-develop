<?php

namespace App\Repository;

use App\Entity\DispoCreneauUserTypeExpedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DispoCreneauUserTypeExpedition>
 *
 * @method DispoCreneauUserTypeExpedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method DispoCreneauUserTypeExpedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method DispoCreneauUserTypeExpedition[]    findAll()
 * @method DispoCreneauUserTypeExpedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispoCreneauUserTypeExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DispoCreneauUserTypeExpedition::class);
    }
    
    //    /**
    //     * @return DispoCreneauUserTypeExpedition[] Returns an array of DispoCreneauUserTypeExpedition objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?DispoCreneauUserTypeExpedition
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
