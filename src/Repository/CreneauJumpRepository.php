<?php

namespace App\Repository;

use App\Entity\CreneauJump;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreneauJump|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreneauJump|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreneauJump[]    findAll()
 * @method CreneauJump[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreneauJumpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreneauJump::class);
    }
    
    // /**
    //  * @return CreneauJump[] Returns an array of CreneauJump objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?CreneauJump
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
