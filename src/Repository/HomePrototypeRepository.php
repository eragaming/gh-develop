<?php

namespace App\Repository;

use App\Entity\HomePrototype;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomePrototype|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomePrototype|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomePrototype[]    findAll()
 * @method HomePrototype[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomePrototypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomePrototype::class);
    }
    
    /**
     * @return HomePrototype[]
     */
    public function findAllIndexed(): array
    {
        return $this->createQueryBuilder('h', 'h.id')
                    ->getQuery()
                    ->getResult();
    }
    
    // /**
    //  * @return HomePrototype[] Returns an array of HomePrototype objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?HomePrototype
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
