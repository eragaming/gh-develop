<?php

namespace App\Repository;

use App\Entity\RessourceUpHome;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RessourceUpHome>
 *
 * @method RessourceUpHome|null find($id, $lockMode = null, $lockVersion = null)
 * @method RessourceUpHome|null findOneBy(array $criteria, array $orderBy = null)
 * @method RessourceUpHome[]    findAll()
 * @method RessourceUpHome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessourceUpHomeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RessourceUpHome::class);
    }

//    /**
//     * @return RessourceUpHome[] Returns an array of RessourceUpHome objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RessourceUpHome
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
