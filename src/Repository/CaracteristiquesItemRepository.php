<?php

namespace App\Repository;

use App\Entity\CaracteristiquesItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CaracteristiquesItem>
 *
 * @method CaracteristiquesItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaracteristiquesItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaracteristiquesItem[]    findAll()
 * @method CaracteristiquesItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaracteristiquesItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaracteristiquesItem::class);
    }
    
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastId(): int
    {
        $query = $this->createQueryBuilder('c')
                      ->select('MAX(c.id)')
                      ->getQuery();
        
        return $query->getSingleScalarResult();
    }
    
    //    /**
    //     * @return CaracteristiquesItem[] Returns an array of CaracteristiquesItem objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }
    
    //    public function findOneBySomeField($value): ?CaracteristiquesItem
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
