<?php

namespace App\Repository;

use App\Entity\TypeDecharge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeDecharge|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeDecharge|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeDecharge[]    findAll()
 * @method TypeDecharge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeDechargeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeDecharge::class);
    }
    
    // /**
    //  * @return TypeDecharge[] Returns an array of TypeDecharge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    
    /*
    public function findOneBySomeField($value): ?TypeDecharge
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
