<?php

namespace App\DataFixtures;

use App\Entity\BonusUpChantier;
use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use App\Entity\RessourceChantier;
use App\Entity\UpChantierPrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class ChantierFixtures extends Fixture implements DependentFixtureInterface
{
    public static array $tabTransco = [
        1010 => 28,
        1023 => 2,
        1041 => 1,
        1024 => 3,
        1027 => 4,
        1028 => 6,
        1066 => 5,
        1031 => 7,
        1036 => 14,
        1037 => 9,
        1056 => 8,
        1070 => 11,
        1157 => 10,
        1071 => 13,
        1158 => 12,
        1054 => 15,
        1055 => 16,
        1058 => 17,
        1156 => 18,
        1159 => 24,
        1160 => 27,
        1161 => 26,
        1162 => 25,
        1163 => 19,
        1164 => 20,
        1165 => 21,
        1166 => 22,
        1167 => 23,
        1182 => 143,
        1011 => 50,
        1020 => 31,
        1032 => 29,
        1143 => 30,
        1026 => 34,
        1144 => 33,
        1145 => 32,
        1029 => 36,
        1030 => 35,
        1060 => 41,
        1039 => 37,
        1040 => 38,
        1059 => 39,
        1149 => 40,
        1073 => 42,
        1141 => 43,
        1142 => 44,
        1146 => 49,
        1150 => 45,
        1151 => 46,
        1152 => 47,
        1153 => 48,
        1021 => 52,
        1045 => 51,
        1033 => 76,
        1025 => 53,
        1046 => 58,
        1043 => 54,
        1047 => 55,
        1048 => 56,
        1100 => 57,
        1049 => 59,
        1065 => 60,
        1067 => 61,
        1075 => 63,
        1101 => 62,
        1076 => 64,
        1102 => 69,
        1103 => 70,
        1104 => 65,
        1105 => 67,
        1106 => 66,
        1107 => 71,
        1108 => 75,
        1109 => 68,
        1110 => 74,
        1111 => 73,
        1112 => 72,
        1050 => 103,
        1015 => 96,
        1077 => 95,
        1035 => 77,
        1042 => 78,
        1064 => 79,
        1072 => 80,
        1074 => 94,
        1012 => 82,
        1013 => 81,
        1014 => 83,
        1019 => 85,
        1022 => 84,
        1044 => 86,
        1068 => 87,
        1168 => 88,
        1169 => 89,
        1170 => 101,
        1178 => 99,
        1179 => 100,
        1180 => 97,
        1181 => 98,
        1171 => 102,
        1051 => 137,
        1052 => 107,
        1053 => 104,
        1057 => 105,
        1061 => 106,
        1113 => 108,
        1114 => 120,
        1115 => 119,
        1116 => 118,
        1117 => 116,
        1118 => 117,
        1119 => 112,
        1120 => 113,
        1123 => 114,
        1172 => 115,
        1122 => 109,
        1124 => 121,
        1125 => 111,
        1126 => 124,
        1127 => 110,
        1128 => 125,
        1129 => 136,
        1130 => 133,
        1131 => 135,
        1132 => 127,
        1133 => 128,
        1134 => 134,
        1135 => 131,
        1136 => 122,
        1137 => 129,
        1138 => 132,
        1139 => 130,
        1140 => 123,
        1173 => 126,
        1062 => 142,
        1034 => 140,
        1154 => 139,
        1069 => 138,
        1155 => 141,
        1176 => 144,
        1177 => 146,
    ];
    
    public static array $chantier = [];
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [ObjetFixtures::class];
    }
    
    public function load(ObjectManager $manager):void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $list = $manager->getRepository(ItemPrototype::class)->findAll();
        
        $item_id = array_map(fn($i) => $i->getId(), $list);
        
        $listItems = array_combine($item_id, $list);
        
        $jsonString    = file_get_contents(__DIR__ . '/data/chantiers.json');
        $listChantiers = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $out->writeln('<info>Installing fixtures: ChantierPrototype & Ressources Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de chantiers : ' . count($listChantiers) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listChantiers));
        
        foreach ($listChantiers as $chantier) {
            
            
            if ($chantier['parent'] !== 0) {
                $parent = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['parent']);
            } else {
                $parent = null;
            }
            
            if ($chantier['cat'] !== 0) {
                $cat = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['cat']);
            } else {
                $cat = null;
            }
            
            /* On créer/met à jour les chantiers */
            
            $entity = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['id']);
            
            if ($entity === null) {
                $entity = (new ChantierPrototype());
                $entity->setId((int)$chantier['id']);
            }
            
            $entity->setIcon($chantier['icon'])
                   ->setNom($chantier['nom'])
                   ->setDescription($chantier['desc'])
                   ->setDef($chantier['def'])
                   ->setWater($chantier['water'])
                   ->setPa($chantier['pa'])
                   ->setNiveau($chantier['niveau'])
                   ->setPlan($chantier['plan'])
                   ->setTemp((bool)$chantier['temp'])
                   ->setIndes((bool)$chantier['indes'])
                   ->setPv($chantier['pv'])
                   ->setRuineHo((bool)$chantier['ruineHo'])
                   ->setRuineBu((bool)$chantier['ruineBu'])
                   ->setRuineHs((bool)$chantier['ruineHs'])
                   ->setParent($parent)
                   ->setCatChantier($cat)
                   ->setOrderby((int)$chantier['orderBy'])
                   ->setLevelMax($chantier['levelMax'])
                   ->setOrderByListing($chantier['orderByListing'])
                   ->setIdHordes($chantier['idHordes'])
                   ->setUid($chantier['uid'])
                   ->setActif($chantier['actif']);
            
            
            /* On travaille sur les ressources */
            $ressourcesTab = $chantier['ressources'];
            
            $ressourcesAct = $entity->getRessources();
            
            
            foreach ($ressourcesAct as $ress) {
                if (isset($ressourcesTab[$ress->getItem()->getId()])) {
                    $ress->setNombre($ressourcesTab[$ress->getItem()->getId()]);
                    unset($ressourcesTab[$ress->getItem()->getId()]);
                } else {
                    $entity->removeRessource($ress);
                }
            }
            
            
            foreach ($ressourcesTab as $key => $nbr) {
                if ($nbr === '0') {
                    break;
                }
                
                $ressource = new RessourceChantier();
                $ressource->setNombre((int)$nbr)
                          ->setItem($listItems[$key]);
                
                $entity->addRessource($ressource);
            }
            
            
            $lvlUps = $chantier['levelUps'];
            
            $lvlUpsAct = $entity->getLevelUps();
            
            foreach ($lvlUpsAct as $lvlUp) {
                if (isset($lvlUps[$lvlUp->getLevel()])) {
                    
                    $bonusAMaj = $lvlUps[$lvlUp->getLevel()];
                    
                    foreach ($lvlUp->getBonusUps() as $bonus) {
                        if (isset($bonusAMaj[$bonus->getTypeBonus()])) {
                            $bonus->setValeurUp($bonusAMaj[$bonus->getTypeBonus()]);
                            unset($bonusAMaj[$bonus->getTypeBonus()]);
                        } else {
                            $lvlUp->removeBonusUp($bonus);
                        }
                    }
                    
                    foreach ($bonusAMaj as $keyBonus => $bonusMaj) {
                        $bonus = new UpChantierPrototype();
                        $bonus->setTypeBonus($keyBonus)
                              ->setValeurUp($bonusMaj);
                        $lvlUp->addBonusUp($bonus);
                    }
                } else {
                    $entity->removeLevelUp($lvlUp);
                }
            }
            
            foreach ($lvlUps as $key => $lvlUp) {
                $lvl = new BonusUpChantier();
                $lvl->setLevel($key);
                
                foreach ($lvlUp as $keyUp => $valueUp) {
                    $bonus = new UpChantierPrototype();
                    $bonus->setTypeBonus($keyUp);
                    $bonus->setValeurUp($valueUp);
                    $lvl->addBonusUp($bonus);
                }
                
                $entity->addLevelUp($lvl);
            }
            
            $manager->persist($entity);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
        
        $this->updateChantier($manager, $out, $listChantiers);
        
    }
    
    private function updateChantier(ObjectManager $manager, ConsoleOutput $out, array $listChantiers):void
    {
        
        $out->writeln('<info>Update fixtures: parent on ChantierPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de chantiers à mettre à jour : ' . count($listChantiers) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listChantiers));
        
        foreach ($listChantiers as $chantier) {
            
            if ($chantier['parent'] !== 0) {
                $parent = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['parent']);
            } else {
                $parent = null;
            }
            if ($chantier['cat'] !== 0) {
                $cat = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['cat']);
            } else {
                $cat = null;
            }
            
            /* On créer/met à jour les chantiers */
            
            $entity = $manager->getRepository(ChantierPrototype::class)->find((int)$chantier['id']);
            
            if ($entity !== null) {
                $entity->setParent($parent)
                       ->setCatChantier($cat);
            }
            
            
            $manager->persist($entity);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $processBar->finish();
        $out->write('');
    }
}
