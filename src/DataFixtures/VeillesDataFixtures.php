<?php

namespace App\DataFixtures;

use App\Entity\EtatPrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class VeillesDataFixtures extends Fixture
{
    public static array $etat = [
        [
            'id'        => 1,
            'nom'       => "Alcoolisé",
            'icon'      => 'status_drunk',
            'modSurvie' => 4,
            'modDef'    => 20,
        ],
        [
            'id'        => 2,
            'nom'       => "Drogué",
            'icon'      => 'status_drugged',
            'modSurvie' => 0,
            'modDef'    => 10,
        ],
        [
            'id'        => 3,
            'nom'       => "Terrorisé",
            'icon'      => 'status_terror',
            'modSurvie' => -45,
            'modDef'    => -30,
        ],
        [
            'id'        => 4,
            'nom'       => "Blessé",
            'icon'      => 'status_wound',
            'modSurvie' => -20,
            'modDef'    => -20,
        ],
        [
            'id'        => 5,
            'nom'       => "Infecté",
            'icon'      => 'status_infect',
            'modSurvie' => -20,
            'modDef'    => -15,
        ],
        [
            'id'        => 6,
            'nom'       => "Convalescent",
            'icon'      => 'status_heal',
            'modSurvie' => -10,
            'modDef'    => -10,
        ],
        [
            'id'        => 7,
            'nom'       => "Gueule de bois",
            'icon'      => 'status_hungover',
            'modSurvie' => -5,
            'modDef'    => -10,
        ],
        [
            'id'        => 8,
            'nom'       => "Dépendant",
            'icon'      => 'status_addict',
            'modSurvie' => -10,
            'modDef'    => 15,
        ],
        [
            'id'        => 9,
            'nom'       => "Goule",
            'icon'      => 'status_ghoul',
            'modSurvie' => 5,
            'modDef'    => 0,
        ],
    ];
    
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        $jsonString = file_get_contents(__DIR__ . '/data/etat.json');
        $listEtat   = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        
        $out->writeln('<info>Installing fixtures: EtatJoueur Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre d\'état : ' . count($listEtat) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listEtat));
        
        foreach ($listEtat as $etat) {
            
            $entity = $manager->getRepository(EtatPrototype::class)->find((int)$etat['id']);
            
            if ($entity === null) {
                $entity = new EtatPrototype();
                $entity->setId((int)$etat['id']);
            }
            
            $entity->setNom(($etat['nom']))
                   ->setIcon($etat['icon'])
                   ->setModifDef($etat['modDef'])
                   ->setModifSurvie($etat['modSurvie']);
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
        
    }
}
