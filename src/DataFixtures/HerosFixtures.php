<?php

namespace App\DataFixtures;

use App\Entity\HerosPrototype;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class HerosFixtures extends Fixture
{
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $out->writeln('<info>Installation des fixtures: Heros </info>');
        $out->writeln('');
        /* chargement des héros */
        
        $jsonString = file_get_contents(__DIR__ . '/data/heros.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listHeros = $data;
        
        $countHerosSkill = count($listHeros);
        
        $out->writeln('<comment>Pouvoirs héros : ' . $countHerosSkill . ' fixture entries available.</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start($countHerosSkill);
        
        
        foreach ($listHeros as $key => $hero) {
            
            $entity = $manager->getRepository(HerosPrototype::class)
                              ->findOneBy(['nom' => htmlspecialchars_decode((string)$hero['nom'])]);
            
            if ($entity === null) {
                $entity = new HerosPrototype();
                $entity->setId($key);
            }
            
            $entity->setDescription(htmlspecialchars_decode((string)$hero['description']))
                   ->setNom(htmlspecialchars_decode((string)$hero['nom']))
                   ->setIcon($hero['icon'])
                   ->setJour($hero['jour'])
                   ->setJourCumul($hero['jour_cumul'])
                   ->setPouvActif((bool)$hero['pouv_actif'])
                   ->setPouvBase((bool)$hero['pouv_base'])
                   ->setMethod(($hero['method'] !== '') ? $hero['method'] : null)
                   ->setOrdreRecup(($hero['order'] !== '') ? $hero['order'] : null);
            
            $manager->persist($entity);
            $processBar->advance();
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
