<?php

namespace App\DataFixtures;

use App\Entity\TypeCaracteristique;
use App\Entity\TypeObjet;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class TypeObjetFixtures extends Fixture
{
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out        = new ConsoleOutput();
        $jsonString = file_get_contents(__DIR__ . '/data/typeObjets.json');
        $typeObjets = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $out->writeln('<info>Installing fixtures: type_objet Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de type d\'objet : ' . count($typeObjets) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($typeObjets));
        
        foreach ($typeObjets as $key => $typeObjet) {
            $entity = $manager->getRepository(TypeObjet::class)->findOneBy(['nom' => $typeObjet]);
            
            if ($entity === null) {
                $entity = (new TypeObjet($key + 1))->setNom($typeObjet);
            }
            $manager->persist($entity);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
        
        $jsonString = file_get_contents(__DIR__ . '/data/typeCarac.json');
        $listCarac  = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $out->writeln('<info>Installing fixtures: Type Carac Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de carac : ' . count($listCarac) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listCarac));
        
        foreach ($listCarac as $key => $typeCarac) {
            $entity = $manager->getRepository(TypeCaracteristique::class)->find((int)$typeCarac['id']);
            
            if ($entity === null) {
                $entity = new TypeCaracteristique();
                $entity->setId($typeCarac['id']);
            }
            
            $entity->setNom($typeCarac['nom'])
                   ->setIcon($typeCarac['icon']);
            $manager->persist($entity);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
