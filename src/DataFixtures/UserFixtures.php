<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldPersoOption;
use App\Entity\HerosPrototype;
use App\Entity\User;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [HerosFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_USER)) {
            
            return;
        }
        $out = new ConsoleOutput();
        $out->writeln('<info>Installing fixtures: Users Database</info>');
        $out->writeln('');
        
        $allUser = $manager->getRepository(User::class)->findAll();
        
        $listMHId = array_map(fn($i) => $i->getIdMyHordes(), $allUser);
        
        $listEntityUser = array_combine($listMHId, $allUser);
        
        
        $listUser = $manager->getRepository(OldPersoOption::class)->findAll();
        
        $out->writeln('<comment>Nombre d\'utilisateurs : ' . count($listUser) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listUser));
        
        $couleurHexa = ['#FFFFFF', '#0000FF', '#FF0000', '#FFA500', '#00FF00', '#4B0082', '#00FFFF', '#FF1493'];
        
        $listPouvoir = $manager->getRepository(HerosPrototype::class)->findAll();
        
        $countUser = 0;
        
        foreach ($listUser as $user) {
            
            if (!in_array($user->getTwinid(), $listMHId)) {
                $entity = (new User());
                $entity->setIdMyHordes($user->getTwinid());
            } else {
                $entity = $listEntityUser[$user->getTwinid()];
            }
            
            $entity->setRoles(['ROLE_USER']);
            if ($user->getTwinid() == 239) {
                $entity->setRoles(['ROLE_ADMIN']);
            }
            $entity->setPseudo($user->getPseudo());
            $entity->setAvatar($user->getAvatar());
            $entity->setMapId($user->getMapid());
            $entity->setHeros((bool)$user->getHeros());
            $entity->setDateMaj($user->getDatemaj());
            $entity->setPeriodeRappel($user->getPerioderappel() ?? 0);
            $entity->setPossApag((bool)$user->getPossapag() ?? false);
            $entity->setTemArma((bool)$user->getTemarma() ?? false);
            $entity->setForceMaj((bool)$user->getMajforce() ?? true);
            $entity->setLegend((bool)$user->getLegend() ?? false);
            $entity->setAnnonceNews((bool)$user->getAnnoncenews() ?? false);
            $entity->setFigeMenu((bool)$user->getFigemenu());
            $entity->setCouleurScrut($couleurHexa[(int)$user->getCouleurscrut() ?? 0]);
            $entity->setCouleurKm($couleurHexa[(int)$user->getCouleurkm() ?? 1]);
            $entity->setDerPouv($listPouvoir[($user->getDerpouv() ?? 0) + 5]);
            $entity->setTheme(User::THEME_LIGHT);
            
            $manager->persist($entity);
            $processBar->advance();
            
            $countUser++;
            
            if ($countUser == 200) {
                $manager->flush();
                $countUser = 0;
            }
            
        }
        
        // $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
