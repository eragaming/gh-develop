<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldCalculattaque;
use App\Entity\Ancestor\OldEstimjour;
use App\Entity\CalculAttaque;
use App\Entity\EstimationTdg;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldToNewEstimTdgFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected DataCollection  $collection,
        protected ConfMaster      $confMaster,
    )
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_OLD_ESTIM)) {
            return;
        }
        
        $manager = $this->managerRegistry->getManager();
        
        $out = new ConsoleOutput();
        
        $nombreVille       = $manager->getRepository(OldEstimjour::class)->countMigration()[1] ?? 0;
        $nombreVilleCalcul = $manager->getRepository(OldCalculattaque::class)->countMigration()[1] ?? 0;
        
        try {
            $out->writeln('<info>Installing fixtures: estimations tdg and calcul attaque Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre de ville pour les estimations : ' . $nombreVille . ' .</comment>');
            $out->writeln('');
            
            $this->majEstimation($manager, $out);
            
            $out->writeln('<comment>Nombre de ville pour les calculs d\'attaque : ' . $nombreVille . ' .</comment>');
            $out->writeln('');
            
            $this->majCalcul($manager, $out);
            
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()} - {$this->mapId}</error>");
        }
        
    }
    
    private function majCalcul(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        
        $calculOlds = $manager->getRepository(OldCalculattaque::class)->findAll();
        
        $output->writeln("<comment>Progression calcul globale par ville : </comment>");
        
        $processBarGlobale = new ProgressBar($output->section());
        $processBarGlobale->start(count($calculOlds));
        
        foreach ($calculOlds as $calculOld) {
            $mapId = (int)$calculOld->getMapid();
            
            $villeExist =
                $manager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
            
            if ($villeExist === null) {
                $processBarGlobale->advance();
                continue;
            }
            
            $calculAttaque = new CalculAttaque();
            
            $calculAttaque->setDay($calculOld->getDay());
            
            if ($calculOld->getTypeestim() === 1) {
                $calculAttaque->setMargeEstim($calculOld->getMarge())
                              ->setAtkEstim($calculOld->getAtkesti())
                              ->setAtkEstimMarge($calculOld->getAtkestimarge())
                              ->setAtkVisee($calculOld->getAtkvisee());
            }
            
            /**
             * @var OldCalculattaque $nextCalcul
             */
            $nextCalcul = next($calculOlds);
            
            if ($nextCalcul->getMapid() === $calculOld->getMapid() && $nextCalcul->getDay() === $calculOld->getDay() &&
                $nextCalcul->getTypeestim() != $calculOld->getTypeestim()) {
                
                if ($nextCalcul->getTypeestim() == 2) {
                    $calculAttaque->setMargePlanif($nextCalcul->getMarge())
                                  ->setAtkPlanif($nextCalcul->getAtkesti())
                                  ->setAtkPlanifMarge($nextCalcul->getAtkestimarge())
                                  ->setAtkPlanifVisee($nextCalcul->getAtkvisee());
                }
                
            }
            
            $villeExist->addCalculAttaque($calculAttaque);
            
            $manager->persist($villeExist);
            $processBarGlobale->advance();
            
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        $output->writeln('');
        
    }
    
    private function majEstimation(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        
        $estimationOlds = $manager->getRepository(OldEstimjour::class)->findAll();
        
        $output->writeln("<comment>Progression estimations globale par ville : </comment>");
        
        $processBarGlobale = new ProgressBar($output->section());
        $processBarGlobale->start(count($estimationOlds));
        
        foreach ($estimationOlds as $estimationOld) {
            $mapId = (int)$estimationOld->getMapId();
            
            $villeExist =
                $manager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
            
            if ($villeExist === null) {
                $processBarGlobale->advance();
                continue;
            }
            
            $estimation = new EstimationTdg();
            
            $estimation->setDay($estimationOld->getDay())
                       ->setTypeEstim($estimationOld->getTypeEstim())
                       ->setValPourcentage($estimationOld->getPour100())
                       ->setMinEstim($estimationOld->getMinEstim())
                       ->setMaxEstim($estimationOld->getMaxEstim());
            
            $villeExist->addEstimationsTdg($estimation);
            
            $manager->persist($villeExist);
            $processBarGlobale->advance();
            
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        $output->writeln('');
        
    }
}
