<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldOutilchantier;
use App\Entity\Ancestor\OldOutilchantierpadispo;
use App\Entity\Ancestor\OldOutilchantiertableur;
use App\Entity\Ancestor\OldOutilchantierupable;
use App\Entity\Ancestor\OldOutilchantieruphabitation;
use App\Entity\Ancestor\OldOutildecharge;
use App\Entity\Ancestor\OldOutildechargetab;
use App\Entity\Ancestor\OldOutilveille;
use App\Entity\Ancestor\OldOutilveillehistorique;
use App\Entity\Ancestor\OldOutilveilleplan;
use App\Entity\Ancestor\OldOutilveillestock;
use App\Entity\Ancestor\OldReparationPande;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Decharges;
use App\Entity\EtatPrototype;
use App\Entity\HistoriqueVeille;
use App\Entity\ItemPrototype;
use App\Entity\Outils;
use App\Entity\OutilsChantier;
use App\Entity\OutilsDecharge;
use App\Entity\OutilsReparation;
use App\Entity\OutilsVeille;
use App\Entity\ProgrammeChantier;
use App\Entity\RegroupementItemsDecharge;
use App\Entity\ReparationChantier;
use App\Entity\SacVeilleur;
use App\Entity\StockVeille;
use App\Entity\User;
use App\Entity\Veilleur;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldToNewOutilsFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected DataCollection  $collection,
        protected ConfMaster      $confMaster,
    )
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class, RegroupementItemsFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_OLD_OUTILS)) {
            return;
        }
        
        $manager = $this->managerRegistry->getManager();
        
        $out = new ConsoleOutput();
        
        // récupération de la totalité des réparations et des décharges
        
        $nbrDecharge   = count($manager->getRepository(OldOutildecharge::class)->findAll());
        $nbrReparation = count($manager->getRepository(OldReparationPande::class)->findAll());
        $nbrChantier   = count($manager->getRepository(OldOutilchantier::class)->findAll());
        $nbrVeille     = count($manager->getRepository(OldOutilveille::class)->findAll());
        
        
        try {
            $out->writeln('<info>Installing fixtures: Outils Database</info>');
            $out->writeln('');
            
            $out->writeln('<comment>Nombre de décharges : ' . $nbrDecharge . ' .</comment>');
            $out->writeln('');
            $this->majDecharge($manager, $out);
            
            $out->writeln('<comment>Nombre de repa : ' . $nbrReparation . ' .</comment>');
            $out->writeln('');
            $this->majRepa($manager, $out);
            
            /*    $out->writeln('<comment>Nombre d\'outils chantiers : '.$nbrChantier.' .</comment>');
                $out->writeln('');
                $this->majOutilsChantiers($manager, $out);
    
                $out->writeln('<comment>Nombre d\'outils veille : '.$nbrVeille.' .</comment>');
                $out->writeln('');
                $this->majOutilsVeille($manager, $out);*/
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()}</error>");
        }
        
    }
    
    private function majDecharge(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $out->writeln('<comment>Progression décharge existante : </comment>');
        
        $listDecharge = $manager->getRepository(OldOutildecharge::class)->findAll();
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listDecharge));
        
        foreach ($listDecharge as $decharge) {
            // verification existance de la ville
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $decharge->getMapId(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            $listingRgpt = $manager->getRepository(RegroupementItemsDecharge::class)->findAllIndexed();
            
            if ($ville === null) {
                continue;
            } else {
                // verification si un outil n'existe pas pour le jour en question
                $outils = $manager->getRepository(Outils::class)->findOneBy(['Ville' => $ville,
                                                                             'day'   => $decharge->getDay()]);
                
                if ($outils === null) {
                    $outils = new Outils($ville, $decharge->getDay());
                }
                
                if ($outils->getOutilsDecharge() === null) {
                    $outilDecharge = new OutilsDecharge();
                } else {
                    $outilDecharge = $outils->getOutilsDecharge();
                }
                
                $outilDecharge->setCreatedAt($decharge->getDateCreation());
                
                $userCreateur =
                    $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $decharge->getTwinIdCrea()]);
                if ($userCreateur === null) {
                    continue;
                }
                $outilDecharge->setCreatedBy($userCreateur);
                
                $outilDecharge->setModifyAt($decharge->getDateModif() == $decharge->getDateCreation() ? null :
                                                $decharge->getDateModif());
                
                if ($decharge->getTwinIdCrea() == $decharge->getTwinIdMod() || $decharge->getTwinIdMod() == null) {
                    $outilDecharge->setModifyBy(null);
                } else {
                    $userModif =
                        $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $decharge->getTwinIdMod()]);
                    $outilDecharge->setModifyBy($userModif);
                }
                
                $outilDecharge->setDefTotale($decharge->getDefDecharge());
                $outilDecharge->setOutils($outils);
                
                // Récupération de la decharge
                
                $listRessources = $manager->getRepository(OldOutildechargetab::class)
                                          ->findBy(['idOutilDecharge' => $decharge->getIdOutilDecharge()]);
                
                foreach ($listRessources as $ressource) {
                    $ressourceDecharge = new Decharges();
                    
                    $ressourceDecharge->setRegroupItems($listingRgpt[$ressource->getIdRessource()])
                                      ->setDefByItem(0)
                                      ->setNbrEstime($ressource->getNbRessEstim())
                                      ->setNbrUtilise($ressource->getNbRessJete());
                    
                    $outilDecharge->addDecharge($ressourceDecharge);
                }
                
                $outils->setOutilsDecharge($outilDecharge);
                
                $manager->persist($outils);
                $processBarGlobale->advance();
                
            }
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        
        $out->writeln(' ');
        
    }
    
    private function majOutilsChantiers(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $out->writeln('<comment>Progression outils chantiers existant : </comment>');
        
        $listOutilsChantiers = $manager->getRepository(OldOutilchantier::class)->findAll();
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listOutilsChantiers));
        
        foreach ($listOutilsChantiers as $oldChantier) {
            // verification existance de la ville
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $oldChantier->getMapId(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            $listingChantiers = $manager->getRepository(ChantierPrototype::class)->findAllIndexed();
            
            if ($ville === null) {
                continue;
            } else {
                // verification si un outil n'existe pas pour le jour en question
                $outils =
                    $manager->getRepository(Outils::class)->findOneBy(
                        ['Ville' => $ville, 'day' => $oldChantier->getDay()],
                    );
                
                if ($outils === null) {
                    $outils = new Outils($ville, $oldChantier->getDay());
                }
                
                
                if ($outils->getOutilsChantier() === null) {
                    $outilsChantier = new OutilsChantier();
                } else {
                    $outilsChantier = $outils->getOutilsChantier();
                }
                
                $outilsChantier->setOutils($outils);
                
                $outilsChantier->setCreatedAt($oldChantier->getDateCreation());
                
                $userCreateur =
                    $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $oldChantier->getIdCreateur()]);
                if ($userCreateur === null) {
                    continue;
                }
                
                $outilsChantier->setCreatedBy($userCreateur);
                
                $outilsChantier->setModifyAt($oldChantier->getDateModif() == $oldChantier->getDateCreation() ? null :
                                                 $oldChantier->getDateModif());
                
                if ($oldChantier->getIdModificateur() == $oldChantier->getIdCreateur() ||
                    $oldChantier->getIdModificateur() == null) {
                    $outilsChantier->setModifyBy(null);
                } else {
                    $userModif = $manager->getRepository(User::class)
                                         ->findOneBy(['idMyHordes' => $oldChantier->getIdModificateur()]);
                    $outilsChantier->setModifyBy($userModif);
                }
                
                $paDispo =
                    $manager->getRepository(OldOutilchantierpadispo::class)
                            ->findOneBy(['idOutilChantier' => $oldChantier->getIdOutilChantier()]);
                
                $oldUpChantier =
                    $manager->getRepository(OldOutilchantierupable::class)
                            ->findOneBy(['idOutilChantier' => $oldChantier->getIdOutilChantier()]);
                
                $outilsChantier->setEcoRessource($oldChantier->getEcoRessou() ?? false)
                               ->setUtilBDE($oldChantier->getUtilisationBDE())
                               ->setPresScie($oldChantier->getPresenceScie())
                               ->setNbrOuvrier($paDispo->getNbOuv())
                               ->setNbrCampeur($paDispo->getNbCamp())
                               ->setNbrMort($paDispo->getNbMort())
                               ->setRationEau((bool)$paDispo->getRationEau())
                               ->setRationNourriture((bool)$paDispo->getRationNour())
                               ->setNbrMaxEau($paDispo->getNbEau())
                               ->setNbrMaxNourriture($paDispo->getNbNour())
                               ->setTypeNourriture($paDispo->getTypeNour())
                               ->setNbrAlcoolExpe($paDispo->getNbAlcoolExpe())
                               ->setNbrAlcoolVeille($paDispo->getNbAlcoolVeille())
                               ->setNbrDrogueExpe($paDispo->getNbDrogueExpe())
                               ->setNbrDrogueVeille($paDispo->getNbDrogueVeille())
                               ->setNbrDrogueAlcool($paDispo->getNbAlcoolDrogue())
                               ->setNbrPAFao(0)
                               ->setNbrPALegendaire(0)
                               ->setPaChantier(0)
                               ->setDefChantier(0)
                               ->setBricotVert((bool)$oldChantier->getUtilKitVert())
                               ->setNbrKitVert($oldChantier->getNbUtilKitVert())
                               ->setPaTransfo(0)
                               ->setPaHabitation(0)
                               ->setPaReparationArme(0)
                               ->setPaTdga($oldChantier->getPaTDGA())
                               ->setPaTotal($oldChantier->getNbPAInit());
                
                if ($oldUpChantier === null || $oldUpChantier->getIdChantierUpdate() == 0) {
                    $outilsChantier->setLvlUp(null)
                                   ->setChantierUp(null);
                } else {
                    $outilsChantier->setLvlUp($oldUpChantier->getLvlUpdate())
                                   ->setChantierUp($listingChantiers[ChantierFixtures::$tabTransco[$oldUpChantier->getIdChantierUpdate()]]
                                                   ?? null);
                }
                
                $oldProg = $manager->getRepository(OldOutilchantiertableur::class)
                                   ->findBy(['idOutilChantier' => $oldChantier->getIdOutilChantier()]);
                
                foreach ($oldProg as $oldChTab) {
                    $newPgmChantier = new ProgrammeChantier();
                    
                    if ($oldChTab->getIdChantierMere() == -1 || $oldChTab->getLvlStatut() == -1) {
                        continue;
                    }
                    
                    $newPgmChantier->setChantier($listingChantiers[ChantierFixtures::$tabTransco[$oldChTab->getIdChantierFille()]]
                                                 ?? null)
                                   ->setBde($oldChTab->getBde())
                                   ->setFinir((bool)$oldChTab->getLvlStatut())
                                   ->setPaALaisser($oldChTab->getPaLaisser())
                                   ->setPriority($oldChTab->getNumOrdre());
                    
                    $outilsChantier->addChantiersProgramme($newPgmChantier);
                    
                }
                
                
                $outils->setOutilsChantier($outilsChantier);
                
                $manager->persist($outils);
                $processBarGlobale->advance();
                
            }
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        
        $out->writeln(' ');
        
    }
    
    private function majOutilsVeille(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $out->writeln('<comment>Progression veille existante : </comment>');
        
        $listVeille = $manager->getRepository(OldOutilveille::class)->findAll();
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listVeille));
        
        $listingObjet = $manager->getRepository(ItemPrototype::class)->findAllIndexed();
        $listingEtat  = $manager->getRepository(EtatPrototype::class)->findAllIndexed();
        
        foreach ($listVeille as $oldVeille) {
            // verification existance de la ville
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $oldVeille->getMapId(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            
            if ($ville === null) {
                continue;
            } else {
                // verification si un outil n'existe pas pour le jour en question
                $outils = $manager->getRepository(Outils::class)->findOneBy(['Ville' => $ville,
                                                                             'day'   => $oldVeille->getDay()]);
                
                if ($outils === null) {
                    $outils = new Outils($ville, $oldVeille->getDay());
                }
                
                if ($outils->getOutilsVeille() === null) {
                    $outilsVeille = new OutilsVeille();
                } else {
                    $outilsVeille = $outils->getOutilsVeille();
                }
                
                $outilsVeille->setCreatedAt($oldVeille->getDateCreation());
                
                $userCreateur =
                    $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $oldVeille->getTwinIdCrea()]);
                if ($userCreateur === null) {
                    continue;
                }
                $outilsVeille->setCreatedBy($userCreateur);
                
                $outilsVeille->setModifyAt($oldVeille->getDateModif() == $oldVeille->getDateCreation() ? null :
                                               $oldVeille->getDateModif());
                
                if ($oldVeille->getTwinIdMod() == $oldVeille->getTwinIdCrea() || $oldVeille->getTwinIdMod() == null) {
                    $outilsVeille->setModifyBy(null);
                } else {
                    $userModif =
                        $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $oldVeille->getTwinIdMod()]);
                    $outilsVeille->setModifyBy($userModif);
                }
                
                $outilsVeille->setDefVeilleur($oldVeille->getDefVeilleur());
                
                
                // Récupération du plan de veilles
                /**
                 * @var OldOutilveilleplan[] $listPlansVeille
                 */
                $listPlansVeille = $manager->getRepository(OldOutilveilleplan::class)
                                           ->createQueryBuilder('oo', 'oo.twinId')
                                           ->where('oo.idOutilVeille = :idOutils')
                                           ->setParameter('idOutils', $oldVeille->getIdOutilVeille())
                                           ->getQuery()
                                           ->getResult();
                
                foreach ($listPlansVeille as $planVeille) {
                    
                    $veilleur = new Veilleur();
                    
                    // Recherche de l'user
                    $userVeilleur =
                        $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $planVeille->getTwinId()]);
                    
                    if ($userVeilleur === null) {
                        continue;
                    }
                    
                    $veilleur->setVeilleValide((bool)$planVeille->getVeilleValid())
                             ->setBesoinVeilleur((bool)$planVeille->getBesoinveilleur())
                             ->setCitoyen($userVeilleur)
                             ->setDrogueExpe((bool)$planVeille->getDrogueExpe())
                             ->setSurvieJour(0);
                    
                    
                    $nbrItemSac = 0;
                    
                    if ($planVeille->getSac() != '') {
                        // Préparation du sac Veilleur et des états
                        $itemSac = array_map(fn($i) => explode('.', $i), explode('|', (string)$planVeille->getSac()));
                        
                        foreach ($itemSac[0] as $key => $item) {
                            
                            $sacItem = new SacVeilleur();
                            
                            $sacItem->setNombre($itemSac[1][$key])
                                    ->setItem($listingObjet[ObjetFixtures::$tabObjetTransco[$item]]);
                            
                            $veilleur->addSacVeilleur($sacItem);
                            
                            $nbrItemSac += $itemSac[1][$key];
                            
                        }
                    }
                    
                    $veilleur->setNbreObjet($nbrItemSac);
                    
                    // liste des états
                    $tabEtatJoueur = explode('.', (string)$planVeille->getEtatVeilleur());
                    
                    foreach ($tabEtatJoueur as $etat) {
                        
                        $veilleur->addListeEtat($listingEtat[(int)$etat + 1]);
                        
                    }
                    
                    
                    $outilsVeille->addVeilleur($veilleur);
                    
                    
                }
                
                
                $outilsVeille->setOutils($outils);
                
                
                $outils->setOutilsVeille($outilsVeille);
                
                $manager->persist($outils);
                
                
                // Traitement des stocks maintenant
                
                $stockVille =
                    $manager->getRepository(OldOutilveillestock::class)->findOneBy(['mapId' => $oldVeille->getMapId()]);
                
                // Eclatement du stock item et nombre
                $stockItem = array_map(fn($i) => explode('.', $i), explode('|', (string)$stockVille->getObjet()));
                foreach ($stockItem[0] as $key => $item) {
                    
                    $stock = new StockVeille();
                    
                    $stock->setNbrDispo($stockItem[1][$key])
                          ->setItem($listingObjet[ObjetFixtures::$tabObjetTransco[$item]]);
                    
                    $ville->addStockVeille($stock);
                    
                }
                
                // Traitement de l'historique de veille
                $listHistorique =
                    $manager->getRepository(OldOutilveillehistorique::class)->findBy(['mapId' => $ville->getMapId()]);
                
                foreach ($listHistorique as $historique) {
                    
                    $userHisto =
                        $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $historique->getTwinId()]);
                    
                    $newHisto = new HistoriqueVeille();
                    
                    $newHisto->setCitoyen($userHisto)
                             ->setJour($historique->getDay());
                    
                    if ($historique->getAlcool()) {
                        $newHisto->addListeEtat($listingEtat[1]);
                    }
                    if ($historique->getGoule()) {
                        $newHisto->addListeEtat($listingEtat[9]);
                    }
                    
                    $newHisto->setVeille((bool)$historique->getVeille());
                    
                    $ville->addHistoriqueVeille($newHisto);
                    
                }
                
                
                $manager->persist($ville);
                
                
                $processBarGlobale->advance();
                
            }
        }
        
        $processBarGlobale->finish();
        $manager->flush();
        
        $out->writeln(' ');
        
    }
    
    private function majRepa(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        
        $out->writeln('<comment>Progression répa existante : </comment>');
        
        $listReparation = $manager->getRepository(OldReparationPande::class)->findAll();
        
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($listReparation));
        
        foreach ($listReparation as $reparation) {
            // verification existance de la ville
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $reparation->getMapId(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            $listingChantiers = $manager->getRepository(ChantierPrototype::class)->findAllIndexed();
            
            if ($ville === null) {
                continue;
            } else {
                // verification si un outil n'existe pas pour le jour en question
                $outils = $manager->getRepository(Outils::class)->findOneBy(['Ville' => $ville,
                                                                             'day'   => $reparation->getJourR()]);
                
                if ($outils === null) {
                    $outils = new Outils($ville, $reparation->getJourR());
                }
                
                if ($outils->getOutilsReparation() === null) {
                    $outilReparation = new OutilsReparation();
                } else {
                    $outilReparation = $outils->getOutilsReparation();
                }
                
                $outilReparation->setCreatedAt($reparation->getDateCrea());
                
                $userCreateur =
                    $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $reparation->getTwinIdC()]);
                if ($userCreateur === null) {
                    continue;
                }
                $outilReparation->setCreatedBy($userCreateur);
                
                $outilReparation->setModifyAt($reparation->getDateMaj() == $reparation->getDateCrea() ? null :
                                                  $reparation->getDateMaj());
                
                if ($reparation->getTwinIdC() == $reparation->getTwinIdM() || $reparation->getTwinIdM() == null) {
                    $outilReparation->setModifyBy(null);
                } else {
                    $userModif =
                        $manager->getRepository(User::class)->findOneBy(['idMyHordes' => $reparation->getTwinIdM()]);
                    $outilReparation->setModifyBy($userModif);
                }
                
                $outilReparation->setDefBase($reparation->getDefBase())
                                ->setGainDef($reparation->getGainDef())
                                ->setPaTot($reparation->getPaTot())
                                ->setTwoStep($reparation->getRepa70p100() == 1);
                
                $outilReparation->setOutils($outils);
                
                // Mise en place des réparations
                
                $listChantiersRepa = explode('.', (string)$reparation->getId());
                $pvATab            = explode('.', (string)$reparation->getPvA());
                $repaTab           = explode('.', (string)$reparation->getRepaC());
                
                $paRepaTab = array_map(fn(string $paRepa) => explode('.', $paRepa),
                    explode('|', (string)$reparation->getPaRepa()));
                $gDefTab   =
                    array_map(fn(string $gDef) => explode('.', $gDef), explode('|', (string)$reparation->getGDef()));
                
                foreach ($listChantiersRepa as $key => $idChantier) {
                    $reparationChantier = new ReparationChantier();
                    
                    $chantierARepa = $listingChantiers[ChantierFixtures::$tabTransco[$idChantier]];
                    
                    $recupDefActuel =
                        $manager->getRepository(Chantiers::class)->findOneBy(['ville'    => $ville,
                                                                              'chantier' => $chantierARepa]);
                    
                    $reparationChantier->setPvActuel($pvATab[$key])
                                       ->setDefActuelle($recupDefActuel?->getDef() ?? 0)
                                       ->setPctRepa($repaTab[$key])
                                       ->setPaRepa70($paRepaTab[0][$key])
                                       ->setPaRepa99($paRepaTab[1][$key])
                                       ->setPaRepa100($paRepaTab[2][$key])
                                       ->setGainDef70($gDefTab[0][$key])
                                       ->setGainDef99($gDefTab[1][$key])
                                       ->setGainDef100($gDefTab[2][$key])
                                       ->setChantier($listingChantiers[ChantierFixtures::$tabTransco[$idChantier]]);
                    
                    $outilReparation->addReparationChantier($reparationChantier);
                }
                
                $outils->setOutilsReparation($outilReparation);
                
                $manager->persist($outils);
                $processBarGlobale->advance();
                
            }
        }
        
        $manager->flush();
        $processBarGlobale->finish();
        
        $out->writeln(' ');
        
    }
}
