<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldBankVille;
use App\Entity\Ancestor\OldChantiersVille;
use App\Entity\Ancestor\OldCitizenDetail;
use App\Entity\Ancestor\OldCitizenVille;
use App\Entity\Ancestor\OldDefVille;
use App\Entity\Ancestor\OldEstimVille;
use App\Entity\Ancestor\OldExpeVilleh;
use App\Entity\Ancestor\OldInfoVille;
use App\Entity\Ancestor\OldMapVilleBat;
use App\Entity\Ancestor\OldMapVilleItem;
use App\Entity\Ancestor\OldNewsVille;
use App\Entity\Ancestor\OldUpgradeVille;
use App\Entity\Banque;
use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\CleanUpCadaver;
use App\Entity\Defense;
use App\Entity\Estimation;
use App\Entity\ExpeHordes;
use App\Entity\HerosPrototype;
use App\Entity\HomePrototype;
use App\Entity\InfoVille;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\Journal;
use App\Entity\MapBat;
use App\Entity\MapItem;
use App\Entity\TypeDeath;
use App\Entity\UpChantier;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldToNewFixtures extends Fixture implements DependentFixtureInterface
{
    
    private static array $tabJobTransCo = [
        ''         => 'looser',
        'basic'    => 'basic',
        'collec'   => 'dig',
        'eclair'   => 'vest',
        'guardian' => 'shield',
        'hunter'   => 'book',
        'tech'     => 'tech',
        'tamer'    => 'tamer',
        'shaman'   => 'shaman',
    ];
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected DataCollection  $collection,
        protected ConfMaster      $confMaster,
    )
    {
    
    }
    
    public function getDependencies(): array
    {
        return [TypeDechargeFixtures::class, UserFixtures::class, BatFixtures::class, CitizenFixtures::class];
    }
    
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_OLD_MIGRATION_VILLE)) {
            return;
        }
        
        ini_set('memory_limit', '8192M');
        
        $manager = $this->managerRegistry->getManager();
        
        $out = new ConsoleOutput();
        
        $nombreVille = $manager->getRepository(OldInfoVille::class)->countMigration()[1] ?? 0;
        
        try {
            $out->writeln('<info>Installing fixtures: ville Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre de ville : ' . $nombreVille . ' .</comment>');
            $out->writeln('');
            
            //for ($i = 1; $i <= ceil($nombreVille / 10); $i++) {
            //$this->insert_ville($manager, $out, $i);
            $this->insert_ville($manager, $out);
            //$out->writeln('');
            //}
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()} - {$this->mapId}</error>");
        }
        
    }
    
    private function insert_ville(ObjectManager $manager, ConsoleOutputInterface $out): void
    {
        //Recupération de toutes les villes
        
        $infoVilles = $manager->getRepository(OldInfoVille::class)->migrationVille();
        //$infoVilles = $manager->getRepository(OldInfoVille::class)->findBy(['mapid'=>781]);
        
        $out->writeln('<comment>Progression globale / progression ville par ville : </comment>');
        $processBarGlobale = new ProgressBar($out->section());
        $processBarGlobale->start(count($infoVilles));
        
        $processBar = new ProgressBar($out->section());
        
        foreach ($infoVilles as $infoVille) {
            
            $mapId = (int)$infoVille->getMapid();
            
            $villeExist =
                $manager->getRepository(Ville::class)->findOneBy(['mapId' => $mapId, 'origin' => Ville::ORIGIN_MH]);
            
            if ($villeExist) {
                $processBar->finish();
                $processBarGlobale->advance();
                continue;
            }
            
            $this->mapId = $mapId;
            $ville       = new Ville($mapId, Ville::ORIGIN_MH);
            
            $tableImpact = 10;
            
            $processBar->start($tableImpact);
            
            //$banqueVille = $bankVilles[$mapId];
            
            $ville->setNom(($infoVille->getNomv()))
                  ->setJour((int)$infoVille->getJourv())
                  ->setWater($infoVille->getWater())
                  ->setPosX($infoVille->getX())
                  ->setPosY($infoVille->getY())
                  ->setBonus($infoVille->getBonuspts())
                  ->setWeight($infoVille->getLar())
                  ->setHeight($infoVille->getLar())
                  ->setPrived((bool)$infoVille->getPrivv())
                  ->setPorte((bool)$infoVille->getPortev())
                  ->setHard((bool)$infoVille->getHardv())
                  ->setChaos((bool)$infoVille->getChaos())
                  ->setDevast((bool)$infoVille->getDevast())
                  ->setSaison($infoVille->getSaison())
                  ->setDateTime($infoVille->getDatemaj());
            
            $processBar->advance();
            
            $bank_old = $manager->getRepository(OldBankVille::class)->find($infoVille->getMapid());
            if ($bank_old) {
                
                $tabid     = explode('.', (string)$bank_old->getId());
                $tabBroked = explode('.', (string)$bank_old->getBroken());
                $tabQte    = explode('.', (string)$bank_old->getQto());
                
                foreach ($tabid as $keyObjet => $id) {
                    $item =
                        $manager->getRepository(ItemPrototype::class)->find(ObjetFixtures::$tabObjetTransco[$id]);
                    
                    $itemBank = new Banque();
                    $itemBank->setItem($item)
                             ->setBroked($tabBroked[$keyObjet])
                             ->setNombre($tabQte[$keyObjet]);
                    $ville->addBanque($itemBank);
                    
                }
            }
            $processBar->advance();
            
            /* ChantierVille */
            $chantier_old = $manager->getRepository(OldChantiersVille::class)->find($infoVille->getMapid());
            if ($chantier_old) {
                
                if ($chantier_old->getIdchantier() != '') {
                    
                    $tabIdChantier = explode('.', (string)$chantier_old->getIdchantier());
                    $tabDef        = explode('.', (string)$chantier_old->getDef());
                    $tabLife       = explode('.', (string)$chantier_old->getLife());
                    
                    foreach ($tabIdChantier as $keyChantier => $idChantier) {
                        if ($idChantier == '') {
                            continue;
                        }
                        $chantier       = $manager->getRepository(ChantierPrototype::class)
                                                  ->find(ChantierFixtures::$tabTransco[$idChantier]);
                        $chantiersVille = (new Chantiers($chantier))->setPv((int)$tabLife[$keyChantier])->setDef(
                            (int)$tabDef[$keyChantier],
                        );
                        
                        $ville->addChantier($chantiersVille);
                        
                    }
                }
            }
            $processBar->advance();
            
            /* DefVille */
            $def_old = $manager->getRepository(OldDefVille::class)->find($infoVille->getMapid());
            if ($def_old) {
                
                $tabDay          = explode('.', (string)$def_old->getDays());
                $tabTotal        = explode('.', (string)$def_old->getTotal());
                $tabBuildings    = explode('.', (string)$def_old->getBuildings());
                $tabUpgrade      = explode('.', (string)$def_old->getUpgrades());
                $tabItems        = explode('.', (string)$def_old->getItems());
                $tabBonusOd      = explode('|', (string)$def_old->getItemsmul());
                $tabcitizenHomes = explode('.', (string)$def_old->getCitizenhomes());
                $tabGardien      = explode('.', (string)$def_old->getCitizenguardians());
                $tabVeilleur     = explode('.', (string)$def_old->getWatchmens());
                $tabSouls        = explode('.', (string)$def_old->getSouls());
                $tabCadavre      = explode('.', (string)$def_old->getCadav());
                $tabTempo        = explode('.', (string)$def_old->getTempo());
                
                foreach ($tabDay as $keyDef => $day) {
                    $defVille = (new Defense($day));
                    $defVille->setTotal((int)$tabTotal[$keyDef])
                             ->setBuildings((int)$tabBuildings[$keyDef])
                             ->setUpgrades((int)$tabUpgrade[$keyDef])
                             ->setObjet((int)$tabItems[$keyDef])
                             ->setBonusOd((int)($tabBonusOd[$keyDef] * 10))
                             ->setMaisonCitoyen((int)$tabcitizenHomes[$keyDef])
                             ->setGardiens((int)$tabGardien[$keyDef])
                             ->setVeilleurs((int)$tabVeilleur[$keyDef])
                             ->setAmes((int)$tabSouls[$keyDef])
                             ->setMorts(isset($tabCadavre[$keyDef]) ? (int)$tabCadavre[$keyDef] : 0)
                             ->setTempos((int)$tabTempo[$keyDef]);
                    
                    /* calcul bonus SD */
                    $defOd = min(500, round(((int)$tabItems[$keyDef] * (double)$tabBonusOd[$keyDef] / 10)));
                    
                    $defSD = $defVille->getTotal() - ($defVille->getBuildings() +
                                                      $defVille->getUpgrades() +
                                                      $defOd +
                                                      $defVille->getMaisonCitoyen() +
                                                      $defVille->getGardiens() +
                                                      $defVille->getVeilleurs() +
                                                      $defVille->getTempos() +
                                                      $defVille->getMorts() +
                                                      $defVille->getAmes() + 10
                        );
                    
                    $defVille->setBonusSd($defSD);
                    
                    $ville->addDefense($defVille);
                    
                }
            }
            $processBar->advance();
            
            /* EstimVilles */
            $estim_old = $manager->getRepository(OldEstimVille::class)->find($infoVille->getMapid());
            if ($estim_old) {
                
                $tabDay    = explode('.', (string)$estim_old->getDays());
                $tabminJ   = explode('.', (string)$estim_old->getMinj());
                $tabmaxJ   = explode('.', (string)$estim_old->getMaxj());
                $tabmaxedJ = explode('.', (string)$estim_old->getMaxedj());
                $tabminP   = explode('.', (string)$estim_old->getMinjp());
                $tabmaxP   = explode('.', (string)$estim_old->getMaxjp());
                $tabmaxedP = explode('.', (string)$estim_old->getMaxedjp());
                
                foreach ($tabDay as $keyEstim => $day) {
                    $estim = (new Estimation($day));
                    $estim->setMinJour((int)$tabminJ[$keyEstim])
                          ->setMaxJour((int)$tabmaxJ[$keyEstim])
                          ->setMaxedJour((bool)$tabmaxedJ[$keyEstim]);
                    
                    if ((int)$tabminP[$keyEstim] != 0) {
                        $estim->setMinPlanif((int)$tabminP[$keyEstim])
                              ->setMaxPlanif((int)$tabmaxP[$keyEstim])
                              ->setMaxedPlanif((bool)$tabmaxedP[$keyEstim]);
                    }
                    
                    $ville->addEstimation($estim);
                    
                }
                
            }
            $processBar->advance();
            
            /* news Villes */
            $news_old = $manager->getRepository(OldNewsVille::class)->find($infoVille->getMapid());
            if ($news_old) {
                
                $tabDay = explode('.', (string)$news_old->getDays());
                $tabDef = explode('.', (string)$news_old->getDef());
                $tabZ   = explode('.', (string)$news_old->getZ());
                if (count(explode('~|~', (string)$news_old->getContent())) > 1) {
                    $tabContent = explode('~|~', (string)$news_old->getContent());
                } else {
                    $tabContent =
                        explode('~|~', str_replace('</p>.<p>', '</p>~|~<p>', (string)$news_old->getContent()));
                }
                $tabRegenDir = explode('.', (string)$news_old->getRegendir());
                $tabWater    = explode('.', (string)$news_old->getWater());
                
                foreach ($tabDay as $keyNews => $day) {
                    $maxlenght = min(
                        2000,
                        strlen($tabContent[$keyNews] ?? 'La page du journal a été dévorée par les zombies'),
                    );
                    $journal   = (new Journal($day));
                    $journal->setDef((int)$tabDef[$keyNews])
                            ->setContent(
                                substr(
                                    mb_convert_encoding($tabContent[$keyNews], 'ISO-8859-1') ??
                                    'La page du journal a été dévorée par les zombies',
                                    0,
                                    $maxlenght,
                                ),
                            )
                            ->setZombie((int)$tabZ[$keyNews]);
                    
                    if (isset($tabRegenDir[$keyNews]) && $tabRegenDir[$keyNews] != 'Aucune' &&
                        $tabRegenDir[$keyNews] != 'invalid regenDir') {
                        $journal->setRegenDir($tabRegenDir[$keyNews]);
                    }
                    if ((int)$tabWater[$keyNews] != 0) {
                        $journal->setWater((int)$tabWater[$keyNews]);
                    }
                    
                    $ville->addJournal($journal);
                    
                }
                
            }
            $processBar->advance();
            
            
            /* up chantier */
            $up_old = $manager->getRepository(OldUpgradeVille::class)->find($infoVille->getMapid());
            if ($up_old) {
                
                $tabIdChantier = explode('|', (string)$up_old->getBuildingid());
                $tabLvlActuel  = explode('|', (string)$up_old->getLevel());
                
                foreach ($tabIdChantier as $keyUp => $idChantier) {
                    
                    $chantier   = $manager->getRepository(ChantierPrototype::class)
                                          ->find(ChantierFixtures::$tabTransco[(int)$idChantier]);
                    $upChantier = new UpChantier($chantier);
                    $upChantier->setLvlActuel((int)$tabLvlActuel[$keyUp])
                               ->setDays(
                                   array_fill(0, (int)$tabLvlActuel[$keyUp], (int)$infoVille->getJourv() - 1),
                               );
                    
                    $ville->addUpChantier($upChantier);
                    
                }
                
            }
            $processBar->advance();
            
            /* CitizenVilles */
            $citizenVille = $manager->getRepository(OldCitizenVille::class)->find($infoVille->getMapid());
            if ($citizenVille) {
                // On récup les infos de citizen detail
                
                /* CitizenDetail */
                
                $citizenDetailValues = $manager->getRepository(OldCitizenDetail::class)->findBy(
                    ['mapid' => $mapId],
                );
                $citizenDetailKey    = array_map(fn($i) => $i->getTwinid(), $citizenDetailValues);
                
                /**
                 * @var OldCitizenDetail[] $citizenDetails
                 */
                $citizenDetails = array_combine($citizenDetailKey, $citizenDetailValues);
                
                $listCitoyens = array_filter(
                    explode('|', (string)$citizenVille->getTwinid()),
                    fn($i) => $i != '99999999999',
                );
                
                // On récupére les users lié à la liste :
                $usersVal = $manager->getRepository(User::class)->findBy(['idMyHordes' => $listCitoyens]);
                $twinkey  = array_map(fn($i) => $i->getIdMyHordes(), $usersVal);
                
                $users = array_combine($twinkey, $usersVal);
                
                // Creation tableau pour chaque citoyens :
                $citoyenOld['twinId']  = explode('|', (string)$citizenVille->getTwinid());
                $citoyenOld['nom']     = explode('|', (string)$citizenVille->getNomj());
                $citoyenOld['job']     = explode('|', (string)$citizenVille->getJob());
                $citoyenOld['mort']    = explode('|', (string)$citizenVille->getMort());
                $citoyenOld['heros']   = explode('|', (string)$citizenVille->getHeros());
                $citoyenOld['x']       = explode('|', (string)$citizenVille->getX());
                $citoyenOld['y']       = explode('|', (string)$citizenVille->getY());
                $citoyenOld['ban']     = explode('|', (string)$citizenVille->getBan());
                $citoyenOld['out']     = explode('|', (string)$citizenVille->getOutc());
                $citoyenOld['message'] = explode('~|~', (string)$citizenVille->getMessage());
                if (!is_array($citoyenOld['message'])) {
                    $citoyenOld['message'] = explode('|', (string)$citoyenOld['message']);
                }
                $citoyenOld['inca']        = explode('|', (string)$citizenVille->getInca());
                $citoyenOld['dtype']       = explode('|', (string)$citizenVille->getDtype());
                $citoyenOld['day']         = explode('|', (string)$citizenVille->getDay());
                $citoyenOld['cleanUpType'] = explode('|', (string)$citizenVille->getCleanuptype());
                $citoyenOld['cleanUpName'] = explode('|', (string)$citizenVille->getCleanupname());
                
                //Pour récup l'id pour les cleanUpName = >
                $cleanUpName = array_combine($citoyenOld['nom'], $citoyenOld['twinId']);
                
                $ville_deva = $infoVille->getDevast();
                
                foreach ($listCitoyens as $keyCitoyen => $id) {
                    
                    if (!isset($users[$id])) {
                        $pouvHeros = $manager->getRepository(HerosPrototype::class)->find(1);
                        $user      = new User();
                        $user->setIdMyHordes((int)$id)
                             ->setPseudo($citoyenOld['nom'][$keyCitoyen])
                             ->setRoles(["ROLE_USER"])
                             ->setDerPouv($pouvHeros)
                             ->setTheme(User::THEME_LIGHT);
                        $users[$id] = $user;
                        
                        $manager->persist($user);
                    }
                }
                
                /* chaman et guide */
                if ($infoVille->getIdguide() != null || $infoVille->getIdguide() != 0) {
                    $ville->setGuide($users[$infoVille->getIdguide()] ?? null);
                }
                if ($infoVille->getIdchaman() != null || $infoVille->getIdchaman() != 0) {
                    $ville->setChaman($users[$infoVille->getIdchaman()] ?? null);
                }
                
                foreach ($listCitoyens as $keyCitoyen => $id) {
                    
                    $citoyen = new Citoyens();
                    $citoyen->setCitoyen($users[$id]);
                    if ($citoyenOld['job'][$keyCitoyen] == '') {
                        $job = $manager->getRepository(JobPrototype::class)->findOneBy(
                            ['ref' => 'looser'],
                        );
                    } else {
                        $job = $manager->getRepository(JobPrototype::class)->findOneBy(
                            ['ref' => self::$tabJobTransCo[$citoyenOld['job'][$keyCitoyen]]],
                        );
                    }
                    
                    $deathType = null;
                    if (isset($citoyenOld['mort'][$keyCitoyen])) {
                        $deathType = $manager->getRepository(TypeDeath::class)->find(
                            (int)$citoyenOld['dtype'][$keyCitoyen],
                        );
                    }
                    $cleanUp = null;
                    if ($citoyenOld['cleanUpType'][$keyCitoyen] != 0 && $citoyenOld['cleanUpType'][$keyCitoyen] != '') {
                        $cleanUp = $manager->getRepository(CleanUpCadaver::class)->findOneBy(
                            ['ref' => $citoyenOld['cleanUpType'][$keyCitoyen]],
                        );
                    }
                    
                    $citoyen->setJob($job)
                            ->setMort((bool)$citoyenOld['mort'][$keyCitoyen])
                            ->setX(
                                ($ville_deva) ? null :
                                    (($citoyenOld['x'][$keyCitoyen] == '') ? 0 : $citoyenOld['x'][$keyCitoyen]),
                            )
                            ->setY(
                                ($ville_deva) ? null :
                                    (($citoyenOld['y'][$keyCitoyen] == '') ? 0 : $citoyenOld['y'][$keyCitoyen]),
                            )
                            ->setBan((bool)$citoyenOld['ban'][$keyCitoyen])
                            ->setDehors((bool)$citoyenOld['out'][$keyCitoyen])
                            ->setMessage(
                                (isset($citoyenOld['message'][$keyCitoyen])) ?
                                    (($citoyenOld['message'][$keyCitoyen] == '') ? null :
                                        substr($citoyenOld['message'][$keyCitoyen], 0, 1024)) :
                                    null,
                            )
                            ->setIncarner((bool)$citoyenOld['inca'][$keyCitoyen])
                            ->setDeathType($deathType ?? null)
                            ->setDeathDay(
                                ($citoyenOld['mort'][$keyCitoyen]) ? (int)$citoyenOld['day'][$keyCitoyen] : null,
                            )
                            ->setCleanUpType($cleanUp ?? null)
                            ->setCleanUpName(
                                ($citoyenOld['cleanUpType'][$keyCitoyen] != 0 &&
                                 $citoyenOld['cleanUpName'][$keyCitoyen] != '') ?
                                    ((isset($cleanUpName[$citoyenOld['cleanUpName'][$keyCitoyen]])) ?
                                        $users[$cleanUpName[$citoyenOld['cleanUpName'][$keyCitoyen]]] : null) : null,
                            );
                    if (isset($citizenDetails[$id])) {
                        if (isset($citizenDetails[$id])) {
                            $home = $manager->getRepository(HomePrototype::class)->find(
                                $citizenDetails[$id]?->getLvlmaison() + 1 ?? 1,
                            );
                        } else {
                            $home = $manager->getRepository(HomePrototype::class)->find(1);
                        }
                        $citoyen->setCorpsSain(!(bool)$citizenDetails[$id]?->getCorpssain() ?? true)
                                ->setChargeApag($citizenDetails[$id]?->getChargeapag() ?? 3)
                                ->setPef((bool)$citizenDetails[$id]?->getPef() ??
                                         ($citoyen->getCitoyen()->getTemArma() ?? true))
                                ->setSauvetage(!(bool)$citizenDetails[$id]?->getSauvetage() ?? true)
                                ->setRdh(!(bool)$citizenDetails[$id]?->getRdh() ?? true)
                                ->setUppercut(!(bool)$citizenDetails[$id]?->getUppercut() ?? true)
                                ->setDonJh(!(bool)$citizenDetails[$id]?->getDonjh() ?? true)
                                ->setSecondSouffle(!(bool)$citizenDetails[$id]?->getSecondsouffle() ?? true)
                                ->setVlm(!(bool)$citizenDetails[$id]?->getVlm() ?? true)
                                ->setTrouvaille(!(bool)$citizenDetails[$id]?->getTrouvaille() ?? true)
                                ->setLvlMaison($home)
                                ->setLvlCoinSieste($citizenDetails[$id]?->getLvlcs() ?? 0)
                                ->setLvlCuisine($citizenDetails[$id]?->getLvlcuisine() ?? 0)
                                ->setLvlLabo($citizenDetails[$id]?->getLvllabo() ?? 0)
                                ->setLvlRenfort($citizenDetails[$id]?->getLvlrenfort() ?? 0)
                                ->setLvlRangement($citizenDetails[$id]?->getLvlrangement() ?? 0)
                                ->setNbBarricade($citizenDetails[$id]?->getNbbarricade() ?? 0)
                                ->setNbCarton($citizenDetails[$id]?->getNbcarton() ?? 0)
                                ->setNbCamping($citizenDetails[$id]?->getNbcamp() ?? 0)
                                ->setLvlRuine($citizenDetails[$id]?->getLvlruine() ?? 0)
                                ->setImmuniser($citizenDetails[$id]?->getImmu() ?? false)
                                ->setCloture($citizenDetails[$id]?->getCloture() ?? false)
                                ->setDateMaj($citizenDetails[$id]?->getDatemaj() ?? null);
                    } else {
                        $home = $manager->getRepository(HomePrototype::class)->find(1);
                        $citoyen->setCorpsSain(true)
                                ->setChargeApag(3)
                                ->setPef(true)
                                ->setSauvetage(true)
                                ->setRdh(true)
                                ->setUppercut(true)
                                ->setDonJh(true)
                                ->setSecondSouffle(true)
                                ->setVlm(true)
                                ->setTrouvaille(true)
                                ->setLvlMaison($home)
                                ->setLvlCoinSieste(0)
                                ->setLvlCuisine(0)
                                ->setLvlLabo(0)
                                ->setLvlRenfort(0)
                                ->setLvlRangement(0)
                                ->setNbBarricade(0)
                                ->setNbCarton(0)
                                ->setNbCamping(0)
                                ->setLvlRuine(0)
                                ->setImmuniser(false)
                                ->setCloture(false)
                                ->setDateMaj(null);
                    }
                    
                    $ville->addCitoyen($citoyen);
                    
                }
            }
            $processBar->advance();
            
            /* Expe villes Hordes */
            $exp_old = $manager->getRepository(OldExpeVilleh::class)->find($infoVille->getMapid());
            if ($exp_old) {
                
                if ($exp_old->getTwinid() != '') {
                    
                    $day       = $exp_old->getDays();
                    $tabTwin   = explode('.', (string)$exp_old->getTwinid());
                    $tabNom    = explode('~|~', (string)$exp_old->getNome());
                    $tabTrace  = explode('!', (string)$exp_old->getTracesexpe());
                    $tabLength = explode('.', (string)$exp_old->getLength());
                    
                    foreach ($tabTwin as $keyExpe => $twinId) {
                        if ($tabTrace[$keyExpe] == '.|.') {
                            continue;
                        }
                        $expe = (new ExpeHordes($day));
                        $expe->setNom($tabNom[$keyExpe])
                             ->setLength((int)$tabLength[$keyExpe]);
                        
                        $citoyen = $users[$twinId];
                        
                        $expe->setCitoyen($citoyen);
                        
                        $tabTemp = explode('|', $tabTrace[$keyExpe]);
                        
                        $tabExpedition = [explode('.', $tabTemp[0]), explode('.', $tabTemp[1])];
                        
                        // Transformation en couple x,y
                        $trace = [];
                        foreach ($tabExpedition[0] as $key => $x) {
                            $trace[] = [$x, $tabExpedition[1][$key]];
                        }
                        
                        $expe->setTrace($trace);
                        
                        $ville->addExpeHorde($expe);
                    }
                }
                
            }
            $processBar->advance();
            
            /* Map Ville bat */
            $mapVilleBatOld = $manager->getRepository(OldMapVilleBat::class)->find($infoVille->getMapid());
            /* Map Ville Case */
            $mapVilleCaseOld = $manager->getRepository(OldMapVilleItem::class)->find($infoVille->getMapid());
            if ($mapVilleCaseOld) {
                $mapVille['h']        = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getH()),
                );
                $mapVille['z']        = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getZ()),
                );
                $mapVille['tag']      = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getTag()),
                );
                $mapVille['danger']   = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getDanger()),
                );
                $mapVille['jour']     = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getJour()),
                );
                $mapVille['twinId']   = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getTwinid()),
                );
                $mapVille['dried']    = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getDried()),
                );
                $tempVue              = explode('.', (string)$mapVilleCaseOld->getVue());
                $mapVille['vueExplo'] = array_map(fn($i) => explode('|', $i), explode('/', $tempVue[0]));
                $mapVille['vueJour']  = array_map(fn($i) => explode('|', $i), explode('/', $tempVue[1]));
                $mapVille['heureMaj'] = array_map(
                    fn($i) => explode('|', $i),
                    explode('/', (string)$mapVilleCaseOld->getHeuremaj()),
                );
                
                // Pour les objets, on explode un coup avant
                $objet = explode('!', (string)$mapVilleCaseOld->getObjc());
                
                $mapVille['id']     = array_map(fn($i) => explode('|', $i), explode('/', $objet[0]));
                $mapVille['broked'] = array_map(fn($i) => explode('|', $i), explode('/', $objet[1]));
                $mapVille['nbr']    = array_map(fn($i) => explode('|', $i), explode('/', $objet[2]));
                
                
                foreach ($mapVille['h'] as $keyY => $row) {
                    foreach ($row as $keyX => $h) {
                        $zone = new ZoneMap();
                        $zone->createId((int)$keyX, (int)$keyY, $ville->getId())
                             ->setX((int)$keyX)
                             ->setY((int)$keyY);
                        if (isset($mapVille['vueExplo'][$keyY][$keyX]) && $mapVille['vueExplo'][$keyY][$keyX] === '1') {
                            
                            $zone->setZombie(
                                ($mapVille['z'][$keyY][$keyX] != '') ? (int)$mapVille['z'][$keyY][$keyX] : null,
                            )
                                 ->setPdc(
                                     ($mapVille['h'][$keyY][$keyX] != '') ? (int)$mapVille['h'][$keyY][$keyX] : null,
                                 )
                                 ->setTag(
                                     ($mapVille['tag'][$keyY][$keyX] != '') ? (int)$mapVille['tag'][$keyY][$keyX] :
                                         null,
                                 )
                                 ->setDried(
                                     ($mapVille['dried'][$keyY][$keyX] != '') ? (bool)$mapVille['dried'][$keyY][$keyX] :
                                         null,
                                 )
                                 ->setDanger(
                                     ($mapVille['danger'][$keyY][$keyX] != '') ?
                                         (int)$mapVille['danger'][$keyY][$keyX] : null,
                                 )
                                 ->setDay(
                                     ($mapVille['jour'][$keyY][$keyX] != '') ? (int)$mapVille['jour'][$keyY][$keyX] :
                                         null,
                                 )
                                 ->setVue(
                                     ($mapVille['vueJour'][$keyY][$keyX] != '') ?
                                         (int)$mapVille['vueJour'][$keyY][$keyX] : 0,
                                 )
                                 ->setCitoyen(
                                     ($mapVille['twinId'][$keyY][$keyX] != '') ?
                                         ($users[$mapVille['twinId'][$keyY][$keyX]] ?? null) : null,
                                 )
                                 ->setHeureMaj(
                                     ($mapVille['heureMaj'][$keyY][$keyX] != '') ? $mapVille['heureMaj'][$keyY][$keyX] :
                                         null,
                                 );
                            
                            
                            //Ajout des items pour chaque zone
                            if (isset($mapVille['id'][$keyY][$keyX]) && $mapVille['id'][$keyY][$keyX] != '') {
                                $tabObjet['id']     = explode('.', $mapVille['id'][$keyY][$keyX]);
                                $tabObjet['broked'] = explode('.', $mapVille['broked'][$keyY][$keyX]);
                                $tabObjet['nbr']    = explode('.', $mapVille['nbr'][$keyY][$keyX]);
                                
                                foreach ($tabObjet['id'] as $keyObjet => $id) {
                                    $item    = $manager->getRepository(ItemPrototype::class)
                                                       ->find(ObjetFixtures::$tabObjetTransco[(int)$id]);
                                    $itemSol = new MapItem($item, (bool)$tabObjet['broked'][$keyObjet]);
                                    $itemSol->setNombre((int)$tabObjet['nbr'][$keyObjet]);
                                    
                                    $zone->addItem($itemSol);
                                }
                                
                            }
                            
                            
                        } elseif (isset($mapVille['vueExplo'][$keyY][$keyX]) &&
                                  $mapVille['vueExplo'][$keyY][$keyX] === '0') {
                            $zone->setVue(ZoneMap::NON_EXPLO);
                        } elseif (isset($mapVille['vueExplo'][$keyY][$keyX]) &&
                                  $mapVille['vueExplo'][$keyY][$keyX] === 'V') {
                            $zone->setVue(ZoneMap::VILLE);
                        }
                        
                        $ville->addZone($zone);
                    }
                }
                
            }
            $processBar->advance();
            
            $processBar->finish();
            
            $manager->persist($ville);
            
            
            $processBarGlobale->advance();
            try {
                $manager->flush();
                
                //On traite également les bâtiments.
                
                if ($mapVilleBatOld) {
                    $batVille['x']    = explode('|', (string)$mapVilleBatOld->getX());
                    $batVille['y']    = explode('|', (string)$mapVilleBatOld->getY());
                    $batVille['type'] = explode('|', (string)$mapVilleBatOld->getType());
                    $batVille['dig']  = explode('|', (string)$mapVilleBatOld->getDig());
                    $batVille['camp'] = explode('|', (string)$mapVilleBatOld->getCamp());
                    $batVille['vide'] = explode('|', (string)$mapVilleBatOld->getVide());
                    $batVille['dist'] = explode('|', (string)$mapVilleBatOld->getDist());
                    
                    foreach ($batVille['x'] as $keyBat => $x) {
                        if ($x == '-1') {
                            continue;
                        }
                        
                        $id = (int)$ville->getId() * 10000 + (int)$batVille['y'][$keyBat] * 100 +
                              (int)$batVille['x'][$keyBat];
                        
                        $zoneAMaj = $manager->getRepository(ZoneMap::class)->find($id);
                        
                        if ($zoneAMaj !== null) {
                            $bat = $manager->getRepository(BatPrototype::class)->find(
                                BatFixtures::$tabTransco[(int)$batVille['type'][$keyBat]],
                            );
                            $zoneAMaj->setBat($bat)
                                     ->setCamped((bool)$batVille['camp'][$keyBat])
                                     ->setEmpty((bool)$batVille['vide'][$keyBat])
                                     ->setDig((int)$batVille['dig'][$keyBat]);
                            
                            $manager->persist($zoneAMaj);
                            
                        }
                        
                    }
                    $manager->flush();
                }
                
                
                $manager->clear();
            } catch (Exception $exception) {
                $manager = $this->managerRegistry->resetManager();
                $out->writeln("<error>{$exception->getMessage()} - {$this->mapId}</error>");
            }
            
        }
        
        $processBarGlobale->finish();
        
        //        $this->migrateVille($manager, $outPut);
        
        $out->writeln('');
    }
}