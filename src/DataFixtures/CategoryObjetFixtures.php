<?php

namespace App\DataFixtures;

use App\Entity\CategoryObjet;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategoryObjetFixtures extends Fixture
{
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function load(ObjectManager $manager):void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $jsonString = file_get_contents(__DIR__ . '/data/categories.json');
        $data       = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $categories = $data;
        
        $out->writeln('<info>Installing fixtures: CategoryObjet Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de catégorie d\'objet : ' . count($categories) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($categories));
        
        foreach ($categories as $key => $category) {
            $entity = $manager->getRepository(CategoryObjet::class)->findOneBy(['nom' => $category]);
            
            if ($entity === null) {
                $entity = (new CategoryObjet())->setId($key + 1)->setNom($category);
            }
            $manager->persist($entity);
            $processBar->advance();
            
        }
        
        $manager->flush();
        $processBar->finish();
        $out->writeln('');
    }
}
