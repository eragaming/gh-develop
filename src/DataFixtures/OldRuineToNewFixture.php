<?php

namespace App\DataFixtures;

use App\Entity\Ancestor\OldMapVilleBat;
use App\Entity\Ancestor\OldRuineVille;
use App\Entity\BatPrototype;
use App\Entity\Ruines;
use App\Entity\RuinesCases;
use App\Entity\RuinesPlans;
use App\Entity\User;
use App\Entity\Ville;
use App\Service\ConfMaster;
use App\Service\DataCollection;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;

class OldRuineToNewFixture extends Fixture implements DependentFixtureInterface
{
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected DataCollection  $collection,
        protected ConfMaster      $confMaster,
    )
    {
    }
    
    public function getDependencies(): array
    {
        return [OldToNewFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_OLD_RUINE)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        $nombreVille = count($manager->getRepository(OldMapVilleBat::class)->villeAvecRuine());
        
        try {
            $out->writeln('<info>Installing fixtures: Ruines Database</info>');
            $out->writeln('');
            $out->writeln('<comment>Nombre de ville pour les ruines : ' . $nombreVille . ' .</comment>');
            $out->writeln('');
            
            $this->majRuine($manager, $out);
            
        } catch (Exception $exception) {
            $out->writeln("<error>{$exception->getMessage()} - {$this->mapId}</error>");
        }
        
        
    }
    
    public function majRuine(ObjectManager $manager, ConsoleOutputInterface $output): void
    {
        $ruinOlds = $manager->getRepository(OldMapVilleBat::class)->villeAvecRuine();
        
        $output->writeln("<comment>Progression ruines globale par ville : </comment>");
        $processBarGlobale = new ProgressBar($output->section());
        $processBarGlobale->start(count($ruinOlds));
        
        $listBatBdd = $manager->getRepository(BatPrototype::class)->createQueryBuilder('b')
                              ->where('b.explorable = 1')->getQuery()->getResult();
        
        $listBat = array_combine(array_map(fn(BatPrototype $bat) => $bat->getId(), $listBatBdd), $listBatBdd);
        // Balayage des villes
        foreach ($ruinOlds as $ruinOld) {
            
            // Verification si la ville existe
            $ville = $manager->getRepository(Ville::class)->findOneBy(['mapId'  => $ruinOld->getMapid(),
                                                                       'origin' => Ville::ORIGIN_MH]);
            
            if ($ville === null) {
                continue;
            }
            
            $batVille['x']    = explode('|', (string)$ruinOld->getX());
            $batVille['y']    = explode('|', (string)$ruinOld->getY());
            $batVille['type'] = explode('|', (string)$ruinOld->getType());
            
            $ruines = [];
            // Recherche des positions des ruines
            foreach ($batVille['x'] as $key => $x) {
                if ($batVille['type'][$key] >= 100) {
                    $tmp['type'] = $batVille['type'][$key];
                    $tmp['x']    = $batVille['x'][$key];
                    $tmp['y']    = $batVille['y'][$key];
                    
                    $ruines[] = $tmp;
                }
            }
            
            $idOld              = explode('.', (string)$ruinOld->getIdruine1());
            $ruines[0]['idOld'] = $idOld[1];
            
            if (count($ruines) > 1) {
                $idOld              = explode('.', (string)$ruinOld->getIdruine2());
                $ruines[1]['idOld'] = $idOld[1];
            }
            
            if (isset($ruines[2])) {
                $ruines[2]['idOld'] = '';
            }
            
            
            foreach ($ruines as $ruine) {
                $ruineObj = new Ruines();
                
                $ruineObj->setX($ruine['x'])
                         ->setY($ruine['y'])
                         ->setBat($listBat[BatFixtures::$tabTransco[$ruine['type']]]);
                
                // récupération des plans existants
                
                if ($ruine['idOld'] !== '') {
                    /**
                     * @var OldRuineVille $plansExistant
                     */
                    $plansExistant = $manager->getRepository(OldRuineVille::class)->createQueryBuilder('r')
                                             ->where('r.ruineid = :ruineId')
                                             ->setParameter('ruineId', $ruine['idOld'])->getQuery()
                                             ->getOneOrNullResult();
                    if ($plansExistant !== null) {
                        
                        $ruinesPlans = new RuinesPlans();
                        
                        // Recup User créateur
                        $userCrea = $manager->getRepository(User::class)
                                            ->findOneBy(['idMyHordes' => $plansExistant->getCreaid()]);
                        $userMod  = $manager->getRepository(User::class)
                                            ->findOneBy(['idMyHordes' => $plansExistant->getModid()]);
                        
                        if ($userCrea === $userMod) {
                            $userMod = null;
                        }
                        
                        if ($plansExistant->getDatecrea() === $plansExistant->getDatemod()) {
                            $dateMod = null;
                        } else {
                            $dateMod = $plansExistant->getDatemod();
                        }
                        
                        $ruinesPlans->setCreateBy($userCrea)
                                    ->setModifyBy($userMod)
                                    ->setCreatedAt($plansExistant->getDatecrea())
                                    ->setModifyAt($dateMod);
                        
                        
                        // Cartographie
                        
                        $cartes =
                            array_map(fn($i) => explode('.', $i), explode('|', (string)$plansExistant->getMapruine()));
                        $zombie =
                            array_map(fn($i) => explode('.', $i), explode('|', (string)$plansExistant->getNbzruine()));
                        $portes = array_map(fn($i) => explode('.', $i),
                            explode('|', (string)$plansExistant->getPorteruine()));
                        
                        foreach ($cartes as $y => $lignes) {
                            foreach ($lignes as $x => $values) {
                                $casesRuines = new RuinesCases();
                                
                                $casesRuines->setY($y)
                                            ->setX($x)
                                            ->setTypeCase($values)
                                            ->setNbrZombie(($zombie[$y][$x] >= 4) ? null : $zombie[$y][$x])
                                            ->setTypePorte(($portes[$y][$x] == 0) ? null : $portes[$y][$x]);
                                
                                $ruinesPlans->addCase($casesRuines);
                                
                            }
                        }
                        
                        
                        $ruineObj->addPlan($ruinesPlans);
                        
                    }
                }
                
                $ville->addRuine($ruineObj);
                
                $manager->persist($ville);
                $processBarGlobale->advance();
                
                $manager->flush();
            }
            
            
        }
        
        $processBarGlobale->finish();
        $output->writeln('');
        
    }
    
}
