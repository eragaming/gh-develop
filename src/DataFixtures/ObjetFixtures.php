<?php

namespace App\DataFixtures;

use App\Entity\CategoryObjet;
use App\Entity\ItemNeed;
use App\Entity\ItemProbability;
use App\Entity\ItemPrototype;
use App\Entity\ListAssemblage;
use App\Entity\TypeActionAssemblage;
use App\Entity\TypeObjet;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class ObjetFixtures extends Fixture implements DependentFixtureInterface
{
    
    public static array $tabObjetTransco = [
        1    => 1,
        2    => 2,
        3    => 3,
        4    => 4,
        5    => 5,
        7    => 6,
        8    => 7,
        9    => 8,
        10   => 9,
        11   => 10,
        13   => 11,
        14   => 12,
        15   => 13,
        16   => 14,
        17   => 15,
        18   => 16,
        19   => 17,
        20   => 18,
        22   => 19,
        23   => 20,
        25   => 21,
        26   => 22,
        28   => 23,
        29   => 24,
        30   => 25,
        31   => 26,
        32   => 27,
        33   => 28,
        34   => 29,
        35   => 30,
        36   => 31,
        38   => 32,
        39   => 33,
        40   => 34,
        41   => 35,
        42   => 36,
        43   => 37,
        44   => 38,
        45   => 39,
        46   => 40,
        47   => 41,
        48   => 42,
        51   => 43,
        52   => 44,
        53   => 45,
        54   => 46,
        56   => 47,
        58   => 48,
        59   => 49,
        60   => 50,
        62   => 51,
        64   => 52,
        65   => 53,
        66   => 54,
        69   => 336,
        70   => 56,
        73   => 58,
        74   => 59,
        76   => 60,
        77   => 61,
        78   => 62,
        79   => 63,
        80   => 64,
        81   => 65,
        82   => 66,
        84   => 67,
        85   => 68,
        88   => 69,
        89   => 70,
        90   => 71,
        91   => 72,
        92   => 73,
        93   => 74,
        94   => 75,
        95   => 76,
        96   => 77,
        97   => 78,
        98   => 79,
        99   => 80,
        100  => 81,
        101  => 82,
        102  => 83,
        103  => 84,
        104  => 85,
        105  => 86,
        106  => 87,
        107  => 88,
        108  => 89,
        109  => 90,
        110  => 91,
        111  => 92,
        112  => 93,
        113  => 94,
        114  => 95,
        115  => 96,
        116  => 97,
        117  => 98,
        118  => 99,
        119  => 100,
        120  => 101,
        122  => 102,
        123  => 103,
        124  => 104,
        125  => 105,
        126  => 106,
        127  => 107,
        128  => 108,
        129  => 109,
        130  => 110,
        132  => 111,
        133  => 112,
        134  => 113,
        135  => 114,
        136  => 115,
        137  => 116,
        138  => 117,
        139  => 118,
        140  => 119,
        141  => 120,
        142  => 121,
        143  => 122,
        144  => 123,
        145  => 124,
        146  => 125,
        147  => 126,
        148  => 127,
        149  => 128,
        150  => 129,
        151  => 130,
        152  => 131,
        153  => 132,
        154  => 133,
        157  => 134,
        158  => 135,
        159  => 136,
        160  => 137,
        161  => 138,
        162  => 139,
        163  => 140,
        164  => 141,
        165  => 142,
        166  => 143,
        167  => 144,
        168  => 145,
        169  => 146,
        170  => 147,
        171  => 148,
        172  => 149,
        173  => 150,
        174  => 151,
        175  => 152,
        178  => 153,
        179  => 154,
        180  => 155,
        181  => 156,
        182  => 157,
        183  => 158,
        184  => 159,
        185  => 160,
        186  => 161,
        187  => 162,
        188  => 163,
        189  => 164,
        190  => 165,
        191  => 166,
        192  => 314,
        193  => 167,
        194  => 168,
        195  => 169,
        196  => 170,
        197  => 171,
        198  => 172,
        199  => 173,
        200  => 174,
        201  => 175,
        202  => 176,
        203  => 177,
        204  => 178,
        205  => 179,
        206  => 180,
        207  => 181,
        208  => 182,
        209  => 183,
        210  => 184,
        211  => 185,
        212  => 186,
        213  => 187,
        214  => 188,
        215  => 189,
        216  => 190,
        217  => 191,
        218  => 192,
        219  => 193,
        220  => 194,
        221  => 195,
        225  => 196,
        226  => 349,
        227  => 197,
        229  => 198,
        230  => 199,
        231  => 200,
        233  => 201,
        234  => 202,
        235  => 203,
        236  => 204,
        237  => 310,
        238  => 311,
        239  => 312,
        240  => 6000,
        241  => 350,
        242  => 205,
        243  => 206,
        244  => 207,
        245  => 208,
        246  => 209,
        247  => 210,
        248  => 211,
        250  => 212,
        252  => 214,
        253  => 215,
        257  => 216,
        258  => 217,
        259  => 218,
        260  => 219,
        263  => 220,
        264  => 221,
        265  => 222,
        270  => 223,
        271  => 6001,
        277  => 224,
        281  => 225,
        282  => 226,
        283  => 227,
        284  => 228,
        285  => 229,
        286  => 230,
        287  => 231,
        288  => 346,
        291  => 232,
        292  => 233,
        293  => 234,
        294  => 327,
        295  => 328,
        296  => 329,
        297  => 235,
        298  => 236,
        299  => 237,
        300  => 353,
        301  => 330,
        302  => 331,
        303  => 238,
        304  => 313,
        305  => 239,
        306  => 240,
        307  => 241,
        308  => 242,
        309  => 243,
        310  => 244,
        311  => 245,
        312  => 246,
        313  => 247,
        314  => 248,
        321  => 249,
        322  => 250,
        323  => 251,
        324  => 252,
        325  => 253,
        326  => 254,
        327  => 255,
        328  => 262,
        329  => 263,
        330  => 264,
        331  => 259,
        332  => 260,
        333  => 261,
        334  => 256,
        335  => 257,
        336  => 258,
        351  => 265,
        352  => 266,
        353  => 267,
        357  => 271,
        358  => 272,
        359  => 273,
        360  => 274,
        361  => 275,
        364  => 276,
        365  => 277,
        366  => 278,
        370  => 279,
        371  => 280,
        372  => 281,
        373  => 282,
        374  => 283,
        375  => 284,
        376  => 285,
        377  => 286,
        378  => 287,
        379  => 288,
        380  => 289,
        381  => 290,
        382  => 291,
        383  => 292,
        384  => 293,
        388  => 294,
        389  => 295,
        390  => 296,
        391  => 297,
        392  => 298,
        393  => 299,
        394  => 300,
        395  => 301,
        396  => 333,
        397  => 303,
        398  => 304,
        399  => 305,
        400  => 306,
        401  => 307,
        402  => 308,
        403  => 309,
        404  => 337,
        405  => 6002,
        406  => 344,
        407  => 332,
        408  => 343,
        409  => 355,
        410  => 354,
        411  => 348,
        412  => 347,
        413  => 345,
        414  => 342,
        415  => 270,
        416  => 269,
        417  => 268,
        418  => 213,
        419  => 57,
        420  => 55,
        251  => 356,
        222  => 2001,
        262  => 2089,
        223  => 2043,
        224  => 2004,
        261  => 2219,
        1000 => 5000,
        1001 => 5001,
        1002 => 5002,
        1003 => 5003,
        1004 => 5004,
    ];
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [CategoryObjetFixtures::class, TypeObjetFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        $out = new ConsoleOutput();
        
        $jsonString = file_get_contents(__DIR__ . '/data/listObjet.json');
        $listObjets = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listTypeObjets = $manager->getRepository(TypeObjet::class)->findAll();
        $listCategories = $manager->getRepository(CategoryObjet::class)->findAll();
        
        $out->writeln('<info>Installing fixtures: itemPrototype Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre d\'objets : ' . count($listObjets) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listObjets));
        
        foreach ($listObjets as $objet) {
            
            $entity = $manager->getRepository(ItemPrototype::class)->find($objet['id']);
            
            if ($entity === null) {
                $entity = new ItemPrototype();
                $entity->setId($objet['id']);
            }
            $entity->setNom(htmlspecialchars_decode((string)$objet['nom']))
                   ->setDescription(htmlspecialchars_decode((string)$objet['description']))
                   ->setIcon(htmlspecialchars_decode((string)$objet['icon']))
                   ->setCategoryObjet($listCategories[$objet['category'] - 1])
                   ->setTypeObjet(
                       (!isset($objet['type_objet'])) ? null : $listTypeObjets[$objet['type_objet'] - 1],
                   )
                   ->setObjetVeille($objet['objet_veille'])
                   ->setEncombrant($objet['encombrant'] ?? null)
                   ->setUsageUnique($objet['usageUnique'] ?? null)
                   ->setReparable($objet['reparable'] ?? null)
                   ->setTourelle($objet['tourelle'] ?? null)
                   ->setArmurerie($objet['armurerie'] ?? null)
                   ->setMagasin($objet['magasin'] ?? null)
                   ->setLanceBete($objet['lanceBete'] ?? null)
                   ->setDefBase($objet['defBase'] ?? null)
                   ->setUid($objet['uid'])
                   ->setConteneur($objet['conteneur'] ?? null)
                   ->setChanceKill($objet['chance_kill'] ?? null)
                   ->setChance($objet['chance'] ?? null)
                   ->setType($objet['type'] ?? null)
                   ->setActif($objet['actif'] ?? false)
                   ->setProbaPoubelle($objet['proba'] ?? 0)
                   ->setUpdateByAdmin(false);
            
            if (isset($objet['kill'])) {
                if (is_array($objet['kill'])) {
                    $entity->setKillMin($objet['kill'][0])
                           ->setKillMax($objet['kill'][1]);
                } else {
                    $entity->setKillMin($objet['kill']);
                }
            }
            
            $manager->persist($entity);
            
            
            $processBar->advance();
            
        }
        $manager->flush();
        
        
        $processBar->finish();
        $out->writeln('');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listObjets));
        
        foreach ($listObjets as $objet) {
            
            if (isset($objet['id_transform'])) {
                $entity = $manager->getRepository(ItemPrototype::class)->find($objet['id']);
                $entity->removeAllItemResult();
                if (is_array($objet['id_transform'])) {
                    foreach ($objet['id_transform'] as $id_transform) {
                        $objet_transform = $manager->getRepository(ItemPrototype::class)->find($id_transform);
                        
                        $entity->addItemResult($objet_transform);
                    }
                } else {
                    $objet_transform = $manager->getRepository(ItemPrototype::class)->find($objet['id_transform']);
                    
                    $entity->addItemResult($objet_transform);
                }
                
                $manager->persist($entity);
            }
            
            
            $processBar->advance();
            
        }
        $manager->flush();
        
        $processBar->finish();
        $out->writeln('');
        
        $jsonString  = file_get_contents(__DIR__ . '/data/listTypeAction.json');
        $listActions = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        
        $out->writeln('<info>Installing fixtures: typeAction Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre d\'actions : ' . count($listActions) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listActions));
        
        foreach ($listActions as $key => $action) {
            
            $entity = $manager->getRepository(TypeActionAssemblage::class)->find($key);
            
            if ($entity === null) {
                $entity = new TypeActionAssemblage();
                $entity->setId($key);
            }
            $entity->setNom(htmlspecialchars_decode((string)$action['nom']))
                   ->setNomItemNeed(htmlspecialchars_decode((string)$action['need']))
                   ->setNomItemObtain(htmlspecialchars_decode((string)$action['obtain']))
                   ->setDescription((($action['description'] ?? null) === null) ? null :
                                        htmlspecialchars_decode((string)$action['description']));
            
            $manager->persist($entity);
            
            $processBar->advance();
            
        }
        $manager->flush();
        
        $processBar->finish();
        $out->writeln('');
        
        $jsonString      = file_get_contents(__DIR__ . '/data/listAssemblage.json');
        $listAssemblages = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        
        $out->writeln('<info>Installing fixtures: listAssemblages Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre d\'assemblage : ' . count($listAssemblages) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listAssemblages));
        
        $listItems = $manager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        foreach ($listItems as $item) {
            if ($item->getListAssemblage() !== null) {
                $item->setListAssemblage(null);
                $manager->persist($item);
            }
        }
        
        $manager->flush();
        
        $listAssemblagesBdd = $manager->getRepository(ListAssemblage::class)->findAll();
        
        
        foreach ($listAssemblagesBdd as $assemblage) {
            $manager->remove($assemblage);
        }
        
        $manager->flush();
        
        
        foreach ($listAssemblages as $key => $assemblage) {
            
            $entity = $manager->getRepository(ListAssemblage::class)->find($key);
            
            if ($entity === null) {
                $entity = new ListAssemblage();
                $entity->setId($key);
            }
            
            $entity->removeItemObtainAll();
            
            foreach ($assemblage['item_obtain'] as $item_obtain) {
                
                $itemObtain =
                    $manager->getRepository(ItemProbability::class)->findOneBy(['id' => $item_obtain[1] * 10000 +
                                                                                        $item_obtain[0]]);
                
                if ($itemObtain === null) {
                    $itemObtain = new ItemProbability($listItems[$item_obtain[0]], $item_obtain[1]);
                }
                
                $entity->addItemObtain($itemObtain);
            }
            
            $entity->removeItemNeedAll();
            foreach ($assemblage['item_need'] as $item_need) {
                
                $nbr     = $item_need[1] ?? 1;
                $id_item = $item_need[0] ?? 1;
                
                $itemNeed = $manager->getRepository(ItemNeed::class)->findOneBy(['id' => ($nbr * 10000 + $id_item)]);
                
                if ($itemNeed === null) {
                    $itemNeed = new ItemNeed($listItems[$item_need[0]], $item_need[1] ?? 1);
                }
                
                $entity->addItemNeed($itemNeed);
            }
            
            
            $typeAction =
                $manager->getRepository(TypeActionAssemblage::class)->findOneBy(['id' => $assemblage['type_action']]);
            
            $entity->setTypeAction($typeAction);
            
            
            $entity->setItemPrincipal($listItems[$assemblage['item_pp'][0]]);
            
            
            $manager->persist($entity);
            $manager->persist($listItems[$assemblage['item_pp'][0]]->setListAssemblage($entity));
            
            $manager->flush();
            
            $processBar->advance();
            
        }
        
        
        $processBar->finish();
        $out->writeln('');
        
        
    }
}
