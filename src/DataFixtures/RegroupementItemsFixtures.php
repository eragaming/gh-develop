<?php

namespace App\DataFixtures;

use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use App\Entity\RegroupementItemsDecharge;
use App\Service\ConfMaster;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class RegroupementItemsFixtures extends Fixture implements DependentFixtureInterface
{
    
    
    public function __construct(protected ConfMaster $confMaster)
    {
    }
    
    public function getDependencies(): array
    {
        return [ObjetFixtures::class, ChantierFixtures::class];
    }
    
    public function load(ObjectManager $manager): void
    {
        
        if (!$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_MIGRATION_DATA)) {
            return;
        }
        
        $out = new ConsoleOutput();
        
        $jsonString       = file_get_contents(__DIR__ . '/data/regroupement.json');
        $listRegroupement = json_decode($jsonString, true, 512, JSON_THROW_ON_ERROR); // convertit en tableau associatif
        
        $listItems = $manager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $listChantiers = $manager->getRepository(ChantierPrototype::class)->findAllIndexed();
        
        $out->writeln('<info>Installing fixtures: RegroupementItems Database</info>');
        $out->writeln('');
        $out->writeln('<comment>Nombre de regroupement : ' . count($listRegroupement) . ' .</comment>');
        
        $processBar = new ProgressBar($out->section());
        $processBar->start(count($listRegroupement));
        
        foreach ($listRegroupement as $regpt) {
            // Récup du regroupement si existe
            $regroupement =
                $manager->getRepository(RegroupementItemsDecharge::class)->findOneBy(['id' => $regpt['id']]);
            
            if ($regroupement === null) {
                $regroupement = new RegroupementItemsDecharge();
                $regroupement->setId($regpt['id']);
            }
            
            $regroupement->setNom($regpt['nom'])
                         ->setPointDefBase($regpt['pointDefBase'])
                         ->setPointDefDecharge($regpt['pointDefDecharge'])
                         ->setChantierDecharge($listChantiers[ChantierFixtures::$tabTransco[$regpt['decharge']]]);
            
            foreach ($regroupement->getRegroupItems() as $items) {
                $regroupement->removeRegroupItem($items);
            }
            
            foreach ($regpt['listItemsRgt'] as $idItem) {
                $regroupement->addRegroupItem($listItems[ObjetFixtures::$tabObjetTransco[$idItem]]);
            }
            foreach ($regroupement->getItemAffiches() as $items) {
                $regroupement->removeItemAffich($items);
            }
            
            foreach ($regpt['listItemsAff'] as $idItem) {
                $regroupement->addItemAffich($listItems[ObjetFixtures::$tabObjetTransco[$idItem]]);
            }
            
            $manager->persist($regroupement);
            $processBar->advance();
            
        }
        
        
        $manager->flush();
        $processBar->finish();
        $out->write('');
    }
}
