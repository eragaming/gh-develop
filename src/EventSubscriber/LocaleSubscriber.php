<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    
    public function __construct(protected TokenStorageInterface $tokenStorage, private readonly Security $security,
                                private readonly string         $defaultLocale = 'en')
    {
    }
    
    public function applyLanguage(ControllerEvent $event): void
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if ($user?->getLang() && $event->getRequest()->getLocale() !== $user?->getLang()) {
            $event->getRequest()->getSession()->set('_locale', $user->getLang());
        }
    }
    
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST    => [['onKernelRequest', 20]],
            KernelEvents::CONTROLLER => [['applyLanguage', -20]],
        ];
    }
    
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $user    = $this->security->getUser();
        
        // Définit la langue basée sur l'utilisateur connecté, sinon utilise la langue par défaut
        if ($user instanceof User && $user->getLang()) {
            $request->setLocale($user->getLang());
            $event->getRequest()->getSession()->set('_locale', $user->getLang());
        } elseif ($request->getSession()->has('_locale')) {
            $request->setLocale($request->getSession()->get('_locale'));
        } else {
            $request->setLocale($this->defaultLocale);
        }
    }
    
}
