<?php

namespace App\Security;

use App\Entity\RememberMeTokens;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;


abstract class RememberMeSupportingAuthenticator extends AbstractAuthenticator
{
    
    public function __construct(protected EntityManagerInterface $em)
    {
    }
    
    protected function enableRememberMe(TokenInterface $token, Response &$response): void
    {
        /** @var User $user */
        $user = $token->getUser();
        
        if (!($existing_token = $this->em->getRepository(RememberMeTokens::class)->findOneBy(['user' => $user]))) {
            
            do {
                try {
                    $random = bin2hex(random_bytes(16));
                } catch (Exception) {
                    return;
                }
            } while ($this->em->getRepository(RememberMeTokens::class)->findOneBy(['token' => $random]));
            
            $this->em->persist($existing_token = (new RememberMeTokens())->setUser($user)->setToken($random));
            try {
                $this->em->flush();
            } catch (Exception) {
                return;
            }
        }
        
        if ($existing_token) {
            $response->headers->setCookie(new Cookie('gh_remember_me', $existing_token->getToken(),
                                                     strtotime('now+3years'), '/', null, true, true));
        }
    }
    
    
}