<?php

namespace App\Security\Voter;

use App\Entity\Jump;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class InJumpVoter extends Voter
{
    
    public const VIEW     = 'view';
    public const EDIT     = 'edit';
    public const EDIT_SPE = 'edit_spe';
    
    protected function supports(string $attribute, $subject): bool
    {
        
        if (!in_array($attribute, [self::EDIT, self::VIEW, self::EDIT_SPE])) {
            return false;
        }
        
        if (!$subject instanceof Jump) {
            return false;
        }
        
        return true;
        
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        
        /**
         * @var Jump $jump
         */
        $jump = $subject;
        
        // ... (check conditions and return true to grant permission) ...
        return match ($attribute) {
            self::EDIT     => $this->canEdit($jump, $user),
            self::VIEW     => $this->canView($jump, $user),
            self::EDIT_SPE => $this->canEditSpe($jump, $user),
            default        => false,
        };
        
    }
    
    private function canEdit(Jump $jump, User $user): bool
    {
        return $this->canView($jump, $user);
    }
    
    private function canEditSpe(Jump $jump, User $user): bool
    {
        foreach ($user->getLeadJumps() as $leadJump) {
            
            $villeJump = $leadJump->getJump();
            
            if ($villeJump !== null && $villeJump->getId() == $jump->getId()) {
                return true;
            }
            
        }
        
        foreach ($user->getGestionJumps() as $gestJump) {
            
            if ($gestJump !== null && $gestJump->getId() == $jump->getId()) {
                return true;
            }
        };
        
        return false;
    }
    
    private function canView(Jump $jump, User $user): bool
    {
        
        foreach ($user->getInscriptionJumps() as $insJump) {
            
            $villeJump = $insJump->getJump();
            
            if ($villeJump !== null && $villeJump->getId() == $jump->getId()) {
                return true;
            }
        };
        
        return false;
    }
}
