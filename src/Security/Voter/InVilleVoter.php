<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\Ville;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class InVilleVoter extends Voter
{
    
    public const VIEW = 'view';
    public const EDIT = 'edit';
    
    protected function supports(string $attribute, $subject): bool
    {
        
        if (!in_array($attribute, [self::EDIT, self::VIEW])) {
            return false;
        }
        
        if (!$subject instanceof Ville) {
            return false;
        }
        
        return true;
        
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        
        /**
         * @var Ville $ville
         */
        $ville = $subject;
        
        // ... (check conditions and return true to grant permission) ...
        return match ($attribute) {
            self::EDIT => $this->canEdit($ville, $user),
            self::VIEW => $this->canView($ville, $user),
            default    => false,
        };
        
    }
    
    private function canEdit(Ville $ville, User $user): bool
    {
        return $this->canView($ville, $user);
    }
    
    private function canView(Ville $ville, User $user): bool
    {
        foreach ($user->getLeadJumps() as $leadJump) {
            
            $villeJump = $leadJump->getJump()->getVille();
            
            if ($villeJump !== null && $villeJump->getMapId() == $ville->getMapId()) {
                return true;
            }
            
        }
        
        return $ville->getMapId() == $user->getMapId();
    }
}
