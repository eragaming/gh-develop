<?php

namespace App\Security;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class RememberMeAuthenticator extends AbstractAuthenticator
{
    public function __construct(protected RouterInterface $router)
    {
    }
    
    public function authenticate(Request $request): Passport
    {
        
        return new SelfValidatingPassport(
            new UserBadge("tkn::{$request->cookies->get('gh_remember_me', null)}"),
        );
        
    }
    
    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $r = new RedirectResponse($this->router->generate('index'));
        $r->headers->clearCookie('gh_remember_me');
        
        return $r;
    }
    
    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): ?Response
    {
        return new RedirectResponse($request->getRequestUri());
    }
    
    public function supports(Request $request): ?bool
    {
        return $request->cookies->has('gh_remember_me') && !$request->cookies->has('gh_session_id');
    }
    
}
