<?php

namespace App\Security;

use App\Entity\User;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Service\Utils\UpdateAPIHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class LoginAuthenticator extends RememberMeSupportingAuthenticator
{
    
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RouterInterface        $router,
        private readonly RequestStack           $requestStack,
        private readonly UpdateAPIHandler       $updateAPIHandler,
    )
    {
        parent::__construct($entityManager);
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws Exception
     */
    public function authenticate(Request $request): Passport
    {
        
        if ($_ENV['MODE_BETA'] === "0") {
            $accessToken = $request->get('key');
            if ($accessToken === null) {
                $accessToken = $request->request->all()['key'];
            }
        } else {
            $accessToken = $request->cookies->get('myHordesKey', null);
        }
        
        if ($accessToken === null) {
            $userBadge = new UserBadge('null', fn() => null);
            return new Passport($userBadge, new CustomCredentials(fn($credentials, User $user) => false, 0),);
        }
        
        
        try {
            $myHordesUser = $this->updateAPIHandler->appelApiGestionAttackMaintenance("getMeTown", $accessToken);
            
            $userBadge = new UserBadge($accessToken, function () use ($accessToken, $request, $myHordesUser) {
                $updateTwon = false;
                $user       =
                    $this->updateAPIHandler->createUpdateUserMaj($request, $myHordesUser, $accessToken, $updateTwon);
                
                if ($updateTwon || $user->isMajCo()) {
                    $this->updateAPIHandler->updateTown($myHordesUser, $user, $accessToken);
                }
                
                //Stockage de l'accessToken en session
                $this->requestStack->getSession()->set('access_token', $accessToken);
                
                return $user;
                
            },
            );
            
            
            return new Passport($userBadge,
                                new CustomCredentials(fn($credentials, User $user) => $user->getIdMyHordes() ===
                                                                                      $credentials,
                                    $myHordesUser->getId()),);
        } catch (MyHordesAttackException|MyHordesMajApiException|MyHordesMajException $e) {
            throw new Exception("Erreur dans l'authentification : {$e->getMessage()} - userkey : " . $accessToken);
        }
        
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $targetUrl = $this->router->generate('myhordes_loginMH');
        $message   = strtr($exception->getMessageKey(), $exception->getMessageData());
        return new RedirectResponse($targetUrl);
        
        
        //return new Response($message, Response::HTTP_FORBIDDEN);
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // Change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('index');
        
        $response = new RedirectResponse($targetUrl);
        $response->headers->setCookie(new Cookie('_locale', $request->getSession()->get('_locale'),
                                                 time() + 31_536_000));
        $host      = $request->getHost();
        $hostParts = explode('.', $host);
        $subdomain = array_shift($hostParts);
        
        if ($subdomain === 'gest-hordes2') {
            $response->headers->setCookie(new Cookie('myHordesKey', $request->get('key'), time() + 31_536_000, '/', '.' . implode('.', $hostParts)));
        }
        $this->enableRememberMe($token, $response);
        
        return $response;
    }
    
    public function supports(Request $request): ?bool
    {
        return $request->attributes->get('_route') === 'myhordes_login';
    }
}
