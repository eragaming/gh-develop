<?php


namespace App\Doctrine;


use App\Entity\Expeditionnaire;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class IdAlphaExpeditionnaireGenerator extends AbstractIdGenerator
{
    
    /**
     * @inheritDoc
     */
    public function generateId(EntityManagerInterface $em, $entity): string
    {
        $listAlpha     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $uniqueIdFound = false;
        
        do {
            $alphaSuffle = str_shuffle($listAlpha);
            $id          = substr($alphaSuffle, 0, 24);
            
            if (null === $em->getRepository(Expeditionnaire::class)->findOneBy(['id' => $id])) {
                $uniqueIdFound = true;
            }
        } while (!$uniqueIdFound);
        
        return $id;
        
    }
    
}