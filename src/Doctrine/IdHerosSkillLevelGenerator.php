<?php


namespace App\Doctrine;


use App\Entity\HerosSkillLevel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use PHPUnit\Logging\Exception;

class IdHerosSkillLevelGenerator extends AbstractIdGenerator
{
    
    /**
     * @param EntityManagerInterface $em
     * @param $entity
     * @inheritDoc
     */
    public function generateId(EntityManagerInterface $em, $entity): string
    {
        try {
            $lastId = $em->getRepository(HerosSkillLevel::class)->getLastId();
            return $lastId + 1;
        } catch (NoResultException|NonUniqueResultException $e) {
            throw new Exception("Erreur lors de la génération de l'id pour HerosSkillLevel", 0, $e);
        }
    }
}