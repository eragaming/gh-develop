<?php

namespace App\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use JetBrains\PhpStorm\Pure;
use PDO;

class MyBigIntType extends Type
{
    
    public function convertToPHPValue($value, AbstractPlatform $platform): ?int
    {
        return (null === $value) ? null : (int)$value;
    }
    
    #[Pure] public function getBindingType(): int
    {
        return PDO::PARAM_STR;
    }
    
    public function getName(): string
    {
        return Types::BIGINT;
    }
    
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getBigIntTypeDeclarationSQL($column);
    }
}