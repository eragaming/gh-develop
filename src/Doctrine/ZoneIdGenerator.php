<?php

namespace App\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class ZoneIdGenerator extends AbstractIdGenerator
{
    
    public function generateId(EntityManagerInterface $em, $entity): int
    {
        return $entity->getVille()->getMapId() * 10000 + $entity->getY() * 100 + $entity->getX();
    }
}