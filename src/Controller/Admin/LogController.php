<?php


namespace App\Controller\Admin;


use App\Controller\AbstractGestHordesController;
use DateTime;
use DirectoryIterator;
use Exception;
use SplFileInfo;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class LogController extends AbstractGestHordesController
{
    
    
    /**
     * @return Response
     */
    #[Route(path: 'admin/fileSystem/log/delete/{file}', name: 'admin_clear_log', condition: '!request.isXmlHttpRequest()')]
    public function delete_log(ParameterBagInterface $params, string $action = '', string $file = ''): Response
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return new Response('', 403);
        }
        
        if (empty($file)) {
            $file = $params->get('kernel.environment');
        }
        $file = str_replace(['..', '::'], ['', '/'], $file);
        
        try {
            $spl_core_path = new SplFileInfo("{$params->get('kernel.project_dir')}/var/log");
            $path          = new SplFileInfo($file);
            if ($path->isFile() && strtolower($path->getExtension()) === 'log' &&
                str_starts_with($path->getRealPath(), $spl_core_path->getRealPath())) {
                unlink($path->getRealPath());
            }
            $this->logger->info("Admin <info>{$this->user->getPseudo()}</info> deleted log <debug>{$file}</debug>");
        } catch (Exception $e) {
            $this->logger->info("Admin <warning>{$this->user->getPseudo()}</warning> error deleted log <debug>{$e->getMessage()}</debug>");
            
        }
        
        return $this->redirect('/admin#/logs');
    }
    
    /**
     * @return Response
     */
    #[Route(path: 'admin/fileSystem', name: 'admin_file_system')]
    public function main(ParameterBagInterface $params, TranslatorInterface $trans): Response
    {
        $log_base_dir      = "{$params->get('kernel.project_dir')}/var/log";
        $log_spl_base_path = new SplFileInfo($log_base_dir);
        $extract_log_type  = function (SplFileInfo $f) use ($log_spl_base_path, $trans): array {
            if ($f->getPathInfo(SplFileInfo::class)->getRealPath() === $log_spl_base_path->getRealPath()) {
                return [
                    'tag'   => 'Kernel',
                    'color' => '#F71735',
                ];
            } else {
                return match ($f->getPathInfo(SplFileInfo::class)->getFilename()) {
                    'update' => [
                        'tag'   => "Nouvelle mise à jour",
                        'color' => '#82846D',
                    ],
                    'admin'  => [
                        'tag'   => "Admin",
                        'color' => '#ff6633',
                    ],
                    default  => [
                        'tag'   => "Inconnu",
                        'color' => '#646165',
                    ],
                };
            }
        };
        
        $log_files = array_map(fn($e) => [
            'info'   => $e,
            'rel'    => $e->getRealPath(),
            'time'   => (new DateTime())->setTimestamp($e->getMTime()),
            'access' => str_replace(['/', '\\'], '::', $e->getRealPath()),
            'tags'   => [$extract_log_type($e)],
        ], $this->list_files($log_base_dir, 'log'));
        usort($log_files, fn($a, $b) => $b['time'] <=> $a['time']);
        
        
        return $this->render(
            'admin/log.html.twig', [
            'logs' => $log_files,
        ],
        );
    }
    
    /**
     * @return Response
     */
    #[Route(path: 'admin/fileSystem/log/fetch/{action}/{file}', name: 'admin_view_log', condition: '!request.isXmlHttpRequest()')]
    public function view_log(ParameterBagInterface $params, string $action = '', string $file = ''): Response
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return new Response('', 403);
        }
        
        if (empty($file)) {
            $file = $params->get('kernel.environment');
        }
        $file = str_replace(['..', '::'], ['', '/'], $file);
        
        $spl_core_path = new SplFileInfo("{$params->get('kernel.project_dir')}/var/log");
        $path          = new SplFileInfo($file);
        if (!$path->isFile() || strtolower($path->getExtension()) !== 'log' ||
            !str_starts_with($path->getRealPath(), $spl_core_path->getRealPath())) {
            return new Response('', 404);
        }
        
        if ($path->getSize() > 67_108_864) {
            $action = 'download';
        } else {
            if ($path->getSize() > 16_777_216 && $action === 'view') {
                $action = 'print';
            }
        }
        
        return match ($action) {
            'view'     => $this->render('admin/log/logviewer.html.twig', [
                'filename' => $path->getRealPath(),
                'log'      => file_get_contents($path->getRealPath()),
            ]),
            'print'    => $this->file($path->getRealPath(), $path->getFilename(),
                                      ResponseHeaderBag::DISPOSITION_INLINE),
            'download' => $this->file($path->getRealPath(), $path->getFilename(),
                                      ResponseHeaderBag::DISPOSITION_ATTACHMENT),
            default    => new Response('', 403),
        };
    }
    
    /**
     * @param string|string[] $extensions
     * @return SplFileInfo[]
     */
    protected function list_files(string $base_path, $extensions): array
    {
        if (!is_array($extensions)) {
            $extensions = [$extensions];
        }
        
        $result = [];
        
        $paths = [$base_path];
        while (!empty($paths)) {
            $path = array_pop($paths);
            if (!is_dir($path)) {
                continue;
            }
            foreach (new DirectoryIterator($path) as $fileInfo) {
                /** @var SplFileInfo $fileInfo */
                if ($fileInfo->isDot() || $fileInfo->isLink()) {
                    continue;
                } elseif ($fileInfo->isFile() && in_array(strtolower($fileInfo->getExtension()), $extensions)) {
                    $result[] = $fileInfo->getFileInfo(SplFileInfo::class);
                } elseif ($fileInfo->isDir()) {
                    $paths[] = $fileInfo->getRealPath();
                }
            }
        }
        
        return $result;
    }
    
    
}