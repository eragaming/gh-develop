<?php


namespace App\Controller\Api\Outils;


use App\Controller\AbstractGestHordesController;
use App\Entity\ItemPrototype;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;


class VeilleApiController extends AbstractGestHordesController
{
    
    
    /**
     * @param string|null $idOutils
     * @return JsonResponse
     */
    #[Route('/api/veille/{token}/{idOutils}', name: 'api_outils_veille', methods: 'GET'),
        ParamConverter('token', options: ['mapping' => ['token' => 'token']]),
        ParamConverter('token', options: ['mapping' => ['idOutils' => 'idOutils']])]
    public function chargementVeilleAvecOutils(string $token, ?string $idOutils = ""): JsonResponse
    {
        
        
        return new JsonResponse(['items' => $this->recupItemVeille()]);
        
    }
    
    private function recupItemVeille(): array
    {
        
        $listItemVeille = $this->entityManager->getRepository(ItemPrototype::class)->findBy(['objetVeille' => true]);
        
        // transformation arrayCollection en array
        $itemsVeille = [];
        foreach ($listItemVeille as $item) {
            
            $itemsVeille[] = [
                'id'          => $item->getId(),
                'nom'         => $this->translator->trans($item->getNom(), [], 'items'),
                'icon'        => $item->getIcon(),
                'defBase'     => $item->getDefBase(),
                'armurerie'   => $item->getArmurerie(),
                'magasin'     => $item->getMagasin(),
                'tourelle'    => $item->getTourelle(),
                'lanceBete'   => $item->getLanceBete(),
                'encombrant'  => $item->getEncombrant(),
                'usageUnique' => $item->getUsageUnique(),
                'reparable'   => $item->getReparable(),
            ];
            
        }
        
        return $itemsVeille;
        
    }
    
}