<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractApiGestHordesController extends AbstractController
{
    
    protected int    $codeRetour = 0;
    protected string $libRetour  = '';
    protected string $zoneRetour = '';
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected RequestStack           $requestStack,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
    )
    {
    
    }
    
    public function checkUser(int $idMh, string $userKey): User|null
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['idMyHordes' => $idMh]);
        
        if ($user === null) {
            return null;
        }
        
        if ($user->getApiToken() !== $userKey) {
            return null;
        }
        
        return $user;
        
    }
    
    public function randomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string     = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[random_int(0, strlen($characters) - 1)];
        }
        return $string;
    }
    
    public function returnJson(): JsonResponse
    {
        return new JsonResponse(['codeRetour' => $this->codeRetour, 'libRetour' => $this->libRetour,
                                 'zoneRetour' => $this->zoneRetour]);
    }
    
}