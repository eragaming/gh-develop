<?php

namespace App\Controller;

use App\Entity\BatPrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\PictoPrototype;
use App\Entity\User;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\MyHordesApi;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\UpdateAPIHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MyHordesController extends AbstractGestHordesController
{
    
    /**
     * @var User[]|null
     */
    private readonly array $listUsers;
    
    
    /**
     * @var JobPrototype[]|null
     */
    private readonly array $listJob;
    
    /**
     * @var BatPrototype[]|null
     */
    private readonly array $listBat;
    
    /**
     * @var ItemPrototype[]|null
     */
    private readonly array $listObjet;
    
    /**
     * @var PictoPrototype[]|null
     */
    private readonly array $listPicto;
    
    private readonly array $nameToId;
    
    public function __construct(
        protected EntityManagerInterface      $entityManager,
        protected RequestStack                $requestStack,
        protected UserHandler                 $userHandler,
        protected GestHordesHandler           $gh,
        protected Security                    $security,
        protected TranslatorInterface         $translator,
        protected LoggerInterface             $logger,
        protected TranslateHandler            $translateHandler,
        protected MyHordesApi                 $myHordesApi,
        protected UpdateAPIHandler            $APIHandler,
        private readonly ErrorHandlingService $errorHandlingService,
        private DiscordService                $discordService,
    )
    {
        parent::__construct($entityManager, $this->requestStack, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler);
        
    }
    
    #[Route('/login', name: 'myhordes_login')]
    public function connectAction(Request $request): void
    {
    }
    
    #[Route('/login_script/{userKey}', name: 'myhordes_login_script')]
    public function connectAction_script(Request $request, string $userKey): void
    {
    }
    
    /**
     * @return RedirectResponse
     */
    #[Route('/loginMH', name: 'myhordes_loginMH')]
    public function connectToMH(): RedirectResponse
    {
        if ($_ENV['MODE_BETA'] === "0") {
            return $this->redirect($_ENV['URL_DISCLAIMER']);
        } else {
            return $this->redirectToRoute('myhordes_login');
        }
    }
    
    /**
     * @return void
     */
    #[Route('/logout', name: 'myhordes_logout')]
    public function logout(): void
    {
    }
    
    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/maj', name: 'myhordes_maj')]
    public function maj(Request $request): RedirectResponse
    {
        
        if ($this->user === null) {
            return $this->redirectToRoute('myhordes_loginMH');
        }
        
        /* On vérifie que le jeu n'est pas en maintenance ou attaque des zombies */
        
        $attaqueOrMaintenance = $this->myHordesApi->isAttackOrMaintenance($this->user->getApiToken());
        
        $url = $_SERVER['HTTP_REFERER'] ?? $this->generateUrl('index');
        
        if (!$attaqueOrMaintenance) {
            // $this->updateVille($request, $this->user->getApiToken());
            try {
                $this->APIHandler->updateAll($request, $this->user->getApiToken());
            } catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                
                return $this->redirect($url);
            }
        }
        
        return $this->redirect($url);
    }
    
    
    /**
     *
     * @param Request $request
     * @return RedirectResponse|JsonResponse
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws GuzzleException
     */
    #[Route('/majScript', name: 'myhordes_maj_script')]
    public function majScript(Request $request): RedirectResponse|JsonResponse
    {
        
        try {
            $source = $request->headers?->get('X-Source') ?? "Inconnu";
            $token  = $request->get('key');
            
            if ($token === null) {
                throw new GestHordesException($this->translator->trans("L'userkey n'a pas été fournis par le script que vous utilisez, la mise à jour est impossible.", [], 'app'), GestHordesException::SHOW_MESSAGE);
            }
            
            /* On vérifie que le jeu n'est pas en maintenance ou attaque des zombies */
            
            $attaqueOrMaintenance = $this->myHordesApi->isAttackOrMaintenance($token);
            
            if ($attaqueOrMaintenance) {
                return new JsonResponse(json_encode(['retour' => 0, 'lib' => $this->translator->trans("MyHordes est en cours de maintenance ou c'est l'heure de l'attaque.", [], 'app')], JSON_THROW_ON_ERROR));
            } else {
                
                $this->APIHandler->updateAll($request, $token, 1);
                
                return new JsonResponse(['retour' => 1, 'lib' => $this->translator->trans("Mise à jour OK.", [], 'app')]);
                
            }
            
        } catch (Exception $e) {
            return $this->errorHandlingService->handleException($e, 'MyHordesController', "majScript-{$source}");
        }
        
    }
    
}