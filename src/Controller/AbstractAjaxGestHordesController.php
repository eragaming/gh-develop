<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Ville;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractAjaxGestHordesController extends AbstractController
{
    
    public ?Ville $ville;
    public ?User  $user;
    
    protected int    $codeRetour = 0;
    protected string $libRetour  = '';
    protected string $zoneRetour = '';
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected RequestStack           $requestStack,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
    )
    {
        
        $this->ville = $this->userHandler->getTownBySession();
        
        $this->user = $this->security->getUser();
        
    }
    
    public function returnJson(): JsonResponse
    {
        return new JsonResponse(['codeRetour' => $this->codeRetour, 'libRetour' => $this->libRetour,
                                 'zoneRetour' => $this->zoneRetour]);
    }
    
}