<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Banque;
use App\Entity\ChantierPrototype;
use App\Entity\PlansChantier;
use App\Entity\UpChantier;
use App\Entity\Ville;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\LocalFormatterUtils;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Structures\Collection\PlansChantiers;
use App\Structures\Dto\Api\Autre\Hotel\PlansChantiersMaj;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/plans_chantier', name: 'rest_plans_chantier_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class PlansChantierRestController extends AbstractRestGestHordesController
{
    
    
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected LocalFormatterUtils    $lfu,
        protected ChantiersHandler       $chantiersHandler,
        protected UpGradeHandler         $upGradeHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     * @throws \JsonException
     */
    #[Route(path: '/{mapId}', name: 'hotel_plan', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId = 0): Response
    {
        
        $this->ville = $this->userHandler->getTownBySession($mapId);
        
        if ($this->ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        //Récupération des plans de chantier
        $listPlansBdd  = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        $arrRuine      =
            [ChantierPrototype::RUINE_HOTEL, ChantierPrototype::RUINE_HOPITAL, ChantierPrototype::RUINE_BUNKER];
        $plansChantier = [];
        foreach ($listPlansBdd as $plan) {
            if ($plan->getPlan() !== null && $plan->getPlan() !== 0) {
                $plansChantier[] = $plan;
            }
        }
        
        $plansCollec = new PlansChantiers();
        
        $plansCollec->setPlansChantiers($this->ville->getPlansChantiers());
        
        $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie();
        
        // On va regrouper les items cassés avec les non cassés
        $banque = $this->entityManager->getRepository(Banque::class)->somItemBanque($this->ville);
        
        $listItemBanque = array_combine(array_map(fn($i) => $i['id'], $banque), $banque);
        
        $reductionPA = $this->upGradeHandler->recupBonusAtelierReductionPA($this->ville);
        
        $listAllChantier = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        
        $evoChantiers = (new ArrayCollection($listAllChantier))->filter(fn(ChantierPrototype $c) => $c->getLevelMax() > 0 && $c->isActif());
        
        
        // Gestion des évolutions
        $evoVille = $this->entityManager->getRepository(UpChantier::class)->findBy(['ville' => $this->ville]);
        
        
        $banque             = $this->serializerService->serializeArray($listItemBanque, 'json', ['banque']);
        $listChantier       = $this->serializerService->serializeArray($listChantiers, 'json', ['chantier']);
        $listPlansVille     = $this->serializerService->serializeArray($plansCollec->getPlansChantiers(), 'json', ['plan']);
        $listPlans          = $this->serializerService->serializeArray($plansChantier, 'json', ['plan']);
        $plansConstruit     = $this->serializerService->serializeArray($this->ville->getChantiers(), 'json', ['plan_construit']);
        $avancementChantier = $this->serializerService->serializeArray($this->ville->getAvancementChantiers(), 'json', ['chantier']);
        $evoChantierJson    = $this->serializerService->serializeArray($evoChantiers, 'json', ['evo_chantier']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'plansChantier' => [
                'banque'                 => $banque,
                'listChantier'           => $listChantier,
                'listPlans'              => $listPlans,
                'listPlansVille'         => $listPlansVille,
                'listChantiersConstruit' => $plansConstruit,
                'listAvancement'         => $avancementChantier,
                'reductionPA'            => $reductionPA,
                'myVille'                => $this->userHandler->myVille($this->ville),
                'ville'                  => $this->serializerService->serializeArray($this->ville, 'json', ['general_res', 'plan']),
                'user'                   => $this->serializerService->serializeArray($this->user, 'json', ['general_res']),
                'listEvoChantiers'       => $evoChantierJson,
                'listEvoChantiersVille'  => $this->serializerService->serializeArray($evoVille, 'json', ['evo_chantier']),
                'translateEvo'           => $this->translateHandler->evo_chantier_list_translate($evoChantiers->toArray()),
            ],
            'general'       => $this->generateArrayGeneral($request, $mapId),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/majPlans', name: 'majPlans', methods: ['POST'])]
    public function maj_plans(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        
        $encoder              = new JsonEncoder();
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $nameConverter        = new CamelCaseToSnakeCaseNameConverter();
        
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                if ($object instanceof ChantierPrototype) {
                    return $object->getId();
                } else {
                    return $object->getId();
                }
            },
        ];
        $normaliser     = new ObjectNormalizer(
            $classMetadataFactory,
            $nameConverter,
            null,
            null,
            null,
            null,
            $defaultContext,
        );
        $serializer     = new Serializer([new DateTimeNormalizer(), $normaliser], [$encoder]);
        
        $data =
            $serializer->deserialize($request->getContent(), PlansChantiersMaj::class, 'json', ['groups' => ['plan']]);
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - La data fournis est incomplète - maj plans - {$this->user->getId()}.");
            
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}");
            
            return new JsonResponse($retour);
        }
        
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte : " . $data->getMapId());
            
            return new JsonResponse($retour);
        }
        
        if (!$this->userHandler->myVille($ville) && !$this->userHandler->isLead($data->getMapId())) {
            $retour['codeRetour'] = 1;
            $retour['libRetour']  =
                $this->translator->trans("Vous ne pouvez pas tracer pour une ville dont vous ne faites pas partis.", [],
                                         'outils');
            
            return new JsonResponse($retour);
        }
        
        // Récupération des plans de chantiers déjà cochés pour mettre à jour
        $listPlansObtenus = $ville->getPlansChantiers();
        
        // Récupération de la liste complète des chantiers à plan
        $listPlansBdd  = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        $plansChantier = [];
        foreach ($listPlansBdd as $plan) {
            if ($plan->getPlan() !== null && $plan->getPlan() !== 0) {
                $plansChantier[$plan->getId()] = $plan;
            }
        }
        
        $listPlansModifier = $data->getListPlans();
        
        // On remet l'id des chantiers en index du tableau
        
        $listPlansModifierIndexed = [];
        foreach ($listPlansModifier as $plan) {
            $listPlansModifierIndexed[$plan['chantier']['id']] = $plan;
        }
        
        foreach ($listPlansObtenus as $plansObtenu) {
            if (!isset($listPlansModifierIndexed[$plansObtenu->getChantier()->getId()])) {
                $ville->removePlansChantier($plansObtenu);
            } else {
                unset($listPlansModifierIndexed[$plansObtenu->getChantier()->getId()]);
            }
        }
        
        // Une fois qu'on a balayé tous les plans de la ville déjà en base de donnée, on va balayer ce qu'il reste pour rajouter des plans à la ville
        
        foreach ($listPlansModifierIndexed as $newPlans) {
            $ville->addPlansChantier((new PlansChantier())->setChantier($plansChantier[$newPlans['chantier']['id']]));
        }
        
        $ville->setPlansChantierUpdateBy($this->user)
              ->setPlansChantierDateMaj(new DateTime('now'));
        
        try {
            $this->entityManager->persist($ville);
            $this->entityManager->flush();
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'zoneRetour' => [
                                            'ville'  => json_decode($serializer->serialize($this->ville,
                                                                                           'json',
                                                                                           ['groups' => ['general_res',
                                                                                                         'plan']]),
                                                                    null,
                                                                    512,
                                                                    JSON_THROW_ON_ERROR),
                                            'phrase' => $this->translator->trans(
                                                "Mis à jour par <strong>{userMaj}</strong> le <strong>{dateMaj}</strong>.",
                                                [
                                                    '{userMaj}' => $ville->getPlansChantierUpdateBy()->getPseudo() ??
                                                                   'inconnu',
                                                    '{dateMaj}' => $this->lfu->getFormattedDateHeure($ville->getPlansChantierDateMaj()),
                                                ],
                                                'hotel',
                                            ),
                                        ]]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Probleme mise à jour Plans de chantier {$data->getMapId()} : " .
                                 $exception->getMessage());
            
            return new JsonResponse($retour);
        }
    }
    
}