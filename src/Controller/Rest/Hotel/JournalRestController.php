<?php

namespace App\Controller\Rest\Hotel;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\UpChantierPrototype;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ChantierHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/journal', name: 'rest_journal_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class JournalRestController extends AbstractRestGestHordesController
{
    
    
    public function __construct(protected EntityManagerInterface $entityManager,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected Security               $security,
                                protected TranslatorInterface    $translator,
                                protected LoggerInterface        $logger,
                                protected TranslateHandler       $translateHandler,
                                protected SerializerService      $serializerService,
                                protected ErrorHandlingService   $errorHandler,
                                protected DiscordService         $discordService,
                                protected UpGradeHandler         $upGradeHandler,
                                protected ChantierHandler        $chantierHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/{mapId}', name: 'main', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function main(Request $request, int $mapId = 0): JsonResponse
    {
        
        $this->ville = $this->userHandler->getTownBySession($mapId);
        
        if ($this->ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        $listJournalJson = $this->serializerService->serializeArray($this->ville->getJournal()->toArray(), 'json', ['journal']);
        
        $recapLevelScrut = $this->upGradeHandler->recupLevelByDayChantier($this->ville, ChantierPrototype::ID_CHANTIER_SCRUT);
        $bonusLevel      = $this->upGradeHandler->recupLevelChantierArray(ChantierPrototype::ID_CHANTIER_SCRUT, UpChantierPrototype::TYPE_ADD_PCT_REGEN);
        
        $repartitionLevel = [];
        $maxLevel         = count($bonusLevel) - 1;                                                                                                                                                                                                                                                                   // Niveau maximum
        $currentLevel     = 0;                                                                                                                                                                                                                                                                                        // Niveau initial global
        
        // Jours où les bâtiments sont détruits
        $joursDetruits = $this->chantierHandler->getDaysOfDestructionChantier($this->ville, ChantierPrototype::ID_CHANTIER_SCRUT);
        
        // Fusionner les tableaux de votes dans un seul tableau
        $joursVotes = [];
        foreach ($recapLevelScrut as $levelsScrut) {
            $joursVotes = array_merge($joursVotes, $levelsScrut);
        }
        
        // Supprimer les doublons s'il y en a
        $joursVotes = array_unique($joursVotes);
        
        for ($i = 1; $i <= $this->ville->getJour(); $i++) {
            // Vérifier si le jour actuel est un jour de destruction
            if (in_array($i, $joursDetruits)) {
                $currentLevel = 0; // Réinitialiser au niveau 0 si détruit
            }
            
            // Par défaut, le bonus est celui du niveau initial
            $repartitionLevel[$i] = intval($bonusLevel[$currentLevel]);
            
            
            // Vérifier si le jour actuel est un jour de construction et voter
            if (in_array($i, $joursVotes)) {
                $currentLevel = min($currentLevel + 1, $maxLevel); // Augmenter le niveau après reconstruction et vote
            }
        }
        
        $arrayEvo = $this->upGradeHandler->recupAllLevelChantierArray($this->ville);
        ksort($arrayEvo);
        
        $myVille = $this->userHandler->myVille($this->ville);
        
        // récupération des chantiers construits
        //if ($myVille) {
        $chantiers             = $this->entityManager->getRepository(Chantiers::class)->findBy(['ville' => $this->ville]);
        $chantiersByDay        = [];
        $chantiersByDayDetruit = [];
        foreach ($chantiers as $chantier) {
            // on balaye tous les jours de construction
            foreach ($chantier->getJourConstruction() as $key => $jour) {
                $chantiersByDay[$jour][] = $chantier;
            }
            // on balaye tous les jours de destruction
            foreach ($chantier->getJourDestruction() as $key => $jour) {
                $chantiersByDayDetruit[$jour][] = $chantier;
            }
            
        }
        
        $chantiersJson        = $this->serializerService->serializeArray($chantiersByDay, 'json', ['journal']);
        $chantiersDetruitJson = $this->serializerService->serializeArray($chantiersByDayDetruit, 'json', ['journal']);
        
        //} else {
        //	$chantiersJson = [];
        //}
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'journal' => [
                'chantiersList'        => $chantiersJson,
                'chantiersDetruitList' => $chantiersDetruitJson,
                'listJournal'          => $listJournalJson,
                'level_regen'          => $repartitionLevel,
                'level_up_chantier'    => $arrayEvo,
            ],
            'general' => $this->generateArrayGeneral($request, $mapId),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
        
    }
}
