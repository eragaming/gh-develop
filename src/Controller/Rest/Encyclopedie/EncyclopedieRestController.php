<?php

namespace App\Controller\Rest\Encyclopedie;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillType;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\ListAssemblage;
use App\Entity\TypeActionAssemblage;
use App\Entity\TypeCaracteristique;
use App\Entity\UpHomePrototype;
use App\Entity\User;
use App\Entity\ZoneMap;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\CssUser;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/encyclopedie', name: 'rest_ency_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class EncyclopedieRestController extends AbstractRestGestHordesController
{
    public function __construct(protected EntityManagerInterface $entityManager,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected Security               $security,
                                protected TranslatorInterface    $translator,
                                protected LoggerInterface        $logger,
                                protected TranslateHandler       $translateHandler,
                                protected SerializerService      $serializerService,
                                protected ErrorHandlingService   $errorHandler,
                                protected DiscordService         $discordService,
                                protected ChantiersHandler       $chantiersHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/batiments/{id}', name: 'info_bat', methods: ['GET'])]
    public function info_bat(BatPrototype $batPrototype): JsonResponse
    {
        try {
            
            // Traduction du bâtiments, et de ses items
            $batPrototype->setNom($this->translator->trans($batPrototype->getNom(), [], 'bats'))
                         ->setDescription($this->translator->trans($batPrototype->getDescription(), [], 'bats'));
            
            foreach ($batPrototype->getItems() as $item) {
                $itemBat = $item->getItem();
                
                $itemBat->setNom($this->translator->trans($itemBat->getNom(), [], 'items'));
            }
            
            return new JsonResponse([
                                        'bat'       => $this->serializerService->serializeArray($batPrototype, 'json', ['ency', 'ency_bat']),
                                        'translate' => [
                                            'bat'         => $this->translator->trans("Objets du bâtiment :", [], "ency"),
                                            'nom'         => $this->translator->trans("Nom :", [], "ency"),
                                            'description' => $this->translator->trans("Description :", [], "ency"),
                                            'objets'      => $this->translator->trans("Objets obtenables dans le bâtiment :", [], "ency"),
                                            'fermer'      => $this->translator->trans("Fermer", [], "ency"),
                                        ],
                                    ],
                                    Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'EncyclopedieRestController', 'info_bat');
        }
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/batiments', name: 'main_bat', methods: ['GET'])]
    public function main_bats(Request $request): JsonResponse
    {
        $mapIdSession  = (int)$request->headers->get('gh-mapId') ?? 0;
        $list_batiment = $this->entityManager->getRepository(BatPrototype::class)->findAllIndexed();
        
        
        $list_batiment_json = json_decode($this->gh->getSerializer()->serialize($list_batiment, 'json',
                                                                                ['groups' => ['ency', 'ency_bat']]),
                                          null, 512, JSON_THROW_ON_ERROR);
        
        $list_job = $this->entityManager->getRepository(JobPrototype::class)->findAll();
        
        $list_job_json =
            json_decode($this->gh->getSerializer()->serialize($list_job, 'json', ['groups' => ['outils_chantier']]),
                        null, 512, JSON_THROW_ON_ERROR);
        
        $listBatMaVille = [];
        if ($this->ville !== null) {
            $batimenVille     = $this->entityManager->getRepository(ZoneMap::class)->recupBatiment($this->ville);
            $batimenHypoVille = $this->entityManager->getRepository(ZoneMap::class)->recupHypoBatiment($this->ville);
            $ruineVille       = $this->entityManager->getRepository(ZoneMap::class)->recupRuine($this->ville);
            
            foreach ($batimenVille as $zone) {
                $listBatMaVille[] = $zone->getBat()->getId();
            }
            foreach ($batimenHypoVille as $zone) {
                $listBatMaVille[] = $zone->getBatHypothese()->getId();
            }
            foreach ($ruineVille as $zone) {
                $listBatMaVille[] = $zone->getBat()->getId();
            }
            
            $listBatMaVille = array_unique($listBatMaVille, SORT_NUMERIC);
        }
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'batiments' => [
                'listBat'        => $list_batiment_json,
                'listBatMaVille' => $listBatMaVille,
                'listJob'        => $list_job_json,
            ],
            'general'   => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/chantiers', name: 'main_chantier', methods: ['GET'])]
    public function main_chantiers(Request $request): JsonResponse
    {
        $mapIdSession  = (int)$request->headers->get('gh-mapId') ?? 0;
        $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie();
        
        $listAllChantier = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
        
        $evoChantiers =
            (new ArrayCollection($listAllChantier))->filter(fn(ChantierPrototype $c) => $c->getLevelMax() > 0 &&
                                                                                        $c->isActif());
        $listPlans    = (new ArrayCollection($listAllChantier))->filter(fn(ChantierPrototype $c) => $c->getPlan() >= 1);
        
        $listBat = $this->entityManager->getRepository(BatPrototype::class)->findBy(['explorable' => true]);
        
        foreach ($listBat as $bat) {
            $bat->setNom($this->translator->trans($bat->getNom(), [], 'bats'));
        }
        
        $listChantierJson = $this->serializerService->serializeArray($listChantiers, 'json', ['chantier']);
        $evoChantierJson  = $this->serializerService->serializeArray($evoChantiers, 'json', ['evo_chantier']);
        $listPlansJson    = $this->serializerService->serializeArray($listPlans, 'json', ['plan']);
        $listRuinesJson   = $this->serializerService->serializeArray($listAllChantier, 'json', ['plan']);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'chantiers' => [
                'listChantier' => $listChantierJson,
                'listEvo'      => $evoChantierJson,
                'listPlans'    => $listPlansJson,
                'listRuines'   => $listRuinesJson,
                'translate'    => $this->translateHandler->evo_chantier_list_translate($evoChantiers->toArray()),
            ],
            'general'   => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/citoyens', name: 'main_citoyens', methods: ['GET'])]
    public function main_citoyens(Request $request): JsonResponse
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        
        $tabPointsDAme                      = [];
        $tabPointsClean                     = [];
        $tabPointsPande                     = [];
        $tabPointsMortParDesespoirJ1        = [];
        $tabPointsMortParDesespoirCascadeJ1 = [];
        for ($k = 0; $k < 2; $k++) {
            for ($i = 0; $i <= 22; $i++) {
                
                $j = $i + $k * 23;
                
                $tabPointsDAme[$k][$j] = ($j ** 2 + $j) / 2;
                if ($i <= 3 && $k == 0) {
                    $tabPointsClean[$k][$j] = 0;
                } else {
                    $tabPointsClean[$k][$j] = round($j ** 1.5, 0);
                }
                $tabPointsPande[$k][$j] = floor(max($j - 3, 0) ** 1.5);
            }
        }
        
        for ($k = 0; $k < 2; $k++) {
            for ($i = 1; $i <= 23; $i++) {
                $j                                       = $i + $k * 23;
                $tabPointsMortParDesespoirJ1[$k][$j]     = floor($j / 1.5 + 1);
                $tabPointsMortParDesespoirMortJ1[$k][$j] = max($j - $tabPointsMortParDesespoirJ1[$k][$j], 0);
                if ($j < 10) {
                    $tabPointsMortParDesespoirCascadeJ1[$k][$j]     = floor($j / 1.5 + 1);
                    $tabPointsMortParDesespoirCascadeMortJ1[$k][$j] = max($j - $tabPointsMortParDesespoirCascadeJ1[$k][$j], 0);
                    $tabPointsMortParDesespoirCascadeMortJ2[$k][$j] = 0;
                } else {
                    $tabPointsMortParDesespoirCascadeJ1[$k][$j]     = (4 * $j + 5) / 7;
                    $tabPointsMortParDesespoirCascadeMortJ1[$k][$j] = max(($tabPointsMortParDesespoirCascadeJ1[$k][$j] - 1) / 2, 0);
                    $tabPointsMortParDesespoirCascadeMortJ2[$k][$j] = max(($tabPointsMortParDesespoirCascadeMortJ1[$k][$j] - 1) / 2, 0);
                }
            }
        }
        
        
        $cssUser = new CssUser($this->entityManager);
        $cssUser->alimId($this->user?->getId() ?? (new User())->getId());
        
        $listExperienceHeros = $this->entityManager->getRepository(HerosPrototype::class)->listExperienceHeros();
        
        
        $listExperienceHerosJson = $this->serializerService->serializeArray($listExperienceHeros, 'json', ['ency']);
        
        $listCompetence = $this->entityManager->getRepository(HerosSkillType::class)->findAll();
        
        $listCompetenceJson = $this->serializerService->serializeArray($listCompetence, 'json', ['ency']);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'citoyens' => [
                "listHeros"               => $listExperienceHerosJson,
                "listCompetences"         => $listCompetenceJson,
                'tabPointDame'            => $tabPointsDAme,
                'tabPointsClean'          => $tabPointsClean,
                'tabPointsPande'          => $tabPointsPande,
                "tabDespoir"              => $tabPointsMortParDesespoirJ1,
                "tabDespoirMort"          => $tabPointsMortParDesespoirMortJ1,
                "tabDespoirCascade"       => $tabPointsMortParDesespoirCascadeJ1,
                "tabDespoirCascadeMortJ1" => $tabPointsMortParDesespoirCascadeMortJ1,
                "tabDespoirCascadeMortJ2" => $tabPointsMortParDesespoirCascadeMortJ2,
            ],
            'general'  => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    /**
     * @throws JsonException
     */
    #[Route('/objets', name: 'main_objets', methods: ['GET'])]
    public function main_objets(Request $request): JsonResponse
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        
        $listObjets = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
        
        $listAssemblages = $this->entityManager->getRepository(ListAssemblage::class)->findAll();
        
        $listObtainsJson = [];
        $listNeedJson    = [];
        foreach ($listAssemblages as $objet) {
            
            $objetJson =
                json_decode($this->gh->getSerializer()->serialize($objet, 'json', ['groups' => ['ency']]), null, 512,
                            JSON_THROW_ON_ERROR);
            
            foreach ($objet->getItemObtains() as $itemObtain) {
                $listObtainsJson[$itemObtain->getItem()->getId()][$objet->getTypeAction()->getId()][] = $objetJson;
            }
            
            foreach ($objet->getItemNeeds() as $itemNeed) {
                $listNeedJson[$itemNeed->getItem()->getId()][$objet->getTypeAction()->getId()][] = $objetJson;
            }
        }
        
        
        $list_weapons   = $this->entityManager->getRepository(ItemPrototype::class)->findAllWeapons();
        $list_veilles   = $this->entityManager->getRepository(ItemPrototype::class)->findAllArmesVeilles();
        $list_poubelles = $this->entityManager->getRepository(ItemPrototype::class)->findAllPoubelle();
        $list_decharges = $this->entityManager->getRepository(ItemPrototype::class)->findAllDecharge();
        
        $listAction = $this->entityManager->getRepository(TypeActionAssemblage::class)->findAll();
        $listCarac  = $this->entityManager->getRepository(TypeCaracteristique::class)->findAll();
        
        // On filtre les carac qu'on ne veut pas voir
        $listCarac = array_filter($listCarac, fn(TypeCaracteristique $c) => $c->getId() !== 15 && $c->getId() !== 20 && $c->getId() !== 21 && $c->getId() !== 22 && $c->getId() !== 23 && $c->getId() !== 24);
        
        $listObjetsIndex = [];
        foreach ($listObjets as $objet) {
            $listObjetsIndex[$objet->getId()] = $objet;
        }
        
        
        $listObjetsJson     = $this->serializerService->serializeArray($listObjetsIndex, 'json', ['general', 'ency_objet']);
        $list_weaponsJson   = $this->serializerService->serializeArray($list_weapons, 'json', ['ency_weapons']);
        $list_poubellesJson = $this->serializerService->serializeArray($list_poubelles, 'json', ['ency_poubelle']);
        $list_veillesJson   = $this->serializerService->serializeArray($list_veilles, 'json', ['ency_veille']);
        $list_dechargesJson = $this->serializerService->serializeArray($list_decharges, 'json', ['ency_decharge']);
        $listActionJson     = $this->serializerService->serializeArray($listAction, 'json', ['ency']);
        $listCaracJson      = $this->serializerService->serializeArray($listCarac, 'json', ['ency_objet']);
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'objets'  => [
                'listAction'           => $listActionJson,
                'listNeeds'            => $listNeedJson,
                'listObjets'           => $listObjetsJson,
                'listObtains'          => $listObtainsJson,
                'listPoubelles'        => $list_poubellesJson,
                'listVeilles'          => $list_veillesJson,
                'listWeapons'          => $list_weaponsJson,
                'listDecharges'        => $list_dechargesJson,
                'val_def'              => [1 => 1, 2 => 1, 3 => 6, 4 => 5, 5 => 3, 6 => 2],
                'listCaracteristiques' => $listCaracJson,
            ],
            'general' => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/villes', name: 'main_villes', methods: ['GET'])]
    public function main_villes(Request $request): JsonResponse
    {
        try {
            $mapIdSession  = (int)$request->headers->get('gh-mapId') ?? 0;
            $habitationBdd = $this->entityManager->getRepository(HomePrototype::class)->findAll();
            
            $upHomeProto = $this->entityManager->getRepository(UpHomePrototype::class)->findAll();
            
            
            return new JsonResponse([
                                        'villes'  => [
                                            'listHabitation' => $this->serializerService->serializeArray($habitationBdd, 'json', ['ency_hab']),
                                            'listUpHome'     => $this->serializerService->serializeArray($upHomeProto, 'json', ['ency_hab']),
                                        ],
                                        'general' => $this->generateArrayGeneral($request, $mapIdSession),
                                    ],
                                    Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'EncyclopedieRestController', 'main_villes');
        }
    }
}
