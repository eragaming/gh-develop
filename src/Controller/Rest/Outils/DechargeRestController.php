<?php

namespace App\Controller\Rest\Outils;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Banque;
use App\Entity\ChantierPrototype;
use App\Entity\Decharges;
use App\Entity\Outils;
use App\Entity\OutilsDecharge;
use App\Entity\RegroupementItemsDecharge;
use App\Entity\Ville;
use App\Security\Voter\InVilleVoter;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ChantierHandler;
use App\Service\Outils\ReparationHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Service\Utils\UpGradeHandler;
use App\Structures\Dto\GH\Outils\Decharges\DechargesSaveDto;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/outils/decharge', name: 'rest_outils_decharge_', condition: "request.headers.get('Accept') === 'application/json'  and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class DechargeRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected ChantierHandler        $chantierHandler,
        protected ChantiersHandler       $chantiersHandler,
        protected UpGradeHandler         $upGradeHandler,
        protected ReparationHandler      $reparationHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route(path: '/', name: 'outil_decharge', methods: ['GET'])]
    public function main(Request $request): Response
    {
        $mapIdSession = (int)$request->headers->get('gh-mapId') ?? 0;
        $ville        = $this->userHandler->getTownBySession($mapIdSession);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
        }
        
        try {
            $this->denyAccessUnlessGranted(InVilleVoter::VIEW, $ville,
                                           $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville",
                                                                    [], 'outils'));
        } catch (Exception $exception) {
            
            $this->logger->error('DechargesController - deny - ' . $exception->getMessage());
            
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 2;
            $retour['libRetour']  =
                $this->translator->trans("Acces non autorisé, il ne s'agit pas de votre ville", [], 'outils');
            $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
            return new JsonResponse($retour, Response::HTTP_OK);
            
            //return $this->redirectToRoute('index');
        }
        
        
        $manager = $this->entityManager;
        
        
        //$outils = $manager->getRepository(Outils::class)->findOneBy(['id' => 88723]);
        $outils = $manager->getRepository(Outils::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
        
        if ($outils === null || $outils->getOutilsDecharge() === null) {
            
            if ($outils === null) {
                $outils = new Outils($ville, $ville->getJour());
                
                try {
                    $manager->persist($outils);
                    $manager->flush();
                } catch (Exception $exception) {
                    
                    
                    $this->logger->error('DechargesController - sauvegarde outils - ' . $exception->getMessage());
                    
                    $codeErreur           = $this->randomString();
                    $retour['codeRetour'] = 1;
                    $retour['libRetour']  = $this->translator->trans(
                        "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                        ['{code}' => $codeErreur],
                        'app',
                    );
                    return new JsonResponse($retour, Response::HTTP_OK);
                }
            }
            
            $outilsDecharge = new OutilsDecharge();
            
            
        } else {
            
            $outilsDecharge = $outils->getOutilsDecharge();
            
        }
        
        $chantier_decharge_humidifie = $this->entityManager->getRepository(ChantierPrototype::class)->findOneBy(['uid' => 'small_trash_#06']);
        $dh_construit_prevu          = $this->gh->chantierConstruitOrPrevu($ville, $chantier_decharge_humidifie);
        
        
        $listDecharge = $this->entityManager->getRepository(ChantierPrototype::class)->findBy(['id' => [
            ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS,
            ChantierPrototype::ID_CHANTIER_FERRAILLERIE,
            ChantierPrototype::ID_CHANTIER_ENCLOS,
            ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE,
            ChantierPrototype::ID_CHANTIER_APPAT,
            ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE,
        ]]);
        
        $listDechargeConstruit = [];
        foreach ($listDecharge as $decharge) {
            if ($this->chantierHandler->getDechargeEvolutionDechargePublique($ville, $decharge)) {
                $listDechargeConstruit[] = $decharge->getId();
            }
        }
        
        // On va regrouper les items cassés avec les non cassés
        $banque = $this->entityManager->getRepository(Banque::class)->somItemBanque($ville);
        
        $listItemBanque = array_combine(array_map(fn($i) => $i['id'], $banque), $banque);
        
        // transformation en tableau regroupementItem
        $listRegroupement = $this->entityManager->getRepository(RegroupementItemsDecharge::class)->findAllIndexed();
        
        $nbrItmRegroupement = [];
        foreach ($listRegroupement as $regroupement) {
            $nbrItmRegroupement[$regroupement->getId()] = 0;
            foreach ($regroupement->getRegroupItems() as $item) {
                $nbrItmRegroupement[$regroupement->getId()] += (isset($listItemBanque[$item->getId()])) ?
                    $listItemBanque[$item->getId()]['nbrItem'] : 0;
            }
        }
        
        
        $outilsDechargeJson   = json_decode($this->gh->getSerializer()
                                                     ->serialize($outilsDecharge,
                                                                 'json',
                                                                 ['groups' => ['outils_decharge',
                                                                               'general_res']]),
                                            null,
                                            512,
                                            JSON_THROW_ON_ERROR);
        $listRegroupementJson = json_decode($this->gh->getSerializer()->serialize($listRegroupement, 'json',
                                                                                  ['groups' => ['outils_decharge']]),
                                            null, 512, JSON_THROW_ON_ERROR);
        
        //dump($outilsDechargeJson);
        //dd($listDechargeConstruit);
        
        
        $retour['codeRetour'] = 0;
        $retour['zoneRetour'] = [
            'outilsDecharge' => [
                'chantier_decharge' => $listDechargeConstruit,
                'chantier_dh'       => $dh_construit_prevu,
                'itemBanque'        => $nbrItmRegroupement,
                'mapId'             => $ville->getMapId(),
                'outils_decharge'   => $outilsDechargeJson,
                'regroupements'     => $listRegroupementJson,
                'userId'            => $this->user->getId(),
            ],
            'general'        => $this->generateArrayGeneral($request, $mapIdSession),
        ];
        
        return new JsonResponse($retour, Response::HTTP_OK);
    }
    
    #[Route('/save', name: 'save', methods: ['POST'])]
    public function saveDecharge(Request $request): JsonResponse
    {
        
        if ($this->user === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $this->logger->error("$codeErreur - Utilisateur non connecté");
            
            return new JsonResponse($retour);
        }
        $data = new DechargesSaveDto();
        
        
        try {
            $this->gh->getSerializer()->deserialize($request->getContent(), DechargesSaveDto::class, 'json',
                                                    ['groups'             => ['decharges', 'outils_decharge'],
                                                     'object_to_populate' => $data]);
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           = "$codeErreur - Format fournis non correct outils décharge - {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si les champs sont non null
        if ($data->getMapId() === null || $data->getUserId() === null || count($data->getDecharge()) === 0) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - La data fournis est incomplète - outils décharges." . $this->user->getId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification si user identique
        if ($this->user->getId() !== $data->getUserId()) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Vous n'êtes pas correctement identifié, merci de vérifier votre identification . Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils Décharges - Utilisateur non cohérant : {$data->getUserId()} <> {$this->user->getId()}";
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Vérification existance de la ville
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['mapId' => $data->getMapId()]);
        
        if ($ville === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils Décharges - Le numéro de la ville est incorrecte : " . $data->getMapId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Verification user dans les citoyens de la ville
        
        if ($ville->getCitoyen($this->user->getId()) === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils Décharges - Le user fournis n'est pas dans la ville : " . $data->getMapId() .
                ' - user fournis : ' . $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // transformation en tableau regroupementItem
        $listRegroupement = $this->entityManager->getRepository(RegroupementItemsDecharge::class)->findAllIndexed();
        
        if (count($data->getDecharge()) !== count($listRegroupement)) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils Décharges - Le nombre fournis des décharges n'est pas correcte : " .
                $data->getMapId() . ' - user fournis : ' . $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
        // Récupération de l'outils de la ville, et ensuite de la décharge
        
        $outils = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville, 'day' => $ville->getJour()]);
        
        if ($outils === null) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'outils',
            );
            $messageLog           =
                "$codeErreur - Outils Décharges - Tentative d'utilisation d'une data outils non crée : " .
                $data->getMapId() . ' - user fournis : ' . $data->getUserId();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
            
        }
        
        if ($outils->getOutilsDecharge() === null) {
            $outilsDecharge = new OutilsDecharge();
            $outilsDecharge->setCreatedBy($this->user)
                           ->setCreatedAt(new DateTime('now'))
                           ->setOutils($outils);
            
            $outils->setOutilsDecharge($outilsDecharge);
        } else {
            $outilsDecharge = $outils->getOutilsDecharge();
            
            $outilsDecharge->setModifyBy($this->user)
                           ->setModifyAt(new DateTime('now'));
        }
        $dataDecharge = [];
        foreach ($data->getDecharge() as $decharge) {
            $dataDecharge[$decharge->getRegroupItems()->getId()] = $decharge;
        }
        
        foreach ($outilsDecharge->getDecharges() as $decharge) {
            $idRegroupement = $decharge->getRegroupItems()->getId();
            if (isset($dataDecharge[$idRegroupement])) {
                $dataDechargeSpe = $dataDecharge[$idRegroupement];
                
                if ($dataDechargeSpe->getNbrUtilise() === 0 && $dataDechargeSpe->getNbrEstime() === 0) {
                    $outilsDecharge->removeDecharge($decharge);
                } else {
                    $decharge->setDefByItem($dataDechargeSpe->getDefByItem())
                             ->setNbrUtilise($dataDechargeSpe->getNbrUtilise())
                             ->setNbrEstime($dataDechargeSpe->getNbrEstime());
                }
                
                unset($dataDecharge[$idRegroupement]);
            } else {
                $outilsDecharge->removeDecharge($decharge);
            }
        }
        
        foreach ($dataDecharge as $decharge) {
            
            if ($decharge->getNbrUtilise() !== 0 || $decharge->getNbrEstime() !== 0) {
                $newDecharge = new Decharges();
                
                $newDecharge->setRegroupItems($listRegroupement[$decharge->getRegroupItems()->getId()])
                            ->setDefByItem($decharge->getDefByItem())
                            ->setNbrUtilise($decharge->getNbrUtilise())
                            ->setNbrEstime($decharge->getNbrEstime());
                
                $outilsDecharge->addDecharge($newDecharge);
            }
            
        }
        
        $totalDef = 0;
        foreach ($outilsDecharge->getDecharges() as $decharge) {
            $totalDef += ($decharge->getNbrUtilise() * $decharge->getDefByItem());
        }
        
        $outilsDecharge->setDefTotale($totalDef);
        
        try {
            
            $this->entityManager->persist($outilsDecharge);
            $this->entityManager->flush();
            $this->entityManager->refresh($outilsDecharge);
            
            $outilsDechargeJson = json_decode($this->gh->getSerializer()
                                                       ->serialize($outilsDecharge,
                                                                   'json',
                                                                   ['groups' => ['outils_decharge',
                                                                                 'general_res']]),
                                              null,
                                              512,
                                              JSON_THROW_ON_ERROR);
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => $this->translator->trans("Mise à jour OK.", [], 'app'),
                                        'zoneRetour' => [
                                            'outilsDecharge' => $outilsDechargeJson,
                                        ],
                                    ]);
            
        } catch (Exception $exception) {
            $codeErreur           = $this->randomString();
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = $this->translator->trans(
                "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                ['{code}' => $codeErreur],
                'app',
            );
            $messageLog           =
                "$codeErreur - Probleme récupération des estimations {$data->getMapId()} : " . $exception->getMessage();
            $this->logger->error($messageLog);
            $this->gh->generateMessageDiscord($messageLog);
            return new JsonResponse($retour);
        }
        
    }
}