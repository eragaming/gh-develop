<?php

namespace App\Controller\Rest\Ville;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Banque;
use App\Entity\CategoryObjet;
use App\Entity\ItemPrototype;
use App\Entity\ListAssemblage;
use App\Entity\TypeActionAssemblage;
use App\Structures\Collection\Banques;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;


#[Route('/rest/v1/banque', name: 'rest_banque_', condition: "request.headers.get('Accept') === 'application/json' and request.headers.get('Origin') === request.server.get('HTTP_ORIGIN')")]
class BanqueRestController extends AbstractRestGestHordesController
{
    
    #[Route('/item/{id}', name: 'detail_item', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function detail_item(Request $request, int $id): JsonResponse
    {
        try {
            
            $itemPrototype = $this->entityManager->getRepository(ItemPrototype::class)->find($id);
            
            $retour = [
                'item' => $this->serializerService->serializeArray($itemPrototype, 'json', ['banque', 'ency_objet', 'ency_veille']),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'BanqueRestController', 'main');
        }
        
    }
    
    #[Route('/{mapId}', name: 'general', requirements: ['mapId' => '\d+'], methods: ['GET'])]
    public function index(Request $request, int $mapId): JsonResponse
    {
        try {
            $mapIdSession = (int)$request->headers->get('gh-mapId');
            
            $this->ville = $this->userHandler->getTownBySession($mapId);
            $ville       = $this->ville;
            
            if ($ville === null) {
                $codeErreur           = $this->randomString();
                $retour['codeRetour'] = 1;
                $retour['libRetour']  = $this->translator->trans(
                    "Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}",
                    ['{code}' => $codeErreur],
                    'app',
                );
                $this->logger->error("$codeErreur - Le numéro de la ville est incorrecte.");
                return new JsonResponse($retour, Response::HTTP_OK);
                
            }
            
            /**
             * @var Banque[] $banque
             */
            $banque = (new Banques($this->translator))->setBanques($this->ville->getBanque())->triByCategoryNombreNom();
            
            /**
             * @var CategoryObjet[] $categories
             */
            $categories = $this->entityManager->getRepository(CategoryObjet::class)->findAllExceptMarqueurs();
            
            $total         = 0;
            $totalCategory = [];
            
            foreach ($categories as $category) {
                $totalCategory[$category->getId()] = 0;
            }
            
            foreach ($banque as $itemBanque) {
                $total                                                              += $itemBanque->getNombre();
                $totalCategory[$itemBanque->getItem()->getCategoryObjet()->getId()] += $itemBanque->getNombre();
            }
            
            $listObjets = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
            
            $listAssemblages = $this->entityManager->getRepository(ListAssemblage::class)->findAll();
            
            $listObtainsJson = [];
            $listNeedJson    = [];
            foreach ($listAssemblages as $objet) {
                
                $objetJson =
                    json_decode($this->gh->getSerializer()->serialize($objet, 'json', ['groups' => ['ency']]), null, 512,
                                JSON_THROW_ON_ERROR);
                
                foreach ($objet->getItemObtains() as $itemObtain) {
                    $listObtainsJson[$itemObtain->getItem()->getId()][$objet->getTypeAction()->getId()][] = $objetJson;
                }
                
                foreach ($objet->getItemNeeds() as $itemNeed) {
                    $listNeedJson[$itemNeed->getItem()->getId()][$objet->getTypeAction()->getId()][] = $objetJson;
                }
            }
            
            $listAction = $this->entityManager->getRepository(TypeActionAssemblage::class)->findAll();
            
            
            $listObjetsIndex = [];
            foreach ($listObjets as $objet) {
                $listObjetsIndex[$objet->getId()] = $objet;
            }
            
            $listObjetsJson = $this->serializerService->serializeArray($listObjetsIndex, 'json', ['general', 'ency_objet']);
            $listActionJson = $this->serializerService->serializeArray($listAction, 'json', ['ency']);
            
            $retour = [
                'banque'  => [
                    'banque'      => $this->serializerService->serializeArray($banque, 'json', ['banque']),
                    'categories'  => $this->serializerService->serializeArray($categories, 'json', ['banque']),
                    'total'       => $total,
                    'totalCat'    => $totalCategory,
                    'listAction'  => $listActionJson,
                    'listNeeds'   => $listNeedJson,
                    'listObjets'  => $listObjetsJson,
                    'listObtains' => $listObtainsJson,
                ],
                'general' => $this->generateArrayGeneral($request, $mapIdSession),
            ];
            
            return new JsonResponse($retour, Response::HTTP_OK);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception, 'BanqueRestController', 'main');
        }
        
    }
    
}