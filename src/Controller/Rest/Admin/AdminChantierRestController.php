<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\BonusUpChantier;
use App\Entity\ChantierPrototype;
use App\Entity\ItemPrototype;
use App\Entity\RessourceChantier;
use App\Entity\UpChantierPrototype;
use App\Utils\StringManipulation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/chantier', name: 'rest_admin_chantier_')]
class AdminChantierRestController extends AbstractRestGestHordesController
{
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('c')
           ->from(ChantierPrototype::class, 'c'); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('c.nom', ':chantier'));
            $qb->setParameter('chantier', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('c.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if (!empty($sort)) {
            $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        } else {
            $sortArray = false;
        }
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'nom', 'actif'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('c.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $chantiers = $qb->getQuery()->getResult();
        
        $totalChantiersAll = count($this->entityManager->getRepository(ChantierPrototype::class)->findAll());
        
        
        $totalChantiers = count($chantiers);
        
        $serializedChantiers = $this->serializerService->serialize($chantiers, 'json', ['admin_gen']);
        
        
        $response = new JsonResponse($serializedChantiers, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalChantiers/$totalChantiersAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(ChantierPrototype $chantier): JsonResponse
    {
        
        $serializedChantier = $this->serializerService->serialize($chantier, 'json', ['admin', 'admin_gen']);
        
        return new JsonResponse($serializedChantier, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, ChantierPrototype $chantier): JsonResponse
    {
        $chantierNew = new ChantierPrototype();
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        $this->serializerService->deserialize($request->getContent(), ChantierPrototype::class, 'json', $chantierNew, ['admin', 'admin_gen']);
        $lastIdUp   = $this->entityManager->getRepository(UpChantierPrototype::class)->getLastId();
        $lastIdBonus   = $this->entityManager->getRepository(BonusUpChantier::class)->getLastId();
        
        
        $chantier->setNom($chantierNew->getNom())
                 ->setIcon($chantierNew->getIcon())
                 ->setDescription($chantierNew->getDescription())
                 ->setDef($chantierNew->getDef())
                 ->setWater($chantierNew->getWater())
                 ->setPa($chantierNew->getPa())
                 ->setNiveau($chantierNew->getNiveau())
                 ->setPv($chantierNew->getPv())
                 ->setPlan($chantierNew->getPlan())
                 ->setTemp($chantierNew->getTemp())
                 ->setIndes($chantierNew->getIndes())
                 ->setRuineHo($chantierNew->getRuineHo())
                 ->setRuineHs($chantierNew->getRuineHs())
                 ->setRuineBu($chantierNew->getRuineBu())
                 ->setOrderby($chantierNew->getOrderby())
                 ->setOrderByListing($chantierNew->getOrderByListing())
                 ->setUid($chantierNew->getUid())
                 ->setActif($chantierNew->isActif())
                 ->setLevelMax($chantierNew->getLevelMax())
                 ->setIdMh($chantierNew->getIdMh())
                 ->setSpecifiqueVillePrive($chantierNew->isSpecifiqueVillePrive());
        
        if ($chantierNew->getParent() !== null) {
            $chantierParent = $this->entityManager->getRepository(ChantierPrototype::class)->findOneBy(['id' => $chantierNew->getParent()->getId()]);
            
            $chantier->setParent($chantierParent);
        }
        
        // récupération du chantier categorie
        if ($chantierNew->getCatChantier() !== null) {
            $chantierCategorie = $this->entityManager->getRepository(ChantierPrototype::class)->findOneBy(['id' => (($chantierNew->getCatChantier()->getId() === 0) ? $chantierNew->getId() : $chantierNew->getCatChantier()->getId())]);
            
            $chantier->setCatChantier($chantierCategorie);
        }
        
        if ($chantierNew->getRessources() !== null) {
            
            // tableau de nouveau ressource indexé pour mettre à jour l'ancien tableau de ressource (ajout, supp, modif)
            $tableRessources = [];
            
            
            foreach ($chantierNew->getRessources() as $ressource) {
                $tableRessources[$ressource->getItem()->getId()] = $ressource;
            }
            
            // On balaye l'ancien tableau de ressource pour voir si y a besoin de modifier ou supprimer
            foreach ($chantier->getRessources() as $ressourceOld) {
                if (isset($tableRessources[$ressourceOld->getItem()->getId()])) {
                    // On regarde si la quantité est modifié sinon on passe
                    if ($ressourceOld->getNombre() !==
                        $tableRessources[$ressourceOld->getItem()->getId()]->getNombre()) {
                        $ressourceOld->setNombre($tableRessources[$ressourceOld->getItem()->getId()]->getNombre());
                    }
                    unset($tableRessources[$ressourceOld->getItem()->getId()]);
                } else {
                    $chantier->removeRessource($ressourceOld);
                }
            }
            
            // On balaye les nouvelles ressources existantes
            foreach ($tableRessources as $ressourceNew) {
                $itemProto = $this->entityManager->getRepository(ItemPrototype::class)
                                                 ->findOneBy(['id' => $ressourceNew->getItem()->getId()]);
                
                $ressourceObjet = new RessourceChantier();
                $ressourceObjet->setItem($itemProto)
                               ->setNombre($ressourceNew->getNombre());
                
                $chantier->addRessource($ressourceObjet);
                
            }
            
            
        }
        
        if ($chantierNew->getLevelUps()->count() > 0 || count($chantier->getLevelUps()) > 0) {
            
            // tableau des ups nouveau pour pouvoir faire les ajouts, suppression et modification
            $tableUpsNew      = [];
            $tableUpsExisting = [];
            
            
            foreach ($chantierNew->getLevelUps() as $levelUp) {
                if ($levelUp->getId() !== null) {
                    $tableUpsExisting[$levelUp->getId()] = $levelUp;
                } else {
                    $tableUpsNew[] = $levelUp;
                }
            }
            
            // On balaye l'ancien tableau de ressource pour voir si y a besoin de modifier ou supprimer
            foreach ($chantier->getLevelUps() as $levelUpOld) {
                if (isset($tableUpsExisting[$levelUpOld->getId()])) {
                    // On regarde pour chacun des bonus si c'est modifié ou supprimé
                    
                    $bonusUpNewNew = [];
                    $bonusUpNewOld = [];
                    foreach ($tableUpsExisting[$levelUpOld->getId()]->getBonusUps() as $bonusUp) {
                        if ($bonusUp->getId() !== null) {
                            $bonusUpNewOld[$bonusUp->getId()] = $bonusUp;
                        } else {
                            $bonusUpNewNew[] = $bonusUp;
                        }
                    }
                    
                    foreach ($levelUpOld->getBonusUps() as $bonusUpOld) {
                        if (isset($bonusUpNewOld[$bonusUpOld->getId()])) {
                            $bonusUpOld->setValeurUp($bonusUpNewOld[$bonusUpOld->getId()]->getValeurUp());
                        } else {
                            $levelUpOld->removeBonusUp($bonusUpOld);
                        }
                    }
                    
                    foreach ($bonusUpNewNew as $newBonusTabs) {
                        $newBonus = new UpChantierPrototype();
                        $lastIdUp++;
                        $newBonus->setTypeBonus($newBonusTabs->getTypeBonus())
                                 ->setValeurUp($newBonusTabs->getValeurUp())
                                 ->setId($lastIdUp);
                        
                        $levelUpOld->addBonusUp($newBonus);
                    }
                    
                } else {
                    foreach ($levelUpOld->getBonusUps() as $bonusUp) {
                        $levelUpOld->removeBonusUp($bonusUp);
                    }
                    $chantier->removeLevelUp($levelUpOld);
                }
            }
            
            // On balaye les nouvelles ressources existantes
            foreach ($tableUpsNew as $levelUpNew) {
                $lastIdBonus++;
                $newLevelUp = new BonusUpChantier();
                $newLevelUp->setLevel($levelUpNew->getLevel())
                           ->setId($lastIdBonus);
                
                foreach ($levelUpNew->getBonusUps() as $bonusNew) {
                    $lastIdUp++;
                    $newBonus = new UpChantierPrototype();
                    $newBonus->setTypeBonus($bonusNew->getTypeBonus())
                             ->setValeurUp($bonusNew->getValeurUp())
                             ->setId($lastIdUp);
                    
                    $newLevelUp->addBonusUp($newBonus);
                }
                
                
                $chantier->addLevelUp($newLevelUp);
                
            }
            
            
        }
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($chantier);
        $this->entityManager->flush();
        
        $serializedChantier = $this->serializerService->serialize($chantier, 'json', ['admin', 'admin_gen']);
        
        return new JsonResponse($serializedChantier, 200, [], true);
    }
    
}