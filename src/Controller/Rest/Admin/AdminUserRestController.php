<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\HerosPrototype;
use App\Entity\User;
use App\Utils\StringManipulation;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/user', name: 'rest_admin_user_')]
class AdminUserRestController extends AbstractRestGestHordesController
{
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = $request->query->get('range');
        $range      =
            json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Conversion de la chaîne JSON en tableau PHP
        $start      = $range[0];
        $end        = $range[1];
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('u')
           ->from(User::class, 'u')
           ->setFirstResult($start)
           ->setMaxResults($end - $start + 1); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('u.pseudo', ':pseudo'));
            $qb->setParameter('pseudo', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('u.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort      = $request->query->get('sort');
        $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'pseudo', 'date_maj', 'map_id'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('u.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $users = $qb->getQuery()->getResult();
        
        $totalUsersAll = $this->entityManager->getRepository(User::class)->countUser();
        
        
        $totalUsers = count($users);
        
        $serializedUsers = $this->serializerService->serialize($users, 'json', ['admin_gen']);
        
        $response = new JsonResponse($serializedUsers, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalUsers/$totalUsersAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(User $user): JsonResponse
    {
        $serializedUser = $this->serializerService->serialize($user, 'json', ['admin']);
        
        return new JsonResponse($serializedUser, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, User $user): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet User
        $userNew = new User();
        
        $this->serializerService->deserialize($request->getContent(), User::class, 'json',$userNew, ['admin'], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['roles']
        ]);
        
        // gestion du roles
        $tempData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        
        $newDerPouv = $this->entityManager->getRepository(HerosPrototype::class)->findOneBy(['id' => $userNew->getDerPouv()->getId()]);
        
        
        if (is_array($tempData['roles'])) {
            $tempData['roles'] = $tempData['roles'][0];
        }
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $user->setMapId($userNew->getMapId())
             ->setRoles([$tempData['roles']])
             ->setHeros($userNew->getHeros())
             ->setDateMaj(new DateTime('now'))
             ->setPeriodeRappel($userNew->getPeriodeRappel())
             ->setPossApag($userNew->getPossApag())
             ->setTemArma($userNew->getTemArma())
             ->setLegend($userNew->getLegend())
             ->setAnnonceNews($userNew->getAnnonceNews())
             ->setLang($userNew->getLang())
             ->setDateMajAme($userNew->getDateMajAme())
             ->setVilleMaj($userNew->getVilleMaj())
             ->setMapIdMajVille($userNew->getMapIdMajVille())
             ->setForceMajOldTown($userNew->isForceMajOldTown())
             ->setDerPouv($newDerPouv)
             ->setForceMaj($userNew->getForceMaj());
        //->setTheme($userNew->getTheme())
        
        // Mise à jour de la personnalisation
        $userPersonnalisation    = $user->getUserPersonnalisation();
        $userPersonnalisationNew = $userNew->getUserPersonnalisation();
        
        $userPersonnalisation->setFigeMenu($userPersonnalisationNew->isFigeMenu())
                             ->setPopUpClick($userPersonnalisationNew->isPopUpClick())
                             ->setCitoyensModeCompact($userPersonnalisationNew->isCitoyensModeCompact())
                             ->setBlocMajCitoyens($userPersonnalisationNew->isBlocMajCitoyens());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        
        $serializedUser = $this->gh->getSerializer()->serialize($user, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedUser, 200, [], true);
    }
    
}