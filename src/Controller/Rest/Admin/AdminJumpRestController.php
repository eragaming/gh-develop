<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Jump;
use App\Entity\MasquageJump;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/jump', name: 'rest_admin_jump_')]
class AdminJumpRestController extends AbstractRestGestHordesController
{
    
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE'])]
    public function delete(Jump $jump): JsonResponse
    {
        
        // On va faire pour chaque entité liée au jump de la suppression
        foreach ($jump->getInscriptionJumps() as $inscriptionJump) {
            // On va supprimer les occurences liés à l'insiptionJump
            foreach ($inscriptionJump->getLogEventInscriptions() as $logEventInscription) {
                $this->entityManager->remove($logEventInscription);
                $inscriptionJump->removeLogEventInscription($logEventInscription);
                $this->entityManager->flush();
            }
            foreach ($inscriptionJump->getLeadInscriptions() as $leadInscription) {
                $this->entityManager->remove($leadInscription);
                $inscriptionJump->removeLeadInscription($leadInscription);
                $this->entityManager->flush();
            }
//            foreach ($inscriptionJump->getDispo() as $dispo) {
//                $this->entityManager->remove($dispo);
//                $this->entityManager->flush();
//            }
            
            $this->entityManager->remove($inscriptionJump);
            $jump->removeInscriptionJump($inscriptionJump);
            $this->entityManager->flush();
        }
        
        // On va supprimer les potentielles masquages de jump
        // On récupère le masquage des jumps pour les supprimer en cas de besoin
        $listMasquage = $this->entityManager->getRepository(MasquageJump::class)->findBy(['jump' => $jump]);
        
        foreach ($listMasquage as $masquage) {
            $this->entityManager->remove($masquage);
        }
        
        $this->entityManager->remove($jump);
        // Persistez les modifications dans la base de données
        $this->entityManager->flush();
        
        
        return $this->json(['message' => 'Jump supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $nameSearch = $filter['nom'] ?? '';
        $range      = $request->query->get('range');
        $range      = json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Convertir la chaîne JSON en tableau PHP
        $start      = $range[0] ?? 0;
        $end        = $range[1] ?? 10;
        
        
        if ($textSearch === '' && $nameSearch === '') {
            $totalJumpsAll = count($this->entityManager->getRepository(Jump::class)->findAll());
            $jumps         = $this->entityManager->getRepository(Jump::class)
                                                 ->createQueryBuilder('j')
                                                 ->orderBy('j.dateCreation', 'DESC')
                                                 ->setFirstResult($start)
                                                 ->setMaxResults($end - $start +
                                                                 1) // Calculez le nombre d'éléments à récupérer
                                                 ->getQuery()->getResult();
        } elseif ($nameSearch !== "") {
            $jumps         = $this->entityManager->getRepository(Jump::class)
                                                 ->createQueryBuilder('j')
                                                 ->where('j.nom like :nom')
                                                 ->setParameter(':nom', "%" . $nameSearch . "%")
                                                 ->orderBy('j.dateCreation', 'DESC')
                                                 ->getQuery()->getResult();
            $totalJumpsAll = count($jumps);
        } else {
            $jumps         = $this->entityManager->getRepository(Jump::class)
                                                 ->createQueryBuilder('j')
                                                 ->where('j.id = :id')
                                                 ->setParameter(':id', $textSearch)
                                                 ->orderBy('j.dateCreation', 'DESC')
                                                 ->getQuery()->getResult();
            $totalJumpsAll = count($jumps);
        }
        
        
        $totalJumps = count($jumps);
        
        $serializedJump = $this->serializerService->serialize($jumps, 'json', ['admin']);
        
        $response = new JsonResponse($serializedJump, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalJumps/$totalJumpsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(Jump $jump): JsonResponse
    {
        
        $serializedJump = $this->serializerService->serialize($jump, 'json', ['admin_jump', 'jump', 'admin_gen', 'gestion_jump']);
        
        return new JsonResponse($serializedJump, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT'])]
    public function update(Request $request, Jump $jump): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $jumpNew = new Jump();
        $this->serializerService->deserialize($request->getContent(), Jump::class, 'json', $jumpNew, ['admin_jump', 'jump', 'admin_gen']);
        
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($jump);
        $this->entityManager->flush();
        
        $serializedJump = $this->serializerService->serialize($jump, 'json', ['admin']);
        
        return new JsonResponse($serializedJump, 200, [], true);
    }
}