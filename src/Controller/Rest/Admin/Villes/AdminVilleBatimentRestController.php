<?php

namespace App\Controller\Rest\Admin\Villes;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/ville_batiment', name: 'rest_admin_ville_batiment_')]
class AdminVilleBatimentRestController extends AbstractRestGestHordesController
{
    
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter  = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $villeId = $filter['villeId'] ?? '0';
        
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['id' => $villeId]);
        
        if ($ville === null) {
            $batiments = [];
        } else {
            $batiments = $this->entityManager->getRepository(ZoneMap::class)
                                             ->createQueryBuilder('zm')
                                             ->where('zm.ville = :ville')
                                             ->andWhere('zm.bat is not null')
                                             ->setParameter(':ville', $ville)
                                             ->orderBy('zm.bat', 'DESC')
                                             ->getQuery()->getResult();
        }
        
        
        $totalExpeditionsAll = count($batiments);
        
        
        $serializedBatiment = $this->gh->getSerializer()->serialize($batiments, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen'],
        ]);
        
        $response = new JsonResponse($serializedBatiment, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalExpeditionsAll/$totalExpeditionsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(ZoneMap $zoneMap): JsonResponse
    {
        
        $serializedTrace = $this->gh->getSerializer()->serialize($zoneMap, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_ville_bat', 'general_res'],
        ]);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, ZoneMap $zoneMap): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $zoneMapNew = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $zoneMap->setCamped($zoneMapNew['camped'])
                ->setEmpty($zoneMapNew['empty']);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($zoneMap);
        $this->entityManager->flush();
        
        $serializedTrace = $this->gh->getSerializer()->serialize($zoneMap, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_bat'],
        ]);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
}