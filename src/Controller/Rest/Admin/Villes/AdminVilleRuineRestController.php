<?php

namespace App\Controller\Rest\Admin\Villes;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Ruines;
use App\Entity\Ville;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/ville_ruine', name: 'rest_admin_ville_ruine_')]
class AdminVilleRuineRestController extends AbstractRestGestHordesController
{
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Ruines $ruines): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($ruines);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Ruine supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter  = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $villeId = $filter['villeId'] ?? '0';
        
        $ville = $this->entityManager->getRepository(Ville::class)->findOneBy(['id' => $villeId]);
        
        if ($ville === null) {
            $batiments = [];
        } else {
            $batiments = $this->entityManager->getRepository(Ruines::class)
                                             ->createQueryBuilder('r')
                                             ->where('r.ville = :ville')
                                             ->setParameter(':ville', $ville)
                                             ->getQuery()->getResult();
        }
        
        
        $totalExpeditionsAll = count($batiments);
        
        
        $serializedBatiment = $this->gh->getSerializer()->serialize($batiments, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen'],
        ]);
        
        $response = new JsonResponse($serializedBatiment, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalExpeditionsAll/$totalExpeditionsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(Ruines $ruines): JsonResponse
    {
        
        $serializedRuine = $this->gh->getSerializer()->serialize($ruines, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_ruine', 'general_res', 'admin_gen'],
        ]);
        
        return new JsonResponse($serializedRuine, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, Ruines $ruines): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $ruineNew = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        
        // On met à jour chaque plans le fil d'ariane
        
        foreach ($ruines->getPlans() as $key => $plan) {
            $newPlan = $ruineNew['plans'][$key] ?? null;
            if ($newPlan !== null) {
                
                $plan->setTraceSafe($newPlan['trace_safe']);
                
            } else {
                $ruines->removePlan($plan);
            }
        }
        
        
        /*$zoneMap->setCamped($zoneMapNew['camped'])
                ->setEmpty($zoneMapNew['empty']);*/
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($ruines);
        $this->entityManager->flush();
        
        $serializedTrace = $this->gh->getSerializer()->serialize($ruines, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_ruine'],
        ]);
        
        return new JsonResponse($serializedTrace, 200, [], true);
    }
}