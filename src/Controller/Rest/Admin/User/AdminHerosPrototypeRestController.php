<?php

namespace App\Controller\Rest\Admin\User;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\HerosPrototype;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/heros', name: 'rest_admin_heros_')]
class AdminHerosPrototypeRestController extends AbstractRestGestHordesController
{
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /** @var HerosPrototype $heroNew */
        $heroNew = $this->gh->getSerializer()->deserialize($request->getContent(), HerosPrototype::class, 'json',
                                                           ['groups' => ['admin']]);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $heros = new HerosPrototype();
        
        $heros->setId($heroNew->getId())
              ->setNom($heroNew->getNom())
              ->setDescription($heroNew->getDescription())
              ->setIcon($heroNew->getIcon())
              ->setJour($heroNew->getJour())
              ->setJourCumul($heroNew->getJourCumul())
              ->setPouvBase($heroNew->isPouvBase())
              ->setPouvActif($heroNew->isPouvActif())
              ->setMethod($heroNew->getMethod())
              ->setOrdreRecup($heroNew->getOrdreRecup());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($heros);
        $this->entityManager->flush();
        
        $serializedUser = $this->gh->getSerializer()->serialize($heros, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedUser, 200, [], true);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, HerosPrototype $heros): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($heros);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Pouvoir héros supprimée']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        $heros = $this->entityManager->getRepository(HerosPrototype::class)->findAll();
        
        $totalHeros = count($heros);
        
        $serializedHeros = $this->gh->getSerializer()->serialize($heros, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin'],
        ]);
        
        $response = new JsonResponse($serializedHeros, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalHeros/$totalHeros");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(HerosPrototype $hero): JsonResponse
    {
        
        $serializedHero = $this->gh->getSerializer()->serialize($hero, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedHero, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT'])]
    public function update(Request $request, HerosPrototype $heros): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /** @var HerosPrototype $heroNew */
        $heroNew = $this->gh->getSerializer()->deserialize($request->getContent(), HerosPrototype::class, 'json',
                                                           ['groups' => ['admin']]);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $heros->setId($heroNew->getId())
              ->setNom($heroNew->getNom())
              ->setDescription($heroNew->getDescription())
              ->setIcon($heroNew->getIcon())
              ->setJour($heroNew->getJour())
              ->setJourCumul($heroNew->getJourCumul())
              ->setPouvBase($heroNew->isPouvBase())
              ->setPouvActif($heroNew->isPouvActif())
              ->setMethod($heroNew->getMethod())
              ->setOrdreRecup($heroNew->getOrdreRecup());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($heros);
        $this->entityManager->flush();
        
        $serializedUser = $this->gh->getSerializer()->serialize($heros, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedUser, 200, [], true);
    }
    
}