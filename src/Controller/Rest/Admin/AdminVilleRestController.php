<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Ville;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/ville', name: 'rest_admin_ville_')]
class AdminVilleRestController extends AbstractRestGestHordesController
{
    
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE'])]
    public function delete(Ville $ville): JsonResponse
    {
        $this->entityManager->remove($ville);
        // Persistez les modifications dans la base de données
        $this->entityManager->flush();
        
        
        return $this->json(['message' => 'Expedition supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = $request->query->get('range');
        $range      = json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Convertir la chaîne JSON en tableau PHP
        $start      = $range[0] ?? 0;
        $end        = $range[1] ?? 10;
        
        
        if ($textSearch === '') {
            $totalVillesAll = count($this->entityManager->getRepository(Ville::class)->findAll());
            $villes         = $this->entityManager->getRepository(Ville::class)
                                                  ->createQueryBuilder('ville')
                                                  ->orderBy('ville.dateTime', 'DESC')
                                                  ->setFirstResult($start)
                                                  ->setMaxResults($end - $start +
                                                                  1) // Calculez le nombre d'éléments à récupérer
                                                  ->getQuery()->getResult();
        } else {
            $villes         = $this->entityManager->getRepository(Ville::class)
                                                  ->createQueryBuilder('v')
                                                  ->where('v.id = :id')
                                                  ->setParameter(':id', $textSearch . '2')
                                                  ->orderBy('v.dateTime', 'DESC')
                                                  ->getQuery()->getResult();
            $totalVillesAll = count($villes);
        }
        
        
        $totalVilles = count($villes);
        
        $serializedVilles = $this->gh->getSerializer()->serialize($villes, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen'],
        ]);
        
        $response = new JsonResponse($serializedVilles, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalVilles/$totalVillesAll");
        
        return $response;
    }
    
    #[Route('/', name: 'get_expe', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index_expedition(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = $request->query->get('range');
        $range      = json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Convertir la chaîne JSON en tableau PHP
        $start      = $range[0] ?? 0;
        $end        = $range[1] ?? 10;
        
        
        if ($textSearch === '') {
            $totalVillesAll = count($this->entityManager->getRepository(Ville::class)->findAll());
            $villes         = $this->entityManager->getRepository(Ville::class)
                                                  ->createQueryBuilder('ville')
                                                  ->orderBy('ville.dateTime', 'DESC')
                                                  ->setFirstResult($start)
                                                  ->setMaxResults($end - $start +
                                                                  1) // Calculez le nombre d'éléments à récupérer
                                                  ->getQuery()->getResult();
        } else {
            $villes         = $this->entityManager->getRepository(Ville::class)
                                                  ->createQueryBuilder('v')
                                                  ->where('v.id = :id')
                                                  ->setParameter(':id', $textSearch . '2')
                                                  ->orderBy('v.dateTime', 'DESC')
                                                  ->getQuery()->getResult();
            $totalVillesAll = count($villes);
        }
        
        
        $totalVilles = count($villes);
        
        $serializedVilles = $this->gh->getSerializer()->serialize($villes, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_gen'],
        ]);
        
        $response = new JsonResponse($serializedVilles, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalVilles/$totalVillesAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(Ville $ville): JsonResponse
    {
        
        $serializedVille = $this->gh->getSerializer()->serialize($ville, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedVille, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT'])]
    public function update(Request $request, Ville $ville): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $villeNew = $this->gh->getSerializer()->deserialize($request->getContent(), Ville::class, 'json', [
            'groups' => ['admin'],
        ]);
        
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $ville->setDayMajScrut($villeNew->getDayMajScrut())
              ->setPremiereMaj($villeNew->isPremiereMaj())
              ->setMajAllPicto($villeNew->isMajAllPicto())
              ->setMajAllOldTown($villeNew->isMajAllOldTown());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($ville);
        $this->entityManager->flush();
        
        $serializedUser = $this->gh->getSerializer()->serialize($ville, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin'],
        ]);
        
        return new JsonResponse($serializedUser, 200, [], true);
    }
}