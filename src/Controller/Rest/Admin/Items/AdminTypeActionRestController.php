<?php

namespace App\Controller\Rest\Admin\Items;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\TypeActionAssemblage;
use App\Utils\StringManipulation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/type_action_assemblage', name: 'rest_admin_type_action_assemblage_')]
class AdminTypeActionRestController extends AbstractRestGestHordesController
{
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $typeAction = new TypeActionAssemblage();
        
        $this->serializerService->deserialize($request->getContent(), TypeActionAssemblage::class, 'json', $typeAction, ['admin_ass']);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($typeAction);
        $this->entityManager->flush();
        $this->entityManager->refresh($typeAction);
        
        $serializedItem = $this->serializerService->serializeArray($typeAction, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 201, []);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, TypeActionAssemblage $typeActionAssemblage): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($typeActionAssemblage);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Type Action supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('taa')
           ->from(TypeActionAssemblage::class, 'taa');
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('taa.nom', ':nom'));
            $qb->setParameter('nom', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('taa.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if ($sort !== null) {
            $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
            if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
                // Analyse du paramètre de tri (qui peut être au format 'field,order')
                [$field, $order] = $sortArray;
                
                // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
                $allowedFields = ['id', 'nom'];
                if (in_array($field, $allowedFields)) {
                    // On convertit le field fournis en camelCase
                    $field = (new StringManipulation())->underscoreToCamelCase($field);
                    // On ajoute le tri à la requête Doctrine
                    $qb->orderBy('taa.' . $field, $order);
                }
            }
        }
        
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(TypeActionAssemblage::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->serializerService->serializeArray($items, 'json', ['admin_ass']);
        
        $response = new JsonResponse($serializedItems, 200, []);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(TypeActionAssemblage $typeActionAssemblage): JsonResponse
    {
        $serializedItem = $this->serializerService->serializeArray($typeActionAssemblage, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 200, []);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, TypeActionAssemblage $typeActionAssemblage): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $typeAction = new TypeActionAssemblage();
        
        $this->serializerService->deserialize($request->getContent(), TypeActionAssemblage::class, 'json', $typeAction, ['admin_ass']);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $typeActionAssemblage->setNom($typeAction->getNom())
                             ->setDescription($typeAction->getDescription())
                             ->setNomItemNeed($typeAction->getNomItemNeed())
                             ->setNomItemObtain($typeAction->getNomItemObtain());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($typeActionAssemblage);
        $this->entityManager->flush();
        
        $serializedItem = $this->serializerService->serializeArray($typeAction, 'json', ['admin_ass']);
        
        return new JsonResponse($serializedItem, 201, []);
    }
    
}