<?php

namespace App\Controller\Rest\Admin\Items;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\CategoryObjet;
use App\Utils\StringManipulation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[Route('/rest/v1/admin/items_categorie', name: 'rest_admin_items_categorie_')]
class AdminCategorieRestController extends AbstractRestGestHordesController
{
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /**
         * @var CategoryObjet $categoryNew
         */
        $categoryNew = $this->gh->getSerializer()->deserialize($request->getContent(), CategoryObjet::class, 'json', [
            'groups'                               => ['admin_obj'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
        ]);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        
        if ($this->entityManager->getRepository(CategoryObjet::class)->findOneBy(['id' => $categoryNew->getId()]) !==
            null) {
            return $this->json(['message' => 'Conflit détecté : Catégorie déjà existant'], Response::HTTP_CONFLICT);
        }
        
        $categoryNewBDD = new CategoryObjet();
        $categoryNewBDD->setId($categoryNew->getId())
                       ->setNom($categoryNew->getNom());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($categoryNewBDD);
        $this->entityManager->flush();
        
        $serializedItem = $this->gh->getSerializer()->serialize($categoryNewBDD, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj'],
        ]);
        
        return new JsonResponse($serializedItem, 201, [], true);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, CategoryObjet $categoryObjet): JsonResponse
    {
        // Supprimez la quête de la base de données
        $this->entityManager->remove($categoryObjet);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Categorie supprimée']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('c')
           ->from(CategoryObjet::class, 'c'); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('c.nom', ':objet'));
            $qb->setParameter('objet', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('c.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort = $request->query->get('sort');
        if ($sort !== null) {
            $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
            if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
                // Analyse du paramètre de tri (qui peut être au format 'field,order')
                [$field, $order] = $sortArray;
                
                // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
                $allowedFields = ['id', 'nom'];
                if (in_array($field, $allowedFields)) {
                    // On convertit le field fournis en camelCase
                    $field = (new StringManipulation())->underscoreToCamelCase($field);
                    // On ajoute le tri à la requête Doctrine
                    $qb->orderBy('c.' . $field, $order);
                }
            }
        }
        
        
        // Exécution de la requête et récupération les résultats
        $items = $qb->getQuery()->getResult();
        
        $totalItemsAll = count($this->entityManager->getRepository(CategoryObjet::class)->findAll());
        
        
        $totalItems = count($items);
        
        $serializedItems = $this->gh->getSerializer()->serialize($items, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_obj'],
        ]);
        
        $response = new JsonResponse($serializedItems, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalItems/$totalItemsAll");
        
        return $response;
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(CategoryObjet $categoryObjet): JsonResponse
    {
        
        $serializedItem = $this->gh->getSerializer()->serialize($categoryObjet, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj'],
        ]);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, CategoryObjet $categoryObjet): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        /**
         * @var CategoryObjet $categoryNew
         */
        $categoryNew = $this->gh->getSerializer()->deserialize($request->getContent(), CategoryObjet::class, 'json', [
            'groups'                               => ['admin_obj'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
        ]);
        // Mettez à jour les propriétés de la quête en fonction des données de la requête
        
        $categoryObjet->setNom($categoryNew->getNom());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($categoryObjet);
        $this->entityManager->flush();
        
        $serializedItem = $this->gh->getSerializer()->serialize($categoryObjet, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            // Remplacez 'someAttributeToIgnore' par le nom d'une éventuelle propriété à exclure de la sérialisation
            'groups'                               => ['admin_obj'],
        ]);
        
        return new JsonResponse($serializedItem, 200, [], true);
    }
    
}