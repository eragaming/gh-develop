<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\Menu;
use App\Entity\MenuPrototype;
use App\Enum\MenuType;
use App\Exception\GestHordesException;
use App\Service\ErrorHandlingService;
use App\Service\Generality\MenuHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Utils\StringManipulation;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/admin/menu', name: 'rest_admin_menu_')]
class AdminMenuRestController extends AbstractRestGestHordesController
{
    
    public function __construct(protected EntityManagerInterface $entityManager,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected Security               $security,
                                protected TranslatorInterface    $translator,
                                protected LoggerInterface        $logger,
                                protected TranslateHandler       $translateHandler,
                                protected SerializerService      $serializerService,
                                protected ErrorHandlingService   $errorHandler,
                                protected DiscordService         $discordService,
                                protected MenuHandler            $menuHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger, $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        $menuNew = new MenuPrototype();
        $this->serializerService->deserialize($request->getContent(), MenuPrototype::class, 'json', $menuNew, ['admin']);
        
        $menuNew->setId($this->entityManager->getRepository(MenuPrototype::class)->lastId() + 1);
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($menuNew);
        $this->entityManager->flush();
        $this->entityManager->refresh($menuNew);
        
        $serializedMenu = $this->serializerService->serializeArray($menuNew, 'json', ['admin']);
        
        return new JsonResponse($serializedMenu, 201);
    }
    
    #[Route('/current-menu/', name: 'current_menu', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function current_menu(Request $request): JsonResponse
    {
        // On récupére le menu de base général du site
        $menu = $this->entityManager->getRepository(Menu::class)
                                    ->createQueryBuilder("m")
                                    ->where('m.user IS NULL')
                                    ->setMaxResults(1)
                                    ->getQuery()
                                    ->getOneOrNullResult();
        
        
        $serializedMenus = $this->serializerService->serializeArray($menu, 'json', ['admin']);
        
        return new JsonResponse($serializedMenus, 200, []);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE'])]
    public function delete(MenuPrototype $menu): JsonResponse
    {
        
        $this->entityManager->remove($menu);
        // Persistez les modifications dans la base de données
        $this->entityManager->flush();
        
        
        return $this->json(['message' => 'Menu supprimé']);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = json_decode($request->query->get('range') ?? '[]', true, 512, JSON_THROW_ON_ERROR);
        [$start, $end] = [$range[0] ?? 0, $range[1] ?? 0];
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('m')
           ->from(MenuPrototype::class, 'm');
        
        if ($end !== 0) {
            $qb->setFirstResult($start)
               ->setMaxResults($end - $start + 1); // Calcul du nombre d'éléments à récupérer
        }
        
        if ($textSearch !== '') {
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans le label du menu
            $orX->add($qb->expr()->like('m.label', ':label'));
            $qb->setParameter('label', '%' . $textSearch . '%');
            
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sortArray = json_decode($request->query->get('sort') ?? '[]', true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'label'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('m.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $menus = $qb->getQuery()->getResult();
        
        $totalMenusAll = count($this->entityManager->getRepository(MenuPrototype::class)->findAll());
        
        
        $totalMenus = count($menus);
        
        $serializedMenus = $this->serializerService->serializeArray($menus, 'json', ['admin']);
        
        $response = new JsonResponse($serializedMenus, 200, []);
        $response->headers->set('Content-Range', "0-$totalMenus/$totalMenusAll");
        
        return $response;
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listMenus = $this->entityManager->getRepository(MenuPrototype::class)->findAll();
            
            $listMenusArray = [];
            
            foreach ($listMenus as $menu) {
                $menuArray = [
                    'id'        => $menu->getId(),
                    'label'     => $menu->getLabel(),
                    'category'  => $menu->getCategory(),
                    'connected' => $menu->isConnected(),
                    'habAdmin'  => $menu->isHabAdmin(),
                    'habBeta'   => $menu->isHabBeta(),
                    'habUser'   => $menu->isHabUser(),
                    'ville'     => $menu->isVille(),
                    'myVille'   => $menu->isMyVille(),
                    'icone'     => $menu->getIcone(),
                    'route'     => $menu->getRoute(),
                    'forUser'   => $menu->isForUser(),
                ];
                
                
                $listMenusArray[] = $menuArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/listMenu.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listMenusArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                throw new GestHordesException("Problème d'écriture dans le fichier !", GestHordesException::SHOW_MESSAGE);
            }
            
            // Construction du menu de base général du site
            
            $menu = $this->entityManager->getRepository(Menu::class)->findOneBy(['user' => null]);
            
            $menuElementArrayJson = [];
            
            foreach ($menu->getItems() as $menuElement) {
                
                if ($menuElement->getTypeMenu() === MenuType::TYPE_G) {
                    $enfant = [];
                    foreach ($menuElement->getItems() as $menuElementChild) {
                        $menuElementChildArray = [
                            'menu'      => $menuElementChild?->getMenu()?->getId() ?? null,
                            'type_menu' => MenuType::TYPE_M,
                            'ordre'     => $menuElementChild->getOrdre(),
                        ];
                        $enfant[]              = $menuElementChildArray;
                    }
                    $menuElementArray = [
                        'menu'      => null,
                        'parent'    => null,
                        'type_menu' => MenuType::TYPE_G,
                        'ordre'     => $menuElement->getOrdre(),
                        'name'      => $menuElement?->getName(),
                        'enfant'    => $enfant,
                    ];
                } else {
                    $menuElementArray = [
                        'menu'      => $menuElement?->getMenu()?->getId(),
                        'parent'    => null,
                        'type_menu' => MenuType::TYPE_M,
                        'ordre'     => $menuElement->getOrdre(),
                        'name'      => "",
                    ];
                }
                
                
                $menuElementArrayJson[] = $menuElementArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePathMenu = '../src/DataFixtures/data/menuBase.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonMenuString = json_encode($menuElementArrayJson,
                                             JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                             JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePathMenu, $newJsonMenuString) === false) {
                throw new GestHordesException("Problème d'écriture dans le fichier !", GestHordesException::SHOW_MESSAGE);
            }
            
            return new JsonResponse(['libRetour' => "Mise à jour OK."], Response::HTTP_OK);
            
        } catch (Exception $exception) {
            
            return $this->errorHandler->handleException($exception);
        }
        
    }
    
    #[Route('/save-menu/', name: 'save_menu', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function save_menu(Request $request): JsonResponse
    {
        
        try {
            
            // On récupére le menu de base général du site
            $menu = $this->entityManager->getRepository(Menu::class)
                                        ->createQueryBuilder("m")
                                        ->where('m.user IS NULL')
                                        ->setMaxResults(1)
                                        ->getQuery()
                                        ->getOneOrNullResult();
            
            if (!$menu) {
                // Si aucun menu n'existe, en créer un nouveau
                $menu = new Menu();
                $menu->setUser(null); // ou toute autre propriété nécessaire
            }
            
            $menuNew = new Menu();
            $this->serializerService->deserialize($request->getContent(), Menu::class, 'json', $menuNew, ['admin']);
            
            $this->menuHandler->miseAJourMenu($menu, $menuNew);
            
            $this->entityManager->persist($menu);
            $this->entityManager->flush();
            $this->entityManager->refresh($menu);
            
            $serializedMenus = $this->serializerService->serializeArray($menu, 'json', ['admin']);
            
            return new JsonResponse(['menu' => $serializedMenus], Response::HTTP_OK, []);
            
        } catch (Exception $exception) {
            return $this->errorHandler->handleException($exception);
        }
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(MenuPrototype $menu): JsonResponse
    {
        
        $serializedMenu = $this->serializerService->serializeArray($menu, 'json', ['admin']);
        
        return new JsonResponse($serializedMenu, 200);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, MenuPrototype $menu): JsonResponse
    {
        $menuNew = new MenuPrototype();
        $this->serializerService->deserialize($request->getContent(), MenuPrototype::class, 'json', $menuNew, ['admin']);
        
        $menu->setLabel($menuNew->getLabel())
             ->setCategory($menuNew->getCategory())
             ->setConnected($menuNew->isConnected())
             ->setHabAdmin($menuNew->isHabAdmin())
             ->setHabBeta($menuNew->isHabBeta())
             ->setHabUser($menuNew->isHabUser())
             ->setVille($menuNew->isVille())
             ->setMyVille($menuNew->isMyVille())
             ->setIcone($menuNew->getIcone())
             ->setRoute($menuNew->getRoute())
             ->setForUser($menuNew->isForUser());
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($menu);
        $this->entityManager->flush();
        
        $serializedMenu = $this->serializerService->serializeArray($menu, 'json', ['admin']);
        
        return new JsonResponse($serializedMenu, 200);
    }
    
}