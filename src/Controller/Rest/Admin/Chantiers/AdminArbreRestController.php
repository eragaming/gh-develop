<?php

namespace App\Controller\Rest\Admin\Chantiers;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\ChantierPrototype;
use App\Service\Encyclopedie\ChantiersHandler;
use App\Service\ErrorHandlingService;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\DiscordService;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Admin\ChantierCategories;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/rest/v1/admin/chantier_arbre', name: 'rest_admin_chantier_arbre_')]
class AdminArbreRestController extends AbstractRestGestHordesController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected UserHandler            $userHandler,
        protected GestHordesHandler      $gh,
        protected Security               $security,
        protected TranslatorInterface    $translator,
        protected LoggerInterface        $logger,
        protected TranslateHandler       $translateHandler,
        protected SerializerService      $serializerService,
        protected ErrorHandlingService   $errorHandler,
        protected DiscordService         $discordService,
        protected ChantiersHandler       $chantiersHandler,
    )
    {
        parent::__construct($entityManager, $userHandler, $gh, $security, $translator, $logger,
                            $translateHandler, $serializerService, $errorHandler, $discordService);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie();
        
        $serializedItems = $this->gh->getSerializer()->serialize($listChantiers, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'groups'                               => ['admin_arbre'],
        ]);
        
        return new JsonResponse($serializedItems, 200, [], true);
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->findAll();
            
            $listChantiersArray = [];
            
            foreach ($listChantiers as $chantier) {
                $chantierArray = [
                    'id'                   => $chantier->getId(),
                    'nom'                  => $chantier->getNom(),
                    'desc'                 => $chantier->getDescription(),
                    'icon'                 => $chantier->getIcon(),
                    'def'                  => $chantier->getDef(),
                    'water'                => $chantier->getWater(),
                    'pa'                   => $chantier->getPa(),
                    'niveau'               => $chantier->getNiveau(),
                    'plan'                 => $chantier->getPlan(),
                    'temp'                 => $chantier->getTemp(),
                    'pv'                   => $chantier->getPv(),
                    'indes'                => $chantier->getIndes(),
                    'ruineHo'              => $chantier->getRuineHo(),
                    'ruineHs'              => $chantier->getRuineHs(),
                    'ruineBu'              => $chantier->getRuineBu(),
                    'parent'               => $chantier->getParent()?->getId() ?? null,
                    'orderBy'              => $chantier->getOrderByListing(),
                    'levelMax'             => $chantier->getLevelMax(),
                    'cat'                  => $chantier->getCatChantier()->getId(),
                    'orderByListing'       => $chantier->getOrderByListing(),
                    'idHordes'             => $chantier->getIdHordes(),
                    'uid'                  => $chantier->getUid(),
                    'idMh'                 => $chantier->getIdMh(),
                    'actif'                => $chantier->isActif(),
                    'orderByGeneral'       => $chantier->getOrderByGeneral(),
                    'specifiqueVillePrive' => $chantier->isSpecifiqueVillePrive(),
                ];
                
                $ressourceArray = [];
                foreach ($chantier->getRessources() as $ressource) {
                    $ressourceArray[$ressource->getItem()->getId()] = $ressource->getNombre();
                }
                
                $chantierArray['ressources'] = $ressourceArray;
                
                $levelUps = [];
                foreach ($chantier->getLevelUps() as $levelUp) {
                    $bonus = [];
                    foreach ($levelUp->getBonusUps() as $bonusUp) {
                        $bonusTmp['id'] = $bonusUp->getId();
                        $bonusTmp['type_bonus'] = $bonusUp->getTypeBonus();
                        $bonusTmp['valeur'] = $bonusUp->getValeurUp();
                        
                        $bonus[] = $bonusTmp;
                    }
                    
                    $levelUpsTmp['id'] = $levelUp->getId();
                    $levelUpsTmp['lvl'] = $levelUp->getLevel();
                    $levelUpsTmp['bonus'] = $bonus;
                    
                    $levelUps[] = $levelUpsTmp;
                }
                
                $chantierArray['levelUps'] = $levelUps;
                
                $listChantiersArray[] = $chantierArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/chantiers.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listChantiersArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
        
    }
    
    #[Route('/majOrder', name: 'post', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majOrder(Request $request): JsonResponse
    {
        $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->recupCategorie();
        
        
        $this->chantiersHandler->recalculOrderBy($listChantiers);
        foreach ($listChantiers as $chantier) {
            $this->entityManager->persist($chantier);
        }
        try {
            $this->entityManager->flush();
            $serializedItems = json_decode($this->gh->getSerializer()->serialize($listChantiers, 'json', [
                AbstractNormalizer::IGNORED_ATTRIBUTES => [],
                'groups'                               => ['admin_arbre'],
            ]),                            null, 512, JSON_THROW_ON_ERROR);
            
            
            $retour['codeRetour']              = 0;
            $retour['libRetour']               = "Maj ok";
            $retour['zoneRetour']['categorie'] = $serializedItems;
            
            return new JsonResponse($retour, 200, [], false);
            
            
        } catch (Exception $exception) {
            
            
            $retour['codeRetour'] = 1;
            $retour['libRetour']  = "Problème mise à jour : {$exception->getMessage()}";
            
            return new JsonResponse($retour, Response::HTTP_INTERNAL_SERVER_ERROR, [], false);
        }
        
    }
    
    #[Route('/maj_arbre', name: 'post_arbre', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request): JsonResponse
    {
        $categoriesNew = new ChantierCategories();
        $this->gh->getSerializer()->deserialize($request->getContent(), ChantierCategories::class, 'json', [
            'groups'                               => ['admin_arbre'],
            AbstractNormalizer::IGNORED_ATTRIBUTES => [],
            'object_to_populate'                   => $categoriesNew,
        ]);
        
        
        // Utilisation de la fonction
        /** @var ChantierPrototype[] $chantierApplatis */
        $chantierApplatis = [];
        $this->chantiersHandler->aplatirChantiers($categoriesNew->getCategories(), $chantierApplatis);
        
        
        try {
            foreach ($chantierApplatis as $chantierApplati) {
                $chantierBdd = $this->entityManager->getRepository(ChantierPrototype::class)
                                                   ->findOneBy(['id' => $chantierApplati->getId()]);
                
                if ($chantierBdd !== null) {
                    $chantierBdd->setOrderByListing($chantierApplati->getOrderByListing());
                    $this->entityManager->persist($chantierBdd);
                }
                
            }
            
            $this->entityManager->flush();
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
        
    }
    
}