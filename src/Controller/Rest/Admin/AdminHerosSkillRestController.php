<?php

namespace App\Controller\Rest\Admin;

use App\Controller\Rest\AbstractRestGestHordesController;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillPrototype;
use App\Entity\HerosSkillType;
use App\Utils\StringManipulation;
use Exception;
use Proxies\__CG__\App\Entity\HerosKillType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/rest/v1/admin/herosskill', name: 'rest_admin_herosskill_')]
class AdminHerosSkillRestController extends AbstractRestGestHordesController
{
    
    #[Route('', name: 'create', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function create(Request $request): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $herosSkillTypeNew = new HerosSkillType();
        $this->serializerService->deserialize($request->getContent(), HerosSkillType::class, 'json', $herosSkillTypeNew, ['admin_heros_detail']);
        
        $herosSkillType = new HerosSkillType();
        
        // On vient mettre à jour le nom du type de compétence
        $lastId = $this->entityManager->getRepository(HerosSkillType::class)->getLastId();
        $herosSkillType->setId($lastId + 1)
                       ->setName($herosSkillTypeNew->getName());
        
        $this->entityManager->persist($herosSkillType);
        $this->entityManager->flush();
        $this->entityManager->refresh($herosSkillType);
        
        // On va ensuite traiter les niveaux, on stocke les niveaux actuels dans deux tableaux avec id et sans id
        $this->updateHerosSkillLevel($herosSkillType, $herosSkillTypeNew);
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($herosSkillType);
        $this->entityManager->flush();
        $this->entityManager->refresh($herosSkillType);
        
        $serializedPicto = $this->serializerService->serialize($herosSkillType, 'json', ['admin_heros_detail']);
        
        return new JsonResponse($serializedPicto, 200, [], true);
    }
    
    #[Route('/{id}', name: 'delete_id', methods: ['DELETE']), IsGranted('ROLE_ADMIN')]
    public function delete(HerosSkillType $herosSkillType): JsonResponse
    {
        $this->entityManager->remove($herosSkillType);
        $this->entityManager->flush();
        
        return new JsonResponse(null, 204);
    }
    
    #[Route('/', name: 'get', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        
        $filter     = json_decode($request->query->get('filter') ?? '{}', true, 512, JSON_THROW_ON_ERROR);
        $textSearch = $filter['q'] ?? '';
        $range      = $request->query->get('range');
        $range      =
            json_decode($range, true, 512, JSON_THROW_ON_ERROR); // Conversion de la chaîne JSON en tableau PHP
        $start      = $range[0];
        $end        = $range[1];
        
        // Création instance du QueryBuilder
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('hkt')
           ->from(HerosSkillType::class, 'hkt')
           ->setFirstResult($start)
           ->setMaxResults($end - $start + 1); // Calcul du nombre d'éléments à récupérer
        
        if ($textSearch !== '') {
            // Recherche sur les champs 'id' et 'pseudo'
            $idSearch = is_numeric($textSearch) ? (int)$textSearch : null;
            
            // On fait un groupe de conditions
            $orX = $qb->expr()->orX();
            
            // Toujours rechercher dans pseudo
            $orX->add($qb->expr()->like('hkt.name', ':name'));
            $qb->setParameter('name', '%' . $textSearch . '%');
            
            // On ajoute une condition sur l'id si $textSearch est numérique
            if ($idSearch !== null) {
                $orX->add($qb->expr()->eq('hkt.id', ':id'));
                $qb->setParameter('id', $idSearch);
            }
            
            // Appliquer les conditions au query builder
            $qb->andWhere($orX);
        }
        
        // On récupère le paramètre de tri depuis la requête React-admin (par exemple, 'sort' est le nom du champ)
        $sort      = $request->query->get('sort');
        $sortArray = json_decode($sort, true, 512, JSON_THROW_ON_ERROR);
        if ($sortArray && is_array($sortArray) && count($sortArray) === 2) {
            // Analyse du paramètre de tri (qui peut être au format 'field,order')
            [$field, $order] = $sortArray;
            
            // On s'assure que le champ de tri est valide (pour des raisons de sécurité)
            $allowedFields = ['id', 'name'];
            if (in_array($field, $allowedFields)) {
                // On convertit le field fournis en camelCase
                $field = (new StringManipulation())->underscoreToCamelCase($field);
                // On ajoute le tri à la requête Doctrine
                $qb->orderBy('hkt.' . $field, $order);
            }
        }
        
        // Exécution de la requête et récupération les résultats
        $herosSkillTypes = $qb->getQuery()->getResult();
        
        $totalSkillTypesAll = count($this->entityManager->getRepository(HerosSkillType::class)->findAll());
        
        
        $totalPictos = count($herosSkillTypes);
        
        $serializedPictos = $this->serializerService->serialize($herosSkillTypes, 'json', ['admin_heros_list']);
        
        $response = new JsonResponse($serializedPictos, 200, [], true);
        $response->headers->set('Content-Range', "0-$totalPictos/$totalSkillTypesAll");
        
        return $response;
    }
    
    #[Route('/maj_json', name: 'maj_json', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function majJson(Request $request): JsonResponse
    {
        try {
            
            $listTypes = $this->entityManager->getRepository(HerosSkillType::class)->findAll();
            
            $listSkillTypesArray = [];
            
            foreach ($listTypes as $skillType) {
                
                
                // On va traiter les niveaux
                $niveauArray = [];
                foreach ($skillType->getLevel() as $niveau) {
                    
                    // On va traiter les pouvoirs
                    $pouvoirArray = [];
                    foreach ($niveau->getPouvoir() as $pouvoir) {
                        $pouvoirArray[] = [
                            'id'     => $pouvoir->getId(),
                            'value'  => $pouvoir->getValue(),
                            'value2' => $pouvoir->getValue2(),
                            'text'   => $pouvoir->getText(),
                            'heros'  => $pouvoir->getHeros()?->getId() ?? null, // On ne récupère que l'id
                        ];
                    }
                    
                    $niveauArray[] = [
                        'id'      => $niveau->getId(),
                        'name'    => $niveau->getName(),
                        'xp'      => $niveau->getXp(),
                        'pouvoir' => $pouvoirArray,
                    ];
                }
                
                $objetArray = [
                    'id'    => $skillType->getId(),
                    'name'  => $skillType->getName(),
                    'level' => $niveauArray,
                ];
                
                $listSkillTypesArray[] = $objetArray;
            }
            
            // Le chemin doit être relatif au répertoire où se trouve ce script ou absolu.
            $filePath = '../src/DataFixtures/data/herosSkill.json';
            
            
            // Encodage des données mises à jour en JSON
            $newJsonString = json_encode($listSkillTypesArray,
                                         JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT |
                                         JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
            
            // Écriture des données mises à jour dans le fichier
            if (file_put_contents($filePath, $newJsonString) === false) {
                // Échec de l'écriture dans le fichier
                return new JsonResponse([
                                            'codeRetour' => 1,
                                            'libRetour'  => "Problème d'écriture dans le fichier !",
                                        ]);
            }
            
            
            return new JsonResponse([
                                        'codeRetour' => 0,
                                        'libRetour'  => "Mise à jour OK.",
                                    ]);
            
        } catch (Exception $exception) {
            
            return new JsonResponse([
                                        'codeRetour' => 1,
                                        'libRetour'  => "Problème mise à jour : {$exception->getMessage()}",
                                    ]);
        }
        
    }
    
    #[Route('/{id}', name: 'get_id', methods: ['GET']), IsGranted('ROLE_ADMIN')]
    public function show(HerosSkillType $herosSkillType): JsonResponse
    {
        
        $serializedHerosSkill = $this->serializerService->serialize($herosSkillType, 'json', ['admin_heros_detail']);
        
        return new JsonResponse($serializedHerosSkill, 200, [], true);
    }
    
    #[Route('/{id}', name: 'put_id', methods: ['PUT']), IsGranted('ROLE_ADMIN')]
    public function update(Request $request, HerosSkillType $herosSkillType): JsonResponse
    {
        // Utilisez le désérialiseur pour convertir les données JSON en objet Quête
        $herosSkillTypeNew = new HerosSkillType();
        $this->serializerService->deserialize($request->getContent(), HerosSkillType::class, 'json', $herosSkillTypeNew, ['admin_heros_detail']);
        
        // On vient mettre à jour le nom du type de compétence
        $herosSkillType->setName($herosSkillTypeNew->getName());
        
        // On va ensuite traiter les niveaux, on stocke les niveaux actuels dans deux tableaux avec id et sans id
        $this->updateHerosSkillLevel($herosSkillType, $herosSkillTypeNew);
        
        
        // Persistez les modifications dans la base de données
        $this->entityManager->persist($herosSkillType);
        $this->entityManager->flush();
        $this->entityManager->refresh($herosSkillType);
        
        $serializedPicto = $this->serializerService->serialize($herosSkillType, 'json', ['admin_heros_detail']);
        
        return new JsonResponse($serializedPicto, 200, [], true);
    }
    
    private function updateHerosSkillLevel(HerosSkillType &$herosSkillType, HerosSkillType $herosSkillTypeNew): void
    {
        // On va traiter les niveaux du type de compétence, on stocke les niveaux nouveaux dans deux tableaux avec id et sans id
        $niveauxActuels = [];
        $niveauxSansId  = [];
        foreach ($herosSkillTypeNew->getLevel() as $niveau) {
            $niveauxActuels[$niveau->getId()] = $niveau;
            $niveauxSansId[]                  = $niveau;
        }
        
        // On va partir des niveaux actuels pour supprimer ou mettre à jour les niveaux
        foreach ($herosSkillType->getLevel() as $niveau) {
            if (isset($niveauxActuels[$niveau->getId()])) {
                // On met à jour le niveau
                $niveau->setName($niveauxActuels[$niveau->getId()]->getName());
                $niveau->setXp($niveauxActuels[$niveau->getId()]->getXp());
            } else {
                // On supprime le niveau
                $this->entityManager->remove($niveau);
                $herosSkillType->removeLevel($niveau);
            }
        }
        
        // On va ajouter les compétences qui n'ont pas d'id
        foreach ($niveauxSansId as $niveau) {
            if ($niveau->getId() === null) {
                $lastId             = $this->entityManager->getRepository(HerosSkillLevel::class)->getLastId();
                $newHerosSkillLevel = new HerosSkillLevel();
                $newHerosSkillLevel->setId($lastId + 1)
                                   ->setName($niveau->getName())
                                   ->setXp($niveau->getXp())
                                   ->setHerosSkillType($herosSkillType);
                
                $this->entityManager->persist($newHerosSkillLevel);
                $this->entityManager->flush();
                $this->entityManager->refresh($newHerosSkillLevel);
                
                $herosSkillType->addLevel($newHerosSkillLevel);
                
                $this->updateHerosSkillPrototype($newHerosSkillLevel, $niveau);
                
            }
        }
    }
    
    private function updateHerosSkillPrototype(HerosSkillLevel &$herosSkillLevel, HerosSkillLevel $herosSkillLevelNew): void
    {
        // On va traiter les compétences du niveaux de skill, on stocke les compétences nouvelles dans deux tableaux avec id et sans id
        $pouvoirsActuels = [];
        $pouvoirsSansId  = [];
        
        foreach ($herosSkillLevelNew->getPouvoir() as $pouvoir) {
            $pouvoirsActuels[$pouvoir->getId()] = $pouvoir;
            $pouvoirsSansId[]                   = $pouvoir;
        }
        
        // On va partir des compétences actuelles pour supprimer ou mettre à jour les compétences
        foreach ($herosSkillLevel->getPouvoir() as $pouvoir) {
            if (isset($pouvoirsActuels[$pouvoir->getId()])) {
                // On met à jour la compétence
                $pouvoir->setValue($pouvoirsActuels[$pouvoir->getId()]->getValue());
                $pouvoir->setValue2($pouvoirsActuels[$pouvoir->getId()]->getValue2());
                $pouvoir->setText($pouvoirsActuels[$pouvoir->getId()]->getText());
                $pouvoir->setHeros($pouvoirsActuels[$pouvoir->getId()]->getHeros());
            } else {
                // On supprime la compétence
                $this->entityManager->remove($pouvoir);
                $herosSkillLevel->removePouvoir($pouvoir);
            }
        }
        
        // On va ajouter les compétences qui n'ont pas d'id
        foreach ($pouvoirsSansId as $pouvoir) {
            if ($pouvoir->getId() === null) {
                $lastId = $this->entityManager->getRepository(HerosSkillPrototype::class)->getLastId();
                
                $newHerosSkillPrototype = new HerosSkillPrototype();
                $newHerosSkillPrototype->setText($pouvoir->getText())
                                       ->setId($lastId + 1)
                                       ->setValue($pouvoir->getValue())
                                       ->setValue2($pouvoir->getValue2())
                                       ->setHerosSkillLevel($herosSkillLevel);
                
                // Récupération du pouvoir heros associés
                if ($pouvoir->getHeros() !== null) {
                    $heros = $this->entityManager->getRepository(HerosPrototype::class)->find($pouvoir->getHeros()->getId());
                    if ($heros !== null) {
                        $newHerosSkillPrototype->setHeros($heros);
                    }
                }
                $this->entityManager->persist($newHerosSkillPrototype);
                $this->entityManager->flush();
                $this->entityManager->refresh($newHerosSkillPrototype);
                
                $herosSkillLevel->addPouvoir($newHerosSkillPrototype);
                
            }
        }
    }
    
}