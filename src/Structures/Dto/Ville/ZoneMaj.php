<?php

namespace App\Structures\Dto\Ville;

use App\Entity\MapItem;

class ZoneMaj
{
    private ?int $dried     = null;
    private ?int $zombie    = null;
    private ?int $balise    = null;
    private ?int $zombieMin = null;
    private ?int $zombieMax = null;
    private ?int $x         = null;
    private ?int $y         = null;
    private ?int $xRel      = null;
    private ?int $yRel      = null;
    private ?int $distance  = null;
    private ?int $paAller   = null;
    private ?int $paRetour  = null;
    /**
     * @var MapItem[] $items
     */
    private array $items;
    
    /**
     * @return int|null
     */
    public function getBalise(): ?int
    {
        return $this->balise;
    }
    
    /**
     * @param int|null $balise
     * @return ZoneMaj
     */
    public function setBalise(?int $balise): ZoneMaj
    {
        $this->balise = $balise;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }
    
    /**
     * @param int|null $distance
     * @return ZoneMaj
     */
    public function setDistance(?int $distance): ZoneMaj
    {
        $this->distance = $distance;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getDried(): ?int
    {
        return $this->dried;
    }
    
    /**
     * @param int|null $dried
     * @return ZoneMaj
     */
    public function setDried(?int $dried): ZoneMaj
    {
        $this->dried = $dried;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @return ZoneMaj
     */
    public function setItems(array $items): ZoneMaj
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPaAller(): ?int
    {
        return $this->paAller;
    }
    
    /**
     * @param int|null $paAller
     * @return ZoneMaj
     */
    public function setPaAller(?int $paAller): ZoneMaj
    {
        $this->paAller = $paAller;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPaRetour(): ?int
    {
        return $this->paRetour;
    }
    
    /**
     * @param int|null $paRetour
     * @return ZoneMaj
     */
    public function setPaRetour(?int $paRetour): ZoneMaj
    {
        $this->paRetour = $paRetour;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getX(): ?int
    {
        return $this->x;
    }
    
    /**
     * @param int|null $x
     * @return ZoneMaj
     */
    public function setX(?int $x): ZoneMaj
    {
        $this->x = $x;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getXRel(): ?int
    {
        return $this->xRel;
    }
    
    /**
     * @param int|null $xRel
     * @return ZoneMaj
     */
    public function setXRel(?int $xRel): ZoneMaj
    {
        $this->xRel = $xRel;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getY(): ?int
    {
        return $this->y;
    }
    
    /**
     * @param int|null $y
     * @return ZoneMaj
     */
    public function setY(?int $y): ZoneMaj
    {
        $this->y = $y;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getYRel(): ?int
    {
        return $this->yRel;
    }
    
    /**
     * @param int|null $yRel
     * @return ZoneMaj
     */
    public function setYRel(?int $yRel): ZoneMaj
    {
        $this->yRel = $yRel;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getZombie(): ?int
    {
        return $this->zombie;
    }
    
    /**
     * @param int|null $zombie
     * @return ZoneMaj
     */
    public function setZombie(?int $zombie): ZoneMaj
    {
        $this->zombie = $zombie;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getZombieMax(): ?int
    {
        return $this->zombieMax;
    }
    
    /**
     * @param int|null $zombieMax
     * @return ZoneMaj
     */
    public function setZombieMax(?int $zombieMax): ZoneMaj
    {
        $this->zombieMax = $zombieMax;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getZombieMin(): ?int
    {
        return $this->zombieMin;
    }
    
    /**
     * @param int|null $zombieMin
     * @return ZoneMaj
     */
    public function setZombieMin(?int $zombieMin): ZoneMaj
    {
        $this->zombieMin = $zombieMin;
        return $this;
    }
    
    
}