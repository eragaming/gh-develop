<?php

namespace App\Structures\Dto\Ville;


class CaseMaj
{
    private ?int $mapid = null;
    
    private ?int $userid = null;
    
    private ?ZoneMaj $zone_maj = null;
    
    /**
     * @return int|null
     */
    public function getMapid(): ?int
    {
        return $this->mapid;
    }
    
    /**
     * @param int|null $mapid
     * @return CaseMaj
     */
    public function setMapid(?int $mapid): CaseMaj
    {
        $this->mapid = $mapid;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserid(): ?int
    {
        return $this->userid;
    }
    
    /**
     * @param int|null $userid
     * @return CaseMaj
     */
    public function setUserid(?int $userid): CaseMaj
    {
        $this->userid = $userid;
        
        return $this;
    }
    
    /**
     * @return ZoneMaj|null
     */
    public function getZoneMaj(): ?ZoneMaj
    {
        return $this->zone_maj;
    }
    
    /**
     * @param ZoneMaj|null $zone_maj
     * @return CaseMaj
     */
    public function setZoneMaj(?ZoneMaj $zone_maj): CaseMaj
    {
        $this->zone_maj = $zone_maj;
        
        return $this;
    }
    
    
}