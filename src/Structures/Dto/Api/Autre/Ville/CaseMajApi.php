<?php

namespace App\Structures\Dto\Api\Autre\Ville;

use App\Structures\Dto\Api\MH\ApiMaj;

class CaseMajApi extends ApiMaj
{
    
    public function getEpuise(): ?int
    {
        return $this->getField('epuise');
    }
    
    /**
     * @return MapItemApi[]|null
     */
    public function getItems(): ?array
    {
        return array_map(fn($i) => new MapItemApi($i), $this->getField('items'));
    }
    
    public function getKey(): ?string
    {
        return $this->getField("userKey");
    }
    
    public function getMapId(): ?int
    {
        return $this->getField('idMap');
    }
    
    public function getNbrZombie(): ?int
    {
        return $this->getField('nbrZombie') ?? $this->getField('NbrZombie');
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
}

