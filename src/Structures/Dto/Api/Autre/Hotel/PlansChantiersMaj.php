<?php

namespace App\Structures\Dto\Api\Autre\Hotel;

use App\Entity\PlansChantier;
use Symfony\Component\Serializer\Attribute\Groups;

class PlansChantiersMaj
{
    #[Groups(['plan'])]
    private ?int $mapId  = null;
    #[Groups(['plan'])]
    private ?int $userId = null;
    /** @var array|PlansChantier[] */
    #[Groups(['plan'])]
    private array $listPlans;
    
    /**
     * @return array
     */
    public function getListPlans(): array
    {
        return $this->listPlans;
    }
    
    /**
     * @return PlansChantiersMaj
     */
    public function setListPlans(array $listPlans): PlansChantiersMaj
    {
        $this->listPlans = $listPlans;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return PlansChantiersMaj
     */
    public function setMapId(?int $mapId): PlansChantiersMaj
    {
        $this->mapId = $mapId;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return PlansChantiersMaj
     */
    public function setUserId(?int $userId): PlansChantiersMaj
    {
        $this->userId = $userId;
        
        return $this;
    }
    
    
}