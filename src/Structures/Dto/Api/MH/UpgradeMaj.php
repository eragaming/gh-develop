<?php

namespace App\Structures\Dto\Api\MH;

class UpgradeMaj extends ApiMaj
{
    
    public function getBuildingId(): ?int
    {
        return $this->getField('buildingId');
    }
    
    public function getLevel(): ?int
    {
        return $this->getField('level');
    }
    
}