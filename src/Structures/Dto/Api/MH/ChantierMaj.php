<?php

namespace App\Structures\Dto\Api\MH;

class ChantierMaj extends ApiMaj
{
    
    public function getActions(): ?int
    {
        return $this->getField('actions');
    }
    
    public function getDef(): ?int
    {
        return $this->getField('def');
    }
    
    public function getId(): ?int
    {
        return $this->getField('id');
    }
    
    public function getLife(): ?int
    {
        return $this->getField('life');
    }
    
    public function getPa(): ?int
    {
        return $this->getField('pa');
    }
    
    public function getUid(): ?string
    {
        return $this->getField('uid');
    }
    
}