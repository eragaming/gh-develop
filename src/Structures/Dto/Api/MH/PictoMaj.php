<?php


namespace App\Structures\Dto\Api\MH;


class PictoMaj extends ApiMaj
{
    
    public function getIdPicto(): ?int
    {
        return $this->getField('id');
    }
    
    public function getNumber(): ?int
    {
        return $this->getField('number');
    }
    
    public function getTitres(): ?array
    {
        return $this->getField('titles');
    }
    
}