<?php

namespace App\Structures\Dto\Api\MH;

class EstimMaj extends ApiMaj
{
    public function getDays(): ?int
    {
        return $this->getField('days');
    }
    
    public function getMax(): ?int
    {
        return $this->getField('max');
    }
    
    public function getMin(): ?int
    {
        return $this->getField('min');
    }
    
    public function isMaxed(): ?bool
    {
        return $this->getField('maxed');
    }
}