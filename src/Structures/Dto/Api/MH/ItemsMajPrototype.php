<?php

namespace App\Structures\Dto\Api\MH;

class ItemsMajPrototype extends ApiMaj
{
    
    /**
     * @return ItemsMaj[]|null
     */
    public function getItems(): ?array
    {
        return array_combine(array_keys($this->data), array_map(fn($i) => new ItemsMaj($i), $this->data));
    }
    
    
}