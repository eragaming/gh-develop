<?php

namespace App\Structures\Dto\Api\MH;

class ExpeditionMaj extends ApiMaj
{
    
    public function getAuthor(): ?int
    {
        $tabAuthor = $this->getField('author');
        
        if ($tabAuthor === null) {
            return null;
        } else {
            return $tabAuthor['id'];
        }
    }
    
    public function getLength(): ?int
    {
        return $this->getField('length');
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
    public function getPointsX(): ?array
    {
        $tabPoints = $this->getField('points');
        
        if ($tabPoints === null) {
            return null;
        } else {
            return $tabPoints['x'];
        }
    }
    
    public function getPointsY(): ?array
    {
        $tabPoints = $this->getField('points');
        
        if ($tabPoints === null) {
            return null;
        } else {
            return $tabPoints['y'];
        }
    }
    
}