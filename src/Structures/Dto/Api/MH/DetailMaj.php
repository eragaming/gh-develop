<?php

namespace App\Structures\Dto\Api\MH;

class DetailMaj extends ApiMaj
{
    
    public function getDried(): ?bool
    {
        return $this->getField('dried');
    }
    
    public function getH(): ?int
    {
        return $this->getField('h');
    }
    
    public function getZ(): ?int
    {
        return $this->getField('z');
    }
    
}