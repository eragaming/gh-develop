<?php

namespace App\Structures\Dto\Api\MH;

class ZoneMaj extends ApiMaj
{
    
    public function getBuilding(): ?BatimentMaj
    {
        if ($this->getField('building') == null) {
            return null;
        } else {
            return new BatimentMaj($this->getField('building'));
        }
    }
    
    public function getDanger(): ?int
    {
        return $this->getField('danger');
    }
    
    public function getDetails(): ?DetailMaj
    {
        return new DetailMaj($this->getField('details'));
    }
    
    /**
     * @return ItemMaj[]|null
     */
    public function getItems(): ?array
    {
        if ($this->getField('items') != null) {
            return array_map(fn($i) => new ItemMaj($i), $this->getField('items'));
        } else {
            return [];
        }
    }
    
    public function getNvt(): ?bool
    {
        return $this->getField('nvt');
    }
    
    public function getTag(): ?int
    {
        return $this->getField('tag');
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
}