<?php

namespace App\Structures\Dto\Api\MH;

class MapMaj extends ApiMaj
{
    
    public function getBonusPts(): ?int
    {
        return $this->getField('bonusPts');
    }
    
    /**
     * @return CitoyensMaj[]|null
     */
    public function getCadavers(): ?array
    {
        if ($this->getField('cadavers') == null) {
            return [];
        }
        return array_map(fn($i) => new CitoyensMaj($i), $this->getField('cadavers'));
    }
    
    /**
     * @return CitoyensMaj[]|null
     */
    public function getCitizens(): ?array
    {
        return array_map(fn($i) => new CitoyensMaj($i), $this->getField('citizens'));
    }
    
    public function getCity(): ?CityMaj
    {
        return new CityMaj($this->getField('city'));
    }
    
    public function getDays(): ?int
    {
        return $this->getField('days');
    }
    
    /**
     * @return ExpeditionMaj[]|null
     */
    public function getExpeditions(): ?array
    {
        if ($this->getField('expeditions') == null) {
            return [];
        }
        return array_map(fn($i) => new ExpeditionMaj($i), $this->getField('expeditions'));
    }
    
    public function getGuide(): ?int
    {
        return $this->getField('guide');
    }
    
    public function getHei(): ?int
    {
        return $this->getField('hei');
    }
    
    public function getLanguage(): ?string
    {
        return $this->getField('language') ?? null;
    }
    
    public function getPhase(): ?string
    {
        return $this->getField('phase') ?? null;
    }
    
    public function getSeason(): ?int
    {
        return $this->getField('season');
    }
    
    public function getShaman(): ?int
    {
        return $this->getField('shaman');
    }
    
    public function getWid(): ?int
    {
        return $this->getField('wid');
    }
    
    /**
     * @return ZoneMaj[]|null
     */
    public function getZones(): ?array
    {
        return array_map(fn($i) => new ZoneMaj($i), $this->getField('zones'));
    }
    
    public function isConspiracy(): ?bool
    {
        return $this->getField('conspiracy');
    }
    
    public function isCustom(): ?bool
    {
        return $this->getField('custom');
    }
    
    public function isError(): bool
    {
        return !is_null($this->getField('error'));
    }
    
}