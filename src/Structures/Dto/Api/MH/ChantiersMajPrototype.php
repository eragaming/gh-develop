<?php

namespace App\Structures\Dto\Api\MH;

class ChantiersMajPrototype extends ApiMaj
{
    
    /**
     * @return ChantiersMaj[]|null
     */
    public function getChantiers(): ?array
    {
        return array_combine(array_keys($this->data), array_map(fn($i) => new ChantiersMaj($i), $this->data));
    }
    
    
}