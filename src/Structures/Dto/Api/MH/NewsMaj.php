<?php

namespace App\Structures\Dto\Api\MH;

class NewsMaj extends ApiMaj
{
    
    public function getContent(): ?NewsContentMaj
    {
        return new NewsContentMaj($this->getField('content'));
    }
    
    public function getDef(): ?int
    {
        return $this->getField('def');
    }
    
    public function getRegenDir(): ?RegenDirMaj
    {
        return new RegenDirMaj($this->getField('regenDir'));
    }
    
    public function getWater(): ?int
    {
        return $this->getField('water');
    }
    
    public function getZ(): ?int
    {
        return $this->getField('z');
    }
}