<?php

namespace App\Structures\Dto\Api\MH;

class DefenseMaj extends ApiMaj
{
    public function getBase(): ?int
    {
        return $this->getField('base');
    }
    
    public function getBonus(): ?float
    {
        return $this->getField('bonus');
    }
    
    public function getBuildings(): ?int
    {
        return $this->getField('buildings');
    }
    
    public function getCadavers(): ?int
    {
        return $this->getField('cadavers');
    }
    
    public function getCitizenGuardians(): ?int
    {
        return $this->getField('citizenGuardians');
    }
    
    public function getCitizenHomes(): ?int
    {
        return $this->getField('citizenHomes');
    }
    
    public function getItems(): ?int
    {
        return $this->getField('items');
    }
    
    public function getItemsMul(): ?float
    {
        return $this->getField('itemsMul');
    }
    
    public function getSouls(): ?int
    {
        return $this->getField('souls');
    }
    
    public function getTemp(): ?int
    {
        return $this->getField('temp');
    }
    
    public function getTotal(): ?int
    {
        return $this->getField('total');
    }
    
    public function getUpgrades(): ?int
    {
        return $this->getField('upgrades');
    }
    
    public function getWatchmen(): ?int
    {
        return $this->getField('watchmen');
    }
}