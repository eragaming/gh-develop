<?php

namespace App\Structures\Dto\Api\MH;

class CityMaj extends ApiMaj
{
    
    /**
     * @return ItemMaj[]|null
     */
    public function getBank(): ?array
    {
        return array_map(fn($i) => new ItemMaj($i), $this->getField('bank'));
    }
    
    /**
     * @return ChantierMaj[]|null
     */
    public function getBuildings(): ?array
    {
        return array_map(fn($i) => new ChantierMaj($i), $this->getField('buildings'));
    }
    
    /**
     * @return ChantierMaj[]|null
     */
    public function getChantiers(): ?array
    {
        return array_map(fn($i) => new ChantierMaj($i), $this->getField('chantiers'));
    }
    
    public function getDefense(): ?DefenseMaj
    {
        return new DefenseMaj($this->getField('defense'));
    }
    
    public function getEstimations(): ?EstimMaj
    {
        $estim = $this->getField('estimations');
        if (count($estim) === 0) {
            return null;
        } else {
            return new EstimMaj($this->getField('estimations'));
        }
    }
    
    public function getEstimationsNext(): ?EstimMaj
    {
        $estimNext = $this->getField('estimationsNext');
        if (count($estimNext) === 0) {
            return null;
        } else {
            return new EstimMaj($this->getField('estimationsNext'));
        }
    }
    
    public function getName(): ?string
    {
        return $this->getField('name');
    }
    
    public function getNews(): ?NewsMaj
    {
        return new NewsMaj($this->getField('news'));
    }
    
    public function getType(): ?string
    {
        return $this->getField('type');
    }
    
    /**
     * @return UpgradeMaj[]|null
     */
    public function getUpgrades(): ?array
    {
        return array_map(fn($i) => new UpgradeMaj($i), $this->getField('upgrades')['list'] ?? []);
    }
    
    public function getWater(): ?int
    {
        return $this->getField('water');
    }
    
    public function getX(): ?int
    {
        return $this->getField('x');
    }
    
    public function getY(): ?int
    {
        return $this->getField('y');
    }
    
    public function isChaos(): ?bool
    {
        return $this->getField('chaos');
    }
    
    public function isDevast(): ?bool
    {
        return $this->getField('devast');
    }
    
    public function isDoor(): ?bool
    {
        return $this->getField('door');
    }
    
    public function isHard(): ?bool
    {
        return $this->getField('hard');
    }
    
}