<?php

namespace App\Structures\Dto\Api\MH;

class BatsMajPrototype extends ApiMaj
{
    
    /**
     * @return BatsMaj[]|null
     */
    public function getBats(): ?array
    {
        return array_combine(array_keys($this->data), array_map(fn($i) => new BatsMaj($i), $this->data));
    }
    
    
}