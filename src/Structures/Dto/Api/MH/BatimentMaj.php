<?php

namespace App\Structures\Dto\Api\MH;

class BatimentMaj extends ApiMaj
{
    
    public function getCamped(): ?bool
    {
        return $this->getField('camped');
    }
    
    public function getDig(): ?int
    {
        return $this->getField('dig');
    }
    
    public function getDried(): ?bool
    {
        return $this->getField('dried');
    }
    
    public function getType(): ?int
    {
        return $this->getField('type');
    }
    
}