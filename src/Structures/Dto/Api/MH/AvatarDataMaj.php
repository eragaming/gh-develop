<?php

namespace App\Structures\Dto\Api\MH;

class AvatarDataMaj extends ApiMaj
{
    
    public function getClassic(): ?string
    {
        return $this->getField('classic');
    }
    
    public function getCompressed(): ?string
    {
        return $this->getField('compressed');
    }
    
    public function getFormat(): ?string
    {
        return $this->getField('format');
    }
    
    public function getUrl(): ?string
    {
        return $this->getField('url');
    }
    
    public function getX(): ?string
    {
        return $this->getField('x');
    }
    
    public function getY(): ?string
    {
        return $this->getField('y');
    }
    
}