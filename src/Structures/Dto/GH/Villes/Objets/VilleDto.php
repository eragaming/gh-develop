<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use App\Enum\EtatVille;
use Symfony\Component\Serializer\Attribute\Groups;

class VilleDto
{
    #[Groups(['comparatif'])]
    private string $nom = "";
    
    #[Groups(['comparatif'])]
    private EtatVille $etat = EtatVille::NORMAL;
    
    #[Groups(['comparatif'])]
    private int $saison = 0;
    
    #[Groups(['comparatif'])]
    private int $jour = 0;
    
    #[Groups(['comparatif'])]
    private int $mapId = 0;
    
    #[Groups(['comparatif'])]
    private int $points = 0;
    
    #[Groups(['comparatif'])]
    private ?string $nomJump = null;
    
    #[Groups(['comparatif'])]
    private ?int $x = 0;
    
    #[Groups(['comparatif'])]
    private ?int $y = 0;
    
    public function getEtat(): EtatVille
    {
        return $this->etat;
    }
    
    public function setEtat(EtatVille $etat): VilleDto
    {
        $this->etat = $etat;
        return $this;
    }
    
    public function getJour(): int
    {
        return $this->jour;
    }
    
    public function setJour(int $jour): VilleDto
    {
        $this->jour = $jour;
        return $this;
    }
    
    public function getMapId(): int
    {
        return $this->mapId;
    }
    
    public function setMapId(int $mapId): VilleDto
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    public function getNom(): string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): VilleDto
    {
        $this->nom = $nom;
        return $this;
    }
    
    public function getNomJump(): ?string
    {
        return $this->nomJump;
    }
    
    public function setNomJump(?string $nomJump): VilleDto
    {
        $this->nomJump = $nomJump;
        return $this;
    }
    
    public function getPoints(): int
    {
        return $this->points;
    }
    
    public function setPoints(int $points): VilleDto
    {
        $this->points = $points;
        return $this;
    }
    
    public function getSaison(): int
    {
        return $this->saison;
    }
    
    public function setSaison(int $saison): VilleDto
    {
        $this->saison = $saison;
        return $this;
    }
    
    public function getX(): ?int
    {
        return $this->x;
    }
    
    public function setX(?int $x): VilleDto
    {
        $this->x = $x;
        return $this;
    }
    
    public function getY(): ?int
    {
        return $this->y;
    }
    
    public function setY(?int $y): VilleDto
    {
        $this->y = $y;
        return $this;
    }
}