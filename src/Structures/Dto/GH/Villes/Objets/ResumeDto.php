<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use App\Entity\Defense;
use App\Entity\Estimation;
use Symfony\Component\Serializer\Attribute\Groups;

class ResumeDto
{
    #[Groups(['comparatif'])]
    private VilleDto $ville;
    
    #[Groups(['comparatif'])]
    private CitoyensDto $citoyens;
    
    #[Groups(['comparatif'])]
    private ProvisionsDto $provisions;
    
    #[Groups(['comparatif'])]
    private RessourcesDto $ressources;
    
    #[Groups(['comparatif'])]
    private Defense $defense;
    
    #[Groups(['comparatif'])]
    private ?Estimation $estim;
    
    public function getCitoyens(): CitoyensDto
    {
        return $this->citoyens;
    }
    
    public function setCitoyens(CitoyensDto $citoyens): ResumeDto
    {
        $this->citoyens = $citoyens;
        return $this;
    }
    
    public function getDefense(): Defense
    {
        return $this->defense;
    }
    
    public function setDefense(Defense $defense): ResumeDto
    {
        $this->defense = $defense;
        return $this;
    }
    
    public function getEstim(): ?Estimation
    {
        return $this->estim;
    }
    
    public function setEstim(?Estimation $estim): ResumeDto
    {
        $this->estim = $estim;
        return $this;
    }
    
    public function getProvisions(): ProvisionsDto
    {
        return $this->provisions;
    }
    
    public function setProvisions(ProvisionsDto $provisions): ResumeDto
    {
        $this->provisions = $provisions;
        return $this;
    }
    
    public function getRessources(): RessourcesDto
    {
        return $this->ressources;
    }
    
    public function setRessources(RessourcesDto $ressources): ResumeDto
    {
        $this->ressources = $ressources;
        return $this;
    }
    
    public function getVille(): VilleDto
    {
        return $this->ville;
    }
    
    public function setVille(VilleDto $ville): ResumeDto
    {
        $this->ville = $ville;
        return $this;
    }
}