<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use App\Entity\ZoneMap;
use Symfony\Component\Serializer\Attribute\Groups;

class CarteDetailDto
{
    #[Groups(['comparatif'])]
    private int $nbEpuise = 0;
    
    #[Groups(['comparatif'])]
    private int $nbDecouvert = 0;
    
    #[Groups(['comparatif'])]
    private int $nbCitoyen = 0;
    
    #[Groups(['comparatif'])]
    private int $nbTotal = 0;
    
    #[Groups(['comparatif'])]
    private int $taille = 0;
    
    /**
     * @var ZoneMap[][] $zones
     */
    #[Groups(['comparatif'])]
    private array $zones = [];
    
    public function addZone(ZoneMap $zone): CarteDetailDto
    {
        $this->zones[$zone->getY()][$zone->getX()] = $zone;
        return $this;
    }
    
    public function getNbCitoyen(): int
    {
        return $this->nbCitoyen;
    }
    
    public function setNbCitoyen(int $nbCitoyen): CarteDetailDto
    {
        $this->nbCitoyen = $nbCitoyen;
        return $this;
    }
    
    public function getNbDecouvert(): int
    {
        return $this->nbDecouvert;
    }
    
    public function setNbDecouvert(int $nbDecouvert): CarteDetailDto
    {
        $this->nbDecouvert = $nbDecouvert;
        return $this;
    }
    
    public function getNbEpuise(): int
    {
        return $this->nbEpuise;
    }
    
    public function setNbEpuise(int $nbEpuise): CarteDetailDto
    {
        $this->nbEpuise = $nbEpuise;
        return $this;
    }
    
    public function getNbTotal(): int
    {
        return $this->nbTotal;
    }
    
    public function setNbTotal(int $nbTotal): CarteDetailDto
    {
        $this->nbTotal = $nbTotal;
        return $this;
    }
    
    public function getTaille(): int
    {
        return $this->taille;
    }
    
    public function setTaille(int $taille): CarteDetailDto
    {
        $this->taille = $taille;
        return $this;
    }
    
    public function getZones(): array
    {
        return $this->zones;
    }
    
    public function setZones(array $zones): CarteDetailDto
    {
        $this->zones = $zones;
        return $this;
    }
    
    public function removeZone(ZoneMap $zone): CarteDetailDto
    {
        unset($this->zones[$zone->getY()][$zone->getX()]);
        return $this;
    }
}