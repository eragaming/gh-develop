<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use Symfony\Component\Serializer\Attribute\Groups;

class CitoyensDto
{
    #[Groups(['comparatif'])]
    private int $nbVivant = 0;
    
    #[Groups(['comparatif'])]
    private int $nbMort = 0;
    
    #[Groups(['comparatif'])]
    private int $nbBanni = 0;
    
    public function getNbBanni(): int
    {
        return $this->nbBanni;
    }
    
    public function setNbBanni(int $nbBanni): CitoyensDto
    {
        $this->nbBanni = $nbBanni;
        return $this;
    }
    
    public function getNbMort(): int
    {
        return $this->nbMort;
    }
    
    public function setNbMort(int $nbMort): CitoyensDto
    {
        $this->nbMort = $nbMort;
        return $this;
    }
    
    public function getNbVivant(): int
    {
        return $this->nbVivant;
    }
    
    public function setNbVivant(int $nbVivant): CitoyensDto
    {
        $this->nbVivant = $nbVivant;
        return $this;
    }
}