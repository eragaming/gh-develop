<?php

namespace App\Structures\Dto\GH\Villes\Objets;

use Symfony\Component\Serializer\Attribute\Groups;

class ProvisionsDto
{
    #[Groups(['comparatif'])]
    private int $nbEau = 0;
    
    #[Groups(['comparatif'])]
    private int $nbNourriture = 0;
    
    #[Groups(['comparatif'])]
    private int $nbDrogues = 0;
    
    #[Groups(['comparatif'])]
    private int $nbAlcool = 0;
    
    #[Groups(['comparatif'])]
    private int $nbCafe = 0;
    
    public function getNbAlcool(): int
    {
        return $this->nbAlcool;
    }
    
    public function setNbAlcool(int $nbAlcool): ProvisionsDto
    {
        $this->nbAlcool = $nbAlcool;
        return $this;
    }
    
    public function getNbCafe(): int
    {
        return $this->nbCafe;
    }
    
    public function setNbCafe(int $nbCafe): ProvisionsDto
    {
        $this->nbCafe = $nbCafe;
        return $this;
    }
    
    public function getNbDrogues(): int
    {
        return $this->nbDrogues;
    }
    
    public function setNbDrogues(int $nbDrogues): ProvisionsDto
    {
        $this->nbDrogues = $nbDrogues;
        return $this;
    }
    
    public function getNbEau(): int
    {
        return $this->nbEau;
    }
    
    public function setNbEau(int $nbEau): ProvisionsDto
    {
        $this->nbEau = $nbEau;
        return $this;
    }
    
    public function getNbNourriture(): int
    {
        return $this->nbNourriture;
    }
    
    public function setNbNourriture(int $nbNourriture): ProvisionsDto
    {
        $this->nbNourriture = $nbNourriture;
        return $this;
    }
}