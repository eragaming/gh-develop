<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions;

class ConsigneValidation
{
    private ?int  $expeditionPartId = null;
    private ?int  $consigneId       = null;
    private ?bool $valide           = null;
    
    /**
     * @return int|null
     */
    public function getConsigneId(): ?int
    {
        return $this->consigneId;
    }
    
    /**
     * @param int|null $consigneId
     * @return ConsigneValidation
     */
    public function setConsigneId(?int $consigneId): ConsigneValidation
    {
        $this->consigneId = $consigneId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getExpeditionPartId(): ?int
    {
        return $this->expeditionPartId;
    }
    
    /**
     * @param int|null $expeditionPartId
     * @return ConsigneValidation
     */
    public function setExpeditionPartId(?int $expeditionPartId): ConsigneValidation
    {
        $this->expeditionPartId = $expeditionPartId;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getValide(): ?bool
    {
        return $this->valide;
    }
    
    /**
     * @param bool|null $valide
     * @return ConsigneValidation
     */
    public function setValide(?bool $valide): ConsigneValidation
    {
        $this->valide = $valide;
        return $this;
    }
}