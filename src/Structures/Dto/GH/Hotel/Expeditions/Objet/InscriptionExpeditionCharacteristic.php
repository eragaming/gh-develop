<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions\Objet;

use App\Entity\Citoyens;
use App\Entity\DispoExpedition;
use App\Entity\DispoUserTypeExpedition;
use Symfony\Component\Serializer\Attribute\Groups;

class InscriptionExpeditionCharacteristic
{
    #[Groups('outils_expe')]
    private ?Citoyens $citoyen = null;
    
    #[Groups('outils_expe')]
    private ?DispoExpedition $dispo = null;
    
    #[Groups('outils_expe')]
    private ?DispoUserTypeExpedition $dispos = null;
    
    #[Groups('outils_expe')]
    private ?bool $soif = null;
    
    #[Groups('outils_expe')]
    private ?bool $placeVide = null;
    
    /**
     * @return Citoyens|null
     */
    public function getCitoyen(): ?Citoyens
    {
        return $this->citoyen;
    }
    
    /**
     * @param Citoyens|null $citoyen
     * @return InscriptionExpeditionCharacteristic
     */
    public function setCitoyen(?Citoyens $citoyen): InscriptionExpeditionCharacteristic
    {
        $this->citoyen = $citoyen;
        return $this;
    }
    
    /**
     * @return DispoExpedition|null
     */
    public function getDispo(): ?DispoExpedition
    {
        return $this->dispo;
    }
    
    /**
     * @param DispoExpedition|null $dispo
     * @return InscriptionExpeditionCharacteristic
     */
    public function setDispo(?DispoExpedition $dispo): InscriptionExpeditionCharacteristic
    {
        $this->dispo = $dispo;
        return $this;
    }
    
    /**
     * @return DispoUserTypeExpedition|null
     */
    public function getDispos(): ?DispoUserTypeExpedition
    {
        return $this->dispos;
    }
    
    /**
     * @param DispoUserTypeExpedition|null $dispos
     * @return InscriptionExpeditionCharacteristic
     */
    public function setDispos(?DispoUserTypeExpedition $dispos): InscriptionExpeditionCharacteristic
    {
        $this->dispos = $dispos;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    final  public function getPlaceVide(): ?bool
    {
        return $this->placeVide;
    }
    
    /**
     * @param bool|null $placeVide
     * @return InscriptionExpeditionCharacteristic
     */
    public function setPlaceVide(?bool $placeVide): InscriptionExpeditionCharacteristic
    {
        $this->placeVide = $placeVide;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getSoif(): ?bool
    {
        return $this->soif;
    }
    
    /**
     * @param bool|null $soif
     * @return InscriptionExpeditionCharacteristic
     */
    public function setSoif(?bool $soif): InscriptionExpeditionCharacteristic
    {
        $this->soif = $soif;
        return $this;
    }
}