<?php

namespace App\Structures\Dto\GH\Hotel\Expeditions;

class SauvegardeCommentaire
{
    private ?int    $expeditionPartId  = null;
    private ?string $expeditionnaireId = null;
    private ?string $commentaire       = null;
    
    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }
    
    public function setCommentaire(?string $commentaire): sauvegardeCommentaire
    {
        $this->commentaire = $commentaire;
        return $this;
    }
    
    public function getExpeditionPartId(): ?int
    {
        return $this->expeditionPartId;
    }
    
    public function setExpeditionPartId(?int $expeditionPartId): sauvegardeCommentaire
    {
        $this->expeditionPartId = $expeditionPartId;
        return $this;
    }
    
    public function getExpeditionnaireId(): ?string
    {
        return $this->expeditionnaireId;
    }
    
    public function setExpeditionnaireId(?string $expeditionnaireId): SauvegardeCommentaire
    {
        $this->expeditionnaireId = $expeditionnaireId;
        return $this;
    }
    
    
}