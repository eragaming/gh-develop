<?php

namespace App\Structures\Dto\GH\Hotel\Citoyens;

use Symfony\Component\Serializer\Attribute\Groups;

class CompetenceCitoyensMajRest
{
    #[Groups(['citoyens'])]
    private ?int $mapId     = null;
    #[Groups(['citoyens'])]
    private ?int $userId    = null;
    #[Groups(['citoyens'])]
    private ?int $citoyenId = null;
    #[Groups(['citoyens'])]
    private ?int $skillId   = null;
    #[Groups(['citoyens'])]
    private ?int $level     = null;
    
    public function getCitoyenId(): ?int
    {
        return $this->citoyenId;
    }
    
    public function setCitoyenId(?int $citoyenId): CompetenceCitoyensMajRest
    {
        $this->citoyenId = $citoyenId;
        return $this;
    }
    
    public function getLevel(): ?int
    {
        return $this->level;
    }
    
    public function setLevel(?int $level): CompetenceCitoyensMajRest
    {
        $this->level = $level;
        return $this;
    }
    
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    public function setMapId(?int $mapId): CompetenceCitoyensMajRest
    {
        $this->mapId = $mapId;
        return $this;
    }

    public function getSkillId(): ?int
    {
        return $this->skillId;
    }

    public function setSkillId(?int $skillId): CompetenceCitoyensMajRest
    {
        $this->skillId = $skillId;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): CompetenceCitoyensMajRest
    {
        $this->userId = $userId;
        return $this;
    }
    
}