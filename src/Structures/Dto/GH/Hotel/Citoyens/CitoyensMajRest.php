<?php

namespace App\Structures\Dto\GH\Hotel\Citoyens;

use App\Entity\Citoyens;
use Symfony\Component\Serializer\Attribute\Groups;

class CitoyensMajRest
{
    #[Groups(['citoyens'])]
    private ?int      $mapId    = null;
    #[Groups(['citoyens'])]
    private ?int      $userId   = null;
    #[Groups(['citoyens'])]
    private ?Citoyens $citoyens = null;
    #[Groups(['citoyens'])]
    private ?string   $nameForm = null;
    
    /**
     * @return ?Citoyens
     */
    public function getCitoyens(): ?Citoyens
    {
        return $this->citoyens;
    }
    
    /**
     * @return CitoyensMajRest
     */
    public function setCitoyens(?Citoyens $citoyens): CitoyensMajRest
    {
        $this->citoyens = $citoyens;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return CitoyensMajRest
     */
    public function setMapId(?int $mapId): CitoyensMajRest
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNameForm(): ?string
    {
        return $this->nameForm;
    }
    
    /**
     * @param string|null $nameForm
     * @return CitoyensMajRest
     */
    public function setNameForm(?string $nameForm): CitoyensMajRest
    {
        $this->nameForm = $nameForm;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return CitoyensMajRest
     */
    public function setUserId(?int $userId): CitoyensMajRest
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}