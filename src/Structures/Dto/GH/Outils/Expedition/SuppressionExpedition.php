<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use Symfony\Component\Serializer\Attribute\Groups;

class SuppressionExpedition
{
    #[Groups('outils_expe')]
    private ?int   $mapId  = null;
    #[Groups('outils_expe')]
    private ?int   $idUser = null;
    #[Groups('outils_expe')]
    private string $expeditionId;
    #[Groups('outils_expe')]
    private ?int   $jour   = null;
    
    /**
     * @return string
     */
    public function getExpeditionId(): string
    {
        return $this->expeditionId;
    }
    
    /**
     * @param string $expeditionId
     * @return ChangeStatutExpedition
     */
    public function setExpeditionId(string $expeditionId): SuppressionExpedition
    {
        $this->expeditionId = $expeditionId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return ChangeStatutExpedition
     */
    public function setIdUser(?int $idUser): SuppressionExpedition
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return ChangeStatutExpedition
     */
    public function setJour(?int $jour): SuppressionExpedition
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return ChangeStatutExpedition
     */
    public function setMapId(?int $mapId): SuppressionExpedition
    {
        $this->mapId = $mapId;
        return $this;
    }
    
}