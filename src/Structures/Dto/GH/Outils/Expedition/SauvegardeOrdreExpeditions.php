<?php

namespace App\Structures\Dto\GH\Outils\Expedition;

use App\Entity\Expedition;
use Symfony\Component\Serializer\Attribute\Groups;

class SauvegardeOrdreExpeditions
{
    #[Groups('outils_expe')]
    private ?int $mapId = null;
    
    #[Groups('outils_expe')]
    private ?int $idUser = null;
    
    /** @var Expedition[] $expeditions */
    #[Groups('outils_expe')]
    private array $expeditions = [];
    
    #[Groups('outils_expe')]
    private ?int $jour = null;
    
    /**
     * @return array
     */
    public function getExpeditions(): array
    {
        return $this->expeditions;
    }
    
    /**
     * @param array $expeditions
     * @return SauvegardeOrdreExpeditions
     */
    public function setExpeditions(array $expeditions): SauvegardeOrdreExpeditions
    {
        $this->expeditions = $expeditions;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdUser(): ?int
    {
        return $this->idUser;
    }
    
    /**
     * @param int|null $idUser
     * @return ChangeStatutExpedition
     */
    public function setIdUser(?int $idUser): SauvegardeOrdreExpeditions
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getJour(): ?int
    {
        return $this->jour;
    }
    
    /**
     * @param int|null $jour
     * @return ChangeStatutExpedition
     */
    public function setJour(?int $jour): SauvegardeOrdreExpeditions
    {
        $this->jour = $jour;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return ChangeStatutExpedition
     */
    public function setMapId(?int $mapId): SauvegardeOrdreExpeditions
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    
}