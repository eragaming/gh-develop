<?php

namespace App\Structures\Dto\GH\Ame;

use App\Entity\Pictos;

class Ame
{
    /**
     * @var Pictos[] $listPicto
     */
    private readonly array $listPicto;
}