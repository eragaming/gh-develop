<?php


namespace App\Structures\Dto\GH\Jump\Gestion;


use Symfony\Component\Serializer\Attribute\Groups;

class AssociateVilleDto
{
    #[Groups(['jump'])]
    private ?string $idJump = null;
    
    #[Groups(['jump'])]
    private ?int $userId = null;
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return AssociateVilleDto
     */
    public function setIdJump(?string $idJump): AssociateVilleDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return AssociateVilleDto
     */
    public function setUserId(?int $userId): AssociateVilleDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}