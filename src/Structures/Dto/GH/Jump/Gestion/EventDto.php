<?php


namespace App\Structures\Dto\GH\Jump\Gestion;


use App\Entity\Event;
use Symfony\Component\Serializer\Attribute\Groups;

class EventDto
{
    #[Groups(['event'])]
    private Event $event;
    
    public function getEvent(): Event
    {
        return $this->event;
    }
    
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }
}