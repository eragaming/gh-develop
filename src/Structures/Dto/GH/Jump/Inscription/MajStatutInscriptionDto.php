<?php

namespace App\Structures\Dto\GH\Jump\Inscription;

use Symfony\Component\Serializer\Attribute\Groups;

class MajStatutInscriptionDto
{
    #[Groups(['jump'])]
    private ?int $userId = null;
    
    #[Groups(['jump'])]
    private ?string $idJump = null;
    
    #[Groups(['jump'])]
    private ?int $idStatut = null;
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return MajStatutInscriptionDto
     */
    public function setIdJump(?string $idJump): MajStatutInscriptionDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getIdStatut(): ?int
    {
        return $this->idStatut;
    }
    
    /**
     * @param int|null $idStatut
     * @return MajStatutInscriptionDto
     */
    public function setIdStatut(?int $idStatut): MajStatutInscriptionDto
    {
        $this->idStatut = $idStatut;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return MajStatutInscriptionDto
     */
    public function setUserId(?int $userId): MajStatutInscriptionDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    
}
