<?php

namespace App\Structures\Dto\GH\Jump\Inscription;

use App\Entity\InscriptionJump;
use Symfony\Component\Serializer\Attribute\Groups;

class InscriptionJumpRestDto
{
    #[Groups(['inscription'])]
    private ?int $userId = null;
    
    #[Groups(['inscription'])]
    private ?string $jumpId = null;
    
    #[Groups(['inscription'])]
    private ?InscriptionJump $inscription = null;
    
    #[Groups(['inscription'])]
    private ?bool $isOrga = null;
    
    /**
     * @return InscriptionJump|null
     */
    public function getInscription(): ?InscriptionJump
    {
        return $this->inscription;
    }
    
    /**
     * @param InscriptionJump|null $inscription
     * @return InscriptionJumpRestDto
     */
    public function setInscription(?InscriptionJump $inscription): self
    {
        $this->inscription = $inscription;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getIsOrga(): ?bool
    {
        return $this->isOrga;
    }
    
    /**
     * @param bool|null $isOrga
     * @return self
     */
    public function setIsOrga(?bool $isOrga): self
    {
        $this->isOrga = $isOrga;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJumpId(): ?string
    {
        return $this->jumpId;
    }
    
    /**
     * @param string|null $jumpId
     * @return InscriptionJumpRestDto
     */
    public function setJumpId(?string $jumpId): self
    {
        $this->jumpId = $jumpId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return InscriptionJumpRestDto
     */
    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }
    
}
