<?php

namespace App\Structures\Dto\GH\Jump\Inscription;

use Symfony\Component\Serializer\Attribute\Groups;

class MajMetierDefinitifDto
{
    #[Groups(['coalition'])]
    private ?int $userId = null;
    
    #[Groups(['coalition'])]
    private ?string $idJump = null;
    
    #[Groups(['coalition'])]
    private ?int $idJob = null;
    
    #[Groups(['coalition'])]
    private ?int $userIdMaj = null;
    
    /**
     * @return int|null
     */
    public function getIdJob(): ?int
    {
        return $this->idJob;
    }
    
    /**
     * @param int|null $idJob
     * @return MajMetierDefinitifDto
     */
    public function setIdJob(?int $idJob): MajMetierDefinitifDto
    {
        $this->idJob = $idJob;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return MajMetierDefinitifDto
     */
    public function setIdJump(?string $idJump): MajMetierDefinitifDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return MajMetierDefinitifDto
     */
    public function setUserId(?int $userId): MajMetierDefinitifDto
    {
        $this->userId = $userId;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserIdMaj(): ?int
    {
        return $this->userIdMaj;
    }
    
    /**
     * @param int|null $userIdMaj
     * @return MajMetierDefinitifDto
     */
    public function setUserIdMaj(?int $userIdMaj): MajMetierDefinitifDto
    {
        $this->userIdMaj = $userIdMaj;
        return $this;
    }
    
    
}
