<?php

namespace App\Structures\Dto\GH\Jump\Inscription;

use Symfony\Component\Serializer\Attribute\Groups;

class InscriptionJumpMasqueRest
{
    #[Groups(['inscription'])]
    private ?int    $userId = null;
    #[Groups(['inscription'])]
    private ?string $idJump = null;
    #[Groups(['inscription'])]
    private ?bool   $sens   = null;
    
    /**
     * @return string|null
     */
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    /**
     * @param string|null $idJump
     * @return self
     */
    public function setIdJump(?string $idJump): self
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getSens(): ?bool
    {
        return $this->sens;
    }
    
    /**
     * @param bool|null $sens
     * @return self
     */
    public function setSens(?bool $sens): self
    {
        $this->sens = $sens;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return self
     */
    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }
    
}
