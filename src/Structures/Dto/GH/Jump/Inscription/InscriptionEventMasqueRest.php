<?php

namespace App\Structures\Dto\GH\Jump\Inscription;

use Symfony\Component\Serializer\Attribute\Groups;

class InscriptionEventMasqueRest
{
    #[Groups(['inscription'])]
    private ?int    $userId  = null;
    #[Groups(['inscription'])]
    private ?string $idEvent = null;
    #[Groups(['inscription'])]
    private ?bool   $sens    = null;
    
    /**
     * @return string|null
     */
    public function getIdEvent(): ?string
    {
        return $this->idEvent;
    }
    
    /**
     * @param string|null $idEvent
     * @return self
     */
    public function setIdEvent(?string $idEvent): self
    {
        $this->idEvent = $idEvent;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getSens(): ?bool
    {
        return $this->sens;
    }
    
    /**
     * @param bool|null $sens
     * @return self
     */
    public function setSens(?bool $sens): self
    {
        $this->sens = $sens;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     * @return self
     */
    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }
    
}
