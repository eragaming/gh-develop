<?php


namespace App\Structures\Dto\GH\Jump\Coalition;


use Symfony\Component\Serializer\Attribute\Groups;

class CoalitionStatutDto
{
    #[Groups(['coalition'])]
    private string $idJump;
    
    #[Groups(['coalition'])]
    private int $userId;
    
    #[Groups(['coalition'])]
    private int $idStatut;
    
    /**
     * @return string
     */
    public function getIdJump(): string
    {
        return $this->idJump;
    }
    
    /**
     * @return CoalitionStatutDto
     */
    public function setIdJump(string $idJump): CoalitionStatutDto
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdStatut(): int
    {
        return $this->idStatut;
    }
    
    /**
     * @return CoalitionStatutDto
     */
    public function setIdStatut(int $idStatut): CoalitionStatutDto
    {
        $this->idStatut = $idStatut;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    /**
     * @return CoalitionStatutDto
     */
    public function setUserId(int $userId): CoalitionStatutDto
    {
        $this->userId = $userId;
        return $this;
    }
    
}