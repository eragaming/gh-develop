<?php

namespace App\Structures\Dto\GH\Jump\Coalition;

use Symfony\Component\Serializer\Attribute\Groups;

class CompetenceCoalitionMajRest
{
    #[Groups(['coalition'])]
    private ?string $idJump = null;
    
    #[Groups(['coalition'])]
    private ?int $userId = null;
    
    #[Groups(['coalition'])]
    private ?int $userIdMaj = null;
    
    #[Groups(['coalition'])]
    private ?int $idSkill = null;
    
    #[Groups(['coalition'])]
    private ?int $lvlSkill = null;
    
    public function getIdJump(): ?string
    {
        return $this->idJump;
    }
    
    public function setIdJump(?string $idJump): CompetenceCoalitionMajRest
    {
        $this->idJump = $idJump;
        return $this;
    }
    
    public function getIdSkill(): ?int
    {
        return $this->idSkill;
    }
    
    public function setIdSkill(?int $idSkill): CompetenceCoalitionMajRest
    {
        $this->idSkill = $idSkill;
        return $this;
    }
    
    public function getLvlSkill(): ?int
    {
        return $this->lvlSkill;
    }
    
    public function setLvlSkill(?int $lvlSkill): CompetenceCoalitionMajRest
    {
        $this->lvlSkill = $lvlSkill;
        return $this;
    }
    
    public function getUserId(): ?int
    {
        return $this->userId;
    }
    
    public function setUserId(?int $userId): CompetenceCoalitionMajRest
    {
        $this->userId = $userId;
        return $this;
    }
    
    public function getUserIdMaj(): ?int
    {
        return $this->userIdMaj;
    }
    
    public function setUserIdMaj(?int $userIdMaj): CompetenceCoalitionMajRest
    {
        $this->userIdMaj = $userIdMaj;
        return $this;
    }
    
}