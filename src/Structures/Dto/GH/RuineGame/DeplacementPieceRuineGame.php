<?php

namespace App\Structures\Dto\GH\RuineGame;

class DeplacementPieceRuineGame
{
    private int           $idUser;
    private CaseRuineGame $case;
    private string        $idRuineGame;
    private bool          $inPiece;
    
    /**
     * @return CaseRuineGame
     */
    public function getCase(): CaseRuineGame
    {
        return $this->case;
    }
    
    /**
     * @param CaseRuineGame $case
     * @return FouillePieceRuineGame
     */
    public function setCase(CaseRuineGame $case): DeplacementPieceRuineGame
    {
        $this->case = $case;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return FouillePieceRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): DeplacementPieceRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return FouillePieceRuineGame
     */
    public function setIdUser(int $idUser): DeplacementPieceRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isInPiece(): bool
    {
        return $this->inPiece;
    }
    
    /**
     * @param bool $inPiece
     * @return DeplacementPieceRuineGame
     */
    public function setInPiece(bool $inPiece): DeplacementPieceRuineGame
    {
        $this->inPiece = $inPiece;
        return $this;
    }
}