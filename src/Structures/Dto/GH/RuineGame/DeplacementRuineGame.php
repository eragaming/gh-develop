<?php

namespace App\Structures\Dto\GH\RuineGame;

use Symfony\Component\Serializer\Attribute\Groups;

class DeplacementRuineGame
{
    #[Groups(['ruine', 'admin_ruine'])]
    private int $idUser;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private CaseRuineGame $case;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private string $idRuineGame;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private CaseRuineGame $caseSuivante;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private bool $caseFuite;
    
    /**
     * @return CaseRuineGame
     */
    public function getCase(): CaseRuineGame
    {
        return $this->case;
    }
    
    /**
     * @param CaseRuineGame $case
     * @return DeplacementRuineGame
     */
    public function setCase(CaseRuineGame $case): DeplacementRuineGame
    {
        $this->case = $case;
        return $this;
    }
    
    /**
     * @return CaseRuineGame
     */
    public function getCaseSuivante(): CaseRuineGame
    {
        return $this->caseSuivante;
    }
    
    /**
     * @param CaseRuineGame $caseSuivante
     * @return DeplacementRuineGame
     */
    public function setCaseSuivante(CaseRuineGame $caseSuivante): DeplacementRuineGame
    {
        $this->caseSuivante = $caseSuivante;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getIdRuineGame(): string
    {
        return $this->idRuineGame;
    }
    
    /**
     * @param string $idRuineGame
     * @return DeplacementRuineGame
     */
    public function setIdRuineGame(string $idRuineGame): DeplacementRuineGame
    {
        $this->idRuineGame = $idRuineGame;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdUser(): int
    {
        return $this->idUser;
    }
    
    /**
     * @param int $idUser
     * @return DeplacementRuineGame
     */
    public function setIdUser(int $idUser): DeplacementRuineGame
    {
        $this->idUser = $idUser;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isCaseFuite(): bool
    {
        return $this->caseFuite;
    }
    
    /**
     * @param bool $caseFuite
     * @return DeplacementRuineGame
     */
    public function setCaseFuite(bool $caseFuite): DeplacementRuineGame
    {
        $this->caseFuite = $caseFuite;
        return $this;
    }
}