<?php

namespace App\Structures\Dto\GH\RuineGame;

class ScoreRuineGame
{
    private ?int  $pctKill       = 0;
    private ?int  $pctExplo      = 0;
    private ?int  $pctFouille    = 0;
    private ?bool $ejected       = false;
    private ?int  $oxygenRestant = 0;
    
    /**
     * @return int|null
     */
    public function getOxygenRestant(): ?int
    {
        return $this->oxygenRestant;
    }
    
    /**
     * @param int|null $oxygenRestant
     * @return ScoreRuineGame
     */
    public function setOxygenRestant(?int $oxygenRestant): ScoreRuineGame
    {
        $this->oxygenRestant = $oxygenRestant;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPctExplo(): ?int
    {
        return $this->pctExplo;
    }
    
    /**
     * @param int|null $pctExplo
     * @return ScoreRuineGame
     */
    public function setPctExplo(?int $pctExplo): ScoreRuineGame
    {
        $this->pctExplo = $pctExplo;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPctFouille(): ?int
    {
        return $this->pctFouille;
    }
    
    /**
     * @param int|null $pctFouille
     * @return ScoreRuineGame
     */
    public function setPctFouille(?int $pctFouille): ScoreRuineGame
    {
        $this->pctFouille = $pctFouille;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPctKill(): ?int
    {
        return $this->pctKill;
    }
    
    /**
     * @param int|null $pctKill
     * @return ScoreRuineGame
     */
    public function setPctKill(?int $pctKill): ScoreRuineGame
    {
        $this->pctKill = $pctKill;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isEjected(): ?bool
    {
        return $this->ejected;
    }
    
    /**
     * @param bool|null $ejected
     * @return ScoreRuineGame
     */
    public function setEjected(?bool $ejected): ScoreRuineGame
    {
        $this->ejected = $ejected;
        return $this;
    }
}