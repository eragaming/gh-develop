<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

use App\Structures\Dto\GH\RuineGame\ScoreRuineGame;

class EndGameResponse
{
    private ScoreRuineGame $score;
    
    /**
     * @return ScoreRuineGame
     */
    public function getScore(): ScoreRuineGame
    {
        return $this->score;
    }
    
    /**
     * @param ScoreRuineGame $score
     * @return EndGameResponse
     */
    public function setScore(ScoreRuineGame $score): EndGameResponse
    {
        $this->score = $score;
        return $this;
    }
}