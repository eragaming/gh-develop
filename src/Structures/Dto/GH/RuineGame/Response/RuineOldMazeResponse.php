<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

use App\Structures\Dto\GH\RuineGame\CaseRuineGame;
use Symfony\Component\Serializer\Attribute\Groups;

class RuineOldMazeResponse
{
    /** @var CaseRuineGame[][][] $ruinePlan */
    #[Groups(['ruine'])]
    private ?array $ruinePlan = null;
    
    /**
     * @return array|null
     */
    public function getRuinePlan(): ?array
    {
        return $this->ruinePlan;
    }
    
    /**
     * @param array|null $ruinePlan
     * @return RuineOldMazeResponse
     */
    public function setRuinePlan(?array $ruinePlan): RuineOldMazeResponse
    {
        $this->ruinePlan = $ruinePlan;
        return $this;
    }
    
}