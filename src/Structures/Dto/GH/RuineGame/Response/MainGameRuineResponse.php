<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

use App\Entity\User;
use App\Structures\Dto\GH\RuineGame\NiveauDifficulte;
use Symfony\Component\Serializer\Attribute\Groups;

class MainGameRuineResponse extends RuineMazeResponse
{
    
    /** @var NiveauDifficulte[] $listeNiveauDifficulte */
    #[Groups(['ruine'])]
    private array $listeNiveauDifficulte = [];
    
    #[Groups(['ruine'])]
    private User $userCss;
    
    /**
     * @var string[] $tradPorte
     */
    #[Groups(['ruine'])]
    private array $tradPorte = [];
    
    /**
     * @return array
     */
    public function getListeNiveauDifficulte(): array
    {
        return $this->listeNiveauDifficulte;
    }
    
    /**
     * @param array $listeNiveauDifficulte
     * @return MainGameRuineResponse
     */
    public function setListeNiveauDifficulte(array $listeNiveauDifficulte): MainGameRuineResponse
    {
        $this->listeNiveauDifficulte = $listeNiveauDifficulte;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getTradPorte(): array
    {
        return $this->tradPorte;
    }
    
    /**
     * @param array $tradPorte
     * @return MainGameRuineResponse
     */
    public function setTradPorte(array $tradPorte): MainGameRuineResponse
    {
        $this->tradPorte = $tradPorte;
        return $this;
    }
    
    /**
     * @return User
     */
    public function getUserCss(): User
    {
        return $this->userCss;
    }
    
    /**
     * @param User $userCss
     * @return MainGameRuineResponse
     */
    public function setUserCss(User $userCss): MainGameRuineResponse
    {
        $this->userCss = $userCss;
        return $this;
    }
    
    
}