<?php

namespace App\Structures\Dto\GH\RuineGame\Response;

class KillZombieResponse
{
    private int $zombieRestant;
    private int $zombieKill;
    private int $manaKill;
    
    /**
     * @return int
     */
    public function getManaKill(): int
    {
        return $this->manaKill;
    }
    
    /**
     * @param int $manaKill
     * @return KillZombieResponse
     */
    public function setManaKill(int $manaKill): KillZombieResponse
    {
        $this->manaKill = $manaKill;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getZombieKill(): int
    {
        return $this->zombieKill;
    }
    
    /**
     * @param int $zombieKill
     * @return KillZombieResponse
     */
    public function setZombieKill(int $zombieKill): KillZombieResponse
    {
        $this->zombieKill = $zombieKill;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getZombieRestant(): int
    {
        return $this->zombieRestant;
    }
    
    /**
     * @param int $zombieRestant
     * @return KillZombieResponse
     */
    public function setZombieRestant(int $zombieRestant): KillZombieResponse
    {
        $this->zombieRestant = $zombieRestant;
        return $this;
    }
    
}