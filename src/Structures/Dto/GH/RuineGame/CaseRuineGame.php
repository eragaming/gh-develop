<?php

namespace App\Structures\Dto\GH\RuineGame;

use App\Entity\RuineGameZone;
use Symfony\Component\Serializer\Attribute\Groups;

class CaseRuineGame
{
    private ?int $id = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private int $x;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private int $y;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private int $z;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $typeCase = RuineGameZone::CASE_VIDE;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?string $typePorte = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $nbrZombie = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?string $typeEscalier = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?bool $porteFouillee = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?bool $caseVue = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $zombieKill = null;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $positionPorte = 0;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $decoration = 0;
    
    #[Groups(['ruine', 'admin_ruine'])]
    private ?int $decorationVariation = 0;
    
    
    /**
     * @return bool|null
     */
    public function getCaseVue(): ?bool
    {
        return $this->caseVue;
    }
    
    /**
     * @param bool|null $caseVue
     * @return CaseRuineGame
     */
    public function setCaseVue(?bool $caseVue): CaseRuineGame
    {
        $this->caseVue = $caseVue;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getDecoration(): ?int
    {
        return $this->decoration;
    }
    
    /**
     * @param int|null $decoration
     * @return CaseRuineGame
     */
    public function setDecoration(?int $decoration): CaseRuineGame
    {
        $this->decoration = $decoration;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getDecorationVariation(): ?int
    {
        return $this->decorationVariation;
    }
    
    /**
     * @param int|null $decorationVariation
     * @return CaseRuineGame
     */
    public function setDecorationVariation(?int $decorationVariation): CaseRuineGame
    {
        $this->decorationVariation = $decorationVariation;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    
    /**
     * @param int|null $id
     * @return CaseRuineGame
     */
    public function setId(?int $id): CaseRuineGame
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getNbrZombie(): ?int
    {
        return $this->nbrZombie;
    }
    
    /**
     * @param int|null $nbrZombie
     * @return CaseRuineGame
     */
    public function setNbrZombie(?int $nbrZombie): CaseRuineGame
    {
        $this->nbrZombie = $nbrZombie;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function getPorteFouillee(): ?bool
    {
        return $this->porteFouillee;
    }
    
    /**
     * @param bool|null $porteFouillee
     * @return CaseRuineGame
     */
    public function setPorteFouillee(?bool $porteFouillee): CaseRuineGame
    {
        $this->porteFouillee = $porteFouillee;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPositionPorte(): ?int
    {
        return $this->positionPorte;
    }
    
    /**
     * @param int|null $positionPorte
     * @return CaseRuineGame
     */
    public function setPositionPorte(?int $positionPorte): CaseRuineGame
    {
        $this->positionPorte = $positionPorte;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getTypeCase(): ?int
    {
        return $this->typeCase;
    }
    
    /**
     * @param int|null $typeCase
     * @return CaseRuineGame
     */
    public function setTypeCase(?int $typeCase): CaseRuineGame
    {
        $this->typeCase = $typeCase;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTypeEscalier(): ?string
    {
        return $this->typeEscalier;
    }
    
    /**
     * @param string|null $typeEscalier
     * @return CaseRuineGame
     */
    public function setTypeEscalier(?string $typeEscalier): CaseRuineGame
    {
        $this->typeEscalier = $typeEscalier;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTypePorte(): ?string
    {
        return $this->typePorte;
    }
    
    /**
     * @param string|null $typePorte
     * @return CaseRuineGame
     */
    public function setTypePorte(?string $typePorte): CaseRuineGame
    {
        $this->typePorte = $typePorte;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }
    
    /**
     * @param int $x
     * @return CaseRuineGame
     */
    public function setX(int $x): CaseRuineGame
    {
        $this->x = $x;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }
    
    /**
     * @param int $y
     * @return CaseRuineGame
     */
    public function setY(int $y): CaseRuineGame
    {
        $this->y = $y;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getZ(): int
    {
        return $this->z;
    }
    
    /**
     * @param int $z
     * @return CaseRuineGame
     */
    public function setZ(int $z): CaseRuineGame
    {
        $this->z = $z;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getZombieKill(): ?int
    {
        return $this->zombieKill;
    }
    
    /**
     * @param int|null $zombieKill
     * @return CaseRuineGame
     */
    public function setZombieKill(?int $zombieKill): CaseRuineGame
    {
        $this->zombieKill = $zombieKill;
        return $this;
    }
}