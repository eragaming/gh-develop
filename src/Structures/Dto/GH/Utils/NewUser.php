<?php

namespace App\Structures\Dto\GH\Utils;

class NewUser
{
    private int     $idMh;
    private string  $pseudo;
    private ?string $avatar;
    private ?int    $mapId;
    private ?string $lang;
    
    /**
     * @return string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }
    
    /**
     * @param string $avatar
     * @return NewUser
     */
    public function setAvatar(?string $avatar): NewUser
    {
        $this->avatar = $avatar;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getIdMh(): int
    {
        return $this->idMh;
    }
    
    /**
     * @param int $idMh
     * @return NewUser
     */
    public function setIdMh(int $idMh): NewUser
    {
        $this->idMh = $idMh;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }
    
    /**
     * @param string $lang
     * @return NewUser
     */
    public function setLang(?string $lang): NewUser
    {
        $this->lang = $lang;
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMapId(): ?int
    {
        return $this->mapId;
    }
    
    /**
     * @param int|null $mapId
     * @return NewUser
     */
    public function setMapId(?int $mapId): NewUser
    {
        $this->mapId = $mapId;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPseudo(): string
    {
        return $this->pseudo;
    }
    
    /**
     * @param string $pseudo
     * @return NewUser
     */
    public function setPseudo(string $pseudo): NewUser
    {
        $this->pseudo = $pseudo;
        return $this;
    }
}