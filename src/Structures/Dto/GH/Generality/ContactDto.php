<?php

namespace App\Structures\Dto\GH\Generality;

class ContactDto
{
    private string $titre;
    private string $message;
    private array  $tag;
    /** @var ImageContactDto[] $urlImage */
    private array $urlImage     = [];
    private bool  $confidential = false;
    
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
    
    /**
     * @param string $message
     * @return ContactDto
     */
    public function setMessage(string $message): ContactDto
    {
        $this->message = $message;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getTag(): array
    {
        return $this->tag;
    }
    
    /**
     * @param array $tag
     * @return ContactDto
     */
    public function setTag(array $tag): ContactDto
    {
        $this->tag = $tag;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }
    
    /**
     * @param string $titre
     * @return ContactDto
     */
    public function setTitre(string $titre): ContactDto
    {
        $this->titre = $titre;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getUrlImage(): array
    {
        return $this->urlImage;
    }
    
    /**
     * @param array $urlImage
     * @return ContactDto
     */
    public function setUrlImage(array $urlImage): ContactDto
    {
        $this->urlImage = $urlImage;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isConfidential(): bool
    {
        return $this->confidential;
    }
    
    /**
     * @param bool $confidential
     * @return ContactDto
     */
    public function setConfidential(bool $confidential): ContactDto
    {
        $this->confidential = $confidential;
        return $this;
    }
}
