<?php

namespace App\Structures\Dto\GH\Generality;

class ImageContactDto
{
    private string $src;
    private string $legend;
    
    /**
     * @return string
     */
    public function getLegend(): string
    {
        return $this->legend;
    }
    
    /**
     * @param string $legend
     * @return ImageContactDto
     */
    public function setLegend(string $legend): ImageContactDto
    {
        $this->legend = $legend;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSrc(): string
    {
        return $this->src;
    }
    
    /**
     * @param string $src
     * @return ImageContactDto
     */
    public function setSrc(string $src): ImageContactDto
    {
        $this->src = $src;
        return $this;
    }
}