<?php

namespace App\Structures\Dto\GH\OptionPerso;

use App\Entity\DispoUserTypeExpedition;
use Symfony\Component\Serializer\Attribute\Groups;

class MajDispoUser
{
    #[Groups('option')]
    private int $userId;
    
    #[Groups('option')]
    private DispoUserTypeExpedition $dispoType;
    
    public function getDispoType(): DispoUserTypeExpedition
    {
        return $this->dispoType;
    }
    
    public function setDispoType(DispoUserTypeExpedition $dispoType): void
    {
        $this->dispoType = $dispoType;
    }
    
    public function getUserId(): int
    {
        return $this->userId;
    }
    
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }
}