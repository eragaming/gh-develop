<?php


namespace App\Structures\Dto;


class RetourAjax
{
    private int    $codeRetour = 0;
    private string $libRetour  = '';
    private string $zoneRetour = '';
    
    /**
     * @return int
     */
    public function getCodeRetour(): int
    {
        return $this->codeRetour;
    }
    
    /**
     * @return RetourAjax
     */
    public function setCodeRetour(int $codeRetour): RetourAjax
    {
        $this->codeRetour = $codeRetour;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLibRetour(): string
    {
        return $this->libRetour;
    }
    
    /**
     * @return RetourAjax
     */
    public function setLibRetour(string $libRetour): RetourAjax
    {
        $this->libRetour = $libRetour;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getZoneRetour(): string
    {
        return $this->zoneRetour;
    }
    
    /**
     * @return RetourAjax
     */
    public function setZoneRetour(string $zoneRetour): RetourAjax
    {
        $this->zoneRetour = $zoneRetour;
        
        return $this;
    }
}