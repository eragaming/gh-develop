<?php


namespace App\Structures\Utils;


use App\Entity\Ville;

class Position
{
    
    public const DIRECTION_VILLE      = 1;
    public const DIRECTION_NORD       = 2;
    public const DIRECTION_EST        = 3;
    public const DIRECTION_SUD        = 4;
    public const DIRECTION_OUEST      = 5;
    public const DIRECTION_NORD_EST   = 6;
    public const DIRECTION_SUD_EST    = 7;
    public const DIRECTION_SUD_OUEST  = 8;
    public const DIRECTION_NORD_OUEST = 9;
    private ?int $xRel = null;
    private ?int $yRel = null;
    
    public function __construct(private Ville $ville, private ?int $x, private ?int $y)
    {
    }
    
    public function calculDirection(): int
    {
        if ($this->getXRel() === null || $this->getYRel() === null) {
            $this->calculPositionRelatif();
        }
        $x = $this->getXRel();
        $y = $this->getYRel();
        
        
        if ($x === 0 && $y === 0) {
            return self::DIRECTION_VILLE;
        } elseif ($x != 0 && $y != 0 && (abs(abs($x) - abs($y)) < min(abs($x), abs($y)))) {
            if ($x < 0 && $y < 0) {
                return self::DIRECTION_SUD_OUEST;
            }
            if ($x < 0 && $y > 0) {
                return self::DIRECTION_NORD_OUEST;
            }
            if ($x > 0 && $y < 0) {
                return self::DIRECTION_SUD_EST;
            }
            if ($x > 0 && $y > 0) {
                return self::DIRECTION_NORD_EST;
            }
        } else {
            if (abs($x) > abs($y) && $x < 0) {
                return self::DIRECTION_OUEST;
            }
            if (abs($x) > abs($y) && $x > 0) {
                return self::DIRECTION_EST;
            }
            if (abs($x) < abs($y) && $y < 0) {
                return self::DIRECTION_SUD;
            }
            if (abs($x) < abs($y) && $y > 0) {
                return self::DIRECTION_NORD;
            }
        }
        
        return 0;
    }
    
    public function calculPositionRelatif()
    {
        
        if ($this->x === null || $this->y === null) {
            return;
        }
        
        // Calcul du x :
        
        $this->setXRel($this->x - $this->ville->getPosX());
        
        // Calcul du y :
        
        $this->setYRel($this->ville->getPosY() - $this->y);
        
    }
    
    /**
     * @return Ville
     */
    public function getVille(): Ville
    {
        return $this->ville;
    }
    
    /**
     * @return int|null
     */
    public function getX(): ?int
    {
        return $this->x;
    }
    
    /**
     * @return int|null
     */
    public function getXRel(): ?int
    {
        return $this->xRel;
    }
    
    /**
     * @param int|null $xRel
     * @return Position
     */
    public function setXRel(?int $xRel): Position
    {
        $this->xRel = $xRel;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getY(): ?int
    {
        return $this->y;
    }
    
    /**
     * @return int|null
     */
    public function getYRel(): ?int
    {
        return $this->yRel;
    }
    
    /**
     * @param int|null $yRel
     * @return Position
     */
    public function setYRel(?int $yRel): Position
    {
        $this->yRel = $yRel;
        
        return $this;
    }
    
    /**
     * @return Position
     */
    public function setVille(Ville $ville): Position
    {
        $this->ville = $ville;
        
        return $this;
    }
    
    /**
     * @param int|null $x
     * @return Position
     */
    public function setX(?int $x): Position
    {
        $this->x = $x;
        
        return $this;
    }
    
    /**
     * @param int|null $y
     * @return Position
     */
    public function setY(?int $y): Position
    {
        $this->y = $y;
        
        return $this;
    }
    
}