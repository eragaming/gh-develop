<?php

namespace App\Structures\Collection;

use App\Entity\ReparationChantier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class Reparations
{
    /**
     * @var Collection|ReparationChantier[]
     */
    private array|Collection $reparations;
    
    /**
     * @return ReparationChantier[]|Collection
     */
    public function getReparations(): array|Collection
    {
        return $this->reparations;
    }
    
    /**
     * @param ReparationChantier[] $reparations
     * @return Reparations
     */
    public function setReparations(array|Collection $reparations): Reparations
    {
        $this->reparations = $reparations;
        
        return $this;
    }
    
    /**
     * @return ReparationChantier[]|Collection
     * @throws Exception
     */
    public function triByOrder(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->reparations->getIterator();
        
        $iterrable->uasort(
            function (ReparationChantier $a, ReparationChantier $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if ($a->getChantier()->getOrderByGeneral() === $b->getChantier()->getOrderByGeneral()) {
                    $retour += 0;
                } else {
                    $retour += $a->getChantier()->getOrderByGeneral() < $b->getChantier()->getOrderByGeneral() ? -1 : 1;
                }
                
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
    
    /**
     * @return ReparationChantier[]|Collection
     * @throws Exception
     */
    public function triByRentabilite(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->reparations->getIterator();
        
        $iterrable->uasort(
            function (ReparationChantier $a, ReparationChantier $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $ratioChantierA = $a->getChantier()->getDef() / ($a->getChantier()->getPv() / 2);
                $ratioChantierB = $b->getChantier()->getDef() / ($b->getChantier()->getPv() / 2);
                
                $retour = 0;
                if ($ratioChantierA === $ratioChantierB) {
                    if ($a->getChantier()->getOrderByListing() === $b->getChantier()->getOrderByListing()) {
                        $retour += 0;
                    } else {
                        $retour += $a->getChantier()->getOrderByListing() < $b->getChantier()->getOrderByListing() ?
                            -1 : 1;
                    }
                } else {
                    $retour += $ratioChantierA > $ratioChantierB ? -2 : 2;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
}