<?php

namespace App\Structures\Collection;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class GestionJumps
{
    /**
     * @var Collection<User>
     */
    private Collection $gestionJumps;
    
    /**
     * @return Collection<User>
     */
    public function getGestionJumps(): Collection
    {
        return $this->gestionJumps;
    }
    
    /**
     * @param Collection<User> $gestionJumps
     * @return GestionJumps
     */
    public function setGestionJumps(Collection $gestionJumps): GestionJumps
    {
        $this->gestionJumps = $gestionJumps;
        
        return $this;
    }
    
    /**
     * @return Collection|User[]
     * @throws Exception
     */
    public function triByPseudo(): Collection|array
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->gestionJumps->getIterator();
        
        $iterrable->uasort(
            function (User $a, User $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if (ucfirst($a->getPseudo()) === ucfirst($b->getPseudo())) {
                    $retour += 0;
                } else {
                    $retour += (ucfirst($a->getPseudo()) < ucfirst($b->getPseudo())) ? -1 : 1;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
}