<?php

namespace App\Structures\Collection;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class Users
{
    /**
     * @var Collection|User[]
     */
    private array|Collection $users;
    
    /**
     * @return User[]|Collection
     */
    public function getUsers(): array|Collection
    {
        return $this->users;
    }
    
    /**
     * @param User[]|Collection $users
     * @return users
     */
    public function setUsers(array|Collection|ArrayCollection $users): self
    {
        if (!($users instanceof ArrayCollection)) {
            $this->users = new ArrayCollection($users);
        } else {
            $this->users = $users;
        }
        
        return $this;
    }
    
    
    /**
     * @return User[]|Collection
     * @throws Exception
     */
    public function triByPseudo(): array|Collection
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->users->getIterator();
        
        $iterrable->uasort(
            fn(User $a, User $b) => ucfirst($a->getPseudo()) < ucfirst($b->getPseudo()) ? -1 : 1,
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
}