<?php


namespace App\Structures\Collection;

use App\Entity\ChantierPrototype;
use App\Entity\PlansChantier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class PlansChantiers
{
    /**
     * @var Collection|PlansChantier[]
     */
    private array|Collection|null $plansChantiers = null;
    
    private array $plansChantierObtenu = [];
    
    public function addPlansChantiers(ChantierPrototype $chantierPrototype): void
    {
        if ($this->plansChantiers === null) {
            $this->plansChantiers = new ArrayCollection();
        }
        
        $this->plansChantiers->add((new PlansChantier())->setChantier($chantierPrototype));
        
    }
    
    public function countPlanByColor(int $lvlPlan): int
    {
        return $this->plansChantiers->filter(fn(PlansChantier $plansChantier) => $plansChantier->getChantier()
                                                                                               ->getPlan() === $lvlPlan)
                                    ->count();
    }
    
    public function existChantiers(int $idChantier): bool
    {
        return in_array($idChantier, $this->plansChantierObtenu);
    }
    
    public function fillPlansObtenu(): void
    {
        $this->plansChantierObtenu =
            $this->plansChantiers->map(fn(PlansChantier $plansChantier) => $plansChantier->getChantier()
                                                                                         ->getId())->toArray();
    }
    
    /**
     * @return PlansChantier[]|Collection|null
     */
    public function getPlansChantiers(): null|array|Collection
    {
        return $this->plansChantiers;
    }
    
    /**
     * @param PlansChantier[]|Collection $plansChantiers
     * @return PlansChantiers
     */
    public function setPlansChantiers(null|array|Collection $plansChantiers): PlansChantiers
    {
        $this->plansChantiers = $plansChantiers;
        
        $this->fillPlansObtenu();
        
        return $this;
    }
    
}