<?php

namespace App\Structures\Collection;

use App\Entity\Defense;
use App\Entity\Estimation;
use App\Entity\Journal;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;

class Statistiques
{
    /**
     * @var Collection<Defense>|null
     */
    private ?Collection $defenses;
    /**
     * @var Collection<Journal>|null
     */
    private ?Collection $journaux;
    /**
     * @var Collection<Estimation>|null
     */
    private ?Collection $estimation;
    
    private array       $estimMinMax;
    
    private array       $attaqueMoy;
    
    /**
     * @return array
     */
    public function getAttaqueMoy(): array
    {
        return $this->attaqueMoy;
    }
    
    /**
     * @return Statistiques
     */
    public function setAttaqueMoy(array $attaqueMoy): Statistiques
    {
        $this->attaqueMoy = $attaqueMoy;
        
        return $this;
    }
    
    /**
     * @return Collection
     */
    public function getDefenses(): Collection
    {
        return $this->defenses;
    }
    
    /**
     * @param Collection<Defense> $defenses
     * @return Statistiques
     */
    public function setDefenses(Collection $defenses): Statistiques
    {
        $this->defenses = $defenses;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getEstimMinMax(): array
    {
        return $this->estimMinMax;
    }
    
    /**
     * @param array $estimMinMax
     * @return Statistiques
     */
    public function setEstimMinMax(array $estimMinMax): Statistiques
    {
        $this->estimMinMax = $estimMinMax;
        
        return $this;
    }
    
    /**
     * @return Collection<Estimation>
     */
    public function getEstimation(): Collection
    {
        return $this->estimation;
    }
    
    /**
     * @param Collection<Estimation> $estimation
     * @return Statistiques
     */
    public function setEstimation(Collection $estimation): Statistiques
    {
        $this->estimation = $estimation;
        
        return $this;
    }
    
    /**
     * @return Collection<Journal>
     */
    public function getJournaux(): Collection
    {
        return $this->journaux;
    }
    
    /**
     * @param Collection<Journal> $journaux
     * @return Statistiques
     */
    public function setJournaux(Collection $journaux): Statistiques
    {
        $this->journaux = $journaux;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function recupAtkArray(): array
    {
        
        $arrayJournaux = [];
        $derJour       = 1;
        foreach ($this->journaux as $journal) {
            if ($journal->getDay() !== 1) {
                $arrayJournaux[$journal->getDay() - 1] = $journal;
            }
            
        }
        $arrayEstimations = [];
        foreach ($this->estimation as $estim) {
            $arrayEstimations[$estim->getDay()] = $estim;
            $derJour                            = $estim->getDay();
        }
        
        $derJour = 45;
        
        $arrayRetour = [];
        for ($i = 1; $i <= $derJour; $i++) {
            $estim                           = $arrayEstimations[$i] ?? null;
            $journal                         = $arrayJournaux[$i] ?? new Journal($i);
            $arrayRetour['day'][$i]          = $i;
            $arrayRetour['attaqueJour'][$i]  = $journal?->getZombie() ?? null;
            $arrayRetour['def'][$i]          = $journal?->getDef() ?? null;
            $arrayRetour['minEstim'][$i]     = $estim?->getMinJour() ?? null;
            $arrayRetour['maxEstim'][$i]     = $estim?->getMaxJour() ?? null;
            $arrayRetour['atk'][$i]          = array_map(fn($i) => $i['zombie'] ?? null, $this->attaqueMoy[$i] ?? []);
            $arrayRetour['atk_min_theo'][$i] = round(($i <= 3 ? 0.66 : 1) * (max(1, $i - 1) * 0.75 + 2.5) ** 3 * 1.1);
            $arrayRetour['atk_max_theo'][$i] = round(($i <= 3 ? ($i <= 1 ? 0.4 : 0.66) : 1) * ($i * 0.75 + 3.5) ** 3 * 1.1);
        }
        
        
        return $arrayRetour;
    }
    
    #[ArrayShape([
        'day'                    => "mixed",
        'total'                  => "mixed",
        'chantiersUpChantierAme' => "mixed",
        'objetDef'               => "mixed",
        'maisonCitizen'          => "mixed",
        'gardiens'               => "mixed",
        'veilleurs'              => "mixed",
        'morts'                  => "mixed",
        'tempos'                 => "mixed",
    ])] public function recupDefArray(): array
    {
        
        return [
            'day'                    => $this->defenses->map(
                fn(Defense $d) => $d->getDay(),
            )->toArray(),
            'total'                  => $this->defenses->map(
                fn(Defense $d) => $d->getTotal(),
            )->toArray(),
            'chantiersUpChantierAme' => $this->defenses->map(
                fn(Defense $d) => $d->getAmes() + $d->getBuildings() + $d->getUpgrades(),
            )->toArray(),
            'objetDef'               => $this->defenses->map(
                fn(Defense $d) => $d->getObjet() * $d->getBonusOd() / 10,
            )->toArray(),
            'maisonCitizen'          => $this->defenses->map(
                fn(Defense $d) => $d->getMaisonCitoyen(),
            )->toArray(),
            'gardiens'               => $this->defenses->map(
                fn(Defense $d) => $d->getGardiens(),
            )->toArray(),
            'veilleurs'              => $this->defenses->map(
                fn(Defense $d) => $d->getVeilleurs(),
            )->toArray(),
            'morts'                  => $this->defenses->map(
                fn(Defense $d) => $d->getMorts(),
            )->toArray(),
            'tempos'                 => $this->defenses->map(
                fn(Defense $d) => $d->getTempos(),
            )->toArray(),
        ];
        
    }
    
    
}