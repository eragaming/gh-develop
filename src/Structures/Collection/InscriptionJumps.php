<?php


namespace App\Structures\Collection;


use App\Entity\InscriptionJump;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;

class InscriptionJumps
{
    
    /**
     * @var Collection<InscriptionJump>
     */
    private readonly Collection $inscriptionJumps;
    
    /**
     * @return Collection<InscriptionJump>
     */
    public function getInscriptionJumps(): Collection
    {
        return $this->inscriptionJumps;
    }
    
    /**
     * @param Collection<InscriptionJump> $inscriptionJump
     * @return InscriptionJumps
     */
    public function setGestionJumps(Collection $inscriptionJump): InscriptionJumps
    {
        $this->inscriptionJump = $inscriptionJump;
        
        return $this;
    }
    
    /**
     * @return Collection|InscriptionJump[]
     * @throws Exception
     */
    public function triByPseudo(): Collection|array
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->getInscriptionJumps()->getIterator();
        
        $iterrable->uasort(
            function (InscriptionJump $a, InscriptionJump $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if (ucfirst($a->getUser()->getPseudo()) === ucfirst($b->getUser()->getPseudo())) {
                    $retour += 0;
                } else {
                    $retour += (ucfirst($a->getUser()->getPseudo()) < ucfirst($b->getUser()->getPseudo())) ? -1 : 1;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
    
    /**
     * @return Collection|InscriptionJump[]
     * @throws Exception
     */
    public function triByStatutEtPseudo(): Collection|array
    {
        /**
         * Transform to Iterrable
         */
        $iterrable = $this->getInscriptionJumps()->getIterator();
        
        $iterrable->uasort(
            function (InscriptionJump $a, InscriptionJump $b) {
                
                if ($a === $b) {
                    return 0;
                }
                
                $retour = 0;
                if (ucfirst($a->getUser()->getPseudo()) === ucfirst($b->getUser()->getPseudo())) {
                    $retour += 0;
                } else {
                    $retour += (ucfirst($a->getUser()->getPseudo()) < ucfirst($b->getUser()->getPseudo())) ? -1 : 1;
                }
                if ($a->getStatut()->getOrderInGestion() === $b->getStatut()->getOrderInGestion()) {
                    $retour += 0;
                } else {
                    $retour += ($a->getStatut()->getOrderInGestion() < $b->getStatut()->getOrderInGestion()) ? -2 : 2;
                }
                
                return $retour;
                
            },
        );
        
        return new ArrayCollection(iterator_to_array($iterrable));
    }
    
}