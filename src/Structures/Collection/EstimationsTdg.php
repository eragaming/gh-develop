<?php


namespace App\Structures\Collection;


use App\Entity\EstimationTdg;
use Doctrine\Common\Collections\ArrayCollection;

class EstimationsTdg
{
    
    private int $day;
    /**
     * @var EstimationTdg[]|ArrayCollection|null
     */
    private array|ArrayCollection|null $estimationsTdg = null;
    /**
     * @var EstimationTdg[]|ArrayCollection|null
     */
    private array|ArrayCollection|null $estimTdg = null;
    /**
     * @var EstimationTdg[]|ArrayCollection|null
     */
    private array|ArrayCollection|null $estimPlanif = null;
    
    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->day;
    }
    
    /**
     * @return EstimationsTdg
     */
    public function setDay(int $day): EstimationsTdg
    {
        $this->day = $day;
        
        return $this;
    }
    
    /**
     * @return array|ArrayCollection|null
     */
    public function getEstimPlanif(): array|ArrayCollection|null
    {
        if ($this->estimPlanif === null) {
            // On filtre les estimations pour ne garder que les estims du planif.
            $this->estimPlanif = $this->estimationsTdg?->filter(fn(EstimationTdg $e) => $e->getTypeEstim() ===
                                                                                        EstimationTdg::TYPE_ESTIM_PLANIF);
        }
        
        return $this->estimPlanif;
        
    }
    
    /**
     * @param EstimationTdg[]|ArrayCollection|null $estimPlanif
     * @return EstimationsTdg
     */
    public function setEstimPlanif(array|ArrayCollection|null $estimPlanif): EstimationsTdg
    {
        $this->estimPlanif = $estimPlanif;
        
        return $this;
    }
    
    /**
     * @return EstimationTdg[]|ArrayCollection|null
     */
    public function getEstimTdg(): array|ArrayCollection|null
    {
        if ($this->estimTdg === null) {
            // On filtre les estimations pour ne garder que les estims de la Tdg.
            $this->estimTdg = $this->estimationsTdg?->filter(fn(EstimationTdg $e) => $e->getTypeEstim() ===
                                                                                     EstimationTdg::TYPE_ESTIM_TDG);
        }
        
        return $this->estimTdg;
    }
    
    /**
     * @param EstimationTdg[]|ArrayCollection|null $estimTdg
     * @return EstimationsTdg
     */
    public function setEstimTdg(array|ArrayCollection|null $estimTdg): EstimationsTdg
    {
        $this->estimTdg = $estimTdg;
        
        return $this;
    }
    
    /**
     * @return EstimationTdg[]|ArrayCollection|null
     */
    public function getEstimationsTdg(): array|ArrayCollection|null
    {
        if ($this->estimationsTdg === null) {
            $this->estimationsTdg =
                new ArrayCollection(
                    array_merge($this->getEstimTdg()->toArray(),
                                ($this->getEstimPlanif() === null) ? [] : $this->getEstimPlanif()->toArray()),
                );
        }
        
        return $this->estimationsTdg;
    }
    
    /**
     * @param EstimationTdg[]|ArrayCollection|null $estimationsTdg
     * @return EstimationsTdg
     */
    public function setEstimationsTdg(array|ArrayCollection|null $estimationsTdg): EstimationsTdg
    {
        $this->estimationsTdg = $estimationsTdg;
        
        return $this;
    }
    
}