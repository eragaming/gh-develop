<?php


namespace App\Structures\jQueryBuilder;


use Doctrine\ORM\QueryBuilder;
use Exception;
use stdClass;

trait jQueryBuilderFunction
{
    
    protected $operators    = ['equal'            => ['accept_values' => true,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'not_equal'        => ['accept_values' => true,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'in'               => ['accept_values' => true,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'not_in'           => ['accept_values' => true,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'less'             => ['accept_values' => true, 'apply_to' => ['number', 'datetime']],
                               'less_or_equal'    => ['accept_values' => true, 'apply_to' => ['number', 'datetime']],
                               'greater'          => ['accept_values' => true, 'apply_to' => ['number', 'datetime']],
                               'greater_or_equal' => ['accept_values' => true, 'apply_to' => ['number', 'datetime']],
                               'between'          => ['accept_values' => true, 'apply_to' => ['number', 'datetime']],
                               'begins_with'      => ['accept_values' => true, 'apply_to' => ['string']],
                               'not_begins_with'  => ['accept_values' => true, 'apply_to' => ['string']],
                               'contains'         => ['accept_values' => true, 'apply_to' => ['string']],
                               'not_contains'     => ['accept_values' => true, 'apply_to' => ['string']],
                               'ends_with'        => ['accept_values' => true, 'apply_to' => ['string']],
                               'not_ends_with'    => ['accept_values' => true, 'apply_to' => ['string']],
                               'is_empty'         => ['accept_values' => false, 'apply_to' => ['string']],
                               'is_not_empty'     => ['accept_values' => false, 'apply_to' => ['string']],
                               'is_null'          => ['accept_values' => false,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'is_not_null'      => ['accept_values' => false,
                                                      'apply_to'      => ['string', 'number', 'datetime']],
                               'poss'             => ['accept_values' => false, 'apply_to' => ['boolean']],
                               'not-poss'         => ['accept_values' => false, 'apply_to' => ['boolean']],
                               'done'             => ['accept_values' => false, 'apply_to' => ['boolean']],
                               'not-done'         => ['accept_values' => false, 'apply_to' => ['boolean']]];
    protected $operator_sql = ['equal'           => ['operator' => '='], 'not_equal' => ['operator' => '!='],
                               'in'              => ['operator' => 'IN'], 'not_in' => ['operator' => 'NOT IN'],
                               'less'            => ['operator' => '<'], 'less_or_equal' => ['operator' => '<='],
                               'greater'         => ['operator' => '>'], 'greater_or_equal' => ['operator' => '>='],
                               'between'         => ['operator' => 'BETWEEN'],
                               'begins_with'     => ['operator' => 'LIKE', 'prepend' => '%'],
                               'not_begins_with' => ['operator' => 'NOT LIKE', 'prepend' => '%'],
                               'contains'        => ['operator' => 'LIKE', 'append' => '%', 'prepend' => '%'],
                               'not_contains'    => ['operator' => 'NOT LIKE', 'append' => '%', 'prepend' => '%'],
                               'ends_with'       => ['operator' => 'LIKE', 'append' => '%'],
                               'not_ends_with'   => ['operator' => 'NOT LIKE', 'append' => '%'],
                               'is_empty'        => ['operator' => '='], 'is_not_empty' => ['operator' => '!='],
                               'is_null'         => ['operator' => 'NULL'],
                               'is_not_null'     => ['operator' => 'NOT NULL'], 'poss' => ['operator' => '=1'],
                               'not-poss'        => ['operator' => '=0'], 'not-done' => ['operator' => '=0'],
                               'done'            => ['operator' => '=1']];
    protected $needs_array  = ['IN', 'NOT IN', 'BETWEEN'];
    
    /**
     * Append or prepend a string to the query if required.
     *
     * @param bool $requireArray value must be an array
     * @param mixed $value the value we are checking against
     * @return mixed $value
     */
    protected function appendOperatorIfRequired(bool $requireArray, mixed $value, mixed $sqlOperator): mixed
    {
        if (!$requireArray) {
            if (isset($sqlOperator['append'])) {
                $value = $sqlOperator['append'] . $value;
            }
            
            if (isset($sqlOperator['prepend'])) {
                $value = $value . $sqlOperator['prepend'];
            }
        }
        
        return $value;
    }
    
    /**
     * Ensure that a given field is an array if required.
     *
     * @param $value
     * @throws Exception
     * @see enforceArrayOrString
     */
    protected function checkFieldIsAnArray(bool $requireArray, $value, string $field)
    {
        if ($requireArray && !is_array($value)) {
            throw new Exception("Field ($field) should be an array, but it isn't.");
        }
    }
    
    abstract protected function checkRuleCorrect(stdClass $rule);
    
    /**
     * Convert an array with just one item to a string.
     *
     * In some instances, and array may be given when we want a string.
     *
     * @param string $field
     * @param $value
     * @return mixed
     * @throws Exception
     * @see enforceArrayOrString
     */
    protected function convertArrayToFlatValue($field, $value): mixed
    {
        if (count($value) !== 1) {
            throw new Exception("Field ($field) should not be an array, but it is.");
        }
        
        return $value[0];
    }
    
    /**
     * Enforce whether the value for a given field is the correct type
     *
     * @param bool $requireArray value must be an array
     * @param mixed $value the value we are checking against
     * @param string $field the field that we are enforcing
     * @return mixed value after enforcement
     * @throws Exception if value is not a correct type
     */
    protected function enforceArrayOrString(bool $requireArray, mixed $value, string $field): mixed
    {
        $this->checkFieldIsAnArray($requireArray, $value, $field);
        
        if (!$requireArray && is_array($value)) {
            return $this->convertArrayToFlatValue($field, $value);
        }
        
        return $value;
    }
    
    /**
     * makeQuery, for arrays.
     *
     * Some types of SQL Operators (ie, those that deal with lists/arrays) have specific requirements.
     * This function enforces those requirements.
     *
     * @param string $condition
     *
     * @return QueryBuilder
     * @throws Exception
     *
     */
    protected function makeQueryWhenArray(QueryBuilder $queryBuilder, stdClass $rule, array $sqlOperator, array $value,
                                                       $condition): QueryBuilder
    {
        if ($sqlOperator['operator'] == 'IN' || $sqlOperator['operator'] == 'NOT IN') {
            return $this->makeArrayQueryIn($queryBuilder, $rule, $sqlOperator['operator'], $value, $condition);
        } elseif ($sqlOperator['operator'] == 'BETWEEN') {
            return $this->makeArrayQueryBetween($queryBuilder, $rule, $value, $condition);
        }
        
        throw new Exception('makeQueryWhenArray could not return a value');
    }
    
    /**
     * Create a 'null' query when required.
     *
     *
     * @return QueryBuilder
     * @throws Exception
     */
    protected function makeQueryWhenNull(QueryBuilder $queryBuilder, stdClass $rule, array $sqlOperator,
                                         string       $condition): QueryBuilder
    {
        
        if ($sqlOperator['operator'] == 'NULL') {
            if ($condition === 'and') {
                return $queryBuilder->andWhere($queryBuilder->expr()->isNull($rule->field));
            } else {
                if ($condition === 'or') {
                    return $queryBuilder->orWhere($queryBuilder->expr()->isNull($rule->field));
                }
            }
        } elseif ($sqlOperator['operator'] == 'NOT NULL') {
            if ($condition === 'and') {
                return $queryBuilder->andWhere($queryBuilder->expr()->isNotNull($rule->field));
            } else {
                if ($condition === 'or') {
                    return $queryBuilder->orWhere($queryBuilder->expr()->isNotNull($rule->field));
                }
            }
        }
        
        throw new Exception('makeQueryWhenNull was called on an SQL operator that is not null');
    }
    
    /**
     * Determine if an operator is NULL/NOT NULL
     *
     * @param $operator
     *
     * @return bool
     */
    protected function operatorIsNull($operator): bool
    {
        return ($operator == 'NULL' || $operator == 'NOT NULL') ? true : false;
    }
    
    /**
     * Determine if an operator (LIKE/IN) requires an array.
     *
     * @param $operator
     *
     * @return bool
     */
    protected function operatorRequiresArray($operator): bool
    {
        return in_array($operator, $this->needs_array);
    }
    
    /**
     * Make sure that a condition is either 'or' or 'and'.
     *
     * @param $condition
     * @return string
     * @throws Exception
     */
    protected function validateCondition($condition): string
    {
        $condition = trim(strtolower((string)$condition));
        
        if ($condition !== 'and' && $condition !== 'or') {
            throw new Exception("Condition can only be one of: 'and', 'or'.");
        }
        
        return $condition;
    }
    
    /**
     * Decode the given JSON
     *
     * @param string incoming json
     * @return stdClass
     * @throws Exception
     */
    private function decodeJSON($json): stdClass
    {
        $query = json_decode((string)$json);
        if (json_last_error()) {
            throw new Exception('JSON parsing threw an error: ' . json_last_error_msg());
        }
        if (!is_object($query)) {
            throw new Exception('The query is not valid JSON');
        }
        
        return $query;
    }
    
    /**
     * Check that a given field is in the allowed list if set.
     *
     * @param $fields
     * @param $field
     * @throws Exception
     */
    private function ensureFieldIsAllowed($fields, $field)
    {
        if (is_array($fields) && !in_array($field, $fields)) {
            throw new Exception("Field ({$field}) does not exist in fields list");
        }
    }
    
    /**
     * get a value for a given rule.
     *
     * throws an exception if the rule is not correct.
     *
     * @return mixed
     * @throws Exception
     */
    private function getRuleValue(stdClass $rule): mixed
    {
        if (!$this->checkRuleCorrect($rule)) {
            throw new Exception('ERROR : checkRuleCorrect !');
        }
        
        return $rule->value;
    }
    
    /**
     * makeArrayQueryBetween, when the query is an IN or NOT IN...
     *
     * @return QueryBuilder
     * @throws Exception when more then two items given for the between
     * @see makeQueryWhenArray
     */
    private function makeArrayQueryBetween(QueryBuilder $queryBuilder, stdClass $rule, array $value,
                                           string       $condition): QueryBuilder
    {
        
        if (count($value) !== 2) {
            throw new Exception("{$rule->field} should be an array with only two items.");
        }
        
        if ($condition === 'and') {
            return $queryBuilder->andWhere($queryBuilder->expr()->between($rule->field, $value));
        } else {
            return $queryBuilder->orWhere($queryBuilder->expr()->between($rule->field, $value));
        }
        
    }
    
    /**
     * makeArrayQueryIn, when the query is an IN or NOT IN...
     *
     * @return QueryBuilder
     * @see makeQueryWhenArray
     */
    private function makeArrayQueryIn(QueryBuilder $queryBuilder, stdClass $rule, string $operator, array $value,
                                      string       $condition)
    {
        
        if ($operator == 'NOT IN') {
            if ($condition === 'and') {
                return $queryBuilder->andWhere($queryBuilder->expr()->notIn($rule->field, $value));
            } else {
                if ($condition === 'or') {
                    return $queryBuilder->orWhere($queryBuilder->expr()->notIn($rule->field, $value));
                }
            }
        }
        
        if ($condition === 'and') {
            return $queryBuilder->andWhere($queryBuilder->expr()->in($rule->field, $value));
        } else {
            if ($condition === 'or') {
                return $queryBuilder->orWhere($queryBuilder->expr()->in($rule->field, $value));
            }
        }
        
    }
    
}