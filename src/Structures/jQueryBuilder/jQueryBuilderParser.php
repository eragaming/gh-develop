<?php


namespace App\Structures\jQueryBuilder;

use Doctrine\ORM\QueryBuilder;
use Exception;
use stdClass;

class jQueryBuilderParser
{
    
    use jQueryBuilderFunction;
    
    protected ?array $fields = null;
    
    /**
     * QueryBuilderParser's parse function!
     *
     * Build a query based on JSON that has been passed into the function, onto the QueryBuilder passed into the function.
     *
     * @param array|null $fields
     * @return QueryBuilder
     * @throws Exception
     */
    public function jQueryToDoctrine(string $json, QueryBuilder $queryBuilder, array $fields = null): QueryBuilder
    {
        $this->fields = $fields;
        
        $query = $this->decodeJSON($json);
        if (!isset($query->rules) || !is_array($query->rules)) {
            return $queryBuilder;
        }
        if (count($query->rules) < 1) {
            return $queryBuilder;
        }
        
        return $this->loopThroughRules($query->rules, $queryBuilder, $query->condition);
    }
    
    /**
     * Check if a given rule is correct.
     *
     * Just before making a query for a rule, we want to make sure that the field, operator and value are set
     *
     *
     * @return bool true if values are correct.
     */
    protected function checkRuleCorrect(stdClass $rule): bool
    {
        // vérifie la présence des valeurs indispensables
        if (!isset($rule->id, $rule->field, $rule->type, $rule->input, $rule->operator, $rule->value)) {
            return false;
        }
        // vérifie l'existance de l'opérateur
        if (!isset($this->operators[$rule->operator])) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Convert an incomming rule from jQuery QueryBuilder to the Doctrine Querybuilder
     *
     * (This used to be part of makeQuery, where the name made sense, but I pulled it
     * out to reduce some duplicated code inside JoinSupportingQueryBuilder)
     *
     * @param mixed $value the value that needs to be queried in the database.
     * @param string $queryCondition and/or...
     * @return QueryBuilder
     * @throws Exception
     */
    protected function convertIncomingQBtoQuery(
        QueryBuilder $queryBuilder,
        stdClass     $rule,
        mixed        $value,
        string       $queryCondition = 'AND',
    ): QueryBuilder
    {
        // Convertissez l'opérateur (LIKE / NOT LIKE / GREATER THAN) qui nous est fourni par QueryBuilder
        // sur un que nous pouvons utiliser à l'intérieur de la requête SQL
        $sqlOperator = $this->operator_sql[$rule->operator];
        $operator    = $sqlOperator['operator'];
        $condition   = strtolower($queryCondition);
        
        if ($this->operatorRequiresArray($operator)) {
            return $this->makeQueryWhenArray($queryBuilder, $rule, $sqlOperator, $value, $condition);
        } else {
            if ($this->operatorIsNull($operator)) {
                return $this->makeQueryWhenNull($queryBuilder, $rule, $sqlOperator, $condition);
            }
        }
        
        $key = uniqid('qb');
        // inutile de sécurisé "field" ici car il a été filtré à l'entrée, idem pour "operator"
        if ($condition === 'and') {
            return $queryBuilder->andWhere($rule->field . ' ' . $sqlOperator['operator'] . ' :' . $key)
                                ->setParameter($key, $value);
        } else {
            return $queryBuilder->orWhere($rule->field . ' ' . $sqlOperator['operator'] . ' :' . $key)
                                ->setParameter($key, $value);
        }
        
    }
    
    /**
     * Create nested queries
     *
     * When a rule is actually a group of rules, we want to build a nested query with the specified condition (AND/OR)
     *
     * @param string|null $condition
     * @return QueryBuilder
     * @throws Exception
     */
    protected function createNestedQuery(QueryBuilder $queryBuilder, stdClass $rule,
                                         string       $condition = null): QueryBuilder
    {
        if ($condition === null) {
            $condition = $rule->condition;
        }
        
        $condition = $this->validateCondition($condition);
        
        foreach ($rule->rules as $loopRule) {
            $function = 'makeQuery';
            if ($this->isNested($loopRule)) {
                $function = 'createNestedQuery';
            }
            $queryBuilder = $this->{$function}($queryBuilder, $loopRule, $rule->condition);
        }
        
        return $queryBuilder;
    }
    
    /**
     * Ensure that the value for a field is correct.
     *
     * Append/Prepend values for SQL statements, etc.
     *
     * @param $operator
     * @param $value
     *
     * @return string|array
     * @throws Exception
     */
    protected function getCorrectValue($operator, stdClass $rule, $value): string|array
    {
        $field        = $rule->field;
        $sqlOperator  = $this->operator_sql[$rule->operator];
        $requireArray = $this->operatorRequiresArray($operator);
        
        $value = $this->enforceArrayOrString($requireArray, $value, $field);
        
        return $this->appendOperatorIfRequired($requireArray, $value, $sqlOperator);
    }
    
    /**
     * Ensure that the value is correct for the rule, try and set it if it's not.
     *
     *
     * @return mixed
     * @throws Exception
     *
     */
    protected function getValueForQueryFromRule(stdClass $rule): mixed
    {
        // assurez-vous que la plupart des champs communs de QueryBuilder ont été ajoutés
        $value = $this->getRuleValue($rule);
        
        // le "field" doit exister dans notre liste de "fields" (fournie à l'entrée)
        $this->ensureFieldIsAllowed($this->fields, $rule->field);
        
        // si l'opérateur SQL est défini pour ne pas avoir une valeur, assurez-vous que nous définissons la valeur à null
        if ($this->operators[$rule->operator]['accept_values'] === false) {
            return $this->operatorValueWhenNotAcceptingOne($rule);
        }
        
        // Convertissez l'opérateur (LIKE / NOT LIKE / GREATER THAN) qui nous est fourni par QueryBuilder
        // sur un que nous pouvons utiliser à l'intérieur de la requête SQL
        $sqlOperator = $this->operator_sql[$rule->operator];
        $operator    = $sqlOperator['operator'];
        
        // vérifie que la valeur est un tableau uniquement si elle doit être
        $value = $this->getCorrectValue($operator, $rule, $value);
        
        return $value;
    }
    
    /**
     * Déterminer si une règle particulière est en réalité un groupe d'autres règles.
     *
     * @param $rule
     *
     * @return bool
     */
    protected function isNested($rule): bool
    {
        if (isset($rule->rules) && is_array($rule->rules) && count($rule->rules) > 0) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Called by parse, loops through all the rules to find out if nested or not.
     *
     *
     * @return QueryBuilder
     * @throws Exception
     *
     */
    protected function loopThroughRules(array  $rules, QueryBuilder $queryBuilder,
                                        string $queryCondition = 'AND'): QueryBuilder
    {
        foreach ($rules as $rule) {
            
            // si makeQuery ne voit pas les champs corrects, il retournera $queryBuilder sans modifications
            $queryBuilder = $this->makeQuery($queryBuilder, $rule, $queryCondition);
            
            // si plusieurs groupes de rules
            if ($this->isNested($rule)) {
                $queryBuilder = $this->createNestedQuery($queryBuilder, $rule, $queryCondition);
            }
            
        }
        
        return $queryBuilder;
    }
    
    /**
     * makeQuery: The money maker!
     *
     * Take a particular rule and make build something that the QueryBuilder would be proud of.
     *
     * Make sure that all the correct fields are in the rule object then add the expression to
     * the query that was given by the user to the QueryBuilder.
     *
     * @param string $queryCondition and/or...
     *
     * @return QueryBuilder
     */
    protected function makeQuery(QueryBuilder $queryBuilder, stdClass $rule, string $queryCondition = 'AND')
    {
        
        // vérifie que la $rule est correcte
        try {
            $value = $this->getValueForQueryFromRule($rule);
        } catch (Exception) {
            return $queryBuilder;
        }
        
        return $this->convertIncomingQBtoQuery($queryBuilder, $rule, $value, $queryCondition);
    }
    
    /**
     * Give back the correct value when we don't accept one.
     *
     *
     * @return null|string
     */
    protected function operatorValueWhenNotAcceptingOne(stdClass $rule): ?string
    {
        if ($rule->operator == 'is_empty' || $rule->operator == 'is_not_empty') {
            return '';
        }
        
        return null;
    }
    
}