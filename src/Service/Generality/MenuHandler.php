<?php

namespace App\Service\Generality;

use App\Entity\Menu;
use App\Entity\MenuElement;
use App\Entity\MenuPrototype;
use App\Entity\User;
use App\Enum\MenuType;
use Doctrine\ORM\EntityManagerInterface;

class MenuHandler
{
    public function __construct(
        private readonly EntityManagerInterface $em,
    )
    {
    }
    
    public function filtrageMenuHabilitation(Menu $menu, User $user, bool $isCo, bool $inVille, bool $myVille): Menu
    {
        
        $menuFiltre = new Menu();
        
        foreach ($menu->getItems() as $element) {
            // Si l'élément du menu est un groupe, il faut balayer ses enfants, et regarder au niveau des menus enfants pour garder les éléments autorisés
            if ($element->getTypeMenu() === MenuType::TYPE_G) {
                $elementFiltre = new MenuElement();
                $elementFiltre->setId($element->getId())
                              ->setName($element->getName())
                              ->setOrdre($element->getOrdre())
                              ->setTypeMenu($element->getTypeMenu());
                
                foreach ($element->getItems() as $child) {
                    if ($this->isAuthorized($child, $user)
                        && $this->isConnectedConditionMet($child->getMenu(), $isCo)
                        && $this->checkVilleConditions($child->getMenu(), $inVille, $myVille)) {
                        $elementFiltre->addItem($child);
                    }
                }
                if ($elementFiltre->getItems()->count() > 0) {
                    $menuFiltre->addItem($elementFiltre);
                }
            } else {
                if ($this->isAuthorized($element, $user)) {
                    $menuFiltre->addItem($element);
                }
            }
        }
        
        return $menuFiltre;
    }
    
    // Fonction pour vérifier si la connexion est requise et si l'utilisateur est connecté
    
    public function findMenuElementInMenu(MenuElement|Menu $menu, ?int $elementId): ?MenuElement
    {
        foreach ($menu->getItems() as $element) {
            if ($element->getId() === $elementId) {
                return $element;
            }
            if ($element->getTypeMenu() === MenuType::TYPE_G) {
                foreach ($element->getItems() as $child) {
                    if ($child->getId() === $elementId) {
                        return $child;
                    }
                }
            }
        }
        return null;
    }

// Fonction pour vérifier si être en ville est requis
    
    /**
     * @param User $user
     * @return MenuPrototype[]
     */
    public function getPrototypeMenuListWithHabilitation(User $user): array
    {
        $allMenuPrototype = $this->em->getRepository(MenuPrototype::class)->findAll();
        
        $menuPrototypeList = [];
        
        foreach ($allMenuPrototype as $menuPrototype) {
            switch ($user->getRoles()[0]) {
                case 'ROLE_ADMIN':
                    if ($menuPrototype->isHabAdmin()) {
                        $menuPrototypeList[] = $menuPrototype;
                    }
                    break;
                case 'ROLE_BETA':
                    if ($menuPrototype->isHabBeta()) {
                        $menuPrototypeList[] = $menuPrototype;
                    }
                    break;
                case 'ROLE_USER':
                    if ($menuPrototype->isHabUser()) {
                        $menuPrototypeList[] = $menuPrototype;
                    }
                    break;
            }
        }
        
        return $menuPrototypeList;
        
    }
    
    public function isAuthorized(MenuElement $element, User $user): bool
    {
        if ($element->getMenu() !== null) {
            $menuPrototype = $element->getMenu();
            switch ($user->getRoles()[0]) {
                case 'ROLE_ADMIN':
                    return $menuPrototype->isHabAdmin();
                case 'ROLE_BETA':
                    return $menuPrototype->isHabBeta();
                case 'ROLE_USER':
                    return $menuPrototype->isHabUser();
            }
        }
        return true;
    }
    
    public function miseAJourMenu(Menu &$menu, Menu $newMenu): void
    {
        $this->removeOldElements($menu, $newMenu);
        
        foreach ($newMenu->getItems() as $newElement) {
            if ($newElement?->getId() === null) {
                $this->setMenuPrototype($newElement);
                $menu->addItem($newElement);
            } else {
                $existingElement = $this->findMenuElementInMenu($menu, $newElement?->getId());
                if ($existingElement) {
                    $this->updateMenuElement($existingElement, $newElement);
                } else {
                    $this->setMenuPrototype($newElement);
                    $menu->addItem($newElement);
                }
            }
        }
    }
    
    public function recuperationMenu(?User $user): Menu
    {
        if ($user !== null) {
            $menu = $this->em->getRepository(Menu::class)->findOneBy(['user' => $user]);
            if ($menu !== null) {
                return $menu;
            }
        }
        
        return $this->em->getRepository(Menu::class)->findOneBy(['user' => null]);
    }
    
    public function removeOldElements(Menu $menu, Menu $menuNew): void
    {
        foreach ($menu->getItems() as $existingElement) {
            if ($existingElement->getTypeMenu() === MenuType::TYPE_M) {
                if (!$this->findMenuElementInMenu($menuNew, $existingElement?->getId())) {
                    $menu->removeItem($existingElement);
                    $this->em->remove($existingElement);
                }
            } else if ($existingElement->getTypeMenu() === MenuType::TYPE_G) {
                foreach ($existingElement->getItems() as $existingChild) {
                    if (!$this->findMenuElementInMenu($menuNew, $existingChild?->getId())) {
                        $existingElement->removeItem($existingChild);
                        $this->em->remove($existingChild);
                    }
                }
                if (!$this->findMenuElementInMenu($menuNew, $existingElement?->getId())) {
                    $menu->removeItem($existingElement);
                    $this->em->remove($existingElement);
                }
            }
        }
    }
    
    // Gestion des conditions liées à la ville

    public function updateMenuElement(MenuElement &$existingElement, MenuElement $newElement): void
    {
        if ($newElement->getMenu() !== null) {
            $menuPrototype = $this->em->getRepository(MenuPrototype::class)->find($newElement->getMenu()?->getId());
            if ($menuPrototype !== null) {
                $existingElement->setMenu($menuPrototype);
            }
        }
        $existingElement->setName($newElement->getName())
                        ->setOrdre($newElement->getOrdre())
                        ->setTypeMenu($newElement->getTypeMenu());
        
        if ($existingElement->getTypeMenu() === MenuType::TYPE_G) {
            foreach ($existingElement->getItems() as $existingChild) {
                if (!$this->findMenuElementInMenu($newElement, $existingChild->getId())) {
                    $existingElement->removeItem($existingChild);
                    $this->em->remove($existingChild);
                }
            }
            foreach ($newElement->getItems() as $newChild) {
                $existingChild = $this->findMenuElementInMenu($existingElement, $newChild->getId());
                if ($existingChild) {
                    $this->updateMenuElement($existingChild, $newChild);
                } else {
                    $this->setMenuPrototype($newChild);
                    $existingElement->addItem($newChild);
                }
            }
        }
    }
    
    private function checkVilleConditions(MenuPrototype $menu, bool $inVille, bool $myVille): bool
    {
        // Si le menu requiert "être en ville" et "dans sa propre ville"
        if ($menu->isVille() && $menu->isMyVille()) {
            return $inVille || $myVille;
        }
        // Si le menu requiert uniquement d'être en ville (peu importe la ville)
        if ($menu->isVille()) {
            return $inVille;
        }
        // Si le menu requiert uniquement d'être dans sa propre ville
        if ($menu->isMyVille()) {
            return $myVille;
        }
        // Si aucune des conditions n'est requise, la condition est validée par défaut
        return true;
    }
    
    private function isConnectedConditionMet(MenuPrototype $menu, bool $isCo): bool
    {
        return !$menu->isConnected() || $isCo;
    }
    
    private function setMenuPrototype(MenuElement $element): void
    {
        if ($element->getTypeMenu() === MenuType::TYPE_M) {
            $menuPrototype = $this->em->getRepository(MenuPrototype::class)->find($element->getMenu()?->getId());
            if ($menuPrototype !== null) {
                $element->setMenu($menuPrototype);
            }
        } else if ($element->getTypeMenu() === MenuType::TYPE_G) {
            foreach ($element->getItems() as $child) {
                $menuPrototype = $this->em->getRepository(MenuPrototype::class)->find($child->getMenu()?->getId());
                if ($menuPrototype !== null) {
                    $child->setMenu($menuPrototype);
                }
            }
        }
    }
}