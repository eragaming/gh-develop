<?php

namespace App\Service\Generality;

use App\Entity\ChantierPrototype;
use App\Entity\UpChantierPrototype;
use App\Entity\ZoneMap;
use App\Service\GestHordesHandler;
use App\Service\Utils\LocalFormatterUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslateHandler
{
    
    public function __construct(private TranslatorInterface $translator, private LocalFormatterUtils $lfu,
                                private GestHordesHandler   $gh)
    {
    }
    
    
    public function direction_carte(): array
    {
        return [
            ['id' => ZoneMap::DIRECTION_NORD, 'nom' => $this->translator->trans("Nord", [], 'game')],
            ['id' => ZoneMap::DIRECTION_EST, 'nom' => $this->translator->trans("Est", [], 'game')],
            ['id' => ZoneMap::DIRECTION_SUD, 'nom' => $this->translator->trans("Sud", [], 'game')],
            ['id' => ZoneMap::DIRECTION_OUEST, 'nom' => $this->translator->trans("Ouest", [], 'game')],
            ['id' => ZoneMap::DIRECTION_NORD_EST, 'nom' => $this->translator->trans("Nord-Est", [], 'game')],
            ['id' => ZoneMap::DIRECTION_NORD_OUEST, 'nom' => $this->translator->trans("Nord-Ouest", [], 'game')],
            ['id' => ZoneMap::DIRECTION_SUD_EST, 'nom' => $this->translator->trans("Sud-Est", [], 'game')],
            ['id' => ZoneMap::DIRECTION_SUD_OUEST, 'nom' => $this->translator->trans("Sud-Ouest", [], 'game')],
        ];
    }
    
    public function direction_fao(): array
    {
        return [
            ['id' => ZoneMap::DIRECTION_NORD, 'nom' => $this->translator->trans("Nord", [], 'game')],
            ['id' => ZoneMap::DIRECTION_EST, 'nom' => $this->translator->trans("Est", [], 'game')],
            ['id' => ZoneMap::DIRECTION_SUD, 'nom' => $this->translator->trans("Sud", [], 'game')],
            ['id' => ZoneMap::DIRECTION_OUEST, 'nom' => $this->translator->trans("Ouest", [], 'game')],
        ];
    }
    
    
    /**
     * @param ChantierPrototype[] $listChantiers
     *
     * @return array
     */
    public function evo_chantier_list_translate(array $listChantiers): array
    {
        
        $listChantiersTranslate = [];
        
        foreach ($listChantiers as $chantier) {
            foreach ($chantier->getLevelUps() as $bonus) {
                foreach ($bonus->getBonusUps() as $up) {
                    $listChantiersTranslate[$bonus->getId()][$up->getId()] = $this->recupTranslateBonus($up);
                }
            }
        }
        
        return $listChantiersTranslate;
    }
    
    
    public function recupTranslateBonus(UpChantierPrototype $upChantierPrototype): string
    {
        
        return match ($upChantierPrototype->getTypeBonus()) {
            UpChantierPrototype::TYPE_ADD_EAU       => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span>{$this->gh->svgImg('small_water')}</span>",
            UpChantierPrototype::TYPE_ADD_DEF       => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span> {$this->gh->svgImg('small_def')}</span>",
            UpChantierPrototype::TYPE_ADD_DEF_PCT   => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}%</span> {$this->gh->svgImg('small_hield')}</span>",
            UpChantierPrototype::TYPE_ADD_PCT_REGEN => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}%</span> {$this->gh->svgImg('h_collec')}</span>",
            UpChantierPrototype::TYPE_RED_PA        => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>-{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}%</span> {$this->gh->svgImg('h_pa')}</span>",
            UpChantierPrototype::TYPE_MOD_COUT_PA   => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}{$this->translator->trans('pv',[], 'app')}/ </span>{$this->gh->svgImg('h_pa')}</span>",
            UpChantierPrototype::TYPE_BONUS_OD      => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span> {$this->gh->svgImg('item_shield')}<span> / </span>{$this->gh->svgImg('item_plate')}</span>",
            UpChantierPrototype::TYPE_ADD_AME       => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span> {$this->gh->svgImg('item_shield')}<span> / </span>{$this->gh->svgImg('item_soul_blue')}</span>",
            UpChantierPrototype::TYPE_SPE           => match ($upChantierPrototype->getValeurUp()) {
                UpChantierPrototype::STRA_CIT_LV0                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>1</span>{$this->gh->svgImg('item_bplan_u')}<span>/{$this->translator->trans("jour", [], 'app')}</span></span>",
                UpChantierPrototype::STRA_CIT_LV1                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>1</span> {$this->gh->svgImg('item_bplan_c')} <span> + 1 </span>{$this->gh->svgImg('item_bplan_u')}<span>/{$this->translator->trans("jour", [], 'app')}</span></span></span>",
                UpChantierPrototype::STRA_CIT_LV2                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>1</span>{$this->gh->svgImg('item_bplan_u')} <span>+ 1 </span>{$this->gh->svgImg('item_bplan_r')}<span>/{$this->translator->trans("jour", [], 'app')}</span></span></span>",
                UpChantierPrototype::STRA_CIT_LV3                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>2</span> {$this->gh->svgImg('item_bplan_r')}<span>/{$this->translator->trans("jour", [], 'app')}</span></span></span>",
                UpChantierPrototype::STRA_CIT_LV4                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>1</span> {$this->gh->svgImg('item_bplan_r')}<span> (+1</span> {$this->gh->svgImg('item_bplan_e')}<span>) /{$this->translator->trans("jour", [], 'app')}</span></span></span>",
                UpChantierPrototype::TDG_LVL0                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>0</span> {$this->gh->svgImg('small_watchmen')}</span> <span class='infoBulle' ><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::TDG_LVL1                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>3</span> {$this->gh->svgImg('small_watchmen')}</span> <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::TDG_LVL2                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>6</span> {$this->gh->svgImg('small_watchmen')}</span> <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::TDG_LVL3                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>10</span> {$this->gh->svgImg('small_watchmen')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::TDG_LVL4                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>10</span> {$this->gh->svgImg('small_watchmen')} <br/> {$this->translator->trans("+rapat à 1 {icone}", ['{icone}'=>'KM'], 'service')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::TDG_LVL5                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>10</span> {$this->gh->svgImg('small_watchmen')} <br/> {$this->translator->trans("+rapat à 2 {icone}", ['{icone}'=>'KM'], 'service')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::ECL_LVL0, UpChantierPrototype::ECL_LVL2, UpChantierPrototype::ECL_LVL1 => "<span class='d-flex gap-1 align-items-center justify-content-center'><span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::POT_LVL0                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'>{$this->gh->svgImg('item_vegetable')}, {$this->gh->svgImg('item_vegetable_tasty')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::POT_LVL1                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span>{$this->gh->svgImg('item_fungus')}, +{$this->gh->svgImg('item_ryebag')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::POT_LVL2                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span>{$this->gh->svgImg('item_boomfruit')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::POT_LVL3                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span>{$this->gh->svgImg('item_apple')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::POT_LVL4                                                               => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span>{$this->gh->svgImg('item_pumpkin_tasty')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::RONDE_LVL0                                                             => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>10</span>{$this->gh->svgImg('r_guard')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::RONDE_LVL1                                                             => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>20</span>{$this->gh->svgImg('r_guard')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::RONDE_LVL2                                                             => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>40</span>{$this->gh->svgImg('r_guard')}</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::RONDE_LVL3                                                             => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>40</span>{$this->gh->svgImg('r_guard')} + ???</span>  <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::PORTAIL_LV3, UpChantierPrototype::SOURCE_LVL3                          => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span> <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::SOURCE_LVL2,                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+</span> <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}(-2%)</span></span></span>",
                UpChantierPrototype::DECHARGE_LV0                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>1</span>{$this->gh->svgImg('item_shield')} <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::DECHARGE_LV1                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+5</span>{$this->gh->svgImg('item_shield')}{$this->gh->svgImg('item_gun')}/+3{$this->gh->svgImg('item_shield')}{$this->gh->svgImg('item_vegetable')} <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                UpChantierPrototype::DECHARGE_LV2                                                           => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>+6</span>{$this->gh->svgImg('item_shield')}{$this->gh->svgImg('item_pet_snake')}/+2{$this->gh->svgImg('item_shield')}{$this->gh->svgImg('item_door')} <span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$this->translator->trans($upChantierPrototype->getValeurUp(),[], 'chantiers')}</span></span></span>",
                default                                                                                     => "<span class='infoBulle'><i class=\"fa-solid fa-circle-info\"></i><span class='info'>{$upChantierPrototype->getValeurUp()}</span></span>",
            },
            UpChantierPrototype::TYPE_SUP_EAU       => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>-{$this->translator->trans($upChantierPrototype->getValeurUp(), [], 'chantiers')}</span> {$this->gh->svgImg('small_water')}</span>",
            UpChantierPrototype::TYPE_SUP_PAMP      => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>-{$this->translator->trans($upChantierPrototype->getValeurUp(), [], 'chantiers')}</span> {$this->gh->svgImg('item_boomfruit')}</span>",
            UpChantierPrototype::TYPE_SUP_PILE      => "<span class='d-flex gap-1 align-items-center justify-content-center'><span>-{$this->translator->trans($upChantierPrototype->getValeurUp(), [], 'chantiers')}</span> {$this->gh->svgImg('item_pile')}</span>",
            default                                 => "",
        };
        
    }
    
    public function z_date_format(): string
    {
        return $this->translator->trans("dd/MM", [], 'app');
    }
    
    public function z_date_format_time(): string
    {
        return $this->translator->trans("dd/MM/yyyy à H:mm", [], 'app');
    }
    
    
}