<?php

namespace App\Service\Generality;

use App\Entity\RuinesCases;
use App\Entity\ZoneMap;

class TranslateArrayHandler
{
    
    
    public function camping_translate(): array
    {
        return [
            'fermer'        => ['key' => "Fermer calcul camping", 'namespace' => 'ville'],
            'batiment'      => ['key' => "Bâtiment", 'namespace' => 'ville'],
            'distance'      => ['key' => "Distance de la ville:", 'namespace' => 'ville'],
            'aucun'         => ['key' => "Aucun", 'namespace' => 'ville'],
            'aucunBatiment' => ['key' => "Bâtiment : Aucun", 'namespace' => 'ville'],
            'phare'         => ['key' => "Phare construit", 'namespace' => 'ville'],
            'oui'           => ['key' => "Oui", 'namespace' => 'app'],
            'non'           => ['key' => "Non", 'namespace' => 'app'],
            'nuit'          => ['key' => "Nuit", 'namespace' => 'ville'],
            'amelioOD'      => ['key' => "Nombre d'amélioration + OD", 'namespace' => 'ville'],
            'zombie'        => ['key' => "Zombie", 'namespace' => 'ville'],
            'devaste'       => ['key' => "Dévasté", 'namespace' => 'ville'],
            'max_camp'      => ['key' => "Nombre de campeur maximum", 'namespace' => 'ville'],
            'ordre'         => ['key' => "Ordre", 'namespace' => 'ville'],
            'campeur'       => ['key' => "Campeur", 'namespace' => 'ville'],
            'nbCamping'     => ['key' => "Nombre Camping", 'namespace' => 'ville'],
            'campeur_pro'   => ['key' => "Campeur pro", 'namespace' => 'ville'],
            'tombe'         => ['key' => "Tombe", 'namespace' => 'ville'],
            'odc'           => ['key' => "Objet Camping", 'namespace' => 'ville'],
            'chance'        => ['key' => "Chance de réussite", 'namespace' => 'ville'],
            'addCamp'       => ['key' => "Ajouter un campeur", 'namespace' => 'ville'],
            'nbTas'         => ['key' => "Nombre de tas :", 'namespace' => 'ville'],
            'specifique'    => ['key' => "Spécifique", 'namespace' => 'ville'],
            'capuche'       => ['key' => "Capuche présente", 'namespace' => 'ville'],
            'help'          => [
                'campeur'   => ['key' => "Les campeurs en couleur rouge sont ceux qui n'ont pas le pouvoir héros campeur pro", 'namespace' => 'ville'],
                'amelio'    => ['key' => "Comptez 1,8 pour un OD, et 1 pour une amelioration, maximum 11,6", 'namespace' => 'ville'],
                'up'        => ['key' => "Remonter le campeur", 'namespace' => 'ville'],
                'down'      => ['key' => "Descendre le campeur", 'namespace' => 'ville'],
                'attention' => ['key' => "Décomptez 3 améliorations par jour", 'namespace' => 'ville'],
            ],
        ];
    }
    
    public function cartes_translate(): array
    {
        return [
            'carte'     => [
                'coordonnees'            => ['key' => 'Coordonnées', 'namespace' => 'ville'],
                'distance'               => ['key' => 'Distance', 'namespace' => 'ville'],
                'nonExplo'               => ['key' => "Une partie du désert que vous n'avez pas encore exploré.", 'namespace' => 'ville'],
                'mixExplo'               => ['key' => "Une partie du désert que vous n'avez pas encore tout à fait exploré", 'namespace' => 'ville'],
                'maj'                    => ['key' => "Mise à jour", 'namespace' => 'ville'],
                'zombie'                 => ['key' => "Contrôle de la case", 'namespace' => 'ville'],
                'balisage'               => ['key' => "Balisage de la case :", 'namespace' => 'ville'],
                'pdc'                    => ['key' => "Nombre pdc :", 'namespace' => 'ville'],
                'statut'                 => ['key' => "Statut de la case :", 'namespace' => 'ville'],
                'epuisee'                => ['key' => "Epuisée", 'namespace' => 'ville'],
                'nonEpuisee'             => ['key' => "Non-épuisée", 'namespace' => 'ville'],
                'citoyens'               => ['key' => "Citoyens présents :", 'namespace' => 'ville'],
                'citoyensPortes'         => ['key' => "Citoyens présents en ville ou aux portes :", 'namespace' => 'ville'],
                'objetsSol'              => ['key' => "Objets au sol :", 'namespace' => 'ville'],
                'objetSol'               => ['key' => "Objet au sol :", 'namespace' => 'ville'],
                'batCamp'                => ['key' => "Bâtiment campé :", 'namespace' => 'ville'],
                'batVide'                => ['key' => "Bâtiment vide :", 'namespace' => 'ville'],
                'batHypo'                => ['key' => "Bâtiment hypothétique :", 'namespace' => 'ville'],
                'oui'                    => ['key' => "Oui", 'namespace' => 'app'],
                'non'                    => ['key' => "Non", 'namespace' => 'app'],
                'phraseMajPasMaj'        => ['key' => "Pas de dernière mise à jour connue", 'namespace' => 'ville'],
                'phraseMajAuj'           => ['key' => "Zone mise à jour aujourd'hui par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'phraseMajHier'          => ['key' => "Zone mise à jour hier par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'phraseMajAutre'         => ['key' => "Zone mise à jour il y a {jour} jours par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'phraseMajMarqueurAuj'   => ['key' => "marqueur de la zone mise à jour aujourd'hui par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'phraseMajMarqueurHier'  => ['key' => "marqueur de la zone mise à jour hier par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'phraseMajMarqueurAutre' => ['key' => "marqueur de la zone mise à jour il y a {jour} jours par {pseudo} à {heureMaj}", 'namespace' => 'ville'],
                'camping'                => ['key' => "Calculer camping", 'namespace' => 'ville'],
                'consigne'               => ['key' => "Consigne(s)", 'namespace' => 'ville'],
                'maj_ok'                 => ['key' => "Mise à jour ok", 'namespace' => 'ville'],
                'et'                     => ['key' => 'et', 'namespace' => 'app'],
                'mais'                   => ['key' => "mais", 'namespace' => 'app'],
                'btn_ruine'              => ['key' => "Plan de la ruine", 'namespace' => 'ville'],
            ],
            'popUp'     => [
                'titreErreur'  => ['key' => 'Erreur !', 'namespace' => 'app'],
                'boutonErreur' => ['key' => 'Fermer', 'namespace' => 'app'],
                'texteConseil' => ['key' => 'Veuillez réactualiser la page ou contacter les administrateurs si le problème persiste.', 'namespace' => 'app'],
                'titreSuccess' => ['key' => 'Succès !', 'namespace' => 'app'],
            ],
            'camping'   => $this->camping_translate(),
            'menuCarte' => [
                'onglet'     => [
                    'ville'      => ['key' => "Info Ville", 'namespace' => 'ville'],
                    'citoyen'    => ['key' => "Citoyens", 'namespace' => 'ville'],
                    'objet'      => ['key' => "Objets", 'namespace' => 'ville'],
                    'batiment'   => ['key' => "Bâtiments", 'namespace' => 'ville'],
                    'expe'       => ['key' => "Expéditions", 'namespace' => 'ville'],
                    'outilsExpe' => ['key' => "Gestion Expéditions", 'namespace' => 'ville'],
                    'param'      => ['key' => "Paramètres", 'namespace' => 'ville'],
                    'outils'     => ['key' => "Outils", 'namespace' => 'ville'],
                ],
                'ongletDiff' => [
                    'upAndrefresh' => ['key' => "MAJ et rafraichir", 'namespace' => 'ville'],
                    'refresh'      => ['key' => "Rafraichir la carte", 'namespace' => 'ville'],
                    'unpin'        => ['key' => "Détacher et déplacer le menu", 'namespace' => 'ville'],
                    'pin'          => ['key' => "Rattacher le menu", 'namespace' => 'ville'],
                    'isOnUpdate'   => ['key' => "Mise à jour en cours...", 'namespace' => 'ville'],
                    'isOnRefresh'  => ['key' => "Rafraichissement en cours...", 'namespace' => 'ville'],
                ],
                'ville'      => [
                    'generale' => [
                        'recapVille'   => ['key' => "Récapitulatif ville", 'namespace' => 'ville'],
                        'nom_ville'    => ['key' => "Nom de la ville :", 'namespace' => 'ville'],
                        'jour'         => ['key' => "Jour :", 'namespace' => 'ville'],
                        'type_ville'   => ['key' => "Type de ville :", 'namespace' => 'ville'],
                        'statut_ville' => ['key' => "Statut de la ville :", 'namespace' => 'ville'],
                        'portes_ville' => ['key' => "Portes de la ville :", 'namespace' => 'ville'],
                        'ouvert'       => ['key' => "Ouvertes", 'namespace' => 'game'],
                        'fermes'       => ['key' => "Fermées", 'namespace' => 'game'],
                        'puits'        => ['key' => "Le puits a encore :", 'namespace' => 'ville'],
                        'banque'       => ['key' => "La banque a :", 'namespace' => 'ville'],
                        'nourriture'   => ['key' => "Nourriture en banque :", 'namespace' => 'ville'],
                        'alcool'       => ['key' => "Alcool en banque :", 'namespace' => 'ville'],
                        'drogue'       => ['key' => "Drogue en banque :", 'namespace' => 'ville'],
                    ],
                    'defense'  => [
                        'recapVille' => ['key' => "Récapitulatif défense", 'namespace' => 'ville'],
                        'muraille'   => ['key' => "Muraille :", 'namespace' => 'ville'],
                        'chantiers'  => ['key' => "Chantiers :", 'namespace' => 'ville'],
                        'objetDef'   => ['key' => "Objets de défense :", 'namespace' => 'ville'],
                        'objet'      => ['key' => "objet", 'namespace' => 'ville'],
                        'objets'     => ['key' => "objets", 'namespace' => 'ville'],
                        'maisons'    => ['key' => "Maisons des citoyens :", 'namespace' => 'ville'],
                        'gardiens'   => ['key' => "Gardiens :", 'namespace' => 'ville'],
                        'veilleurs'  => ['key' => "Veilleurs :", 'namespace' => 'ville'],
                        'tempos'     => ['key' => "Tempos :", 'namespace' => 'ville'],
                        'ame'        => ['key' => "Âmes :", 'namespace' => 'ville'],
                        'morts'      => ['key' => "Morts :", 'namespace' => 'ville'],
                        'bonusSd'    => ['key' => "Bonus SD (+{bonusSd}%) :", 'namespace' => 'ville'],
                        'total'      => ['key' => "Total", 'namespace' => 'ville'],
                    ],
                    'evo'      => [
                        'recapVille' => ['key' => "Récapitulatif des évolutions chantiers (lvl)", 'namespace' => 'ville'],
                    ],
                    'estim'    => [
                        'recapVille' => ['key' => "Récapitulatif des estimations", 'namespace' => 'ville'],
                        'tour'       => ['key' => "Tour de guet :", 'namespace' => 'ville'],
                        'min'        => ['key' => "min :", 'namespace' => 'ville'],
                        'max'        => ['key' => "max :", 'namespace' => 'ville'],
                        'planif'     => ['key' => "Planificateur :", 'namespace' => 'ville'],
                    ],
                    'stats'    => [
                        'zone'       => ['key' => "Zone", 'namespace' => 'ville'],
                        'entiere'    => ['key' => "Toute la carte", 'namespace' => 'ville'],
                        'stats'      => ['key' => "Statistiques cases", 'namespace' => 'ville'],
                        'epuisement' => ['key' => "Epuisées", 'namespace' => 'ville'],
                        'decouverte' => ['key' => "Découvertes", 'namespace' => 'ville'],
                    ],
                ],
                'citoyen'    => [
                    'selectAll'   => ['key' => "Sélectionner tous les citoyens", 'namespace' => 'ville'],
                    'deselectAll' => ['key' => "Désélectionner tous les citoyens", 'namespace' => 'ville'],
                    'inverse'     => ['key' => "Inverser la sélection", 'namespace' => 'ville'],
                    'dehors'      => ['key' => "Dehors", 'namespace' => 'ville'],
                    'ville'       => ['key' => "En ville ou aux portes", 'namespace' => 'ville'],
                    'portes'      => ['key' => "Aux portes", 'namespace' => 'ville'],
                ],
                'objet'      => [
                    'rechercheObjet' => ['key' => "Rechercher un objet par son nom :", 'namespace' => 'ville'],
                    'tri'            => ['key' => "Tri par ordre :", 'namespace' => 'ville'],
                    'decroissant'    => ['key' => "Décroissant", 'namespace' => 'ville'],
                    'alphabetique'   => ['key' => "Alphabétique", 'namespace' => 'ville'],
                    'selectAll'      => ['key' => "Sélectionner tous les objets", 'namespace' => 'ville'],
                    'deselectAll'    => ['key' => "Désélectionner tous les objets", 'namespace' => 'ville'],
                    'inverse'        => ['key' => "Inverser la sélection", 'namespace' => 'ville'],
                    'casse'          => ['key' => "dont cassé", 'namespace' => 'ville'],
                    'casses'         => ['key' => "dont cassés", 'namespace' => 'ville'],
                    'filtre'         => [
                        'titre'       => ['key' => "Filtrer les objets", 'namespace' => 'ville'],
                        'action'      => ['key' => "Activer le filtre", 'namespace' => 'ville'],
                        'km'          => ['key' => "Entre les km", 'namespace' => 'ville'],
                        'zone'        => ['key' => "Entre les zones", 'namespace' => 'ville'],
                        'et'          => ['key' => "et", 'namespace' => 'ville'],
                        'ou'          => ['key' => "ou", 'namespace' => 'ville'],
                        'filtrer'     => ['key' => "Filtrer", 'namespace' => 'ville'],
                        'filtre_type' => [
                            'label' => ['key' => "Type de filtre", 'namespace' => 'ville'],
                            'km'    => ['key' => "Km", 'namespace' => 'ville'],
                            'zone'  => ['key' => "Zone", 'namespace' => 'ville'],
                        ],
                    ],
                ],
                'batiment'   => [
                    'selectAll'    => ['key' => "Sélectionner tous les Bâtiments", 'namespace' => 'ville'],
                    'selectCamp'   => ['key' => "Sélectionner tous les bâts restant à camper", 'namespace' => 'ville'],
                    'selectInhab'  => ['key' => "Sélectionner Bâtiments à plan inhab", 'namespace' => 'ville'],
                    'selectRare'   => ['key' => "Sélectionner Bâtiments à plan rare", 'namespace' => 'ville'],
                    'deselect'     => ['key' => "Désélectionner tous les Bâtiments", 'namespace' => 'ville'],
                    'inverse'      => ['key' => "Inverser la sélection", 'namespace' => 'ville'],
                    'batiments'    => ['key' => "Bâtiments", 'namespace' => 'ville'],
                    'plansJaune'   => ['key' => "Plans Jaunes", 'namespace' => 'ville'],
                    'plansBleu'    => ['key' => "Plans Bleus", 'namespace' => 'ville'],
                    'ruine'        => ['key' => "Ruine", 'namespace' => 'ville'],
                    'campe'        => ['key' => "campé", 'namespace' => 'ville'],
                    'campes'       => ['key' => "campés", 'namespace' => 'ville'],
                    'restants'     => ['key' => "restants", 'namespace' => 'ville'],
                    'restant'      => ['key' => "restant", 'namespace' => 'ville'],
                    'batHypothese' => ['key' => "Bâtiment hypothèse : {batHypo}", 'namespace' => 'ville'],
                    'camped'       => ['key' => "Bâtiment déjà campé", 'namespace' => 'ville'],
                    'notCamped'    => ['key' => "Bâtiment pas encore campé", 'namespace' => 'ville'],
                    'vide'         => ['key' => "Bâtiment vide", 'namespace' => 'ville'],
                    'notVide'      => ['key' => "Bâtiment non vide", 'namespace' => 'ville'],
                    'chargement'   => ['key' => "Chargement", 'namespace' => 'app'],
                ],
                'expe'       => [
                    'seeTrace'         => ['key' => "Voir les tracés d'expéditions de :", 'namespace' => 'ville'],
                    'mh'               => ['key' => "MyHordes", 'namespace' => 'ville'],
                    'biblio'           => ['key' => "Bibliothèque", 'namespace' => 'ville'],
                    'trace_outils'     => ['key' => "Outils expédition", 'namespace' => 'ville'],
                    'selectAll'        => ['key' => "Sélectionner toutes les expeditions", 'namespace' => 'ville'],
                    'deselectAll'      => ['key' => "Désélectionner toutes les expeditions", 'namespace' => 'ville'],
                    'inverse'          => ['key' => "Inverser la sélection", 'namespace' => 'ville'],
                    'couleurExpe'      => ['key' => "Nom expédition - Couleur", 'namespace' => 'ville'],
                    'jourExpe'         => ['key' => "J", 'namespace' => 'ville'],
                    'createur'         => ['key' => "Créateur", 'namespace' => 'ville'],
                    'modificateur'     => ['key' => "Modificateur", 'namespace' => 'ville'],
                    'collaborative'    => ['key' => "Collaborative", 'namespace' => 'ville'],
                    'personnel'        => ['key' => "Privée", 'namespace' => 'ville'],
                    'action'           => ['key' => "Action", 'namespace' => 'ville'],
                    'mode_expe'        => ['key' => "Activer le mode expédition :", 'namespace' => 'ville'],
                    'nom_expe'         => ['key' => "Nom de l'expédition :", 'namespace' => 'ville'],
                    'couleur_expe'     => ['key' => "Couleur de l'expédition :", 'namespace' => 'ville'],
                    'mode_collab'      => ['key' => "Tracé en mode collaboratif", 'namespace' => 'ville'],
                    'mode_collab_help' => ['key' => "Le tracé pourra alors être modifié par n'importe qui de la ville", 'namespace' => 'ville'],
                    'save'             => ['key' => "Enregistrer", 'namespace' => 'ville'],
                    'cancel'           => ['key' => "Annuler", 'namespace' => 'ville'],
                    'canPoint'         => ['key' => "Annuler le dernier point", 'namespace' => 'ville'],
                    'mode_personnel'   => ['key' => "Tracé en mode privé", 'namespace' => 'ville'],
                    'jour_expe'        => ['key' => "Jour de l'expédition", 'namespace' => 'ville'],
                    'help_personnel'   => ['key' => "Si vous cochez cette case, cette expédition ne sera visible que par vous", 'namespace' => 'ville'],
                    'confirmation'     => [
                        'message'      => ['key' => "Êtes-vous sûr de supprimer définitivement cette expédition dont vous n'êtes pas le créateur ?", 'namespace' => 'ville'],
                        'titre'        => ['key' => "Confirmation de suppression", 'namespace' => 'ville'],
                        'annuler'      => ['key' => "Annuler", 'namespace' => 'ville'],
                        'confirmation' => ['key' => "Confirmer", 'namespace' => 'ville'],
                    ],
                    'help_masque'      => ['key' => "Permet de masquer une expédition en la rendant insélectionnable pour les boutons de selection rapide, l'action est réversible en recliquant sur l'oeil barré.", 'namespace' => 'ville'],
                    'trace_biblio'     => ['key' => "Tracé en bibliothèque", 'namespace' => 'ville'],
                    'help_biblio'      => ['key' => "Permet de sauvegarder le tracé dans la bibliothèque et de pouvoir être réutilisable à l'infini.", 'namespace' => 'ville'],
                ],
                'outilsExpe' => $this->outils_expedition(),
                'param'      => [
                    'afficher'           => ['key' => "Afficher :", 'namespace' => 'ville'],
                    'danger'             => ['key' => "Danger", 'namespace' => 'ville'],
                    'distance'           => ['key' => "Distance", 'namespace' => 'ville'],
                    'distance_pa'        => ['key' => "Distance en PA", 'namespace' => 'ville'],
                    'scrut'              => ['key' => "Scrutateur", 'namespace' => 'ville'],
                    'zombie'             => ['key' => "Zombie", 'namespace' => 'ville'],
                    'epuise'             => ['key' => "Epuisé", 'namespace' => 'ville'],
                    'objetSol'           => ['key' => "Objet sol", 'namespace' => 'ville'],
                    'citoyensVille'      => ['key' => "Citoyens", 'namespace' => 'ville'],
                    'indicVisit'         => ['key' => "Indicateur de visite", 'namespace' => 'ville'],
                    'carteAlter'         => ['key' => "Carte Alternative", 'namespace' => 'ville'],
                    'carteScrut'         => ['key' => "Carte Scrutateur", 'namespace' => 'ville'],
                    'afficher_km'        => ['key' => "Afficher les kms :", 'namespace' => 'ville'],
                    'afficher_pa'        => ['key' => "Afficher les pa :", 'namespace' => 'ville'],
                    'afficher_zone'      => ['key' => "Afficher les zones :", 'namespace' => 'ville'],
                    'afficher_decharge'  => ['key' => "Afficher les décharges :", 'namespace' => 'ville'],
                    'zonage'             => ['key' => "Zone PA", 'namespace' => 'ville'],
                    'estimZombie'        => ['key' => "Estimation Zombie", 'namespace' => 'ville'],
                    'balisage'           => ['key' => "Balisage", 'namespace' => 'ville'],
                    'afficher_prevision' => ['key' => "Estimation de zombie pour le jour", 'namespace' => 'ville'],
                ],
                'outils'     => [
                    'bouton_scrut'       => ['key' => "Ajouter marqueurs de régénération selon la direction du scrutateur", 'namespace' => 'ville'],
                    'bouton_scrut_trait' => ['key' => "Traitement en cours...", 'namespace' => 'ville'],
                ],
            ],
        ];
    }
    
    public function changelog_translate(): array
    {
        return [
            'contenu'    => ['key' => 'Contenus :', 'namespace' => 'version'],
            'new'        => ['key' => 'Nouveau', 'namespace' => 'version'],
            'fix'        => ['key' => 'Correction', 'namespace' => 'version'],
            'other'      => ['key' => 'Autre', 'namespace' => 'version'],
            'dateFormat' => $this->z_date_format_time(),
        ];
    }
    
    public function chantier_list_translate(): array
    {
        return [
            'liste'          => ['key' => "Liste des chantiers de la ville", 'namespace' => 'hotel'],
            'construit'      => ['key' => "Construit", 'namespace' => 'outils'],
            'construction'   => ['key' => "En construction", 'namespace' => 'outils'],
            'reparer'        => ['key' => "Construit mais à réparer", 'namespace' => 'outils'],
            'obtenu'         => ['key' => "Chantier disponible ou Plan obtenu", 'namespace' => 'outils'],
            'manquant'       => ['key' => "Plan manquant", 'namespace' => 'outils'],
            'couleur'        => ['key' => "Couleur du plan de chantier", 'namespace' => 'outils'],
            'temp'           => ['key' => "Chantier temporaire", 'namespace' => 'outils'],
            'repa'           => ['key' => "Chantier réparable", 'namespace' => 'outils'],
            'pa'             => ['key' => "Coût en PA", 'namespace' => 'outils'],
            'pa_restant'     => ['key' => "PA restant", 'namespace' => 'outils'],
            'pv'             => ['key' => "Point de vie max du chantier", 'namespace' => 'outils'],
            'pv_restant'     => ['key' => "PV restant", 'namespace' => 'outils'],
            'def'            => ['key' => "Apport en défense du chantier", 'namespace' => 'outils'],
            'eau'            => ['key' => "Apport en eau du chantier", 'namespace' => 'outils'],
            'nom'            => ['key' => "Nom", 'namespace' => 'outils'],
            'ressources'     => ['key' => "Ressources", 'namespace' => 'outils'],
            'aide'           => ['key' => "Cliquez sur la légende pour afficher ou masquer les chantiers correspondants", 'namespace' => 'outils'],
            'legende'        => ['key' => "Légende", 'namespace' => 'outils'],
            'filtre_tri'     => ['key' => "Filtre et tri", 'namespace' => 'outils'],
            'recherche_item' => ['key' => "Rechercher un objet", 'namespace' => 'outils'],
            'tri'            => ['key' => "Tri des chantiers par", 'namespace' => 'outils'],
            'tri_nom'        => ['key' => "nom", 'namespace' => 'outils'],
            'tri_pa'         => ['key' => "PA", 'namespace' => 'outils'],
            'tri_pv'         => ['key' => "PV", 'namespace' => 'outils'],
            'tri_def'        => ['key' => "défense", 'namespace' => 'outils'],
            'tri_eau'        => ['key' => "quantité d'eau", 'namespace' => 'outils'],
            'tri_arbre'      => ['key' => "défaut", 'namespace' => 'outils'],
            'asc'            => ['key' => "Croissant", 'namespace' => 'outils'],
            'az'             => ['key' => "A à Z", 'namespace' => 'outils'],
            'desc'           => ['key' => "Décroissant", 'namespace' => 'outils'],
            'za'             => ['key' => "Z à A", 'namespace' => 'outils'],
            'cumul'          => [
                'titre'     => ['key' => "Cumul des ressources", 'namespace' => 'outils'],
                'select'    => ['key' => "Cumul des ressources pour", 'namespace' => 'outils'],
                'place'     => ['key' => "choisir le type de cumul", 'namespace' => 'outils'],
                'tous'      => ['key' => "tous les chantiers", 'namespace' => 'outils'],
                'choix'     => ['key' => "uniquement les chantiers choisis", 'namespace' => 'outils'],
                'def'       => ['key' => "uniquement les chantiers de défense", 'namespace' => 'outils'],
                'conf'      => ['key' => "uniquement les chantiers de confort", 'namespace' => 'outils'],
                'temp'      => ['key' => "uniquement les chantiers temporaires", 'namespace' => 'outils'],
                'eau'       => ['key' => "uniquement les chantiers donnant de l'eau", 'namespace' => 'outils'],
                'const'     => ['key' => "uniquement les chantiers restant à construire", 'namespace' => 'outils'],
                'possible'  => ['key' => "uniquement les chantiers disponibles", 'namespace' => 'outils'],
                'construit' => ['key' => "uniquement les chantiers déjà construit", 'namespace' => 'outils'],
                'ress'      => ['key' => "Ressources", 'namespace' => 'outils'],
                'nbr'       => ['key' => "Nombre", 'namespace' => 'outils'],
                'bank'      => ['key' => "Banque", 'namespace' => 'outils'],
                'selection' => ['key' => "Sélectionner tout", 'namespace' => 'outils'],
            ],
        ];
    }
    
    public function citoyens_translate(): array
    {
        return [
            'compteurs' => [
                'titre'       => ['key' => "Compteurs", 'namespace' => 'hotel'],
                'metiers'     => ['key' => "Métiers", 'namespace' => 'hotel'],
                'pouvHeros'   => ['key' => "Pouvoirs Héros", 'namespace' => 'hotel'],
                'habitations' => ['key' => "Habitations", 'namespace' => 'hotel'],
            ],
            'citoyens'  => [
                'chaman'       => ['key' => "Chaman :", 'namespace' => 'hotel'],
                'gom'          => ['key' => "Guide de l'Outre-monde :", 'namespace' => 'hotel'],
                'citoyensVie'  => ['key' => 'Les citoyens en vie', 'namespace' => 'hotel'],
                'citoyensMort' => ['key' => 'Les citoyens morts', 'namespace' => 'hotel'],
                'cause'        => ['key' => 'Cause :', 'namespace' => 'hotel'],
                'par'          => ['key' => 'par', 'namespace' => 'hotel'],
                'inconnu'      => ['key' => 'inconnu', 'namespace' => 'hotel'],
                'portes'       => ['key' => 'En ville', 'namespace' => 'ville'],
                'ville'        => ['key' => 'Aux portes', 'namespace' => 'ville'],
                'jour'         => ['key' => 'Jour', 'namespace' => 'hotel'],
                'coffre'       => ['key' => "Ouvrir le coffre", 'namespace' => 'hotel'],
                'dern_maj_inc' => ['key' => "Dernière mise à jour inconnue", 'namespace' => 'hotel'],
                'dern_maj'     => ['key' => "Dernière mise à jour le {dateMaj} par {user}", 'namespace' => 'hotel'],
                'tab'          => [
                    'pseudo'          => ['key' => "Pseudo", 'namespace' => 'hotel'],
                    'derPouv'         => ['key' => "Dernier pouvoir", 'namespace' => 'hotel'],
                    'position'        => ['key' => "Position du joueur", 'namespace' => 'hotel'],
                    'reveil'          => ['key' => "Réveil légendaire", 'namespace' => 'hotel'],
                    'arma'            => ['key' => "Témoin de l'Arma", 'namespace' => 'hotel'],
                    'moyen'           => ['key' => "Moyen de contact", 'namespace' => 'hotel'],
                    'ruine'           => ['key' => "Niveau Ruine", 'namespace' => 'hotel'],
                    'camping'         => ['key' => "Nombre de camping", 'namespace' => 'hotel'],
                    'immu'            => ['key' => "Immunisé", 'namespace' => 'hotel'],
                    'apag'            => ['key' => "APAG", 'namespace' => 'hotel'],
                    'rdh'             => ['key' => "RDH", 'namespace' => 'hotel'],
                    'uppercut'        => ['key' => "Uppercut", 'namespace' => 'hotel'],
                    'sauvetage'       => ['key' => "Sauvetage", 'namespace' => 'hotel'],
                    'donJH'           => ['key' => "Camaraderie", 'namespace' => 'hotel'],
                    'camaraderie_don' => ['key' => "Camaraderie reçue", 'namespace' => 'hotel'],
                    'corps'           => ['key' => "Corps sain", 'namespace' => 'hotel'],
                    'secondS'         => ['key' => "Second Souffle", 'namespace' => 'hotel'],
                    'pef'             => ['key' => "PEF", 'namespace' => 'hotel'],
                    'vlm'             => ['key' => "VLM", 'namespace' => 'hotel'],
                    'trouvaille'      => ['key' => "Trouvaille", 'namespace' => 'hotel'],
                    'habitation'      => ['key' => "Habitations", 'namespace' => 'hotel'],
                    'cs'              => ['key' => "Coin sieste", 'namespace' => 'hotel'],
                    'cuisine'         => ['key' => "Cuisine", 'namespace' => 'hotel'],
                    'lab'             => ['key' => "Laboratoire", 'namespace' => 'hotel'],
                    'rangement'       => ['key' => "Rangement", 'namespace' => 'hotel'],
                    'renfort'         => ['key' => "Renfort", 'namespace' => 'hotel'],
                    'cloture'         => ['key' => "Clôture", 'namespace' => 'hotel'],
                    'carton'          => ['key' => "Carton", 'namespace' => 'hotel'],
                    'barricade'       => ['key' => "Barricade", 'namespace' => 'hotel'],
                    'coffre'          => ['key' => "Coffre", 'namespace' => 'hotel'],
                ],
            ],
            'filtre'    => [
                'operateur'   => [
                    'equal'          => ['key' => 'est égal à', 'namespace' => 'app'],
                    'notEqual'       => ['key' => "n'est pas égal à", 'namespace' => 'app'],
                    'in'             => ['key' => 'est compris dans', 'namespace' => 'app'],
                    'notIn'          => ['key' => "n'est pas compris dans", 'namespace' => 'app'],
                    'less'           => ['key' => 'est inférieur à', 'namespace' => 'app'],
                    'lessOrEqual'    => ['key' => 'est inférieur ou égal à', 'namespace' => 'app'],
                    'greater'        => ['key' => 'est supérieur à', 'namespace' => 'app'],
                    'greaterOrEqual' => ['key' => 'est supérieur ou égal à', 'namespace' => 'app'],
                    'poss'           => ['key' => 'disponible', 'namespace' => 'app'],
                    'notPoss'        => ['key' => "plus disponible", 'namespace' => 'app'],
                    'usePouv'        => ['key' => 'utilisée', 'namespace' => 'app'],
                    'notDispo'       => ['key' => "non disponible", 'namespace' => 'app'],
                    'done'           => ['key' => 'faite', 'namespace' => 'app'],
                    'notDone'        => ['key' => 'non faite', 'namespace' => 'app'],
                    'had'            => ['key' => 'oui', 'namespace' => 'app'],
                    'notHad'         => ['key' => 'non', 'namespace' => 'app'],
                    'notPouv'        => ['key' => 'non débloquée', 'namespace' => 'app'],
                ],
                'fields'      => [
                    'metier'          => ['key' => 'Métier', 'namespace' => 'hotel'],
                    'immuniser'       => ['key' => 'Immunisé', 'namespace' => 'hotel'],
                    'positionJoueur'  => ['key' => 'Position du joueur', 'namespace' => 'hotel'],
                    'apag'            => ['key' => 'APAG', 'namespace' => 'hotel'],
                    'lvlRuine'        => ['key' => 'Lvl Ruine', 'namespace' => 'hotel'],
                    'nbCamping'       => ['key' => 'Nombre de camping', 'namespace' => 'hotel'],
                    'citoyenBanni'    => ['key' => 'Citoyen banni', 'namespace' => 'hotel'],
                    'legendaire'      => ['key' => 'Légendaire', 'namespace' => 'game'],
                    'pouvoirHeros'    => ['key' => 'Pouvoir Héros', 'namespace' => 'game'],
                    'sauvetage'       => ['key' => 'Sauvetage', 'namespace' => 'game'],
                    'rdh'             => ['key' => 'RDH', 'namespace' => 'game'],
                    'uppercut'        => ['key' => 'Uppercut', 'namespace' => 'game'],
                    'donJH'           => ['key' => 'Camaraderie', 'namespace' => 'hotel'],
                    'camaraderie_don' => ['key' => 'Camaraderie reçue', 'namespace' => 'hotel'],
                    'corpsSain'       => ['key' => 'Corps Sain', 'namespace' => 'game'],
                    'secondSouffle'   => ['key' => 'Second Souffle', 'namespace' => 'game'],
                    'trouvaille'      => ['key' => 'Trouvaille', 'namespace' => 'game'],
                    'trouvailleAme'   => ['key' => 'Jolies trouvailles', 'namespace' => 'game'],
                    'vlm'             => ['key' => 'Vaincre la Mort', 'namespace' => 'game'],
                    'passageEnForce'  => ['key' => 'Passage en Force', 'namespace' => 'game'],
                    'habitation'      => ['key' => 'Habitation', 'namespace' => 'game'],
                    'coinSieste'      => ['key' => 'Coin Sieste', 'namespace' => 'game'],
                    'cuisine'         => ['key' => 'Cuisine', 'namespace' => 'game'],
                    'labo'            => ['key' => 'Laboratoire', 'namespace' => 'game'],
                    'rangement'       => ['key' => 'Rangement', 'namespace' => 'game'],
                    'renfort'         => ['key' => 'Renfort', 'namespace' => 'game'],
                    'cloture'         => ['key' => 'Clôture', 'namespace' => 'game'],
                    'carton'          => ['key' => 'Cartons', 'namespace' => 'game'],
                    'barricade'       => ['key' => 'Barricade', 'namespace' => 'game'],
                ],
                'values'      => [
                    'oui'      => ['key' => 'Oui', 'namespace' => 'app'],
                    'non'      => ['key' => 'Non', 'namespace' => 'app'],
                    'inVille'  => ['key' => 'En ville ou aux portes', 'namespace' => 'app'],
                    'dehors'   => ['key' => 'Dehors', 'namespace' => 'app'],
                    'charge_0' => ['key' => '0 charge', 'namespace' => 'hotel'],
                    'charge_1' => ['key' => '1 charge', 'namespace' => 'hotel'],
                    'charge_2' => ['key' => '2 charges', 'namespace' => 'hotel'],
                    'charge_3' => ['key' => '3 charges', 'namespace' => 'hotel'],
                ],
                'combinaison' => [
                    'or'  => ['key' => 'Ou', 'namespace' => 'app'],
                    'and' => ['key' => 'Et', 'namespace' => 'app'],
                ],
                'menu'        => [
                    'removeRule'     => ['key' => 'Supprimer la règle', 'namespace' => 'hotel'],
                    'removeGroup'    => ['key' => 'Supprimer groupe', 'namespace' => 'hotel'],
                    'addRuleLabel'   => ['key' => 'Ajouter une règle', 'namespace' => 'hotel'],
                    'addRule'        => ['key' => 'Ajouter une règle', 'namespace' => 'hotel'],
                    'addGroupLabel'  => ['key' => 'Ajouter un groupe', 'namespace' => 'hotel'],
                    'addGroup'       => ['key' => 'Ajouter un groupe', 'namespace' => 'hotel'],
                    'notToggleLabel' => ['key' => 'Inverser', 'namespace' => 'hotel'],
                    'notToggle'      => ['key' => 'Inverser se groupe', 'namespace' => 'hotel'],
                    'cloneRule'      => ['key' => 'Cloner la règle', 'namespace' => 'hotel'],
                    'cloneRuleGroup' => ['key' => 'Cloner le groupe', 'namespace' => 'hotel'],
                    'reinit'         => ['key' => 'Reinitialiser', 'namespace' => 'hotel'],
                    'filtre'         => ['key' => 'Filtrer', 'namespace' => 'hotel'],
                    'affFiltre'      => ['key' => 'Afficher les filtres', 'namespace' => 'hotel'],
                    'masquerFiltre'  => ['key' => 'Masquer les filtres', 'namespace' => 'hotel'],
                    'titreFiltre'    => ['key' => 'Filtres', 'namespace' => 'hotel'],
                ],
                'error'       => [
                    'validation' => ['key' => "Votre filtre n'est pas correct, il n'est pas possible de filtrer !", 'namespace' => 'hotel'],
                    'reglGrp'    => ['key' => 'Vous devez au moins avoir une règle dans le groupe', 'namespace' => 'hotel'],
                    'regle'      => ['key' => "Vous devez au moins avoir une règle", 'namespace' => 'hotel'],
                    'tropValeur' => ['key' => "Ne sélectionnez qu'une seule valeur pour {field}", 'namespace' => 'hotel'],
                    'uneValeur'  => ['key' => 'Vous devez sélectionner au moins une valeur pour {field}', 'namespace' => 'hotel'],
                ],
            ],
            'divers'    => [
                'generatePuce'    => ['key' => "Générer pseudo dans liste à puce", 'namespace' => 'hotel'],
                'generateList'    => ['key' => "Générer liste pseudo", 'namespace' => 'hotel'],
                'generateColonne' => ['key' => "Générer liste pseudo sans puce", 'namespace' => 'hotel'],
                'boutonCopier'    => ['key' => "Copier le texte", 'namespace' => 'hotel'],
                'boutonFermer'    => ['key' => "Fermer liste", 'namespace' => 'hotel'],
                'boutonCopierOk'  => ['key' => "Texte copié", 'namespace' => 'hotel'],
                'boutonCopierKO'  => ['key' => "Appuyer sur \"Ctrl + C\" pour copier", 'namespace' => 'hotel'],
            ],
            'popUp'     => [
                'titreErreur'  => ['key' => 'Erreur !', 'namespace' => 'app'],
                'boutonErreur' => ['key' => 'Fermer', 'namespace' => 'app'],
                'texteConseil' => ['key' => 'Veuillez réactualiser la page ou contacter les administrateurs si le problème persiste.', 'namespace' => 'app'],
                'titreSuccess' => ['key' => 'Succès !', 'namespace' => 'app'],
            ],
        ];
    }
    
    public function classementAme_translate(): array
    {
        return [
            'affichage' => [
                'choix'   => ['key' => "Choix d'affichage du classement :", 'namespace' => 'ame'],
                'general' => ['key' => "Général", 'namespace' => 'ame'],
                'ville'   => ['key' => "Ma ville", 'namespace' => 'ame'],
            ],
            'table'     => [
                'pos'    => ['key' => "Pos", 'namespace' => 'ame'],
                'pseudo' => ['key' => "Pseudo", 'namespace' => 'ame'],
                'nombre' => ['key' => "Nombre", 'namespace' => 'ame'],
            ],
            'popUp'     => $this->general_popUp(),
            'popUp_spe' => [
                'titre_general' => ['key' => "Classement pour :", 'namespace' => 'ame'],
                'titre_ville'   => ['key' => "Classement de la ville pour :", 'namespace' => 'ame'],
            ],
        ];
    }
    
    public function creationJump_translate(): array
    {
        return [
            'onglet' => [
                'jump'  => ['key' => "Jump", 'namespace' => 'jumpEvent'],
                'event' => ['key' => "Event", 'namespace' => 'jumpEvent'],
            ],
            'common' => $this->form_common_jump_event(),
            'jump'   => $this->form_jump_jump_event(),
            'event'  => $this->form_event_jump_event(),
            'popUp'  => $this->general_popUp(),
            'divers' => [
                'date'       => $this->z_date_format_time(),
                'save_jump'  => ['key' => "Créer jump", 'namespace' => 'jumpEvent'],
                'save_event' => ['key' => "Créer event", 'namespace' => 'jumpEvent'],
            ],
        ];
    }
    
    public function direction_carte(): array
    {
        return [
            ['id' => ZoneMap::DIRECTION_NORD, 'nom' => ['key' => "Nord", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_EST, 'nom' => ['key' => "Est", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_SUD, 'nom' => ['key' => "Sud", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_OUEST, 'nom' => ['key' => "Ouest", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_NORD_EST, 'nom' => ['key' => "Nord-Est", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_NORD_OUEST, 'nom' => ['key' => "Nord-Ouest", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_SUD_EST, 'nom' => ['key' => "Sud-Est", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_SUD_OUEST, 'nom' => ['key' => "Sud-Ouest", 'namespace' => 'game']],
        ];
    }
    
    public function direction_fao(): array
    {
        return [
            ['id' => ZoneMap::DIRECTION_NORD, 'nom' => ['key' => "Nord", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_EST, 'nom' => ['key' => "Est", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_SUD, 'nom' => ['key' => "Sud", 'namespace' => 'game']],
            ['id' => ZoneMap::DIRECTION_OUEST, 'nom' => ['key' => "Ouest", 'namespace' => 'game']],
        ];
    }
    
    public function encyclopedie_batiments_translate(): array
    {
        return [
            'onglet'    => [
                'batiments' => ['key' => 'Liste des bâtiments', 'namespace' => 'ency'],
                'campings'  => ['key' => "Calculateur de camping", 'namespace' => 'ency'],
            ],
            'recherche' => [
                'titre'  => ['key' => 'Recherche', 'namespace' => 'ency'],
                'bat'    => ['key' => 'Nom bâtiment :', 'namespace' => 'ency'],
                'km'     => ['key' => 'Km du bâtiment :', 'namespace' => 'ency'],
                'objet'  => ['key' => 'Objet cherché :', 'namespace' => 'ency'],
                'filtre' => ['key' => 'Filtrer sur les bâtiments de ma ville :', 'namespace' => 'ency'],
            ],
            'table'     => [
                'image'       => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'         => ['key' => 'Nom', 'namespace' => 'ency'],
                'description' => ['key' => 'Description', 'namespace' => 'ency'],
                'objets'      => ['key' => 'Objets', 'namespace' => 'ency'],
                'bonus'       => ['key' => 'Bonus<sub>camping</sub>', 'namespace' => 'ency'],
                'place'       => ['key' => 'Campeur<sub>max</sub>', 'namespace' => 'ency'],
            ],
            'camping'   => $this->camping_translate(),
        ];
    }
    
    public function encyclopedie_chantiers_translate(): array
    {
        return [
            'chantiers' => $this->chantier_list_translate(),
            'evo'       => $this->evo_chantier_list_translate(),
            'onglet'    => [
                'chantier' => ['key' => 'Liste des chantiers', 'namespace' => 'ency'],
                'evo'      => ['key' => 'Liste des évolutions', 'namespace' => 'ency'],
                'plans'    => ['key' => 'Liste des plans en ruine', 'namespace' => 'ency'],
            ],
        ];
    }
    
    public function encyclopedie_citoyens_translate(): array
    {
        return [
            'onglet'      => [
                'pts_ame'   => ['key' => "Points d'âme", 'namespace' => 'ency'],
                'pts_clean' => ['key' => "Points clean", 'namespace' => 'ency'],
                'pts_surv'  => ['key' => "Points survivant de l'enfer", 'namespace' => 'ency'],
                'mort'      => ['key' => 'Mort par désespoir', 'namespace' => 'jumpEvent'],
                'exp'       => ['key' => "Expériences héroiques", 'namespace' => 'ency'],
            ],
            'point_ame'   => [
                'titre'       => ['key' => "Points d'âme", 'namespace' => 'ency'],
                'explication' => ['key' => "Tableau donnant le nombre de point d'âme qu'il est possible de gagner en fonction du jours survécus.", 'namespace' => 'ency'],
                'entete'      => ['key' => "Jour de survie", 'namespace' => 'app'],
                'nbr_ame'     => ['key' => "Nombre de point d'âme", 'namespace' => 'app'],
            ],
            'point_clean' => [
                'titre'       => ['key' => "Points clean", 'namespace' => 'ency'],
                'explication' => ['key' => "Tableau donnant le nombre de point clean qu'il est possible de gagner en fonction du nombre de jours sans avoir consommé une drogue de son existance dans la ville.", 'namespace' => 'ency'],
                'entete'      => ['key' => "Jour de survie", 'namespace' => 'app'],
                'nbr_clean'   => ['key' => "Nombre de point clean", 'namespace' => 'app'],
            ],
            'point_surv'  => [
                'titre'       => ['key' => "Points survivant de l'enfer", 'namespace' => 'ency'],
                'explication' => ['key' => "Tableau donnant le nombre de point survivant de l'enfer qu'il est possible de gagner en fonction du nombre de jours survécus.", 'namespace' => 'ency'],
                'entete'      => ['key' => "Jour de survie", 'namespace' => 'app'],
                'nbr_surv'    => ['key' => "Nombre de point survivant de l'enfer", 'namespace' => 'app'],
            ],
            'kill_desesp' => [
                'titre_1'       => ['key' => "Mort par désespoir pour le lendemain", 'namespace' => 'ency'],
                'explication_1' => ['key' => "Tableau donnant le nombre de zombies à éliminer avec le nombre de zombies mort de désespoir lors de l'attaque afin d'avoir 0 zombie le lendemain.", 'namespace' => 'ency'],
                'entete'        => ['key' => "Nombre de zombies sur la case", 'namespace' => 'app'],
                'nbr_kill'      => ['key' => "Nombre de zombies à éliminer pour avoir 0 zombie le lendemain", 'namespace' => 'app'],
                'nbr_kill2'     => ['key' => "Nombre de zombies mort par désespoir lors de l'attaque", 'namespace' => 'app'],
                'titre_2'       => ['key' => "Mort par désespoir en cascade pour le surlendemain", 'namespace' => 'ency'],
                'explication_2' => ['key' => "Tableau donnant le nombre de zombies à éliminer avec le nombre de zombies mort de désespoir lors de l'attaque et lors de l'attaque du lendemain afin d'avoir 0 zombie le surlendemain.", 'namespace' => 'ency'],
                'entete_2'      => ['key' => "Nombre de zombies sur la case", 'namespace' => 'app'],
                'nbr_kill_2'    => ['key' => "Nombre de zombies à éliminer pour avoir 0 zombie le surlendemain", 'namespace' => 'app'],
                'nbr_kill2_2'   => ['key' => "Nombre de zombies mort par désespoir lors de l'attaque", 'namespace' => 'app'],
                'nbr_kill3_2'   => ['key' => "Nombre de zombies mort par désespoir lors de l'attaque du lendemain", 'namespace' => 'app'],
            ],
            'exp'         => [
                'titre'       => ['key' => "Expériences héroiques", 'namespace' => 'ency'],
                'explication' => ['key' => "Tableau donnant les différents pouvoirs héros obtenables en fonction du nombre d'expérience jour héros.", 'namespace' => 'ency'],
                'image'       => ['key' => "Image", 'namespace' => 'ency'],
                'nom'         => ['key' => "Nom", 'namespace' => 'ency'],
                'desc'        => ['key' => "Description", 'namespace' => 'ency'],
                'jours_diff'  => ['key' => "Jours différences", 'namespace' => 'ency'],
                'jours_cumul' => ['key' => "Jours cumulés", 'namespace' => 'ency'],
            ],
        ];
    }
    
    public function encyclopedie_objet_translate(): array
    {
        return [
            'onglet'    => [
                'objet'     => ['key' => 'Objets', 'namespace' => 'ency'],
                'armes'     => ['key' => 'Armes', 'namespace' => 'ency'],
                'veilles'   => ['key' => 'Armes de veille', 'namespace' => 'ency'],
                'poubelles' => ['key' => 'Objets poubelle', 'namespace' => 'ency'],
            ],
            'item'      => [
                'image'       => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'         => ['key' => 'Nom', 'namespace' => 'ency'],
                'categorie'   => ['key' => 'Categorie', 'namespace' => 'ency'],
                'description' => ['key' => 'Description', 'namespace' => 'ency'],
                'action'      => ['key' => 'Action', 'namespace' => 'ency'],
            ],
            'arme'      => [
                'image'        => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'          => ['key' => 'Nom', 'namespace' => 'ency'],
                'nbr_kill'     => ['key' => 'Nombre de kill', 'namespace' => 'ency'],
                'nbr_kill_avg' => ['key' => 'Nombre de kill moyen', 'namespace' => 'ency'],
                'pct_reussite' => ['key' => '% de réussite des kills', 'namespace' => 'ency'],
                'pct_casse'    => ['key' => '% de casse ou épuisement', 'namespace' => 'ency'],
                'devient'      => ['key' => 'devient', 'namespace' => 'ency'],
            ],
            'veille'    => [
                'image'      => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'        => ['key' => 'Nom', 'namespace' => 'ency'],
                'def'        => ['key' => 'Défense de base', 'namespace' => 'ency'],
                'affecte'    => ['key' => 'Affecté par', 'namespace' => 'ency'],
                'statut'     => ['key' => 'Est', 'namespace' => 'ency'],
                'tourelle'   => ['key' => 'tourelle lance-eau', 'namespace' => 'ency'],
                'armurerie'  => ['key' => 'armurerie', 'namespace' => 'ency'],
                'magasin'    => ['key' => 'magasin suédois', 'namespace' => 'ency'],
                'bete'       => ['key' => 'lance-bête', 'namespace' => 'ency'],
                'repa'       => ['key' => 'reparable', 'namespace' => 'ency'],
                'unique'     => ['key' => 'à usage unique', 'namespace' => 'ency'],
                'encombrant' => ['key' => 'encombrant', 'namespace' => 'ency'],
            ],
            'poubelle'  => [
                'image' => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'   => ['key' => 'Nom', 'namespace' => 'ency'],
                'proba' => ['key' => 'Probabilité (en %)', 'namespace' => 'ency'],
            ],
            'action'    => [
                'label'     => ['key' => "Nom de l'objet :", 'namespace' => 'ency'],
                'recherche' => ['key' => "Recherche", 'namespace' => 'ency'],
            ],
            'divers'    => [
                'et'         => ['key' => "et", 'namespace' => 'app'],
                'ou'         => ['key' => "ou", 'namespace' => 'app'],
                'casse'      => ['key' => "cassé(e)", 'namespace' => 'items'],
                'disparu'    => ['key' => "disparu(e)", 'namespace' => 'items'],
                'killMinMax' => ['key' => '{killMin} à {killMax}', 'namespace' => 'ency'],
            ],
            'recherche' => [
                'label'       => ['key' => "Rechercher un objet :", 'namespace' => 'ency'],
                'placeholder' => ['key' => "Nom de l'objet", 'namespace' => 'ency'],
                'titre'       => ['key' => "Recherche", 'namespace' => 'ency'],
            
            ],
        ];
    }
    
    public function encyclopedie_villes_translate(): array
    {
        return [
            'onglet' => [
                'hab'  => ['key' => 'Habitations', 'namespace' => 'ency'],
                'amel' => ['key' => 'Améliorations', 'namespace' => 'ency'],
            ],
            'hab'    => [
                'image'     => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'       => ['key' => 'Nom', 'namespace' => 'ency'],
                'def'       => ['key' => 'Def', 'namespace' => 'ency'],
                'cout'      => ['key' => 'Coût', 'namespace' => 'ency'],
                'cout_u'    => ['key' => "Coût avec le chantier plan d'urbanisme", 'namespace' => 'ency'],
                'pa'        => ['key' => 'en PA', 'namespace' => 'ency'],
                'ressource' => ['key' => 'en ressources', 'namespace' => 'ency'],
            ],
            'amel'   => [
                'image'     => ['key' => 'Image', 'namespace' => 'ency'],
                'nom'       => ['key' => 'Nom', 'namespace' => 'ency'],
                'cout'      => ['key' => 'Coût', 'namespace' => 'ency'],
                'pa'        => ['key' => 'en PA', 'namespace' => 'ency'],
                'ressource' => ['key' => 'en ressources', 'namespace' => 'ency'],
                'effet'     => ['key' => 'Effet', 'namespace' => 'ency'],
            ],
        ];
    }
    
    public function event_gestion_tranlate(): array
    {
        return [
            'titre'       => ['key' => "Gestion de l'event", 'namespace' => 'jumpEvent'],
            'titre_orga'  => ['key' => "Organisateur de l'event", 'namespace' => 'jumpEvent'],
            'titre_orgas' => ['key' => "Organisateurs de l'event", 'namespace' => 'jumpEvent'],
            'gestion'     => [
                'info'        => ['key' => "Info principal", 'namespace' => 'jumpEvent'],
                'pseudo_orga' => ['key' => "Joueur :", 'namespace' => 'jumpEvent'],
                'add_orga'    => ['key' => "Ajouter un organisateur", 'namespace' => 'jumpEvent'],
                'orga_ko'     => ['key' => "Impossible d'ajouter d'autres organisateurs tant que vous ne serez pas inscrit à votre event.", 'namespace' => 'jumpEvent'],
                'choix'       => ['key' => "Choix joueur.", 'namespace' => 'jumpEvent'],
                'save'        => ['key' => "Sauvegarder event", 'namespace' => 'jumpEvent'],
                'supp'        => ['key' => "Supprimer event", 'namespace' => 'jumpEvent'],
                'annul'       => ['key' => "Annuler", 'namespace' => 'jumpEvent'],
                'confirmer'   => ['key' => "Oui", 'namespace' => 'jumpEvent'],
                'titre'       => ['key' => "Confimation", 'namespace' => 'jumpEvent'],
                'message'     => ['key' => "Voulez-vous réellement supprimer l'event ? Attention, toute suppression est définitive, cela supprimera également tous les jumps et les candidatures.", 'namespace' => 'jumpEvent'],
                'jump'        => [
                    'gestion'  => ['key' => "Gestion des jumps de l'event", 'namespace' => 'jumpEvent'],
                    'nom'      => ['key' => "Nom du jump", 'namespace' => 'jumpEvent'],
                    'date_deb' => ['key' => "Date de début <br/> inscription", 'namespace' => 'jumpEvent'],
                    'date_fin' => ['key' => "Date de fin <br/> inscription", 'namespace' => 'jumpEvent'],
                    'lien'     => ['key' => "Lien vers <br/> la gestion", 'namespace' => 'jumpEvent'],
                    'gerer'    => ['key' => "Gérer le jump", 'namespace' => 'jumpEvent'],
                ],
                'log'         => [
                    'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
                    'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
                    'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
                    'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
                    'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
                    'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
                    'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
                ],
            ],
            'commonForm'  => $this->form_common_jump_event(),
            'eventForm'   => $this->form_event_jump_event(),
            'popUp'       => $this->general_popUp(),
            'divers'      => [
                'date' => $this->z_date_format_time(),
            ],
        ];
    }
    
    public function event_inscription_tranlate(): array
    {
        return [
            'general' => [
                'type'       => ['key' => "Type de ville :", 'namespace' => 'jumpEvent'],
                'orga'       => ['key' => "Organisé par", 'namespace' => 'jumpEvent'],
                'orgas'      => ['key' => "Organisés par", 'namespace' => 'jumpEvent'],
                'date_fin'   => ['key' => "Date de fin d'inscription :", 'namespace' => 'jumpEvent'],
                'date_event' => ['key' => "Date début de l'event :", 'namespace' => 'jumpEvent'],
                'statut'     => ['key' => "Statut de votre inscription :", 'namespace' => 'jumpEvent'],
            ],
            'form'    => $this->form_inscription_translate(),
            'log'     => [
                'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
                'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
                'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
                'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
                'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
                'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
                'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
            ],
            'control' => [
                'metier_deja_choisis' => ['key' => "Vous ne pouvez pas choisir un métier que vous avez déjà choisis", 'namespace' => 'jumpEvent'],
                'metier_vide'         => ['key' => "Le métier ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'pouvoir_vide'        => ['key' => "Le pouvoir héros ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'lvl_ruine_vide'      => ['key' => "Le level de la ruine ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'dispo_vide'          => ['key' => "Vous ne pouvez pas être 'Pas co' tous le temps", 'namespace' => 'jumpEvent'],
                'role_vide'           => ['key' => "Vous devez choisir un rôle", 'namespace' => 'jumpEvent'],
                'lead_vide'           => ['key' => "Vous avez choisis d'être lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
                'applead_vide'        => ['key' => "Vous avez choisis d'être apprenti lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
            
            ],
            'btn'     => [
                'btn_save'   => ['key' => "S'enregistrer", 'namespace' => 'jumpEvent'],
                'btn_mod'    => ['key' => "Modifier", 'namespace' => 'jumpEvent'],
                'btn_retour' => ['key' => "Retour", 'namespace' => 'jumpEvent'],
                'btn_desit'  => ['key' => "Se désister", 'namespace' => 'jumpEvent'],
                'btn_reins'  => ['key' => "Se réinscrire", 'namespace' => 'jumpEvent'],
            ],
            'confirm' => [
                'annul'     => ['key' => "Annuler", 'namespace' => 'jumpEvent'],
                'confirmer' => ['key' => "Oui", 'namespace' => 'jumpEvent'],
                'titre'     => ['key' => "Confimation", 'namespace' => 'jumpEvent'],
                'message'   => ['key' => "Êtes-vous sur de vous désister ? Il sera possible de se réinscrire.", 'namespace' => 'jumpEvent'],
            ],
            'popUp'   => $this->general_popUp(),
            'divers'  => [
                'date' => $this->z_date_format_time(),
                'et'   => ['key' => "et", 'namespace' => 'app'],
            ],
        ];
    }
    
    public function event_list_inscription(): array
    {
        return [
            'bouton' => [
                'modifier' => ['key' => 'Modifier', 'namespace' => 'jumpEvent'],
                'inscrire' => ['key' => "S'inscrire", 'namespace' => 'jumpEvent'],
            ],
            'finis'  => ['key' => "Les inscriptions sont finis, il n'est plus possible de s'y inscrire !", 'namespace' => 'jumpEvent'],
        ];
    }
    
    public function evo_chantier_list_translate(): array
    {
        
        
        return [
            'nom'   => ['key' => "Nom Chantier", 'namespace' => 'ency'],
            'effet' => ['key' => "Effet de base", 'namespace' => 'ency'],
            'lvl1'  => ['key' => "Niveau 1", 'namespace' => 'ency'],
            'lvl2'  => ['key' => "Niveau 2", 'namespace' => 'ency'],
            'lvl3'  => ['key' => "Niveau 3", 'namespace' => 'ency'],
            'lvl4'  => ['key' => "Niveau 4", 'namespace' => 'ency'],
            'lvl5'  => ['key' => "Niveau 5", 'namespace' => 'ency'],
        ];
    }
    
    public function general_popUp(): array
    {
        return [
            'succes'         => ['key' => 'Succès !', 'namespace' => 'app'],
            'erreur'         => ['key' => 'Erreur !', 'namespace' => 'app'],
            'warning'        => ['key' => 'Attention !', 'namespace' => 'app'],
            'info'           => ['key' => 'Information', 'namespace' => 'app'],
            'attention'      => ['key' => 'Attention !', 'namespace' => 'app'],
            'avertissement'  => ['key' => 'Avertissement', 'namespace' => 'app'],
            'conseil_erreur' => ['key' => 'Veuillez réactualiser la page ou contacter les administrateurs si le problème persiste.', 'namespace' => 'app'],
            'btn_fermer'     => ['key' => 'Fermer', 'namespace' => 'app'],
            'btn_valider'    => ['key' => 'Valider', 'namespace' => 'app'],
            'btn_annuler'    => ['key' => 'Annuler', 'namespace' => 'app'],
        ];
    }
    
    public function generale_translate(): array
    {
        return [
            'entete'     => [
                'devise'     => ['key' => 'Le site pour gérer ta ville MyHordes !', 'namespace' => 'app'],
                'inconnu'    => ['key' => 'inconnu', 'namespace' => 'app'],
                'numVille'   => ['key' => 'Ville n°', 'namespace' => 'app'],
                'maVille'    => ['key' => 'Vous êtes actuellement dans la ville {ville} jour <span id="jourVille">{jour}</span>', 'namespace' => 'app'],
                'pasVille'   => ['key' => 'Pas de ville', 'namespace' => 'app'],
                'visitVille' => ['key' => 'Vous visitez actuellement la ville {ville} jour <span id="jourVille">{jour}</span>', 'namespace' => 'app'],
                'majPhrase'  => ['key' => 'Dernière mise à jour le {date} par {user}', 'namespace' => 'app'],
                'ptsSaison'  => ['key' => 'Points de saison : {point}', 'namespace' => 'app'],
            ],
            'pied_page'  => [
                'developpeur'  => ['key' => "Gest'Hordes codé par Eragony", 'namespace' => 'app'],
                'version'      => ['key' => "Version du {date}", 'namespace' => 'app'],
                'version_jeu'  => ['key' => "Version du jeu : {version}", 'namespace' => 'app'],
                'affiliation'  => ['key' => "Ce site n'est pas affilié à Eternal Twin, MyHordes ou Motion Twin - certains éléments graphiques appartiennent à Motion Twin et MyHordes.", 'namespace' => 'app'],
                'scriptMaj'    => ['key' => "Script de mise à jour depuis MyHordes directement :", 'namespace' => 'app'],
                'mhe'          => ['key' => "clique-ici pour MHE", 'namespace' => 'app'],
                'mho'          => ['key' => "clique-ici pour MHOA", 'namespace' => 'app'],
                'tamper'       => ['key' => "(Utilise Tampermonkey ou Greasemonkey (Google Chrome et Firefox)), un grand merci à Ludo (MHE) et à Zerah et Renack (MHOA)", 'namespace' => 'app'],
                'merci'        => ['key' => "Les thèmes ont été pensés en grande partie par Katt, un grand merci à elle !", 'namespace' => 'app'],
                'sugg'         => ['key' => "Une suggestion, un bug rencontré, une anomalie, un seul endroit :", 'namespace' => 'app'],
                'remerciement' => ['key'       => "Suite à la mise à jour de la saison 16, Gest'Hordes a été rapidement actualisé grâce aux retours des joueurs. Il reste encore des choses à faire, mais un grand merci à : <span id='listPseudoRemerciement'>{listPseudo}</span>. Si j'ai oublié quelqu'un, n'hésitez pas à me le signaler, cela n'était pas intentionnel !",
                                   'namespace' => 'app'],
                'discord'      => ['key' => "rejoignez le discord !", 'namespace' => 'app'],
                'ou'           => ['key' => "ou", 'namespace' => 'app'],
            ],
            'menu'       => [
                'general'      => [
                    'maj'         => ['key' => "Mise à jour", 'namespace' => 'app'],
                    'myVille'     => ['key' => 'Ma ville', 'namespace' => 'app'],
                    'quitte'      => ['key' => 'Quitter la ville', 'namespace' => 'app'],
                    'connexion'   => ['key' => "Connexion", 'namespace' => 'app'],
                    'deconnexion' => ['key' => "Déconnexion", 'namespace' => 'app'],
                ],
                'Ville'        => [
                    'ville'  => ['key' => "Ville", 'namespace' => 'app'],
                    'carte'  => ['key' => "Carte", 'namespace' => 'app'],
                    'banque' => ['key' => "Banque", 'namespace' => 'app'],
                    'ruine'  => ['key' => "Ruines", 'namespace' => 'app'],
                ],
                'Hotel'        => [
                    'hotel'           => ['key' => "Hôtel de ville", 'namespace' => 'app'],
                    'citoyens'        => ['key' => "Citoyens", 'namespace' => 'app'],
                    'journal'         => ['key' => "Journal", 'namespace' => 'app'],
                    'plansDeChantier' => ['key' => "Plans de chantier", 'namespace' => 'app'],
                    'stats'           => ['key' => "Statistiques", 'namespace' => 'app'],
                    'tour'            => ['key' => "Tour de guet", 'namespace' => 'app'],
                    'inscription'     => ['key' => "Inscriptions Expéditions", 'namespace' => 'app'],
                ],
                'Outils'       => [
                    'outils'    => ['key' => "Outils", 'namespace' => 'app'],
                    'expes'     => ['key' => "Expéditions", 'namespace' => 'app'],
                    'repa'      => ['key' => "Réparations", 'namespace' => 'app'],
                    'chantiers' => ['key' => "Chantiers", 'namespace' => 'app'],
                    'veilles'   => ['key' => "Veilles", 'namespace' => 'app'],
                    'decharges' => ['key' => "Décharges", 'namespace' => 'app'],
                    'cm'        => ['key' => "Camping Massif", 'namespace' => 'app'],
                ],
                'Jump'         => [
                    'jump'        => ['key' => "Jump/Event", 'namespace' => 'app'],
                    'creation'    => ['key' => "Créations", 'namespace' => 'app'],
                    'inscription' => ['key' => "Inscriptions", 'namespace' => 'app'],
                    'gestion'     => ['key' => "Gestions", 'namespace' => 'app'],
                    'coa'         => ['key' => "Coalitions", 'namespace' => 'app'],
                    'event'       => ['key' => "Event", 'namespace' => 'app'],
                ],
                'Ency'         => [
                    'titre'    => ['key' => "Encyclopédie", 'namespace' => 'app'],
                    'bat'      => ['key' => "Bâtiments", 'namespace' => 'app'],
                    'chantier' => ['key' => "Chantiers", 'namespace' => 'app'],
                    'citoyens' => ['key' => "Citoyens", 'namespace' => 'app'],
                    'objets'   => ['key' => "Objets", 'namespace' => 'app'],
                    'villes'   => ['key' => 'Villes', 'namespace' => 'app'],
                ],
                'Ame'          => [
                    'titre'      => ['key' => "Âme", 'namespace' => 'app'],
                    'ame'        => ['key' => "Mon âme", 'namespace' => 'app'],
                    'classement' => ['key' => "Classement", 'namespace' => 'app'],
                ],
                'villes'       => ['key' => "Villes", 'namespace' => 'app'],
                'news'         => ['key' => "News", 'namespace' => 'app'],
                'statistiques' => ['key' => "Statistiques", 'namespace' => 'app'],
                'contact'      => ['key' => "Contact", 'namespace' => 'app'],
                'ruineGame'    => ['key' => "Ruine (Jeu)", 'namespace' => 'app'],
            ],
            'chargement' => ['key' => 'Chargement...', 'namespace' => 'app'],
            'popUp'      => $this->general_popUp(),
            'divers'     => [
                'date' => $this->z_date_format_time(),
            ],
        ];
    }
    
    public function getTranslateContact(): array
    {
        return [
            'titre_page'          => ['key' => "Formulaire de contact", 'namespace' => 'app'],
            'discord'             => ['key' => "Attention, toutes les informations fournis seront visibles sur le discord de Gest'Hordes", 'namespace' => 'app'],
            'clique'              => ['key' => "Cliquez-ici pour rejoindre", 'namespace' => 'app'],
            'creer'               => ['key' => "Créer un ticket", 'namespace' => 'app'],
            'tag'                 => ['key' => "Tag", 'namespace' => 'app'],
            'bug'                 => ['key' => "Bug", 'namespace' => "app"],
            'suggestion'          => ['key' => "Suggestion", 'namespace' => "app"],
            'titre'               => ['key' => "Titre", 'namespace' => 'app'],
            'message'             => ['key' => "Message", 'namespace' => 'app'],
            'obligatoire'         => ['key' => "Saisie obligatoire", 'namespace' => 'app'],
            'piece_jointe'        => ['key' => "Pièce jointe (vous pouvez collés les images dans le cadre ou les insérez via le bouton)", 'namespace' => 'app'],
            'placeholder_titre'   => ['key' => "Votre titre ici", 'namespace' => 'app'],
            'placeholder_message' => ['key' => "Ecrivez votre message ici", 'namespace' => 'app'],
            'imageTranslate'      => [
                'insert' => ['key' => "Insérer une image", 'namespace' => 'app'],
            ],
            'lienMessageDiscord'  => ['key' => "Cliquez ici pour rejoindre votre post sur discord", 'namespace' => 'app'],
        ];
    }
    
    public function getTranslatePageAme(): array
    {
        return [
            'identity'   => [
                'pseudo'       => ['key' => 'Pseudo : {pseudo}', 'namespace' => 'ame'],
                'dernier_pouv' => ['key' => 'Dernier pouvoir : {pouvoir}', 'namespace' => 'ame'],
                'ville_cours'  => ['key' => 'Ville en cours : {ville}', 'namespace' => 'ame'],
                'pas_ville'    => ['key' => 'Actuellement pas en ville', 'namespace' => 'ame'],
                'der_maj_inc'  => ['key' => "Dernière mise à jour de l'âme : inconnue", 'namespace' => 'ame'],
                'der_maj'      => ['key' => "Dernière mise à jour de l'âme : {date}", 'namespace' => 'ame'],
                'ame'          => ['key' => "Total des pictos :", 'namespace' => 'ame'],
            ],
            'historique' => [
                'titre'      => ['key' => 'Historiques des incarnations', 'namespace' => 'ame'],
                'saison'     => ['key' => 'Saison', 'namespace' => 'ame'],
                'voir_carte' => ['key' => 'Voir la carte', 'namespace' => 'ame'],
                'mort_j'     => ['key' => 'Mort J', 'namespace' => 'ame'],
                'cause'      => ['key' => 'Cause :', 'namespace' => 'ame'],
                'par'        => ['key' => 'par', 'namespace' => 'ame'],
                'inconnu'    => ['key' => 'inconnu', 'namespace' => 'ame'],
                'msg_mort'   => ['key' => 'Message de mort', 'namespace' => 'ame'],
                'ville_com'  => ['key' => 'Villes communes', 'namespace' => 'ame'],
            ],
            'voisin'     => [
                'nbr_ville' => ['key' => 'Nombre de ville totale :', 'namespace' => 'ame'],
                'titre'     => ['key' => 'Liste des voisins', 'namespace' => 'ame'],
                'pseudo'    => ['key' => 'Pseudo', 'namespace' => 'ame'],
                'nombre'    => ['key' => 'Nombre', 'namespace' => 'ame'],
                'filtre'    => ['key' => 'Voir mes voisins de la saison :', 'namespace' => 'ame'],
                'pct_ville' => ['key' => '% ville commune', 'namespace' => 'ame'],
            ],
            'menu'       => [
                'bouton_charg'  => ['key' => 'Charger plus de villes', 'namespace' => 'ame'],
                'bouton_moins'  => ['key' => 'Voir moins de villes', 'namespace' => 'ame'],
                'toutes_saison' => ['key' => 'Toutes les saisons', 'namespace' => 'ame'],
                'saison'        => ['key' => 'Saison', 'namespace' => 'ame'],
                'filtre'        => ['key' => 'Voir les villes de la saison :', 'namespace' => 'ame'],
            ],
        ];
    }
    
    public function getTranslateRuineGame(): array
    {
        return [
            'titre_page' => ['key' => "Exploration de ruine", 'namespace' => 'ruineGame'],
            'chargement' => ['key' => "Chargement en cours", 'namespace' => 'app'],
            'onglet'     => [
                'game'       => ['key' => "Jeu", 'namespace' => 'ruineGame'],
                'classement' => ['key' => "Classement", 'namespace' => 'ruineGame'],
                'historique' => ['key' => "Historique des parties", 'namespace' => 'ruineGame'],
            ],
            'game'       => [
                'regle'         => ['key' => "Règles du jeu", 'namespace' => 'ruineGame'],
                'voir_regle'    => ['key' => "Voir les règles du jeu", 'namespace' => 'ruineGame'],
                'mask_regle'    => ['key' => "Masquer les règles du jeu", 'namespace' => 'ruineGame'],
                'description1'  => ['key' => "Ce jeu vous permet d'explorer des ruines, comme vous pouvez le faire sur MyHordes, à quelques exceptions près.", 'namespace' => 'ruineGame'],
                'description2'  => ['key' => "En effet, vous avez plusieurs niveaux de difficulté à disposition.", 'namespace' => 'ruineGame'],
                'description3'  => ['key' => "Une fois la partie lancée, vous aurez deux jauges : une d'oxygène et une d'énergie, vous permettant d'essayer de tuer des zombies (tuer n'est pas garantie).", 'namespace' => 'ruineGame'],
                'description4'  => ['key' => "Une fois celle-ci à 0, vous ne pourrez plus tuer de zombie, il faudra fuir, et donc perdre de l'oxygène. Cette jauge est en fonction de vos options choisies.", 'namespace' => 'ruineGame'],
                'description5'  => ['key' => "La partie s'arrête quand vous sortez de la ruine ou que vous n'avez plus d'oxygène. Vous pourrez ensuite refaire une nouvelle partie !", 'namespace' => 'ruineGame'],
                'description6'  => ['key' => "À chaque lancement de partie, c'est une nouvelle ruine qui s'offre à vous !", 'namespace' => 'ruineGame'],
                'help_plan'     => ['key' => "<ul><li>Plan complet : le plan est visible à côté de la ruine.</li><li>Plan autotracé : le plan se dessine au fur et à mesure de votre exploration</li><li>Sans plan : comme sur MyHordes, exploration à l'aveugle.</li></ul>", 'namespace' => 'ruineGame'],
                'choix_option'  => ['key' => "Choix des options possibles :", 'namespace' => 'ruineGame'],
                'lancer_partie' => ['key' => "Entrer dans la ruine", 'namespace' => 'ruineGame'],
                'lancer_new'    => ['key' => "Relancer une partie", 'namespace' => 'ruineGame'],
                'etages'        => ['key' => "Nombre d'étage :", 'namespace' => 'ruineGame'],
                'oxygene'       => ['key' => "Quantité d'oxygène :", 'namespace' => 'ruineGame'],
                'plan'          => ['key' => "Type de plan :", 'namespace' => 'ruineGame'],
                'zombie'        => ['key' => "Nombre de zombie par étage :", 'namespace' => 'ruineGame'],
                'play'          => [
                    'sac'      => ['key' => "Sac :", 'namespace' => 'ruineGame'],
                    'fuite'    => ['key' => "Fuir", 'namespace' => 'ruineGame'],
                    'tuer'     => ['key' => "Tuer zombie", 'namespace' => 'ruineGame'],
                    'fouiller' => ['key' => "Fouiller piece", 'namespace' => 'ruineGame'],
                    'enter'    => ['key' => "Entrer dans la piece", 'namespace' => 'ruineGame'],
                    'quit'     => ['key' => "Sortir de la piece", 'namespace' => 'ruineGame'],
                    'up'       => ['key' => "Monter l'escalier", 'namespace' => 'ruineGame'],
                    'down'     => ['key' => "Descendre l'escalier", 'namespace' => 'ruineGame'],
                    'plan'     => ['key' => "Plan de la ruine", 'namespace' => 'ruineGame'],
                    'sortir'   => ['key' => "Sortir", 'namespace' => 'ruineGame'],
                    'action'   => ['key' => "Liste des actions", 'namespace' => 'ruineGame'],
                ],
            ],
            'historique' => [
                'date_partie'     => ['key' => "Date de la partie", 'namespace' => 'ruineGame'],
                'nb_oxygene'      => ['key' => "Quantité d'oxygène", 'namespace' => 'ruineGame'],
                'nb_etage'        => ['key' => "Nombre d'étage", 'namespace' => 'ruineGame'],
                'nb_zombie'       => ['key' => "Nombre de zombie", 'namespace' => 'ruineGame'],
                'type_carte'      => ['key' => "Type carte", 'namespace' => 'ruineGame'],
                'type_ruine'      => ['key' => "Type ruine", 'namespace' => 'ruineGame'],
                'nb_oxy_restant'  => ['key' => "Quantité d'oxygène restant", 'namespace' => 'ruineGame'],
                'nb_mana_restant' => ['key' => "Quantité d'énergie restante", 'namespace' => 'ruineGame'],
                'score'           => ['key' => "Score", 'namespace' => 'ruineGame'],
                'pct_explo'       => ['key' => "Exploration", 'namespace' => 'ruineGame'],
                'pct_fouille'     => ['key' => "Fouille", 'namespace' => 'ruineGame'],
                'pct_kill'        => ['key' => "Extermination", 'namespace' => 'ruineGame'],
                'sortis'          => ['key' => "Sorti", 'namespace' => 'ruineGame'],
                'voir_etage'      => ['key' => "Voir l'étage", 'namespace' => 'ruineGame'],
                'map'             => ['key' => "Revoir la map", 'namespace' => 'ruineGame'],
            ],
            'map'        => [
                'legende'           => ['key' => "Légende", 'namespace' => 'ruineGame'],
                'zone_explo'        => ['key' => "Case explorée", 'namespace' => 'ruineGame'],
                'porte_non_fouille' => ['key' => "Piéce non fouillée", 'namespace' => 'ruineGame'],
                'zombie_1'          => ['key' => "1 zombie", 'namespace' => 'ruineGame'],
                'zombie_2'          => ['key' => "2 zombies", 'namespace' => 'ruineGame'],
                'zombie_3'          => ['key' => "3 zombies", 'namespace' => 'ruineGame'],
                'zombie_4'          => ['key' => "4 zombies", 'namespace' => 'ruineGame'],
                'fermer'            => ['key' => "Fermer", 'namespace' => 'ruineGame'],
            ],
            'score'      => [
                'titre'      => ['key' => "Score :", 'namespace' => 'ruineGame'],
                'kill'       => ['key' => "Pourcentage d'extermination de zombie", 'namespace' => 'ruineGame'],
                'explo'      => ['key' => "Pourcentage d'exploration de la ruine", 'namespace' => 'ruineGame'],
                'fouille'    => ['key' => "Pourcentage de fouille de pièce", 'namespace' => 'ruineGame'],
                'is_ejected' => ['key' => "Sorti de la ruine", 'namespace' => 'ruineGame'],
                'oxygene'    => ['key' => "Oxygène restant", 'namespace' => 'ruineGame'],
            ],
            'trad_porte' => [
                RuinesCases::TYPE_PORTE_OUV   => "small_enter",
                RuinesCases::TYPE_PORTE_FERM  => "item_lock",
                RuinesCases::TYPE_PORTE_DECAP => "item_classicKey",
                RuinesCases::TYPE_PORTE_PERCU => "item_bumpKey",
                RuinesCases::TYPE_PORTE_MAGN  => "item_magneticKey",
            ],
            'divers'     => [
                'date_format' => $this->z_date_format_time(),
            ],
        ];
    }
    
    public function getTranslateStats(): array
    {
        return [
            'onglet'    => [
                'users'     => ['key' => 'Utilisateurs', 'namespace' => 'app'],
                'cities'    => ['key' => 'Villes', 'namespace' => 'app'],
                'buildings' => ['key' => 'Chantiers', 'namespace' => 'app'],
                'maps'      => ['key' => 'Carte', 'namespace' => 'app'],
            ],
            'commun'    => [
                'nbr'    => ['key' => 'Nombre', 'namespace' => 'stats'],
                'pct'    => ['key' => 'Pourcentage', 'namespace' => 'stats'],
                'saison' => ['key' => 'Statistiques pour : ', 'namespace' => 'stats'],
                'total'  => ['key' => 'Total', 'namespace' => 'stats'],
                'tous'   => ['key' => 'Tous types', 'namespace' => 'stats'],
                'RNE'    => ['key' => 'RNE', 'namespace' => 'stats'],
                'RE'     => ['key' => 'RE', 'namespace' => 'stats'],
                'pande'  => ['key' => 'Pandemonium', 'namespace' => 'stats'],
            ],
            'users'     => [
                'themes'     => ['key' => 'Thèmes', 'namespace' => 'app'],
                'power'      => ['key' => 'Pouvoir', 'namespace' => 'app'],
                'active'     => ['key' => 'Actifs', 'namespace' => 'app'],
                'chartTheme' => [
                    'titre'  => ['key' => 'Repartition des thèmes utilisés', 'namespace' => 'stats'],
                    'legend' => ['key' => 'Thème', 'namespace' => 'stats'],
                ],
                'chartPower' => [
                    'titre'  => ['key' => 'Repartition des pouvoirs héros', 'namespace' => 'stats'],
                    'legend' => ['key' => 'Pouvoir héros', 'namespace' => 'stats'],
                ],
            ],
            'cities'    => [
                'cities'        => ['key' => 'Villes', 'namespace' => 'app'],
                'jobs'          => ['key' => 'Métiers', 'namespace' => 'app'],
                'chartJob'      => [
                    'titre'  => ['key' => 'Repartition des métiers', 'namespace' => 'stats'],
                    'legend' => ['key' => 'Métier', 'namespace' => 'stats'],
                ],
                'chartMort'     => [
                    'titre'  => ['key' => 'Repartition des morts par jour', 'namespace' => 'stats'],
                    'legend' => ['key' => 'Mort', 'namespace' => 'stats'],
                ],
                'chartTypeMort' => [
                    'titre'  => ['key' => 'Repartition des types de morts', 'namespace' => 'stats'],
                    'legend' => ['key' => 'Type de mort', 'namespace' => 'stats'],
                ],
            ],
            'buildings' => [
                'work'     => ['key' => 'Répartitions des Chantiers', 'namespace' => 'app'],
                'tabsWork' => ['key' => 'Nom du chantier', 'namespace' => 'stats'],
            ],
            'maps'      => [
                'ruins'     => ['key' => 'Répartition des bâtiments trouvés', 'namespace' => 'stats'],
                'scrut'     => ['key' => 'Répartition des directions du scrutateur', 'namespace' => 'stats'],
                'tabsRuins' => ['key' => 'Nom du bâtiment', 'namespace' => 'stats'],
                'tabsScrut' => ['key' => 'Direction du scrutateur', 'namespace' => 'stats'],
            ],
        ];
    }
    
    public function inscription_expedition_translate(): array
    {
        return [
            'preinscrit'              => ['key' => "Préinscrits", 'namespace' => 'hotel'],
            'preinscrit_expe'         => ['key' => "Préinscrits sur cette expédition :", 'namespace' => 'hotel'],
            'pdc'                     => ['key' => "PDC", 'namespace' => 'hotel'],
            'pseudo'                  => ['key' => "Pseudo", 'namespace' => 'hotel'],
            'sac'                     => ['key' => "Sac", 'namespace' => 'hotel'],
            'action_h'                => ['key' => "Action héroïque", 'namespace' => 'hotel'],
            'dispo'                   => ['key' => "Disponibilités", 'namespace' => 'hotel'],
            'dispo_h'                 => ['key' => "Légende des choix de disponibilité possibile :", 'namespace' => 'hotel'],
            'consignes'               => ['key' => "Consignes", 'namespace' => 'hotel'],
            'consigne'                => ['key' => "Consigne", 'namespace' => 'hotel'],
            'description'             => ['key' => "Description", 'namespace' => 'hotel'],
            'soif'                    => ['key' => "Soif", 'namespace' => 'hotel'],
            'complete'                => ['key' => "Complète", 'namespace' => 'hotel'],
            'oui'                     => ['key' => "Oui", 'namespace' => 'app'],
            'non'                     => ['key' => "Non", 'namespace' => 'app'],
            'fao_direction'           => ['key' => "Fao direction :", 'namespace' => 'app'],
            'titre_exp'               => ['key' => "Liste des expéditions", 'namespace' => 'app'],
            'titre_ouv'               => ['key' => "Ouvriers", 'namespace' => 'app'],
            'phrase_information'      => ['key' => "Il n'y a pas d'expédition et de place ouvrier encore ouvertes, veuillez patienter !", 'namespace' => 'hotel'],
            'phrase_information_expe' => ['key' => "Il n'y a pas encore de place en expédition ouverte, veuillez patienter !", 'namespace' => 'hotel'],
            'warning_preinscrit'      => ['key' => "Attention, vous êtes préinscrit ailleurs !", 'namespace' => 'hotel'],
            'confirmationpdc'         => [
                'message' => ['key' => "Êtes vous sûr de vous inscrire ici ? Vous n'aurez pas les points de contrôle nécessaire !", 'namespace' => 'hotel'],
                'titre'   => ['key' => "Attention !", 'namespace' => 'hotel'],
            ],
            'confirmationpseudo'      => [
                'message' => ['key' => "Joueur déjà inscrit ailleurs, êtes vous sûr ?", 'namespace' => 'hotel'],
                'titre'   => ['key' => "Attention !", 'namespace' => 'hotel'],
            ],
            'confirmationpseudo2'     => [
                'message' => ['key' => "Il y a déjà un joueur inscrit à cette place, êtes vous sûr ?", 'namespace' => 'hotel'],
                'titre'   => ['key' => "Attention !", 'namespace' => 'hotel'],
            ],
            'confirmation'            => [
                'annuler'      => ['key' => "Non", 'namespace' => 'hotel'],
                'confirmation' => ['key' => "Oui", 'namespace' => 'hotel'],
            ],
            'masquer'                 => ['key' => "Masquer les expéditions complètes", 'namespace' => 'hotel'],
            'jour'                    => ['key' => "Jour", 'namespace' => 'hotel'],
            'appercu'                 => ['key' => "Aperçu du tracé", 'namespace' => 'hotel'],
            'fermer'                  => ['key' => "Fermer", 'namespace' => 'hotel'],
            'dispo_rapide'            => ['key' => "Utiliser une dispo rapide", 'namespace' => 'hotel'],
            'dispo_ok'                => ['key' => "Ok", 'namespace' => 'hotel'],
        ];
    }
    
    public function journal_translate(): array
    {
        return [
            'jour'      => ['key' => 'Jour', 'namespace' => 'hotel'],
            'contenu'   => ['key' => 'Contenu de la gazette', 'namespace' => 'hotel'],
            'zombie'    => ['key' => 'Zombies', 'namespace' => 'hotel'],
            'defense'   => ['key' => 'Défense', 'namespace' => 'hotel'],
            'direction' => ['key' => 'Direction et regénération scrutateur', 'namespace' => 'hotel'],
            'inconnue'  => ['key' => 'Direction inconnue', 'namespace' => 'hotel'],
            'vote'      => ['key' => 'Evolution voté la veille', 'namespace' => 'hotel'],
            'chantiers' => ['key' => 'Chantiers construits', 'namespace' => 'hotel'],
        ];
    }
    
    public function jump_candidature_tranlate(): array
    {
        return [
            'general' => [
                'candidature' => ['key' => "Candidature de {inscripuser}", 'namespace' => 'jumpEvent'],
                'date_crea'   => ['key' => "Date d'inscription :", 'namespace' => 'jumpEvent'],
                'date_mod'    => ['key' => "Date derniere modification :", 'namespace' => 'jumpEvent'],
                'statut'      => ['key' => "Statut de la candidature :", 'namespace' => 'jumpEvent'],
            ],
            'form'    => [
                'info'                => ['key' => "Information générale sur le joueur", 'namespace' => 'jumpEvent'],
                'banCommu'            => ['key' => "Volontaire banni communautaire :", 'namespace' => 'jumpEvent'],
                'gouleCommu'          => ['key' => "Volontaire goule communautaire :", 'namespace' => 'jumpEvent'],
                'voeuxMetier_place'   => ['key' => "choix métier", 'namespace' => 'jumpEvent'],
                'voeuxMetier1'        => ['key' => "Voeu métier n°1 :", 'namespace' => 'jumpEvent'],
                'voeuxMetier2'        => ['key' => "Voeu métier n°2 :", 'namespace' => 'jumpEvent'],
                'voeuxMetier3'        => ['key' => "Voeu métier n°3 :", 'namespace' => 'jumpEvent'],
                'infoMetier'          => ['key' => "Pour rappel les vœux des métiers sont classés du plus important au moins important.", 'namespace' => 'jumpEvent'],
                'lvlRuine'            => ['key' => "Le niveau de ruine :", 'namespace' => 'jumpEvent'],
                'lvlRuine_place'      => ['key' => "choix niveau de ruine", 'namespace' => 'jumpEvent'],
                'pouvFutur'           => ['key' => "Le dernier pouvoir débloqué prévu pour le jump :", 'namespace' => 'jumpEvent'],
                'pouvFutur_place'     => ['key' => "choix pouvoir", 'namespace' => 'jumpEvent'],
                'dispo'               => ['key' => "Les disponibilités types", 'namespace' => 'jumpEvent'],
                'dispo_place'         => ['key' => "choix dispo", 'namespace' => 'jumpEvent'],
                'dispo_semaine'       => ['key' => "Disponibilité type en semaine", 'namespace' => 'jumpEvent'],
                'dispo_weekend'       => ['key' => "Disponibilité type en week-end", 'namespace' => 'jumpEvent'],
                'role_ville'          => ['key' => "Le ou les rôle(s) souhaité(s) du joueur durant la ville", 'namespace' => 'jumpEvent'],
                'echangeMoyenContact' => ['key' => "Echanges sur la candidature et moyens de contact pendant la ville", 'namespace' => 'jumpEvent'],
                'motivation'          => ['key' => "Motivations du joueur :", 'namespace' => 'jumpEvent'],
                'commentaires'        => ['key' => "Commentaires du joueur :", 'namespace' => 'jumpEvent'],
                'moyenContact'        => ['key' => "Moyen de contact du joueur :", 'namespace' => 'jumpEvent'],
                'typeDiffu'           => ['key' => "Diffusion des moyens de contact :", 'namespace' => 'jumpEvent'],
                'retourOrga'          => ['key' => "Reponse au joueur :", 'namespace' => 'jumpEvent'],
                'role'                => ['key' => "Rôle souhaité :", 'namespace' => 'jumpEvent'],
                'lead'                => ['key' => "Lead souhaité(s) :", 'namespace' => 'jumpEvent'],
                'applead'             => ['key' => "Apprenti Lead souhaité(s) :", 'namespace' => 'jumpEvent'],
                'type_diff_all'       => ['key' => "Tout le monde", 'namespace' => 'jumpEvent'],
                'type_diff_lead'      => ['key' => "Uniquement aux leads/organistateurs", 'namespace' => 'jumpEvent'],
                'bloc_notes'          => ['key' => "Bloc-notes (non visible par le joueur) :", 'namespace' => 'jumpEvent'],
                'legendaire'          => ['key' => "Légendaire :", 'namespace' => 'jumpEvent'],
                'temoins'             => ['key' => "Témoin de l'arma :", 'namespace' => 'jumpEvent'],
                'fuseau'              => ['key' => "Fuseau horaire du joueur :", 'namespace' => 'jumpEvent'],
            ],
            'log'     => [
                'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
                'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
                'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
                'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
                'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
                'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
                'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
            ],
            'control' => [
                'metier_deja_choisis' => ['key' => "Vous ne pouvez pas choisir un métier que vous avez déjà choisis", 'namespace' => 'jumpEvent'],
                'metier_vide'         => ['key' => "Le métier ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'pouvoir_vide'        => ['key' => "Le pouvoir héros ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'lvl_ruine_vide'      => ['key' => "Le level de la ruine ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'dispo_vide'          => ['key' => "Vous ne pouvez pas être 'Pas co' tous le temps", 'namespace' => 'jumpEvent'],
                'role_vide'           => ['key' => "Vous devez choisir un rôle", 'namespace' => 'jumpEvent'],
                'lead_vide'           => ['key' => "Vous avez choisis d'être lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
                'applead_vide'        => ['key' => "Vous avez choisis d'être apprenti lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
            ],
            'btn'     => [
                'btn_save'   => ['key' => "Enregistrer", 'namespace' => 'jumpEvent'],
                'btn_retour' => ['key' => "Retour à la gestion du jump", 'namespace' => 'jumpEvent'],
            ],
            'confirm' => [
                'annul'     => ['key' => "Annuler", 'namespace' => 'jumpEvent'],
                'confirmer' => ['key' => "Oui", 'namespace' => 'jumpEvent'],
                'titre'     => ['key' => "Confimation", 'namespace' => 'jumpEvent'],
                'message'   => ['key' => "Êtes-vous sur de vous désister ? Il sera possible de se réinscrire.", 'namespace' => 'jumpEvent'],
            ],
            'popUp'   => $this->general_popUp(),
            'divers'  => [
                'date' => $this->z_date_format_time(),
                'et'   => ['key' => "et", 'namespace' => 'app'],
            ],
        ];
    }
    
    public function jump_coa_list_translate(): array
    {
        return [
            'table_jump' => [
                'nom'       => ['key' => "Nom du jump", 'namespace' => 'jumpEvent'],
                'orga'      => ['key' => "Organisateur(s)", 'namespace' => 'jumpEvent'],
                'date_jump' => ['key' => "Date de jump <br/> approximatif", 'namespace' => 'jumpEvent'],
                'lien'      => ['key' => "Lien vers <br/> la page coalition", 'namespace' => 'jumpEvent'],
            ],
            'popUp'      => $this->general_popUp(),
            'divers'     => [
                'et'      => ['key' => 'et', 'namespace' => 'app'],
                'date'    => $this->z_date_format_time(),
                'btn_acc' => ['key' => "Accéder", 'namespace' => 'jumpEvent'],
            ],
            'aucun'      => ['key' => "Vous êtes actuellement inscrit dans aucun jump prévu prochainnement", 'namespace' => 'jumpEvent'],
            'btn_jump'   => ['key' => "Vers les jumps", 'namespace' => 'jumpEvent'],
        ];
        
    }
    
    public function jump_coalition_translate(): array
    {
        return [
            'onglet'      => [
                'coa'         => ['key' => "Coalition", 'namespace' => 'jumpEvent'],
                'repartition' => ['key' => "Repartition", 'namespace' => 'jumpEvent'],
            ],
            'coalition'   => [
                'table'  => [
                    'titre'         => ['key' => "Coalition n° {i}", 'namespace' => 'jumpEvent'],
                    'pos'           => ['key' => "Pos", 'namespace' => 'jumpEvent'],
                    'pseudo'        => ['key' => "Pseudo", 'namespace' => 'jumpEvent'],
                    'moyen_contact' => ['key' => "Moyen de contact", 'namespace' => 'jumpEvent'],
                    'metier'        => ['key' => "Metier", 'namespace' => 'jumpEvent'],
                    'metier_pot'    => ['key' => "Metier <br/> Potentiel", 'namespace' => 'jumpEvent'],
                    'statut'        => ['key' => "Statut", 'namespace' => 'jumpEvent'],
                    'createur'      => ['key' => "Créateur", 'namespace' => 'jumpEvent'],
                    'metier_nondef' => ['key' => "Métier non défini", 'namespace' => 'jumpEvent'],
                ],
                'phrase' => [
                    'placer'   => ['key' => "Placer", 'namespace' => 'jumpEvent'],
                    'position' => ['key' => "à la position :", 'namespace' => 'jumpEvent'],
                    'phrase_1' => ['key' => "Les pseudos en rouge dans la liste déroulante sont les joueurs déjà présent en coalition", 'namespace' => 'jumpEvent'],
                    'phrase_2' => ['key' => "Si vous choississez une position dans une coalition où un joueur est déjà présent, cela effectuera un échange avec la personne que vous tentez de placer", 'namespace' => 'jumpEvent'],
                    'phrase_3' => ['key' => "Pour changer un joueur de place, il suffit de le sélectionner et de choisir une nouvelle place libre ou déjà prise afin de faire un échange.", 'namespace' => 'jumpEvent'],
                ],
                'divers' => [
                    'btn_valider'  => ['key' => "Valider", 'namespace' => 'jumpEvent'],
                    'en_coa'       => ['key' => '(en coa)', 'namespace' => 'jumpEvent'],
                    'place_statut' => ['key' => "Choix statut", 'namespace' => 'jump'],
                    'place_dispo'  => ['key' => "Choix dispo", 'namespace' => 'jump'],
                    'place_coa'    => ['key' => "Choix joueur", 'namespace' => 'jump'],
                    'place_posCoa' => ['key' => "Choix position", 'namespace' => 'jump'],
                ],
            ],
            'repartition' => [
                'table_compteur'    => [
                    'nbr'      => ['key' => "Nombre de joueur dans le jump", 'namespace' => 'jumpEvent'],
                    'tem_arma' => ['key' => "Témoin de l'Arma", 'namespace' => 'jumpEvent'],
                    'legend'   => ['key' => "Légendaire", 'namespace' => 'jumpEvent'],
                    'archi'    => ['key' => "Architecte et plus", 'namespace' => 'jumpEvent'],
                    'vp'       => ['key' => "Veilleur Pro", 'namespace' => 'jumpEvent'],
                    'nombre'   => ['key' => "Nombre", 'namespace' => 'jumpEvent'],
                ],
                'table_repartition' => [
                    'pseudo'     => ['key' => "Pseudo", 'namespace' => 'jumpEvent'],
                    'der_pouv'   => ['key' => "Dernier Pouvoir", 'namespace' => 'jumpEvent'],
                    'voeux_1'    => ['key' => "Voeux n°1", 'namespace' => 'jumpEvent'],
                    'voeux_2'    => ['key' => "Voeux n°2", 'namespace' => 'jumpEvent'],
                    'voeux_3'    => ['key' => "Voeux n°3", 'namespace' => 'jumpEvent'],
                    'metier_def' => ['key' => "Métier définitif", 'namespace' => 'jumpEvent'],
                ],
                'fige_metier'       => ['key' => "Figer les métiers", 'namespace' => 'jumpEvent'],
                'placeholder'       => ['key' => "Choix métier", 'namespace' => 'jumpEvent'],
                'extraction'        => ['key' => "Extraction des joueurs avec leur méter", 'namespace' => 'jumpEvent'],
                'extrac_btn'        => ['key' => "Générer le texte", 'namespace' => 'jumpEvent'],
                'boutonCopier'      => ['key' => "Copier le texte", 'namespace' => 'hotel'],
                'boutonCopierOk'    => ['key' => "Texte copié", 'namespace' => 'hotel'],
                'boutonCopierKO'    => ['key' => "Appuyer sur \"Ctrl + C\" pour copier", 'namespace' => 'hotel'],
            ],
            'popUp'       => $this->general_popUp(),
            'divers'      => [
                'date' => $this->z_date_format(),
            ],
        ];
        
    }
    
    public function jump_event_gestion_translate(): array
    {
        return [
            'table_jump'  => [
                'nom'        => ['key' => "Nom du jump", 'namespace' => 'jumpEvent'],
                'date_debut' => ['key' => "Date de début <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_fin'   => ['key' => "Date de fin <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_jump'  => ['key' => "Date de jump <br/> approximative", 'namespace' => 'jumpEvent'],
                'lien'       => ['key' => "Lien vers <br/> la fiche", 'namespace' => 'jumpEvent'],
                'btn'        => ['key' => "Gérer le jump", 'namespace' => 'jumpEvent'],
            ],
            'table_event' => [
                'nom'        => ['key' => "Nom de l'event", 'namespace' => 'jumpEvent'],
                'date_debut' => ['key' => "Date de début <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_fin'   => ['key' => "Date de fin <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_jump'  => ['key' => "Date début <br/> Event", 'namespace' => 'jumpEvent'],
                'lien'       => ['key' => "Lien vers <br/> la fiche", 'namespace' => 'jumpEvent'],
                'btn'        => ['key' => "Gérer l'Event", 'namespace' => 'jumpEvent'],
            ],
            'popUp'       => $this->general_popUp(),
            'divers'      => [
                'date' => $this->z_date_format_time(),
            ],
        ];
        
    }
    
    public function jump_event_list_translate(): array
    {
        return [
            'table_jump'  => [
                'nom'            => ['key' => "Nom du jump", 'namespace' => 'jumpEvent'],
                'orga'           => ['key' => "Organisateur(s)", 'namespace' => 'jumpEvent'],
                'type'           => ['key' => "Type de ville", 'namespace' => 'jumpEvent'],
                'objectif'       => ['key' => "Objectif(s)", 'namespace' => 'jumpEvent'],
                'date_fin'       => ['key' => "Date de fin <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_jump'      => ['key' => "Date de jump <br/> approximative", 'namespace' => 'jumpEvent'],
                'lien'           => ['key' => "Lien vers <br/> la fiche", 'namespace' => 'jumpEvent'],
                'statut'         => ['key' => "Statut <br/> de mon inscription", 'namespace' => 'jumpEvent'],
                'masquer'        => ['key' => "Masquer <br/> jump", 'namespace' => 'jumpEvent'],
                'commu'          => ['key' => "Communauté", 'namespace' => 'jumpEvent'],
                'btn_mod'        => ['key' => "Modifier", 'namespace' => 'jumpEvent'],
                'btn_inscrire'   => ['key' => "S'inscrire", 'namespace' => 'jumpEvent'],
                'btn_reinscrire' => ['key' => "Se réinscrire", 'namespace' => 'jumpEvent'],
            ],
            'table_event' => [
                'nom'       => ['key' => "Nom de l'event", 'namespace' => 'jumpEvent'],
                'type'      => ['key' => "Type de ville", 'namespace' => 'jumpEvent'],
                'orga'      => ['key' => "Organisateur(s)", 'namespace' => 'jumpEvent'],
                'date_fin'  => ['key' => "Date de fin <br/> inscription", 'namespace' => 'jumpEvent'],
                'date_jump' => ['key' => "Date début <br/> Event", 'namespace' => 'jumpEvent'],
                'lien'      => ['key' => "Lien vers <br/> la fiche", 'namespace' => 'jumpEvent'],
                'masquer'   => ['key' => "Masquer <br/> event", 'namespace' => 'jumpEvent'],
                'commu'     => ['key' => "Communauté", 'namespace' => 'jumpEvent'],
                'btn_event' => ['key' => "Vers Event", 'namespace' => 'jumpEvent'],
            ],
            'popUp'       => $this->general_popUp(),
            'divers'      => [
                'et'                => ['key' => 'et', 'namespace' => 'app'],
                'date'              => $this->z_date_format_time(),
                'titre_description' => ['key' => 'Description', 'namespace' => 'jumpEvent'],
                'btn_masque'        => ['key' => "Remasquer les jumps et events masqués", 'namespace' => 'jumpEvent'],
                'btn_aff'           => ['key' => "Réafficher les jumps et events masqué", 'namespace' => 'jumpEvent'],
            ],
        ];
        
    }
    
    public function jump_gestion_tranlate(): array
    {
        return [
            'titre'       => ['key' => "Gestion du jump", 'namespace' => 'jumpEvent'],
            'titre_orga'  => ['key' => "Organisateur du jump", 'namespace' => 'jumpEvent'],
            'titre_orgas' => ['key' => "Organisateurs du jump", 'namespace' => 'jumpEvent'],
            'gestion'     => [
                'info'        => ['key' => "Info principal", 'namespace' => 'jumpEvent'],
                'pseudo_orga' => ['key' => "Joueur :", 'namespace' => 'jumpEvent'],
                'add_orga'    => ['key' => "Ajouter un organisateur", 'namespace' => 'jumpEvent'],
                'orga_ko'     => ['key' => "Impossible d'ajouter d'autres organisateurs tant que vous ne serez pas inscrit à votre jump.", 'namespace' => 'jumpEvent'],
                'choix'       => ['key' => "Choix joueur.", 'namespace' => 'jumpEvent'],
                'save'        => ['key' => "Sauvegarder jump", 'namespace' => 'jumpEvent'],
                'supp'        => ['key' => "Supprimer jump", 'namespace' => 'jumpEvent'],
                'annul'       => ['key' => "Annuler", 'namespace' => 'jumpEvent'],
                'confirmer'   => ['key' => "Oui", 'namespace' => 'jumpEvent'],
                'titre'       => ['key' => "Confimation", 'namespace' => 'jumpEvent'],
                'message'     => ['key' => "Voulez-vous réellement supprimer le jump ? Attention, toute suppression est définitive, cela supprimera également toutes les candidatures.", 'namespace' => 'jumpEvent'],
                'log'         => [
                    'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
                    'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
                    'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
                    'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
                    'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
                    'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
                    'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
                ],
                'lead'        => [
                    'titre' => ['key' => "Lead", 'namespace' => 'jumpEvent'],
                    'lead'  => ['key' => "lead {lead}", 'namespace' => 'jumpEvent'],
                ],
                'app_lead'    => [
                    'titre' => ['key' => "Apprenti Lead", 'namespace' => 'jumpEvent'],
                    'lead'  => ['key' => "apprenti {lead}", 'namespace' => 'jumpEvent'],
                ],
                'candidature' => [
                    'titre'                 => ['key' => "Candidatures au jump", 'namespace' => 'jumpEvent'],
                    'compteur'              => ['key' => "Compteur des candidatures :", 'namespace' => 'jumpEvent'],
                    'nombre'                => ['key' => "Nombre", 'namespace' => 'jumpEvent'],
                    'filtre'                => ['key' => "Afficher les candidatures aux statuts suivants :", 'namespace' => 'jumpEvent'],
                    'table'                 => [
                        'pseudo'    => ['key' => "Pseudo", 'namespace' => 'jumpEvent'],
                        'bloc'      => ['key' => "Bloc Note", 'namespace' => 'jumpEvent'],
                        'date_crea' => ['key' => "Date créa.", 'namespace' => 'jumpEvent'],
                        'date_mod'  => ['key' => "Date mod.", 'namespace' => 'jumpEvent'],
                        'statut'    => ['key' => "Statut", 'namespace' => 'jumpEvent'],
                        'voeux'     => ['key' => "Voeux :", 'namespace' => 'jumpEvent'],
                        'voir'      => ['key' => "Voir candidature", 'namespace' => 'jumpEvent'],
                    ],
                    'extraction'            => ['key' => "Extraction des joueurs selon un statut", 'namespace' => 'jumpEvent'],
                    'phrase_separatrice'    => ['key' => "pour les joueurs au statut", 'namespace' => 'jumpEvent'],
                    'extrac_btn'            => ['key' => "Générer la liste", 'namespace' => 'jumpEvent'],
                    'boutonCopier'          => ['key' => "Copier le texte", 'namespace' => 'hotel'],
                    'boutonCopierOk'        => ['key' => "Texte copié", 'namespace' => 'hotel'],
                    'boutonCopierKO'        => ['key' => "Appuyer sur \"Ctrl + C\" pour copier", 'namespace' => 'hotel'],
                    'maxCandidatureAtteint' => ['key' => "Le nombre de candidature accepté atteins, vous ne pouvez pas en accepter plus.", 'namespace' => 'jumpEvent'],
                ],
                'ville'       => [
                    'titre'       => ['key' => "Ville en cours", 'namespace' => 'jumpEvent'],
                    'nom'         => ['key' => "Nom de la ville :", 'namespace' => 'jumpEvent'],
                    'jour'        => ['key' => "Jour :", 'namespace' => 'jumpEvent'],
                    'association' => ['key' => "Attention toute association est definitive.", 'namespace' => 'jumpEvent'],
                    'asso_btn'    => ['key' => "Associer ma ville actuelle à ce jump", 'namespace' => 'jumpEvent'],
                    'alerte_btn'  => ['key' => "Pensez à associer la ville après jump afin de bénéficier de tous les avantages (contact dans la page citoyen, continuer de modifier après la mort pour les leads...).", 'namespace' => 'jumpEvent'],
                ],
            ],
            'commonForm'  => $this->form_common_jump_event(),
            'jumpForm'    => $this->form_jump_jump_event(),
            'popUp'       => $this->general_popUp(),
            'divers'      => [
                'date' => $this->z_date_format_time(),
            ],
        ];
    }
    
    public function jump_inscription_tranlate(): array
    {
        return [
            'general' => [
                'type'      => ['key' => "Type de ville :", 'namespace' => 'jumpEvent'],
                'orga'      => ['key' => "Organisé par", 'namespace' => 'jumpEvent'],
                'orgas'     => ['key' => "Organisés par", 'namespace' => 'jumpEvent'],
                'objectif'  => ['key' => "Objectifs", 'namespace' => 'jumpEvent'],
                'date_fin'  => ['key' => "Date de fin d'inscription :", 'namespace' => 'jumpEvent'],
                'date_jump' => ['key' => "Date de jump approximatif :", 'namespace' => 'jumpEvent'],
                'statut'    => ['key' => "Statut de votre inscription :", 'namespace' => 'jumpEvent'],
            ],
            'form'    => $this->form_inscription_translate(),
            'log'     => [
                'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
                'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
                'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
                'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
                'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
                'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
                'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
            ],
            'control' => [
                'metier_deja_choisis' => ['key' => "Vous ne pouvez pas choisir un métier que vous avez déjà choisis", 'namespace' => 'jumpEvent'],
                'metier_vide'         => ['key' => "Le métier ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'pouvoir_vide'        => ['key' => "Le pouvoir héros ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'lvl_ruine_vide'      => ['key' => "Le level de la ruine ne peut pas être vide", 'namespace' => 'jumpEvent'],
                'dispo_vide'          => ['key' => "Vous ne pouvez pas être 'Pas co' tous le temps", 'namespace' => 'jumpEvent'],
                'role_vide'           => ['key' => "Vous devez choisir un rôle", 'namespace' => 'jumpEvent'],
                'lead_vide'           => ['key' => "Vous avez choisis d'être lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
                'applead_vide'        => ['key' => "Vous avez choisis d'être apprenti lead, mais la spécialité est vide", 'namespace' => 'jumpEvent'],
            ],
            'btn'     => [
                'btn_save'   => ['key' => "S'enregistrer", 'namespace' => 'jumpEvent'],
                'btn_mod'    => ['key' => "Modifier", 'namespace' => 'jumpEvent'],
                'btn_retour' => ['key' => "Retour", 'namespace' => 'jumpEvent'],
                'btn_desit'  => ['key' => "Se désister", 'namespace' => 'jumpEvent'],
                'btn_reins'  => ['key' => "Se réinscrire", 'namespace' => 'jumpEvent'],
            ],
            'confirm' => [
                'annul'     => ['key' => "Annuler", 'namespace' => 'jumpEvent'],
                'confirmer' => ['key' => "Oui", 'namespace' => 'jumpEvent'],
                'titre'     => ['key' => "Confimation", 'namespace' => 'jumpEvent'],
                'message'   => ['key' => "Êtes-vous sur de vous désister ? Il sera possible de se réinscrire.", 'namespace' => 'jumpEvent'],
            ],
            'popUp'   => $this->general_popUp(),
            'divers'  => [
                'date' => $this->z_date_format_time(),
                'et'   => ['key' => "et", 'namespace' => 'app'],
            ],
        ];
    }
    
    public function log_translate(): array
    {
        return [
            'log'         => ['key' => "Historique des modifications", 'namespace' => 'jumpEvent'],
            'date'        => ['key' => "Date", 'namespace' => 'jumpEvent'],
            'user'        => ['key' => "Qui ?", 'namespace' => 'jumpEvent'],
            'event'       => ['key' => "Evénement", 'namespace' => 'jumpEvent'],
            'valeurAvant' => ['key' => "Valeur avant", 'namespace' => 'jumpEvent'],
            'valeurApres' => ['key' => "Valeur après", 'namespace' => 'jumpEvent'],
            'orga'        => ['key' => "Organisateur", 'namespace' => 'jumpEvent'],
        ];
    }
    
    public function news_translate(): array
    {
        return [
            'bonjour'    => ['key' => "Bonjour à tous,", 'namespace' => 'version'],
            'modif'      => ['key' => "Le site a été mis à jour avec les modifications suivantes :", 'namespace' => 'version'],
            'aurevoir'   => ['key' => "À très bientôt et bon jeu !", 'namespace' => 'version'],
            'date'       => ['key' => "Par <em>{auteur}</em>, le {dateAjout}", 'namespace' => 'version'],
            'date_mod'   => ['key' => "Modifiée le {dateModif}", 'namespace' => 'version'],
            'changelog'  => $this->changelog_translate(),
            'dateFormat' => $this->z_date_format_time(),
        ];
    }
    
    public function outils_chantiers_translate(): array
    {
        return [
            'chantier'           => $this->chantier_list_translate(),
            'onglet'             => [
                'outilsChantiers' => ['key' => "Outils chantiers", 'namespace' => 'outils'],
                'chantiers'       => ['key' => "Liste chantiers", 'namespace' => 'outils'],
            ],
            'tabOutilsChantiers' => [
                'action'     => [
                    'enregistrer' => ['key' => 'Enregistrer', 'namespace' => 'outils'],
                    'annuler'     => ['key' => 'Annuler', 'namespace' => 'outils'],
                ],
                'onglet'     => [
                    'chantier'   => ['key' => 'Chantiers', 'namespace' => 'outils'],
                    'repa'       => ['key' => 'Réparation Armes', 'namespace' => 'outils'],
                    'habitation' => ['key' => 'Up Habitations', 'namespace' => 'outils'],
                    'evolution'  => ['key' => 'Evolutions', 'namespace' => 'outils'],
                    'gain_pa'    => ['key' => 'PA ouvriers', 'namespace' => 'outils'],
                    'ressource'  => ['key' => 'Ressources', 'namespace' => 'outils'],
                ],
                'chantier'   => [
                    'nom'            => ['key' => 'Chantier', 'namespace' => 'outils'],
                    'bde'            => ['key' => 'Pour les bannis', 'namespace' => 'outils'],
                    'finir'          => ['key' => 'Finir', 'namespace' => 'outils'],
                    'pa_r'           => ['key' => 'PA restant au chantier', 'namespace' => 'outils'],
                    'pa_m'           => ['key' => 'PA à laisser au chantier', 'namespace' => 'outils'],
                    'def'            => ['key' => 'Def chantier', 'namespace' => 'outils'],
                    'priorite'       => ['key' => 'Priorité', 'namespace' => 'outils'],
                    'eco_ressources' => ['key' => 'Mode économie de ressource :', 'namespace' => 'outils'],
                    'util_bde'       => ['key' => 'Utilisation du BDE', 'namespace' => 'outils'],
                    'pres_scie'      => ['key' => 'Presence de la scie', 'namespace' => 'outils'],
                    'add_chantier'   => ['key' => 'Ajouter un chantier', 'namespace' => 'outils'],
                ],
                'habitation' => [
                    'citoyen'    => ['key' => 'Citoyen', 'namespace' => 'outils'],
                    'lvlAmelio'  => ['key' => 'Lvl Amenagement', 'namespace' => 'outils'],
                    'hab'        => ['key' => 'Habitation', 'namespace' => 'outils'],
                    'amelio'     => ['key' => 'Amelioration', 'namespace' => 'outils'],
                    'lvl'        => ['key' => 'Lvl', 'namespace' => 'outils'],
                    'add_up_hab' => ['key' => 'Ajouter un up habitation', 'namespace' => 'outils'],
                    'add_ame'    => ['key' => 'Ajouter une amélioration', 'namespace' => 'outils'],
                    'up_trop'    => ['key' => "Vous ne pouvez pas faire construire plus d'un level habitation par jour.", 'namespace' => 'outils'],
                    'down_up'    => ['key' => 'Vous ne pouvez pas démonter une maison.', 'namespace' => 'outils'],
                    'hab_max'    => ['key' => "Tous les citoyens ont actuellement monté leur habitation au maximum de ce qu'il est possible.", 'namespace' => 'outils'],
                    'up_hab'     => ['key' => "Gestion habitation", 'namespace' => 'outils'],
                    'up_ame'     => ['key' => "Gestion amelioration", 'namespace' => 'outils'],
                ],
                'reparation' => [
                    'brico_vert' => ['key' => 'Utiliser des kits verts', 'namespace' => 'outils'],
                    'nbr_vert'   => ['key' => 'Nombre de kit vert à utiliser', 'namespace' => 'outils'],
                    'nom_objet'  => ['key' => 'Armes', 'namespace' => 'outils'],
                    'nb_casse'   => ['key' => 'Nombre cassé', 'namespace' => 'outils'],
                    'nb_a_repa'  => ['key' => 'Nombre à réparer', 'namespace' => 'outils'],
                    'add_repa'   => ['key' => 'Ajouter une arme à réparer', 'namespace' => 'outils'],
                ],
                'evolution'  => [
                
                ],
                'gain_pa'    => [
                    'nbr_ouvrier'       => ['key' => "Nombre d'ouvrier", 'namespace' => 'outils'],
                    'nbr_campeur'       => ['key' => 'Nombre de campeur', 'namespace' => 'outils'],
                    'nbr_mort'          => ['key' => 'Nombre de mort', 'namespace' => 'outils'],
                    'ratio_eau'         => ['key' => 'Rationnement en eau', 'namespace' => 'outils'],
                    'ratio_nourriture'  => ['key' => 'Rationnement en nourriture', 'namespace' => 'outils'],
                    'choix_nourriture'  => ['key' => 'Choix nourriture :', 'namespace' => 'outils'],
                    'max_eau'           => ['key' => 'Max eau à utiliser', 'namespace' => 'outils'],
                    'max_nourriture'    => ['key' => 'Max nourriture à utiliser', 'namespace' => 'outils'],
                    'nbr_alcool_expe'   => ['key' => "Nombre d'alcool en expé", 'namespace' => 'outils'],
                    'nbr_alcool_veille' => ['key' => "Nombre d'alcool en veille", 'namespace' => 'outils'],
                    'nbr_drogue_expe'   => ['key' => "Nombre de drogue en expé", 'namespace' => 'outils'],
                    'nbr_drogue_veille' => ['key' => "Nombre de drogue en veille", 'namespace' => 'outils'],
                    'nbr_alcooldrogue'  => ['key' => "Nombre citoyen drogué et alcoolisé", 'namespace' => 'outils'],
                    'nbr_PA_leg'        => ['key' => "PA légendaire pour chantier", 'namespace' => 'outils'],
                    'nbr_PA_fao'        => ['key' => "PA pour la FAO", 'namespace' => 'outils'],
                ],
                'recap'      => [
                    'defense_act'   => ['key' => 'Défense actuelle', 'namespace' => 'outils'],
                    'defanse_prevu' => ['key' => 'Défense future', 'namespace' => 'outils'],
                    'defense'       => [
                        'recapVille' => ['key' => "Récapitulatif défense", 'namespace' => 'ville'],
                        'muraille'   => ['key' => "Muraille :", 'namespace' => 'ville'],
                        'chantiers'  => ['key' => "Chantiers :", 'namespace' => 'ville'],
                        'objetDef'   => ['key' => "Objets de défense :", 'namespace' => 'ville'],
                        'objet'      => ['key' => "objet", 'namespace' => 'ville'],
                        'objets'     => ['key' => "objets", 'namespace' => 'ville'],
                        'maisons'    => ['key' => "Maisons des citoyens :", 'namespace' => 'ville'],
                        'gardiens'   => ['key' => "Gardiens :", 'namespace' => 'ville'],
                        'veilleurs'  => ['key' => "Veilleurs :", 'namespace' => 'ville'],
                        'tempos'     => ['key' => "Tempos :", 'namespace' => 'ville'],
                        'ame'        => ['key' => "Âmes :", 'namespace' => 'ville'],
                        'morts'      => ['key' => "Morts :", 'namespace' => 'ville'],
                        'bonusSd'    => ['key' => "Bonus SD (+{bonusSd}%) :", 'namespace' => 'ville'],
                        'total'      => ['key' => "Total", 'namespace' => 'ville'],
                    ],
                    'recap_depense' => [
                        'cout_pa'          => ['key' => 'Cout PA total', 'namespace' => 'outils'],
                        'total_pa'         => ['key' => 'Total PA dispo', 'namespace' => 'outils'],
                        'pa_chantier'      => ['key' => 'Cout PA total chantier', 'namespace' => 'outils'],
                        'sub_pa_chantier'  => ['key' => 'total', 'namespace' => 'outils'],
                        'def_chantier'     => ['key' => 'Gain def chantier', 'namespace' => 'outils'],
                        'sub_def_chantier' => ['key' => 'total', 'namespace' => 'outils'],
                        'pa_habitation'    => ['key' => 'Cout PA up Habitation', 'namespace' => 'outils'],
                        'pa_repa'          => ['key' => "Cout PA réparation d'arme", 'namespace' => 'outils'],
                        'pa_tranfo'        => ['key' => "Cout PA de tranformation", 'namespace' => 'outils'],
                        'pa_tdga'          => ['key' => "PA dans la Tour des Gardiens", 'namespace' => 'outils'],
                    ],
                ],
            ],
        
        ];
    }
    
    public function outils_decharge_translate(): array
    {
        
        
        return [
            'table'  => [
                'items'   => ['key' => "Items <br/> décharges", 'namespace' => 'outils'],
                'def'     => ['key' => "Def / <br/> objets", 'namespace' => 'outils'],
                'nbr'     => ['key' => "Nbr <br/> banque", 'namespace' => 'outils'],
                'estim'   => ['key' => "Estim. <br/> suppl.", 'namespace' => 'outils'],
                'utils'   => ['key' => "Util. <br/> réelle", 'namespace' => 'outils'],
                'def_app' => ['key' => "Def <br/> apportée", 'namespace' => 'outils'],
                'def_tot' => ['key' => "Total def", 'namespace' => 'outils'],
            ],
            'phrase' => [
                'creer' => ['key' => "Créé par <strong>{createur}</strong> le <strong>{date}</strong>", 'namespace' => 'outils'],
                'modif' => ['key' => "Modifiée par <strong>{createur}</strong> le <strong>{date}</strong>", 'namespace' => 'outils'],
            ],
            'bouton' => [
                'sauvegarder' => ['key' => "Sauvegarder", 'namespace' => 'outils'],
                'annuler'     => ['key' => "Annuler", 'namespace' => 'outils'],
            ],
            'popUp'  => $this->general_popUp(),
            'divers' => [
                'dateFormat' => $this->z_date_format_time(),
                'et'         => ['key' => "et", 'namespace' => 'app'],
            ],
        ];
    }
    
    public function outils_expedition(): array
    {
        return [
            'date'               => $this->z_date_format_time(),
            'jour'               => ['key' => "Jour", 'namespace' => 'ville'],
            'stock'              => ['key' => "Gérer le stock", 'namespace' => 'ville'],
            'maj_ok'             => ['key' => "Mise à jour ok !", 'namespace' => 'ville'],
            'supp_ok'            => ['key' => "Suppresion ok !", 'namespace' => 'ville'],
            'valider_consigne'   => ['key' => "Valider", 'namespace' => 'ville'],
            'annuler_consigne'   => ['key' => "Annuler", 'namespace' => 'ville'],
            'compteurs'          => ['key' => "Compteurs métiers", 'namespace' => 'ville'],
            'nbr'                => ['key' => "Nombres", 'namespace' => 'ville'],
            'nonDef'             => ['key' => "Non-définis", 'namespace' => 'ville'],
            'help_cpt_1'         => ['key' => "En <span className='text-primary'>bleu</span> : limite non atteinte.", 'namespace' => 'ville'],
            'help_cpt_2'         => ['key' => "En <span className='text-success'>vert</span> : limite atteinte.", 'namespace' => 'ville'],
            'help_cpt_3'         => ['key' => "En <span className='text-danger'>rouge</span> : limite dépassée.", 'namespace' => 'ville'],
            'max_job'            => ['key' => "Max :", 'namespace' => 'ville'],
            'onglet'             => [
                'list'    => ['key' => "Liste des expeditions", 'namespace' => 'ville'],
                'gestion' => ['key' => "Création/Modification expéditions", 'namespace' => 'ville'],
            ],
            'soif'               => [
                'oui' => ['key' => "Oui", 'namespace' => 'app'],
                'non' => ['key' => "Non", 'namespace' => 'app'],
            ],
            'tabExpe'            => [
                'nom'              => ['key' => "Nom", 'namespace' => 'ville'],
                'nbr'              => ['key' => "Nombre expéditionnaire", 'namespace' => 'ville'],
                'nbr_part'         => ['key' => "Nombre partie", 'namespace' => 'ville'],
                'tot_pa'           => ['key' => "Total PA", 'namespace' => 'ville'],
                'type'             => ['key' => "Type", 'namespace' => 'ville'],
                'createur'         => ['key' => "Créateur", 'namespace' => 'ville'],
                'modif'            => ['key' => "Modificateur", 'namespace' => 'ville'],
                'preinscrit'       => ['key' => "Pré-inscrit", 'namespace' => 'ville'],
                'action'           => ['key' => "Action", 'namespace' => 'ville'],
                'editer'           => ['key' => "Editer", 'namespace' => 'ville'],
                'supprimer'        => ['key' => "Supprimer", 'namespace' => 'ville'],
                'up'               => ['key' => "Augmenter priorité", 'namespace' => 'ville'],
                'down'             => ['key' => "Descendre priorité", 'namespace' => 'ville'],
                'statut'           => ['key' => "Statut", 'namespace' => 'ville'],
                'ouvert'           => ['key' => "Ouvert", 'namespace' => 'ville'],
                'fermer'           => ['key' => "Stop", 'namespace' => 'ville'],
                'legend_expe'      => ['key' => "Expéditions", 'namespace' => 'ville'],
                'sauvegarder'      => ['key' => "Sauvegarder l'ordre", 'namespace' => 'ville'],
                'expeditionnaire'  => ['key' => "{nombre} expéditionnaire(s)", 'namespace' => 'ville'],
                'choix_jour'       => ['key' => "Dupliquer l'expédition pour :", 'namespace' => 'ville'],
                'jour'             => ['key' => "jour", 'namespace' => 'ville'],
                'placeholder_jour' => ['key' => "choix jour", 'namespace' => 'ville'],
            ],
            'ouvrier'            => [
                'legend_ouvrier' => ['key' => "Ouvriers", 'namespace' => 'ville'],
                'pseudo'         => ['key' => "Pseudo", 'namespace' => 'ville'],
                'job'            => ['key' => "Métier", 'namespace' => 'ville'],
                'soif'           => ['key' => "Soif", 'namespace' => 'ville'],
                'sac'            => ['key' => "Sac", 'namespace' => 'ville'],
                'consigne'       => ['key' => "Consigne", 'namespace' => 'ville'],
                'mod_consigne'   => ['key' => "Modifier consigne", 'namespace' => 'ville'],
                'action'         => ['key' => "Action", 'namespace' => 'ville'],
                'fao'            => ['key' => "Direction FAO :", 'namespace' => 'ville'],
                'save'           => ['key' => "Sauvegarder les ouvriers", 'namespace' => 'ville'],
                'cancel'         => ['key' => "Annuler modification", 'namespace' => 'ville'],
                'add'            => ['key' => "Ajouter un ouvrier", 'namespace' => 'ville'],
                'creation'       => ['key' => "Créé par <strong>{createur}</strong> le <em>{date}</em> ", 'namespace' => 'ville'],
                'modif'          => ['key' => "Modifié par <strong>{modificateur}</strong> le <em>{date}</em> ", 'namespace' => 'ville'],
                'bannis'         => ['key' => "Banni ?", 'namespace' => 'ville'],
                'ouvrir'         => ['key' => "Statut des places ouvrières :", 'namespace' => 'ville'],
            ],
            'creation'           => [
                'titre_expe'  => ['key' => "Expédition", 'namespace' => 'ville'],
                'titre_part'  => ['key' => "Partie ", 'namespace' => 'ville'],
                'type'        => ['key' => "Type expédition :", 'namespace' => 'ville'],
                'carac'       => ['key' => "Caractéristiques", 'namespace' => 'ville'],
                'pdc_min'     => ['key' => "PDC mini :", 'namespace' => 'ville'],
                'nbr_part'    => ['key' => "Nombre de partie", 'namespace' => 'ville'],
                'nom'         => ['key' => "Nom expédition :", 'namespace' => 'ville'],
                'trace'       => ['key' => "Tracé d'expédition :", 'namespace' => 'ville'],
                'description' => ['key' => "Description/Commentaire :", 'namespace' => 'ville'],
                'creation'    => ['key' => "Crée par <strong>{createur}</strong> le <em>{date}</em> ", 'namespace' => 'ville'],
                'modif'       => ['key' => "Modifiée par <strong>{modificateur}</strong> le <em>{date}</em> ", 'namespace' => 'ville'],
                'phrase'      => ['key' => "Phrase pré-faite :", 'namespace' => 'ville'],
                'choix'       => ['key' => "Choisir une phrase", 'namespace' => 'ville'],
                'tab_expe'    => [
                    'job'             => ['key' => "Métier", 'namespace' => 'ville'],
                    'pseudo'          => ['key' => "Pseudo", 'namespace' => 'ville'],
                    'pa_base'         => ['key' => "PA base", 'namespace' => 'ville'],
                    'soif'            => ['key' => "Soif", 'namespace' => 'ville'],
                    'sac'             => ['key' => "Sac", 'namespace' => 'ville'],
                    'action'          => ['key' => "Action héroïque", 'namespace' => 'ville'],
                    'ajout_exp'       => ['key' => "Ajouter un expéditionnaire", 'namespace' => 'ville'],
                    'consigne'        => ['key' => "Consignes expéditions", 'namespace' => 'ville'],
                    'mod_consigne'    => ['key' => "Modifier consigne", 'namespace' => 'ville'],
                    'mod_description' => ['key' => "Modifier description", 'namespace' => 'ville'],
                    'case'            => ['key' => "Case consigne", 'namespace' => 'ville'],
                    'ajout_cons'      => ['key' => "Ajouter une consigne", 'namespace' => 'ville'],
                    'ajout_part'      => ['key' => "Ajouter une partie", 'namespace' => 'ville'],
                    'action_gest'     => ['key' => "Action", 'namespace' => 'ville'],
                    'mod_sac'         => ['key' => "Modifier sac", 'namespace' => 'ville'],
                    'bannis'          => ['key' => "Banni ?", 'namespace' => 'ville'],
                ],
                'liste_trace' => [
                    'carte'  => ['key' => "Carte - Gest'Hordes", 'namespace' => 'ville'],
                    'outils' => ['key' => "Outils Expédition", 'namespace' => 'ville'],
                    'biblio' => ['key' => "Bibliothèque", 'namespace' => 'ville'],
                ],
            ],
            'confirmation'       => [
                'message'      => ['key' => "Êtes-vous sûr de supprimer définitivement cette expédition ?", 'namespace' => 'ville'],
                'titre'        => ['key' => "Confirmation de suppression", 'namespace' => 'ville'],
                'annuler'      => ['key' => "Annuler", 'namespace' => 'ville'],
                'confirmation' => ['key' => "Confirmer", 'namespace' => 'ville'],
            ],
            'appercu'            => [
                'preinscrit_expe' => ['key' => "Préinscrits sur cette expédition :", 'namespace' => 'hotel'],
                'pdc'             => ['key' => "PDC", 'namespace' => 'hotel'],
                'pseudo'          => ['key' => "Pseudo", 'namespace' => 'hotel'],
                'sac'             => ['key' => "Sac", 'namespace' => 'hotel'],
                'action_h'        => ['key' => "Action héroïque", 'namespace' => 'hotel'],
                'consignes'       => ['key' => "Consignes", 'namespace' => 'hotel'],
                'consigne'        => ['key' => "Consigne", 'namespace' => 'hotel'],
                'description'     => ['key' => "Description", 'namespace' => 'hotel'],
            ],
            'creer'              => ['key' => "Créer une nouvelle expedition", 'namespace' => 'ville'],
            'annuler'            => ['key' => "Annuler l'expédition", 'namespace' => 'ville'],
            'annuler_return'     => ['key' => "Annuler l'édition et revenir à la liste", 'namespace' => 'ville'],
            'return'             => ['key' => "Retourner à la liste", 'namespace' => 'ville'],
            'modifications'      => ['key' => "Enregistrer les modifications de l'expédition", 'namespace' => 'ville'],
            'total'              => ['key' => "Total de places créées", 'namespace' => 'ville'],
            'fermer'             => ['key' => "Fermer", 'namespace' => 'ville'],
            'generer'            => ['key' => "Générer liste pré-inscrit", 'namespace' => 'ville'],
            'gen_fao'            => ['key' => "{number} préinscrit(s) en FAO :", 'namespace' => 'ville'],
            'gen_exp'            => ['key' => "{number} préinscrit(s) en expédition :", 'namespace' => 'ville'],
            'boutonCopier'       => ['key' => "Copier le texte", 'namespace' => 'hotel'],
            'boutonFermer'       => ['key' => "Fermer liste", 'namespace' => 'hotel'],
            'boutonCopierOk'     => ['key' => "Texte copié", 'namespace' => 'hotel'],
            'boutonCopierKO'     => ['key' => "Appuyer sur \"Ctrl + C\" pour copier", 'namespace' => 'hotel'],
            'appercuExpe'        => ['key' => "Aperçu du tracé", 'namespace' => 'hotel'],
            'confirmationpseudo' => [
                'message' => ['key' => "Êtes-vous sur de pré-inscrire ce citoyen à nouveau ?", 'namespace' => 'hotel'],
                'titre'   => ['key' => "Attention !", 'namespace' => 'hotel'],
            ],
        ];
    }
    
    public function outils_reparation_translate(): array
    {
        
        
        return [
            'table'  => [
                'def_actu' => ['key' => "Def <br/> actuelle", 'namespace' => 'outils'],
                'def_min'  => ['key' => "Def <br/> min", 'namespace' => 'outils'],
                'def_max'  => ['key' => "Def <br/> max", 'namespace' => 'outils'],
                'pv_actu'  => ['key' => "PV <br/> actuel", 'namespace' => 'outils'],
                'pv_min'   => ['key' => "PV <br/> min", 'namespace' => 'outils'],
                'pv_max'   => ['key' => "Pv <br/> max", 'namespace' => 'outils'],
                'ratio'    => ['key' => "Ratio <br/> def/PA", 'namespace' => 'outils'],
                'choix'    => ['key' => "Choix répa", 'namespace' => 'outils'],
                
                'cout'         => ['key' => "PA répa", 'namespace' => 'outils'],
                'gain'         => ['key' => "Gain def", 'namespace' => 'outils'],
                'all0'         => ['key' => "Tout 0%", 'namespace' => 'outils'],
                'all70'        => ['key' => "Tout 70%+1", 'namespace' => 'outils'],
                'conf70'       => ['key' => "Confort 70%+1 Def 99%/100%", 'namespace' => 'outils'],
                'conf70Def100' => ['key' => "Confort 70%+1 Def 100%", 'namespace' => 'outils'],
                'all99'        => ['key' => "Tout 99%/100%", 'namespace' => 'outils'],
                'all100'       => ['key' => "Tout 100%", 'namespace' => 'outils'],
                'total'        => ['key' => "Total", 'namespace' => 'outils'],
                'gain_def'     => ['key' => "Gain def", 'namespace' => 'outils'],
                'def_avant'    => ['key' => "Def avant", 'namespace' => 'outils'],
                'def_apres'    => ['key' => "Def après", 'namespace' => 'outils'],
            ],
            'choix'  => [
                'pas_repa'    => ['key' => "Ne pas réparer", 'namespace' => 'outils'],
                'choix_perso' => ['key' => "Repa Perso", 'namespace' => 'outils'],
                'two_step'    => ['key' => "Cochez pour générer un texte avec tous les chantiers à 70% (quelque soit le choix de réparation) puis en classant par ordre de rentabilité de réparation pour ceux à 99/100% lorsque les 70%+1 sont terminés", 'namespace' => 'outils'],
            ],
            'phrase' => [
                'creer'         => ['key' => "Plan de réparation créé par <strong>{createur}</strong > le {date}", 'namespace' => 'outils'],
                'modif'         => ['key' => "Modifié par <strong>{modificateur}</strong > le {date}.", 'namespace' => 'outils'],
                'confirmation1' => ['key' => "Attention, en enregistrant vous effacerez le choix qui a été fait auparavant, êtes - vous sûr de vouloir enregistrer les modifications ? ", 'namespace' => 'outils'],
                'confirmation2' => ['key' => 'Si vous souhaitez simplement regénérer un texte sans sauvegarder, utiliser le bouton "Générer le texte".', 'namespace' => 'outils'],
            ],
            'bouton' => [
                'sauvegarder' => ['key' => "Sauvegarder et générer le texte", 'namespace' => 'outils'],
                'generer'     => ['key' => "Générer le texte", 'namespace' => 'outils'],
                'annuler'     => ['key' => "Annuler", 'namespace' => 'outils'],
                'confirmer'   => ['key' => "Confirmer", 'namespace' => 'outils'],
            ],
            'popUp'  => $this->general_popUp(),
            'divers' => [
                'dateFormat'     => $this->z_date_format_time(),
                'boutonCopier'   => ['key' => "Copier le texte", 'namespace' => 'hotel'],
                'boutonCopierOk' => ['key' => "Texte copié", 'namespace' => 'hotel'],
                'boutonCopierKO' => ['key' => "Appuyer sur \"Ctrl + C\" pour copier", 'namespace' => 'hotel'],
            ],
        ];
    }
    
    public function plans_chantiers_translate(): array
    {
        return [
            'plansChantier' => [
                'phrase'            => ['key' => "Mis à jour par <strong>{userMaj}</strong> le <strong>{dateMaj}</strong>.", 'namespace' => 'hotel'],
                'ruine'             => ['key' => "Plans de chantier dans les ruines", 'namespace' => 'hotel'],
                'boutonRuine'       => ['key' => "Cliquer pour <span>ouvrir</span>", 'namespace' => 'hotel'],
                'boutonRuineFermer' => ['key' => "Cliquer pour <span>fermer</span>", 'namespace' => 'hotel'],
                'ouvrir'            => ['key' => "ouvrir", 'namespace' => 'hotel'],
                'fermer'            => ['key' => "fermer", 'namespace' => 'hotel'],
                'bunker'            => ['key' => "Bunker abandonné", 'namespace' => 'bats'],
                'hopital'           => ['key' => "Hôpital abandonné", 'namespace' => 'bats'],
                'hotel'             => ['key' => "Hôtel abandonné", 'namespace' => 'hotel'],
                'form'              => [
                    'sauver'  => ['key' => "Sauvegarder", 'namespace' => 'hotel'],
                    'annuler' => ['key' => "Annuler modification", 'namespace' => 'hotel'],
                ],
            ],
            'chantier'      => $this->chantier_list_translate(),
            'onglet'        => [
                'plans'     => ['key' => "Plans de chantier", 'namespace' => 'hotel'],
                'chantiers' => ['key' => "Chantiers", 'namespace' => 'hotel'],
            ],
            'alert'         => ['key' => "Une modification a été effectué, pensez à sauvegarder", 'namespace' => 'hotel'],
        ];
    }
    
    public function statistiques_translate(): array
    {
        return [
            'legend' => [
                'estim_mini'        => ['key' => 'Estimation Mini', 'namespace' => 'hotel'],
                'estim_max'         => ['key' => 'Estimation Max', 'namespace' => 'hotel'],
                'atk_saison'        => ['key' => 'Attaque de la saison', 'namespace' => 'hotel'],
                'atk_reelle'        => ['key' => 'Attaque réelle', 'namespace' => 'hotel'],
                'def_atk'           => ['key' => "Défense à l'Attaque", 'namespace' => 'hotel'],
                'atk_min_theo'      => ['key' => "Attaque théorique Mini", 'namespace' => 'hotel'],
                'atk_max_theo'      => ['key' => "Attaque théorique Maxi", 'namespace' => 'hotel'],
                'titre_stat'        => ['key' => "Statistiques des attaques et estimations de l'attaque de la ville", 'namespace' => 'hotel'],
                'total'             => ['key' => "Défense totale", 'namespace' => 'hotel'],
                'chantier_ame'      => ['key' => 'Chantier + évolution + âme', 'namespace' => 'hotel'],
                'objet_def'         => ['key' => 'Objet de défense', 'namespace' => 'hotel'],
                'maison_cit'        => ['key' => 'Maison de citoyen', 'namespace' => 'hotel'],
                'gardien'           => ['key' => 'Gardien', 'namespace' => 'hotel'],
                'veille'            => ['key' => 'Veille', 'namespace' => 'hotel'],
                'tempo'             => ['key' => 'Temporaire', 'namespace' => 'hotel'],
                'titre_detail_stat' => ['key' => 'Statistiques des défenses  de la ville', 'namespace' => 'hotel'],
            ],
            'filtre' => [
                'bouton_reinit' => ['key' => 'Réinitialiser', 'namespace' => 'hotel'],
                'jour_min'      => ['key' => 'Jour mini :', 'namespace' => 'hotel'],
                'jour_max'      => ['key' => 'Jour max :', 'namespace' => 'hotel'],
            ],
        ];
    }
    
    public function tdg_translate(): array
    {
        return [
            'jour'          => ['key' => 'Jour  :', 'namespace' => 'hotel'],
            'estim_jour'    => ['key' => 'Estimations du jour :', 'namespace' => 'hotel'],
            'planif'        => ['key' => 'Estimations du planificateur :', 'namespace' => 'hotel'],
            'planif_const'  => ['key' => 'Le planificateur doit être construit pour avoir accés à cette zone . ', 'namespace' => 'hotel'],
            'atk_vise'      => ['key' => 'Attaque visée :', 'namespace' => 'hotel'],
            'atk_theo'      => ['key' => 'Attaque théorique J{jour}', 'namespace' => 'hotel'],
            'min'           => ['key' => 'Min :', 'namespace' => 'hotel'],
            'max'           => ['key' => 'Max :', 'namespace' => 'hotel'],
            'enreg'         => ['key' => 'Enregistrer les estimations', 'namespace' => 'hotel'],
            'calcul'        => ['key' => 'Calculer', 'namespace' => 'hotel'],
            'sauvegarder'   => ['key' => "Sauvegarder l'attaque", 'namespace' => 'hotel'],
            'annuler'       => ['key' => 'Annuler', 'namespace' => 'hotel'],
            'avertissement' => ['key' => "Pensez bien à renseigner l'attaque visée et à cliquer sur sauvegarder afin d'utiliser les outils(veille et chantier) pleinement .", 'namespace' => 'hotel'],
            'evol'          => ['key' => "Calcul de l'attaque à venir", 'namespace' => 'hotel'],
            'add_estim'     => ['key' => 'Ajouter une estimation', 'namespace' => 'hotel'],
            'edit_estim'    => ['key' => "Editer l'estimation", 'namespace' => 'hotel'],
            'save_estim'    => ['key' => "Enregistrer l'estimation", 'namespace' => 'hotel'],
            'supp_estim'    => ['key' => "Supprimer l'estimation", 'namespace' => 'hotel'],
            'mode_form'     => ['key' => "Mode formulaire", 'namespace' => 'hotel'],
            'mode_list'     => ['key' => "Mode liste", 'namespace' => 'hotel'],
            'placeholder'   => [
                'pct'  => ['key' => 'qualité', 'namespace' => 'hotel'],
                'min'  => ['key' => 'min', 'namespace' => 'hotel'],
                'max'  => ['key' => 'max', 'namespace' => 'hotel'],
                'text' => ['key' => 'Saisir sous le format 99% : 99999 - 99999', 'namespace' => 'hotel'],
            ],
            'popUp'         => $this->general_popUp(),
            'controle'      => [
                'estim_vide'            => ['key' => "Il n'est pas possible de sauvegarder sans avoir mis d'estimation . ", 'namespace' => 'hotel'],
                'estim_min_erreur'      => ['key' => 'Il y a une erreur avec vos estimations mini, elles ne sont pas correctement croissantes ou égales.', 'namespace' => 'hotel'],
                'estim_max_erreur'      => ['key' => 'Il y a une erreur avec vos estimations maxi, elles ne sont pas correctement décroissantes ou égales.', 'namespace' => 'hotel'],
                'estim_min_max_erreur'  => ['key' => "L'estimation mini ne peut pas être supérieur au maximum.", 'namespace' => 'hotel'],
                'planif_min_erreur'     => ['key' => 'Il y a une erreur avec vos estimations du planificateur mini, elles ne sont pas correctement croissantes ou égales . ', 'namespace' => 'hotel'],
                'planif_max_erreur'     => ['key' => 'Il y a une erreur avec vos estimations du planificateur maxi, elles ne sont pas correctement décroissantes ou égales . ', 'namespace' => 'hotel'],
                'planif_min_max_erreur' => ['key' => "L'estimation du planificateur mini ne peut pas être supérieur au maximum . ", 'namespace' => 'hotel'],
            
            ],
        ];
    }
    
    public function z_date_format(): array
    {
        return ['key' => "dd / MM", 'namespace' => 'app'];
    }
    
    public function z_date_format_time(): array
    {
        return ['key' => "dd / MM / yyyy à H:mm", 'namespace' => 'app'];
    }
    
    private function form_common_jump_event(): array
    {
        return [
            'banniere'    => [
                'titre'  => ['key' => "Bannière :", 'namespace' => 'jumpEvent'],
                'aucune' => ['key' => "Aucune bannière", 'namespace' => 'jumpEvent'],
                'taille' => ['key' => "Votre image ne doit pas faire plus de {
        taille}.", 'namespace' => 'jumpEvent'],
                'mod'    => ['key' => "Modifier la bannière", 'namespace' => 'jumpEvent'],
                'add'    => ['key' => "Ajouter une bannière", 'namespace' => 'jumpEvent'],
                'supp'   => ['key' => "Supprimer la bannière", 'namespace' => 'jumpEvent'],
                'error'  => [
                    'nombre' => ['key' => "Veuillez sélectionner un seul fichier . ", 'namespace' => 'jumpEvent'],
                    'taille' => ['key' => "Ce fichier est trop volumineux . ", 'namespace' => 'jumpEvent'],
                    'type'   => ['key' => "Ce format de fichier n'est pas pris en charge.", 'namespace' => 'jumpEvent'],
                ],
                'succes' => ['key' => "Image correctement ajouté.", 'namespace' => 'jumpEvent'],
            ],
            'typeVille'   => ['key' => "Type de ville :", 'namespace' => 'jumpEvent'],
            'description' => ['key' => "Descriptions :", 'namespace' => 'jumpEvent'],
            'commu'       => ['key' => "Communauté :", 'namespace' => 'jumpEvent'],
        ];
    }
    
    private function form_event_jump_event(): array
    {
        return [
            'nom'                  => ['key' => "Nom de l'event :", 'namespace' => 'jumpEvent'],
            'debInscriptionDateAt' => ['key' => "Date de début d'inscription :", 'namespace' => 'jumpEvent'],
            'finInscriptionDateAt' => ['key' => "Date de fin d'inscription :", 'namespace' => 'jumpEvent'],
            'eventBeginAt'         => ['key' => "Date de début de l'event :", 'namespace' => 'jumpEvent'],
            'oneMetier'            => ['key' => "Inscription avec un seul métier :", 'namespace' => 'jumpEvent'],
            'villePrive'           => ['key' => "Event en ville privée :", 'namespace' => 'jumpEvent'],
            'nom_jump'             => ['key' => "Nom de la ville :", 'namespace' => 'jumpEvent'],
            'add_jump'             => ['key' => "Ajouter une ville", 'namespace' => 'jumpEvent'],
            'controle'             => [
                'dateDebFin'    => ['key' => "La date de début d'inscription doit être inférieur à la date de fin d'inscription.", 'namespace' => 'jumpEvent'],
                'dateFinApprox' => ['key' => "La date de fin d'inscription doit être inférieur à la date du début de l'évènement.", 'namespace' => 'jumpEvent'],
            ],
            'listVille'            => ['key' => "Listes des villes associés à l'event", 'namespace' => 'jumpEvent'],
        ];
    }
    
    private function form_inscription_translate(): array
    {
        return [
            'info'                => ['key' => "Information générale sur vous", 'namespace' => 'jumpEvent'],
            'banCommu'            => ['key' => "Volontaire banni communautaire :", 'namespace' => 'jumpEvent'],
            'gouleCommu'          => ['key' => "Volontaire goule communautaire :", 'namespace' => 'jumpEvent'],
            'voeuxMetier_place'   => ['key' => "choix métier", 'namespace' => 'jumpEvent'],
            'voeuxMetier'         => ['key' => "Choix métier :", 'namespace' => 'jumpEvent'],
            'voeuxMetier1'        => ['key' => "Voeu métier n°1 :", 'namespace' => 'jumpEvent'],
            'voeuxMetier2'        => ['key' => "Voeu métier n°2 :", 'namespace' => 'jumpEvent'],
            'voeuxMetier3'        => ['key' => "Voeu métier n°3 :", 'namespace' => 'jumpEvent'],
            'infoMetier'          => ['key' => "Mettez les métiers du plus important au moins important pour vous, ne mettre qu'une seule fois le même métier, les métiers seront attribués selon les disponibilités, le nombre de personnes choississant le métier, vos motivations, le type de ville, etc.", 'namespace' => 'jumpEvent'],
            'lvlRuine'            => ['key' => "Votre niveau de ruine :", 'namespace' => 'jumpEvent'],
            'lvlRuine_place'      => ['key' => "choix niveau de ruine", 'namespace' => 'jumpEvent'],
            'pouvFutur'           => ['key' => "Dernier pouvoir héros prévu pour le jump :", 'namespace' => 'jumpEvent'],
            'pouvFutur_place'     => ['key' => "choix pouvoir", 'namespace' => 'jumpEvent'],
            'dispo'               => ['key' => "Vos disponibilités types", 'namespace' => 'jumpEvent'],
            'dispo_place'         => ['key' => "choix dispo", 'namespace' => 'jumpEvent'],
            'dispo_semaine'       => ['key' => "Disponibilité type en semaine", 'namespace' => 'jumpEvent'],
            'dispo_weekend'       => ['key' => "Disponibilité type en week-end", 'namespace' => 'jumpEvent'],
            'role_ville'          => ['key' => "Votre ou vos rôle(s) durant la ville", 'namespace' => 'jumpEvent'],
            'echangeMoyenContact' => ['key' => "Echanges sur la candidature et moyens de contact pendant la ville", 'namespace' => 'jumpEvent'],
            'motivation'          => ['key' => "Vos motivations :", 'namespace' => 'jumpEvent'],
            'commentaires'        => ['key' => "Commentaires :", 'namespace' => 'jumpEvent'],
            'moyenContact'        => ['key' => "Vos moyens de contact autres que les MPs MyHordes (Numéro de tel, ID Discord (ex : pseudo#1234), ...) : <br/> <span class='warning'>Les données saisies ici sont chiffrées avant d'être mis en base de données</span>", 'namespace' => 'jumpEvent'],
            'typeDiffu'           => ['key' => "Diffusion des moyens de contact :", 'namespace' => 'jumpEvent'],
            'retourOrga'          => ['key' => "Retour des organisateurs :", 'namespace' => 'jumpEvent'],
            'role'                => ['key' => "Choix des roles :", 'namespace' => 'jumpEvent'],
            'lead'                => ['key' => "Choix des leads :", 'namespace' => 'jumpEvent'],
            'applead'             => ['key' => "Choix des apprentis leads :", 'namespace' => 'jumpEvent'],
            'type_diff_all'       => ['key' => "Tout le monde", 'namespace' => 'jumpEvent'],
            'type_diff_lead'      => ['key' => "Uniquement aux leads / organistateurs", 'namespace' => 'jumpEvent'],
            'fuseau'              => ['key' => "Votre fuseau horaire :", 'namespace' => 'jumpEvent'],
        ];
    }
    
    private function form_jump_jump_event(): array
    {
        return [
            'nom'                => ['key' => "Nom de la ville :", 'namespace' => 'jumpEvent'],
            'dateDebInscription' => ['key' => "Date début d'inscription :", 'namespace' => 'jumpEvent'],
            'dateFinInscription' => ['key' => "Date fin d'inscription :", 'namespace' => 'jumpEvent'],
            'dateApproxJump'     => ['key' => "Date approximative du jump :", 'namespace' => 'jumpEvent'],
            'villePrive'         => ['key' => "Ville privée (création ville privée) :", 'namespace' => 'jumpEvent'],
            'objectif'           => ['key' => "Objectif de la ville (plusieurs choix possibles) :", 'namespace' => 'jumpEvent'],
            'controle'           => [
                'dateDebFin'    => ['key' => "La date de début d'inscription doit être inférieur à la date de fin d'inscription . ", 'namespace' => 'jumpEvent'],
                'dateFinApprox' => ['key' => "La date de fin d'inscription doit être inférieur à la date approximative de jump . ", 'namespace' => 'jumpEvent'],
            ],
        ];
    }
}