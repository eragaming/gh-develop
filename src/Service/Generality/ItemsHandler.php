<?php

namespace App\Service\Generality;

use App\Entity\CategoryObjet;
use App\Entity\ItemPrototype;
use App\Service\GestHordesHandler;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Symfony\Contracts\Translation\TranslatorInterface;

class ItemsHandler
{
    
    public function __construct(
        protected EntityManagerInterface $em,
        protected TranslatorInterface    $translator,
        protected GestHordesHandler      $gestHordesHandler,
    )
    {
    }
    
    /**
     * @param array $excludeCatId
     * @return array
     * @throws JsonException
     */
    #[ArrayShape(['listCategorie' => "array", 'listItems' => "array"])]
    public function recuperationItems(array $excludeCatId = [], string $serialiseurGroupsItems = 'carte', string $serialiseurGroupsCate = 'carte', bool $forExpedition = false): array
    {
        $categories = $this->em->getRepository(CategoryObjet::class)->findAll();
        
        $categoriesFilter =
            array_filter($categories, fn(CategoryObjet $cat) => !in_array($cat->getId(), $excludeCatId));
        /**
         * @var ItemPrototype[] $recupItems
         */
        $request = $this->em->getRepository(ItemPrototype::class)->createQueryBuilder('i')
                            ->where('i.categoryObjet in (:categories)')
                            ->andWhere('i.id < 6000 and i.actif = 1');
        
        if ($forExpedition) {
            $request->orWhere('i.expedition = 1');
        }
        
        $recupItems = $request->setParameter(':categories', $categoriesFilter)
                              ->orderBy('i.categoryObjet')
                              ->addOrderBy('i.nom')
                              ->getQuery()->getResult();
        
        $serializer = $this->gestHordesHandler->getSerializer();
        
        return [
            'listCategorie' => json_decode($serializer->serialize($categoriesFilter, 'json', ['groups' => [$serialiseurGroupsCate]]),
                                           true, 512, JSON_THROW_ON_ERROR),
            'listItems'     => json_decode($serializer->serialize($recupItems, 'json', ['groups' => [$serialiseurGroupsItems]]), true,
                                           512, JSON_THROW_ON_ERROR),
        ];
    }
    
    /**
     * @return array
     */
    #[ArrayShape(['cat' => "array", 'items' => "array"])] public function recuperationItemsArray(array $excludeCatId = []): array
    {
        $categories = $this->em->getRepository(CategoryObjet::class)->findAll();
        
        $categoriesFilter =
            array_filter($categories, fn(CategoryObjet $cat) => !in_array($cat->getId(), $excludeCatId));
        /**
         * @var ItemPrototype[] $recupItems
         */
        $recupItems = $this->em->getRepository(ItemPrototype::class)->createQueryBuilder('i')
                               ->where('i.categoryObjet in (:categories)')
                               ->andWhere('i.id < 6000')
                               ->andWhere('i.actif = 1')
                               ->setParameter(':categories', $categoriesFilter)
                               ->orderBy('i.categoryObjet')
                               ->addOrderBy('i.nom')
                               ->getQuery()->getResult();
        
        
        return [
            'listCategorie' => array_map(fn(CategoryObjet $cat) => $cat->setNom($this->translator->trans($cat->getNom(), [], 'game')), $categoriesFilter),
            'listItems'     => array_map(fn(ItemPrototype $items) => $items->setNom($this->translator->trans($items->getNom(), [], 'game')), $recupItems),
        ];
    }
    
}