<?php


namespace App\Service\Utils;


class ColorGeneratorHandler
{
    
    
    public function randomColorHexa(): string
    {
        
        $r = dechex(random_int(0, 255));
        $g = dechex(random_int(0, 255));
        $b = dechex(random_int(0, 255));
        
        $color = ((strlen($r) < 2) ? '0' : '') . $r;
        $color .= ((strlen($g) < 2) ? '0' : '') . $g;
        $color .= ((strlen($b) < 2) ? '0' : '') . $b;
        
        return '#' . $color;
        
    }
    
}