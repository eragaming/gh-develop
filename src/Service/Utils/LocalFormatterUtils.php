<?php


namespace App\Service\Utils;


use DateTime;
use Symfony\Contracts\Translation\TranslatorInterface;

class LocalFormatterUtils
{
    
    public function __construct(protected TranslatorInterface $translator)
    {
    }
    
    /**
     * @return string|null
     */
    public function getFormattedDateHeure(?DateTime $dateAformater): ?string
    {
        $format = $this->translator->trans("d/m/Y à H:i", [], 'app');
        
        return $dateAformater?->format($format) ?? null;
    }
    
    public function getFormattedDateMoisJour(DateTime $dateAFormater): string
    {
        $format = $this->translator->trans("d/m", [], 'app');
        
        return $dateAFormater->format($format);
    }
    
}