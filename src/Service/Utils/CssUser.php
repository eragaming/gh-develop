<?php


namespace App\Service\Utils;


use Doctrine\ORM\EntityManagerInterface;

class CssUser
{
    
    private ?int $id = null;
    
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }
    
    public function alimId(?int $id)
    {
        $this->id = $id;
    }
    
    public function generateCSSCoalition(): string
    {
        $cssOnglet = '';
        if (isset($_COOKIE['selectedOngletCoa'])) {
            $idZone    = $_COOKIE['selectedOngletCoa'];
            $cssOnglet .= $idZone . '{display:block;}';
            $cssOnglet .= '#zone_total_coa > div:not(' . $idZone . '){display:none}';
        } else {
            $cssOnglet .= '#zone_coalition{display:block;}';
            $cssOnglet .= '#zone_total_coa > div:not(#zone_coalition){display:none}';
        }
        
        return $cssOnglet;
    }
    
    public function generateCSSCreation(): string
    {
        $cssOnglet = '';
        if (isset($_COOKIE['ongletSelectedJE'])) {
            $idZone    = $_COOKIE['ongletSelectedJE'];
            $cssOnglet .= $idZone . '{display:block;}';
            $cssOnglet .= '#zone_creation > div:not(' . $idZone . '){display:none}';
        } else {
            $cssOnglet .= '#zone_jump{display:block;}';
            $cssOnglet .= '#zone_creation > div:not(#zone_jump){display:none}';
        }
        
        return $cssOnglet;
    }
    
    
}