<?php

namespace App\Service\Utils;

use App\Factory\SerializerFactory;
use JsonException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerService
{
    private SerializerInterface $serializer;
    
    public function __construct(private readonly SerializerFactory $serializerFactory)
    {
        $this->serializer = $this->serializerFactory->create();
    }
    
    public function deserialize(string $data, string $type, string $format = 'json', object $object = null, array $groups = [], array $context = []): object
    {
        // Fusion des groupes de sérialisation avec le contexte existant
        if (!empty($groups)) {
            $context[AbstractNormalizer::GROUPS] = array_merge($groups, $context[AbstractNormalizer::GROUPS] ?? []);
        }
        
        if ($object !== null) {
            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $object;
        }
        
        return $this->serializer->deserialize($data, $type, $format, $context);
    }
    
    public function serialize($data, string $format = 'json', array $groups = [], array $context = []): string
    {
        // Fusion des groupes de sérialisation avec le contexte existant
        if (!empty($groups)) {
            $context[AbstractNormalizer::GROUPS] = array_merge($groups, $context[AbstractNormalizer::GROUPS] ?? []);
        }
        $retour = $this->serializer->serialize($data, $format, $context);
        
        return $retour;
    }
    
    /**
     * @throws JsonException
     */
    public function serializeArray($data, string $format = 'json', array $groups = [], array $context = []): array|string|null
    {
        if ($data === null) {
            return null;
        }
        return json_decode($this->serialize($data, $format, $groups, $context), true, 512, JSON_THROW_ON_ERROR);
    }
    
}