<?php


namespace App\Service\Utils;


use App\Entity\AvancementChantier;
use App\Entity\Banque;
use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Citoyens;
use App\Entity\CleanUpCadaver;
use App\Entity\DecouverteTitre;
use App\Entity\Defense;
use App\Entity\Estimation;
use App\Entity\ExpeHordes;
use App\Entity\HistoriquePictos;
use App\Entity\HistoriqueVille;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\Journal;
use App\Entity\MapItem;
use App\Entity\PictoPrototype;
use App\Entity\Pictos;
use App\Entity\PictoTitrePrototype;
use App\Entity\PlansChantier;
use App\Entity\Ruines;
use App\Entity\TypeDeath;
use App\Entity\UpChantier;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\VilleHistorique;
use App\Entity\ZoneMap;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Repository\VilleHistoriqueRepository;
use App\Repository\VilleRepository;
use App\Service\MyHordesApi;
use App\Service\UserHandler;
use App\Service\Ville\CarteHandlerV2;
use App\Structures\Dto\Api\MH\CitoyensMaj;
use App\Structures\Dto\Api\MH\DefenseMaj;
use App\Structures\Dto\Api\MH\EstimMaj;
use App\Structures\Dto\Api\MH\ExpeditionMaj;
use App\Structures\Dto\Api\MH\MyHordesMaj;
use App\Structures\Dto\Api\MH\NewsMaj;
use App\Structures\Dto\Api\MH\PictoMaj;
use App\Structures\Dto\Api\MH\UpgradeMaj;
use App\Structures\Dto\Api\MH\VilleHistoriqueMaj;
use App\Structures\Dto\Api\MH\ZoneMaj;
use App\Structures\Dto\GH\Utils\NewUser;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateAPIHandler
{
    
    /**
     * @var ItemPrototype[]
     */
    private array $listObjet;
    /**
     * @var BatPrototype[]
     */
    private array $listBat;
    /**
     * @var ChantierPrototype[]
     */
    private array $listChantier;
    /**
     * @var JobPrototype[]
     */
    private array $listJob;
    /**
     * @var PictoPrototype[]
     */
    private array $listPicto;
    /**
     * @var TypeDeath[]
     */
    private array $listMort;
    /**
     * @var CleanUpCadaver[]
     */
    private array $listCleanUpType;
    
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly MyHordesApi            $myHordesApi,
        private readonly UserHandler            $userHandler,
        private readonly TranslatorInterface    $translator,
        private readonly LoggerInterface        $logger,
        private readonly CarteHandlerV2         $carteHandlerV2,
        private readonly DataService            $dataService, private readonly DiscordService $discordService,
    )
    {
        $this->listObjet       = $this->dataService->getListObjetIndexed();
        $this->listBat         = $this->dataService->getListBatIndexed();
        $this->listChantier    = $this->dataService->getListChantierIndexed();
        $this->listJob         = $this->dataService->getListJobIndexed();
        $this->listPicto       = $this->dataService->getListPictoIndexed();
        $this->listMort        = $this->dataService->getListMortIndexed();
        $this->listCleanUpType = $this->dataService->getListCleanUpTypeIndexed();
        
    }
    
    /**
     * @throws MyHordesMajException
     * @throws MyHordesAttackException|MyHordesMajApiException
     */
    public function appelApiGestionAttackMaintenance(string $method, ?string $token): MyHordesMaj
    {
        $messageError = " - Problème lors de la mise à jour : {message}";
        try {
            
            if ($token === null) {
                throw new MyHordesMajException($this->translator->trans($messageError, ['{message}' => 'token null'], 'service'), MyHordesMajException::TOKEN_NULL);
            }
            
            $json_api = $this->myHordesApi->$method($token);
            
            $maj = new MyHordesMaj(json_decode($json_api, true, 512, JSON_THROW_ON_ERROR));
            
            if ($maj->getError() !== null) {
                if ($maj->getError() === 'nightly_attack') {
                    throw new MyHordesAttackException(
                        $this->translator->trans($messageError, ['{message}' => $maj->getError()], 'service'),
                    );
                } else {
                    if ($maj->getError() === 'invalid_userkey') {
                        throw new MyHordesMajException($this->translator->trans($messageError, ['{message}' => $maj->getError()], 'service'), MyHordesMajException::INVALID_USER_KEY);
                    } else if ($maj->getError() === 'rate_limit_reached') {
                        throw new MyHordesMajException($this->translator->trans($messageError, ['{message}' => $maj->getError()], 'service'), MyHordesMajException::TOO_MANY_REQUESTS);
                    } else if ($maj->getError() === 'server_error') {
                        throw new MyHordesMajException($this->translator->trans($messageError, ['{message}' => $maj->getError()], 'service'), MyHordesMajException::INTERNAL_ERROR);
                    } else {
                        throw new MyHordesMajException($this->translator->trans($messageError, ['{message}' => $maj->getError()], 'service'));
                    }
                    
                }
            }
            return $maj;
            
        } catch (JsonException|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            throw new MyHordesMajApiException($this->translator->trans($messageError, ['{message}' => "{$e->getMessage()} | Fichier : " . __FILE__ . " | Ligne : " . __LINE__], 'service'),);
        }
    }
    
    /**
     * @throws Exception
     */
    public function createUpdateUserMaj(Request $request, MyHordesMaj $maj, string $token, bool &$updateTown = false): User
    {
        $user = $this->dataService->getFindOneUser(['idMyHordes' => $maj->getId()]);
        
        $langLocal = $maj->locale ?? 'en';
        
        if ($user === null) {
            $newUser = new NewUser();
            $newUser->setIdMh($maj->getId())
                    ->setAvatar($maj->getAvatar())
                    ->setLang($maj->getLocale() ?? 'en')
                    ->setPseudo($maj->getName())
                    ->setMapId($maj->getMapId() ?? null);
            $user = $this->userHandler->createNewUserFromApi($newUser, true);
            $request->getSession()->set('_locale', $maj->getLocale() ?? 'en');
            $request->setLocale($maj->getLocale() ?? 'en');
            $request->setDefaultLocale($langLocal);
            $updateTown = true;
        } else {
            
            if (($user->getMapId() === null && $maj->getMapId() !== null) ||
                ($user->getMapId() !== null && $maj->getMapId() !== $user->getMapId())) {
                $updateTown = true;
            }
            
            $user->setApiToken($token);
            $this->updateUser($maj, $user);
        }


//        if ($user->getLang() === null) {
//            switch ($maj->getLocale()) {
//                case 'en':
//                case 'fr' :
//                case 'de':
//                case 'es':
//                    $this->userHandler->sauvegardeLang($user, $maj->getLocale());
//                    $request->getSession()->set('_locale', $maj->getLocale());
//                    $request->setLocale($maj->getLocale());
//                    break;
//                default:
//                    $this->userHandler->sauvegardeLang($user, 'fr');
//                    $request->getSession()->set('_locale', 'fr');
//                    $request->setLocale('fr');
//                    $request->setDefaultLocale('fr');
//            }
//        }
        
        return $user;
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws MyHordesMajException
     * @throws Exception
     */
    public function updateAll(Request $request, string $token, int $typeMaj = 0): void
    {
        
        $maj = $this->appelApiGestionAttackMaintenance("getMeTown", $token);
        
        $user = $this->createUpdateUserMaj($request, $maj, $token);
        if ($maj->getMapId() !== null && !$maj->isDead()) {
            $this->updateTown($maj, $user, $token, $typeMaj);
        } else if ($user->getMapId() !== null && $maj->getMapId() === null) {
            $majPictos = $this->appelApiGestionAttackMaintenance("", $token);
            $this->updateUserDetails($majPictos, $user);
        }
        
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws Exception
     */
    public function updateAllDetails(Ville $ville, string $token): void
    {
        $majAll = new MyHordesMaj(json_decode($this->myHordesApi->getMeOldTownPictosAllTown($token), true, 512, JSON_THROW_ON_ERROR));
        $this->updateCitoyensDetails($majAll, $ville);
        
        $ville->setMajAllOldTown(true)->setMajAllPicto(true);
        $this->em->persist($ville);
        $this->em->flush();
    }
    
    /**
     * @param VilleHistoriqueMaj[] $listOldVille
     * @throws Exception
     */
    public function updateAllOldVille(array $listOldVille, User $user): void
    {
        try {
            
            $this->processVilles($listOldVille, $user);
            
            $user->setForceMajOldTown(false);
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception $exception) {
            $this->handleException(null, $user, $exception, "mise à jour vieille ville | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateAvancementChantier(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        try {
            // Récupération de l'avancement des chantiers existants
            $avancementChantiers = $ville->getAvancementChantiers()->toArray();
            
            /** @var AvancementChantier[] $arrayAvancementChantier */
            $arrayAvancementChantier = count($avancementChantiers) != 0
                ? array_combine(
                    array_map(fn(AvancementChantier $chantier) => $chantier->getChantier()->getIdMh(), $avancementChantiers),
                    $avancementChantiers,
                )
                : [];
            
            // Récupération de l'avancement des chantiers à mettre à jour
            $avancementChantiersMaj = $maj->getMap()->getCity()->getChantiers();
            
            foreach ($avancementChantiersMaj as $avancementMaj) {
                $id = $avancementMaj->getId();
                
                if (isset($arrayAvancementChantier[$id])) {
                    $chantierExist = $arrayAvancementChantier[$id];
                    if ($chantierExist->getPa() !== $avancementMaj->getActions()) {
                        $chantierExist->setPa($avancementMaj->getActions());
                    }
                    if ($chantierExist->getChantier()->getPlan() > 0) {
                        $this->updatePlansChantiers($chantierExist->getChantier(), $ville, $user);
                    }
                    unset($arrayAvancementChantier[$id]);
                } else {
                    $chantierProto = $this->listChantier[$id];
                    $newAvancement = new AvancementChantier();
                    $newAvancement->setChantier($chantierProto)
                                  ->setPa($avancementMaj->getActions());
                    $ville->addAvancementChantier($newAvancement);
                    if ($chantierProto->getPlan() > 0) {
                        $this->updatePlansChantiers($chantierProto, $ville, $user);
                    }
                }
                
                
            }
            
            // Supprimer les objets restants dans le tableau des données actuelles
            foreach ($arrayAvancementChantier as $avancementChantierToRemove) {
                $ville->removeAvancementChantier($avancementChantierToRemove);
            }
            
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Avancement Chantier | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateBanque(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        try {
            // Récupération de la banque existante
            $banques = $ville->getBanque()->toArray();
            
            /** @var Banque[] $arrayBanque */
            $arrayBanque = count($banques) != 0
                ? array_combine(
                    array_map(fn(Banque $i) => $i->getItem()->getIdMh() . '_' . ($i->getBroked() ? 1 : 0), $banques),
                    $banques,
                )
                : [];
            
            $banquesMaj = $maj->getMap()->getCity()->getBank();
            
            foreach ($banquesMaj as $banqueMaj) {
                $id = $banqueMaj->getId() . '_' . ($banqueMaj->isBroken() ? 1 : 0);
                
                if (isset($arrayBanque[$id])) {
                    $banqueExist = $arrayBanque[$id];
                    if ($banqueExist->getNombre() !== $banqueMaj->getCount()) {
                        $banqueExist->setNombre($banqueMaj->getCount());
                    }
                    unset($arrayBanque[$id]);
                } else {
                    $itemProto  = $this->listObjet[$banqueMaj->getId()];
                    $banqueItem = new Banque();
                    $banqueItem->setItem($itemProto)
                               ->setBroked($banqueMaj->isBroken())
                               ->setNombre($banqueMaj->getCount());
                    $ville->addBanque($banqueItem);
                }
            }
            
            // Supprimer les objets restants dans le tableau des données actuelles
            foreach ($arrayBanque as $banqueToRemove) {
                $ville->removeBanque($banqueToRemove);
            }
            
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Banque | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateChantier(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        try {
            // Récupération des chantiers existants
            $chantiers = $ville->getChantiers()->toArray();
            
            /** @var Chantiers[] $arrayChantier */
            $arrayChantier = count($chantiers) != 0
                ? array_combine(
                    array_map(fn(Chantiers $chantier) => $chantier->getChantier()->getIdMh(), $chantiers),
                    $chantiers,
                )
                : [];
            
            // Récupération des chantiers à mettre à jour
            $chantiersMaj = $maj->getMap()->getCity()->getBuildings();
            
            foreach ($chantiersMaj as $chantierMaj) {
                $id = $chantierMaj->getId();
                
                if (isset($arrayChantier[$id])) {
                    $chantierExist = $arrayChantier[$id];
                    
                    if ($chantierExist->getDef() !== $chantierMaj->getDef() ||
                        $chantierExist->getPv() !== $chantierMaj->getLife()) {
                        $chantierExist->setDef($chantierMaj->getDef())
                                      ->setPv($chantierMaj->getLife());
                    }
                    if ($chantierExist->isDetruit()) {
                        // Vu qu'il est reconstruit, on rajoute un jour au tableau de construction
                        $jourConstruit   = $chantierExist->getJourConstruction();
                        $jourConstruit[] = $ville->getJour();
                        
                        $chantierExist->setDetruit(false)
                                      ->setNbrConstruit($chantierExist->getNbrConstruit() + 1)
                                      ->setJourConstruction($jourConstruit);
                    }
                    if ($chantierExist->getChantier()->getPlan() > 0) {
                        $this->updatePlansChantiers($chantierExist->getChantier(), $ville, $user);
                    }
                    unset($arrayChantier[$id]);
                } else {
                    $chantierProto = $this->listChantier[$id];
                    $newChantier   = new Chantiers($chantierProto);
                    $newChantier->setDef($chantierMaj->getDef())
                                ->setJour($ville->getJour())
                                ->setDetruit(false)
                                ->setJourConstruction([$ville->getJour()])
                                ->setNbrConstruit(1)
                                ->setPv($chantierMaj->getLife());
                    $ville->addChantier($newChantier);
                    if ($chantierProto->getPlan() > 0) {
                        $this->updatePlansChantiers($chantierProto, $ville, $user);
                    }
                }
            }
            
            // Définir les chantiers restants a détruit
            foreach ($arrayChantier as $chantierToRemove) {
                if (!$chantierToRemove->isDetruit()) {
                    $jourDestruction   = $chantierToRemove->getJourDestruction();
                    $jourDestruction[] = $ville->getJour();
                    $chantierToRemove->setDetruit(true)
                                     ->setJourDestruction($jourDestruction);
                }
            }
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Chantier | Fichier : " . __FILE__ . " | Ligne :" . __LINE__);
        }
    }
    
    /**
     * @param User[]|array $listUsers
     * @throws Exception
     */
    public function updateCitoyens(MyHordesMaj $maj, Ville $ville, array &$listUsers, User $user): void
    {
        $listDeathTypeIndex      = $this->listMort;
        $listCleanUpCadaverIndex = $this->listCleanUpType;
        $listHomeProtoIndex      = $this->dataService->getHomePrototypeByDef();
        
        $citoyensMaj           = $maj->getMap()->getCitizens();
        $cadavresMaj           = $maj->getMap()->getCadavers();
        $citoyensExisting      = $ville->getCitoyens()->toArray();
        $citoyensExistingIndex = array_combine(array_map(fn(Citoyens $citoyen) => $citoyen->getCitoyen()->getIdMyHordes(), $citoyensExisting), $citoyensExisting);
        
        $listId    = array_merge(array_map(fn($i) => $i->getId(), $citoyensMaj), array_map(fn($i) => $i->getId(), $cadavresMaj));
        $listUsers = $this->dataService->getListCitoyensByIds($listId);
        
        $citoyensAllMaj = array_merge($citoyensMaj, $cadavresMaj);
        
        try {
            $this->updateUsers($citoyensAllMaj, $listUsers, $ville);
            $this->updateCitizens($citoyensAllMaj, $citoyensExistingIndex, $ville, $listUsers, $listHomeProtoIndex, $listDeathTypeIndex, $listCleanUpCadaverIndex);
            
            if ($ville->getJour() > 1) {
                $this->updateVilleSpecialists($maj, $ville, $listUsers);
            }
            
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Citoyens | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateDefense(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        try {
            $defense    = $this->getLastDefense($ville);
            $defenseMaj = $this->getDefenseMaj($maj);
            
            if ($defenseMaj === null) {
                return;
            }
            
            $this->updateDefenses($defenseMaj, $defense, $ville);
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Defense ville  | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateEstimation(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        $functionMajEstimation = function (Estimation $estimation, ?EstimMaj $estimMaj, ?EstimMaj $estimMajNext): void {
            $estimation->setMinJour($estimMaj->getMin())
                       ->setMaxJour($estimMaj->getMax())
                       ->setMaxedJour($estimMaj->isMaxed());
            if ($estimMajNext != null) {
                $estimation->setMinPlanif($estimMajNext->getMin())
                           ->setMaxPlanif($estimMajNext->getMax())
                           ->setMaxedPlanif($estimMajNext->isMaxed());
            }
        };
        
        try {
            $estimationExistante = $this->getLastEstimation($ville);
            $estimJourMaj        = $this->getEstimationMaj($maj);
            $estimJourNextMaj    = $this->getEstimationMajNext($maj);
            
            if ($estimJourMaj !== null) {
                $this->updateEstimations($functionMajEstimation, $estimationExistante, $estimJourMaj, $estimJourNextMaj, $ville);
            }
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Defense ville | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateExpeHordes(MyHordesMaj $maj, Ville $ville, array $listUsers, User $user): void
    {
        $expeMajs         = $maj->getMap()->getExpeditions();
        $expeExisting     = $ville->getExpeHordes()->toArray();
        $listExpeExisting = array_combine(
            array_map(fn(ExpeHordes $expeHordes) => $expeHordes->getCitoyen()->getIdMyHordes() . "_" . $expeHordes->getNom(), $expeExisting),
            $expeExisting,
        );
        
        try {
            $this->processExpeditions($expeMajs, $listExpeExisting, $listUsers, $ville);
            $this->removeRemainingExpeditions($listExpeExisting, $ville);
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Update Expeditions Hordes | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateJournal(Ville $ville, User $user): void
    {
        $functionMajNews = function (Journal $news, ?NewsMaj $newsMaj): void {
            
            // Cas où la regenDir possède une apostrophe typographyque
            if ($newsMaj->getRegenDir()->getContentFR() === "l’est" || $newsMaj->getRegenDir()->getContentFR() === "l’ouest") {
                $news->setRegenDir(str_replace("’", "'", $newsMaj->getRegenDir()->getContentFR()));
            } else {
                $news->setRegenDir($newsMaj->getRegenDir()->getContentFR());
            }
            
            $news->setDef($newsMaj->getDef() ?? 0)
                 ->setZombie($newsMaj->getZ() ?? 0)
                 ->setContent($newsMaj->getContent()?->getContentFR() ?? '')
                 ->setContentDe($newsMaj->getContent()?->getContentDE() ?? '')
                 ->setContentEn($newsMaj->getContent()?->getContentEN() ?? '')
                 ->setContentEs($newsMaj->getContent()?->getContentES() ?? '')
                 ->setWater($newsMaj->getWater());
        };
        
        try {
            $newsJourMajJour1 = new NewsMaj([
                                                'def'     => 0,
                                                'z'       => 0,
                                                'content' => [
                                                    "fr" => "<p>Il s'agit du premier jour en ville, il n'y a donc pas de gazette pour ce jour </p>",
                                                    "en" => "<p>It is the first day in town, so there is no gazette for today</p>",
                                                    "es" => "<p>Es el primer día en la ciudad, así que no hay gaceta para hoy </p>",
                                                    "de" => "<p>Es ist der erste Tag in der Stadt, daher gibt es für heute keine Zeitung </p>",
                                                ],
                                            ]);
            
            $json_api = $this->myHordesApi->getMeGazette($user->getApiToken());
            
            $majGazette = new MyHordesMaj(json_decode($json_api, true, 512, JSON_THROW_ON_ERROR));
            
            $newsJourMaj = $this->getNewsMaj($majGazette);
            
            $newsJourMajVide = new NewsMaj([
                                               'def'     => 0,
                                               'z'       => 0,
                                               'content' => [
                                                   "fr" => '<p>Aucune gazette connue pour ce jour</p>',
                                                   "en" => "<p>No known gazette for this day</p>",
                                                   "es" => "<p>No hay gaceta conocida para este día</p>",
                                                   "de" => "<p>Keine bekannte Zeitung für diesen Tag</p>",
                                               ],
                                           ]);
            
            $journalExistant = $this->getLastJournal($ville);
            $this->updateJournals($functionMajNews, $journalExistant, $newsJourMaj, $newsJourMajVide, $newsJourMajJour1, $ville);
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "mise à jour journal | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @param VilleHistoriqueMaj[] $listOldVille
     * @throws Exception
     */
    public function updateOldVille(array $listOldVille, User $user): void
    {
        try {
            $historiqueVilleRepo = $this->em->getRepository(HistoriqueVille::class);
            $villeCount          = $historiqueVilleRepo->count(['user' => $user]);
            
            if (abs($villeCount - count($listOldVille)) > 2 || $user->isForceMajOldTown()) {
                $this->processVilles($listOldVille, $user);
            } else {
                $this->processVilles(array_slice($listOldVille, -2, 2), $user);
            }
            
            $user->setForceMajOldTown(false);
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception $exception) {
            $this->handleException(null, $user, $exception, "mise à jour vieille ville | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @param PictoMaj[] $pictosMaj
     * @param User $user
     * @param int|null $mapIdDerVille
     * @throws Exception
     */
    public function updatePicto(array $pictosMaj, User $user): void
    {
        if (!$this->shouldUpdatePictos($user)) {
            return;
        }
        
        try {
            $pictosUser  = $this->em->getRepository(Pictos::class)->findBy(['user' => $user]);
            $arrayPictos = $this->getArrayPictos($pictosUser);
            
            $newTitreDiscord = [];
            foreach ($pictosMaj as $pictoMaj) {
                $this->processPictoMaj($pictoMaj, $user, $arrayPictos, $newTitreDiscord);
            }
            
            $this->removeRemainingPictos($arrayPictos);
            $this->finalizeUpdate($user);
            
            if (count($newTitreDiscord) > 0) {
                // On formate les différents titres découverts sous forme de liste à puce pour Discord et on envoie le message en pingant l'admin (Eragony)
                $message = "Hé <@{$_ENV['ID_ADMIN']}>, des nouveaux titres ont été découverts par {$user->getPseudo()} : ";
                $message .= "\n";
                $message .= implode("\n", array_map(fn($i) => "- {$i}", $newTitreDiscord));
                
                $this->discordService->generateMessageMessageDiscord($message, []);
            }
            
        } catch (Exception $exception) {
            $this->handleException(null, $user, $exception, "mise à jour pictos | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        } catch (GuzzleException $e) {
            $this->handleException(null, $user, $e, "mise à jour pictos | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function updateTown(MyHordesMaj $maj, User $user, string $token, int $typeMaj = 0): void
    {
        $mapId = $maj->getMapId();
        if ($mapId === null || $maj->getMap()?->isError() || ($maj->isDead() ?? true)) {
            return;
        }
        
        $ville        = $this->dataService->getDataVille($mapId);
        $jourAvantMaj = $ville?->getJour() ?? 0;
        $isNewVille   = $ville === null;
        
        try {
            if ($isNewVille) {
                $ville = new Ville($mapId, Ville::ORIGIN_MH);
            } else {
                $this->dataService->updateZoneVue($ville);
            }
            
            $this->updateVilleDetails($maj, $ville, $user, $typeMaj, $isNewVille, $jourAvantMaj);
            
            $this->em->persist($ville);
            $this->em->flush();
            
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "sauvegarde | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
        
        try {
            if ($this->shouldUpdateAll($ville)) {
                $this->updateAllDetails($ville, $token);
            } else {
                if ($user->getMapId() !== $ville->getMapId()) {
                    $majPictos = $this->appelApiGestionAttackMaintenance("", $token);
                    $this->updateUserDetails($majPictos, $user);
                }
            }
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "traitement de tous les pictos et villes | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateUpChantier(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        if ($ville->getJour() === 1) {
            return;
        }
        
        try {
            $listUpMaj      = $this->getListUpMaj($maj);
            $listUpExisting = $this->getListUpExisting($ville);
            
            $this->updateUpgrades($listUpMaj, $listUpExisting, $ville);
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "mise à jour up chantier | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateUser(MyHordesMaj $maj, User $user): void
    {
        
        
        try {
            $user->setPseudo($maj->getName())
                 ->setAvatar($maj->getAvatar())
                 ->setLastConnexionAt(new DateTimeImmutable('Now'))
                 ->setMapId($maj->getMapId() ?? null);
            
            $this->updateUserDetails($maj, $user);
            
            $this->em->persist($user);
            $this->em->flush();
            $this->em->refresh($user);
            
            
        } catch (Exception $exception) {
            $this->handleException(null, $user, $exception, "problème sauvegarde utilisateur | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
        
    }
    
    /**
     * @throws Exception
     */
    public function updateUserDetails(MyHordesMaj $maj, User $user): void
    {
        if ($maj->getRewards() !== null) {
            $this->updatePicto($maj->getRewards(), $user);
        }
        
        if ($maj->getPlayedMap() !== null) {
            $this->updateOldVille($maj->getPlayedMap(), $user);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateVilleDetails(MyHordesMaj $maj, Ville $ville, User $user, int $typeMaj, bool $isNewVille, int $jourVille): void
    {
        $this->updateVilleGeneral($maj, $ville, $user);
        $this->updateBanque($maj, $ville, $user);
        $this->updateChantier($maj, $ville, $user);
        $this->updateAvancementChantier($maj, $ville, $user);
        $this->updateDefense($maj, $ville, $user);
        $this->updateEstimation($maj, $ville, $user);
        if ($jourVille < $ville->getJour()) {
            $this->updateJournal($ville, $user);
        }
        
        if ($ville->getJour() > 1) {
            $this->updateUpChantier($maj, $ville, $user);
        }
        
        $listUsers = [];
        $this->updateCitoyens($maj, $ville, $listUsers, $user);
        $this->updateExpeHordes($maj, $ville, $listUsers, $user);
        $this->updateZonesMap($maj, $ville, $listUsers, $isNewVille, $user);
        
        if ($typeMaj === 1) {
            $ville->incrementCountMajScript();
        }
        
        $ville->incrementCountMaj();
    }
    
    /**
     * @param MyHordesMaj $maj
     * @param Ville $ville
     * @param User $user
     * @return void
     * @throws Exception
     */
    public function updateVilleGeneral(MyHordesMaj $maj, Ville $ville, User $user): void
    {
        try {
            $map  = $maj->getMap();
            $city = $map->getCity();
            
            $ville->setJour($map->getDays())
                  ->setNom($city->getName())
                  ->setPosX($city->getX())
                  ->setPosY($city->getY())
                  ->setWeight($map->getWid())
                  ->setHeight($map->getHei())
                  ->setBonus($map->getBonusPts())
                  ->setPrived($map->isCustom())
                  ->setPorte($city->isDoor())
                  ->setHard($city->isHard())
                  ->setWater($city->getWater())
                  ->setSaison($map->getSeason())
                  ->setPhase($map->getPhase())
                  ->setUpdateBy($user)
                  ->setLang($map->getLanguage() ?? null)
                  ->setDateTime(new DateTime());
            
            // On stocke l'état actuel de la ville (chaos, devast)
            if (!$ville->isChaos() && $city->isChaos() && $ville->getDayChaos() === null) {
                $ville->setChaos($city->isChaos())
                      ->setDayChaos($ville->getJour());
            } else {
                $ville->setChaos($city->isChaos());
            }
            
            if (!$ville->isDevast() && $city->isDevast() && $ville->getDayDevast() === null) {
                $ville->setDevast($city->isDevast())
                      ->setDayDevast($ville->getJour());
            } else {
                $ville->setDevast($city->isDevast());
            }
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "Alim Ville | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    /**
     * @throws Exception
     */
    public function updateZonesMap(MyHordesMaj $maj, Ville $ville, array $listUsers, bool $topCreationVille, User $user): void
    {
        try {
            $zonesMaj       = $this->getZonesMaj($maj);
            $ruinesExisting = $this->dataService->getRuineVille($ville);
            
            if ($topCreationVille || $ville->getZone()->count() == 0) {
                $this->initializeZones($ville);
            }
            
            $this->updateExistingZones($zonesMaj, $ville, $listUsers, $ruinesExisting, $maj, $user);
            $ville->setUpdateBy($user);
            
        } catch (Exception $exception) {
            $this->handleException($ville, $user, $exception, "mise à jour zone map | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
        }
    }
    
    private function addNewDefense(DefenseMaj $defenseMaj, Ville $ville): void
    {
        $defenseNew = new Defense($ville->getJour());
        $this->updateDefenseFromMaj($defenseMaj, $defenseNew);
        $ville->addDefense($defenseNew);
    }
    
    private function addNewEstimation(callable $functionMajEstimation, ?EstimMaj $estimJourMaj, ?EstimMaj $estimJourNextMaj, Ville $ville): void
    {
        $estimation = new Estimation($ville->getJour());
        $functionMajEstimation($estimation, $estimJourMaj, $estimJourNextMaj);
        $ville->addEstimation($estimation);
    }
    
    private function addNewJournal(callable $functionMajNews, ?NewsMaj $newsJourMaj, Ville $ville): void
    {
        $journalNew = new Journal($ville->getJour());
        $functionMajNews($journalNew, $newsJourMaj);
        $ville->addJournal($journalNew);
    }
    
    private function calculateZoneBorders(Ville $ville, array $mapTab): void
    {
        foreach ($ville->getZone() as $zone) {
            $xRel = $zone->getXRel();
            $yRel = $zone->getYRel();
            $this->carteHandlerV2->calculBordureCaseKm(
                $zone,
                isset($mapTab[$xRel][$yRel + 1]) ? $mapTab[$xRel][$yRel + 1]->getKm() : null,
                isset($mapTab[$xRel][$yRel - 1]) ? $mapTab[$xRel][$yRel - 1]->getKm() : null,
                isset($mapTab[$xRel + 1][$yRel]) ? $mapTab[$xRel + 1][$yRel]->getKm() : null,
                isset($mapTab[$xRel - 1][$yRel]) ? $mapTab[$xRel - 1][$yRel]->getKm() : null,
            );
            $this->carteHandlerV2->calculBordureCasePa($zone,
                                                       isset($mapTab[$xRel][$yRel + 1]) ? $mapTab[$xRel][$yRel + 1]->getPa() : null,
                                                       isset($mapTab[$xRel][$yRel - 1]) ? $mapTab[$xRel][$yRel - 1]->getPa() : null,
                                                       isset($mapTab[$xRel + 1][$yRel]) ? $mapTab[$xRel + 1][$yRel]->getPa() : null,
                                                       isset($mapTab[$xRel - 1][$yRel]) ? $mapTab[$xRel - 1][$yRel]->getPa() : null,
            );
            $this->carteHandlerV2->calculBordureCaseZone($zone,
                                                         isset($mapTab[$xRel][$yRel + 1]) ? $mapTab[$xRel][$yRel + 1]->getZone() : null,
                                                         isset($mapTab[$xRel][$yRel - 1]) ? $mapTab[$xRel][$yRel - 1]->getZone() : null,
                                                         isset($mapTab[$xRel + 1][$yRel]) ? $mapTab[$xRel + 1][$yRel]->getZone() : null,
                                                         isset($mapTab[$xRel - 1][$yRel]) ? $mapTab[$xRel - 1][$yRel]->getZone() : null,
            );
            $this->carteHandlerV2->calculBordureCaseScrutateur($zone,
                                                               $mapTab[$xRel][$yRel + 1] ?? null,
                                                               $mapTab[$xRel + 1][$yRel] ?? null,
                                                               $mapTab[$xRel][$yRel - 1] ?? null,
                                                               $mapTab[$xRel - 1][$yRel] ?? null,
                                                               $mapTab[$xRel + 1][$yRel + 1] ?? null,
                                                               $mapTab[$xRel - 1][$yRel + 1] ?? null,
                                                               $mapTab[$xRel + 1][$yRel - 1] ?? null,
                                                               $mapTab[$xRel - 1][$yRel - 1] ?? null,
            );
            
        }
    }
    
    private function createEmptyDefense(int $day, Ville $ville): Defense
    {
        return (new Defense($day))
            ->setVille($ville)
            ->setTotal(0)
            ->setBuildings(0)
            ->setUpgrades(0)
            ->setObjet(0)
            ->setBonusOd(0)
            ->setMaisonCitoyen(0)
            ->setGardiens(0)
            ->setVeilleurs(0)
            ->setAmes(0)
            ->setMorts(0)
            ->setTempos(0)
            ->setBonusSd(0);
    }
    
    private function createEmptyEstimation(int $day, Ville $ville): Estimation
    {
        return (new Estimation($day))
            ->setVille($ville)
            ->setMinJour(0)
            ->setMaxJour(0)
            ->setMaxedJour(0)
            ->setMinPlanif(0)
            ->setMaxPlanif(0)
            ->setMaxedPlanif(0);
    }
    
    private function createNewExpedition(ExpeditionMaj $expeMaj, array $listUsers, Ville $ville): void
    {
        $expeHordes = new ExpeHordes($ville->getJour());
        $arrayTrace = $this->generateTraceArray($expeMaj);
        
        $expeHordes->setNom($expeMaj->getName())
                   ->setLength($expeMaj->getLength())
                   ->setCitoyen($listUsers[$expeMaj->getAuthor()])
                   ->setTrace($arrayTrace);
        
        $ville->addExpeHorde($expeHordes);
    }
    
    private function createNewPicto(PictoMaj $pictoMaj, User $user): void
    {
        $pictoNew = new Pictos();
        $pictoNew->setPicto($this->listPicto[$pictoMaj->getIdPicto()])
                 ->setUser($user)
                 ->setNombre($pictoMaj->getNumber());
        $this->em->persist($pictoNew);
    }
    
    private function createNewUpChantier(UpgradeMaj $upMaj, Ville $ville): void
    {
        $chantierProto = $this->listChantier[$upMaj->getBuildingId()];
        $upChantierNew = new UpChantier($chantierProto);
        $upChantierNew->setLvlActuel($upMaj->getLevel())
                      ->addDays(array_fill(0, $upMaj->getLevel(), $ville->getJour() - 1));
        $ville->addUpChantier($upChantierNew);
    }
    
    private function createOrUpdateHistoriquePicto(User $user, ?int $mapId, PictoMaj $pictoMaj, int $number): void
    {
        // Recherche de l'historique du picto s'il existe
        $histobdd = $this->em->getRepository(HistoriquePictos::class)->findBy(['user' => $user, 'mapId' => $mapId * 10 + Ville::ORIGIN_MH, 'picto' => $this->listPicto[$pictoMaj->getIdPicto()]]);
        
        if ($histobdd !== null && count($histobdd) > 0) {
            $histo = $histobdd[0];
            if ($histo->getNombre() !== $number) {
                $histo->setNombre($number);
                $this->em->persist($histo);
            }
            // On supprime les autres valeurs qui sont des doublons
            for ($i = 1; $i < count($histobdd); $i++) {
                $this->em->remove($histobdd[$i]);
            }
        } else {
            $histo = new HistoriquePictos();
            $histo->setUser($user)
                  ->setMapId($mapId * 10 + Ville::ORIGIN_MH)
                  ->setPicto($this->listPicto[$pictoMaj->getIdPicto()])
                  ->setNombre($number);
            $this->em->persist($histo);
        }
        
    }
    
    private function finalizeUpdate(User $user): void
    {
        
        $user->setVilleMaj($user->getMapId());
        $user->setDateMajAme(new DateTime('Now'));
        $this->em->persist($user);
        
        $this->em->flush();
        $this->em->refresh($user);
    }
    
    private function generateTraceArray(ExpeditionMaj $expeMaj): array
    {
        $arrayTrace = [];
        foreach ($expeMaj->getPointsX() as $keyX => $x) {
            $arrayTrace[] = [$x, $expeMaj->getPointsY()[$keyX]];
        }
        return $arrayTrace;
    }
    
    /**
     * @param Pictos[] $pictosUser
     * @return Pictos[]
     */
    private function getArrayPictos(array $pictosUser): array
    {
        return count($pictosUser) > 0 ?
            array_combine(array_map(fn(Pictos $pictos): int => $pictos->getPicto()->getIdMh(), $pictosUser), $pictosUser) :
            [];
    }
    
    private function getDefenseMaj(MyHordesMaj $maj): ?DefenseMaj
    {
        return $maj->getMap()->getCity()->getDefense();
    }
    
    private function getDerniereVilleMapId(User $user): ?int
    {
        $derVille = $this->dataService->getDerniereVille($user);
        return $derVille?->getVille()->getMapId();
    }
    
    private function getEstimationMaj(MyHordesMaj $maj): ?EstimMaj
    {
        return $maj->getMap()->getCity()->getEstimations();
    }
    
    private function getEstimationMajNext(MyHordesMaj $maj): ?EstimMaj
    {
        return $maj->getMap()->getCity()->getEstimationsNext();
    }
    
    private function getLastDefense(Ville $ville): ?Defense
    {
        $defenseArray = $ville->getDefense()->toArray();
        return end($defenseArray) ?: null;
    }
    
    private function getLastEstimation(Ville $ville): ?Estimation
    {
        $estimationArray = $ville->getEstimation()->toArray();
        return end($estimationArray) ?: null;
    }
    
    private function getLastJournal(Ville $ville): ?Journal
    {
        $journalArray = $ville->getJournal()->toArray();
        return end($journalArray) ?: null;
    }
    
    /**
     * @param Ville $ville
     * @return UpChantier[]
     */
    private function getListUpExisting(Ville $ville): array
    {
        $listUpExisting = $ville->getUpChantier()->toArray();
        return array_combine(
            array_map(fn(UpChantier $up) => $up->getChantier()->getIdMh(), $listUpExisting),
            $listUpExisting,
        );
    }
    
    /**
     * @param MyHordesMaj $maj
     * @return UpgradeMaj[]
     */
    private function getListUpMaj(MyHordesMaj $maj): array
    {
        return $maj->getMap()->getCity()->getUpgrades();
    }
    
    private function getNewsMaj(MyHordesMaj $maj): ?NewsMaj
    {
        return $maj->getMap()->getCity()->getNews();
    }
    
    private function getOrCreateHistoriqueVille(VilleHistorique $villeHistoBdd, User $user): HistoriqueVille
    {
        $histoBdd = $villeHistoBdd->getHistoriqueByCitizen($user->getId());
        
        if ($histoBdd === null) {
            $histoBdd = new HistoriqueVille();
            $histoBdd->setUser($user);
            $villeHistoBdd->addHistoriqueByCitizen($histoBdd);
        }
        
        return $histoBdd;
    }
    
    private function getOrCreateVilleHistorique(VilleHistoriqueMaj $villeHisto, VilleHistoriqueRepository $villeHistoriqueRepo): VilleHistorique
    {
        $origin        = ($villeHisto->getOrigin() !== '') ? Ville::ORIGIN_H : Ville::ORIGIN_MH;
        $villeHistoBdd = $villeHistoriqueRepo->findOneBy(['mapid' => $villeHisto->getMapId(), 'origin' => $origin]);
        
        if ($villeHistoBdd === null) {
            $villeHistoBdd = new VilleHistorique();
        }
        
        $villeHistoBdd->setMapid($villeHisto->getMapId())
                      ->setOrigin($origin)
                      ->setSaison($villeHisto->getSeason())
                      ->setDerJour($villeHisto->getDureeVille())
                      ->setNom($villeHisto->getNomVille())
                      ->setPhase($villeHisto->getPhase());
        
        return $villeHistoBdd;
    }
    
    /**
     * @param MyHordesMaj $maj
     * @return ZoneMaj[]
     */
    private function getZonesMaj(MyHordesMaj $maj): array
    {
        $tempZonesMaj = $maj->getMap()->getZones();
        return array_combine(
            array_map(fn($i) => $i->getY() * 100 + $i->getX(), $tempZonesMaj),
            $tempZonesMaj,
        );
    }
    
    /**
     * @throws Exception
     */
    private function handleException(?Ville $ville, User $user, Exception $exception, string $context): void
    {
        
        $this->logger->error(($ville ? $ville->getMapId() : 'Unknown Map') . ' - Problème lors du ' . $context . ' - ' . $user->getIdMyHordes() . ' - ' . $exception->getMessage() . ' - ' . $exception->getTraceAsString());
        
        throw new Exception(
            $this->translator->trans('Problème lors de la mise à jour, veuillez réessayer, si cela persiste, veuillez contacter l\'administrateur.', [], 'service'),
        );
    }
    
    private function initializeDefenses(DefenseMaj $defenseMaj, Ville $ville, int $startDay): void
    {
        for ($i = $startDay; $i <= $ville->getJour() - 1; $i++) {
            $defenseNew = $this->createEmptyDefense($i, $ville);
            $ville->addDefense($defenseNew);
        }
        
        $this->addNewDefense($defenseMaj, $ville);
    }
    
    private function initializeEstimations(callable $functionMajEstimation, ?EstimMaj $estimJourMaj, ?EstimMaj $estimJourNextMaj, Ville $ville, int $startDay): void
    {
        for ($i = $startDay; $i <= $ville->getJour() - 1; $i++) {
            $estimation = $this->createEmptyEstimation($i, $ville);
            $ville->addEstimation($estimation);
        }
        
        $this->addNewEstimation($functionMajEstimation, $estimJourMaj, $estimJourNextMaj, $ville);
    }
    
    private function initializeJournals(callable $functionMajNews, ?Journal $journalExistant, ?NewsMaj $newsJourMaj, NewsMaj $newsJourMajVide, NewsMaj $newsJourMajJour1, Ville $ville): void
    {
        $startDay = ($journalExistant ? $journalExistant->getDay() + 1 : 1);
        
        for ($i = $startDay; $i <= $ville->getJour() - 1; $i++) {
            $journalNew = new Journal($i);
            if ($i == 1) {
                $functionMajNews($journalNew, $newsJourMajJour1);
            } else {
                $functionMajNews($journalNew, $newsJourMajVide);
            }
            $ville->addJournal($journalNew);
        }
        
        $this->addNewJournal($functionMajNews, $newsJourMaj, $ville);
    }
    
    private function initializeZones(Ville $ville): void
    {
        $kmMax  = 0;
        $paMax  = 0;
        $mapTab = [];
        for ($y = 0; $y < $ville->getHeight(); $y++) {
            for ($x = 0; $x < $ville->getHeight(); $x++) {
                $zoneNew = new ZoneMap();
                $zoneNew->createId($x, $y, $ville->getId())
                        ->setX($x)
                        ->setY($y)
                        ->setVue(ZoneMap::NON_EXPLO);
                if ($ville->getPosX() === $x && $ville->getPosY() === $y) {
                    $zoneNew->setVue(ZoneMap::VILLE);
                }
                $this->processZoneAttributes($zoneNew, $ville, $kmMax, $paMax);
                $mapTab[$zoneNew->getXRel()][$zoneNew->getYRel()] = $zoneNew;
                $ville->addZone($zoneNew);
            }
        }
        $this->calculateZoneBorders($ville, $mapTab);
        $ville->setMaxKm($kmMax)
              ->setMaxPa($paMax);
    }
    
    private function majCitoyens(Citoyens $citoyen, CitoyensMaj $citoyensMaj, array $listUserName, Ville $ville, array $listUsers, array $listDeathTypeIndex, array $listCleanUpCadaverIndex, array $listHomeProtoIndex): void
    {
        if ($citoyensMaj->getSurvival() !== null) {
            $citoyen->setMort(true)
                    ->setX(null)
                    ->setY(null)
                    ->setMessage($citoyensMaj->getMsg())
                    ->setDeathDay($citoyensMaj->getSurvival())
                    ->setDeathType($listDeathTypeIndex[$citoyensMaj->getDtype()] ?? null);
            if ($citoyensMaj->getCleanupType() != null) {
                $citoyen->setCleanUpType($listCleanUpCadaverIndex[$citoyensMaj->getCleanupType()] ?? null);
                if ($citoyensMaj->getcleanupName() != null) {
                    $citoyen->setCleanUpName($listUsers[$listUserName[$citoyensMaj->getcleanupName()]] ?? null);
                }
            } else if ($citoyensMaj->getcleanupName() != null) {
                $citoyen->setCleanUpName($listUsers[$listUserName[$citoyensMaj->getcleanupName()]] ?? null);
                $citoyen->setCleanUpType($listCleanUpCadaverIndex["burn"] ?? null);
            }
        } else {
            $citoyen->setMort(false)
                    ->setBan($citoyensMaj->isBan())
                    ->setDehors($citoyensMaj->isOut())
                    ->setMessage($citoyensMaj->getHomeMessage())
                    ->setIncarner(!$citoyensMaj->isGhost())
                    ->setJob($this->listJob[$citoyensMaj->getJob()] ?? $this->listJob['looser']);
            if ($ville->isDevast()) {
                $citoyen->setX(null)
                        ->setY(null);
            } else {
                $citoyen->setX($citoyensMaj->getX())
                        ->setY($citoyensMaj->getY());
            }
        }
        $citoyen->setLvlMaison($listHomeProtoIndex[$citoyensMaj->getBaseDef() ?? 0] ?? $listHomeProtoIndex[0]);
        if ($citoyen->getJob() === null) {
            $citoyen->setJob($this->listJob['looser']);
        }
    }
    
    /**
     * @param ExpeditionMaj[] $expeMajs
     * @param array $listExpeExisting
     * @param array $listUsers
     * @param Ville $ville
     * @return void
     */
    private function processExpeditions(array $expeMajs, array &$listExpeExisting, array $listUsers, Ville $ville): void
    {
        foreach ($expeMajs as $expeMaj) {
            if (isset($listExpeExisting[$expeMaj->getAuthor() . "_" . $expeMaj->getName()])) {
                $expeActuel = $listExpeExisting[$expeMaj->getAuthor() . "_" . $expeMaj->getName()];
                $this->updateExistingExpedition($expeActuel, $expeMaj, $listUsers, $ville);
                unset($listExpeExisting[$expeMaj->getAuthor() . "_" . $expeMaj->getName()]);
            } else {
                if ($expeMaj->getAuthor() !== null) {
                    $this->createNewExpedition($expeMaj, $listUsers, $ville);
                }
            }
        }
    }
    
    /**
     * @param PictoMaj $pictoMaj
     * @param User $user
     * @param Pictos[] $arrayPictos
     * @param bool $topMajHisto
     * @param int|null $mapIdDerVille
     * @return void
     * @throws GuzzleException
     */
    private function processPictoMaj(PictoMaj $pictoMaj, User $user, array &$arrayPictos, array &$newTitreDiscord): void
    {
        $idPicto = $pictoMaj->getIdPicto();
        
        if (isset($arrayPictos[$idPicto])) {
            $pictoExistant = $arrayPictos[$idPicto];
            $this->updateExistingPicto($pictoExistant, $pictoMaj, $user);
            unset($arrayPictos[$idPicto]);
        } else {
            $this->createNewPicto($pictoMaj, $user);
        }
        
        // Verifications si les titres ont été totalement découverts par ce joueur
        foreach ($pictoMaj->getTitres() as $titre) {
            $titreExist = $this->em->getRepository(PictoTitrePrototype::class)->findOneBy(['titre' => $titre]);
            
            if ($titreExist === null) {
                
                $newTitreDiscord[] = "{$this->listPicto[$idPicto]->getName()} - {$titre}";
                
                $titreNew = new PictoTitrePrototype();
                $titreNew->setTitre($titre)
                         ->setPictoPrototype($this->listPicto[$idPicto]);
                
                $this->em->persist($titreNew);
                $this->em->flush();
                $this->em->refresh($titreNew);
                
                
                // On rajoute une ligne dans la table de découverte de titre
                $titreDecouvert = new DecouverteTitre();
                $titreDecouvert->setPicto($this->listPicto[$idPicto])
                               ->setTitre($titreNew)
                               ->setUser($user)
                               ->setDecouvertAt(new DateTimeImmutable());
                
                $this->em->persist($titreDecouvert);
                
                
            } else {
                // On verifie que le titre a pas déjà été découvert par le joueur
                $titreDecouvert = $this->em->getRepository(DecouverteTitre::class)->findOneBy(['picto' => $this->listPicto[$idPicto], 'titre' => $titreExist, 'user' => $user]);
                
                if ($titreDecouvert === null) {
                    $titreDecouvert = new DecouverteTitre();
                    $titreDecouvert->setPicto($this->listPicto[$idPicto])
                                   ->setTitre($titreExist)
                                   ->setUser($user)
                                   ->setDecouvertAt(new DateTimeImmutable());
                    
                    $this->em->persist($titreDecouvert);
                }
            }
        }
        
    }
    
    /**
     * @param VilleHistoriqueMaj[] $listVille
     * @param User $user
     * @return void
     */
    private function processVilles(array $listVille, User $user): void
    {
        $villeHistoriqueRepo = $this->em->getRepository(VilleHistorique::class);
        $villeRepo           = $this->em->getRepository(Ville::class);
        
        foreach ($listVille as $villeHisto) {
            $villeHistoBdd = $this->getOrCreateVilleHistorique($villeHisto, $villeHistoriqueRepo);
            $histoBdd      = $this->getOrCreateHistoriqueVille($villeHistoBdd, $user);
            
            $this->updateHistoriqueVille($histoBdd, $villeHisto, $user);
            
            if ($villeHisto->getOrigin() === '') {
                $this->updateVilleFromHistorique($villeHisto, $histoBdd, $villeRepo, $user);
            }
            
            // On balaye les rewards reçu pour la ville
            foreach ($villeHisto->getRewards() as $reward) {
                if ($reward !== null) {
                    $this->createOrUpdateHistoriquePicto($user, $villeHisto->getMapId(), $reward, $reward?->getNumber() ?? 0);
                }
            }
            
            
            $this->em->persist($villeHistoBdd);
        }
    }
    
    private function processZoneAttributes(ZoneMap $zone, Ville $ville, int &$kmMax, int &$paMax): void
    {
        $this->carteHandlerV2->calculCoorRelatif($zone, $ville);
        $this->carteHandlerV2->calculKmCase($zone, $kmMax);
        $this->carteHandlerV2->calculPaCase($zone, $paMax);
        $this->carteHandlerV2->calculDirectionCase($zone);
        $this->carteHandlerV2->calculZonageCase($zone);
    }
    
    private function removeRemainingExpeditions(array $listExpeExisting, Ville $ville): void
    {
        foreach ($listExpeExisting as $expeToRemove) {
            $ville->removeExpeHorde($expeToRemove);
        }
    }
    
    /**
     * @param Pictos[] $arrayPictos
     * @return void
     */
    private function removeRemainingPictos(array $arrayPictos): void
    {
        foreach ($arrayPictos as $pictoToRemove) {
            $this->em->remove($pictoToRemove);
        }
    }
    
    private function shouldUpdateAll(Ville $ville): bool
    {
        return $ville->getJour() > 1 && (!$ville->isMajAllOldTown() || !$ville->isMajAllPicto());
    }
    
    private function shouldUpdatePictos(User $user): bool
    {
        return ($user->getMapId() === null) || ($user->getMapId() !== null && $user->getVilleMaj() !== $user->getMapId());
    }
    
    private function updateCitizens(array $citoyensAllMaj, array $citoyensExistingIndex, Ville $ville, array $listUsers, array $listHomeProtoIndex, array $listDeathTypeIndex, array $listCleanUpCadaverIndex): void
    {
        $listName = array_map(fn(User $i) => $i->getPseudo(), $listUsers);
        $nameToId = array_combine($listName, array_keys($listUsers));
        
        foreach ($citoyensAllMaj as $citoyenMaj) {
            if (!isset($citoyensExistingIndex[$citoyenMaj->getId()])) {
                $citoyenNew = new Citoyens();
                $citoyenNew->setCitoyen($listUsers[$citoyenMaj->getId()]);
                $this->majCitoyens($citoyenNew, $citoyenMaj, $nameToId, $ville, $listUsers, $listDeathTypeIndex, $listCleanUpCadaverIndex, $listHomeProtoIndex);
                $ville->addCitoyen($citoyenNew);
            } else {
                $citoyenActuel = $citoyensExistingIndex[$citoyenMaj->getId()];
                $this->majCitoyens($citoyenActuel, $citoyenMaj, $nameToId, $ville, $listUsers, $listDeathTypeIndex, $listCleanUpCadaverIndex, $listHomeProtoIndex);
            }
        }
    }
    
    /**
     * @throws Exception
     */
    private function updateCitoyensDetails(MyHordesMaj $maj, Ville $ville): void
    {
        $citoyensMaj           = $maj->getMap()->getCitizens();
        $citoyensExisting      = $ville->getCitoyens()->toArray();
        $citoyensExistingIndex = array_combine(
            array_map(fn(Citoyens $citoyen) => $citoyen->getCitoyen()->getIdMyHordes(), $citoyensExisting),
            $citoyensExisting,
        );
        
        foreach ($citoyensMaj as $citoyen) {
            $userIdMaj = $citoyen->getId();
            $userMaj   = $citoyensExistingIndex[$userIdMaj]->getCitoyen();
            
            if ($ville->isMajAllPicto()) {
                $this->updatePictoForUser($citoyen, $userMaj);
            }
            
            if ($ville->isMajAllOldTown()) {
                $this->updateOldVilleForUser($citoyen, $userMaj);
            }
        }
    }
    
    private function updateDefenseFromMaj(DefenseMaj $defenseMaj, Defense $defense): void
    {
        $defense->setTotal($defenseMaj->getTotal())
                ->setBuildings($defenseMaj->getBuildings())
                ->setUpgrades($defenseMaj->getUpgrades())
                ->setObjet($defenseMaj->getItems())
                ->setBonusOd($defenseMaj->getItemsMul() * 10)
                ->setMaisonCitoyen($defenseMaj->getCitizenHomes())
                ->setGardiens($defenseMaj->getCitizenGuardians())
                ->setVeilleurs($defenseMaj->getWatchmen())
                ->setAmes($defenseMaj->getSouls() ?? 0)
                ->setMorts($defenseMaj->getCadavers() ?? 0)
                ->setTempos($defenseMaj->getTemp());
        
        $sD = $defense->getTotal() - 10 - $defense->getBuildings() - $defense->getUpgrades() -
              $defense->getMaisonCitoyen() - $defense->getGardiens() - $defense->getVeilleurs() -
              $defense->getAmes() - $defense->getMorts() - $defense->getTempos() -
              min(500, round($defense->getObjet() * $defense->getBonusOd() / 10));
        
        $defense->setBonusSd($sD);
    }
    
    private function updateDefenses(DefenseMaj $defenseMaj, ?Defense $defense, Ville $ville): void
    {
        $countDef = count($ville->getDefense()->toArray());
        
        if ($countDef === 0 || $defense->getDay() < $ville->getJour() - 1) {
            $this->initializeDefenses($defenseMaj, $ville, $defense?->getDay() ?? 1);
        } elseif ($defense->getDay() + 1 == $ville->getJour()) {
            $this->addNewDefense($defenseMaj, $ville);
        } elseif ($defense->getDay() === $ville->getJour()) {
            $this->updateExistingDefense($defenseMaj, $defense);
        }
    }
    
    private function updateEstimations(callable $functionMajEstimation, ?Estimation $estimationExistante, ?EstimMaj $estimJourMaj, ?EstimMaj $estimJourNextMaj, Ville $ville): void
    {
        $countEstim = count($ville->getEstimation()->toArray());
        
        if ($countEstim === 0 || ($estimationExistante && $estimationExistante->getDay() < $ville->getJour() - 1)) {
            $this->initializeEstimations($functionMajEstimation, $estimJourMaj, $estimJourNextMaj, $ville, $estimationExistante ? $estimationExistante->getDay() + 1 : 1);
        } elseif ($estimationExistante && $estimationExistante->getDay() + 1 == $ville->getJour()) {
            $this->addNewEstimation($functionMajEstimation, $estimJourMaj, $estimJourNextMaj, $ville);
        } elseif ($estimationExistante && $estimationExistante->getDay() === $ville->getJour()) {
            $functionMajEstimation($estimationExistante, $estimJourMaj, $estimJourNextMaj);
        }
    }
    
    private function updateExistingDefense(DefenseMaj $defenseMaj, Defense $defense): void
    {
        $this->updateDefenseFromMaj($defenseMaj, $defense);
    }
    
    private function updateExistingExpedition(ExpeHordes $expeActuel, ExpeditionMaj $expeMaj, array $listUsers, Ville $ville): void
    {
        $arrayTrace = $this->generateTraceArray($expeMaj);
        
        if ($expeActuel->getTrace() !== $arrayTrace ||
            $expeActuel->getDay() != $ville->getJour() ||
            $expeActuel->getLength() != $expeMaj->getLength()) {
            
            $expeActuel->setNom($expeMaj->getName())
                       ->setLength($expeMaj->getLength())
                       ->setCitoyen($listUsers[$expeMaj->getAuthor()])
                       ->setTrace($arrayTrace);
        }
    }
    
    private function updateExistingPicto(Pictos $pictoExistant, PictoMaj $pictoMaj, User $user): void
    {
        if ($pictoExistant->getNombre() != $pictoMaj->getNumber()) {
            $pictoExistant->setNombre($pictoMaj->getNumber());
            $this->em->persist($pictoExistant);
        }
    }
    
    private function updateExistingUpChantier(UpChantier $upChantierActuel, UpgradeMaj $upMaj, Ville $ville): void
    {
        if ($upChantierActuel->isDestroy()) {
            $upChantierActuel->setDestroy(false)
                             ->addDays([]);
        }
        
        if ($upChantierActuel->getLvlActuel() != $upMaj->getLevel()) {
            if ($upChantierActuel->getLvlActuel() === 0) {
                $lvlStart = 0;
            } else {
                $lvlStart = $upChantierActuel->getLvlActuel() - 1;
            }
            $lastHistoryIndex = count($upChantierActuel->getDays()) - 1;
            $historiqueDay    = $upChantierActuel->getDays();
            if ($lastHistoryIndex >= 0) {
                $arrDaySupp                       = array_fill($lvlStart, $upMaj->getLevel() - $upChantierActuel->getLvlActuel(), $ville->getJour() - 1);
                $historiqueDay[$lastHistoryIndex] = array_merge($historiqueDay[$lastHistoryIndex], $arrDaySupp);
                
                $upChantierActuel->setLvlActuel($upMaj->getLevel())->setDays($historiqueDay);
            }
        }
    }
    
    /**
     * @param ZoneMaj[] $zonesMaj
     * @param Ville $ville
     * @param array $listUsers
     * @param Ruines[] $ruinesExisting
     * @param MyHordesMaj $maj
     * @param User $user
     * @return void
     */
    private function updateExistingZones(array $zonesMaj, Ville $ville, array $listUsers, array $ruinesExisting, MyHordesMaj $maj, User $user): void
    {
        foreach ($ville->getZone()->toArray() as $zone) {
            $zoneKey = $zone->getY() * 100 + $zone->getX();
            if (isset($zonesMaj[$zoneKey])) {
                $zoneMaj = $zonesMaj[$zoneKey];
                $this->updateZone($zone, $zoneMaj, $ville, $listUsers, $ruinesExisting, $maj, $user);
            }
        }
    }
    
    private function updateHistoriqueVille(HistoriqueVille $histoBdd, VilleHistoriqueMaj $villeHisto, User $user): void
    {
        $histoBdd->setUser($user)
                 ->setMsg($villeHisto->getMessageMort())
                 ->setScore($villeHisto->getPointAme())
                 ->setDayOfDeath($villeHisto->getJourMort())
                 ->setTypeMort($this->listMort[$villeHisto->getTypeMort() ?? TypeDeath::Inconnue] ?? $this->listMort[TypeDeath::Inconnue]);
        
        if ($villeHisto->getcleanupType() !== null) {
            $histoBdd->setCleanTypeHisto($this->listCleanUpType[$villeHisto->getcleanupType()] ?? $this->listCleanUpType['garbage']);
        }
        if ($villeHisto->getcleanupName() !== null) {
            $histoBdd->setCleanName($villeHisto->getcleanupName());
        }
    }
    
    private function updateJournals(callable $functionMajNews, ?Journal $journalExistant, ?NewsMaj $newsJourMaj, NewsMaj $newsJourMajVide, NewsMaj $newsJourMajJour1, Ville $ville): void
    {
        $countJournal = count($ville->getJournal()->toArray());
        
        if ($ville->getJour() !== 1) {
            if ($countJournal === 0 || ($journalExistant && $journalExistant->getDay() < $ville->getJour() - 1)) {
                $this->initializeJournals($functionMajNews, $journalExistant, $newsJourMaj, $newsJourMajVide, $newsJourMajJour1, $ville);
            } elseif ($journalExistant && $journalExistant->getDay() + 1 == $ville->getJour()) {
                $this->addNewJournal($functionMajNews, $newsJourMaj, $ville);
            }
        } elseif ($countJournal === 0) {
            $journalNew = new Journal(1);
            $functionMajNews($journalNew, $newsJourMajJour1);
            $ville->addJournal($journalNew);
        }
    }
    
    /**
     * @throws Exception
     */
    private function updateOldVilleForUser(CitoyensMaj $citoyen, User $user): void
    {
        $this->updateOldVille($citoyen->getPlayedMap(), $user);
    }
    
    private function updateOrCreateUpChantier(UpgradeMaj $upMaj, array $listUpExisting, Ville $ville): void
    {
        if (isset($listUpExisting[$upMaj->getBuildingId()])) {
            $upChantierActuel = $listUpExisting[$upMaj->getBuildingId()];
            $this->updateExistingUpChantier($upChantierActuel, $upMaj, $ville);
        } else {
            $this->createNewUpChantier($upMaj, $ville);
        }
    }
    
    /**
     * @throws Exception
     */
    private function updatePictoForUser(Citoyens $citoyen, User $user): void
    {
        $this->updatePicto($citoyen->getPictos(), $user);
    }
    
    private function updatePlansChantiers(ChantierPrototype $chantier, Ville &$ville, User $user): void
    {
        
        // On vérifie que le chantier n'est pas présent dans la liste des plans de chantiers de la ville
        $chantierExistant = $ville->getPlansChantiers()->filter(function (PlansChantier $plan) use ($chantier) {
            return $plan->getChantier()->getId() === $chantier->getId();
        })->first();
        
        if (!$chantierExistant || $ville->getPlansChantiers()->isEmpty()) {
            $planChantier = new PlansChantier();
            $planChantier->setChantier($this->listChantier[$chantier->getId()]);
            $ville->addPlansChantier($planChantier)
                  ->setPlansChantierDateMaj(new DateTime('now'))
                  ->setPlansChantierUpdateBy($user);
        }
        
    }
    
    /**
     * @param int $x
     * @param int $y
     * @param BatPrototype $batProto
     * @param Ville $ville
     * @param Ruines[] $ruinesExisting
     * @return void
     */
    private function updateRuine(int $x, int $y, BatPrototype $batProto, Ville $ville, array $ruinesExisting): void
    {
        $topCreation = true;
        foreach ($ruinesExisting as $ruine) {
            if ($ruine->getX() === $x && $ruine->getY() === $y && $ruine->getBat() === $batProto) {
                $topCreation = false;
                break;
            }
        }
        
        if ($topCreation || count($ruinesExisting) === 0) {
            $ruineNew = new Ruines();
            $ruineNew->setY($y)
                     ->setX($x)
                     ->setBat($batProto);
            $ville->addRuine($ruineNew);
        }
    }
    
    /**
     * @param UpgradeMaj[] $listUpMaj
     * @param array $listUpExisting
     * @param Ville $ville
     * @return void
     */
    private function updateUpgrades(array $listUpMaj, array $listUpExisting, Ville $ville): void
    {
        /** @var UpChantier[] $arrayChantier */
        $arrayChantier = count($listUpExisting) != 0 ? array_combine(array_map(fn(UpChantier $upChantier) => $upChantier->getChantier()->getIdMh(), $listUpExisting), $listUpExisting) : [];
        
        foreach ($listUpMaj as $upMaj) {
            $this->updateOrCreateUpChantier($upMaj, $listUpExisting, $ville);
            unset($arrayChantier[$upMaj->getBuildingId()]);
        }
        
        // On met à jour la zone destroy
        foreach ($arrayChantier as $upChantier) {
            $upChantier->setDestroy(true)->setLvlActuel(0);
        }
    }
    
    private function updateUserCitoyensDetails(User $user, CitoyensMaj $userMaj, Ville $ville): void
    {
        if ($user->getPseudo() !== $userMaj->getName() ||
            $user->getAvatar() !== $userMaj->getAvatar() ||
            $user->getMapId() !== $userMaj->getMapId()) {
            
            $user->setPseudo($userMaj->getName())
                 ->setAvatar($userMaj->getAvatar());
            
            if (!$userMaj->isDead() && ($userMaj->getMapId() !== null || $user->getMapId() === $ville->getMapId())) {
                $user->setMapId($userMaj->getMapId());
            }
            
            $this->em->persist($user);
            $this->em->flush();
            $this->em->refresh($user);
        }
    }
    
    /**
     * @param CitoyensMaj[] $citoyensAllMaj
     * @param array $listUsers
     * @param Ville $ville
     * @return void
     */
    private function updateUsers(array $citoyensAllMaj, array &$listUsers, Ville $ville): void
    {
        foreach ($citoyensAllMaj as $userMaj) {
            if (!isset($listUsers[$userMaj->getId()])) {
                $newUser = new NewUser();
                $newUser->setIdMh($userMaj->getId())
                        ->setAvatar($userMaj->getAvatar())
                        ->setLang('fr')
                        ->setPseudo($userMaj->getName())
                        ->setMapId($userMaj->getMapId());
                $user = $this->userHandler->createNewUserFromApi($newUser);
            } else {
                $user = $listUsers[$userMaj->getId()];
                $this->updateUserCitoyensDetails($user, $userMaj, $ville);
            }
            $listUsers[$userMaj->getId()] = $user;
        }
    }
    
    private function updateVilleFromHistorique(VilleHistoriqueMaj $villeHisto, HistoriqueVille $histoBdd, VilleRepository $villeRepo, User $user): void
    {
        $villeBdd = $villeRepo->findOneBy(['mapId' => $villeHisto->getMapId(), 'origin' => Ville::ORIGIN_MH]);
        
        if ($villeBdd !== null) {
            $histoBdd->setVille($villeBdd);
            
            $citoyen = $villeBdd->getCitoyen($user->getId());
            $citoyen?->setMort(true)
                    ?->setDeathDay($villeHisto->getJourMort())
                    ?->setDeathType($histoBdd->getTypeMort())
                    ?->setCleanUpType($histoBdd->getCleanTypeHisto());
            
            if ($villeBdd->getJour() < $villeHisto->getJourMort()) {
                $villeBdd->setJour($villeHisto->getJourMort());
            }
            
            $villeBdd->setPhase($villeHisto->getPhase());
            $this->em->persist($villeBdd);
        }
    }
    
    private function updateVilleSpecialists(MyHordesMaj $maj, Ville $ville, array $listUsers): void
    {
        $chaman = ($maj->getMap()->getShaman() == null) ? null : $listUsers[$maj->getMap()->getShaman()] ?? null;
        $gom    = ($maj->getMap()->getGuide() == null) ? null : $listUsers[$maj->getMap()->getGuide()] ?? null;
        if ($ville->getChaman() !== $chaman) {
            $ville->setChaman($chaman);
        }
        if ($ville->getGuide() !== $gom) {
            $ville->setGuide($gom);
        }
    }
    
    /**
     * @param ZoneMap $zone
     * @param ZoneMaj $zoneMaj
     * @param Ville $ville
     * @param array $listUsers
     * @param Ruines[] $ruinesExisting
     * @param MyHordesMaj $maj
     * @param User $user
     * @return void
     */
    private function updateZone(ZoneMap $zone, ZoneMaj $zoneMaj, Ville $ville, array $listUsers, array $ruinesExisting, MyHordesMaj $maj, User $user): void
    {
        $x = $zone->getX();
        $y = $zone->getY();
        
        if (!$zoneMaj->getNvt() || ($zoneMaj->getDetails()?->getH() !== null && $zoneMaj->getDetails()->getH() > 0)) {
            $zone->setVue(ZoneMap::CASE_VUE)
                 ->setDanger($zoneMaj->getDanger())
                 ->setTag($zoneMaj->getTag())
                 ->setPdc($zoneMaj->getDetails()->getH() ?? null);
            
            if ($zoneMaj->getDetails()->getZ() !== null) {
                $zone->setZombie($zoneMaj->getDetails()->getZ());
            }
            
            if ($zoneMaj->getBuilding() !== null) {
                $batProto = $this->listBat[$zoneMaj->getBuilding()->getType()];
                $zone->setBat($batProto)
                     ->setDig($zoneMaj->getBuilding()->getDig() ?? 0);
                if ($batProto->isExplorable()) {
                    $this->updateRuine($x, $y, $batProto, $ville, $ruinesExisting);
                }
            }
            
            if (!$ville->isDevast() && ($zoneMaj->getX() == $maj->getX() && $zoneMaj->getY() == $maj->getY())) {
                $this->updateZoneItems($zone, $zoneMaj, $ville, $listUsers, $user);
            }
        } else {
            if ($zoneMaj->getBuilding() !== null) {
                $batProto = $this->listBat[$zoneMaj->getBuilding()->getType()];
                $zone->setBat($batProto)
                     ->setDig($zoneMaj->getBuilding()->getDig() ?? 0);
                if ($batProto->isExplorable()) {
                    $this->updateRuine($x, $y, $batProto, $ville, $ruinesExisting);
                }
            }
            $zone->setPdc(0)
                 ->setVue(ZoneMap::CASE_NONVUE);
        }
        
        if ($zone->getY() === $ville->getPosY() && $zone->getX() === $ville->getPosX()) {
            $zone->setVue(ZoneMap::VILLE);
        }
    }
    
    private function updateZoneItems(ZoneMap $zone, ZoneMaj $zoneMaj, Ville $ville, array $listUsers, User $user): void
    {
        $zone->setDried($zoneMaj->getDetails()->getDried() ?? null)
             ->setDay($ville->getJour())
             ->setMajAt(new DateTimeImmutable('NOW'))
             ->setHeureMaj((new DateTime('NOW'))->format('H:i:s'))
             ->setCitoyen($listUsers[$user->getIdMyHordes()]);
        
        if ($zoneMaj->getBuilding() !== null) {
            $zone->setEmpty($zoneMaj->getBuilding()->getDried() ?? false)
                 ->setCamped($zoneMaj->getBuilding()->getCamped() ?? false);
        }
        
        $itemsZone            = $zone->getItems()->toArray();
        $mapItemArrayExisting = array_combine(
            array_map(fn(MapItem $item) => $item->getItem()->getIdMh() * 10 + ($item->getBroked() ? 1 : 0), $itemsZone),
            $itemsZone,
        );
        
        foreach ($zoneMaj->getItems() as $itemMaj) {
            $idGen = $itemMaj->getId() * 10 + ($itemMaj->isBroken() ? 1 : 0);
            if (isset($mapItemArrayExisting[$idGen])) {
                $itemExist = $mapItemArrayExisting[$idGen];
                $itemExist->setNombre($itemMaj->getCount());
                unset($mapItemArrayExisting[$idGen]);
            } else {
                $itemNew = new MapItem($this->listObjet[$itemMaj->getId()], $itemMaj->isBroken());
                $itemNew->setNombre($itemMaj->getCount());
                $zone->addItem($itemNew);
            }
        }
        
        foreach ($mapItemArrayExisting as $itemRestant) {
            $zone->removeItem($itemRestant);
        }
    }
    
}
