<?php


namespace App\Service\Ville;


use App\Entity\ItemPrototype;
use App\Entity\Ruines;
use App\Entity\RuinesCases;
use App\Entity\RuinesObjets;
use App\Entity\RuinesPlans;
use App\Entity\User;
use App\Exception\GestHordesException;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Contracts\Translation\TranslatorInterface;

class RuineService
{
    public function __construct(protected EntityManagerInterface $em, protected TranslatorInterface $translator)
    {
    }
    
    /**
     * @throws GestHordesException
     */
    public function miseAJourCase(RuinesCases $case, User $user): RuinesCases
    {
        try {
            // on récupère la case actuelle de la ruine pour récupérer les objets actuellement présent sur la case. Et on met à jour les objets
            $caseActuelle = $this->em->getRepository(RuinesCases::class)->findOneBy(['id' => $case->getId()]);
            
            // On met dans un tableau les objets qui vont être maj pour pouvoir supprimer, ajouter, modifier de la case actuelle
            $newItemCaseRuines = [];
            foreach ($case->getItems() as $itemCaseRuine) {
                $idItem                     = $itemCaseRuine->getItem()->getId() * 10 + ($itemCaseRuine->isBroken() ? 1 : 0);
                $newItemCaseRuines[$idItem] = $itemCaseRuine;
            }
            
            // On balaye les objets de la case actuelle pour les supprimer, modifier
            foreach ($caseActuelle->getItems() as $itemCaseRuine) {
                $idItem = $itemCaseRuine->getItem()->getId() * 10 + ($itemCaseRuine->isBroken() ? 1 : 0);
                if (isset($newItemCaseRuines[$idItem])) {
                    // On met à jour l'objet
                    $itemCaseRuine->setNombre($newItemCaseRuines[$idItem]->getNombre());
                    // On supprime l'objet du tableau pour ne pas le traiter plus tard
                    unset($newItemCaseRuines[$idItem]);
                } else {
                    // On supprime l'objet
                    $caseActuelle->removeItem($itemCaseRuine);
                }
            }
            
            // on balaye le reste des objets non supprimer de newItems pour les ajouter à la case actuelle
            foreach ($newItemCaseRuines as $itemCaseRuine) {
                
                // on récupère l'objet pour insérer en base de donnée
                $itemPrototype = $this->em->getRepository(ItemPrototype::class)->findOneBy(['id' => $itemCaseRuine->getItem()->getId()]);
                
                $newItemCaseRuine = new RuinesObjets();
                $newItemCaseRuine->setNombre($itemCaseRuine->getNombre())
                                 ->setBroken($itemCaseRuine->isBroken())
                                 ->setItem($itemPrototype);
                
                
                $caseActuelle->addItem($newItemCaseRuine);
            }
            
            // on met à jour qui a fait la modification et quand
            $caseActuelle->setUpdateBy($user)
                         ->setUpdateAt(new DateTimeImmutable('NOW'));
            
            $this->em->persist($caseActuelle);
            $this->em->flush();
            
            return $caseActuelle;
        } catch (Exception $exception) {
            throw new GestHordesException($exception->getMessage(), $exception->getCode(), $exception);
        }
        
    }
    
    public function recuperationObjetRuine(RuinesPlans $ruinesPlans): array
    {
        $allItems = [];
        
        // On balaye l'entièreter des cases de la ruine puis de chaque objet pour en faire l'inventaire
        foreach ($ruinesPlans->getCases() as $case) {
            foreach ($case->getItems() as $item) {
                $idItem = $item->getItem()->getId() * 10 + ($item->isBroken() ? 1 : 0);
                if (isset($allItems[$idItem])) {
                    $allItems[$idItem]['count'] += $item->getNombre();
                } else {
                    $allItems[$idItem] = [
                        'item'   => $item->getItem(),
                        'count'  => $item->getNombre(),
                        'broken' => $item->isBroken(),
                    ];
                }
            }
        }
        
        return $allItems;
    }
    
    /**
     * @return void
     */
    public function traitementFilAriane(array &$filAriane, array $coord): void
    {
        
        if (count($coord) == 1) {
            $x = $coord[0][0];
            $y = $coord[0][1];
            $z = $coord[0][2];
            
            $filAriane[$z][$y][$x][0] = 20;
        } else {
            for ($c = 0; $c < count($coord) - 1; $c++) {
                // Coordonnée point de départ
                $xd = (int)$coord[$c][0];
                $yd = (int)$coord[$c][1];
                $zd = (int)$coord[$c][2];
                // Coordonnée point d'arrivée
                $xa = (int)$coord[$c + 1][0];
                $ya = (int)$coord[$c + 1][1];
                $za = (int)$coord[$c + 1][2];
                
                
                if ($zd === $za) {
                    // Deplacement NORD/SUD
                    if ($xd == $xa) {
                        // Déplacement vers le NORD
                        if ($ya < $yd) {
                            $key                            = array_key_last(($filAriane[$zd][$yd][$xd]));
                            $filAriane[$zd][$yd][$xd][$key] = match ($filAriane[$zd][$yd][$xd][$key]) {
                                16   => 14,
                                17   => 1,
                                18   => 5,
                                19   => 8,
                                null => 22,
                            };
                            
                            // Déplacement sur les cases du milieu
                            for ($o = $ya + 1; $o < $yd; $o++) {
                                $filAriane[$zd][$o][$xd][] = 1;
                            }
                            // Détermination case d'arrivée
                            $filAriane[$zd][$ya][$xa][] = 17;
                        }
                        // Déplacement vers le SUD
                        if ($ya > $yd) {
                            $key                            = array_key_last($filAriane[$zd][$yd][$xd]);
                            $filAriane[$zd][$yd][$xd][$key] = match ($filAriane[$zd][$yd][$xd][$key]) {
                                16       => 0,
                                17       => 15,
                                18       => 11,
                                19       => 7,
                                20, null => 20,
                            };
                            
                            // Déplacement sur les cases du milieu
                            for ($o = $yd + 1; $o < $ya; $o++) {
                                $filAriane[$zd][$o][$xd][] = 0;
                            }
                            // Détermination case d'arrivée
                            $filAriane[$zd][$ya][$xa][] = 16;
                        }
                        
                    }
                    
                    // Deplacement OUEST/EST
                    if ($yd == $ya) {
                        // Déplacement vers l'OUEST
                        if ($xa < $xd) {
                            $key                            = array_key_last(($filAriane[$zd][$yd][$xd]));
                            $filAriane[$zd][$yd][$xd][$key] = match ($filAriane[$zd][$yd][$xd][$key]) {
                                16   => 4,
                                17   => 10,
                                18   => 2,
                                19   => 12,
                                null => 21,
                            };
                            
                            // Déplacement sur les cases du milieu
                            for ($o = $xa + 1; $o < $xd; $o++) {
                                $filAriane[$zd][$yd][$o][] = 2;
                            }
                            // Détermination case d'arrivée
                            $filAriane[$zd][$ya][$xa][] = 18;
                        }
                        // Déplacement vers l'EST
                        if ($xa > $xd) {
                            $key                            = array_key_last(($filAriane[$zd][$yd][$xd]));
                            $filAriane[$zd][$yd][$xd][$key] = match ($filAriane[$zd][$yd][$xd][$key]) {
                                16   => 9,
                                17   => 6,
                                18   => 13,
                                19   => 3,
                                null => 23,
                            };
                            
                            // Déplacement sur les cases du milieu
                            for ($o = $xd + 1; $o < $xa; $o++) {
                                $filAriane[$zd][$yd][$o][] = 3;
                            }
                            // Détermination case d'arrivée
                            $filAriane[$zd][$ya][$xa][] = 19;
                        }
                    }
                }
                
                
            }
        }
        
    }
    
    /**
     * @throws Exception
     */
    public function traitementMapping(string $ruineId, array $mapRuine, ?User $user = null): Ruines
    {
        $mapRuineTraited = [];
        /**
         * Traitement MapRuine
         * @var RuinesCases[] $mapRuine
         */
        foreach ($mapRuine as $caseRuine) {
            
            $mapRuineTraited[(int)$caseRuine->getZ() * 10000 + (int)$caseRuine->getY() * 100 +
                             (int)$caseRuine->getX()] = $caseRuine;
            
        }
        
        /**
         * Récupération du plan actuel si existant
         * @var Ruines $ruine
         */
        $ruine = $this->em->getRepository(Ruines::class)->findOneBy(['id' => $ruineId]);
        
        $planRuineColec = $ruine->getPlans();
        
        if ($planRuineColec->count() == 0) {
            $planRuine = new RuinesPlans();
            $planRuine->setCreateBy($user)
                      ->setCreatedAt(new DateTime('NOW'));
            $ruine->addPlan($planRuine);
        } else {
            $planRuine = $planRuineColec[0];
            $planRuine->setModifyBy($user)
                      ->setModifyAt(new DateTime('NOW'));
        }
        
        foreach ($planRuine->getCases() as $case) {
            $calculIdCase = $case->getZ() * 10000 + $case->getY() * 100 + $case->getX();
            
            if (isset($mapRuineTraited[$calculIdCase])) {
                $caseRuine = $mapRuineTraited[$calculIdCase];
                
                $case->setTypeCase((int)$caseRuine->getTypeCase())
                     ->setTypePorte($caseRuine->getTypePorte() ?? null)
                     ->setTypeEscalier($caseRuine->getTypeEscalier() ?? null)
                     ->setNbrZombie($caseRuine->getNbrZombie() ?? null);
                unset($mapRuineTraited[$calculIdCase]);
            } else {
                $planRuine->removeCase($case);
            }
        }
        
        foreach ($mapRuineTraited as $caseRuine) {
            
            $case = new RuinesCases();
            $case->setTypeCase((int)$caseRuine->getTypeCase())
                 ->setTypePorte($caseRuine->getTypePorte() ?? null)
                 ->setNbrZombie($caseRuine->getNbrZombie() ?? null)
                 ->setTypeEscalier($caseRuine->getTypeEscalier() ?? null)
                 ->setZ($caseRuine->getZ())
                 ->setX($caseRuine->getX())
                 ->setY($caseRuine->getY());
            
            $planRuine->addCase($case);
        }
        
        return $ruine;
    }
    
    
}