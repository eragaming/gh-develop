<?php


namespace App\Service\Ville;


use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Service\AbstractGHHandler;


class CarteHandlerV2 extends AbstractGHHandler
{
    public function calculBordureCaseKm(ZoneMap &$zoneMap, ?int $kmNord = null, ?int $kmSud = null, ?int $kmEst = null, ?int $kmOuest = null): void
    {
        // On ne calcule les bordures que si la case n'est pas la ville ou que nous ne sommes pas sur une direction nord, est, sud et ouest
        if ($zoneMap->getKm() === 0) {
            return;
        }
        
        if ($kmNord === null) {
            $kmNord = (int)round(sqrt($zoneMap->getXRel() ** 2 + ($zoneMap->getYRel() + 1) ** 2));
        }
        if ($kmSud === null) {
            $kmSud = (int)round(sqrt($zoneMap->getXRel() ** 2 + ($zoneMap->getYRel() - 1) ** 2));
        }
        if ($kmEst === null) {
            $kmEst = (int)round(sqrt(($zoneMap->getXRel() + 1) ** 2 + $zoneMap->getYRel() ** 2));
        }
        if ($kmOuest === null) {
            $kmOuest = (int)round(sqrt(($zoneMap->getXRel() - 1) ** 2 + $zoneMap->getYRel() ** 2));
        }
        
        $bordures = [
            'nord'  => !is_null($kmNord) && $kmNord !== 0 && $kmNord === $zoneMap->getKm() + 1,
            'est'   => !is_null($kmEst) && $kmEst === $zoneMap->getKm() + 1 && $kmEst !== 0,
            'sud'   => !is_null($kmSud) && $kmSud === $zoneMap->getKm() + 1 && $kmSud !== 0,
            'ouest' => !is_null($kmOuest) && $kmOuest === $zoneMap->getKm() + 1 && $kmOuest !== 0,
        ];
        
        $bordureValue = 0;
        if ($bordures['nord']) {
            $bordureValue += ZoneMap::Bordure_n;
        }
        if ($bordures['est']) {
            $bordureValue += ZoneMap::Bordure_e;
        }
        if ($bordures['sud']) {
            $bordureValue += ZoneMap::Bordure_s;
        }
        if ($bordures['ouest']) {
            $bordureValue += ZoneMap::Bordure_o;
        }
        
        if ($bordureValue === 0) {
            $bordureValue = null;
        }
        
        $zoneMap->setBordKm($bordureValue);
        
    }
    
    public function calculBordureCasePa(ZoneMap &$zoneMap, ?int $paNord = null, ?int $paSud = null,
                                        ?int    $paEst = null, ?int $paOuest = null): void
    {
        // On ne calcule les bordures que si la case n'est pas la ville ou que nous ne sommes pas sur une direction nord, est, sud et ouest
        if ($zoneMap->getPa() === 0) {
            return;
        }
        
        if ($paNord === null) {
            $paNord = abs($zoneMap->getXRel()) + abs($zoneMap->getYRel() + 1);
        }
        if ($paSud === null) {
            $paSud = abs($zoneMap->getXRel()) + abs($zoneMap->getYRel() - 1);
        }
        if ($paEst === null) {
            $paEst = abs($zoneMap->getXRel() + 1) + abs($zoneMap->getYRel());
        }
        if ($paOuest === null) {
            $paOuest = abs($zoneMap->getXRel() - 1) + abs($zoneMap->getYRel());
        }
        
        $bordures = [
            'nord'  => $paNord === $zoneMap->getPa() + 1 && $paNord !== 0,
            'est'   => $paEst === $zoneMap->getPa() + 1 && $paEst !== 0,
            'sud'   => $paSud === $zoneMap->getPa() + 1 && $paSud !== 0,
            'ouest' => $paOuest === $zoneMap->getPa() + 1 && $paOuest !== 0,
        ];
        
        $bordureValue = 0;
        if ($bordures['nord']) {
            $bordureValue += ZoneMap::Bordure_n;
        }
        if ($bordures['est']) {
            $bordureValue += ZoneMap::Bordure_e;
        }
        if ($bordures['sud']) {
            $bordureValue += ZoneMap::Bordure_s;
        }
        if ($bordures['ouest']) {
            $bordureValue += ZoneMap::Bordure_o;
        }
        
        $zoneMap->setBordPa($bordureValue);
        
    }
    
    public function calculBordureCaseScrutateur(ZoneMap  &$zoneMap, ?ZoneMap $zoneNord = null,
                                                ?ZoneMap $zoneEst = null, ?ZoneMap $zoneSud = null,
                                                ?ZoneMap $zoneOuest = null, ?ZoneMap $zoneNordEst = null,
                                                ?ZoneMap $zoneNordOuest = null, ?ZoneMap $zoneSudEst = null,
                                                ?ZoneMap $zoneSudOuest = null): void
    {
        // On ne calcule les bordures que si la case n'est pas la ville ou que nous ne sommes pas sur une direction nord, est, sud et ouest
        if ($zoneMap->getDirection() === ZoneMap::DIRECTION_VILLE) {
            return;
        }
        if ($zoneMap->getDirection() !== ZoneMap::DIRECTION_NORD &&
            $zoneMap->getDirection() !== ZoneMap::DIRECTION_EST &&
            $zoneMap->getDirection() !== ZoneMap::DIRECTION_SUD &&
            $zoneMap->getDirection() !== ZoneMap::DIRECTION_OUEST) {
            return;
        }
        
        $bordures = [
            'nord'  => !is_null($zoneNord) && ($zoneNord->getDirection() !== $zoneMap->getDirection() &&
                                               $zoneNord->getDirection() !== ZoneMap::DIRECTION_VILLE),
            'est'   => !is_null($zoneEst) && ($zoneEst->getDirection() !== $zoneMap->getDirection() &&
                                              $zoneEst->getDirection() !== ZoneMap::DIRECTION_VILLE),
            'sud'   => !is_null($zoneSud) && ($zoneSud->getDirection() !== $zoneMap->getDirection() &&
                                              $zoneSud->getDirection() !== ZoneMap::DIRECTION_VILLE),
            'ouest' => !is_null($zoneOuest) && ($zoneOuest->getDirection() !== $zoneMap->getDirection() &&
                                                $zoneOuest->getDirection() !== ZoneMap::DIRECTION_VILLE),
        ];
        
        $bordureValue = 0;
        if ($bordures['nord']) {
            $bordureValue += ZoneMap::Bordure_n;
        }
        if ($bordures['est']) {
            $bordureValue += ZoneMap::Bordure_e;
        }
        if ($bordures['sud']) {
            $bordureValue += ZoneMap::Bordure_s;
        }
        if ($bordures['ouest']) {
            $bordureValue += ZoneMap::Bordure_o;
        }
        
        // Vérifier les cas diagonaux uniquement si les quatre côtés sont identiques (ou nuls)
        if ($bordureValue === 0) {
            if (!is_null($zoneNordEst) && $zoneNordEst->getDirection() !== $zoneMap->getDirection()) {
                $bordureValue += ZoneMap::Bordure_ne + 20;
            }
            if (!is_null($zoneNordOuest) && $zoneNordOuest->getDirection() !== $zoneMap->getDirection()) {
                $bordureValue += ZoneMap::Bordure_no + 20;
            }
            if (!is_null($zoneSudEst) && $zoneSudEst->getDirection() !== $zoneMap->getDirection()) {
                $bordureValue += ZoneMap::Bordure_se + 20;
            }
            if (!is_null($zoneSudOuest) && $zoneSudOuest->getDirection() !== $zoneMap->getDirection()) {
                $bordureValue += ZoneMap::Bordure_so + 20;
            }
        }
        
        if ($bordureValue === 0 || $bordureValue > ZoneMap::Bordure_so + 20) {
            $bordureValue = null;
        }
        
        $zoneMap->setBordScrut($bordureValue);
        
    }
    
    public function calculBordureCaseZone(ZoneMap &$zoneMap, ?int $zoneNord = null, ?int $zoneSud = null,
                                          ?int    $zoneEst = null, ?int $zoneOuest = null): void
    {
        // On ne calcule les bordures que si la case n'est pas la ville
        if ($zoneMap->getZone() === 0) {
            return;
        }
        
        if ($zoneNord === null) {
            $zoneNord = $this->calculZonage($zoneMap->getXRel(), $zoneMap->getYRel() + 1);
        }
        if ($zoneSud === null) {
            $zoneSud = $this->calculZonage($zoneMap->getXRel(), $zoneMap->getYRel() - 1);
        }
        if ($zoneEst === null) {
            $zoneEst = $this->calculZonage($zoneMap->getXRel() + 1, $zoneMap->getYRel());
        }
        if ($zoneOuest === null) {
            $zoneOuest = $this->calculZonage($zoneMap->getXRel() - 1, $zoneMap->getYRel());
        }
        
        $bordures = [
            'nord'  => $zoneNord > $zoneMap->getzone() && $zoneNord !== 0,
            'est'   => $zoneEst > $zoneMap->getzone() && $zoneEst !== 0,
            'sud'   => $zoneSud > $zoneMap->getzone() && $zoneSud !== 0,
            'ouest' => $zoneOuest > $zoneMap->getzone() && $zoneOuest !== 0,
        ];
        
        $bordureValue = 0;
        if ($bordures['nord']) {
            $bordureValue += ZoneMap::Bordure_n;
        }
        if ($bordures['est']) {
            $bordureValue += ZoneMap::Bordure_e;
        }
        if ($bordures['sud']) {
            $bordureValue += ZoneMap::Bordure_s;
        }
        if ($bordures['ouest']) {
            $bordureValue += ZoneMap::Bordure_o;
        }
        
        if ($bordureValue === 0) {
            $bordureValue = null;
        }
        
        $zoneMap->setBordZonage($bordureValue);
        
    }
    
    public function calculCoorRelatif(ZoneMap &$zoneMap, Ville $ville): void
    {
        $zoneMap->setXRel($zoneMap->getX() - $ville->getPosX());
        $zoneMap->setYRel($ville->getPosY() - $zoneMap->getY());
    }
    
    public function calculDirectionCase(ZoneMap &$zoneMap): void
    {
        $x = $zoneMap->getXRel();
        $y = $zoneMap->getYRel();
        
        $direction = null;
        
        if ($x === 0 && $y === 0) {
            $direction = ZoneMap::DIRECTION_VILLE;
        } elseif ($x != 0 && $y != 0 && (abs(abs($x) - abs($y)) < min(abs($x), abs($y)))) {
            if ($x < 0 && $y < 0) {
                $direction = ZoneMap::DIRECTION_SUD_OUEST;
            }
            if ($x < 0 && $y > 0) {
                $direction = ZoneMap::DIRECTION_NORD_OUEST;
            }
            if ($x > 0 && $y < 0) {
                $direction = ZoneMap::DIRECTION_SUD_EST;
            }
            if ($x > 0 && $y > 0) {
                $direction = ZoneMap::DIRECTION_NORD_EST;
            }
        } else {
            if (abs($x) > abs($y) && $x < 0) {
                $direction = ZoneMap::DIRECTION_OUEST;
            }
            if (abs($x) > abs($y) && $x > 0) {
                $direction = ZoneMap::DIRECTION_EST;
            }
            if (abs($x) < abs($y) && $y < 0) {
                $direction = ZoneMap::DIRECTION_SUD;
            }
            if (abs($x) < abs($y) && $y > 0) {
                $direction = ZoneMap::DIRECTION_NORD;
            }
        }
        
        $zoneMap->setDirection($direction);
    }
    
    public function calculKmCase(ZoneMap &$zoneMap, ?int &$kmMax = null): void
    {
        $km = (int)round(sqrt($zoneMap->getXRel() ** 2 + $zoneMap->getYRel() ** 2));
        
        $zoneMap->setKm($km);
        if ($kmMax !== null) {
            $kmMax = max($kmMax, $km);
        }
    }
    
    public function calculPaARCase(int $x, int $y): int
    {
        $paAR = (abs($x) + abs($y)) * 2;
        
        // On calcule la suppression de PA par rapport au rapatriement et à la position de la case par rapport à la ville
        $paVariable = 3;
        if ($x === 0 || $y === 0) {
            $paVariable = 1;
        }
        return $paAR - $paVariable;
    }
    
    public function calculPaCase(ZoneMap &$zoneMap, ?int &$paMax = null): void
    {
        $pa = abs($zoneMap->getXRel()) + abs($zoneMap->getYRel());
        $zoneMap->setPa($pa);
        if ($paMax !== null) {
            $paMax = max($paMax, $pa);
        }
    }
    
    public function calculZonageCase(ZoneMap $zoneMap): void
    {
        $zone = $this->calculZonage($zoneMap->getXRel(), $zoneMap->getYRel());
        $zoneMap->setZone($zone);
    }
    
    private function calculZonage(int $x, int $y): int
    {
        $listZonage = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27];
        
        $paAR = $this->calculPaARCase($x, $y);
        
        foreach ($listZonage as $zonage) {
            if ($zonage >= $paAR) {
                // Une fois trouvé, on met à jour la zone et on quitte la fonction
                return $zonage;
            }
        }
        
        return 99;
    }
    
}