<?php

namespace App\Service;

use App\Entity\BatPrototype;
use App\Entity\ChantierPrototype;
use App\Entity\CleanUpCadaver;
use App\Entity\HomePrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\TypeDeath;
use App\Structures\Conf\GestHordesConf;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

class DataCollection
{
    /**
     * @var ItemPrototype[]
     */
    private array $objets;
    /**
     * @var BatPrototype[]
     */
    private array $bats;
    /**
     * @var ChantierPrototype[]
     */
    private array $chantiers;
    /**
     * @var CleanUpCadaver[]
     */
    private array $cleanUp;
    /**
     * @var HomePrototype[]
     */
    private array $homes;
    /**
     * @var JobPrototype[]
     */
    private array $jobs;
    /**
     * @var TypeDeath[]
     */
    private array         $typeDeath;
    private readonly bool $toSession;
    
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected RequestStack    $requestStack,
        protected ConfMaster      $confMaster,
    )
    {
        $this->toSession = (bool)$this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_SESSION_DATA);
    }
    
    public function recupBat(int $id): BatPrototype
    {
        $this->initialiseBats();
        
        return $this->bats[$id];
    }
    
    public function recupChantier(int $idChantier): ChantierPrototype
    {
        $this->initialiseChantiers();
        
        return $this->chantiers[$idChantier];
    }
    
    public function recupCleanUp(string $ref): CleanUpCadaver
    {
        $this->initialiseCleanUp();
        
        return $this->cleanUp[$ref];
    }
    
    public function recupHome(int $idHome): HomePrototype
    {
        $this->initialiseHomes();
        
        return $this->homes[$idHome];
    }
    
    public function recupJob(string $ref): JobPrototype
    {
        $this->initialiseJobs();
        
        return $this->jobs[$ref];
    }
    
    public function recupObjet(int $id): ItemPrototype
    {
        $this->initialiseObjet();
        
        return $this->objets[$id];
    }
    
    public function recupTypeDeath(int $idMort): TypeDeath
    {
        $this->initialiseTypeDeath();
        
        return $this->typeDeath[$idMort];
    }
    
    private function initialiseBats()
    {
        $this->bats = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('bats'))) {
            
            $listBats = $this->managerRegistry->getRepository(BatPrototype::class)->findAll();
            $keyBats  = array_map(fn($i) => $i->getId(), $listBats);
            
            $this->bats = array_combine($keyBats, $listBats);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('bats', $this->bats);
            }
        } else {
            $this->bats = $this->requestStack->getSession()->get('bats');
        }
    }
    
    private function initialiseChantiers()
    {
        $this->chantiers = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('chantiers'))) {
            
            $listChantiers = $this->managerRegistry->getRepository(ChantierPrototype::class)->findAll();
            $keyChantiers  = array_map(fn($i) => $i->getId(), $listChantiers);
            
            $this->chantiers = array_combine($keyChantiers, $listChantiers);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('chantiers', $this->chantiers);
            }
        } else {
            $this->chantiers = $this->requestStack->getSession()->get('chantiers');
        }
    }
    
    private function initialiseCleanUp()
    {
        $this->cleanUp = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('cleanUp'))) {
            
            $listCleanUp = $this->managerRegistry->getRepository(CleanUpCadaver::class)->findAll();
            $keyCleanUp  = array_map(fn($i) => $i->getRef(), $listCleanUp);
            
            $this->cleanUp = array_combine($keyCleanUp, $listCleanUp);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('cleanUp', $this->cleanUp);
            }
        } else {
            $this->cleanUp = $this->requestStack->getSession()->get('cleanUp');
        }
    }
    
    private function initialiseHomes()
    {
        $this->homes = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('homes'))) {
            
            $listHomes = $this->managerRegistry->getRepository(HomePrototype::class)->findAll();
            $keyHomes  = array_map(fn($i) => $i->getId(), $listHomes);
            
            $this->homes = array_combine($keyHomes, $listHomes);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('homes', $this->homes);
            }
        } else {
            $this->homes = $this->requestStack->getSession()->get('homes');
        }
    }
    
    private function initialiseJobs()
    {
        $this->jobs = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('jobs'))) {
            
            $listJobs = $this->managerRegistry->getRepository(JobPrototype::class)->findAll();
            $keyJobs  = array_map(fn($i) => $i->getRef(), $listJobs);
            
            $this->jobs = array_combine($keyJobs, $listJobs);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('jobs', $this->jobs);
            }
        } else {
            $this->jobs = $this->requestStack->getSession()->get('jobs');
        }
    }
    
    private function initialiseObjet()
    {
        $this->objets = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('objets'))) {
            
            $listObjet = $this->managerRegistry->getRepository(ItemPrototype::class)->findAll();
            $keyObjet  = array_map(fn($i) => $i->getId(), $listObjet);
            
            $this->objets = array_combine($keyObjet, $listObjet);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('objets', $this->objets);
            }
        } else {
            $this->objets = $this->requestStack->getSession()->get('objets');
        }
    }
    
    private function initialiseTypeDeath()
    {
        $this->typeDeath = [];
        
        if (!$this->toSession || is_null($this->requestStack->getSession()->get('typeDeath'))) {
            
            $listTypeDeath = $this->managerRegistry->getRepository(TypeDeath::class)->findAll();
            $keyTypeDeath  = array_map(fn($i) => $i->getIdMort(), $listTypeDeath);
            
            $this->typeDeath = array_combine($keyTypeDeath, $listTypeDeath);
            if ($this->toSession) {
                $this->requestStack->getSession()->set('typeDeath', $this->typeDeath);
            }
        } else {
            $this->typeDeath = $this->requestStack->getSession()->get('typeDeath');
        }
    }
    
}