<?php


namespace App\Service\Outils;


use App\Entity\HerosPrototype;
use App\Entity\HistoriqueVeille;
use App\Entity\JobPrototype;
use App\Entity\Ville;
use App\Structures\Dto\Veilleur;

class VeilleHandler
{
    
    public function calculChanceSurvie(int  $chancePrece, ?HistoriqueVeille $jourPrec, HistoriqueVeille $jour, bool $vp,
                                       bool $gard): int
    {
        
        $listEtatPrec = $jourPrec?->getListeEtats() ?? [];
        $veilleurHier = $jourPrec?->getVeille() ?? false;
        $listEtatAuj  = $jour?->getListeEtats() ?? [];
        $veilleurAuj  = $jour?->getVeille() ?? false;
        
        $chance = 0;
        
        if ($veilleurAuj) {
            foreach ($listEtatAuj as $etat) {
                $chance += $etat->getModifSurvie();
            }
            
            $chancePour = $chancePrece + $chance;
        } else {
            if ($veilleurHier) {
                $chance -= ($vp) ? 5 : 10;
                
                foreach ($listEtatPrec as $etat) {
                    if ($etat !== $listEtatAuj[$etat->getId()]) {
                        $chance -= $etat->getModifSurvie();
                    } else {
                        unset($listEtatAuj[$etat->getId()]);
                    }
                }
                foreach ($listEtatAuj as $etat) {
                    $chance += $etat->getModifSurvie();
                }
                $chancePour = $chancePrece + $chance;
            } else {
                $chance     += ($vp) ? 2.5 : 5;
                $chancePour = $chancePrece + $chance;
                
                $chancePour = ($gard) ? min($chancePour, 99) : min($chancePour, 95);
            }
        }
        
        
        return $chancePour;
    }
    
    public function calculChanceSurvieJour(Ville $ville, Veilleur $veilleur): array
    {
        
        $arrPourcentage = [];
        
        for ($i = 1; $i <= ($ville->getJour() + 5); $i++) {
            
            $historiqueVeille = $veilleur->getHistorique($i - 1);
            $historiqueJour   = $veilleur->getHistorique($i);
            
            $vp   = $veilleur->getCitoyens()->getCitoyen()->getDerPouv()->getOrdreRecup() == HerosPrototype::ID_VP;
            $gard = $veilleur->getCitoyens()->getJob()->getId() == JobPrototype::job_guardian;
            
            $arrPourcentage[$i] = $this->calculChanceSurvie(
                $arrPourcentage[$i - 1] ??
                (($veilleur->getCitoyens()->getJob()->getId() == JobPrototype::job_guardian) ? 99 : 95),
                $historiqueVeille,
                $historiqueJour,
                $vp,
                $gard,
            );
        }
        
        return $arrPourcentage;
        
    }
    
}