<?php


namespace App\Service\Outils;


use App\Entity\Citoyens;
use App\Entity\ConsigneExpedition;
use App\Entity\CreneauHorraire;
use App\Entity\DispoExpedition;
use App\Entity\Expedition;
use App\Entity\Expeditionnaire;
use App\Entity\ExpeditionPart;
use App\Entity\ExpeditionType;
use App\Entity\HerosPrototype;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\Outils;
use App\Entity\OutilsExpedition;
use App\Entity\Ouvriers;
use App\Entity\SacExpeditionnaire;
use App\Entity\TraceExpedition;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Exception\GestHordesException;
use App\Service\AbstractGHHandler;
use App\Service\Generality\ItemsHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\UserHandler;
use App\Service\Utils\ColorGeneratorHandler;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Outils\Expedition\ChangeStatutExpedition;
use App\Structures\Dto\GH\Outils\Expedition\ChangeStatutOuvrier;
use App\Structures\Dto\GH\Outils\Expedition\DuplicateOuvriers;
use App\Structures\Dto\GH\Outils\Expedition\GetExpedition;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeExpeditionOutils;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeOrdreExpeditions;
use App\Structures\Dto\GH\Outils\Expedition\SauvegardeOuvriers;
use App\Structures\Dto\GH\Outils\Expedition\SuppressionExpedition;
use App\Structures\Dto\GH\Outils\Expedition\VerrouillageExpedition;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExpeditionHandler extends AbstractGHHandler
{
    public function __construct(protected EntityManagerInterface $em,
                                protected LoggerInterface        $logger,
                                protected TranslatorInterface    $translator,
                                protected TranslateHandler       $translateHandler,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected SerializerService      $serializerService,
                                protected FormFactoryInterface   $formBuilder,
                                protected ColorGeneratorHandler  $color,
                                protected UrlGeneratorInterface  $urlGenerator,
                                protected ItemsHandler           $itemsHandler, private readonly EntityManagerInterface $entityManager,
    )
    {
        parent::__construct($em, $logger, $translator, $translateHandler, $userHandler, $gh,
                            $serializerService);
    }
    
    public function calculExpedition(Ville $ville, array $coord): array
    {
        $height  = $ville->getHeight();
        $width   = $ville->getWeight();
        $tabExpe = array_fill(0, $height, array_fill(0, $width, null));
        
        if (count($coord) === 1) {
            $x               = $coord[0][0];
            $y               = $coord[0][1];
            $tabExpe[$y][$x] = 25;
        } else {
            for ($c = 0; $c < count($coord) - 1; $c++) {
                $xd = (int)$coord[$c][0];
                $yd = (int)$coord[$c][1];
                $xa = (int)$coord[$c + 1][0];
                $ya = (int)$coord[$c + 1][1];
                
                if ($xd === $xa) {
                    $this->handleVerticalMovement($tabExpe, $xd, $yd, $ya);
                } elseif ($yd === $ya) {
                    $this->handleHorizontalMovement($tabExpe, $yd, $xd, $xa);
                }
            }
        }
        
        return $tabExpe;
    }
    
    public function calculPAExpedition(array $coords): int
    {
        if (count($coords) == 1) {
            return 0;
        }
        
        $PA = 0;
        
        for ($c = 0; $c < count($coords) - 1; $c++) {
            // Coordonnée point de départ
            $xd = (int)$coords[$c][0];
            $yd = (int)$coords[$c][1];
            // Coordonnée point d'arrivée
            $xa = (int)$coords[$c + 1][0];
            $ya = (int)$coords[$c + 1][1];
            
            if ($xd == $xa) {
                $PA += abs($ya - $yd);
            } elseif ($yd == $ya) {
                $PA += abs($xa - $xd);
            }
        }
        
        return $PA;
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function changeOrderExpedition(SauvegardeOrdreExpeditions $expeditions): void
    {
        try {
            
            // On va balayer les expéditions pour mettre à jour leur priorité
            foreach ($expeditions->getExpeditions() as $expedition) {
                $expeditionBdd = $this->em->getRepository(Expedition::class)->find($expedition->getId());
                $expeditionBdd->setPriorite($expedition->getPriorite());
                $this->em->persist($expeditionBdd);
            }
            
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function changeStatutExpedition(ChangeStatutExpedition $changeStatutExpedition): void
    {
        try {
            
            // On récupère l'expédition qu'on souhaite modifier le statut
            $expedition = $this->em->getRepository(Expedition::class)->find($changeStatutExpedition->getExpeditionId());
            
            // On parcourt toutes les parties de l'expédition pour les mettre à jour
            foreach ($expedition->getExpeditionParts() as $part) {
                $part->setOuverte($changeStatutExpedition->getStatut() ?? false);
            }
            
            $this->em->persist($expedition);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function changeStatutOuvrier(Ville $ville, ChangeStatutOuvrier $changeStatutOuvrier): void
    {
        try {
            
            // On récupère l'expédition qu'on souhaite modifier le statut
            $outils = $this->em->getRepository(Outils::class)->findOneBy(['ville' => $ville->getId(), 'day' => $changeStatutOuvrier->getJour()]);
            
            if ($outils === null) {
                throw new GestHordesException("Problème lors de la récupération des outils | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On modifie le statut
            $outils->getExpedition()->setOuvertOuvrier($changeStatutOuvrier->getStatut() ?? false);
            
            $this->em->persist($outils);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws Exception
     */
    public function controleDonneesSauvegardeExpedition(SauvegardeExpeditionOutils $sauvegardeExpeditionOutils): void
    {
        // Vérification que le nom n'est pas vide
        if ($sauvegardeExpeditionOutils->getExpedition()->getNom() === "") {
            throw new GestHordesException($this->translator->trans("Le nom de l'expédition ne peut pas être vide.", [],
                                                                   "outils"), GestHordesException::SHOW_MESSAGE);
        }
        
        // Vérification que le type d'expédition est bien renseigné
        if ($sauvegardeExpeditionOutils->getExpedition()->getTypeExpe() === null) {
            throw new GestHordesException($this->translator->trans("L'expédition doit avoir un type.", [], "outils"),
                                          GestHordesException::SHOW_MESSAGE);
        }
        
        foreach ($sauvegardeExpeditionOutils->getExpedition()->getExpeditionParts() as $part) {
            // Vérification s'il y a un tracé choisi qu'il n'a pas été déjà choisi pour chacune des parties de l'expédition
            if ($part->getTrace() !== null) {
                $traceInbdd = $this->em->getRepository(TraceExpedition::class)->find($part->getTrace()->getId());
                if ($traceInbdd->getExpeditionPart() !== null &&
                    $traceInbdd->getExpeditionPart()->getId() !== $part->getId()) {
                    if (count($sauvegardeExpeditionOutils->getExpedition()->getExpeditionParts()) > 1) {
                        throw new GestHordesException($this->translator->trans("Le tracé choisi pour la partie {number} est déjà utilisé par une autre expédition.",
                                                                               ['{number}' => $traceInbdd->getExpeditionPart()
                                                                                                         ->getNumber()],
                                                                               "outils"),
                                                      GestHordesException::SHOW_MESSAGE);
                    } else {
                        throw new GestHordesException($this->translator->trans("Le tracé choisi est déjà utilisé par une autre expédition.",
                                                                               [],
                                                                               "outils"),
                                                      GestHordesException::SHOW_MESSAGE);
                    }
                    
                }
            }
            
            // Vérification qu'il y a au moins un expéditeur par partie d'expédition
            if (count($part->getExpeditionnaires()) === 0) {
                throw new GestHordesException($this->translator->trans("Une expédition ne peut pas se faire sans expéditionnaire.",
                                                                       [], "outils"),
                                              GestHordesException::SHOW_MESSAGE);
            }
        }
        
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function creationExpeditionOutils(SauvegardeExpeditionOutils $sauvegardeExpeditionOutils): void
    {
        try {
            // controle des données
            $this->controleDonneesSauvegardeExpedition($sauvegardeExpeditionOutils);
            
            $expeditionFront = $sauvegardeExpeditionOutils->getExpedition();
            $jour            = $sauvegardeExpeditionOutils->getJour();
            
            // récupération de la ville
            $ville =
                $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $sauvegardeExpeditionOutils->getMapId()]);
            
            // récupération du type d'expédition depuis la base de donnée
            $typeExpe = $this->em->getRepository(ExpeditionType::class)->find($expeditionFront->getTypeExpe()->getId());
            
            if ($typeExpe === null) {
                throw new GestHordesException("Problème lors de la récupération du type d'expédition | Fichier : " .
                                              __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On récupère tous les créneaux pour expeditions afin de les rajouter à chaque dispo d'un expeditionnaire
            $creneaux = $this->em->getRepository(CreneauHorraire::class)->findBy(['typologie' => 2]);
            
            // récupération des outils afin de pouvoir y rattacher l'expédition
            $outils = $this->recuperationOutilsExpeditionJour($ville, $jour);
            
            $nomExpedition = trim($expeditionFront->getNom());
            
            // On crée les objets pour mettre en base de donnée
            $expeditionNew = new Expedition();
            $expeditionNew->setNom($nomExpedition)
                          ->setNbrPartie(count($expeditionFront->getExpeditionParts()))
                          ->setMinPdc($expeditionFront->getMinPdc() ?? null)
                          ->setTypeExpe($typeExpe)
                          ->setCreatedAt(new DateTimeImmutable())
                          ->setCreatedBy($this->userHandler->getCurrentUser());
            
            // On crée les parties de l'expédition
            foreach ($expeditionFront->getExpeditionParts() as $part) {
                
                $expeditionPart = new ExpeditionPart();
                $expeditionPart->setNumber($part->getNumber())
                               ->setOuverte(false)
                               ->setDescription($part->getDescription())
                               ->setExpedition($expeditionNew)
                               ->setPa(0);
                
                // Si un tracé a été choisi
                if ($part->getTrace() !== null) {
                    //Récupération du tracé choisi
                    $trace = $this->em->getRepository(TraceExpedition::class)->find($part->getTrace()->getId());
                    
                    $nomExpeditionTrace = ($expeditionNew->getTypeExpe()->getId() === ExpeditionType::TYPE_RAMASSAGE) ?
                        ($nomExpedition . ' - ' . $part->getNumber()) : $nomExpedition;
                    
                    // si le tracé vient de la bibliothèque, il faut le cloner
                    if ($trace->isBiblio()) {
                        $trace = clone $trace;
                        $trace->setBiblio(false)
                              ->setJour($jour)
                              ->setTraceExpedition(true)
                              ->setCollab(true)
                              ->setNom($nomExpeditionTrace)
                              ->setCreatedBy($this->userHandler->getCurrentUser())
                              ->setCreatedAt(new DateTimeImmutable());
                    } else {
                        if ($trace->isBrouillon()) {
                            $trace->setCollab(true)
                                  ->setJour($jour)
                                  ->setTraceExpedition(true)
                                  ->setBrouillon(false)
                                  ->setNom($nomExpeditionTrace)
                                  ->setCreatedBy($this->userHandler->getCurrentUser())
                                  ->setCreatedAt(new DateTimeImmutable());
                        } else {
                            $trace->setNom($nomExpeditionTrace)
                                  ->setJour($jour)
                                  ->setTraceExpedition(true)
                                  ->setModifyAt(new DateTimeImmutable())
                                  ->setModifyBy($this->userHandler->getCurrentUser());
                        }
                    }
                    $this->em->persist($trace);
                    $this->em->flush();
                    $this->em->refresh($trace);
                    
                    $expeditionPart->setTrace($trace)
                                   ->setPa($trace->getPa());
                }
                
                
                // On crée les expéditionnaires
                foreach ($part->getExpeditionnaires() as $expediteur) {
                    $expeditionnaire = new Expeditionnaire();
                    
                    // Si un métier a été choisi, on vient le récupérer
                    if ($expediteur->getJob() !== null) {
                        $job = $this->em->getRepository(JobPrototype::class)->find($expediteur->getJob()->getId());
                        $expeditionnaire->setJob($job)->setJobFige($expediteur->isJobFige());
                    } else {
                        $expeditionnaire->setJobFige(false);
                    }
                    
                    // Si un pouvoir a été choisi, on vient le récupérer
                    if ($expediteur->getActionHeroic() !== null) {
                        $pouvoir = $this->em->getRepository(HerosPrototype::class)->find($expediteur->getActionHeroic()
                                                                                                    ->getId());
                        $expeditionnaire->setActionHeroic($pouvoir);
                    }
                    
                    // Si un citoyen a été choisi, on vient le récupérer, et on met l'état de pré-inscrit
                    if ($expediteur->getCitoyen() !== null) {
                        $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                         'citoyen' => $expediteur->getCitoyen()
                                                                                                                 ->getCitoyen()
                                                                                                                 ->getId()]);
                        $expeditionnaire->setCitoyen($citoyen)
                                        ->setPreinscrit($expediteur->isPreinscrit());
                    } else {
                        $expeditionnaire->setPreinscrit(false);
                    }
                    
                    // Si la soif a été choisi, on vient le mettre à jour
                    if ($expediteur->isSoif() !== null) {
                        $expeditionnaire->setSoif($expediteur->isSoif())->setSoifFige($expediteur->isSoifFige());
                    } else {
                        $expeditionnaire->setSoifFige(false);
                    }
                    
                    // Si les PA de base ont été choisi, on vient le mettre à jour
                    if ($expediteur->getPaBase() !== null) {
                        $expeditionnaire->setPaBase($expediteur->getPaBase());
                    }
                    // Si les PE de base ont été choisi, on vient le mettre à jour
                    if ($expediteur->getPeBase() !== null) {
                        $expeditionnaire->setPeBase($expediteur->getPeBase());
                    } else {
                        $expeditionnaire->setPeBase(0);
                    }
                    
                    // Si c'est une place pour banni
                    if ($expediteur->isForBanni() !== null) {
                        $expeditionnaire->setForBanni($expediteur->isForBanni());
                    } else {
                        $expeditionnaire->setForBanni(false);
                    }
                    
                    $expeditionnaire->setPosition($expediteur->getPosition());
                    
                    // On va ajouter les items du sac
                    foreach ($expediteur->getSac() as $item) {
                        $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($item->getItem()->getId());
                        if ($itemProtoBdd === null) {
                            throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " .
                                                          __FILE__ . " | Ligne : " . __LINE__);
                        }
                        
                        $itemNew = new SacExpeditionnaire();
                        $itemNew->setItem($itemProtoBdd)
                                ->setBroken($item->isBroken())
                                ->setNbr($item->getNbr());
                        $expeditionnaire->addSac($itemNew);
                    }
                    
                    // On ajoute les créneaux de dispo à chaque expéditionnaire
                    foreach ($creneaux as $creneau) {
                        $dispo = new DispoExpedition();
                        $dispo->setCreneau($creneau);
                        $expeditionnaire->addDispo($dispo);
                    }
                    
                    $expeditionPart->addExpeditionnaire($expeditionnaire);
                }
                
                // On crée les consignes si il y en a
                foreach ($part->getConsignes() as $consigne) {
                    $consigneNew = new ConsigneExpedition();
                    
                    // si la zone fournis est null, on n'enregistre pas la consigne
                    if ($consigne->getZone() === null) {
                        continue;
                    }
                    
                    // On récupère la zone de la consigne
                    $zone = $this->em->getRepository(ZoneMap::class)->findOneBy(['ville' => $ville,
                                                                                 'xRel'  => $consigne->getZone()
                                                                                                     ->getXRel(),
                                                                                 'yRel'  => $consigne->getZone()
                                                                                                     ->getYRel()]);
                    if ($zone === null) {
                        throw new GestHordesException("Problème lors de la récupération de la zone de la consigne | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
                    }
                    
                    $consigneNew->setZone($zone)
                                ->setOrdreConsigne($consigne->getOrdreConsigne())
                                ->setFait(false)
                                ->setText(trim($consigne->getText()));
                    
                    $expeditionPart->addConsigne($consigneNew);
                }
                
                $expeditionNew->addExpeditionPart($expeditionPart);
            }
            
            // récupération de la position de la nouvelle expédition
            $position = $this->em->getRepository(Expedition::class)->recupMaxPositionExpedition($outils->getId());
            $expeditionNew->setPriorite($position + 1);
            
            $outils->addExpedition($expeditionNew);
            $this->em->persist($outils);
            $this->em->flush();
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    private function deverrouillageExpedition(Expedition &$expedition): void
    {
        // On peut mettre à jour le verrou
        $expedition->setVerrou(false)
                   ->setVerrouBy(null)
                   ->setVerrouAt(null);
    }
    
    /**
     * @throws GestHordesException
     */
    public function duplicateOuvrier(DuplicateOuvriers $duplicateOuvriers): void
    {
        try {
            
            // récupération de la ville
            $ville = $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $duplicateOuvriers->getMapId()]);
            
            // récupération de l'outils expédition sur lequel on va récupérer les ouvriers à dupliquer
            $outilsExpeFrom = $this->recuperationOutilsExpeditionJour($ville, $duplicateOuvriers->getFromDay());
            $outilsExpeTo   = $this->recuperationOutilsExpeditionJour($ville, $duplicateOuvriers->getToDay());
            
            // Si l'outils de départ ne contient pas d'ouvrier, on émet une erreur (normalement pas possible)
            if ($outilsExpeFrom->getOuvriers()->count() === 0) {
                throw new GestHordesException($this->translator->trans("Il n'est pas possible de dupliquer des ouvriers inexistant", [], "outils"), GestHordesException::SHOW_MESSAGE);
            }
            
            // Si l'outils de destination contient des ouvriers, on emet une erreur
            if ($outilsExpeTo->getOuvriers()->count() > 0) {
                throw new GestHordesException($this->translator->trans("Il n'est pas possible de dupliquer des ouvriers sur des ouvriers déjà existants.", [], "outils"), GestHordesException::SHOW_MESSAGE);
            }
            
            // On va dupliquer les ouvriers
            foreach ($outilsExpeFrom->getOuvriers() as $ouvrier) {
                // Si l'option avec pré-inscription est choisis on garde : le citoyen, métier, situation (banni), PA de base
                // Dans tous les cas, les dispos des ouvriers sont remis à blanc
                $ouvrierNew = new Ouvriers();
                if ($duplicateOuvriers->getWithPreinscrit() && $ouvrier->isPreinscrit()) {
                    $ouvrierNew->setCitoyen($ouvrier->getCitoyen());
                } else {
                    $ouvrierNew->setCitoyen(null);
                }
                $ouvrierNew->setJob(($ouvrier->isJobFige()) ? $ouvrier->getJob() : null)
                           ->setJobFige($ouvrier->isJobFige())
                           ->setSoif(false)
                           ->setSoifFige($ouvrier->isSoifFige())
                           ->setForBanni($ouvrier->isForBanni())
                           ->setDispoRapide(null)
                           ->setCommentaire(null)
                           ->setConsigne(null)
                           ->setPosition($ouvrier->getPosition())
                           ->setPreinscrit($ouvrier->isPreinscrit());
                
                // On va rajouter les dispos de l'ancien jour, mais en laissant la valeur à null pour le choix de la dispo
                foreach ($ouvrier->getDispo() as $dispo) {
                    $dispoNew = new DispoExpedition();
                    $dispoNew->setCreneau($dispo->getCreneau());
                    $ouvrierNew->addDispo($dispoNew);
                }
                
                $outilsExpeTo->addOuvrier($ouvrierNew);
            }
            
            $this->em->persist($outilsExpeTo);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    public function getExpeditionVerou(OutilsExpedition $outilsExpedition): array
    {
        // On va balayer toutes les expéditions pour voir si il y en a une de verrouillée (et si le délais n'est pas dépassé, auquel cas, on la déverrouille)
        $expeditions = $outilsExpedition->getExpeditions();
        
        $expeditionsVerou = [];
        foreach ($expeditions as $expedition) {
            if ($expedition->isVerrou()) {
                $dateActuel = new DateTimeImmutable();
                
                // On vérifie si les délais sont dépassés, dans ce cas, on déverrouille
                $dateVerou = $expedition->getVerrouAt()->add(new DateInterval('PT5M'));
                if ($dateActuel > $dateVerou) {
                    $expedition->setVerrou(false)
                               ->setVerrouBy(null)
                               ->setVerrouAt(null);
                    $this->em->persist($expedition);
                    $this->em->flush();
                } else {
                    if ($expedition->getVerrouBy()->getId() !== $this->userHandler->getCurrentUser()->getId()) {
                        $expeditionsVerou[] = $expedition->getId();
                    }
                }
            }
        }
        
        return $expeditionsVerou;
    }
    
    private function handleHorizontalMovement(array &$tabExpe, int $y, int $xd, int $xa): void
    {
        if ($xa < $xd) {
            $this->setStartCell($tabExpe, $y, $xd, [8 => 19, 6 => 24, 5 => 11, 7 => 13], 3);
            $this->setHorizontalMiddleCells($tabExpe, $y, $xa + 1, $xd, 13);
            $tabExpe[$y][$xa][] = 7;
        } elseif ($xa > $xd) {
            $this->setStartCell($tabExpe, $y, $xd, [8 => 20, 6 => 23, 5 => 15, 7 => 9], 1);
            $this->setHorizontalMiddleCells($tabExpe, $y, $xd + 1, $xa, 15);
            $tabExpe[$y][$xa][] = 5;
        }
    }
    
    private function handleVerticalMovement(array &$tabExpe, int $x, int $yd, int $ya): void
    {
        if ($ya < $yd) {
            $this->setStartCell($tabExpe, $yd, $x, [7 => 17, 5 => 21, 6 => 16, 8 => 10], 2);
            $this->setVerticalMiddleCells($tabExpe, $x, $ya + 1, $yd, 16);
            $tabExpe[$ya][$x][] = 6;
        } elseif ($ya > $yd) {
            $this->setStartCell($tabExpe, $yd, $x, [7 => 18, 5 => 22, 6 => 12, 8 => 14], 4);
            $this->setVerticalMiddleCells($tabExpe, $x, $yd + 1, $ya, 14);
            $tabExpe[$ya][$x][] = 8;
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function majVerrouillageExpedition(VerrouillageExpedition $verrouillageExpedition, Ville $ville): void
    {
        $outilsExpedition = $this->recuperationOutilsExpeditionJour($ville, $verrouillageExpedition->getJour());
        
        // recherche de l'expédition à verrouiller
        $expedition = $this->em->getRepository(Expedition::class)->find($verrouillageExpedition->getExpeditionId());
        
        if ($expedition === null) {
            throw new GestHordesException("L'expédition n'existe pas, impossible de verouiller/déverrouiller");
        }
        
        // On vérifie que l'expédition appartient bien à l'outils du jour
        if ($outilsExpedition->getId() !== $expedition->getOutilsExpedition()->getId()) {
            throw new GestHordesException("L'expédition n'appartient pas à l'outils du jour, impossible de verouiller/déverrouiller");
        }
        
        try {
            // Si on veut verrouiller l'expédition
            if ($verrouillageExpedition->isVerrouillage()) {
                $this->verrouillageExpedition($expedition);
            } else {
                $this->deverrouillageExpedition($expedition);
            }
            
            
            $this->em->persist($expedition);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function miseAJourExpeditionOutils(SauvegardeExpeditionOutils $sauvegardeExpeditionOutils): void
    {
        try {
            // controle des données
            $this->controleDonneesSauvegardeExpedition($sauvegardeExpeditionOutils);
            
            $expeditionFront = $sauvegardeExpeditionOutils->getExpedition();
            $jour            = $sauvegardeExpeditionOutils->getJour();
            
            // récupération de la ville
            $ville =
                $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $sauvegardeExpeditionOutils->getMapId()]);
            
            // récupération de l'expédition depuis la base de donnée
            $expeditionBdd = $this->em->getRepository(Expedition::class)->find($expeditionFront->getId());
            
            if ($expeditionBdd === null) {
                throw new GestHordesException("Problème lors de la récupération de l'expédition | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // récupération du type d'expédition depuis la base de donnée
            $typeExpe = $this->em->getRepository(ExpeditionType::class)->find($expeditionFront->getTypeExpe()->getId());
            
            if ($typeExpe === null) {
                throw new GestHordesException("Problème lors de la récupération du type d'expédition | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            $nomExpedition = trim($expeditionFront->getNom());
            
            // On récupère le statut de l'expédition
            $statut = $expeditionBdd->getExpeditionParts()->toArray()[0]->isOuverte();
            
            // On met à jour les objets pour mettre en base de donnée
            $expeditionBdd->setNom($nomExpedition)
                          ->setNbrPartie(count($expeditionFront->getExpeditionParts()))
                          ->setMinPdc($expeditionFront->getMinPdc() ?? null)
                          ->setTypeExpe($typeExpe)
                          ->setModifyAt(new DateTimeImmutable())
                          ->setModifyBy($this->userHandler->getCurrentUser());
            
            // On récupère tous les créneaux pour expeditions afin de les rajouter à chaque dispo d'un expeditionnaire
            $creneaux = $this->em->getRepository(CreneauHorraire::class)->findBy(['typologie' => 2]);
            
            // On met à jour les parties de l'expédition
            // On va d'abord regarder si il y a des parties à supprimer
            // On va séparer les parties d'expeditions avec un id et sans id dans des tableaux
            $partieBdd = [];
            $partieNew = [];
            foreach ($expeditionFront->getExpeditionParts() as $part) {
                if ($part->getId() === null) {
                    $partieNew[] = $part;
                } else {
                    $partieBdd[$part->getId()] = $part;
                }
            }
            
            // On va regarder si il y a des parties à supprimer, et si ça existe on va mettre à jour par rapport à ce qu'on a reçu
            foreach ($expeditionBdd->getExpeditionParts() as $part) {
                if (!array_key_exists($part->getId(), $partieBdd)) {
                    $expeditionBdd->removeExpeditionPart($part);
                    $this->em->remove($part);
                } else {
                    
                    $partBdd = $partieBdd[$part->getId()];
                    $part->setNumber($partBdd->getNumber())
                         ->setDescription($partBdd->getDescription())
                         ->setOuverte($statut);
                    
                    $newNom = ($expeditionBdd->getTypeExpe()->getId() === ExpeditionType::TYPE_RAMASSAGE) ?
                        ($nomExpedition . ' - ' . $part->getNumber()) : $nomExpedition;
                    
                    if ($part->getTrace() === null && $partBdd->getTrace() !== null) {
                        //Récupération du tracé choisi
                        $trace = $this->em->getRepository(TraceExpedition::class)->find($partBdd->getTrace()->getId());
                        
                        
                        // si le tracé vient de la bibliothèque, il faut le cloner
                        if ($trace->isBiblio()) {
                            $trace = clone $trace;
                            $trace->setBiblio(false)
                                  ->setJour($jour)
                                  ->setTraceExpedition(true)
                                  ->setCollab(true)
                                  ->setNom($newNom)
                                  ->setCreatedBy($this->userHandler->getCurrentUser())
                                  ->setCreatedAt(new DateTimeImmutable());
                        } else {
                            if ($trace->isBrouillon()) {
                                $trace->setCollab(true)
                                      ->setJour($jour)
                                      ->setTraceExpedition(true)
                                      ->setBrouillon(false)
                                      ->setNom($newNom)
                                      ->setCreatedBy($this->userHandler->getCurrentUser())
                                      ->setCreatedAt(new DateTimeImmutable());
                            } else {
                                $trace->setNom($newNom)
                                      ->setJour($jour)
                                      ->setTraceExpedition(true)
                                      ->setModifyAt(new DateTimeImmutable())
                                      ->setModifyBy($this->userHandler->getCurrentUser());
                            }
                        }
                        $this->em->persist($trace);
                        $this->em->flush();
                        $this->em->refresh($trace);
                        
                        $part->setTrace($trace)
                             ->setPa($trace->getPa());
                    } else if ($part->getTrace() !== null) {
                        // Quand le tracé est définis, le tracé n'est pas modifiable, on ne fait rien, on modifie cependant le nom si le nom de l'expédition a changé et si le numéro de la partie a changé
                        if ($part->getTrace()->getNom() !== $newNom) {
                            
                            $trace = $this->em->getRepository(TraceExpedition::class)->find($part->getTrace()->getId());
                            $trace->setNom($newNom)
                                  ->setModifyAt(new DateTimeImmutable())
                                  ->setModifyBy($this->userHandler->getCurrentUser());
                            $this->em->persist($trace);
                            
                        }
                        
                    }
                    
                    
                    // On va regarder s'il y a des expéditionnaires à supprimer, et si ça existe, on va mettre à jour par rapport à ce qu'on a reçu
                    $expediteurBdd = [];
                    $expediteurNew = [];
                    foreach ($partBdd->getExpeditionnaires() as $expediteur) {
                        if ($expediteur->getId() === null) {
                            $expediteurNew[] = $expediteur;
                        } else {
                            $expediteurBdd[$expediteur->getId()] = $expediteur;
                        }
                    }
                    
                    foreach ($part->getExpeditionnaires() as $expediteur) {
                        if (!array_key_exists($expediteur->getId(), $expediteurBdd)) {
                            $part->removeExpeditionnaire($expediteur);
                            $this->em->remove($expediteur);
                        } else {
                            $expediteurBddUnique = $expediteurBdd[$expediteur->getId()];
                            
                            // Si un métier a été choisi, on vient le récupérer
                            if ($expediteurBddUnique->getJob() !== null) {
                                $job =
                                    $this->em->getRepository(JobPrototype::class)->find($expediteurBddUnique->getJob()
                                                                                                            ->getId());
                                $expediteur->setJob($job)->setJobFige($expediteurBddUnique->isJobFige());
                            } else {
                                $expediteur->setJob(null)->setJobFige(false);
                            }
                            
                            // Si un pouvoir a été choisi, on vient le récupérer
                            if ($expediteurBddUnique->getActionHeroic() !== null) {
                                $pouvoir = $this->em->getRepository(HerosPrototype::class)
                                                    ->find($expediteurBddUnique->getActionHeroic()
                                                                               ->getId());
                                $expediteur->setActionHeroic($pouvoir);
                            } else {
                                $expediteur->setActionHeroic(null);
                            }
                            
                            // Si un citoyen a été choisi, on vient le récupérer, et on met l'état de pré-inscrit
                            if ($expediteurBddUnique->getCitoyen() !== null) {
                                $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                                 'citoyen' => $expediteurBddUnique->getCitoyen()
                                                                                                                                  ->getCitoyen()
                                                                                                                                  ->getId()]);
                                $expediteur->setCitoyen($citoyen)
                                           ->setPreinscrit($expediteurBddUnique->isPreinscrit());
                            } else {
                                $expediteur->setPreinscrit(false)
                                           ->setCitoyen(null);
                            }
                            
                            // Si la soif a été choisi, on vient le mettre à jour
                            if ($expediteurBddUnique->isSoif() !== null) {
                                $expediteur->setSoif($expediteurBddUnique->isSoif())->setSoifFige($expediteurBddUnique->isSoifFige());
                            } else {
                                $expediteur->setSoif(null)->setSoifFige(false);
                            }
                            
                            // Si les PA de base ont été choisi, on vient le mettre à jour
                            if ($expediteurBddUnique->getPaBase() !== null) {
                                $expediteur->setPaBase($expediteurBddUnique->getPaBase());
                            } else {
                                $expediteur->setPaBase(null);
                            }
                            
                            // Si les PE de base ont été choisi, on vient le mettre à jour
                            if ($expediteurBddUnique->getPeBase() !== null) {
                                $expediteur->setPeBase($expediteurBddUnique->getPeBase());
                            } else {
                                $expediteur->setPeBase(null);
                            }
                            
                            // Si l'expeditionnaire doit être banni pour cette place
                            if ($expediteurBddUnique->isForBanni()) {
                                $expediteur->setForBanni(true);
                            } else {
                                $expediteur->setForBanni(false);
                            }
                            
                            $expediteur->setPosition($expediteurBddUnique->getPosition());
                            
                            // mise à jour du sac, on sépare les items du sac déjà ajouter et les nouveaux pour les ajouter, supprimer ou modifier
                            $sacBdd = [];
                            $sacNew = [];
                            foreach ($expediteurBddUnique->getSac() as $item) {
                                if ($item->getId() === null) {
                                    $sacNew[] = $item;
                                } else {
                                    $sacBdd[$item->getId()] = $item;
                                }
                            }
                            
                            foreach ($expediteur->getSac() as $item) {
                                if (!array_key_exists($item->getId(), $sacBdd)) {
                                    $expediteur->removeSac($item);
                                    $this->em->remove($item);
                                } else {
                                    $itemBdd = $sacBdd[$item->getId()];
                                    $item->setNbr($itemBdd->getNbr());
                                }
                            }
                            
                            foreach ($sacNew as $newItem) {
                                $item = new SacExpeditionnaire();
                                
                                // On récupère l'item en base de donnée
                                $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($newItem->getItem()->getId());
                                if ($itemProtoBdd === null) {
                                    throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " .
                                                                  __FILE__ . " | Ligne : " . __LINE__);
                                }
                                
                                $item->setItem($itemProtoBdd)
                                     ->setBroken($newItem->isBroken())
                                     ->setNbr($newItem->getNbr());
                                $expediteur->addSac($item);
                            }
                        }
                    }
                    
                    // On balaye maintenant les nouveaux expéditeurs
                    foreach ($expediteurNew as $newExpediteur) {
                        $expeditionnaire = new Expeditionnaire();
                        
                        // Si un métier a été choisi, on vient le récupérer
                        if ($newExpediteur->getJob() !== null) {
                            $job =
                                $this->em->getRepository(JobPrototype::class)->find($newExpediteur->getJob()->getId());
                            $expeditionnaire->setJob($job)->setJobFige($newExpediteur->isJobFige());
                        } else {
                            $expeditionnaire->setJobFige(false);
                        }
                        
                        // Si un pouvoir a été choisi, on vient le récupérer
                        if ($newExpediteur->getActionHeroic() !== null) {
                            $pouvoir =
                                $this->em->getRepository(HerosPrototype::class)->find($newExpediteur->getActionHeroic()
                                                                                                    ->getId());
                            $expeditionnaire->setActionHeroic($pouvoir);
                        }
                        
                        // Si un citoyen a été choisi, on vient le récupérer, et on met l'état de pré-inscrit
                        if ($newExpediteur->getCitoyen() !== null) {
                            $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                             'citoyen' => $newExpediteur->getCitoyen()
                                                                                                                        ->getCitoyen()
                                                                                                                        ->getId()]);
                            $expeditionnaire->setCitoyen($citoyen)
                                            ->setPreinscrit($newExpediteur->isPreinscrit());
                        } else {
                            $expeditionnaire->setPreinscrit(false);
                        }
                        
                        // Si la soif a été choisi, on vient le mettre à jour
                        if ($newExpediteur->isSoif() !== null) {
                            $expeditionnaire->setSoif($newExpediteur->isSoif())->setSoifFige($newExpediteur->isSoifFige());
                        } else {
                            $expeditionnaire->setSoifFige(false);
                        }
                        
                        // Si la place est pour un banni
                        if ($newExpediteur->isForBanni() ?? false) {
                            $expeditionnaire->setForBanni(true);
                        } else {
                            $expeditionnaire->setForBanni(false);
                        }
                        
                        // Si les PA de base ont été choisi, on vient le mettre à jour
                        if ($newExpediteur->getPaBase() !== null) {
                            $expeditionnaire->setPaBase($newExpediteur->getPaBase());
                        }
                        
                        // Si les PE de base ont été choisi, on vient le mettre à jour
                        if ($newExpediteur->getPeBase() !== null) {
                            $expeditionnaire->setPeBase($newExpediteur->getPeBase());
                        }
                        
                        $expeditionnaire->setPosition($newExpediteur->getPosition());
                        
                        // On va ajouter les items du sac
                        foreach ($newExpediteur->getSac() as $item) {
                            $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($item->getItem()->getId());
                            if ($itemProtoBdd === null) {
                                throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " .
                                                              __FILE__ . " | Ligne : " . __LINE__);
                            }
                            
                            $itemNew = new SacExpeditionnaire();
                            $itemNew->setItem($itemProtoBdd)
                                    ->setBroken($item->isBroken())
                                    ->setNbr($item->getNbr());
                            $expeditionnaire->addSac($itemNew);
                        }
                        
                        // On ajoute les créneaux de dispo à chaque expéditionnaire
                        foreach ($creneaux as $creneau) {
                            $dispo = new DispoExpedition();
                            $dispo->setCreneau($creneau);
                            $expeditionnaire->addDispo($dispo);
                        }
                        
                        $part->addExpeditionnaire($expeditionnaire);
                    }
                    
                    // On va regarder si il y a des consignes à supprimer, et si ça existe on va mettre à jour par rapport à ce qu'on a reçu
                    $consigneBdd = [];
                    $consigneNew = [];
                    foreach ($partBdd->getConsignes() as $consigne) {
                        if ($consigne->getId() === null) {
                            $consigneNew[] = $consigne;
                        } else {
                            $consigneBdd[$consigne->getId()] = $consigne;
                        }
                    }
                    
                    foreach ($part->getConsignes() as $consigne) {
                        if (!array_key_exists($consigne->getId(), $consigneBdd)) {
                            $part->removeConsigne($consigne);
                            $this->em->remove($consigne);
                        } else {
                            $consigneBddUnique = $consigneBdd[$consigne->getId()];
                            // Si la zone est à null, on n'enregistre pas la modification de la consigne
                            if ($consigneBddUnique->getZone() === null) {
                                continue;
                            }
                            $zone = $this->em->getRepository(ZoneMap::class)->findOneBy(['ville' => $ville, 'xRel' => $consigneBddUnique->getZone()->getXRel(), 'yRel' => $consigneBddUnique->getZone()->getYRel()]);
                            if ($zone === null) {
                                throw new GestHordesException("Problème lors de la récupération de la zone de la consigne | Fichier : " .
                                                              __FILE__ . " | Ligne : " . __LINE__);
                            }
                            
                            $consigne->setZone($zone)
                                     ->setOrdreConsigne($consigneBddUnique->getOrdreConsigne())
                                     ->setText(trim($consigneBddUnique->getText()));
                        }
                    }
                    
                    // On balaye maintenant les nouvelles consignes
                    foreach ($consigneNew as $newConsigne) {
                        $consigne = new ConsigneExpedition();
                        // Si la zone est à null, on n'enregistre pas la modification de la consigne
                        if ($newConsigne->getZone() === null) {
                            continue;
                        }
                        
                        // On récupère la zone de la consigne
                        $zone = $this->em->getRepository(ZoneMap::class)->findOneBy(['ville' => $ville,
                                                                                     'xRel'  => $newConsigne->getZone()
                                                                                                            ->getXRel(),
                                                                                     'yRel'  => $newConsigne->getZone()
                                                                                                            ->getYRel()]);
                        if ($zone === null) {
                            throw new GestHordesException("Problème lors de la récupération de la zone de la consigne | Fichier : " .
                                                          __FILE__ . " | Ligne : " . __LINE__);
                        }
                        
                        $consigne->setZone($zone)
                                 ->setOrdreConsigne($newConsigne->getOrdreConsigne())
                                 ->setFait(false)
                                 ->setText(trim($newConsigne->getText()));
                        
                        $part->addConsigne($consigne);
                    }
                }
            }
            
            // On va regarder si il y a des parties à ajouter
            foreach ($partieNew as $part) {
                $expeditionPart = new ExpeditionPart();
                $expeditionPart->setNumber($part->getNumber())
                               ->setOuverte($statut)
                               ->setDescription($part->getDescription())
                               ->setPa(0)
                               ->setExpedition($expeditionBdd);
                
                if ($part->getTrace() !== null) {
                    //Récupération du tracé choisi
                    $trace = $this->em->getRepository(TraceExpedition::class)->find($part->getTrace()->getId());
                    
                    $nomExpeditionTrace = ($expeditionBdd->getTypeExpe()->getId() === ExpeditionType::TYPE_RAMASSAGE) ?
                        ($nomExpedition . ' - ' . $part->getNumber()) : $nomExpedition;
                    
                    // si le tracé vient de la bibliothèque, il faut le cloner
                    if ($trace->isBiblio()) {
                        $trace = clone $trace;
                        $trace->setBiblio(false)
                              ->setCollab(true)
                              ->setTraceExpedition(true)
                              ->setJour($jour)
                              ->setNom($nomExpeditionTrace)
                              ->setCreatedBy($this->userHandler->getCurrentUser())
                              ->setCreatedAt(new DateTimeImmutable());
                    } else {
                        if ($trace->isBrouillon()) {
                            $trace->setCollab(true)
                                  ->setBrouillon(false)
                                  ->setTraceExpedition(true)
                                  ->setJour($jour)
                                  ->setNom($nomExpeditionTrace)
                                  ->setCreatedBy($this->userHandler->getCurrentUser())
                                  ->setCreatedAt(new DateTimeImmutable());
                        } else {
                            $trace->setNom($nomExpeditionTrace)
                                  ->setTraceExpedition(true)
                                  ->setJour($jour)
                                  ->setModifyAt(new DateTimeImmutable())
                                  ->setModifyBy($this->userHandler->getCurrentUser());
                        }
                    }
                    $this->em->persist($trace);
                    $this->em->flush();
                    $this->em->refresh($trace);
                    
                    $expeditionPart->setTrace($trace)
                                   ->setPa($trace->getPa());
                    
                }
                
                
                // On crée les expéditionnaires
                foreach ($part->getExpeditionnaires() as $expediteur) {
                    $expeditionnaire = new Expeditionnaire();
                    
                    // Si un métier a été choisi, on vient le récupérer
                    if ($expediteur->getJob() !== null) {
                        $job = $this->em->getRepository(JobPrototype::class)->find($expediteur->getJob()->getId());
                        $expeditionnaire->setJob($job)->setJobFige($expediteur->isJobFige());
                    } else {
                        $expeditionnaire->setJobFige(false);
                    }
                    
                    // Si un pouvoir a été choisi, on vient le récupérer
                    if ($expediteur->getActionHeroic() !== null) {
                        $pouvoir = $this->em->getRepository(HerosPrototype::class)->find($expediteur->getActionHeroic()
                                                                                                    ->getId());
                        $expeditionnaire->setActionHeroic($pouvoir);
                    }
                    
                    // Si un citoyen a été choisi, on vient le récupérer, et on met l'état de pré-inscrit
                    if ($expediteur->getCitoyen() !== null) {
                        $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                         'citoyen' => $expediteur->getCitoyen()
                                                                                                                 ->getCitoyen()
                                                                                                                 ->getId()]);
                        $expeditionnaire->setCitoyen($citoyen)
                                        ->setPreinscrit($expediteur->isPreinscrit());
                    } else {
                        $expeditionnaire->setPreinscrit(false);
                    }
                    
                    // Si la soif a été choisi, on vient le mettre à jour
                    if ($expediteur->isSoif() !== null) {
                        $expeditionnaire->setSoif($expediteur->isSoif())->setSoifFige($expediteur->isSoifFige());
                    } else {
                        $expeditionnaire->setSoifFige(false);
                    }
                    
                    // Si les PA de base ont été choisi, on vient le mettre à jour
                    if ($expediteur->getPaBase() !== null) {
                        $expeditionnaire->setPaBase($expediteur->getPaBase());
                    }
                    // Si les PE de base ont été choisi, on vient le mettre à jour
                    if ($expediteur->getPeBase() !== null) {
                        $expeditionnaire->setPeBase($expediteur->getPeBase());
                    }
                    
                    // Si la place est pour un banni
                    if ($expediteur->isForBanni() ?? false) {
                        $expeditionnaire->setForBanni(true);
                    } else {
                        $expeditionnaire->setForBanni(false);
                    }
                    
                    $expeditionnaire->setPosition($expediteur->getPosition());
                    
                    // On va ajouter les items du sac
                    foreach ($expediteur->getSac() as $item) {
                        $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($item->getItem()->getId());
                        if ($itemProtoBdd === null) {
                            throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
                        }
                        
                        $itemNew = new SacExpeditionnaire();
                        $itemNew->setItem($itemProtoBdd)
                                ->setBroken($item->isBroken())
                                ->setNbr($item->getNbr());
                        $expeditionnaire->addSac($itemNew);
                    }
                    
                    // On ajoute les créneaux de dispo à chaque expéditionnaire
                    foreach ($creneaux as $creneau) {
                        $dispo = new DispoExpedition();
                        $dispo->setCreneau($creneau);
                        $expeditionnaire->addDispo($dispo);
                    }
                    
                    $expeditionPart->addExpeditionnaire($expeditionnaire);
                }
                
                // On crée les consignes si il y en a
                foreach ($part->getConsignes() as $consigne) {
                    $consigneNew = new ConsigneExpedition();
                    
                    // si la zone est null, on n'enregistre pas la consigne
                    if ($consigne->getZone() === null) {
                        continue;
                    }
                    
                    // On récupère la zone de la consigne
                    $zone = $this->em->getRepository(ZoneMap::class)->findOneBy(['ville' => $ville, 'xRel' => $consigne->getZone()->getXRel(), 'yRel' => $consigne->getZone()->getYRel()]);
                    if ($zone === null) {
                        throw new GestHordesException("Problème lors de la récupération de la zone de la consigne | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
                    }
                    
                    $consigneNew->setZone($zone)
                                ->setOrdreConsigne($consigne->getOrdreConsigne())
                                ->setFait(false)
                                ->setText(trim($consigne->getText()));
                    
                    $expeditionPart->addConsigne($consigneNew);
                }
                
                $expeditionBdd->addExpeditionPart($expeditionPart);
            }
            
            $expeditionBdd->setVerrou(false)
                          ->setVerrouAt(null);
            
            $this->em->persist($expeditionBdd);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    public function phrasePrefaite(): array
    {
        return [
            "Privilégier les objets de liste de course",
            "Si départ en PAT, Praffe ici de nuit.",
            "Si départ en PAT, Praffe ici de nuit. Au matin, tu laisses les objets trouvés et tu fais ton tracé",
            "Praffer à partir d’ici",
            "Ramasser à partir d’ici",
            "Tuer X Zombies sur la case",
            "Tuer tous les zombies sur la case",
            "Départ à partir de 7h",
            "Départ en PAT",
            "Ramasser ...",
            "Donner ... à ...",
            "Récupérer ... à ...",
            "Penser à renvoyer le bichon",
            "Penser à faire tester son manuel à/aux ermites",
            "Utiliser vos uppercuts ou les armes trouvées en expé",
            "Laisser ...",
            "Ordre de prise: ...",
            "Praffer et ramasser à partir d'ici",
            "Futur banni",
        ];
    }
    
    /**
     * @throws GestHordesException
     */
    public function recuperationExpeditionById(GetExpedition $getExpedition, Ville $ville): Expedition
    {
        // On récupère d'abord l'outils Expedition
        $outilsExpe = $this->recuperationOutilsExpeditionJour($ville, $getExpedition->getJour());
        
        // On récupère l'expédition
        $expedition = $this->em->getRepository(Expedition::class)->find($getExpedition->getExpeditionId());
        
        if ($expedition === null) {
            throw new GestHordesException("L'expédition n'existe pas");
        }
        
        // On vérifie que l'expedition appartient bien à l'outils expédition
        if ($expedition->getOutilsExpedition() !== $outilsExpe) {
            throw new GestHordesException("L'expédition n'appartient pas à l'outils expédition");
        }
        
        return $expedition;
    }
    
    public function recuperationExpeditionWhereIAm(Ville $ville, User $user): array
    {
        //On récupère l'outils du jour
        $outils = $this->em->getRepository(Outils::class)->findOneBy(['ville' => $ville->getId(), 'day' => $ville->getJour()]);
        if ($outils === null) {
            return [];
        } else {
            if ($outils->getExpedition() === null) {
                return [];
            } else {
                // On balaye toutes les expéditions et les parties associés pour fournir la liste des id des expés dont je suis inscrit
                $expeditions = [];
                foreach ($outils->getExpedition()->getExpeditions() as $expedition) {
                    foreach ($expedition->getExpeditionParts() as $part) {
                        foreach ($part->getExpeditionnaires() as $expeditionnaire) {
                            if ($expeditionnaire->getCitoyen() !== null && $expeditionnaire->getCitoyen()->getCitoyen()->getId() === $user->getId() && $part->getTrace() !== null) {
                                $expeditions[] = $part->getTrace()->getId();
                            }
                        }
                    }
                }
                return $expeditions;
                
            }
        }
    }
    
    /**
     * @throws JsonException
     */
    public function recuperationListeTraceExpedition(Ville $ville): array
    {
        $listeTraceCarte = $this->em->getRepository(TraceExpedition::class)->getListExpedition($ville);
        
        $listeTraceBrouillon = $this->em->getRepository(TraceExpedition::class)->findBy(['ville' => $ville, 'biblio' => false, 'brouillon' => true]);
        $listeTraceBiblio    = $this->em->getRepository(TraceExpedition::class)->findBy(['ville' => $ville, 'biblio' => true, 'brouillon' => false]);
        
        return [
            'carte'  => $this->serializerService->serializeArray($listeTraceCarte, 'json', ['expe', 'general_res']),
            'outils' => $this->serializerService->serializeArray($listeTraceBrouillon, 'json', ['expe', 'general_res']),
            'biblio' => $this->serializerService->serializeArray($listeTraceBiblio, 'json', ['expe', 'general_res']),
        ];
    }
    
    /**
     * @throws JsonException
     */
    public function recuperationListeTypologieTraduite(): array
    {
        $listTypeExpe = $this->em->getRepository(ExpeditionType::class)->findAll();
        
        $listJob = $this->em->getRepository(JobPrototype::class)->findAllExceptAutre();
        
        $listPouvoirHerosActif = $this->em->getRepository(HerosPrototype::class)->findPouvActif();
        
        return [
            'typeExpe'  => $this->serializerService->serializeArray($listTypeExpe, 'json', ['outils_expe']),
            'job'       => $this->serializerService->serializeArray($listJob, 'json', ['outils_expe']),
            'pouvoir'   => $this->serializerService->serializeArray($listPouvoirHerosActif, 'json', ['carte_gen']),
            'direction' => $this->translateHandler->direction_fao(),
            'phrase'    => $this->phrasePrefaite(),
        ];
    }
    
    public function recuperationOutilsExpeditionJour(Ville $ville, int $jour): OutilsExpedition
    {
        
        
        $outils = $this->em->getRepository(Outils::class)->findOneBy(['ville' => $ville->getId(), 'day' => $jour]);
        
        // si les outils n'existent pas, on le crée
        if ($outils === null) {
            // On récupère une nouvelle instance de ville pour éviter la maj de prototype
            $villeOutils = $this->em->getRepository(Ville::class)->find($ville->getId());
            
            $outils = new Outils($villeOutils, $jour);
            $this->em->persist($outils);
            $this->em->flush();
            $this->em->refresh($outils);
            
            // On crée les outils expédition vide
            $outilsExpe = new OutilsExpedition();
            $outilsExpe->setOutils($outils);
            $this->em->persist($outilsExpe);
            $this->em->flush();
            $this->em->refresh($outilsExpe);
            
        } else {
            // on vérifie si les outils expédition existe
            if ($outils->getExpedition() === null) {
                $outilsExpe = new OutilsExpedition();
                $outils->setExpedition($outilsExpe);
                $this->em->persist($outils);
                $this->em->flush();
                $this->em->refresh($outils);
            }
            $outilsExpe = $outils->getExpedition();
        }
        
        return $outilsExpe;
    }
    
    /**
     * @throws JsonException
     */
    public function recuperationOutilsExpeditionVilleJour(Ville $ville, int $jour): array
    {
        
        $outilsExpedition = $this->recuperationOutilsExpeditionJour($ville, $jour);
        
        $outilsExpe = $this->serializerService->serializeArray($outilsExpedition, 'json', ['outils_expe', 'general_res', 'carte_gen']);
        
        $popUpMaj = $this->itemsHandler->recuperationItems([9, 10], 'outils_expe', 'carte', true);
        
        $itemsAllIndex = $this->em->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $sacRapide = [
            [
                'id'    => 1,
                'nom'   => "Kit 18 PA",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[118],
                ],
            ],
            [
                'id'    => 2,
                'nom'   => "Kit 19 PA",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                ],
            ],
            [
                'id'    => 3,
                'nom'   => "Kit 18 PA + Sac Supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[118],
                    $itemsAllIndex[21],
                ],
            ],
            [
                'id'    => 4,
                'nom'   => "Kit 19 PA + Sac Supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[21],
                ],
            ],
            //            [
            //                'id'    => 5,
            //                'nom'   => $this->translator->trans("Kit 24 PA", [], 'outils'),
            //                'items' => [
            //                    $itemsAllIndex[1],
            //                    $itemsAllIndex[118],
            //                    $itemsAllIndex[43],
            //                ],
            //            ],
            [
                'id'    => 6,
                'nom'   => "Kit 25 PA",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[43],
                ],
            ],
            //            [
            //                'id'    => 7,
            //                'nom'   => $this->translator->trans("Kit 24 PA + Sac supp", [], 'outils'),
            //                'items' => [
            //                    $itemsAllIndex[1],
            //                    $itemsAllIndex[118],
            //                    $itemsAllIndex[43],
            //                    $itemsAllIndex[21],
            //                ],
            //            ],
            [
                'id'    => 8,
                'nom'   => "Kit 25 PA + Sac supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[43],
                    $itemsAllIndex[21],
                ],
            ],
            [
                'id'    => 9,
                'nom'   => "Kit 27 PA",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[70],
                ],
            ],
            [
                'id'    => 10,
                'nom'   => "Kit 27 PA + Sac supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[70],
                    $itemsAllIndex[21],
                ],
            ],
            [
                'id'    => 11,
                'nom'   => "Kit 18 PA + OB",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[118],
                    $itemsAllIndex[20],
                ],
            ],
            [
                'id'    => 12,
                'nom'   => "Kit 19 PA + OB",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[20],
                ],
            ],
            [
                'id'    => 13,
                'nom'   => "Kit 27 PA + OB",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[70],
                    $itemsAllIndex[20],
                ],
            ],
            [
                'id'    => 14,
                'nom'   => "Kit 18 PA + OB + Sac supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[118],
                    $itemsAllIndex[20],
                    $itemsAllIndex[21],
                ],
            ],
            [
                'id'    => 15,
                'nom'   => "Kit 19 PA + OB + Sac supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[20],
                    $itemsAllIndex[21],
                ],
            ],
            [
                'id'    => 16,
                'nom'   => "Kit 27 PA + OB + Sac supp",
                'items' => [
                    $itemsAllIndex[1],
                    $itemsAllIndex[172],
                    $itemsAllIndex[70],
                    $itemsAllIndex[20],
                    $itemsAllIndex[21],
                ],
            ],
        ];
        
        $sacRapideArray = $this->serializerService->serializeArray($sacRapide, 'json', ['outils_expe']);
        
        $expeditionVerou = $this->getExpeditionVerou($outilsExpedition);
        
        return [
            'outilsExpeditions' => $outilsExpe,
            'liste'             => $this->recuperationListeTypologieTraduite(),
            'isModeExpediton'   => true,
            'liste_trace'       => $this->recuperationListeTraceExpedition($ville),
            'jour'              => $jour,
            'popUpMaj'          => $popUpMaj,
            'sacRapide'         => $sacRapideArray,
            'expeVerrou'        => $expeditionVerou,
        ];
    }
    
    /**
     * @throws GestHordesException
     */
    public function sauvegardeExpeditionOutils(SauvegardeExpeditionOutils $sauvegardeExpeditionOutils): void
    {
        try {
            // Dans un premier temps, on regarde si un id est présent, si déjà présent, on ira vers la fonction de mise à jour
            // sinon, on ira vers la fonction de création
            if ($sauvegardeExpeditionOutils->getExpedition()->getId() !== null) {
                $this->miseAJourExpeditionOutils($sauvegardeExpeditionOutils);
            } else {
                $this->creationExpeditionOutils($sauvegardeExpeditionOutils);
            }
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function sauvegardeOuvrier(SauvegardeOuvriers $ouvriers): void
    {
        try {
            
            // récupération de la ville
            $ville = $this->em->getRepository(Ville::class)->findOneBy(['mapId' => $ouvriers->getMapId()]);
            
            // On récupère tous les créneaux pour expeditions afin de les rajouter à chaque dispo d'un expeditionnaire
            $creneaux = $this->em->getRepository(CreneauHorraire::class)->findBy(['typologie' => 2]);
            
            // récupération de l'outils expédition sur lequel s'appuyer
            $outilsExpe = $this->recuperationOutilsExpeditionJour($ville, $ouvriers->getJour());
            
            // On va séparer les ouvriers avec un id de ceux qui ont pas d'id pour la création, modification, suppression
            $ouvrierBdd = [];
            $ouvrierNew = [];
            
            foreach ($ouvriers->getOuvriers() as $ouvrier) {
                if ($ouvrier->getId() === null) {
                    $ouvrierNew[] = $ouvrier;
                } else {
                    $ouvrierBdd[$ouvrier->getId()] = $ouvrier;
                }
            }
            
            // On récupère les ouvriers actuellement en base de donnée
            $ouvriersBdd = $outilsExpe->getOuvriers();
            
            // On va regarder si il y a des ouvriers à supprimer, et si ça existe on va mettre à jour par rapport à ce qu'on a reçu
            foreach ($ouvriersBdd as $ouvrier) {
                if (!array_key_exists($ouvrier->getId(), $ouvrierBdd)) {
                    $outilsExpe->removeOuvrier($ouvrier);
                    $this->em->remove($ouvrier);
                } else {
                    $ouvrierBddUnique = $ouvrierBdd[$ouvrier->getId()];
                    
                    if ($ouvrierBddUnique->getJob() !== null) {
                        $job =
                            $this->em->getRepository(JobPrototype::class)->find($ouvrierBddUnique->getJob()->getId());
                        $ouvrier->setJob($job)
                                ->setJobFige($ouvrierBddUnique->isJobFige());
                    } else {
                        $ouvrier->setJob(null)
                                ->setJobFige(false);
                    }
                    
                    if ($ouvrierBddUnique->getConsigne() !== $ouvrier->getConsigne()) {
                        $ouvrier->setConsigne($ouvrierBddUnique->getConsigne());
                    }
                    
                    if ($ouvrierBddUnique->getCitoyen() !== null &&
                        $ouvrierBddUnique->getCitoyen() !== $ouvrier->getCitoyen()) {
                        $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville'   => $ville,
                                                                                         'citoyen' => $ouvrierBddUnique->getCitoyen()
                                                                                                                       ->getCitoyen()
                                                                                                                       ->getId()]);
                        $ouvrier->setCitoyen($citoyen)
                                ->setPreinscrit($ouvrierBddUnique->isPreinscrit());
                    } else {
                        $ouvrier->setCitoyen(null)
                                ->setPreinscrit(false);
                    }
                    
                    if ($ouvrierBddUnique->isSoif() !== null) {
                        $ouvrier->setSoifFige($ouvrierBddUnique->isSoifFige());
                    } else {
                        $ouvrier->setSoifFige(false);
                    }
                    
                    // Si c'est une place pour ouvrier Banni
                    if ($ouvrierBddUnique->isForBanni()) {
                        $ouvrier->setForBanni(true);
                    } else {
                        $ouvrier->setForBanni(false);
                    }
                    
                    $ouvrier->setSoif($ouvrierBddUnique->isSoif())
                            ->setPosition($ouvrierBddUnique->getPosition());
                    
                    // Séparation des items sac déjà en base de donnée des nouveaux pour traiter les ajouts, suppressions et modifications
                    /** @var SacExpeditionnaire[] $sacBdd */
                    $sacBdd = [];
                    /** @var SacExpeditionnaire[] $sacNew */
                    $sacNew = [];
                    foreach ($ouvrierBddUnique->getSac() as $item) {
                        if ($item->getId() === null) {
                            $sacNew[] = $item;
                        } else {
                            $sacBdd[$item->getId()] = $item;
                        }
                    }
                    
                    foreach ($ouvrier->getSac() as $item) {
                        if (!array_key_exists($item->getId(), $sacBdd)) {
                            $ouvrier->removeSac($item);
                            $this->em->remove($item);
                        } else {
                            $itemBdd = $sacBdd[$item->getId()];
                            $item->setNbr($itemBdd->getNbr());
                        }
                    }
                    
                    foreach ($sacNew as $newItem) {
                        $item = new SacExpeditionnaire();
                        
                        // On récupère l'item en base de donnée
                        $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($newItem->getItem()->getId());
                        if ($itemProtoBdd === null) {
                            throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " .
                                                          __FILE__ . " | Ligne : " . __LINE__);
                        }
                        
                        $item->setItem($itemProtoBdd)
                             ->setBroken($newItem->isBroken())
                             ->setNbr($newItem->getNbr());
                        $ouvrier->addSac($item);
                    }
                }
            }
            
            // On balaye maintenant les nouveaux ouvriers
            foreach ($ouvrierNew as $ouvrier) {
                $ouvrierNew = new Ouvriers();
                
                if ($ouvrier->getJob() !== null) {
                    $job = $this->em->getRepository(JobPrototype::class)->find($ouvrier->getJob()->getId());
                    $ouvrierNew->setJob($job)->setJobFige(true);
                } else {
                    $ouvrierNew->setJob(null)->setJobFige(false);
                }
                
                if ($ouvrier->getConsigne() !== null) {
                    $ouvrierNew->setConsigne($ouvrier->getConsigne());
                }
                
                if ($ouvrier->getCitoyen() !== null) {
                    $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['ville' => $ville, 'citoyen' => $ouvrier->getCitoyen()->getCitoyen()->getId()]);
                    $ouvrierNew->setCitoyen($citoyen)
                               ->setPreinscrit(true);
                } else {
                    $ouvrierNew->setCitoyen(null)
                               ->setPreinscrit(false);
                }
                if ($ouvrier->isSoif() !== null) {
                    $ouvrierNew->setSoifFige(true);
                } else {
                    $ouvrierNew->setSoifFige(false);
                }
                $ouvrierNew->setSoif($ouvrier->isSoif())
                           ->setPosition($ouvrier->getPosition());
                
                // Si c'est une place pour ouvrier banni
                if ($ouvrier->isForBanni()) {
                    $ouvrierNew->setForBanni(true);
                } else {
                    $ouvrierNew->setForBanni(false);
                }
                
                // On va ajouter les items du sac
                foreach ($ouvrier->getSac() as $item) {
                    $itemProtoBdd = $this->em->getRepository(ItemPrototype::class)->find($item->getItem()->getId());
                    if ($itemProtoBdd === null) {
                        throw new GestHordesException("Problème lors de la récupération de l'item | Fichier : " .
                                                      __FILE__ . " | Ligne : " . __LINE__);
                    }
                    
                    $itemNew = new SacExpeditionnaire();
                    $itemNew->setItem($itemProtoBdd)
                            ->setBroken($item->isBroken())
                            ->setNbr($item->getNbr());
                    $ouvrierNew->addSac($itemNew);
                }
                
                // On ajoute les créneaux de dispo à chaque expéditionnaire
                foreach ($creneaux as $creneau) {
                    $dispo = new DispoExpedition();
                    $dispo->setCreneau($creneau);
                    $ouvrierNew->addDispo($dispo);
                }
                
                $outilsExpe->addOuvrier($ouvrierNew);
            }
            
            if ($ouvriers->getDirectionFao() !== null) {
                $outilsExpe->setFaoDirection($ouvriers->getDirectionFao());
            } else {
                $outilsExpe->setFaoDirection(null);
            }
            
            $outilsExpe->setNbrOuvrier(count($outilsExpe->getOuvriers()));
            if ($outilsExpe->getCreatedBy() === null) {
                $outilsExpe->setCreatedBy($this->userHandler->getCurrentUser())
                           ->setCreatedAt(new DateTimeImmutable());
            } else {
                $outilsExpe->setModifyBy($this->userHandler->getCurrentUser())
                           ->setModifyAt(new DateTimeImmutable());
            }
            
            $this->em->persist($outilsExpe);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    private function setHorizontalMiddleCells(array &$tabExpe, int $y, int $startX, int $endX, int $value): void
    {
        for ($x = $startX; $x < $endX; $x++) {
            $tabExpe[$y][$x][] = $value;
        }
    }
    
    private function setStartCell(array &$tabExpe, int $y, int $x, array $mappings, int $defaultValue): void
    {
        if ($tabExpe[$y][$x] === null) {
            $tabExpe[$y][$x][] = $defaultValue;
        } else {
            $key                   = array_key_last($tabExpe[$y][$x]);
            $tabExpe[$y][$x][$key] = $mappings[$tabExpe[$y][$x][$key]] ?? $tabExpe[$y][$x][$key];
        }
    }
    
    private function setVerticalMiddleCells(array &$tabExpe, int $x, int $startY, int $endY, int $value): void
    {
        for ($y = $startY; $y < $endY; $y++) {
            $tabExpe[$y][$x][] = $value;
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function suppressionExpedition(SuppressionExpedition $suppressionExpedition): void
    {
        try {
            
            // On récupère l'expédition qu'on souhaite supprimer
            $expedition = $this->em->getRepository(Expedition::class)->find($suppressionExpedition->getExpeditionId());
            
            if ($expedition === null) {
                throw new GestHordesException("L'expédition n'existe pas | Fichier : " . __FILE__ . " | Ligne : " . __LINE__);
            }
            
            // On supprime tous les tracés associés également
            foreach ($expedition->getExpeditionParts() as $part) {
                
                if ($part->getTrace() === null) {
                    continue;
                }
                
                $trace = $this->entityManager->getRepository(TraceExpedition::class)->findOneBy(['id' => $part->getTrace()->getId()]);
                
                if ($trace !== null) {
                    $this->em->remove($trace);
                }
            }
            
            $this->em->remove($expedition);
            $this->em->flush();
            
        } catch (Exception $e) {
            throw new GestHordesException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    /**
     * @throws GestHordesException
     */
    private function verrouillageExpedition(Expedition &$expedition): void
    {
        // On vérifie que l'expédition n'est pas déjà verrouillée
        if ($expedition->isVerrou() && $expedition->getVerrouBy()->getId() !== $this->userHandler->getCurrentUser()->getId()) {
            throw new GestHordesException($this->translator->trans("L'expédition est déjà verrouillée par {user}", ["{user}" => $this->userHandler->getCurrentUser()->getPseudo()], 'outils'), GestHordesException::SHOW_MESSAGE);
        }
        
        // On peut mettre à jour le verrou
        $expedition->setVerrou(true)
                   ->setVerrouBy($this->userHandler->getCurrentUser())
                   ->setVerrouAt(new DateTimeImmutable());
    }
    
}