<?php


namespace App\Service\Outils;


use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\Outils;
use App\Entity\OutilsChantier;
use App\Entity\RessourceChantier;
use App\Entity\UpChantier;
use App\Entity\Ville;
use App\Exception\GestHordesException;
use App\Service\GestHordesHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class ChantierHandler
{
    public function __construct(protected EntityManagerInterface $entityManager, protected LoggerInterface $logger,
                                protected GestHordesHandler      $gh)
    {
    }
    
    public function getClassPlans(Ville $ville, ChantierPrototype $chantierPrototype): string
    {
        if ($chantierPrototype->getPlan() === 0 && $ville->getChantier($chantierPrototype->getId()) === null) {
            return '';
        }
        
        if ($ville->getPlansChantier($chantierPrototype->getId()) !== null) {
            return ($ville->getChantier($chantierPrototype->getId()) !== null) ? "chCons" :
                ("plObte");
        } else {
            return ($ville->getChantier($chantierPrototype->getId()) !== null) ? "chCons" :
                ("chBlock");
        }
    }
    
    public function getDaysOfDestructionChantier(Ville $ville, int $idChantier): array
    {
        $chantiers = $this->entityManager->getRepository(Chantiers::class)->findOneBy(['ville' => $ville, 'chantier' => $idChantier]);
        
        if ($chantiers === null) {
            return [];
        } else {
            return $chantiers->getJourDestruction();
        }
    }
    
    public function getDechargeEvolutionDechargePublique(Ville $ville, ChantierPrototype $chantierDecharge): bool
    {
        $evolutionChantier = $this->entityManager->getRepository(UpChantier::class)
                                                 ->findOneBy(['chantier' => ChantierPrototype::ID_CHANTIER_DECHARGE_PUBLIQUE,
                                                              'ville'    => $ville]);
        
        if ($evolutionChantier === null) {
            return false;
        }
        
        switch ($chantierDecharge) {
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_DECHARGE_PIEGE:
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_APPAT:
                return $evolutionChantier->getLvlActuel() >= 1;
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_ENCLOS:
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_DECHARGE_BLINDE:
                return $evolutionChantier->getLvlActuel() >= 2;
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_FERRAILLERIE:
            case $chantierDecharge->getId() === ChantierPrototype::ID_CHANTIER_DECHARDE_BOIS:
                return $evolutionChantier->getLvlActuel() >= 3;
            default:
                return false;
        }
    }
    
    /**
     * @throws GestHordesException
     */
    public function getOutilsChantierOfTown(Ville $ville): Outils
    {
        try {
            $outils = $this->entityManager->getRepository(Outils::class)->findOneBy(['ville' => $ville,
                                                                                     'day'   => $ville->getJour()]);
            
            if ($outils === null || $outils->getOutilsChantier() === null) {
                
                if ($outils === null) {
                    $outils = new Outils($ville, $ville->getJour());
                    $this->entityManager->persist($outils);
                    $this->entityManager->flush();
                    $this->entityManager->refresh($outils);
                }
                
                $outilsChantier = new OutilsChantier();
                $outilsChantier->setPaChantier(0);
                
                $outils->setOutilsChantier($outilsChantier);
                
            }
            
            return $outils;
        } catch (Exception $e) {
            throw new GestHordesException('Erreur dans la génération de Outils' . $e->getMessage());
        }
        
    }
    
    public function getRessourcesComparatif(array $itemsBanques, RessourceChantier $ressourceChantier): string
    {
        $nbrItemBanque = $itemsBanques[$ressourceChantier->getItem()->getId()]['nbrItem'] ?? 0;
        
        $couleurSpan = ($nbrItemBanque >= $ressourceChantier->getNombre()) ? "color-green" : "color-red";
        
        return "<span>{$nbrItemBanque} / <span class='{$couleurSpan} ressourcesChantier'>{$ressourceChantier->getNombre()}</span> <span class='infoBulle'>{$this->gh->svgImg($ressourceChantier->getItem()->getIcon())}<span class='info'>{$ressourceChantier->getItem()->getNom()}</span></span></span>";
    }
    
    public function returnPlanChantier(ChantierPrototype $chantierPrototype): ?string
    {
        return match ($chantierPrototype->getPlan()) {
            0 => '',
            1 => 'item_bplan_c',
            2 => 'item_bplan_u',
            3 => 'item_bplan_r',
            4 => 'item_bplan_e',
        };
    }
    
}
