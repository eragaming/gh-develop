<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\GestHordesException;
use App\Exception\MyHordesAttackException;
use App\Exception\MyHordesMajApiException;
use App\Exception\MyHordesMajException;
use App\Exception\UserNotFoundException;
use App\Exception\AuthenticationException as AuthenticationExceptionGH;
use App\Service\Utils\DiscordService;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Random\RandomException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class ErrorHandlingService
{
    
    public function __construct(private readonly LoggerInterface     $logger, private readonly Security $security,
                                private readonly DiscordService      $discordService,
                                private readonly TranslatorInterface $translator)
    {
    }
    
    public function handleException(Throwable $exception, string $page = '', string $fonction = ''): JsonResponse
    {
        try {
            $errorCode = bin2hex(random_bytes(4)); // Génère un code d'erreur unique
        } catch (RandomException) {
            $errorCode = '00000000';
        }
        
        /** @var User $user */
        $user     = $this->security->getUser();
        $userInfo = $user ? "User ID: {$user->getId()} - {$user->getPseudo()}" : "User non authentifié";
        
        $logMessage = "Error Code: {$errorCode} | {$userInfo} | Page: {$page} | Fonction: {$fonction} | {$exception->getMessage()}";
        $this->logger->error($logMessage, ['exception' => $exception]);
        
        if ($this->shouldNotifyDiscord($exception)) {
            try {
                $this->discordService->generateMessageLogDiscord($logMessage);
            } catch (GuzzleException) {
                // On ne fait rien, on ne veut pas que l'erreur de Discord empêche l'utilisateur de faire son action
            }
        }
        
        if ($exception instanceof UserNotFoundException) {
            return new JsonResponse(['error' => $this->translator->trans("Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_BAD_REQUEST);
        }
        
        if ($exception instanceof AuthenticationException || $exception instanceof AuthenticationExceptionGH) {
            return new JsonResponse(['error' => $this->translator->trans("Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_UNAUTHORIZED);
        }
        
        if ($exception instanceof AccessDeniedException) {
            if ($exception->getMessage() === "Acces non autorisé, il ne s'agit pas de votre ville") {
                return new JsonResponse(['error' => $this->translator->trans("Vous essayez d'accéder aux outils d'une ville dont vous ne faites pas partis. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_UNAUTHORIZED);
            }
            return new JsonResponse(['error' => $this->translator->trans("Merci de vous authentifier sur le site, ou alors de réactualiser votre connexion avant de refaire votre action. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_UNAUTHORIZED);
        }
        
        if ($exception instanceof GestHordesException) {
            if ($exception->getCode() === GestHordesException::SHOW_MESSAGE) {
                return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
            } else if ($exception->getCode() === GestHordesException::SHOW_MESSAGE_INFO) {
                return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_CONFLICT);
            } else if ($exception->getCode() === GestHordesException::SHOW_MESSAGE_REDIRECT) {
                return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_CONFLICT);
            } else {
                return new JsonResponse(['error' => $this->translator->trans("Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_BAD_REQUEST);
            }
        }
        
        if ($exception instanceof MyHordesAttackException) {
            return new JsonResponse(['retour' => 0, 'lib' => $this->translator->trans("Attaque ou maintenance de MyHordes en cours, mise à jour impossible", [], 'app')], Response::HTTP_UNAUTHORIZED,);
        }
        
        if ($exception instanceof MyHordesMajException) {
            if ($exception->getCode() === MyHordesMajException::TOO_MANY_REQUESTS) {
                return new JsonResponse(['retour' => 0, 'lib' => $this->translator->trans("L'application ou vous avez atteins votre limite d'appel à l'API, veuillez patienter que le quotat se réinitialise :{code}", ['{code}' => $errorCode], 'app')], Response::HTTP_TOO_MANY_REQUESTS,);
            } else if ($exception->getCode() === MyHordesMajException::INVALID_USER_KEY || $exception->getCode() === MyHordesMajException::TOKEN_NULL) {
                return new JsonResponse(['retour' => 0, 'lib' => $this->translator->trans("Votre userkey fournis n'est pas valide :{code}", ['{code}' => $errorCode], 'app')], Response::HTTP_BAD_REQUEST,);
            } else {
                return new JsonResponse(['retour' => 0, 'lib' => $this->translator->trans("Une erreur s'est produite lors de la récupération des données :{code}", ['{code}' => $errorCode], 'app')], Response::HTTP_INTERNAL_SERVER_ERROR,);
            }
        }
        
        if ($exception instanceof MyHordesMajApiException) {
            return new JsonResponse(['retour' => 0, 'lib' => $this->translator->trans("Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if ($exception instanceof ProcessFailedException) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        // Réponse générique pour les autres exceptions
        return new JsonResponse(['error' => $this->translator->trans("Un problème est survenu, merci de réessayer, et si cela persiste, veuillez contacter l'administrateur. Code erreur :{code}", ['{code}' => $errorCode], 'outils')], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    
    private function shouldNotifyDiscord(Throwable $exception): bool
    {
        // Exception pour lesquelle on n'envoie pas de message sur Discord, il est également possible de faire en fonction de la gravité de l'exception mais pas fait ici mais possiblement pour évolution
        return !($exception instanceof UserNotFoundException) &&
               !($exception instanceof AuthenticationException) &&
               !($exception instanceof AuthenticationExceptionGH) &&
               !($exception instanceof GestHordesException && $exception->getCode() === GestHordesException::SHOW_MESSAGE_INFO) &&
               !($exception instanceof MyHordesAttackException);
    }
}