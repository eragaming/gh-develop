<?php

namespace App\Service;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class CommandHelper
{
    
    /**
     * @param string $command
     * @param int|null $ret
     * @param bool $detach
     * @param SymfonyStyle|null $io
     * @return array
     */
    public function bin(string $command, ?int &$ret = null, bool $detach = false, ?SymfonyStyle $io = null): array
    {
        $process = Process::fromShellCommandline($command);
        $process->setTimeout(null);                  // No timeout
        $process->setTty(Process::isTtySupported()); // Enable TTY if supported
        $lines = [];
        
        if ($io) {
            $process->run(function ($type, $buffer) use ($io, &$lines) {
                $lines[] = $buffer;
                $io->write($buffer);
            });
        } else {
            $process->run();
            $lines = explode("\n", $process->getOutput());
        }
        
        $ret = $process->getExitCode();
        
        return $lines;
    }
    
    public function capsule(string $command, SymfonyStyle $io, ?string $note = null, bool $bin_console = true): bool
    {
        
        $run_command = $bin_console ? "php bin/console $command 2>&1" : "$command";
        
        $verbose = $io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE;
        
        $io->text($note ?? "<info>Exécution de la commande " . ($bin_console ? 'encapsulé' : '') . " \"<comment>$command</comment>\"... </info>");
        $lines = $this->bin($run_command, $ret, false, $verbose ? $io : null);
        
        if ($ret !== 0) {
            if ($note !== null) {
                $io->info("La commande était \"<comment>{$run_command}</comment>\"");
            }
            if (!$verbose) {
                foreach ($lines as $line) {
                    if (empty($line)) continue;
                    $io->text("> {$line}");
                }
            }
            $io->error("Commande terminée avec un code d'erreur : {$ret}");
        } else {
            $io->success("Commande terminée avec succès");
        }
        
        return $ret === 0;
        
    }
    
    public function getJsonFile(string $path): array
    {
        $json = file_get_contents($path);
        
        return json_decode($json, true);
    }
    
}