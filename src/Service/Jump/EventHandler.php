<?php

namespace App\Service\Jump;


use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventHandler
{
    
    public function __construct(protected EntityManagerInterface $em, protected LoggerInterface $logger,
                                protected TranslatorInterface    $translator)
    {
    }
    
    public function recuperationEvent(string $idEvent): ?Event
    {
        return $this->em->getRepository(Event::class)->findOneBy(['id' => $idEvent]);
    }
    
    
}