<?php

namespace App\Service\Hotel;

use App\Entity\Citoyens;
use App\Entity\ConsigneExpedition;
use App\Entity\CreneauHorraire;
use App\Entity\Expedition;
use App\Entity\Expeditionnaire;
use App\Entity\ExpeditionPart;
use App\Entity\JobPrototype;
use App\Entity\OutilsExpedition;
use App\Entity\Ouvriers;
use App\Entity\TraceExpedition;
use App\Entity\TypeDispo;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\ZoneMap;
use App\Exception\GestHordesException;
use App\Service\AbstractGHHandler;
use App\Service\Generality\TranslateHandler;
use App\Service\GestHordesHandler;
use App\Service\Outils\ExpeditionHandler;
use App\Service\UserHandler;
use App\Service\Utils\SerializerService;
use App\Structures\Dto\GH\Hotel\Expeditions\ConsigneValidation;
use App\Structures\Dto\GH\Hotel\Expeditions\InscriptionExpeditionSauvegarde;
use App\Structures\Dto\GH\Hotel\Expeditions\SauvegardeCommentaire;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class InscriptionExpeditionHandler extends AbstractGHHandler
{
    public function __construct(protected EntityManagerInterface $em,
                                protected LoggerInterface        $logger,
                                protected TranslatorInterface    $translator,
                                protected TranslateHandler       $translateHandler,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected SerializerService      $serializerService,
                                protected ExpeditionHandler      $expeditionHandler,
    )
    {
        parent::__construct($em, $logger, $translator, $translateHandler, $userHandler, $gh, $serializerService);
    }
    
    /**
     * @throws JsonException
     */
    public function generationMiniMap(Ville $ville, User $user, TraceExpedition $traceExpedition): array
    {
        // On va d'abord générer la mini map vide pour pouvoir mettre l'expédition
        $carteExpe = $this->expeditionHandler->calculExpedition($ville, $traceExpedition->getCoordonnee());
        
        // On va rechercher le x minimum et maximum et le y minimum et maximum afin de garder un carré autour du tracé
        $xMin = PHP_INT_MAX;
        $xMax = PHP_INT_MIN;
        $yMin = PHP_INT_MAX;
        $yMax = PHP_INT_MIN;
        
        foreach ($carteExpe as $y => $row) {
            foreach ($row as $x => $value) {
                if ($value !== null) {
                    $xMin = min($xMin, $x);
                    $xMax = max($xMax, $x);
                    $yMin = min($yMin, $y);
                    $yMax = max($yMax, $y);
                }
            }
        }
        
        // On retire ou ajoute 1 pour chacun afin d'avoir une petite marge
        $xMin = max(0, $xMin - 1);
        $xMax = min($ville->getWeight() - 1, $xMax + 1);
        $yMin = max(0, $yMin - 1);
        $yMax = min($ville->getHeight() - 1, $yMax + 1);
        
        
        // On peut maintenant prendre les zones qui nous intéresse
        $zones = [];
        /** @var ZoneMap[] $zonesVilles */
        $zonesVilles = $ville->getZone()->toArray();
        foreach ($zonesVilles as $zone) {
            if ($zone->getX() >= $xMin && $zone->getX() <= $xMax && $zone->getY() >= $yMin && $zone->getY() <= $yMax) {
                $zones[$zone->getY() * 100 + $zone->getX()] = $zone;
            }
        }
        
        // on va récupérer la partie de la carte qui nous intéresse pour l'expédition sur un carré
        $carteExpeFinal = [];
        for ($y = $yMin; $y <= $yMax; $y++) {
            $carteExpeFinal[$y] = [];
            for ($x = $xMin; $x <= $xMax; $x++) {
                $carteExpeFinal[$y][$x] = $carteExpe[$y][$x];
            }
        }
        
        return [
            'trace' => $carteExpeFinal,
            'zones' => $this->serializerService->serializeArray($zones, 'json', ['carte']),
            'x_min' => $xMin,
            'x_max' => $xMax,
            'y_min' => $yMin,
            'y_max' => $yMax,
            'ville' => $this->serializerService->serializeArray($ville, 'json', ['carte']),
            'user'  => $this->serializerService->serializeArray($user, 'json', ['carte_user']),
        ];
    }
    
    /**
     * @throws JsonException
     */
    public function getArchiveInscriptionExpedition(Ville $ville, int $jour = 0): array
    {
        // Récupération de l'outil d'expédition
        $outilsExpedition  = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $jour === 0 ? $ville->getJour() : $jour);
        $expeditionOuverte = $this->recuperationExpeditionOuverte($ville, $jour);
        $ouvriersListe     = $this->recuperationOuvriers($ville, $jour);
        
        return [
            'expeditions'    => $this->serializerService->serializeArray($expeditionOuverte, 'json', ['outils_expe', 'expe', 'general_res']),
            'direction_fao'  => $outilsExpedition->getFaoDirection(),
            'ouvriers'       => $this->serializerService->serializeArray($ouvriersListe, 'json', ['outils_expe', 'expe', 'general_res']),
            'preinscritExpe' => $this->serializerService->serializeArray($this->recuperationExpeditionnairePreinscrit($ville, $jour), 'json', ['citoyens', 'general_res']),
            'ouvert_ouvrier' => $outilsExpedition->isOuvertOuvrier(),
        ];
    }
    
    /**
     * @throws JsonException
     */
    public function getInscriptionExpedition(Ville $ville): array
    {
        // Récupération de l'outil d'expédition
        $outilsExpedition = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $ville->getJour());
        $user             = $this->userHandler->getCurrentUser();
        
        $expeditionOuverte   = $this->recuperationExpeditionOuverte($ville);
        $ouvriersListe       = $this->recuperationOuvriers($ville);
        $citoyensVivantListe = $this->recuperationCitoyensVivantVille($ville);
        
        return [
            'expeditions'    => $this->serializerService->serializeArray($expeditionOuverte, 'json', ['outils_expe', 'expe', 'general_res']),
            'direction_fao'  => $outilsExpedition->getFaoDirection(),
            'userOption'     => $this->serializerService->serializeArray($user, 'json', ['outils_expe', 'expe']),
            'ouvriers'       => $this->serializerService->serializeArray($ouvriersListe, 'json', ['outils_expe', 'expe', 'general_res']),
            'citoyens'       => $this->serializerService->serializeArray($citoyensVivantListe, 'json', ['citoyens', 'general_res']),
            'ouvert_ouvrier' => $outilsExpedition->isOuvertOuvrier(),
            'liste'          => $this->recuperationTypologieInscriptionExpedition(),
            'preinscritExpe' => $this->serializerService->serializeArray($this->recuperationExpeditionnairePreinscrit($ville), 'json', ['citoyens', 'general_res']),
            'userDispo'      => $this->serializerService->serializeArray($user, 'json', ['outils_expe', 'expe']),
        ];
    }
    
    /**
     * @return Citoyens[]
     */
    public function recuperationCitoyensVivantVille(Ville $ville): array
    {
        $listCitoyens = $this->em->getRepository(Citoyens::class)->findBy(['ville' => $ville, 'mort' => false]);
        
        usort($listCitoyens, function (Citoyens $a, Citoyens $b) {
            return ucfirst($a->getCitoyen()->getPseudo()) <=> ucfirst($b->getCitoyen()->getPseudo());
        });
        
        return $listCitoyens;
    }
    
    /**
     * @return Expedition[]
     */
    public function recuperationExpeditionOuverte(Ville $ville, int $jour = 0): array
    {
        // Récupération de l'outil d'expédition
        $outilsExpedition = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $jour === 0 ? $ville->getJour() : $jour);
        
        // Récupération des expéditions ouvertes
        $expeditions = [];
        foreach ($outilsExpedition->getExpeditions() as $expedition) {
            // on regarde la première partie, en effet le statut ouvert est sur les parties
            if ($expedition->getExpeditionParts()?->first() && $expedition->getExpeditionParts()?->first()?->isOuverte() ?? false) {
                $expeditions[] = $expedition;
            }
        }
        
        return $expeditions;
    }
    
    /**
     * @return int[]
     */
    public function recuperationExpeditionnairePreinscrit(Ville $ville, int $jour = 0): array
    {
        // Récupération de l'outil d'expédition
        $outilsExpedition = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $jour === 0 ? $ville->getJour() : $jour);
        
        // Récupération des expéditions ouvertes
        $idPreinscrit = [];
        foreach ($outilsExpedition->getExpeditions() as $expedition) {
            // on regarde la première partie, en effet le statut ouvert est sur les parties
            if ($expedition->getExpeditionParts()->first() !== null || $expedition->getExpeditionParts()->first()) {
                foreach ($expedition->getExpeditionParts()->first()->getExpeditionnaires() as $expeditionnaire) {
                    if ($expeditionnaire->isPreinscrit()) {
                        $idPreinscrit[$expeditionnaire->getCitoyen()->getCitoyen()->getId()] = $expeditionnaire->getCitoyen();
                    }
                }
            }
        }
        
        return $idPreinscrit;
    }
    
    /**
     * @return Ouvriers[]
     */
    public function recuperationOuvriers(Ville $ville, int $jour = 0): array
    {
        // Récupération de l'outil d'expédition
        $outilsExpedition = $this->expeditionHandler->recuperationOutilsExpeditionJour($ville, $jour === 0 ? $ville->getJour() : $jour);
        
        return $outilsExpedition->getOuvriers()->toArray();
    }
    
    /**
     * @throws JsonException
     */
    public function recuperationTypologieCreneauExpedition(): array
    {
        // Récupération des créneaux horaires
        $listCreneau = $this->em->getRepository(CreneauHorraire::class)->findBy(['typologie' => 2]);
        
        // Récupération des dispos pour les inscriptions
        $listDispo = $this->em->getRepository(TypeDispo::class)->findBy(['typologie' => 2]);
        
        $arrayExpediton['creneau'] = $this->serializerService->serializeArray($listCreneau, 'json', ['groups' => 'outils_expe']);
        $arrayExpediton['dispo']   = $this->serializerService->serializeArray($listDispo, 'json', ['groups' => 'outils_expe']);
        
        return $arrayExpediton;
    }
    
    /**
     * @throws JsonException
     */
    public function recuperationTypologieInscriptionExpedition(): array
    {
        $arrayExpediton = $this->expeditionHandler->recuperationListeTypologieTraduite();
        
        $arraySpecifiqueExpediton = $this->recuperationTypologieCreneauExpedition();
        
        $arrayExpediton['creneau'] = $arraySpecifiqueExpediton['creneau'];
        $arrayExpediton['dispo']   = $arraySpecifiqueExpediton['dispo'];
        
        
        return $arrayExpediton;
    }
    
    /**
     * @throws GestHordesException
     */
    public function recupererExpeditionPart(Ville $ville, string $traceId): ExpeditionPart
    {
        // Récupération du tracé
        $traceExpedition = $this->em->getRepository(TraceExpedition::class)->findOneBy(['id' => $traceId, 'ville' => $ville]);
        
        if ($traceExpedition === null) {
            throw new GestHordesException('Trace non trouvé id : ' . $traceId . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // Récupération de l'expedition associé
        $expeditionPart = $this->em->getRepository(ExpeditionPart::class)->findOneBy(['trace' => $traceExpedition]);
        
        if ($expeditionPart === null) {
            throw new GestHordesException('Expedition part non trouvé id : ' . $traceId . '|' . __FILE__ . '|' . __LINE__);
        }
        
        return $expeditionPart;
    }
    
    /**
     * @throws GestHordesException
     */
    public function sauvegardeCommentaire(Ville $ville, OutilsExpedition $outilsExpedition, SauvegardeCommentaire $sauvegardeCommentaire): void
    {
        // On va faire le controle de l'existance de l'expedition surlaquelle on veut s'inscrire
        $expeditionPart = $this->em->getRepository(ExpeditionPart::class)->findOneBy(['id' => $sauvegardeCommentaire->getExpeditionPartId()]);
        
        if ($expeditionPart === null) {
            throw new GestHordesException('Expedition part non trouvé id : ' . $sauvegardeCommentaire->getExpeditionPartId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On récupère l'expeditionnaire surlequel on veut faire des modifications et on vérifie qu'il s'agit bien du même utilisateur
        $expeditionnaire = $this->em->getRepository(Expeditionnaire::class)->findOneBy(['id' => $sauvegardeCommentaire->getExpeditionnaireId()]);
        
        if ($expeditionnaire === null) {
            throw new GestHordesException('Expeditionnaire non trouvé id : ' . $sauvegardeCommentaire->getExpeditionnaireId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On contrôle que le user est bien le même que celui de l'expéditionnaire
        if ($expeditionnaire->getCitoyen()->getCitoyen()->getId() !== $this->userHandler->getCurrentUser()->getId()) {
            throw new GestHordesException('Vous n\'êtes pas autorisé à modifier ce commentaire', GestHordesException::SHOW_MESSAGE_INFO);
        }
        
        // On va maintenant modifier le commentaire, et on limite à 255 caractères
        $expeditionnaire->setCommentaire(substr($sauvegardeCommentaire->getCommentaire(), 0, 255));
        
        // On sauvegarde
        $this->em->persist($expeditionnaire);
        $this->em->flush();
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function sauvegarderInscriptionExpedition(OutilsExpedition $outilsExpedition, InscriptionExpeditionSauvegarde $sauvegarde): void
    {
        // On va faire le controle de l'existance de l'expedition surlaquelle on veut s'inscrire
        $expedition = $this->em->getRepository(Expedition::class)->findOneBy(['id' => $sauvegarde->getExpedition()->getExpeditionId()]);
        
        if ($expedition === null) {
            throw new GestHordesException('Expedition non trouvé id : ' . $sauvegarde->getExpedition()->getExpeditionId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On récupère l'expeditionnaire surlequel on veut faire des modifications
        $expeditionnaire = $this->em->getRepository(Expeditionnaire::class)->findOneBy(['id' => $sauvegarde->getExpedition()->getExpeditionnaireId()]);
        
        if ($expeditionnaire === null) {
            throw new GestHordesException('Expeditionnaire non trouvé id : ' . $sauvegarde->getExpedition()->getExpeditionnaireId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // Maintenant qu'on a vérié que tout est bon, on va rechercher l'index de l'expeditionnaire dans la liste des expéditions afin de pouvoir modifier l'ensemble des parties
        $position = 0;
        foreach ($expedition->getExpeditionParts() as $expeditionPart) {
            foreach ($expeditionPart->getExpeditionnaires() as $expeditionnairePart) {
                if ($expeditionnairePart->getId() === $expeditionnaire->getId()) {
                    $position = $expeditionnairePart->getPosition();
                    break 2;
                }
            }
        }
        
        // On va maintenant modifier l'ensemble des parties situé à l'index trouvé
        foreach ($expedition->getExpeditionParts() as $expeditionPart) {
            // On va rechercher l'expeditionnaire à la position trouvé
            $expeditionnaire = $expeditionPart->getExpeditionnairePosition($position);
            if ($expeditionnaire === null) {
                throw new GestHordesException('Expeditionnaire non trouvé dans la partie d\'expedition id : ' . $expeditionPart->getId() . '|' . __FILE__ . '|' . __LINE__);
            }
            // Si on modifie la soif
            if ($sauvegarde->getModification()->getSoif() !== null) {
                $expeditionPart->getExpeditionnairePosition($position)->setSoif($sauvegarde->getModification()->getSoif());
            }
            
            // Si on modifie le pseudo
            if ($sauvegarde->getModification()->getCitoyen() !== null) {
                // Plusieurs cas s'offre à nous, on va d'abord vérifie que personne ne s'est inscrit entre temps
                if ($expeditionPart->getExpeditionnairePosition($position)->getCitoyen() !== null && $sauvegarde->getModification()->getPlaceVide()) {
                    throw new GestHordesException($this->translator->trans("Impossible de s'inscrire ici, quelqu'un s'est déjà inscrit", [], 'hotel'), GestHordesException::SHOW_MESSAGE_INFO);
                }
                
                // Dans ce cas, on effectue la modification, on récupère le citoyen tout propre pour faire la maj
                $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['citoyen' => $sauvegarde->getModification()->getCitoyen()->getCitoyen()->getId(), 'ville' => $outilsExpedition->getOutils()->getVille()]);
                if ($citoyen === null) {
                    throw new GestHordesException('Citoyen non trouvé id : ' . $sauvegarde->getModification()->getCitoyen()->getCitoyen()->getId() . '|' . __FILE__ . '|' . __LINE__);
                }
                $expeditionnaire->setCitoyen($citoyen);
                // Si le métier n'était pas figé, on met à jour le métier
                if (!$expeditionPart->getExpeditionnairePosition($position)->isJobFige()) {
                    $jobBdd = $this->em->getRepository(JobPrototype::class)->findOneBy(['id' => $citoyen->getJob()->getId()]);
                    $expeditionnaire->setJob($jobBdd);
                }
            }
            
            // Si on modifie la dispo
            if ($sauvegarde->getModification()->getDispo() !== null) {
                // On recherche le creneau horraire sur lequel on modifie la dispo
                foreach ($expeditionnaire->getDispo() as $dispo) {
                    if ($dispo->getCreneau()->getId() === $sauvegarde->getModification()->getDispo()->getCreneau()->getId()) {
                        if ($sauvegarde->getModification()->getDispo()->getDispo() === null) {
                            $dispo->setDispo(null);
                        } else {
                            // on récupère la dispo choisis en bdd
                            $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $sauvegarde->getModification()->getDispo()->getDispo()->getId()]);
                            if ($dispoBdd === null) {
                                throw new GestHordesException('Dispo non trouvé id : ' . $sauvegarde->getModification()->getDispo()->getDispo()->getId() . '|' . __FILE__ . '|' . __LINE__);
                            }
                            $dispo->setDispo($dispoBdd);
                        }
                    }
                }
            }
            
            // Si on utilise une dispo rapide, on met à jour l'ensemble des dispos
            if ($sauvegarde->getModification()->getDispos() !== null) {
                
                // On va ranger les dispo dans un tableau pour chaque creneau
                $dispoArray = [];
                foreach ($sauvegarde->getModification()->getDispos()->getCreneau() as $dispoUser) {
                    $dispoArray[$dispoUser->getCreneau()->getId()] = $dispoUser->getDispo()->getId();
                }
                
                // On va mettre à jour l'ensemble des dispos
                foreach ($expeditionnaire->getDispo() as $dispo) {
                    $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispoArray[$dispo->getCreneau()->getId()]]);
                    if ($dispoBdd === null) {
                        throw new GestHordesException('Dispo non trouvé id : ' . $dispoArray[$dispo->getCreneau()->getId()] . '|' . __FILE__ . '|' . __LINE__);
                    }
                    $dispo->setDispo($dispoBdd);
                }
                
                $expeditionnaire->setDispoRapide($sauvegarde->getModification()->getDispos()->getId());
            }
            
            // si les 4 modifications sont nulles, alors, on met à null les différentes informations de l'expeditionnaire
            if ($sauvegarde->getModification()->getSoif() === null && $sauvegarde->getModification()->getCitoyen() === null && $sauvegarde->getModification()->getDispo() === null && $sauvegarde->getModification()->getDispos() === null) {
                $expeditionnaire->setCitoyen(null);
                $expeditionnaire->setSoif(null);
                $expeditionnaire->setDispoRapide(null);
                if (!$expeditionPart->getExpeditionnairePosition($position)->isJobFige()) {
                    $expeditionnaire->setJob(null);
                }
                foreach ($expeditionnaire->getDispo() as $dispo) {
                    $dispo->setDispo(null);
                }
            }
            
            $this->em->persist($expeditionnaire);
            
        }
        $this->em->flush();
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function sauvegarderInscriptionExpeditionOuvrier(OutilsExpedition $outilsExpedition, InscriptionExpeditionSauvegarde $sauvegarde): void
    {
        // On controle de l'existance de la ligne ouvrier
        $ouvrier = $this->em->getRepository(Ouvriers::class)->findOneBy(['id' => $sauvegarde->getOuvrier()->getOuvrierId()]);
        
        if ($ouvrier === null) {
            throw new GestHordesException('Ouvrier non trouvé id : ' . $sauvegarde->getOuvrier()->getOuvrierId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On vérifie que l'ouvrier appartient bien à l'outils expédition du jour
        if ($ouvrier->getOutilsExpedition()->getId() !== $outilsExpedition->getId()) {
            throw new GestHordesException('Ouvrier non trouvé dans l\'outils d\'expédition id : ' . $outilsExpedition->getId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On va maintenant modifier les différentes zones de l'ouvrier
        if ($sauvegarde->getModification()->getSoif() !== null) {
            $ouvrier->setSoif($sauvegarde->getModification()->getSoif());
        }
        
        if ($sauvegarde->getModification()->getCitoyen() !== null) {
            $citoyen = $this->em->getRepository(Citoyens::class)->findOneBy(['citoyen' => $sauvegarde->getModification()->getCitoyen()->getCitoyen()->getId(), 'ville' => $outilsExpedition->getOutils()->getVille()]);
            if ($citoyen === null) {
                throw new GestHordesException('Citoyen non trouvé id : ' . $sauvegarde->getModification()->getCitoyen()->getCitoyen()->getId() . '|' . __FILE__ . '|' . __LINE__);
            }
            if ($sauvegarde->getModification()->getPlaceVide() && $ouvrier->getCitoyen() !== null) {
                throw new GestHordesException($this->translator->trans("Impossible de s'inscrire ici, quelqu'un s'est déjà inscrit", [], 'hotel'), GestHordesException::SHOW_MESSAGE_INFO);
            }
            $ouvrier->setCitoyen($citoyen);
            // Si le métier n'était pas figé, on met à jour le métier
            if (!$ouvrier->isJobFige()) {
                $jobBdd = $this->em->getRepository(JobPrototype::class)->findOneBy(['id' => $citoyen->getJob()->getId()]);
                $ouvrier->setJob($jobBdd);
            }
        }
        
        if ($sauvegarde->getModification()->getDispo() !== null) {
            foreach ($ouvrier->getDispo() as $dispo) {
                if ($dispo->getCreneau()->getId() === $sauvegarde->getModification()->getDispo()->getCreneau()->getId()) {
                    if ($sauvegarde->getModification()->getDispo()->getDispo() === null) {
                        $dispo->setDispo(null);
                    } else {
                        $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $sauvegarde->getModification()->getDispo()->getDispo()->getId()]);
                        if ($dispoBdd === null) {
                            throw new GestHordesException('Dispo non trouvé id : ' . $sauvegarde->getModification()->getDispo()->getDispo()->getId() . '|' . __FILE__ . '|' . __LINE__);
                        }
                        $dispo->setDispo($dispoBdd);
                    }
                }
            }
        }
        
        if ($sauvegarde->getModification()->getDispos() !== null) {
            // On va ranger les dispo dans un tableau pour chaque creneau
            $dispoArray = [];
            foreach ($sauvegarde->getModification()->getDispos()->getCreneau() as $dispoUser) {
                $dispoArray[$dispoUser->getCreneau()->getId()] = $dispoUser->getDispo()->getId();
            }
            
            // On va mettre à jour l'ensemble des dispos
            foreach ($ouvrier->getDispo() as $dispo) {
                $dispoBdd = $this->em->getRepository(TypeDispo::class)->findOneBy(['id' => $dispoArray[$dispo->getCreneau()->getId()]]);
                if ($dispoBdd === null) {
                    throw new GestHordesException('Dispo non trouvé id : ' . $dispoArray[$dispo->getCreneau()->getId()] . '|' . __FILE__ . '|' . __LINE__);
                }
                $dispo->setDispo($dispoBdd);
            }
            
            $ouvrier->setDispoRapide($sauvegarde->getModification()->getDispos()->getId());
        }
        
        // Si les 3 zones de modification arrive à null
        if ($sauvegarde->getModification()->getSoif() === null && $sauvegarde->getModification()->getCitoyen() === null && $sauvegarde->getModification()->getDispo() === null && $sauvegarde->getModification()->getDispos() === null) {
            $ouvrier->setCitoyen(null)
                    ->setSoif(null)
                    ->setDispoRapide(null);
            if (!$ouvrier->isJobFige()) {
                $ouvrier->setJob(null);
            }
            foreach ($ouvrier->getDispo() as $dispo) {
                $dispo->setDispo(null);
            }
        }
        
        $this->em->persist($ouvrier);
        $this->em->flush();
        
    }
    
    /**
     * @throws GestHordesException
     */
    public function validationConsigne(Ville $ville, OutilsExpedition $outilsExpedition, ConsigneValidation $consigneValidation): void
    {
        // On va faire le controle de l'existance de l'expedition surlaquelle on veut s'inscrire
        $expeditionPart = $this->em->getRepository(ExpeditionPart::class)->findOneBy(['id' => $consigneValidation->getExpeditionPartId()]);
        
        if ($expeditionPart === null) {
            throw new GestHordesException('Expedition part non trouvé id : ' . $consigneValidation->getExpeditionPartId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On récupère la consigne surlequel on veut faire des modifications
        $consigne = $this->em->getRepository(ConsigneExpedition::class)->findOneBy(['id' => $consigneValidation->getConsigneId()]);
        
        if ($consigne === null) {
            throw new GestHordesException('Consigne non trouvé id : ' . $consigneValidation->getConsigneId() . '|' . __FILE__ . '|' . __LINE__);
        }
        
        // On va vérifier que l'utilisateur est inscrit dans l'expédition
        $userPresent = false;
        foreach ($expeditionPart->getExpeditionnaires() as $expeditionnaire) {
            if ($expeditionnaire?->getCitoyen()?->getCitoyen()?->getId() === $this->userHandler->getCurrentUser()->getId()) {
                $userPresent = true;
                break;
            }
        }
        
        if (!$userPresent) {
            throw new GestHordesException('Vous n\'êtes pas inscrit dans cette expédition', GestHordesException::SHOW_MESSAGE_INFO);
        }
        
        if ($consigneValidation->getValide() === null) {
            throw new GestHordesException('Valeur de validation non renseignée');
        }
        
        // On va maintenant modifier la consigne
        $consigne->setFait($consigneValidation->getValide());
        $this->em->persist($consigne);
        $this->em->flush();
        
    }
    
}