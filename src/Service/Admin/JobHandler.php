<?php


namespace App\Service\Admin;


use App\Entity\JobPrototype;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class JobHandler
{
    
    public function __construct(protected EntityManagerInterface $em, protected LoggerInterface $logger)
    {
    }
    
    public function controleExistanceJob(int $idJob): bool
    {
        if ($idJob === 0) {
            return true;
        }
        return $this->em->getRepository(JobPrototype::class)->findOneBy(['id' => $idJob]) !== null;
    }
    
    
}