<?php

namespace App\Service;

use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\ItemPrototype;
use App\Entity\Ville;
use App\Service\Utils\MediaService;
use App\Structures\Utils\Position;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Component\Asset\Packages;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

class GestHordesHandler
{
    
    public const NoError                            = 0;
    public const ErrorAvatarBackendUnavailable      = 1;
    public const ErrorAvatarTooLarge                = 2;
    public const ErrorAvatarFormatUnsupported       = 3;
    public const ErrorAvatarImageBroken             = 4;
    public const ErrorAvatarResolutionUnacceptable  = 5;
    public const ErrorAvatarProcessingFailed        = 6;
    public const ErrorAvatarInsufficientCompression = 7;
    
    public const ImageProcessingForceImagick   = 0;
    public const ImageProcessingPreferImagick  = 1;
    public const ImageProcessingDisableImagick = 2;
    
    
    public function __construct(protected EntityManagerInterface $em, protected Packages $asset,
                                protected TranslatorInterface    $translator, protected MediaService $media)
    {
    }
    
    public function asset(): Packages
    {
        return $this->asset;
    }
    
    public function calculKM(Ville $ville, ?int $x, ?int $y): int
    {
        $position = new Position($ville, $x, $y);
        
        $position->calculPositionRelatif();
        
        return (int)round(sqrt($position->getXRel() ** 2 + $position->getYRel() ** 2));
    }
    
    public function calculPointSaison(?Ville $ville): int
    {
        
        if ($ville === null) {
            return 0;
        }
        
        $tabMortByDay = [];
        
        foreach ($ville->getCitoyens() as $citoyen) {
            if ($citoyen->getMort()) {
                if (isset($tabMortByDay[$citoyen->getDeathDay()])) {
                    $tabMortByDay[$citoyen->getDeathDay()]++;
                } else {
                    $tabMortByDay[$citoyen->getDeathDay()] = 1;
                }
            }
        }
        
        // calcul des points par jour
        $nbrMort         = 0;
        $nbrPoint        = 0;
        $nbrCitoyensBase = $ville->getCitoyens()->count();
        if (isset($tabMortByDay[0])) {
            $nbrMort = $tabMortByDay[0];
        }
        for ($i = 1; $i <= $ville->getJour(); $i++) {
            
            $nbrPoint += $nbrCitoyensBase - $nbrMort;
            $nbrMort  += $tabMortByDay[$i] ?? 0;
        }
        
        return $nbrPoint + $ville->getBonus();
        
    }
    
    public function chantierConstruitJour(Ville $ville, int $id_chantierPrototype, int $jour): bool
    {
        return ($this->em->getRepository(Chantiers::class)
                         ->chantierConstruitJour($ville, $id_chantierPrototype, $jour) !== null);
    }
    
    public function chantierConstruitOrPrevu(Ville $ville, ChantierPrototype $chantierPrototype): bool
    {
        return ($this->em->getRepository(Chantiers::class)
                         ->chantierConstruit($ville, $chantierPrototype->getId()) !== null);
    }
    
    /**
     * @throws GuzzleException
     */
    #[Deprecated(reason: 'Utiliser le service de Discord DiscordService')]
    public function generateMessageDiscord(string $message): void
    {
        $apiUrl = "https://discord.com/api/v10/channels/{$_ENV['ID_LOG']}/messages";
        
        // Préparez les données pour la requête POST
        $data = ['content' => substr($message, 0, 2000)];
        
        // Utilisez Guzzle pour envoyer le message à Discord via le webhook
        $client = new Client();
        $client->post($apiUrl, [
            'headers' => [
                'Authorization' => "Bot {$_ENV['TOKEN_BOT']}",
                'Content-Type'  => 'application/json',
            ],
            'json'    => $data,
        ]);
    }
    
    public function generateToken(): string
    {
        return base_convert(hash('sha256', time() . random_int(0, mt_getrandmax())), 16, 36);
    }
    
    public function getCoordNorm(Ville $ville, ?int $x, ?int $y): string
    {
        $position = new Position($ville, $x, $y);
        $position->calculPositionRelatif();
        
        return "({$position->getXRel()}/{$position->getYRel()})";
        
    }
    
    public function getLibCoord(Ville $ville, ?int $x, ?int $y, bool $dehors): string
    {
        $position = new Position($ville, $x, $y);
        
        $position->calculPositionRelatif();
        
        if ($ville->isDevast() || $position->getYRel() === null || $position->getXRel() === null) {
            return "<span class=\"home infoBulle\"><span class=\"info\">(??/??)</span>{$this->svgImg('r_explor')}</span>";
        } elseif (!$dehors) {
            return "<span class=\"home infoBulle\"><span class=\"info\">{$this->translator->trans("En ville", [], 'ville')}</span>{$this->svgImg('h_home')}</span>";
        } elseif ($dehors && ($position->getXRel() === 0 && $position->getYRel() === 0)) {
            return "<span class=\"home infoBulle\"><span class=\"info\">{$this->translator->trans("Aux portes", [], 'ville')}</span>{$this->svgImg('h_door')}</span>";
        } else {
            return "<span class=\"home infoBulle\"><span class=\"info\">({$position->getXRel()}/{$position->getYRel()})</span>{$this->svgImg('r_explor')}</span>";
        }
        
    }
    
    public function getPrivateKey(): bool|string
    {
        return file_get_contents('../private.key');
    }
    
    public function getPublicKey(): bool|string
    {
        return file_get_contents('../public.key');
    }
    
    #[Deprecated(reason: 'Utiliser le service de sérialisation SerializerService')]
    public function getSerializer(): Serializer
    {
        $encoder              = new JsonEncoder();
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $nameConverter        = new CamelCaseToSnakeCaseNameConverter();
        
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                if ($object instanceof ChantierPrototype) {
                    return $object->getId();
                } elseif ($object instanceof ItemPrototype) {
                    return $object->getId();
                } else {
                    return $object->getId();
                }
            },
        ];
        
        $normaliser = new ObjectNormalizer(
            $classMetadataFactory,
            $nameConverter,
            null,
            new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]),
            null,
            null,
            $defaultContext,
        );
        
        return new Serializer([new DateTimeNormalizer(), $normaliser, new  GetSetMethodNormalizer(),
                               new ArrayDenormalizer()], [$encoder]);
    }
    
    public function regenerationImage(&$payload, int $imagick_setting = self::ImageProcessingForceImagick,
                                      string $ext = null, int $x = 750, int $y = 300): int
    {
        if (strlen((string)$payload) > 8_192_000) {
            return self::ErrorAvatarTooLarge;
        }
        
        $e = $imagick_setting === self::ImageProcessingDisableImagick
            ? MediaService::ErrorBackendMissing
            : $this->media->resizeImage($payload, function (int &$w, int &$h, bool &$fit): bool {
                if ($w / $h < 0.1 || $h / $w < 0.1 || $h < 16 || $w < 16) {
                    return false;
                }
                
                if ($w > 750 || $w < 500) {
                    $w = min(750, max(500, $w));
                }
                if ($h > 300 || $h < 100) {
                    $h = min(300, max(100, $h));
                }
                
                return $fit = true;
            },                          $w_final, $h_final, $processed_format);
        
        switch ($e) {
            case MediaService::ErrorNone:
                break;
            case MediaService::ErrorBackendMissing:
                if ($imagick_setting === self::ImageProcessingForceImagick) {
                    return self::ErrorAvatarBackendUnavailable;
                }
                $processed_format = $ext;
                $w_final          = $x ?: 750;
                $h_final          = $y ?: 300;
                break;
            case MediaService::ErrorInputBroken:
                return self::ErrorAvatarImageBroken;
            case MediaService::ErrorInputUnsupported:
                return self::ErrorAvatarFormatUnsupported;
            case MediaService::ErrorDimensionMismatch:
                return self::ErrorAvatarResolutionUnacceptable;
            case MediaService::ErrorProcessingFailed:
            default:
                return self:: ErrorAvatarProcessingFailed;
        }
        
        // Storage limit: 1MB
        if (strlen((string)$payload) > 8_192_000) {
            return self::ErrorAvatarInsufficientCompression;
        }
        
        return self::NoError;
    }
    
    public function svgImg(string $icon, string $class = 'itemHordes'): string
    {
        if ($class === 'itemHordes' || $class === 'titrePicto' || $class === 'item_home') {
            return "<svg class=\"{$class}\"><use xlink:href=\"{$this->asset()->getUrl('build/img/sprite.svg')}#{$icon}\"></svg>";
        } else {
            return "<svg class=\"icon_{$class}\"><use xlink:href=\"{$this->asset()->getUrl('build/img/sprite_'.$class.'.svg')}#{$icon}\"></svg>";
        }
        
    }
}