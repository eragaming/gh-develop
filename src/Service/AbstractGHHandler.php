<?php

namespace App\Service;

use App\Service\Generality\TranslateHandler;
use App\Service\Utils\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractGHHandler
{
    public function __construct(protected EntityManagerInterface $em,
                                protected LoggerInterface        $logger,
                                protected TranslatorInterface    $translator,
                                protected TranslateHandler       $translateHandler,
                                protected UserHandler            $userHandler,
                                protected GestHordesHandler      $gh,
                                protected SerializerService      $serializerService,
    )
    {
    }
}