<?php

namespace App\Form\Admin;

use App\Entity\ItemPrototype;
use App\Entity\RessourceChantier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RessourceType extends AbstractType
{
    
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $listObjet = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
        
        $builder->add(
            'item',
            ChoiceType::class,
            [
                'choices'      => $listObjet,
                'choice_value' => 'id',
                'choice_label' => fn(?ItemPrototype $itemPrototype) => $itemPrototype ? $itemPrototype->getNom() : '',
                'placeholder'  => 'Aucun',
                'label'        => 'Item : ',
                'required'     => false,
            ],
        )
                ->add(
                    'nombre',
                    IntegerType::class,
                    [
                        'label' => 'Nombre : ',
                    ],
                );
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => RessourceChantier::class]);
    }
    
}