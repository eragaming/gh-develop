<?php

namespace App\Form\Admin;

use App\Entity\BonusUpChantier;
use App\Entity\UpChantierPrototype;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Traversable;

class BonusUpChantierType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'level',
                IntegerType::class,
                [
                    'label'    => 'Level : ',
                    'row_attr' => ['class' => 'levelEvo_proto'],
                ],
            )
            ->add(
                'bonusUps1',
                UpChantiersType::class, [
                    'label'      => 'Bonus n°1 :',
                    'label_attr' => ['class' => 'labelBonusEvo_proto'],
                    'row_attr'   => ['class' => 'bonusEvo_proto'],
                    'attr'       => ['class' => 'bonusEvo_form_proto'],
                ],
            )
            ->add(
                'bonusUps2',
                UpChantiersType::class, [
                    'required'   => false,
                    'label'      => 'Bonus n°2 (optionnel) :',
                    'label_attr' => ['class' => 'labelBonusEvo_proto'],
                    'row_attr'   => ['class' => 'bonusEvo_proto'],
                    'attr'       => ['class' => 'bonusEvo_form_proto'],
                ],
            );
        
        $builder->setDataMapper($this);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class' => BonusUpChantier::class,
                               ]);
    }
    
    public function mapDataToForms($viewData, Traversable $forms): void
    {
        if (null === $viewData) {
            return;
        }
        
        if (!$viewData instanceof BonusUpChantier) {
            throw new Exception\UnexpectedTypeException($viewData, BonusUpChantier::class);
        }
        
        /**
         * @var FormInterface[] $forms
         */
        $forms = iterator_to_array($forms);
        
        $forms['level']->setData($viewData->getLevel());
        $forms['bonusUps1']->setData($viewData->getBonusUps()->first());
        if ($viewData->getBonusUps()->count() > 1) {
            $forms['bonusUps2']->setData($viewData->getBonusUps()->last());
        } else {
            $forms['bonusUps2']->setData(new UpChantierPrototype());
        }
    }
    
    public function mapFormsToData(Traversable $forms, &$viewData): void
    {
        /**
         * @var FormInterface[] $forms
         */
        $forms = iterator_to_array($forms);
        
        $viewData = new BonusUpChantier();
        $viewData->setLevel((int)$forms['level']->getData());
        $viewData->addBonusUp($forms['bonusUps1']->getData());
        if ($forms['bonusUps2']->getData() !== null) {
            $viewData->addBonusUp($forms['bonusUps2']->getData());
        }
    }
}
