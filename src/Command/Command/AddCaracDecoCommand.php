<?php

namespace App\Command\Command;

use App\Entity\CaracteristiquesItem;
use App\Entity\ItemPrototype;
use App\Entity\TypeCaracteristique;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:migration:git:add-carac-deco',
)]
class AddCaracDecoCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de reprendre la valeur de décoration dans les objets et de créer la caractéristique correspondante.')
             ->setHelp('Add defaut value.');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io        = new SymfonyStyle($input, $output);
        $items     = $this->em->getRepository(ItemPrototype::class)->findAll();
        $caracDeco = $this->em->getRepository(TypeCaracteristique::class)->findOneBy(['id' => 16]);
        
        $io->title('Ajout de données dans les caractéristiques des objets');
        $io->text('Ajout en cours...');
        $io->progressStart(count($items));
        
        foreach ($items as $item) {
            if ($item->getDeco() !== 0) {
                $exist = false;
                foreach ($item->getCaracteristiques() as $caracItem) {
                    if ($caracItem->getTypeCarac()->getId() === $caracDeco->getId()) {
                        $caracItem->setValue($item->getDeco());
                        $exist = true;
                        break;
                    }
                }
                if (!$exist) {
                    $caracItem = new CaracteristiquesItem();
                    $caracItem->setTypeCarac($caracDeco)->setValue($item->getDeco());
                    $caracItem->setId($this->em->getRepository(CaracteristiquesItem::class)->getLastId() + 1);
                    
                    $this->em->persist($caracItem);
                    $this->em->flush();
                    $this->em->refresh($caracItem);
                    
                    $item->addCaracteristique($caracItem);
                }
            } else {
                foreach ($item->getCaracteristiques() as $caracItem) {
                    if ($caracItem->getTypeCarac()->getId() === $caracDeco->getId()) {
                        $item->removeCaracteristique($caracItem);
                        break;
                    }
                }
            }
            $this->em->persist($item);
            $io->progressAdvance();
        }
        
        $this->em->flush();
        $io->progressFinish();
        $io->success('Mise à jour terminé.');
        
        return Command::SUCCESS;
    }
    
}
