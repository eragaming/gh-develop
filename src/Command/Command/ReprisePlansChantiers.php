<?php

namespace App\Command\Command;

use App\Entity\AvancementChantier;
use App\Entity\ChantierPrototype;
use App\Entity\Chantiers;
use App\Entity\PlansChantier;
use App\Entity\Ville;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:migration:git:reprise-plans-chantiers',
)]
class ReprisePlansChantiers extends Command
{
    private const BATCH_SIZE = 100;
    
    public function __construct(private readonly EntityManagerInterface $em)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription("Permet de reprendre les plans de chantiers depuis la table chantiers et avancement_chantiers.");
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Ajout de données dans l\'entité PlansChantier');
        
        // Récupération de toutes les villes en une seule requête
        $villes = $this->em->createQueryBuilder()
                           ->select('v')
                           ->from(Ville::class, 'v')
                           ->getQuery()
                           ->getResult();
        
        $io->progressStart(count($villes));
        
        foreach ($villes as $ville) {
            // Traitement des chantiers par lots
            $chantiers      = $this->getChantierQuery($ville);
            $chantiersCount = 0;
            
            foreach (array_chunk($chantiers, self::BATCH_SIZE) as $batch) {
                $this->processBatch($batch);
                $chantiersCount += count($batch);
            }
            
//            if ($chantiersCount > 0) {
//                $io->text(sprintf('Ajout de %d plans de chantiers pour la ville %s', $chantiersCount, $ville->getNom()));
//            }
            
            // Traitement des avancements par lots
            $avancements      = $this->getAvancementQuery($ville);
            $avancementsCount = 0;
            
            foreach (array_chunk($avancements, self::BATCH_SIZE) as $batch) {
                $this->processBatch($batch);
                $avancementsCount += count($batch);
            }
            
//            if ($avancementsCount > 0) {
//                $io->text(sprintf('Ajout de %d plans de chantiers depuis l\'avancement pour la ville %s', $avancementsCount, $ville->getNom()));
//            }
            
            $io->progressAdvance();
            $this->em->clear(); // Libère la mémoire après chaque ville
        }
        
        $io->progressFinish();
        $io->success('Transfert terminé.');
        
        return Command::SUCCESS;
    }
    
    private function getAvancementQuery(Ville $ville): array
    {
        return $this->em->createQueryBuilder()
                        ->select('ac')
                        ->from(AvancementChantier::class, 'ac')
                        ->join(ChantierPrototype::class, 'cp', Join::WITH, 'cp.id = ac.chantier')
                        ->where('cp.plan > 0')
                        ->andWhere('ac.ville = :ville')
                        ->andWhere(
                            'NOT EXISTS (
                SELECT 1
                FROM App\Entity\PlansChantier pc
                WHERE pc.ville = ac.ville
                AND pc.chantier = ac.chantier
                )',
                        )
                        ->setParameter('ville', $ville)
                        ->getQuery()
                        ->getResult();
    }
    
    private function getChantierQuery(Ville $ville): array
    {
        return $this->em->createQueryBuilder()
                        ->select('c')
                        ->from(Chantiers::class, 'c')
                        ->join(ChantierPrototype::class, 'cp', Join::WITH, 'cp.id = c.chantier')
                        ->where('cp.plan > 0')
                        ->andWhere('c.ville = :ville')
                        ->andWhere(
                            'NOT EXISTS (
                SELECT 1
                FROM App\Entity\PlansChantier pc
                WHERE pc.ville = c.ville
                AND pc.chantier = c.chantier
                )',
                        )
                        ->setParameter('ville', $ville)
                        ->getQuery()
                        ->getResult();
    }
    
    private function processBatch(array $entities): void
    {
        foreach ($entities as $entity) {
            $plan = new PlansChantier();
            $plan->setChantier($entity->getChantier());
            $plan->setVille($entity->getVille());
            $this->em->persist($plan);
        }
        
        $this->em->flush();
        $this->em->clear(); // Libère la mémoire
    }
}