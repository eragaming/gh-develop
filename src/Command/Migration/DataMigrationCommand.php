<?php

declare(strict_types=1);

namespace App\Command\Migration;

use App\Service\CommandHelper;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:data_migration', description: 'Migration des datas git et prototype via la version')]
class DataMigrationCommand extends Command
{
    public function __construct(
        private readonly CommandHelper $commandHelper,
    )
    {
        
        parent::__construct();
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $io->title('Mise à jour des datas - Prototype / Git ');
            
            $version = file_get_contents('VERSION');
            if (!$this->commandHelper->capsule("app:update:prototype --update-version {$version}", $io)) {
                $io->error('Impossible de mettre à jour les fixtures.');
                return Command::FAILURE;
            }
            
            $io->text('Mise à jour des datas exceptionnels...');
            
            if (!$this->commandHelper->capsule("app:git_data_migration", $io)) {
                $io->error('Impossible de mettre à jour les datas.');
                return Command::FAILURE;
            }
            
            $io->success('Mise à jour des données effectuée.');
            
        } catch (Exception $e) {
            $io->error($e->getMessage());
        }
        
        return Command::SUCCESS;
    }
}
