<?php

declare(strict_types=1);

namespace App\Command\Migration;

use App\Entity\ContentsVersion;
use App\Entity\VersionsSite;
use App\Service\Utils\DiscordService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand(name: 'app:publication_annonce_changelog', description: 'Publication annonce changelog')]
class PublicationAnnonceChangelogCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface    $translator,
        private readonly DiscordService         $discordService,
    )
    {
        
        parent::__construct();
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        try {
            $roleIdChangelog = $_ENV['ID_CHANGELOG'];
            $versionFichier  = trim(file_get_contents('VERSION'));
            
            preg_match('/^v(\d+)\.(\d+)\.(\d+)(?:-(\w+))?/', $versionFichier, $matches);
            [$major, $minor, $patch, $label] = [$matches[1], $matches[2], $matches[3], $matches[4] ?? null];
            
            $version = $this->entityManager->getRepository(VersionsSite::class)->findOneBy([
                                                                                               'versionMajeur'     => $major,
                                                                                               'versionMineur'     => $minor,
                                                                                               'versionCorrective' => $patch,
                                                                                           ]);
            
            if (!$version) {
                $io->error("La version {$versionFichier} n'existe pas !");
                return Command::FAILURE;
            }
            
            $message    = ":flag_fr: :warning: ***La mise à jour Version $versionFichier a été correctement installée, voici le <@&{$roleIdChangelog}>*** :warning:  :flag_fr: \n";
            $message_gb = ":flag_gb: :warning: ***The Version $versionFichier update has been successfully installed, here's the changelog*** :warning: :flag_gb: \n";
            
            $types    = [
                ContentsVersion::FEATURE => 'Nouveauté(s)',
                ContentsVersion::FIX     => 'Correctif(s)',
                ContentsVersion::OTHER   => 'Autres',
            ];
            $types_gb = [
                ContentsVersion::FEATURE => 'New(s)',
                ContentsVersion::FIX     => 'Fix(es)',
                ContentsVersion::OTHER   => 'Others',
            ];
            
            foreach ($types as $type => $label) {
                $contents = $version->getContents()->filter(fn($content) => $content->getTypeContent() === $type);
                if ($contents && count($contents) > 0) {
                    $message    .= "**$label :** \n";
                    $message_gb .= "**" . $types_gb[$type] . " :** \n";
                    foreach ($contents as $content) {
                        $message    .= "- {$content->getContents()}\n";
                        $message_gb .= "- {$this->translator->trans($content->getContents(), [], 'version', 'en')}\n";
                    }
                }
            }
            
            $message    .= "\nA bientôt pour de nouvelle mise à jour !";
            $message_gb .= "\nSee you soon for a new update!";
            
            $this->discordService->generateMessageAnnonceDiscord($message . " \n " . $message_gb);
            
            $io->success("Annonce du changelog publié !");
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        
        return Command::SUCCESS;
    }
}
