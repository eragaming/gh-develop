<?php

namespace App\Command\Migration;

use App\Entity\ContentsVersion;
use App\Entity\NewsSite;
use App\Entity\User;
use App\Entity\VersionsSite;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:changelog', description: 'Publication du changelog sur le site')]
class ChangelogCommand extends Command
{
    
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    )
    {
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Permet de publier le changelog sur le site')
             ->setHelp('Changelog.');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $fichierChangelog = 'changelog.json';
            $contentChangelog = 'contentsChangelog.json';
            
            $fichierlu = json_decode(file_get_contents($fichierChangelog), true, 512, JSON_THROW_ON_ERROR) ?? [];
            $contenslu = json_decode(file_get_contents($contentChangelog), true, 512, JSON_THROW_ON_ERROR) ?? [];
            
            if (empty($fichierlu)) {
                $io->error('No versions found in changelog.');
                return Command::FAILURE;
            }
            
            $lastVersion = end($fichierlu);
            $versionKey  = key($fichierlu);
            
            $all_version_value = $this->entityManager->getRepository(VersionsSite::class)->findAll();
            $all_version_key   = array_map(fn($version) => 'v' . $version->getVersionMajeur() . '.' . $version->getVersionMineur() . '.' . $version->getVersionCorrective() . ($version->getTagName() ? '-' . $version->getTagName() . ($version->getNumTag() ? '.' . $version->getNumTag() : '') : ''), $all_version_value);
            $all_version       = array_combine($all_version_key, $all_version_value);
            
            $io->section("=== Mise à jour du changelog ===");
            $io->progressStart(1);
            
            $userEra = $this->entityManager->getRepository(User::class)->findOneBy(['idMyHordes' => $_ENV['ID_ERA']]);
            
            preg_match('/^(\d+)\.(\d+)\.(\d+)(?:-(\w+))?/', (string)$versionKey, $matches);
            [$major, $minor, $patch, $label] = [$matches[1], $matches[2], $matches[3], $matches[4] ?? null];
            
            $version_existante = $all_version['v' . $versionKey] ?? new VersionsSite();
            $version_existante->setVersionMajeur($major)->setVersionMineur($minor)->setVersionCorrective($patch)->setTagName($label);
            
            $dateVersion = isset($lastVersion['dateVersion']) ? DateTime::createFromFormat("d/m/Y H:i", $lastVersion['dateVersion']) : new DateTime('now');
            $version_existante->setDateVersion($dateVersion)->setTitre($lastVersion['titre'] ?? ('Mise à jour v' . $versionKey));
            
            foreach (['contents' => ContentsVersion::FEATURE, 'contentsFix' => ContentsVersion::FIX, 'contentsOther' => ContentsVersion::OTHER] as $key => $type) {
                if (isset($lastVersion[$key])) {
                    foreach ($lastVersion[$key] as $content) {
                        $contentObj = $version_existante->getContent($content) ?? new ContentsVersion();
                        $contentObj->setContents($contenslu[$content])->setIdRelatif($content)->setTypeContent($type);
                        $version_existante->addContent($contentObj);
                    }
                }
            }
            
            $this->entityManager->persist($version_existante);
            $this->entityManager->flush();
            $this->entityManager->refresh($version_existante);
            
            $tag     = $version_existante->getTagName() ? '-' . $version_existante->getTagName() . ($version_existante->getNumTag() ? '.' . $version_existante->getNumTag() : '') : '';
            $newsBdd = $this->entityManager->getRepository(NewsSite::class)->findOneBy(['titre' => "Mise à jour Version {$version_existante->getVersionMajeur()}.{$version_existante->getVersionMineur()}.{$version_existante->getVersionCorrective()}$tag"]);
            
            $news = $newsBdd ?? new NewsSite();
            $news->setTitre("Mise à jour Version {$version_existante->getVersionMajeur()}.{$version_existante->getVersionMineur()}.{$version_existante->getVersionCorrective()}$tag")
                 ->setVersion($version_existante)
                 ->setAuteur($userEra)
                 ->setDateAjout($version_existante->getDateVersion());
            
            $this->entityManager->persist($news);
            $this->entityManager->flush();
            
            $io->progressAdvance();
            $io->progressFinish();
            $io->success("Changelog publié !");
        } catch (JsonException $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        
        return Command::SUCCESS;
    }
}
