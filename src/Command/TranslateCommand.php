<?php


namespace App\Command;


use App\Service\CommandHelper;
use App\Service\ConfMaster;
use App\Service\TranslationConfigGlobal;
use App\Structures\Conf\GestHordesConf;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('app:translation:update')]
class TranslateCommand extends Command
{
    
    public function __construct(
        private readonly TranslationConfigGlobal $conf_trans,
        private readonly CommandHelper           $commandHelper,
        private readonly ConfMaster              $confMaster,
    )
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this
            ->setDescription('Updates translation files and adds missing translations')
            ->setHelp('Translation updater.')
            ->addArgument('lang', InputArgument::REQUIRED, 'Language to translate into.')
            ->addOption('file', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL,
                        'Limits translations to specific files', [])
            ->addOption('disable-php', null, InputOption::VALUE_NONE, 'Disables translation of PHP files')
            ->addOption('disable-db', null, InputOption::VALUE_NONE, 'Disables translation of database content')
            ->addOption('disable-twig', null, InputOption::VALUE_NONE, 'Disables translation of twig files')
            ->addOption('disable-config', null, InputOption::VALUE_NONE, 'Disables translation of config files');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Mise à jour des traductions');
        $lang = $input->getArgument('lang');
        
        $langs = ($lang === 'all') ? array_map(fn($item) => $item['code'],
            array_filter($this->confMaster->getGlobalConf()->get(GestHordesConf::CONF_LANGS),
                fn($item) => $item['generate'])) : [$lang];
        if (count($langs) === 1) {
            
            $this->conf_trans->setConfigured(true);
            if ($input->getOption('disable-db')) {
                $this->conf_trans->setDatabaseSearch(false);
            }
            if ($input->getOption('disable-php')) {
                $this->conf_trans->setPHPSearch(false);
            }
            if ($input->getOption('disable-twig')) {
                $this->conf_trans->setTwigSearch(false);
            }
            if ($input->getOption('disable-config')) {
                $this->conf_trans->setConfigSearch(false);
            }
            foreach ($input->getOption('file') as $file_name) {
                $this->conf_trans->addMatchedFileName($file_name);
            }
            
            $command = $this->getApplication()->find('translation:extract');
            
            $io->text("Traitement des traductions pour la langue : <info>{$lang}</info>...");
            
            $input = new ArrayInput([
                                        'locale'   => $lang,
                                        '--force'  => true,
                                        '--sort'   => 'asc',
                                        '--format' => 'yml',
                                        '--prefix' => '',
                                    ]);
            $input->setInteractive(false);
            try {
                $command->run($input, $output);
            } catch (Exception $e) {
                $io->error($e->getMessage());
                
                return 1;
            } catch (ExceptionInterface $e) {
                $io->error($e->getMessage());
                
                return 1;
            }
            
            if (!$this->commandHelper->capsule("app:translation:convert -j $lang", $io)) {
                return 1;
            }
            
            $io->success("Traductions pour la langue <info>{$lang}</info> mises à jour");
            
        } else {
            foreach ($langs as $current_lang) {
                $com = "app:translation:update $current_lang";
                if ($input->getOption('disable-db')) {
                    $com .= " --disable-db";
                }
                if ($input->getOption('disable-php')) {
                    $com .= " --disable-php";
                }
                if ($input->getOption('disable-twig')) {
                    $com .= " --disable-twig";
                }
                if ($input->getOption('disable-config')) {
                    $com .= " --disable-config";
                }
                foreach ($input->getOption('file') as $file_name) {
                    $com .= " --file $file_name";
                }
                
                $this->commandHelper->capsule($com, $io);
            }
        }
        
        
        return 0;
        
    }
    
}