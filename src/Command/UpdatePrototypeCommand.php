<?php


namespace App\Command;


use App\DataFixtures\BatFixtures;
use App\DataFixtures\ChantierFixtures;
use App\DataFixtures\ObjetFixtures;
use App\Entity\BatPrototype;
use App\Entity\BonusUpChantier;
use App\Entity\CaracteristiquesItem;
use App\Entity\CategoryObjet;
use App\Entity\ChantierPrototype;
use App\Entity\CleanUpCadaver;
use App\Entity\CreneauHorraire;
use App\Entity\ExpeditionType;
use App\Entity\HerosPrototype;
use App\Entity\HerosSkillLevel;
use App\Entity\HerosSkillPrototype;
use App\Entity\HerosSkillType;
use App\Entity\HomePrototype;
use App\Entity\ItemBatiment;
use App\Entity\ItemNeed;
use App\Entity\ItemProbability;
use App\Entity\ItemPrototype;
use App\Entity\JobPrototype;
use App\Entity\ListAssemblage;
use App\Entity\Menu;
use App\Entity\MenuElement;
use App\Entity\MenuPrototype;
use App\Entity\PictoPrototype;
use App\Entity\PictoTitrePrototype;
use App\Entity\RegroupementItemsDecharge;
use App\Entity\RessourceChantier;
use App\Entity\RessourceHome;
use App\Entity\RuineGameZonePrototype;
use App\Entity\TypeActionAssemblage;
use App\Entity\TypeCaracteristique;
use App\Entity\TypeDeath;
use App\Entity\TypeDecharge;
use App\Entity\TypeDispo;
use App\Entity\TypeObjet;
use App\Entity\UpChantierPrototype;
use App\Enum\LevelSkill;
use App\Enum\MenuType;
use App\Service\CommandHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand('app:update:prototype')]
class UpdatePrototypeCommand extends Command
{
    public function __construct(
        private readonly KernelInterface        $kernel,
        private readonly EntityManagerInterface $entityManager,
        private readonly CommandHelper          $commandHelper,
    )
    {
        
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this->setDescription('Update les prototypes de la base de données en fonction de la version du site')
             ->setHelp('Update prototypes')
             ->addOption('update-version', 'uv', InputOption::VALUE_REQUIRED, 'Version du site')
             ->addOption('maj_chantier', 'c', InputOption::VALUE_NONE, 'Mise à jour des chantiers')
             ->addOption('maj_items', 'i', InputOption::VALUE_NONE, 'Mise à jour des items')
             ->addOption('maj_homes', null, InputOption::VALUE_NONE, 'Mise à jour des habitations')
             ->addOption('maj_bats', 'b', InputOption::VALUE_NONE, 'Mise à jour des bâtiments')
             ->addOption('maj_job', 'job', InputOption::VALUE_NONE, 'Mise à jour des métiers')
             ->addOption('maj_regroupement', 'rg', InputOption::VALUE_NONE, 'Mise à jour des regroupements')
             ->addOption('maj_ruine_game_proto', 'rgp', InputOption::VALUE_NONE, 'Mise à jour des prototypes des pieces de la ruine')
             ->addOption('maj_type_expedition', 'te', InputOption::VALUE_NONE, 'Mise à jour des types d\'expédition')
             ->addOption('maj_creneau_horraire', 'ch', InputOption::VALUE_NONE, 'Mise à jour des créneaux horraires')
             ->addOption('maj_type_dispo', 'td', InputOption::VALUE_NONE, 'Mise à jour des types de disponibilité')
             ->addOption('maj_type_carac', 'tc', InputOption::VALUE_NONE, 'Mise à jour des types de carac des objets')
             ->addOption('maj_heros', 'he', InputOption::VALUE_NONE, 'Mise à jour des pouvoirs heros')
             ->addOption('maj_categories', 'co', InputOption::VALUE_NONE, 'Mise à jour des categories objets')
             ->addOption('maj_menu', 'menu', InputOption::VALUE_NONE, 'Mise à jour des menus')
             ->addOption('maj_menu_base', 'menubase', InputOption::VALUE_NONE, 'Mise à jour du menu de base')
             ->addOption('maj_pictos', 'pictos', InputOption::VALUE_NONE, 'Mise à jour des pictos')
             ->addOption('maj_skill', 'skill', InputOption::VALUE_NONE, 'Mise à jour des skills')
             ->addOption('maj_clean_up', 'cleanup', InputOption::VALUE_NONE, 'Mise à jour des clean up')
             ->addOption('maj_death', 'death', InputOption::VALUE_NONE, 'Mise à jour des types de mort');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io      = new SymfonyStyle($input, $output);
        $version = $input->getOption('update-version');
        
        if ($version) {
            $this->updateVersion($io, $version);
        }
        
        $this->updateEntities($input, $io);
        
        return 0;
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_bats(SymfonyStyle $io): void
    {
        // On reprend ce qu'on a fait dans BatFixture.php
        $listBats = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/bats.json');
        
        $listItems = $this->entityManager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $io->comment('Nombre de batiments : ' . count($listBats) . ' .');
        $io->progressStart(count($listBats));
        
        foreach ($listBats as $bat) {
            
            $entity = $this->entityManager->getRepository(BatPrototype::class)->find((int)$bat['id']);
            
            if ($entity === null) {
                $entity = new BatPrototype();
                $entity->setId((int)$bat['id']);
            }
            
            $entity->setDescription(htmlspecialchars_decode((string)$bat['description']))
                   ->setNom(htmlspecialchars_decode((string)$bat['nom']))
                   ->setBonusCamping((int)$bat['bonusCamping'])
                   ->setKmMin((int)$bat['kmMin'])
                   ->setKmMax((int)$bat['kmMax'])
                   ->setIdMh((int)$bat['idMh'])
                   ->setIcon($bat['icon'])
                   ->setMaxCampeur($bat['maxCampeur'] ?? 0)
                   ->setIdHordes(array_flip(BatFixtures::$tabTransco)[(int)$bat['id']] ?? null)
                   ->setActif(true);
            
            /**
             * @var ItemBatiment[] $listItemBat
             */
            $listItemBat = array_combine(array_map(fn($i) => $i['item'], $bat['items']), $bat['items']);
            
            
            foreach ($entity->getItems() as $itemBat) {
                if (isset($listItemBat[$itemBat->getItem()->getId()])) {
                    $itemBat->setProbabily($listItemBat[$itemBat->getItem()->getId()]['count']);
                    unset($listItemBat[$itemBat->getItem()->getId()]);
                } else {
                    $entity->removeItem($itemBat);
                }
            }
            
            foreach ($listItemBat as $itemBat) {
                $itemBatNew = new ItemBatiment();
                
                $itemBatNew->setItem($listItems[$itemBat['item']])
                           ->setProbabily($itemBat['count']);
                
                $entity->addItem($itemBatNew);
            }
            
            
            if ($bat['explorable']) {
                $entity->setExplorable(true);
            } else {
                $entity->setExplorable(false);
            }
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_categories(SymfonyStyle $io): void
    {
        
        /* chargement des héros */
        $listCategories = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/categories.json');
        
        $countHerosSkill = count($listCategories);
        
        
        $io->comment('Nombre de catégories : ' . $countHerosSkill . ' .');
        $io->progressStart($countHerosSkill);
        
        foreach ($listCategories as $key => $category) {
            
            // On verifie si ça n'existe pas, sinon, on met à jour
            $entity = $this->entityManager->getRepository(CategoryObjet::class)->findOneBy(['id' => ($key + 1)]);
            
            if ($entity === null) {
                $entity = new CategoryObjet();
                $entity->setId($key + 1);
            }
            $entity->setNom($category);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_chantier(SymfonyStyle $io): void
    {
        // On reprend ce qu'on a fait dans ChantierFixtures.php
        $list      = $this->entityManager->getRepository(ItemPrototype::class)->findAll();
        $id        = array_map(fn($i) => $i->getId(), $list);
        $listItems = array_combine($id, $list);
        
        $listChantiers = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/chantiers.json');
        
        $io->comment('Nombre de chantiers : ' . count($listChantiers) . ' .');
        
        $io->progressStart(count($listChantiers));
        
        foreach ($listChantiers as $chantier) {
            
            if ($chantier['parent'] !== 0) {
                $parent = $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['parent']);
            } else {
                $parent = null;
            }
            
            if ($chantier['cat'] !== 0) {
                $cat = $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['cat']);
            } else {
                $cat = null;
            }
            
            /* On crée/met à jour les chantiers */
            
            $entity =
                $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['id']);
            
            if ($entity === null) {
                $entity = (new ChantierPrototype());
                $entity->setId((int)$chantier['id']);
            }
            
            $entity->setIcon($chantier['icon'])
                   ->setNom($chantier['nom'])
                   ->setDescription($chantier['desc'])
                   ->setDef($chantier['def'])
                   ->setWater($chantier['water'])
                   ->setPa($chantier['pa'])
                   ->setNiveau($chantier['niveau'])
                   ->setPlan($chantier['plan'])
                   ->setTemp((bool)$chantier['temp'])
                   ->setIndes((bool)$chantier['indes'])
                   ->setPv($chantier['pv'])
                   ->setRuineHo((bool)$chantier['ruineHo'])
                   ->setRuineBu((bool)$chantier['ruineBu'])
                   ->setRuineHs((bool)$chantier['ruineHs'])
                   ->setParent($parent)
                   ->setCatChantier($cat)
                   ->setOrderby((int)$chantier['orderBy'])
                   ->setLevelMax($chantier['levelMax'])
                   ->setOrderByListing($chantier['orderByListing'])
                   ->setIdHordes($chantier['idHordes'])
                   ->setUid($chantier['uid'])
                   ->setOrderByGeneral($chantier['orderByGeneral'])
                   ->setActif($chantier['actif'])
                   ->setSpecifiqueVillePrive($chantier['specifiqueVillePrive'] ?? false);
            
            
            /* On travaille sur les ressources */
            $ressourcesTab = $chantier['ressources'];
            
            $ressourcesAct = $entity->getRessources();
            
            
            foreach ($ressourcesAct as $ress) {
                if (isset($ressourcesTab[$ress->getItem()->getId()])) {
                    $ress->setNombre($ressourcesTab[$ress->getItem()->getId()]);
                    unset($ressourcesTab[$ress->getItem()->getId()]);
                } else {
                    $entity->removeRessource($ress);
                }
            }
            
            
            foreach ($ressourcesTab as $key => $nbr) {
                if ($nbr === '0') {
                    break;
                }
                
                $ressource = new RessourceChantier();
                $ressource->setNombre((int)$nbr)
                          ->setItem($listItems[$key]);
                
                $entity->addRessource($ressource);
            }
            
            // Gestion des up chantiers
            if (isset($chantier['levelUps'])) {
                $jsonLevelUps = $chantier['levelUps'];
                // Map des levelUps existants par ID
                $existingLevelUps = [];
                foreach ($entity->getLevelUps() as $levelUp) {
                    $existingLevelUps[$levelUp->getId()] = $levelUp;
                }
                
                foreach ($jsonLevelUps as $levelUpData) {
                    
                    $levelUpId = $levelUpData['id'];
                    
                    // Cas 1: Le levelUp existe déjà (mise à jour)
                    if (isset($existingLevelUps[$levelUpId])) {
                        $levelUp = $existingLevelUps[$levelUpId];
                        $levelUp->setLevel($levelUpData['lvl']);
                        
                        // Map des bonus existants par ID
                        $existingBonuses = [];
                        foreach ($levelUp->getBonusUps() as $bonus) {
                            $existingBonuses[$bonus->getId()] = $bonus;
                        }
                        
                        // Mise à jour des bonus
                        foreach ($levelUpData['bonus'] as $bonusData) {
                            $bonusId = $bonusData['id'];
                            
                            if (isset($existingBonuses[$bonusId])) {
                                // Mise à jour du bonus existant
                                $existingBonuses[$bonusId]
                                    ->setTypeBonus($bonusData['type_bonus'])
                                    ->setValeurUp($bonusData['valeur']);
                                unset($existingBonuses[$bonusId]);
                            } else {
                                // Création d'un nouveau bonus avec l'ID spécifié
                                $newBonus = new UpChantierPrototype();
                                $newBonus->setId($bonusId)
                                         ->setTypeBonus($bonusData['type_bonus'])
                                         ->setValeurUp($bonusData['valeur']);
                                $levelUp->addBonusUp($newBonus);
                            }
                        }
                        
                        // Suppression des bonus qui n'existent plus
                        foreach ($existingBonuses as $oldBonus) {
                            $levelUp->removeBonusUp($oldBonus);
                        }
                        
                        unset($existingLevelUps[$levelUpId]);
                    } // Cas 2: Nouveau levelUp
                    else {
                        $newLevelUp = new BonusUpChantier();
                        $newLevelUp->setId($levelUpId)
                                   ->setLevel($levelUpData['lvl']);
                        
                        // Ajout des bonus
                        foreach ($levelUpData['bonus'] as $bonusData) {
                            $newBonus = new UpChantierPrototype();
                            $newBonus->setId($bonusData['id'])
                                     ->setTypeBonus($bonusData['type_bonus'])
                                     ->setValeurUp($bonusData['valeur']);
                            $newLevelUp->addBonusUp($newBonus);
                        }
                        
                        $entity->addLevelUp($newLevelUp);
                    }
                }
                
                // Suppression des levelUps qui n'existent plus
                foreach ($existingLevelUps as $oldLevelUp) {
                    $entity->removeLevelUp($oldLevelUp);
                }
            }
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
        
        
        $io->info("Update fixtures: parent on ChantierPrototype Database");
        
        $io->comment("Nombre de chantiers à mettre à jour : " . count($listChantiers) . " .");
        
        $io->progressStart(count($listChantiers));
        
        
        foreach ($listChantiers as $chantier) {
            $entity = $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['id']);
            
            if ($entity !== null) {
                if ($chantier['parent'] !== 0) {
                    $parent = $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['parent']);
                } else {
                    $parent = null;
                }
                
                if ($chantier['cat'] !== 0) {
                    $cat = $this->entityManager->getRepository(ChantierPrototype::class)->find((int)$chantier['cat']);
                } else {
                    $cat = null;
                }
                
                $entity->setParent($parent)
                       ->setCatChantier($cat);
                
                $this->entityManager->persist($entity);
                $io->progressAdvance();
            }
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    private function maj_clean_up(SymfonyStyle $io): void
    {
        $listCleanUp = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/clean_up_cadaver_data.json');
        
        $io->comment('Nombre de clean up : ' . count($listCleanUp) . ' .');
        $io->progressStart(count($listCleanUp));
        
        foreach ($listCleanUp as $cleanUp) {
            
            $entity = $this->entityManager->getRepository(CleanUpCadaver::class)->findOneBy(['id' => $cleanUp['id']]);
            
            if ($entity === null) {
                $entity = new CleanUpCadaver($cleanUp['id']);
            }
            
            $entity->setRef($cleanUp['ref'])
                   ->setLabel($cleanUp['label'])
                   ->setIcon($cleanUp['icon']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_creneau_horraire(SymfonyStyle $io): void
    {
        $listCreneauHorraire = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/typeCreneau.json');
        
        $io->comment('Nombre de Creneau horraire : ' . count($listCreneauHorraire) . ' .');
        $io->progressStart(count($listCreneauHorraire));
        
        foreach ($listCreneauHorraire as $creneau) {
            
            $entity = $this->entityManager->getRepository(CreneauHorraire::class)
                                          ->findOneBy(['id' => $creneau['id']]);
            
            if ($entity === null) {
                $entity = new CreneauHorraire();
                $entity->setId($creneau['id']);
            }
            
            $entity->setLibelle($creneau['libelle'])
                   ->setTypologie($creneau['type']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_death(SymfonyStyle $io): void
    {
        $listTypeDeath = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/type_of_death_data.json');
        
        $io->comment('Nombre de TypeDeath : ' . count($listTypeDeath) . ' .');
        $io->progressStart(count($listTypeDeath));
        
        foreach ($listTypeDeath as $key => $death) {
            
            $entity = $this->entityManager->getRepository(TypeDeath::class)
                                          ->findOneBy(['idMort' => $key]);
            
            if ($entity === null) {
                $entity = new TypeDeath($key);
            }
            
            $entity->setNom($death['nom'])
                   ->setLabel($death['label'])
                   ->setIcon($death['icon']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_heros(SymfonyStyle $io): void
    {
        
        /* chargement des héros */
        $listHeros = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/heros.json');
        
        $countHerosSkill = count($listHeros);
        
        $io->comment('Nombre de héros : ' . $countHerosSkill . ' .');
        $io->progressStart($countHerosSkill);
        
        foreach ($listHeros as $key => $hero) {
            
            $entity = $this->entityManager->getRepository(HerosPrototype::class)
                                          ->findOneBy(['nom' => htmlspecialchars_decode((string)$hero['nom'])]);
            
            if ($entity === null) {
                $entity = new HerosPrototype();
                $entity->setId($key);
            }
            
            $entity->setDescription(htmlspecialchars_decode((string)$hero['description']))
                   ->setNom(htmlspecialchars_decode((string)$hero['nom']))
                   ->setIcon($hero['icon'])
                   ->setJour($hero['jour'])
                   ->setJourCumul($hero['jour_cumul'])
                   ->setPouvActif((bool)$hero['pouv_actif'])
                   ->setPouvBase((bool)$hero['pouv_base'])
                   ->setMethod(($hero['method'] !== '') ? $hero['method'] : null)
                   ->setOrdreRecup(($hero['order'] !== '') ? $hero['order'] : null);
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_homes(SymfonyStyle $io): void
    {
        // On reprend ce qu'on a fait dans BatFixture.php
        $listHomes = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/home_data.json');
        
        $listItems = $this->entityManager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $io->comment('Nombre de habitations : ' . count($listHomes) . ' .');
        $io->progressStart(count($listHomes));
        
        foreach ($listHomes as $home) {
            
            $entity = $this->entityManager->getRepository(HomePrototype::class)->find((int)$home['id']);
            
            if ($entity === null) {
                $entity = new HomePrototype();
                $entity->setId($home['id']);
            } else {
                foreach ($entity->getRessources() as $ressource) {
                    $entity->removeRessource($ressource);
                }
                foreach ($entity->getRessourcesUrba() as $ressource) {
                    $entity->removeRessourceUrba($ressource);
                }
            }
            
            $entity->setNom($home['nom'])
                   ->setIcon($home['icon'])
                   ->setDef($home['def'])
                   ->setPa($home['pa'])
                   ->setPaUrba($home['pa_u']);
            
            //On ajoute les ressources
            foreach ($home['ressources'] as $id => $nombre) {
                $ressource = new RessourceHome();
                
                $item = $this->entityManager->getRepository(ItemPrototype::class)->find($id);
                
                $ressource->setId($home['id'] * 10000 + $item->getId() * 10)
                          ->setItem($item)
                          ->setNombre($nombre);
                $entity->addRessource($ressource);
            }
            //On ajoute les ressources
            foreach ($home['ressources_u'] as $id => $nombre) {
                $ressource = new RessourceHome();
                
                $item = $this->entityManager->getRepository(ItemPrototype::class)->find($id);
                
                $ressource->setId($home['id'] * 10000 + $item->getId() * 10 + 1)
                          ->setItem($item)
                          ->setNombre($nombre);
                $entity->addRessourceUrba($ressource);
            }
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_items(SymfonyStyle $io): void
    {
        
        // On reprend ce qu'on a fait dans ObjetFixtures.php
        $listObjets = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/listObjet.json');
        
        $listTypeObjets    = $this->entityManager->getRepository(TypeObjet::class)->findAll();
        $listCategories    = $this->entityManager->getRepository(CategoryObjet::class)->findAll();
        $listTypeDecharges = $this->entityManager->getRepository(TypeDecharge::class)->findAll();
        
        $io->comment('Nombre d\'objets : ' . count($listObjets) . ' .');
        
        $io->progressStart(count($listObjets));
        
        
        foreach ($listObjets as $objet) {
            $entity = $this->entityManager->getRepository(ItemPrototype::class)->find((int)$objet['id']);
            
            if ($entity === null) {
                $entity = new ItemPrototype();
                $entity->setId($objet['id']);
            }
            $entity->setNom(htmlspecialchars_decode((string)$objet['nom']))
                   ->setDescription(htmlspecialchars_decode((string)$objet['description']))
                   ->setIcon(htmlspecialchars_decode((string)$objet['icon']))
                   ->setCategoryObjet($listCategories[$objet['category'] - 1])
                   ->setTypeObjet((!isset($objet['type_objet'])) ? null : $listTypeObjets[$objet['type_objet'] - 1])
                   ->setObjetVeille($objet['objet_veille'])
                   ->setEncombrant($objet['encombrant'] ?? null)
                   ->setUsageUnique($objet['usageUnique'] ?? null)
                   ->setReparable($objet['reparable'] ?? null)
                   ->setTourelle($objet['tourelle'] ?? null)
                   ->setArmurerie($objet['armurerie'] ?? null)
                   ->setMagasin($objet['magasin'] ?? null)
                   ->setLanceBete($objet['lanceBete'] ?? null)
                   ->setDefBase($objet['defBase'] ?? null)
                   ->setUid($objet['uid'])
                   ->setConteneur($objet['conteneur'] ?? null)
                   ->setChanceKill($objet['chance_kill'] ?? null)
                   ->setChance($objet['chance'] ?? null)
                   ->setType($objet['type'] ?? null)
                   ->setActif($objet['actif'] ?? false)
                   ->setProbaPoubelle($objet['proba'] ?? 0)
                   ->setDeco($objet['deco'] ?? 0)
                   ->setExpedition($objet['expedition'] ?? false)
                   ->setIdMh($objet['id_mh'])
                   ->setUpdateByAdmin(false);
            
            if (isset($objet['kill'])) {
                if (is_array($objet['kill'])) {
                    $entity->setKillMin($objet['kill'][0])
                           ->setKillMax($objet['kill'][1]);
                } else {
                    $entity->setKillMin($objet['kill']);
                }
            }
            
            if (isset($objet['caracteristiques'])) {
                $caracteristiques = $objet['caracteristiques'];
                foreach ($caracteristiques as $carac) {
                    $caracEntity = $this->entityManager->getRepository(TypeCaracteristique::class)->find($carac['typeCarac']);
                    $caracItem   = $this->entityManager->getRepository(CaracteristiquesItem::class)->findOneBy(['id' => $carac['id']]);
                    if ($caracItem === null) {
                        $caracItem = new CaracteristiquesItem();
                        $caracItem->setId($carac['id']);
                    }
                    $caracItem->setValue($carac['value'])
                              ->setTypeCarac($caracEntity)
                              ->setProbabilite($carac['probabilite']);
                    
                    $entity->addCaracteristique($caracItem);
                }
            }
            
            if (isset($objet['type_decharge'])) {
                $entity->setTypeDecharge($listTypeDecharges[$objet['type_decharge'] - 1]);
            }
            
            if (isset($objet['decharge'])) {
                $entity->setDecharge($listTypeDecharges[$objet['decharge'] - 1]);
            }
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        
        $io->progressFinish();
        
        
        $io->progressStart(count($listObjets));
        
        foreach ($listObjets as $objet) {
            if (isset($objet['id_transform'])) {
                $entity = $this->entityManager->getRepository(ItemPrototype::class)->find($objet['id']);
                $entity->removeAllItemResult();
                if (is_array($objet['id_transform'])) {
                    foreach ($objet['id_transform'] as $id_transform) {
                        $objet_transform =
                            $this->entityManager->getRepository(ItemPrototype::class)->find($id_transform);
                        
                        $entity->addItemResult($objet_transform);
                    }
                } else {
                    $objet_transform =
                        $this->entityManager->getRepository(ItemPrototype::class)->find($objet['id_transform']);
                    
                    $entity->addItemResult($objet_transform);
                }
                
                $this->entityManager->persist($entity);
                
            }
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
        
        
        // Récupération des actions sur chacun des objets
        $listActions = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/listTypeAction.json');
        
        
        $io->info('Installing fixtures: typeAction Database');
        
        $io->comment('Nombre d\'actions : ' . count($listActions) . ' .');
        
        $io->progressStart(count($listActions));
        
        foreach ($listActions as $key => $action) {
            
            $entity = $this->entityManager->getRepository(TypeActionAssemblage::class)->find($key);
            
            if ($entity === null) {
                $entity = new TypeActionAssemblage();
                $entity->setId($key);
            }
            $entity->setNom(htmlspecialchars_decode((string)$action['nom']))
                   ->setNomItemNeed(htmlspecialchars_decode((string)$action['need']))
                   ->setNomItemObtain(htmlspecialchars_decode((string)$action['obtain']))
                   ->setDescription((($action['description'] ?? null) === null) ? null :
                                        htmlspecialchars_decode((string)$action['description']));
            
            $this->entityManager->persist($entity);
            
            $io->progressAdvance();
            
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
        
        
        // On récupère la liste des assemblages
        $listAssemblage = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/listAssemblage.json');
        
        $io->info('Installing fixtures: assemblage Database');
        
        $io->comment('Nombre d\'assemblages : ' . count($listAssemblage) . ' .');
        
        $io->progressStart(count($listAssemblage));
        
        $listItems = $this->entityManager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        foreach ($listItems as $item) {
            if ($item->getListAssemblage() !== null) {
                $item->setListAssemblage(null);
                $this->entityManager->persist($item);
            }
        }
        
        $this->entityManager->flush();
        $listAssemblageBdd = $this->entityManager->getRepository(ListAssemblage::class)->findAll();
        
        foreach ($listAssemblageBdd as $assemblage) {
            $this->entityManager->remove($assemblage);
        }
        
        $this->entityManager->flush();
        
        foreach ($listAssemblage as $key => $assemblage) {
            $entity = $this->entityManager->getRepository(ListAssemblage::class)->find($key + 1);
            
            if ($entity === null) {
                $entity = new ListAssemblage();
                $entity->setId($key + 1);
            }
            
            $entity->removeItemObtainAll();
            foreach ($assemblage['item_obtains'] as $item_obtain) {
                
                $itemObtain = $this->entityManager->getRepository(ItemProbability::class)
                                                  ->findOneBy(['id' => $item_obtain[1] * 10000 + $item_obtain[0]]);
                
                if ($itemObtain === null) {
                    $itemObtain = new ItemProbability($listItems[$item_obtain[0]], $item_obtain[1]);
                }
                
                $entity->addItemObtain($itemObtain);
            }
            
            $entity->removeItemNeedAll();
            foreach ($assemblage['item_needs'] as $item_need) {
                
                $nbr     = $item_need[1] ?? 1;
                $id_item = $item_need[0] ?? 1;
                
                $itemNeed = $this->entityManager->getRepository(ItemNeed::class)->findOneBy(['id' => ($nbr * 10000 +
                                                                                                      $id_item)]);
                
                if ($itemNeed === null) {
                    $itemNeed = new ItemNeed($listItems[$item_need[0]], $item_need[1] ?? 1);
                }
                
                $entity->addItemNeed($itemNeed);
            }
            
            $typeAction = $this->entityManager->getRepository(TypeActionAssemblage::class)
                                              ->findOneBy(['id' => $assemblage['type_action']]);
            
            $entity->setTypeAction($typeAction);
            
            $entity->setItemPrincipal($listItems[$assemblage['item_pp']]);
            
            $this->entityManager->persist($entity);
            $this->entityManager->persist($listItems[$assemblage['item_pp']]->setListAssemblage($entity));
            
            $this->entityManager->flush();
            
            $io->progressAdvance();
            
        }
        
        $io->progressFinish();
        
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_job(SymfonyStyle $io): void
    {
        /* chargement des héros */
        $listJobs = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/profession_data.json');
        
        $countHerosSkill = count($listJobs);
        
        
        $io->comment('Nombre de métiers : ' . $countHerosSkill . ' .');
        $io->progressStart($countHerosSkill);
        
        foreach ($listJobs as $key => $job) {
            
            // On verifie si ça n'existe pas, sinon, on met à jour
            $entity = $this->entityManager->getRepository(JobPrototype::class)->findOneBy(['id' => $job['id']]);
            
            if ($entity === null) {
                $entity = new JobPrototype();
            }
            $entity->setIsHeros($job['hero']);
            $entity->setNom($job['nom']);
            $entity->setDescription($job['desc']);
            $entity->setIcon($job['icon']);
            $entity->setRef($job['ref']);
            $entity->setAlternatif($job['alternatif']);
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_menu(SymfonyStyle $io): void
    {
        
        /* chargement des menus */
        $listMenus = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/listMenu.json');
        
        $countMenus = count($listMenus);
        
        $io->comment('Nombre de menus : ' . $countMenus . ' .');
        $io->progressStart($countMenus);
        
        foreach ($listMenus as $menu) {
            
            // On verifie si ça n'existe pas, sinon, on met à jour
            $entity = $this->entityManager->getRepository(MenuPrototype::class)->findOneBy(['id' => $menu['id']]);
            
            if ($entity === null) {
                $entity = new MenuPrototype();
                $entity->setId($menu['id']);
            }
            $entity->setLabel($menu['label'])
                   ->setRoute($menu['route'])
                   ->setIcone($menu['icone'])
                   ->setCategory($menu['category'])
                   ->setConnected($menu['connected'])
                   ->setHabUser($menu['habUser'])
                   ->setHabBeta($menu['habBeta'])
                   ->setHabAdmin($menu['habAdmin'])
                   ->setForUser($menu['forUser'])
                   ->setVille($menu['ville'])
                   ->setMyVille($menu['myVille']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_menu_base(SymfonyStyle $io): void
    {
        
        // On vient supprimer l'ancien menu de base
        $menuBaseOld = $this->entityManager->getRepository(Menu::class)->findOneBy(['user' => null]);
        
        if ($menuBaseOld !== null) {
            $this->entityManager->remove($menuBaseOld);
            $this->entityManager->flush();
        }
        
        /* chargement des menus */
        $listGroupMenus = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/menuBase.json');
        
        $countMenus = count($listGroupMenus);
        
        $io->comment('Nombre de menus/groups : ' . $countMenus . ' .');
        $io->progressStart($countMenus);
        
        // création du menu de base
        $menuBase = new Menu();
        
        foreach ($listGroupMenus as $menuElementFile) {
            
            // On regarde le type de menu (group ou menu)
            if ($menuElementFile['type_menu'] === MenuType::TYPE_G->value) {
                
                // On crée le MenuElement pour le group
                $menuElementGroup = new MenuElement();
                
                $menuElementGroup->setName($menuElementFile['name'])
                                 ->setOrdre($menuElementFile['ordre'])
                                 ->setTypeMenu(MenuType::TYPE_G);
                
                // On va venir ajouter les menus enfants
                foreach ($menuElementFile['enfant'] as $menuChild) {
                    $menuElementChild = new MenuElement();
                    
                    // recherche du MenuPrototype pour le menu
                    $menuPrototype = $this->entityManager->getRepository(MenuPrototype::class)->findOneBy(['id' => $menuChild['menu']]);
                    
                    $menuElementChild->setMenu($menuPrototype)
                                     ->setOrdre($menuChild['ordre'])
                                     ->setTypeMenu(MenuType::TYPE_M);
                    
                    $menuElementGroup->addItem($menuElementChild);
                }
                
                $menuBase->addItem($menuElementGroup);
                
            } else {
                // recherche du MenuPrototype pour le menu
                $menuPrototype = $this->entityManager->getRepository(MenuPrototype::class)->findOneBy(['id' => $menuElementFile['menu']]);
                
                $menuElementMenu = new MenuElement();
                $menuElementMenu->setMenu($menuPrototype)
                                ->setOrdre($menuElementFile['ordre'])
                                ->setTypeMenu(MenuType::TYPE_M);
                
                $menuBase->addItem($menuElementMenu);
            }
            
            $io->progressAdvance();
        }
        
        $menuBase->setUser(null);
        $this->entityManager->persist($menuBase);
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_pictos(SymfonyStyle $io): void
    {
        
        // On vient supprimer tous les titres actuellement en base (uniquement pour la version v3.31.0 !)
        $oldTitre = $this->entityManager->getRepository(PictoTitrePrototype::class)->findAll();
        
        foreach ($oldTitre as $titre) {
            $this->entityManager->remove($titre);
        }
        $this->entityManager->flush();
        
        // On récupère les pictos et titre du fichier
        $listPictos = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/pictos.json');
        
        $countPictos = count($listPictos);
        
        
        $io->comment('Nombre de Pictos : ' . $countPictos . ' .');
        $io->progressStart($countPictos);
        
        foreach ($listPictos as $picto) {
            
            $entity = $this->entityManager->getRepository(PictoPrototype::class)->find($picto['id']);
            
            if ($entity === null) {
                $entity = new PictoPrototype();
                $entity->setId($picto['id']);
            }
            
            $entity->setName($picto['name'])
                   ->setDescription($picto['description'])
                   ->setRare($picto['rare'])
                   ->setCommunity($picto['community'])
                   ->setImg($picto['img'])
                   ->setUuid($picto['uuid'])
                   ->setActif(true);
            
            $entity->removeAllTitre();
            
            foreach ($picto['title'] as $titre) {
                
                $titrebdd = $this->entityManager->getRepository(PictoTitrePrototype::class)->findOneBy(['id' => $titre['id']]);
                
                if ($titrebdd === null) {
                    $titrebdd = new PictoTitrePrototype();
                    
                    $titrebdd->setNbr($titre['nbr'])
                             ->setPictoPrototype($entity);
                }
                
                $titrebdd->setTitre($titre['titre']);
                $this->entityManager->persist($titrebdd);
                $this->entityManager->flush();
                $this->entityManager->refresh($titrebdd);
                
                $entity->addTitre($titrebdd);
                
            }
            
            $this->entityManager->persist($entity);
            
            $io->progressAdvance();
            
        }
        
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_regroupement(SymfonyStyle $io): void
    {
        // On récupére les regroupements
        $listRegroupement = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/regroupement.json');
        
        $listItems = $this->entityManager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $listChantiers = $this->entityManager->getRepository(ChantierPrototype::class)->findAllIndexed();
        
        $io->comment('Nombre de regroupement : ' . count($listRegroupement) . ' .');
        $io->progressStart(count($listRegroupement));
        
        foreach ($listRegroupement as $regpt) {
            // Récup du regroupement si existe
            $regroupement = $this->entityManager->getRepository(RegroupementItemsDecharge::class)
                                                ->findOneBy(['id' => $regpt['id']]);
            
            if ($regroupement === null) {
                $regroupement = new RegroupementItemsDecharge();
                $regroupement->setId($regpt['id']);
            }
            
            $regroupement->setNom($regpt['nom'])
                         ->setPointDefBase($regpt['pointDefBase'])
                         ->setPointDefDecharge($regpt['pointDefDecharge'])
                         ->setChantierDecharge($listChantiers[ChantierFixtures::$tabTransco[$regpt['decharge']]]);
            
            foreach ($regroupement->getRegroupItems() as $items) {
                $regroupement->removeRegroupItem($items);
            }
            
            foreach ($regpt['listItemsRgt'] as $idItem) {
                $regroupement->addRegroupItem($listItems[ObjetFixtures::$tabObjetTransco[$idItem]]);
            }
            foreach ($regroupement->getItemAffiches() as $items) {
                $regroupement->removeItemAffich($items);
            }
            
            foreach ($regpt['listItemsAff'] as $idItem) {
                $regroupement->addItemAffich($listItems[ObjetFixtures::$tabObjetTransco[$idItem]]);
            }
            
            $this->entityManager->persist($regroupement);
            $io->progressAdvance();
            
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
        
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_ruine_game_proto(SymfonyStyle $io): void
    {
        // On récupére les regroupements
        $listRuinePrototypes = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/ruine_prototype_data.json');
        
        $listObjets = $this->entityManager->getRepository(ItemPrototype::class)->findAllIndexed();
        
        $countRuineProtosSkill = count($listRuinePrototypes);
        
        $io->comment('Nombre de RuineGamePrototype : ' . $countRuineProtosSkill . ' .');
        $io->progressStart($countRuineProtosSkill);
        
        foreach ($listRuinePrototypes as $ruinePrototype) {
            
            $entity = $this->entityManager->getRepository(RuineGameZonePrototype::class)
                                          ->findOneBy(['id' => $ruinePrototype['id']]);
            
            if ($entity === null) {
                $entity = new RuineGameZonePrototype();
                $entity->setId($ruinePrototype['id']);
            }
            
            $entity->setLabel($ruinePrototype['label'])
                   ->setLevelZone($ruinePrototype['level']);
            
            if ($ruinePrototype['empreinte'] !== null) {
                $entity->setEmpreinteItem($listObjets[$ruinePrototype['empreinte']]);
            }
            if ($ruinePrototype['clef'] !== null) {
                $entity->setEmpreinteItem($listObjets[$ruinePrototype['clef']]);
            }
            if ($ruinePrototype['type'] !== null) {
                $entity->setTypeCase($ruinePrototype['type']);
            }
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_skill(SymfonyStyle $io): void
    {
        
        // On récupère les skills depuis le fichier
        $listSkills = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/herosSkill.json');
        
        $countSkills = count($listSkills);
        
        $io->comment('Nombre de Skill : ' . $countSkills . ' .');
        $io->progressStart($countSkills);
        
        foreach ($listSkills as $skillType) {
            
            $entity = $this->entityManager->getRepository(HerosSkillType::class)->find($skillType['id']);
            
            if ($entity === null) {
                $entity = new HerosSkillType();
                $entity->setId($skillType['id']);
            }
            
            $entity->setName($skillType['name']);
            
            $entity->removeAllLevel();
            
            foreach ($skillType['level'] as $level) {
                
                $levelbdd = $this->entityManager->getRepository(HerosSkillLevel::class)->findOneBy(['id' => $level['id']]);
                
                if ($levelbdd === null) {
                    $levelbdd = new HerosSkillLevel();
                    
                    $levelbdd->setId($level['id']);
                } else {
                    $levelbdd->removeAllPouvoir();
                }
                
                // Recherche du level skill dans l'enum
                $lvlSkin = LevelSkill::tryFrom($level['lvl']) ?? LevelSkill::HABITANT;
                
                $levelbdd->setName($level['name'])->setXp($level['xp'])->setLvl($lvlSkin);
                
                // On ajoute les pouvoirs
                foreach ($level['pouvoir'] as $pouvoir) {
                    $pouvoirBdd = $this->entityManager->getRepository(HerosSkillPrototype::class)->findOneBy(['id' => $pouvoir['id']]);
                    
                    if ($pouvoirBdd === null) {
                        $pouvoirBdd = new HerosSkillPrototype();
                        $pouvoirBdd->setId($pouvoir['id']);
                    }
                    
                    $pouvoirBdd->setText($pouvoir['text'])
                               ->setValue($pouvoir['value'])
                               ->setValue2($pouvoir['value2']);
                    
                    if ($pouvoir['heros'] !== null) {
                        $herosProto = $this->entityManager->getRepository(HerosPrototype::class)->findOneBy(['id' => $pouvoir['heros']]);
                        
                        if ($herosProto !== null) {
                            $pouvoirBdd->setHeros($herosProto);
                        }
                    }
                    
                    $levelbdd->addPouvoir($pouvoirBdd);
                }
                
                $entity->addLevel($levelbdd);
                
            }
            
            $this->entityManager->persist($entity);
            
            $io->progressAdvance();
            
        }
        
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_type_carac(SymfonyStyle $io): void
    {
        // On récupère les types de caractéristiques
        $listCarac = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/typeCarac.json');
        
        $io->comment('Nombre de carac : ' . count($listCarac) . ' .');
        $io->progressStart(count($listCarac));
        
        foreach ($listCarac as $typeCarac) {
            $entity = $this->entityManager->getRepository(TypeCaracteristique::class)->find((int)$typeCarac['id']);
            
            if ($entity === null) {
                $entity = new TypeCaracteristique();
                $entity->setId($typeCarac['id']);
            }
            
            $entity->setNom($typeCarac['nom'])
                   ->setIcon($typeCarac['icon']);
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_type_dispo(SymfonyStyle $io): void
    {
        $listTypeDispo = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/typeDispo.json');
        
        $io->comment('Nombre de TypeDispo : ' . count($listTypeDispo) . ' .');
        $io->progressStart(count($listTypeDispo));
        
        foreach ($listTypeDispo as $dispo) {
            
            $entity = $this->entityManager->getRepository(TypeDispo::class)
                                          ->findOneBy(['id' => $dispo['id']]);
            
            if ($entity === null) {
                $entity = new TypeDispo();
                $entity->setId($dispo['id']);
            }
            
            $entity->setNom($dispo['libelle'])
                   ->setDescription("")
                   ->setTypologie($dispo['type']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    /**
     * @param SymfonyStyle $io
     * @return void
     */
    private function maj_type_expedition(SymfonyStyle $io): void
    {
        $listTypeExpe = $this->commandHelper->getJsonFile($this->kernel->getProjectDir() . '/src/DataFixtures/data/typeExpedition.json');
        
        $io->comment('Nombre de TypeExpedition : ' . count($listTypeExpe) . ' .');
        $io->progressStart(count($listTypeExpe));
        
        foreach ($listTypeExpe as $typeExpe) {
            
            $entity = $this->entityManager->getRepository(ExpeditionType::class)->findOneBy(['id' => $typeExpe['id']]);
            
            if ($entity === null) {
                $entity = new ExpeditionType();
                $entity->setId($typeExpe['id']);
            }
            
            $entity->setNom($typeExpe['nom'])
                   ->setIcon($typeExpe['icon'])
                   ->setDescription($typeExpe['description']);
            
            
            $this->entityManager->persist($entity);
            $io->progressAdvance();
        }
        
        $this->entityManager->flush();
        $io->progressFinish();
    }
    
    private function runCommands(SymfonyStyle $io, string $version, string ...$commands): void
    {
        $io->section("Mise à jour des prototypes pour la version $version");
        foreach ($commands as $command) {
            if (!$this->commandHelper->capsule($command, $io)) {
                $io->error("Impossible d'exécuter la commande : $command");
                exit(1);
            }
        }
        $io->success("Mise à jour des prototypes pour la version $version terminée");
    }
    
    private function updateEntities(InputInterface $input, SymfonyStyle $io): void
    {
        $this->updateOption($input, $io, 'maj_chantier', 'ChantierPrototype', 'ChantierPrototype & Ressources Database');
        $this->updateOption($input, $io, 'maj_items', 'ItemPrototype', 'ItemPrototype Database');
        $this->updateOption($input, $io, 'maj_bats', 'BatPrototype', 'BatPrototype Database');
        $this->updateOption($input, $io, 'maj_homes', 'HomePrototype', 'HomePrototype Database');
        $this->updateOption($input, $io, 'maj_regroupement', 'RegroupementItemsDecharge', 'RegroupementItems Database');
        $this->updateOption($input, $io, 'maj_ruine_game_proto', 'RuineGameZonePrototype', 'RuineGamePrototype Database');
        $this->updateOption($input, $io, 'maj_type_expedition', 'ExpeditionType', 'TypeExpedition Database');
        $this->updateOption($input, $io, 'maj_creneau_horraire', 'CreneauHorraire', 'Type Creneau Database');
        $this->updateOption($input, $io, 'maj_type_dispo', 'TypeDispo', 'Type Dispo Database');
        $this->updateOption($input, $io, 'maj_type_carac', 'TypeCaracteristique', 'Type Carac Database');
        $this->updateOption($input, $io, 'maj_heros', 'HerosPrototype', 'Heros Database');
        $this->updateOption($input, $io, 'maj_job', 'JobPrototype', 'JobPrototype Database');
        $this->updateOption($input, $io, 'maj_categories', 'CategoryObjet', 'Categories Database');
        $this->updateOption($input, $io, 'maj_menu', 'MenuPrototype', 'Menu Prototype Database');
        $this->updateOption($input, $io, 'maj_menu_base', 'Menu', 'Menu Base Database');
        $this->updateOption($input, $io, 'maj_pictos', 'PictoPrototype', 'Pictoprototype Database');
        $this->updateOption($input, $io, 'maj_skill', 'HerosSkillType', 'HerosSkillType Database');
        $this->updateOption($input, $io, 'maj_death', 'TypeDeath', 'TypeDeath Database');
        $this->updateOption($input, $io, 'maj_clean_up', 'CleanUpCadaver', 'CleanUpCadaver Database');
    }
    
    private function updateOption(InputInterface $input, SymfonyStyle $io, string $option, string $entity, string $message): void
    {
        if ($input->getOption($option)) {
            $io->info("Installing fixtures: $message");
            
            if ($input->getOption($option)) {
                $this->$option($io);
            }
            
            $io->success("Mise à jour des $entity terminée");
        }
    }
    
    private function updateVersion(SymfonyStyle $io, string $version): void
    {
        $io->title('Mise à jour des prototypes');
        $io->info('Version du site : ' . $version);
        
        if ($version === 'v3.35.1') {
            $this->runCommands($io, '3.35.1', 'app:update:prototype --maj_chantier');
        }
        
        if ($version === 'v3.36.0') {
            $this->runCommands($io, '3.36.0', 'app:migration:git:reprise-update-chantiers', 'app:update:prototype --maj_chantier', 'app:update:prototype --maj_death', 'app:update:prototype --maj_clean_up', 'app:update:prototype --maj_items', 'app:update:prototype --maj_bats');
        }
    }
}
