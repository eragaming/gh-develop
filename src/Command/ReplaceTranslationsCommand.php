<?php

namespace App\Command;

use App\Service\CommandHelper;
use App\Service\Generality\TranslateArrayHandler;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:replace-translations',
    description: 'Add a short description for your command',
)]
class ReplaceTranslationsCommand extends Command
{
    public function __construct(private readonly CommandHelper $commandHelper)
    {
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this
            ->addArgument('file', InputArgument::REQUIRED, 'The path to the TSX file.')
            ->addArgument('methodName', InputArgument::REQUIRED, 'The name of the method that returns the translations.');
    }
    
    /**
     * @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        $filePath   = __DIR__ . '/../../assets/react/' . $input->getArgument('file');
        $className  = TranslateArrayHandler::class;
        $methodName = $input->getArgument('methodName');
        
        if (!file_exists($filePath)) {
            $io->error("The file $filePath does not exist.");
            return Command::FAILURE;
        }
        
        if (!class_exists($className)) {
            $io->error("The class $className does not exist.");
            return Command::FAILURE;
        }
        
        $reflectionMethod = new ReflectionMethod($className, $methodName);
        $translations     = $reflectionMethod->invoke(new $className());
        
        $flatTranslations = $this->flattenArray($translations);
        
        $tsxContent = file_get_contents($filePath);
        
        $updatedContent = $this->replaceTranslationsInReactCode($tsxContent, $flatTranslations);
        file_put_contents($filePath, $updatedContent);
        
        $io->success("The translation keys in the file $filePath have been successfully replaced.");
        
        return Command::SUCCESS;
    }
    
    
    private function flattenArray($array, $prefix = ''): array
    {
        $result = [];
        
        foreach ($array as $key => $value) {
            $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;
            
            if (is_array($value) && isset($value['key'])) {
                // Only include entries that have a 'key'
                $result[$new_key] = $value;
            } elseif (is_array($value)) {
                // Recurse into nested arrays
                $result = array_merge($result, $this->flattenArray($value, $new_key));
            }
        }
        
        return $result;
    }
    
    private function replaceTranslationsInReactCode(string $code, array $translations): string
    {
        return preg_replace_callback('/translate\.([\w\.]+)/', function ($matches) use ($translations) {
            $key = $matches[1];
            if (isset($translations[$key])) {
                return sprintf("t(\"%s\", { ns: \"%s\" })", $translations[$key]['key'], $translations[$key]['namespace']);
            }
            return $matches[0];
        },                           $code);
    }
    
}
