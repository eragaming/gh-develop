// src/hooks/useCampingCalculator.ts

import { useCallback }       from "react";
import { Campeur, InfoCase } from "../../../types/components/Outils/Camping.type";
import { campingCalculator } from "../../services/outils/campingCalculator";

export const useCampingCalculator = () => {
	const calculateChances = useCallback((
		infoCase: InfoCase,
		campeur: Campeur,
		position: number,
	): number => {
		return campingCalculator.calculateChances(infoCase, campeur, position);
	}, []);
	
	return {
		calculateChances,
		getMalusDistance        : campingCalculator.getMalusDistance,
		getMalusCampingSuccessif: campingCalculator.getMalusCampingSuccessif,
	};
};