import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { JournalApi }                 from "../../services/api/JournalApi";
import Journal                        from "../../containers/Hotel/Journal/Journal";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function JournalGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const { t } = useTranslation();
	
	const [journalData, setJournalData] = useState(null);
	const params = useParams();
	
	// Fonction pour recharger les données
	const reloadJournalData = async () => {
		const journalApi = new JournalApi(parseInt(params.mapId, 10));
		
		journalApi.main(params.mapId).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
				if (response.zoneRetour.journal === undefined) {
					console.error("Erreur de chargement des données journal", response);
				} else {
					setJournalData(response.zoneRetour.journal);
				}
			} else if (response.codeRetour === 1) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
				navigate("/news");
			}
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("hotel", "journal")) {
			reloadJournalData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{journalData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<Journal journal={journalData} />
			)}
		
		</>
	);
}