import React, { useEffect, useState } from "react";
import Carte                          from "../../containers/Ville/Carte/Carte";
import { CarteApi }                   from "../../services/api/CarteApi";
import chargement                     from "../../../img/chargement.svg";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";


export function CarteGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate, triggerRefresh } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	const { t } = useTranslation();
	const [carteData, setCarteData] = useState(null);
	const [popUpData, setPopUpData] = useState(null);
	
	// Fonction pour recharger les données
	const reloadCarteData = async () => {
		const carteAPI = new CarteApi(parseInt(params.mapId, 10));
		carteAPI.general(params.mapId).then((response) => {
			if (response.status === 204) {
				navigate("/news");
			}
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.carte === undefined) {
				console.error("Erreur de chargement de la carte");
			} else {
				setCarteData(response.data.carte);
			}
			if (response.data.popUpMaj !== undefined) {
				setPopUpData(response.data.popUpMaj);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			navigate("/news");
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("ville", "carte")) {
			reloadCarteData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	if (carteData === null) {
		return <div className={"chargement_page"}>
			<img src={chargement} alt="Drapeau" />
			<span>{t("Chargement...", { ns: "app" })}</span>
		</div>;
	}
	return (
		<>
			<Carte carte={carteData} popUpMaj={popUpData} onRefresh={triggerRefresh} />
		</>
	);
}