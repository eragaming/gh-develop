import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { OutilsVeillesApi }           from "../../services/api/OutilsVeillesApi";
import OutilsVeille                   from "../../containers/Outils/Veilles/OutilsVeille";
import { useNavigate, useParams }     from "react-router";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function OutilsVeillesGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	const { t } = useTranslation();
	const [outilsVeillesData, setOutilsVeillesData] = useState(null);
	
	// Fonction pour recharger les données
	const reloadOutilsVeillesData = async () => {
		const outilsVeillesApi = new OutilsVeillesApi(parseInt(params.id, 10));
		outilsVeillesApi.main().then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.codeRetour === 0) {
				if (response.zoneRetour.outilsVeilles === undefined) {
					console.error("Erreur de chargement des données outils veilles", response);
				} else {
					setOutilsVeillesData(response.zoneRetour.outilsVeilles);
				}
				if (response.zoneRetour.general !== undefined) {
					setGeneral(response.zoneRetour.general);
					sessionStorage.setItem("mapId", response.zoneRetour.general.ville ? response.zoneRetour.general.ville.map_id.toString() : null);
				}
			} else if (response.codeRetour === 1) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
			} else if (response.codeRetour === 2) {
				setStatus(Status_error);
				setShowPop(true);
				setMessagePopUp(response.libRetour);
				navigate("/news");
			}
		});
	};
	
	useEffect(() => {
		
		if (checkHabilitation("outils", "veilles")) {
			reloadOutilsVeillesData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{outilsVeillesData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<>
					{outilsVeillesData !== null && <OutilsVeille />}
				</>
			)}
		
		</>
	);
}