import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { ClassementAmeApi }           from "../../services/api/ClassementAmeApi";
import ClassementAme                  from "../../containers/Ame/ClassementAme";
import { useNavigate }                from "react-router";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useTranslation }             from "react-i18next";

export function ClassementGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const { t } = useTranslation();
	const [ameData, setAmeData] = useState(null);
	
	
	// Fonction pour recharger les données
	const reloadData = async () => {
		const ameApi = new ClassementAmeApi(0);
		
		ameApi.mainClassement().then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.classement === undefined) {
				console.error("Erreur de chargement des ames", response.data);
			} else {
				setAmeData(response.data.classement);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
			navigate("/news");
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("ame", "classement")) {
			reloadData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{ameData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<ClassementAme classementAme={ameData} />
			)}
		
		</>
	);
}