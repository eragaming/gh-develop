import React, { useEffect, useState } from "react";
import chargement                     from "../../../img/chargement.svg";
import { useNavigate, useParams }     from "react-router";
import { CoalitionsJumpApi }          from "../../services/api/CoalitionsJumpApi";
import Coalition                      from "../../containers/Jump/Coalition/Coalition";
import { useGeneralContext }          from "../../types/Context/GeneralContext";
import { useGHContext }               from "../../types/Context/GHContext";
import { Status_error, usePopUp }     from "../../types/Context/PopUpContext";
import { useTranslation }             from "react-i18next";

export function CoalitionGeneral() {
	const { general, setGeneral } = useGeneralContext();
	const { checkHabilitation, refreshKey, setIsOnRefresh, setIsOnUpdate } = useGHContext();
	const { setStatus, setMessagePopUp, setShowPop } = usePopUp();
	const navigate = useNavigate();
	const params = useParams();
	const { t } = useTranslation();
	const [coaData, setCoaData] = useState(null);
	
	// Fonction pour recharger les données
	const reloadData = async () => {
		const coaApi = new CoalitionsJumpApi(parseInt(sessionStorage.getItem("mapId") ?? "0", 10));
		coaApi.gestionCoalition(params.idJump).then((response) => {
			setIsOnRefresh(false);
			setIsOnUpdate(false);
			if (response.data.general !== undefined) {
				setGeneral(response.data.general);
				sessionStorage.setItem("mapId", response.data.general.ville ? response.data.general.ville.map_id.toString() : null);
			}
			if (response.data.coalition === undefined) {
				console.error("Erreur de chargement des données coalition", response);
			} else {
				setCoaData(response.data.coalition);
			}
		}).catch((error) => {
			setStatus(Status_error);
			setShowPop(true);
			setMessagePopUp(error?.data?.error);
			navigate("/news");
		});
	};
	
	useEffect(() => {
		if (checkHabilitation("jump", "coalitions")) {
			reloadData().then(r => r);
		} else {
			navigate("/news");
		}
	}, [refreshKey]);
	
	return (
		<>
			{coaData === null ? (
				<div className={"chargement_page"}>
					<img src={chargement} alt="Drapeau" />
					<span>{t("Chargement...", { ns: "app" })}</span>
				</div>
			) : (
				<Coalition coalition={coaData} />
			)}
		
		</>
	);
}