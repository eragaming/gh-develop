import React, { ChangeEvent, useEffect, useState } from "react";
import { InputProps, NumberInput, useInput }       from "react-admin";

interface ShiftNumberInputProps extends InputProps {
	source: string;
	adjustment: number;
	disabled?: boolean;
}

const ShiftNumberInput = ({ source, adjustment, disabled = false, ...rest }: ShiftNumberInputProps) => {
	const { field, fieldState: { invalid, error } } = useInput({ source });
	const [displayValue, setDisplayValue] = useState<number | string>(field.value - adjustment);
	
	useEffect(() => {
		setDisplayValue(field.value - adjustment);
	}, [field.value, adjustment]);
	
	const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
		const newValue = parseFloat(event.target.value) + adjustment;
		field.onChange(newValue);
	};
	
	
	return (
		<NumberInput
			source={source}
			{...field}
			{...rest}
			value={displayValue}
			onChange={handleChange}
			error={invalid}
			helperText={error ? error.message : null}
			disabled={disabled} />
	);
};

export default ShiftNumberInput;