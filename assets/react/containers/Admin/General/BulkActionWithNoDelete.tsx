import { useRecordContext } from "react-admin";

const BulkActionWithNoDelete = () => {
	useRecordContext();
	
	return null;
};

export default BulkActionWithNoDelete;