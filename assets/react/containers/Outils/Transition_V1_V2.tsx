import React from "react";


export default function Transition_V1_V2(props: { userKey: string, text: string, btn: string, url: string }) {
	
	return <div>
		<p>{props.text}</p>
		<form action={props.url} method="post" target="_self">
			<input type="hidden" name="key" value={props.userKey} />
			<button type="submit" className="button">{props.btn}</button>
		</form>
	</div>;
	
}