import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface UpChantierDTO {
	lvl_actuel?: number;
	days?: number[][];
	destroy?: boolean;
	chantier?: ChantierPrototypeDTO;
}
