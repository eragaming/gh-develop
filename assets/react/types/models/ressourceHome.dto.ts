import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface RessourceHomeDTO {
	id?: number;
	nombre?: number;
	item?: ItemPrototypeDTO;
}
