import { UserDTO }          from "./user.dto";
import { JumpDTO }          from "./jump.dto";
import { TypeVilleDTO }     from "./typeVille.dto";
import { LogEventEventDTO } from "./logEventEvent.dto";

export interface EventDTO {
	id?: string;
	nom?: string;
	deb_inscription_date_at?: string;
	fin_inscription_date_at?: string;
	event_begin_at?: string;
	one_metier?: boolean;
	event_prived?: boolean;
	description?: string;
	banniere?: string;
	community?: string;
	organisateurs?: UserDTO[];
	list_jump?: JumpDTO[];
	type_ville?: TypeVilleDTO;
	log_event?: LogEventEventDTO[];
}
