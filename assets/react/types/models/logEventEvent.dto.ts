import { UserDTO } from "./user.dto";

export interface LogEventEventDTO {
	id?: number;
	libelle?: string;
	event_at?: string;
	valeur_avant?: string;
	valeur_apres?: string;
	declencheur?: UserDTO;
}
