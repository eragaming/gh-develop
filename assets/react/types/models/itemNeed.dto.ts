import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface ItemNeedDTO {
	id?: number;
	number?: number;
	item?: ItemPrototypeDTO;
}
