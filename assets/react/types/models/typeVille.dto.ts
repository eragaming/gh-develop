export interface TypeVilleDTO {
	id?: number;
	nom?: string;
	abrev?: string;
	icon?: string;
}
