import { ItemPrototypeDTO }     from "./itemPrototype.dto";
import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface RegroupementItemsDechargeDTO {
	id?: number;
	nom?: string;
	point_def_base?: number;
	point_def_decharge?: number;
	item_affiches?: ItemPrototypeDTO[];
	regroup_items?: ItemPrototypeDTO[];
	chantier_decharge?: ChantierPrototypeDTO;
}
