import { ItemPrototypeDTO }             from "./itemPrototype.dto";
import { StatutItemExpeditionnaireDTO } from "./statutItemExpeditionnaire.dto";
import { ExpeditionnaireDTO }           from "./expeditionnaire.dto";
import { OuvriersDTO }                  from "./ouvriers.dto";

export interface SacExpeditionnaireDTO {
	id?: number;
	broken?: boolean;
	nbr?: number;
	item?: ItemPrototypeDTO;
	statut?: StatutItemExpeditionnaireDTO;
	expeditionnaire?: ExpeditionnaireDTO;
	ouvriers?: OuvriersDTO;
}
