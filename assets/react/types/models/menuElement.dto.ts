import { MenuPrototypeDTO } from "./menuPrototype.dto";

export interface MenuElementDTO {
	id?: number;
	type_menu?: string;
	ordre?: number;
	name?: string;
	menu?: MenuPrototypeDTO;
	items?: MenuElementDTO[];
}
