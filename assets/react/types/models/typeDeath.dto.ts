export interface TypeDeathDTO {
	nom?: string;
	icon?: string;
	label?: string;
	id_mort?: number;
}
