import { RuineGameZoneDTO } from "./ruineGameZone.dto";
import { UserDTO }          from "./user.dto";

export interface RuineGameDTO {
	id?: string;
	nbr_etage?: number;
	qte_oxygene?: number;
	type_plan?: number;
	nbr_zombie?: number;
	saison?: number;
	mana_kill?: number;
	type_ruine?: string;
	x?: number;
	y?: number;
	z?: number;
	in_piece?: boolean;
	mana_initial?: number;
	begin_at?: string;
	ended_at?: string;
	ejected?: boolean;
	pct_zombie_kill?: number;
	pct_exploration?: number;
	pct_fouille?: number;
	oxygene_restant?: number;
	mana_restant?: number;
	oxygen_lost?: number;
	ruine_game_zones?: RuineGameZoneDTO[];
	user?: UserDTO;
}
