import { UserDTO }                 from "./user.dto";
import { HerosPrototypeDTO }       from "./herosPrototype.dto";
import { RoleJumpDTO }             from "./roleJump.dto";
import { LeadInscriptionDTO }      from "./leadInscription.dto";
import { StatutInscriptionDTO }    from "./statutInscription.dto";
import { LevelRuinePrototypeDTO }  from "./levelRuinePrototype.dto";
import { JobPrototypeDTO }         from "./jobPrototype.dto";
import { DisponibiliteJoueursDTO } from "./disponibiliteJoueurs.dto";
import { LogEventInscriptionDTO }  from "./logEventInscription.dto";
import { HerosSkillLevelDTO }      from "./herosSkillLevel.dto";

export interface InscriptionJumpDTO {
	id?: number;
	ban_commu?: boolean;
	goule_commu?: boolean;
	motivations?: string;
	commentaires?: string;
	bloc_notes?: string;
	reponse?: string;
	date_inscription?: string;
	date_modification?: string;
	moyen_contact?: string;
	type_diffusion?: number;
	fuseau?: string;
	user?: UserDTO;
	pouvoir_futur?: HerosPrototypeDTO;
	role_joueur_jumps?: RoleJumpDTO[];
	lead_inscriptions?: LeadInscriptionDTO[];
	statut?: StatutInscriptionDTO;
	lvl_ruine?: LevelRuinePrototypeDTO;
	voeux_metier1?: JobPrototypeDTO;
	voeux_metier2?: JobPrototypeDTO;
	voeux_metier3?: JobPrototypeDTO;
	dispo?: DisponibiliteJoueursDTO[];
	metier_def?: JobPrototypeDTO;
	log_event_inscriptions?: LogEventInscriptionDTO[];
	skill?: HerosSkillLevelDTO[];
}
