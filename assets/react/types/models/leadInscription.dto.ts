import { TypeLeadDTO } from "./typeLead.dto";

export interface LeadInscriptionDTO {
	id?: number;
	apprenti?: boolean;
	lead?: TypeLeadDTO;
}
