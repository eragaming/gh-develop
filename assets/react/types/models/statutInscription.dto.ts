export interface StatutInscriptionDTO {
	id?: number;
	nom?: string;
	nom_gestion?: string;
	visible_candidature?: boolean;
	order_in_gestion?: number;
	nom_gestion_court?: string;
}
