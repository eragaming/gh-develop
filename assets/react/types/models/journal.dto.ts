export interface JournalDTO {
	def?: number;
	zombie?: number;
	content?: string;
	regen_dir?: string;
	water?: number;
	content_en?: string;
	content_de?: string;
	content_es?: string;
	day?: number;
	direction_sans_article?: string;
	direction_translate?: string;
}
