import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface BanqueDTO {
	broked?: boolean;
	nombre?: number;
	item?: ItemPrototypeDTO;
}
