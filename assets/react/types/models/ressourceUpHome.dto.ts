import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface RessourceUpHomeDTO {
	nombre?: number;
	item?: ItemPrototypeDTO;
}
