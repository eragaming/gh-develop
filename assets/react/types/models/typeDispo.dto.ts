export interface TypeDispoDTO {
	id?: number;
	nom?: string;
	description?: string;
	typologie?: number;
}
