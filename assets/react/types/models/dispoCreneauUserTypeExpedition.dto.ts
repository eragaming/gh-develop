import { TypeDispoDTO }       from "./typeDispo.dto";
import { CreneauHorraireDTO } from "./creneauHorraire.dto";

export interface DispoCreneauUserTypeExpeditionDTO {
	id?: number;
	dispo?: TypeDispoDTO;
	creneau?: CreneauHorraireDTO;
}
