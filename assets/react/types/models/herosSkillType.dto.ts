import { HerosSkillLevelDTO } from "./herosSkillLevel.dto";

export interface HerosSkillTypeDTO {
	id?: number;
	name?: string;
	level?: HerosSkillLevelDTO[];
}
