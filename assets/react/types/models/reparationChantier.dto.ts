import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface ReparationChantierDTO {
	id?: number;
	pv_actuel?: number;
	pa_repa70?: number;
	pa_repa99?: number;
	pa_repa100?: number;
	gain_def70?: number;
	gain_def99?: number;
	gain_def100?: number;
	pct_repa?: number;
	def_actuelle?: number;
	pa_repa_perso?: number;
	gain_def_perso?: number;
	chantier?: ChantierPrototypeDTO;
}
