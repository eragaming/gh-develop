import { ItemBatimentDTO } from "./itemBatiment.dto";

export interface BatPrototypeDTO {
	id?: number;
	nom?: string;
	description?: string;
	km_min?: number;
	km_max?: number;
	bonus_camping?: number;
	explorable?: boolean;
	icon?: string;
	id_mh?: number;
	actif?: boolean;
	max_campeur?: number;
	items?: ItemBatimentDTO[];
	proba_item_globale?: number;
}
