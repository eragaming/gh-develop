import { LvlUpHomeDTO } from "./lvlUpHome.dto";

export interface UpHomePrototypeDTO {
	id?: number;
	nom?: string;
	label?: string;
	description?: string;
	icon?: string;
	levels?: LvlUpHomeDTO[];
}
