export interface LevelRuinePrototypeDTO {
	id?: number;
	level?: number;
	description?: string;
	nom?: string;
}
