import { VilleDTO }           from "./ville.dto";
import { VilleHistoriqueDTO } from "./villeHistorique.dto";
import { TypeDeathDTO }       from "./typeDeath.dto";
import { CleanUpCadaverDTO }  from "./cleanUpCadaver.dto";

export interface HistoriqueVilleDTO {
	id?: number;
	msg?: string;
	clean_name?: string;
	day_of_death?: number;
	ville?: VilleDTO;
	ville_histo?: VilleHistoriqueDTO;
	type_mort?: TypeDeathDTO;
	clean_type_histo?: CleanUpCadaverDTO;
}
