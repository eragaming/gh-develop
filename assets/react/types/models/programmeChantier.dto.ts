import { ChantierPrototypeDTO } from "./chantierPrototype.dto";

export interface ProgrammeChantierDTO {
	id?: number;
	bde?: boolean;
	finir?: boolean;
	pa_a_laisser?: number;
	priority?: number;
	uuid?: string;
	chantier?: ChantierPrototypeDTO;
}
