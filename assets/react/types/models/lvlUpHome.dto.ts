import { RessourceUpHomeDTO } from "./ressourceUpHome.dto";

export interface LvlUpHomeDTO {
	id?: number;
	level?: number;
	pa?: number;
	ressources?: RessourceUpHomeDTO[];
}
