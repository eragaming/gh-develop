import { RuinesObjetsDTO } from "./ruinesObjets.dto";
import { UserDTO }         from "./user.dto";

export interface RuinesCasesDTO {
	id?: number;
	x?: number;
	y?: number;
	type_case?: number;
	type_porte?: string;
	nbr_zombie?: number;
	z?: number;
	type_escalier?: string;
	update_at?: string;
	items?: RuinesObjetsDTO[];
	update_by?: UserDTO;
}
