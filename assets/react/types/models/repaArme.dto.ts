import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface RepaArmeDTO {
	id?: number;
	nombre_broked?: number;
	nombre_a_reparer?: number;
	uuid?: string;
	item?: ItemPrototypeDTO;
}
