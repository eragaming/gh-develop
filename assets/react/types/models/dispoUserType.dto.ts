import { CreneauHorraireDTO } from "./creneauHorraire.dto";
import { TypeDispoDTO }       from "./typeDispo.dto";

export interface DispoUserTypeDTO {
	id?: number;
	type_creneau?: number;
	creneau?: CreneauHorraireDTO;
	dispo?: TypeDispoDTO;
}
