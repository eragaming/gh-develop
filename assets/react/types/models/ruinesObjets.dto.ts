import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface RuinesObjetsDTO {
	id?: number;
	broken?: boolean;
	nombre?: number;
	item?: ItemPrototypeDTO;
}
