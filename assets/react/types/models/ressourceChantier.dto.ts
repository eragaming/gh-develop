import { ItemPrototypeDTO } from "./itemPrototype.dto";

export interface RessourceChantierDTO {
	id?: number;
	nombre?: number;
	item?: ItemPrototypeDTO;
}
