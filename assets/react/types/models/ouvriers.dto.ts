import { JobPrototypeDTO }       from "./jobPrototype.dto";
import { SacExpeditionnaireDTO } from "./sacExpeditionnaire.dto";
import { DispoExpeditionDTO }    from "./dispoExpedition.dto";
import { CitoyensDTO }           from "./citoyens.dto";

export interface OuvriersDTO {
	id?: number;
	soif?: boolean;
	preinscrit?: boolean;
	consigne?: string;
	commentaire?: string;
	position?: number;
	job_fige?: boolean;
	soif_fige?: boolean;
	for_banni?: boolean;
	dispo_rapide?: number;
	job?: JobPrototypeDTO;
	sac?: SacExpeditionnaireDTO[];
	dispo?: DispoExpeditionDTO[];
	citoyen?: CitoyensDTO;
}
