export interface EncyChantierContextType {
	listChantierChoisi: number[],
	setListChantierChoisi: (value: number[]) => void,
}