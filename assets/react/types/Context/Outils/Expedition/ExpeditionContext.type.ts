import { SoifType }      from "../../../components/Outils/OutilsExpedition.type";
import { ExpeditionDTO } from "../../../models/expedition.dto";
import { OuvriersDTO }   from "../../../models/ouvriers.dto";
import { CitoyensDTO }   from "../../../models/citoyens.dto";

export interface ExpeditionContextType {
	expeditions: ExpeditionDTO[],
	setExpeditions: (value: ExpeditionDTO[]) => void,
	expedition: ExpeditionDTO,
	setExpedition: (value: ExpeditionDTO) => void,
	ongletActuel: string,
	setOngletActuel: (value: string) => void,
	loadData: boolean,
	setLoadData: (value: boolean) => void,
	ouvriers: OuvriersDTO[],
	setOuvriers: (value: OuvriersDTO[]) => void,
	refreshOutils: (jour: number) => void,
	optionsSoif: SoifType[],
	citoyensUser: CitoyensDTO[],
	onDuplicate: boolean,
	setOnDuplicate: (value: boolean) => void,
	calculStockRestant: (stock: number[]) => number[],
	onUpdateSeletedExpe: (expedition?: string, forcage?: boolean) => void
	verrouExpes: string[],
	setVerrouExpes: (value: string[]) => void,
}