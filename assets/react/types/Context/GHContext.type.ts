export interface GHContextType {
	checkHabilitation: (niveau1: string, niveau2?: string) => boolean;
	handleClose: () => void;
	refreshKey: number;
	triggerRefresh: () => void;
	triggerUpdate: () => void;
	isOnUpdate: boolean,
	isOnRefresh: boolean,
	setIsOnUpdate: (isOnUpdate: boolean) => void;
	setIsOnRefresh: (isOnRefresh: boolean) => void;
}