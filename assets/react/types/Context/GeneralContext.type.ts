import { GeneralType } from "../components/Generality/General.type";

export interface GeneralContextType {
	general: GeneralType | null;
	setGeneral: (general: GeneralType) => void;
}