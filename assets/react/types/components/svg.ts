interface Svg {
	icone: string;
	bordure: string;
	fleche: string;
	divers: string;
}