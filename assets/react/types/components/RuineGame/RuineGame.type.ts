import { CaseRuine } from "../ville/Ruines.type";
import { UserDTO }   from "../../models/user.dto";

export interface RuineGameMainType {
	liste_niveau_difficulte: NiveauDifficulteArrayType,
	user_css: UserDTO;
	ruine_maze?: CaseRuine[][][],
	ruine_maze_vide?: CaseRuine[][][],
	game?: RuineGameType,
	trad_porte: string[],
}

export interface NiveauDifficulteType {
	nombre_etage: number,
	oxygene: number,
	type_plan: number,
	nb_zombie: number,
}

export interface NiveauDifficulteArrayType {
	etages: number[],
	oxygene: number[],
	plan: number[],
	zombie: number[],
}

export interface RuineGameType {
	id: string,
	nbr_etage: number,
	qte_oxygene: number,
	type_plan: number,
	nbr_zombie: number,
	saison: number,
	user: UserDTO,
	mana_kill: number,
	x?: number,
	y?: number,
	z?: number,
	in_piece?: boolean,
	mana_initial?: number,
	type_ruine?: string,
	begin_at?: string,
	end_at?: string,
	ejected?: boolean,
	pct_zombie_kill?: number,
	pct_exploration?: number,
	pct_fouille?: number,
	oxygene_restant?: number,
	mana_restant?: number,
	oxygen_lost?: number,
}

export interface ScoreRuineGameType {
	pct_kill: number,
	pct_explo: number,
	pct_fouille: number,
	ejected?: boolean,
	oxygen_restant?: number,
}