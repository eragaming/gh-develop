import { HerosPrototypeDTO } from "../../models/herosPrototype.dto";
import { HerosSkillTypeDTO } from "../../models/herosSkillType.dto";

export interface EncyclopedieCitoyensType {
	listHeros: HerosPrototypeDTO[],
	listCompetences: HerosSkillTypeDTO[],
	tabPointDame: number[][],
	tabPointsClean: number[][],
	tabPointsPande: number[][],
	tabDespoir: number[][],
	tabDespoirMort: number[][],
	tabDespoirCascade: number[][],
	tabDespoirCascadeMortJ1: number[][],
	tabDespoirCascadeMortJ2: number[][],
}
