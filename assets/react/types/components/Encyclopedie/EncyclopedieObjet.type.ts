import { TypeActionAssemblageDTO } from "../../models/typeActionAssemblage.dto";
import { ListAssemblageDTO }       from "../../models/listAssemblage.dto";
import { ItemPrototypeDTO }        from "../../models/itemPrototype.dto";
import { TypeCaracteristiqueDTO }  from "../../models/typeCaracteristique.dto";

export interface EncyclopedieObjetType {
	listAction: TypeActionAssemblageDTO[],
	listNeeds: ListAssemblageDTO[]
	listObjets: ItemPrototypeDTO[],
	listObtains: ListAssemblageDTO[],
	listPoubelles: ItemPrototypeDTO[],
	listVeilles: ItemPrototypeDTO[],
	listWeapons: ItemPrototypeDTO[],
	listDecharges: ItemPrototypeDTO[],
	val_def: number[],
	listCaracteristiques: TypeCaracteristiqueDTO[],
}

