import { ClassementPicto }   from "./HistoriquePictos.type";
import { PictoPrototypeDTO } from "../../models/pictoPrototype.dto";

export interface ClassementAmeType {
	listPicto: PictoPrototypeDTO[],
	classPicto: [ClassementPicto[]],
	classPictoVille: [ClassementPicto[]],
	listeSaison: { id: number; nom: string }[],
}
