import { UserDTO }          from "../../models/user.dto";
import { EstimationTdgDTO } from "../../models/estimationTdg.dto";
import { VilleDTO }         from "../../models/ville.dto";

export interface TourDeGuetType {
	jour: number,
	tdg: EstimationTdgDTO[],
	planif_cons: boolean,
	planif: EstimationTdgDTO[],
	user: UserDTO,
	ville: VilleDTO,
}