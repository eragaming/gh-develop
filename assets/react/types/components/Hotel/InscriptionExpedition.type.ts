import { DirectionScrutateur }        from "../Generality/Game.type";
import { ExpeditionDTO }              from "../../models/expedition.dto";
import { ExpeditionTypeDTO }          from "../../models/expeditionType.dto";
import { OuvriersDTO }                from "../../models/ouvriers.dto";
import { CitoyensDTO }                from "../../models/citoyens.dto";
import { JobPrototypeDTO }            from "../../models/jobPrototype.dto";
import { HerosPrototypeDTO }          from "../../models/herosPrototype.dto";
import { UserDTO }                    from "../../models/user.dto";
import { DispoUserTypeExpeditionDTO } from "../../models/dispoUserTypeExpedition.dto";
import { DispoExpeditionDTO }         from "../../models/dispoExpedition.dto";
import { CreneauHorraireDTO }         from "../../models/creneauHorraire.dto";
import { TypeDispoDTO }               from "../../models/typeDispo.dto";
import { VilleDTO }                   from "../../models/ville.dto";
import { ZoneMapDTO }                 from "../../models/zoneMap.dto";

export interface InscriptionExpeditionTypeProps {
	liste: ListeInscriptionExditionType,
	expeditions: ExpeditionDTO[],
	direction_fao: number,
	userOption: UserDTO,
	color_user: ColorUserDispotype,
	ouvriers: OuvriersDTO[],
	citoyens: CitoyensDTO[],
	ouvert_ouvrier: boolean,
	preinscritExpe: CitoyensDTO[],
}

export interface ArchiveInscriptionExpeditionTypeProps {
	expeditions: ExpeditionDTO[],
	direction_fao: number,
	ouvriers: OuvriersDTO[],
	preinscritExpe: CitoyensDTO[],
	ouvert_ouvrier: boolean,
}

export interface AppercuInscriptionExpeditionTypeProps {
	zones: ZoneMapDTO[],
	trace: number[],
	x_min: number,
	x_max: number,
	y_min: number,
	y_max: number,
	ville: VilleDTO,
	user: UserDTO
}

export interface ListeInscriptionExditionType {
	typeExpe: ExpeditionTypeDTO[],
	job: JobPrototypeDTO[],
	pouvoir: HerosPrototypeDTO[],
	direction: DirectionScrutateur[],
	creneau: CreneauHorraireDTO[],
	dispo: TypeDispoDTO[]
}

export interface InscriptionExpeditionSauvegardeType {
	expedition?: {
		expedition_id: string,
		expeditionnaire_id: string,
	},
	ouvrier?: {
		ouvrier_id: number,
	},
	modification: {
		citoyen?: CitoyensDTO,
		place_vide?: boolean,
		dispo?: DispoExpeditionDTO,
		dispos?: DispoUserTypeExpeditionDTO,
		soif?: boolean,
	}
}

export interface InscriptionConsigneSauvegardeType {
	consigne_id: number,
	expedition_part_id: number,
	valide: boolean,
}

export interface ColorUserDispotype {
	dispo_1: string,
	dispo_2: string,
	dispo_3: string,
}