import { MapItemDTO } from "../../models/mapItem.dto";

export interface ZoneMaj {
	dried: number;
	zombie: number | null;
	balise: number | null;
	zombie_min: number | null;
	zombie_max: number | null;
	x: number;
	y: number;
	xRel: number;
	yRel: number;
	distance: number;
	paAller: number;
	paRetour: number;
	items: MapItemDTO[];
	vue: number | null;
}
