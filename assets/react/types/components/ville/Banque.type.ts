import { BanqueDTO }               from "../../models/banque.dto";
import { CategoryObjetDTO }        from "../../models/categoryObjet.dto";
import { TypeActionAssemblageDTO } from "../../models/typeActionAssemblage.dto";
import { ListAssemblageDTO }       from "../../models/listAssemblage.dto";
import { ItemPrototypeDTO }        from "../../models/itemPrototype.dto";

export interface BanqueType {
	banque: BanqueDTO[],
	categories: CategoryObjetDTO[],
	total: number,
	totalCat: number[],
	listAction: TypeActionAssemblageDTO[],
	listNeeds: ListAssemblageDTO[]
	listObjets: ItemPrototypeDTO[],
	listObtains: ListAssemblageDTO[],
}
