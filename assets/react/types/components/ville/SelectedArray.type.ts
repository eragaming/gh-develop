export interface SelectedArrayType {
	obj: string[],
	bat: string[],
	cit: number[],
	expe: string[]
}