export interface ParamCarteType {
	danger: boolean;
	distance: boolean;
	distance_pa: boolean;
	zonage: boolean;
	scrutateur: boolean;
	zombie: boolean;
	epuise: boolean;
	objetSol: boolean;
	objetMarq: boolean;
	citoyen: boolean;
	indicVisite: boolean;
	carteAlter: boolean;
	carteScrut: boolean;
	estimZombie: boolean;
	balisage: boolean;
	arrKm: number[];
	arrPa: number[];
	arrZonage: number[];
	arrAlter: number[];
	ctrl: boolean;
}