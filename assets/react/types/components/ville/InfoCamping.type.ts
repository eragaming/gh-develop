import { BatPrototypeDTO } from "../../models/batPrototype.dto";
import { CitoyensDTO }     from "../../models/citoyens.dto";

export interface InfoCampingType {
	x: number;
	y: number;
	bat: BatPrototypeDTO;
	mapId: number;
	origin: number;
	hard: boolean;
	width: number;
	posX: number;
	posY: number;
	zombie: number;
	phare: boolean;
	citoyens: CitoyensDTO[];
	devast: boolean;
	listBat: BatPrototypeDTO[];
	nbTas: number;
}
