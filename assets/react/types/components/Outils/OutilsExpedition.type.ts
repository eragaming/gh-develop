import { ExpeditionType }      from "../ville/Expedition.type";
import { DirectionScrutateur } from "../Generality/Game.type";
import { JobPrototypeDTO }     from "../../models/jobPrototype.dto";
import { HerosPrototypeDTO }   from "../../models/herosPrototype.dto";
import { ItemPrototypeDTO }    from "../../models/itemPrototype.dto";
import { OutilsExpeditionDTO } from "../../models/outilsExpedition.dto";
import { ExpeditionTypeDTO }   from "../../models/expeditionType.dto";
import { CategoryObjetDTO }    from "../../models/categoryObjet.dto";

export interface OutilsExpeditionTypeProps {
	outilsExpeditions: OutilsExpeditionDTO,
	liste: {
		typeExpe: ExpeditionTypeDTO[],
		job: JobPrototypeDTO[],
		pouvoir: HerosPrototypeDTO[],
		direction: DirectionScrutateur[],
		phrase: string[],
	}
	isModeExpediton: boolean,
	liste_trace: {
		carte: ExpeditionType[],
		outils: ExpeditionType[],
		biblio: ExpeditionType[],
	}
	jour: number,
	popUpMaj: PopUpMajSacExpeditionnaireProps,
	sacRapide: SacRapideType[],
	expeVerrou: string[],
}

export interface SacRapideType {
	id: number,
	nom: string,
	items: ItemPrototypeDTO[],
}


export interface PopUpMajSacExpeditionnaireProps {
	listCategorie: CategoryObjetDTO[];
	listItems: ItemPrototypeDTO[];
}

export interface SoifType {
	value: number,
	label: string,
	icon: string
}