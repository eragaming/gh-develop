export interface TransitionTypePropsType {
	userKey: string,
	text: string,
	btn: string,
	url: string
}