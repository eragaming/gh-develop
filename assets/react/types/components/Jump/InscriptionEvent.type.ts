import { InscriptionOptionsListType } from "./InscriptionJump.type";
import { UserDTO }                    from "../../models/user.dto";
import { JumpDTO }                    from "../../models/jump.dto";
import { InscriptionJumpDTO }         from "../../models/inscriptionJump.dto";

export interface InscriptionEventPropsType {
	options: InscriptionOptionsListType,
	user: UserDTO,
	jump: JumpDTO,
	inscription: InscriptionJumpDTO
}

