import { UserDTO } from "../../models/user.dto";
import { JumpDTO } from "../../models/jump.dto";

export interface ListeCoalitionType {
	listJump: JumpDTO[],
	user: UserDTO,
}
