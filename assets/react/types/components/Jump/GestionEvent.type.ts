import { UserDTO }                   from "../../models/user.dto";
import { TypeVilleDTO }              from "../../models/typeVille.dto";
import { ObjectifVillePrototypeDTO } from "../../models/objectifVillePrototype.dto";
import { EventDTO }                  from "../../models/event.dto";
import { JobPrototypeDTO }           from "../../models/jobPrototype.dto";

export interface GestionEventType {
	options: JumpOptionsListType,
	user: UserDTO,
	event: EventDTO,
}

export interface JumpOptionsListType {
	listCommu: string[],
	listTypeVille: TypeVilleDTO[],
	listObjectif: ObjectifVillePrototypeDTO[],
	statutAcc: number,
	listJob: JobPrototypeDTO[],
}