import { UserDTO }            from "../../models/user.dto";
import { JumpDTO }            from "../../models/jump.dto";
import { EventDTO }           from "../../models/event.dto";
import { InscriptionJumpDTO } from "../../models/inscriptionJump.dto";

export interface ListeJumpEvent {
	listJump: JumpDTO[],
	listJumpArch: JumpDTO[],
	listEvent: EventDTO[],
	listEventArch: EventDTO[],
	listInscription: InscriptionJumpDTO[],
	masqueJump: string[],
	masqueEvent: string[],
	user: UserDTO,
}
