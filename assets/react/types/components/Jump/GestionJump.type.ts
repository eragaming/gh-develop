import { UserDTO }                   from "../../models/user.dto";
import { StatutInscriptionDTO }      from "../../models/statutInscription.dto";
import { TypeLeadDTO }               from "../../models/typeLead.dto";
import { ObjectifVillePrototypeDTO } from "../../models/objectifVillePrototype.dto";
import { TypeVilleDTO }              from "../../models/typeVille.dto";
import { JumpDTO }                   from "../../models/jump.dto";
import { JobPrototypeDTO }           from "../../models/jobPrototype.dto";
import { HerosSkillLevelDTO }        from "../../models/herosSkillLevel.dto";
import { HerosSkillTypeDTO }         from "../../models/herosSkillType.dto";

export interface GestionJumpType {
	options: JumpOptionsListType,
	user: UserDTO,
	jump: JumpDTO,
}

export interface JumpOptionsListType {
	listCommu: string[],
	listTypeVille: TypeVilleDTO[],
	listObjectif: ObjectifVillePrototypeDTO[]
	listLead: TypeLeadDTO[],
	listStatut: StatutInscriptionDTO[],
	listJob: JobPrototypeDTO[],
	listSkill: HerosSkillTypeDTO[],
	listSkillBase: HerosSkillLevelDTO[],
	maxAccepted: number,
	statutAcc: number,
}

