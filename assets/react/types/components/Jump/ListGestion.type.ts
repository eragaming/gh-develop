import { JumpDTO }  from "../../models/jump.dto";
import { EventDTO } from "../../models/event.dto";

export interface ListGestionType {
	listJump: JumpDTO[],
	listEvent: EventDTO[],
	listJumpArch: JumpDTO[],
	listEventArch: EventDTO[],
}
