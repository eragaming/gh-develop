import { JobPrototypeDTO }      from "../../models/jobPrototype.dto";
import { UserDTO }              from "../../models/user.dto";
import { JumpDTO }              from "../../models/jump.dto";
import { StatutInscriptionDTO } from "../../models/statutInscription.dto";
import { TypeDispoJumpDTO }     from "../../models/typeDispoJump.dto";
import { HerosSkillTypeDTO }    from "../../models/herosSkillType.dto";
import { HerosSkillLevelDTO }   from "../../models/herosSkillLevel.dto";

export interface CoalitionPropsType {
	jump: JumpDTO,
	listMoyenContact: string[],
	options: {
		listJob: JobPrototypeDTO[],
		listStatut: StatutInscriptionDTO[],
		listTypeDispo: TypeDispoJumpDTO[],
		listSkill: HerosSkillTypeDTO[],
		listSkillBase: HerosSkillLevelDTO[],
		listSkillLvl: HerosSkillLevelDTO[],
		statut_acc: number,
		id_pouv_archi: number,
		id_pouv_vp: number,
		isOrga: boolean,
		isLeadOrOrga: boolean,
	},
	user: UserDTO,
}