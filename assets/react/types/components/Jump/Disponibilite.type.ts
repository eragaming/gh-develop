import { CreneauJumpDTO } from "../../models/creneauJump.dto";

export interface DisponibiliteJoueursType {
	id: number,
	typeCreneau: number,
	type_creneau: number,
	creneau: CreneauHorraireType,
	dispo: TypeDispoType,
}

export interface CreneauHorraireType {
	id: number,
	libelle: string,
}

export interface TypeDispoType {
	id: number,
	nom: string,
	description: string,
}

export interface TypeDispoJumpType {
	id: number,
	nom: string,
}

export interface StatutJoueurType {
	id: number,
	nom: string,
	nom_gestion: string,
	visible_candidature: string,
	order_in_gestion: number,
	nom_gestion_court: string,
}

export interface DispoJumpType {
	id: number,
	choix_dispo: TypeDispoJumpType,
	creneau_jump: CreneauJumpDTO,
}