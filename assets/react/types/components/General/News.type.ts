import { NewsSiteDTO } from "../../models/newsSite.dto";

export interface NewsType {
	newsData: NewsSiteDTO[];
}

