export interface StatsType {
	statsData: StatsEntry;
}


export interface StatsEntry {
	users: {
		themes: {
			legend: string[];
			nbr: number[];
		};
		power: {
			legend: string[];
			nbr: number[];
		};
	};
	cities: {
		jobs: {
			legend: string[];
			nbr: number[];
			total: number[];
		};
		mort: {
			legend: string[];
			nbr: number[];
		};
		typeMort: {
			legend: string[];
			nbr: number[];
		};
		typeVille: {
			legend: string[];
			nbr: number[];
		};
		dayVille: {
			legend: string[];
			nbr: number[][];
			total: number[];
			legend_titre: string[];
		};
	};
	buildings: {
		work: {
			tabsWork: {
				nom: string
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}[];
			total: {
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}
		};
	};
	maps: {
		ruins: {
			tabsRuins: {
				nom: string
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}[];
			total: {
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}
		};
		scrut: {
			tabsScrut: {
				nom: string
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}[];
			total: {
				nbr: number;
				nbrRne: number;
				nbrRe: number;
				nbrPande: number;
			}
		};
	};
	listSaison: {
		idSaison: number;
		nom: string;
	}[];
}