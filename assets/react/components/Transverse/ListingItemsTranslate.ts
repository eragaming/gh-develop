export type ListingItemsTranslate = {
	rechercheItem: string,
	typeItemCasse: string,
	typeItemEmpoi: string,
	typeItemMarqueur: string,
}