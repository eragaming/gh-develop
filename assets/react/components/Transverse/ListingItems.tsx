import React, { useEffect, useState } from "react";
import SvgIcone                       from "../generality/SvgIcone";
import { ListingItemsTranslate }      from "./ListingItemsTranslate";
import { ItemPrototypeDTO }           from "../../types/models/itemPrototype.dto";
import { CategoryObjetDTO }           from "../../types/models/categoryObjet.dto";
import { useNormalizedSearch }        from "../../services/hook/useNormalizedSearch";
import { useTranslation }             from "react-i18next";
import TooltipGH                      from "../utils/TooltipGH";

interface ListingItemsProps {
	listCategory: CategoryObjetDTO[];
	listItems: ItemPrototypeDTO[];
	itemsTranslate: ListingItemsTranslate;
	itemSelected: (item: ItemPrototypeDTO, broken: boolean) => void;
}

const ListingItems = ({
	listCategory,
	listItems,
	itemsTranslate,
	itemSelected,
}: ListingItemsProps) => {
	const [items, setItems] = useState<ItemPrototypeDTO[]>(listItems);
	const [itemSearch, setItemSearch] = useState<string>("");
	const { normalizeSearch } = useNormalizedSearch();
	const { t } = useTranslation();
	
	useEffect(() => {
		setItems(
			listItems.filter((item) =>
				normalizeSearch(itemSearch, t(item?.nom ?? "", { ns: "items" })),
			),
		);
	}, [itemSearch]);
	
	useEffect(() => {
		setItems(listItems);
	}, [listItems]);
	
	const affIcon = (item: ItemPrototypeDTO) => {
		const listIcon = [];
		
		if (item.type_objet?.id === 3) {
			listIcon.push({
				classImg: "videImg4",
				typeName: itemsTranslate.typeItemMarqueur,
				type    : 4,
			});
		} else {
			if ((item?.id ?? 0) >= 2000 && (item?.id ?? 0) < 3000) {
				listIcon.push({
					classImg: "videImg3",
					typeName: itemsTranslate.typeItemEmpoi,
					type    : 3,
				});
			} else {
				listIcon.push({ classImg: "videImg", typeName: "", type: 1 });
				if (item.type_objet?.id === 1) {
					listIcon.push({
						classImg: "videImg2",
						typeName: itemsTranslate.typeItemCasse,
						type    : 2,
					});
				}
			}
		}
		
		return listIcon.map(
			(value: { classImg: string; typeName: string; type: number }) => (
				<div
					className="videListing"
					key={"item_" + item.id + "_" + value.typeName}
					onClick={() => itemSelected(item, value.type === 2)}
				>
					<TooltipGH>
                        <span className={value.classImg}>
                            <span className="infoBulle">
                                <SvgIcone icone={item?.icon ?? ""} />
                            </span>
                        </span>
						<span className="info">{item.nom} {value.typeName} (id : {item.id})</span>
					</TooltipGH>
				
				</div>
			),
		);
	};
	
	const listItemOnCategorie = (idCate: number) => {
		return items
			.filter((item) => (item?.category_objet?.id ?? 0) === idCate)
			.map((item) => affIcon(item));
	};
	
	const categorieArray = listCategory.map((categorie) => (
		<tr className="ensCatItemListing" key={"categorie_" + categorie.id}>
			<td className="catItemListing fondWhite02">{categorie.nom}</td>
			<td className="tdItemListing fondWhite02">
				<div className="lignItemListing ">
					{listItemOnCategorie(categorie?.id ?? 0)}
				</div>
			</td>
		</tr>
	));
	
	return (
		<React.Fragment>
			<div id="searchItemsListing">
				<label htmlFor="searchItemListing">
					{itemsTranslate.rechercheItem}
				</label>
				<input
					type="search"
					name="searchItemListing"
					id="searchItemListing"
					value={itemSearch}
					onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
						setItemSearch(event.currentTarget.value);
					}}
					onKeyUp={(event: React.KeyboardEvent<HTMLInputElement>) => {
						setItemSearch(event.currentTarget.value);
					}}
					onDoubleClick={() => {
						setItemSearch("");
					}}
					onClick={() => {
						setItemSearch("");
					}}
				/>
			</div>
			<table id="tabItemListing">
				<tbody>{categorieArray}</tbody>
			</table>
		</React.Fragment>
	);
};

export default ListingItems;
