import React, { useState } from "react";

interface SelectProps<T> {
	options: T[];
	onChange: (value: T | null) => void;
	OptionComponent: React.FC<{ option: T }>;
	SelectedOptionComponent: React.FC<{ option: T }>;
	customStyles?: React.CSSProperties;
	label?: string;
}

const SelectCustom: React.FC<SelectProps<any>> = ({ options, onChange, OptionComponent, SelectedOptionComponent, customStyles, label }) => {
	const [selectedOption, setSelectedOption] = useState<any | null>(null);
	const [isOpen, setIsOpen] = useState(false);
	
	const handleSelect = (option: any) => {
		setSelectedOption(option);
		setIsOpen(false);
		onChange(option);
	};
	
	return (
		<div className="selectcustom-select-container">
			<div className="selectcustom-selected-option" onClick={() => setIsOpen(!isOpen)}>
				{selectedOption ? (
					<span className={"selectcustom-empty"}><SelectedOptionComponent option={selectedOption} /><span><i className="fa-solid fa-circle-chevron-down"></i></span></span>
				) : (
					<span className={"selectcustom-empty"}>{label !== undefined && <span>{label}</span>}<span><i className="fa-solid fa-circle-chevron-down"></i></span></span>
				)}
			</div>
			
			{isOpen && (
				<div className="selectcustom-options-container">
					{options.map((option, index) => (
						<div
							key={"option-" + index}
							className="selectcustom-option"
							onClick={() => handleSelect(option)}
						>
							<OptionComponent option={option} />
						</div>
					))}
				</div>
			)}
		</div>
	);
};

export default SelectCustom;