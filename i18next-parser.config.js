module.exports = {
    locales           : ["fr"],
    output            : "translations/js/$NAMESPACE.json",
    keySeparator      : false,
    namespaceSeparator: false,
    reactNamespace    : true,
    defaultValue      : (lng, ns, key) => key, // Pré-remplit la valeur avec la clé elle-même
    lexers            : {
        js     : ["JavascriptLexer"],
        jsx    : [{
            lexer    : "JsxLexer",
            attr     : "i18nKey",
            functions: ["t"], // Ajoutez votre fonction de traduction ici
            namespace: true, // Permet de gérer les namespaces
        }],
        ts     : ["JavascriptLexer"],
        tsx    : [{
            lexer    : "JsxLexer",
            attr     : "i18nKey",
            functions: ["t"], // Ajoutez votre fonction de traduction ici
            namespace: true, // Permet de gérer les namespaces
        }],
        default: ["JavascriptLexer"],
    },
};
