const gulp = require("gulp");
const notify = require("gulp-notify");
const svgSprite = require("gulp-svg-sprite");

// Gestionnaire d'erreurs
function handleError(err) {
    notify.onError({
                       title  : "Gulp Task Error",
                       message: "Check your console.",
                       sound  : "Basso",
                   })(err);

    console.log(err.toString());
    this.emit("end");
}

gulp.task("svg", function() {
    return gulp.src("./assets/images/iconHordes/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("svgBordures", function() {
    return gulp.src("./assets/images/bordures/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite_bord.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("svgDivers", function() {
    return gulp.src("./assets/images/imageDivers/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite_divers.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("svgRuine", function() {
    return gulp.src("./assets/images/imageRuine/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite_ruine.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("svgBatiment", function() {
    return gulp.src("./assets/images/imageBat/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite_bat.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("svgFleche", function() {
    return gulp.src("./assets/images/imageFleche/*.svg")
               .on("error", handleError)
               .pipe(svgSprite({
                                   mode: {
                                       symbol: {
                                           dest  : "./",
                                           sprite: "sprite_fleche.svg",
                                       },
                                   },
                               }))
               .pipe(gulp.dest("./assets/img"));
});

gulp.task("default", gulp.parallel(["svg", "svgBordures", "svgDivers", "svgRuine", "svgFleche", "svgBatiment"]));
